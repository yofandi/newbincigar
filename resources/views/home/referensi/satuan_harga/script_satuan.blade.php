<script>
	$('.spinner_nekr').hide();
	$('#loadingdelete').hide();
	$('.spinner_nekrcin').hide();
	$('#loadingdeletecin').hide();
	$('.spinner_nekrcukai').hide();
	$('#loadingdeletecukai').hide();
	$('.spinner_nekrsti').hide();
	$('#loadingdeletestiker').hide();
	$('.spinner_nekrkemasan').hide();
	$('#loadingdeletekemasan').hide();
	
	$.extend(true, $.fn.datetimepicker.defaults, {
		icons: {
			time: 'far fa-clock',
			date: 'far fa-calendar',
			up: 'fas fa-arrow-up',
			down: 'fas fa-arrow-down',
			previous: 'fas fa-chevron-left',
			next: 'fas fa-chevron-right',
			today: 'fas fa-calendar-check',
			clear: 'far fa-trash-alt',
			close: 'far fa-times-circle'
		}
	});
	
	$("#tanggal_cobacin").datetimepicker({
		format: 'YYYY-MM-DD HH:mm:ss',
	}).data("DateTimePicker").date(new Date());

	let token = $("meta[name='csrf-token']").attr("content");
	const berat = {!! json_encode($berat) !!};

	const jenis = {!! json_encode($jenis) !!}
	jenis.unshift({id_jenis: '',jenis: ''});

	const kategori = {!! json_encode($kategori) !!}
	kategori.unshift({id_kategori: '',kategori: ''});

	const produk = {!! json_encode($produk) !!}
	produk.unshift({id_produk: '',produk: ''});

	function format_number(number, prefix, thousand_separator, decimal_separator)
	{
		var 	thousand_separator = thousand_separator || ',',
		decimal_separator = decimal_separator || '.',
		regex		= new RegExp('[^' + decimal_separator + '\\d]', 'g'),
		number_string = number.toString().replace(regex, ''),
		split	  = number_string.split(decimal_separator),
		rest 	  = split[0].length % 3,
		result 	  = split[0].substr(0, rest),
		thousands = split[0].substr(rest).match(/\d{3}/g);

		if (thousands) {
			separator = rest ? thousand_separator : '';
			result += separator + thousands.join(thousand_separator);
		}
		result = split[1] != undefined ? result + decimal_separator + split[1] : result;
		return prefix == undefined ? result : (result ? prefix + result : '');
	};

	function lproduk(position,like = '') {
		let opt = '';
		for (var i = 0; i < produk.length; i++) {
			opt += '<option value="'+ produk[i].id_produk +'"';
			if (produk[i].id_produk == like) { opt += 'selected'; }
			opt += '>'+ produk[i].produk +'</option>';
		}
		$('#' + position).html(opt);
	}

	function lsubpro(position,base,like = '') {
		$.ajax({
			url: '/cukai/subproduk/json',
			type: 'POST',
			dataType: 'json',
			data: {
				"_token": token,
				produk: base
			},
		})
		.done(function(data) {
			let opt = '<option value=""></option>';
			for (var i = 0; i < data.length; i++) {
				opt += '<option value="'+ data[i].id_sub_produk +'"';
				if (data[i].id_sub_produk == like) { opt += 'selected'; }
				opt += ' data-idkemasan="'+ data[i].id_kemasan +'" data-nama_kemasan="'+ data[i].nama_kemasan +'">'+ data[i].sub_kode +' - '+ data[i].sub_produk +'</option>';
			}
			$('#' + position).html(opt);		
		});
	}
</script>