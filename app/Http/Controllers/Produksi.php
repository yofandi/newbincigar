<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use DataTables;
use Auth;
use Cart;
use App\Noc_inrfs;

class Produksi extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function prosesview()
    {
        $data['jenis'] = DB::table('jenis')->get();
        return view('home.produksi.proses',$data);
    }

    public function probase(Request $request)
    {
    	$query = DB::table('jenis_has_produk')
    	->select('jenis_has_produk.id_jnpro as relid','produk.*')
    	->join('jenis','jenis_has_produk.jenis_id_jenis','=','jenis.id_jenis')
    	->join('produk','jenis_has_produk.produk_id_produk','=','produk.id_produk')
    	->where([['jenis_has_produk.jenis_id_jenis','=',$request->jenis]])
    	->get();
    	echo json_encode($query);
    }

    public function getsubproduk(Request $request)
    {
        $query = DB::table('sub_produk')
        ->select('sub_produk.id_sub_produk','sub_produk.sub_kode','sub_produk.sub_produk')
        ->join('produk','sub_produk.produk_id_produk','=','produk.id_produk')
        ->join('kemasan','sub_produk.kemasan_id_kemasan','=','kemasan.id_kemasan')
        ->where([['sub_produk.produk_id_produk','=',$request->produk]])
        ->get();
        echo json_encode($query);
    }

    public function addsessioncart(Request $request)
    {
        $data = [
            'id' => $request->subproduk,
            'name' => $request->name,
            'qty' => $request->jml,
            'price' => 0,
            'options' => ['packing' => $request->idpacking]
        ];
        $query = Cart::add($data);
        $if = $query ? 'data berhasil ditambahkan' : 'data gagal ditambahkan';
        return json_encode($if);
    }

    public function updatesessioncart(Request $request,$rowId,$name)
    {
        $data = [
            'qty' => $request->jml,
        ];
        $query = Cart::update($rowId,$data);
        $if = $query ? 'data '.$name.' berhasil diupdate' : 'data '.$name.' gagal diupdate';
        return json_encode($if);
    }

    public function deletesessioncart($rowId,$name)
    {
        $query = Cart::remove($rowId);
        $if = $query ? 'data '.$name.' gagal dihapus' : 'data '.$name.' berhasil dihapus';
        return json_encode($if);   
    }

    public function loop_insertcart($id)
    {
        $query = Cart::content();
        foreach ($query as $key) {
            DB::table('noc_items')->insert([
                'sub_produk_id' => $key->id,
                'noc_inrfs_id' => $id,
                'created_at' => date('Y-m-d'),
                'jml_nocitm' => $key->qty,
            ]);
        }
    }

    public function addinrfs(Request $request)
    {
        // arah itu maksudnya barang dikirim dari mana ke mana
        // 0 : packing->quality->rfs
        // 1 : rfs->quality->packing or proses
        $query = new Noc_inrfs;
        $query->tanggal_inrfs = $request->tanggal;
        $query->status_lihat = 0;
        $query->status_inrfs = 0;
        $query->arah_dari = 0;
        $query->keterangan = $request->keterangan;
        $query->author_session = Auth::user()->id;
        $query->created_at = date('Y-m-d');
        $query->log = 'INSERT';
        $save = $query->save();
        if ($save) {
            $id = $query->id;
            $this->loop_insertcart($id);
            $alert = 'Data berhasil ditambahkan, tinggal menunggu verifikasi Quality Control';
        } else {
            $alert = 'Data gagal ditambahkan';
        }
        Cart::destroy();
        echo json_encode($alert);
    }
}
