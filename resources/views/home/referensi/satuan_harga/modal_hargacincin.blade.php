<div class="modal fade" id="modal-hargacincin" tabindex="-1" role="dialog" aria-labelledby="title" aria-hidden="true" style="display: none;">
	<div class="modal-dialog modal-xl">
		<div class="modal-content">
			<div class="card">
				<div class="card-header">
					<div class="modal-header">
						<h4 class="modal-title" id="title-hargacincin"></h4>
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					</div>
				</div>
				<div class="card-body">
					<form id="form-hargacincin" method="POST">
						<div class="modal-body row">
							<div class="form-group col-md-12 fa-3x spinner_nekrcin" align="center">
								<i class="fas fa-spinner fa-spin"></i>
							</div>
							<div class="form-group col-md-12">
								<label class="control-label">Tanggal</label>
								<input type="text" class="form-control" id="tanggal_cincin" placeholder="Tanggal">
							</div>
							<div class="form-group col-md-12">
								<label class="control-label">Kode Harga Cincin</label>
								<input type="text" class="form-control" id="kode_hcincin" placeholder="Kode Harga Cincin">
							</div>
							<div class="form-group col-md-12">
								<label class="control-label">Produk</label>
								<select class="form-control" id="produk-cincin"></select>
							</div>
							<div class="form-group col-md-12">
								<label class="control-label">Harga per satuan</label>
								<input type="text" class="form-control" id="harga_satcincin" value="0">
							</div>
							<div class="form-group col-md-12">
								<label class="control-label">Keterangan</label>
								<textarea id="keterangan-cincin" class="form-control" placeholder="keterangan"></textarea>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" onclick="save_methodcincin()" class="btn btn-primary waves-effect text-left">Save</button>
							<button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Close</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>