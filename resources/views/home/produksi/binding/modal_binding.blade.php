<div class="modal fade" id="modal-binding" tabindex="-1" role="dialog" aria-labelledby="title-tembakau" aria-hidden="true" style="display: none;">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="card">
				<div class="card-header">
					<div class="modal-header">
						<h4 class="modal-title" id="title"></h4>
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					</div>
				</div>
				<div class="card-body">
					<form id="form-binding" method="POST">
						<div class="modal-body row">
							<div class="form-group col-md-12 fa-3x spinner_nekrbind" align="center">
								<i class="fas fa-spinner fa-spin"></i>
							</div>
							<div class="form-group col-md-12">
								<label class="control-label">Tanggal</label>
								<input type="text" class="form-control date_sam" id="tanggal_binding">
							</div>
							<div class="form-group col-md-6">
								<label class="control-label">Jenis</label>
								<select class="form-control" id="jenisbind"></select>
								<input type="hidden" id="jenis_inbind">
							</div>
							<div class="form-group col-md-6">
								<label class="control-label">Produk</label>
								<select class="form-control" id="produkbind"></select>
								<input type="hidden" id="produk_inbind">
							</div>
							<div class="form-group col-md-12" id="stock_showitbind">
								<button type="button" onclick="seacrh_databind()" class="btn btn-primary btn-xs btn-block">Search</button>
								<input type="hidden" id="idprobakubind">
								<input type="hidden" id="idfillingbind">
								<input type="hidden" id="idbinding">
								<input type="hidden" id="idjnprobind">
							</div>
							<div class="form-group col-md-5">
								<label class="control-label">Stock Produksi (Omblad: Gram)</label>
								<input type="number" class="form-control" step="any" id="stockprobind" value="0" placeholder="Stock (Saat ini/Awal)" readonly>
							</div>
							<div class="form-group col-md-3">
								<label class="control-label">Stock Weis (*Gram)</label>
								<input type="number" class="form-control" step="any" id="stockweisb" value="0" placeholder="Stock Weis" readonly>
							</div>
							<div class="form-group col-md-4">
								<label class="control-label">Filling (*Gram)</label>
								<input type="number" class="form-control" id="batangfill" placeholder="Jml Filling" readonly>
							</div>
							<div class="form-group col-md-6">
								<label class="control-label">Terpakai Produksi (*Gram)</label>
								<input type="number" class="form-control" step="any" id="terpakaiprobind" placeholder="Stock Produksi Terpakai">
							</div>
							<div class="form-group col-md-6">
								<label class="control-label">Terpakai Filling (*Gram)</label>
								<input type="number" class="form-control" step="any" id="terpakaifilling" placeholder="Stock Filling Terpakai">
							</div>
							<div class="form-group col-md-6">
								<label class="control-label">Masuk Weis (*Gram)</label>
								<input type="number" class="form-control" step="any" id="masukweisb" placeholder="Stock Masuk Weisf">
							</div>
							<div class="form-group col-md-6">
								<label class="control-label">Terpakai Weis (*Gram)</label>
								<input type="number" class="form-control" step="any" id="terpakaiweisb" placeholder="Stock Produksi Weisf">
							</div>
							<div class="form-group col-md-12">
								<label class="control-label">Batang Cerutu Yang Dihasilkan</label>
								<input type="number" class="form-control" id="batangjmlbind" placeholder="Jml Batang">
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" onclick="save_methodbind()" class="btn btn-primary waves-effect text-left">Save</button>
							<button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Close</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>