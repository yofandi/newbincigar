@extends('../../layout')

@section('title')
<title>BIN - ERP : Laporan Bahan</title>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css" />
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/css/bootstrap-select.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/paper-css/0.3.0/paper.css">
@endsection

@section('content')
@php
$awal = date('Y') - 61;
$akhir = date('Y');
@endphp
<div class="row page-titles">
	<div class="col-md-5 align-self-center">
		<h3 class="text-themecolor">Laporan Bahan</h3>
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a href="javascript:void(0)">Laporan</a></li>
			<li class="breadcrumb-item active">Laporan Bahan</li>
		</ol>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<form class="row">
			<div class="col-md-12" align="center">
				<legend>Laporan Bahan</legend>
			</div>
			<div class="form-group col-md-12">
				<label class="control-label">Bahan Baku</label>
				<select class="form-control" id="bahan">
					@foreach ($bahan as $ken => $bang)
					<option value="{{ $ken }}">{{ $bang }}</option>
					@endforeach
				</select>
			</div>
			<div class="form-group col-md-12" id="jenislaporan">
				<label class="control-label">Jenis Laporan</label>
				<select class="form-control" id="jnslap">
					<option value="0">Objek</option>
					<option value="2">Group</option>
					<option value="1">Rincian</option>
				</select>
			</div>
			<div class="form-group col-md-12" id="tahunselect">
				<label class="control-label">Tahun</label>
				<select class="form-control" id="tahuncont">
					@for($i = $akhir; $i > $awal; $i--)
					<option value="{{ $i }}">{{ $i }}</option>
					@endfor
				</select>
			</div>
			<div class="form-group col-md-5" id="tanggalawld">
				<label class="control-label">Tanggal Awal</label>
				<input type="text" class="form-control" id="tgl_awl" placeholder="Tanggal Awal">
			</div>
			<div class="form-group col-md-2" align="center" id="pjnr">
				<br>
				S.d.
			</div>
			<div class="form-group col-md-5" id="tanggalakhrd">
				<label class="control-label">Tanggal Akhir</label>
				<input type="text" class="form-control" id="tgl_akhr" placeholder="Tanggal Akhir">
			</div>
			<div class="form-group col-md-12" id="satuanberat">
				<label class="control-label">Satuan Berat</label>
				<select class="form-control" id="satuanberatcont">
					@foreach ($berat as $element => $value)
					<option value="{{ $element }}">{{ $value }}</option>
					@endforeach
				</select>
			</div>
			<div class="form-group col-md-12" id="produknedf">
				<label class="form-group">Produk</label>
				<select class="form-control" id="produk" multiple data-actions-box="true" data-live-search="true">
					{{-- <option value="">Semua</option> --}}
					@foreach ($produk as $key)
					<option value="{{ $key->id_produk }}">{{ $key->kode_produk.' - '.$key->produk }}</option>
					@endforeach
				</select>
			</div>
			<div class="form-group col-md-12" id="sub-hidden">
				<label class="form-group">Sub Produk</label>
				<select class="form-control" id="sub_produk" multiple data-actions-box="true" data-live-search="true">
					<option value="">Semua</option>
				</select>
			</div>
			<div class="form-group col-md-12" id="kemasan-hidden">
				<label class="control-label">Packaging</label>
				<select class="form-control" id="kemasan">
					<option value="">Semua</option>
					@foreach ($kemasan as $ejn)
					<option value="{{ $ejn->id_kemasan }}">{{ $ejn->nama_kemasan }}</option>
					@endforeach
				</select>
			</div>
			<div class="form-group col-md-12">
				<button type="button" onclick="render()" class="btn btn-lg btn-info btn-block">Search</button>
			</div>
		</form>
	</div>
</div><br><br>
<div id="loading" class="row" align="center">
	<div class="col-md-12">
		<span class="mdi mdi-48px mdi-spin mdi-loading"></span>
		<h3>Mohon tunggu data tengah diproses..</h3>
	</div>
</div>
<div id="print">
	<div class="row" id="table_html">
	</div>
</div>
<div class="row" id="print_html">
	<div class="col-md-12">
		<div class="form-group">
			<button type="button" onclick="print()" class="btn btn-primary">Print</button>
		</div>
	</div>
</div>
@endsection

@section('javascript')
<script src="{{ asset('node_modules/moment/moment.js') }}"></script>
<script src="{{ asset('js/printThis.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/js/bootstrap-select.min.js"></script>
@include('home/laporan/bahan/script_lbahan')
@endsection