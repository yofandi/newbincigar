<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Stock_probaku extends Model
{
    protected $table = 'stock_probaku';


    protected $fillable = [
    	'jml_produksi','jml_bahanbaku','kategori_id_kategori','jenis_id_jenis'
    ];
}
