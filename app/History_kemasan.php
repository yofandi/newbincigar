<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class History_kemasan extends Model
{
    protected $table = 'history_kemasan';

    protected $fillable = [
    	'tanggal_hiskem',
    	'masuk_kemasan',
    	'terpakai_kemasan',
    	'afkir_kemasan',
    	'stok_now',
    	'ket_kem',
    	'stock_kemasan_id_stock_kemasan',
    	'stock_kemasan_kemasan_id_kemasan',
        'stock_kemasan_sub_produk_id',
    	'stock_kemasan_produk_id_produk',
        'harga_kemasan_id',
        'packing_id',
    	'author_session',
    	'created_at',
    	'updated_at',
    	'log',
    ];
}
