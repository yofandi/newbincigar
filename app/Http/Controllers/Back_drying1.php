<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Drying1;
use App\Stock_wrapping;
use App\Stock_drying1;

class Back_drying1 extends Controller
{
	public function sprodry1($request)
	{
		$qusrt = DB::table('stock_wrapping')
		->select('stock_wrapping.id','stock_wrapping.jml_wrap')
		->join('jenis_has_produk','stock_wrapping.jenis_has_produk_id_jnpro','=','jenis_has_produk.id_jnpro')
		->where([['jenis_has_produk.jenis_id_jenis','=',$request->jenis],['jenis_has_produk.produk_id_produk','=',$request->produk]]);

		$qusrt1 = DB::table('stock_drying1')
		->select('stock_drying1.id','stock_drying1.jml_dry1')
		->join('jenis_has_produk','stock_drying1.jenis_has_produk_id_jnpro','=','jenis_has_produk.id_jnpro')
		->where([['jenis_has_produk.jenis_id_jenis','=',$request->jenis],['jenis_has_produk.produk_id_produk','=',$request->produk]]);

		if ($qusrt->count() > 0) {
			$aem = $qusrt->first();
			$qer = $aem->jml_wrap;
			$qwc = $aem->id;
		} else { $qer = 0; $qwc = '';}

		if ($qusrt1->count() > 0) {
			$aem1 = $qusrt1->first();
			$ber = $aem1->jml_dry1;
			$jkd = $aem1->id;
		} else { $ber = 0; $jkd = ''; }
		$array = ['jml_wrap' => $qer,'jml_dry1' => $ber,'idwrapping' => $qwc,'iddrying1' => $jkd];
		return $array;
	}

	public function stockdry1($request)
	{
		$bang = $request->stockbatang - $request->batangjml;
		$cekwrt = Stock_wrapping::find($request->idwrapping);
		$cekwrt->jml_wrap = $bang;
		$cekwrt->save();
		$resultidwrt = $cekwrt->id;

		if ($request->iddrying1 != '') {
			$id = $request->iddrying1; 
			$show = Stock_drying1::where('id',$id)->first();
			$jml = $show->jml_dry1 + $request->batangjml;

			$cek = Stock_drying1::find($id);
			$cek->jml_dry1 = $jml;
			$cek->save();

			$resultid = $cek->id;
		} else {
			$jml = $request->batangjml;
			$save = new Stock_drying1;

			$save->jml_dry1 = $jml;
			$save->jenis_has_produk_id_jnpro = $request->idjnpro;
			$save->jenis_has_produk_id_jenis = $request->jenis;
			$save->jenis_has_produk_id_produk = $request->produk;
			$save->save();
			$resultid = $save->id;
		}
		return ['idwrapping' => $resultidwrt,'iddrying1' => $resultid,'jml_wrap' => $bang,'jml_dry1' => $jml];
	}

	public function ifuppstockdry1($request,$batang)
	{
		if ($batang != $request->batangjml) {
			$shwrap = Stock_wrapping::where('id',$request->idwrapping)->first();
			$shdry1 = Stock_drying1::where('id',$request->iddrying1)->first();

			if ($batang > $request->batangjml) {
				$nah = $batang - $request->batangjml;
				$hasilw = $shdry1->jml_dry1 - $nah;
				$slash = Stock_wrapping::find($request->idwrapping);
				$slash->jml_wrap = $shwrap->jml_wrap + $nah;
				$slash->save();
			} elseif ($batang < $request->batangjml) {
				$nah = $request->batangjml - $batang;
				$hasilw = $shdry1->jml_dry1 + $nah;
				$slash = Stock_wrapping::find($request->idwrapping);
				$slash->jml_wrap = $shwrap->jml_wrap - $nah;
				$slash->save();
			} else {
				$hasilw = $shdry1->jml_dry1;
			}

			$query2 = Stock_drying1::find($request->iddrying1);
			$query2->jml_dry1 = $hasilw;
			$query2->save();
		}
	}

	public function loop_updry1($request,$id,$batang = '')
	{
		$ceh = Drying1::where([['stock_wrapping_id','=',$request->idwrapping],['stock_drying1_id','=',$request->iddrying1],['id','>',$id]]);
		if ($ceh->count() > 0) {
			$reg = $ceh->get();
			for ($i=0; $i < $ceh->count(); $i++) {
				$show = Drying1::where([['stock_wrapping_id','=',$request->idwrapping],['stock_drying1_id','=',$request->iddrying1],['id','<',$reg[$i]->id]])->orderBy('id','desc')->take('1');
				if ($show->count() > 0) {
					$first = $show->first();
					$hsie = $first->hasil_akhirdry1 - $reg[$i]->tambah_dry1;
				} else {
					if ($batang == '') {
						$awl1 = $reg[$i]->hasil_akhirdry1 + $reg[$i]->tambah_dry1;
						$hsie = $awl1 - $reg[$i]->tambah_dry1;
					} else {
						$awl1 = $reg[$i]->hasil_akhirdry1 + $reg[$i]->tambah_dry1 + $batang;
						$hsie = $awl1 - $reg[$i]->tambah_dry1;
					}
				}
				$update = Drying1::where('id',$reg[$i]->id)->update(['hasil_akhirdry1' => $hsie]);
			}
		}
	}

}
