<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;
use App\Wrapping;
use App\Stock_pressing;
use App\Stock_wrapping;
use App\Stock_probaku;

class Back_wrapping extends Controller
{
	public function sprowrap($request)
	{
		$query = DB::table('stock_probaku')
		->select('stock_probaku.id','stock_probaku.jml_produksi')
		->join('jenis','stock_probaku.jenis_id_jenis','=','jenis.id_jenis')
		->join('kategori','stock_probaku.kategori_id_kategori','=','kategori.id_kategori')
		->where([['stock_probaku.jenis_id_jenis','=',$request->jenis],['stock_probaku.kategori_id_kategori','=',4]]);

		$qusrt = DB::table('stock_pressing')
		->select('stock_pressing.id','stock_pressing.jml_pres')
		->join('jenis_has_produk','stock_pressing.jenis_has_produk_id_jnpro','=','jenis_has_produk.id_jnpro')
		->where([['jenis_has_produk.jenis_id_jenis','=',$request->jenis],['jenis_has_produk.produk_id_produk','=',$request->produk]]);

		$qusrt1 = DB::table('stock_wrapping')
		->select('stock_wrapping.id','stock_wrapping.stock_weiswrap')
		->join('jenis_has_produk','stock_wrapping.jenis_has_produk_id_jnpro','=','jenis_has_produk.id_jnpro')
		->where([['jenis_has_produk.jenis_id_jenis','=',$request->jenis],['jenis_has_produk.produk_id_produk','=',$request->produk]]);

		$njsl = DB::table('sub_produk')
		->select('berat_jadi')
		->where([['produk_id_produk','=',$request->produk]])->first();

		if ($query->count() > 0) {
			$wert = $query->first();
			$wer = $wert->jml_produksi;
			$wqd = $wert->id;
		} else { $wer = 0; $wqd = '';}

		if ($qusrt->count() > 0) {
			$aem = $qusrt->first();
			$qer = $aem->jml_pres;
			$qwc = $aem->id;
		} else { $qer = 0; $qwc = '';}

		if ($qusrt1->count() > 0) {
			$aem1 = $qusrt1->first();
			$ber = $aem1->stock_weiswrap;
			$jkd = $aem1->id;
		} else { $ber = 0; $jkd = ''; }
		$array = ['jml_produksi' => $wer,'jml_pres' => $qer,'stock_weiswrap' => $ber,'id' => $wqd,'idpressing' => $qwc,'idwrapping' => $jkd,'berat_jadi' => $njsl->berat_jadi];
		return $array;
	}

	public function stockwrap($request)
	{
		$bang = $request->stockbatang - $request->batangjml - $request->batangreject;
		$dmes = Stock_pressing::find($request->idpressing);
		$dmes->jml_pres = $bang;
		$dmes->save();
		$resultider = $dmes->id;

		if ($request->idwrapping != '') {
			$id = $request->idwrapping; 
			$show = Stock_wrapping::where('id',$id)->first();
			$jml = $show->jml_wrap + $request->batangjml;
			$weis = $show->stock_weiswrap + $request->masukweisw - $request->terpakaiweisw;

			$cek = Stock_wrapping::find($id);
			$cek->jml_wrap = $jml;
			$cek->stock_weiswrap = $weis;
			$cek->save();

			$resultid = $cek->id;
		} else {
			$jml = $request->batangjml;
			$weis = $request->stockweisw + $request->masukweisw - $request->terpakaiweisw;
			$save = new Stock_wrapping;

			$save->jml_wrap = $jml; 
			$save->stock_weiswrap = $weis;
			$save->jenis_has_produk_id_jnpro = $request->idjnpro;
			$save->jenis_has_produk_id_jenis = $request->jenis;
			$save->jenis_has_produk_id_produk = $request->produk;
			$save->save();
			$resultid = $save->id;
		}
		return ['idpressing' => $resultider,'idwrapping' => $resultid,'jmlbat' => $bang,'jmlwrap' => $jml,'stockwies' => $weis];
	}

	public function ifuppstockwrap($request,$masukweis,$terpoakaipro,$terpakaiweis,$rejectbatang,$batang)
	{
		if ($masukweis != $request->masukweisw || $terpoakaipro != $request->terpakaipro || $terpakaiweis != $request->terpakaiweisw || $rejectbatang != $request->batangreject || $batang != $request->batangjml) {
			$shbaku = Stock_probaku::where('id',$request->idprobaku)->first();
			$shpres = Stock_pressing::where('id',$request->idpressing)->first();
			$shwrap = Stock_wrapping::where('id',$request->idwrapping)->first();
			if ($terpoakaipro > $request->terpakaipro) {
				$whj = $terpoakaipro - $request->terpakaipro;
				$hasil = $shbaku->jml_produksi + $whj;
			} elseif ($terpoakaipro < $request->terpakaipro) {
				$whj = $request->terpakaipro - $terpoakaipro;
				$hasil = $shbaku->jml_produksi - $whj;
			} else {
				$hasil = $shbaku->jml_produksi;
			}

			if ($masukweis > $request->masukweisw) {
				$qnb = $masukweis - $request->masukweisw;
				$awl = $shwrap->stock_weiswrap - $qnb;
			} elseif ($masukweis < $request->masukweisw) {
				$qnb = $request->masukweisw - $masukweis;
				$awl = $shwrap->stock_weiswrap + $qnb;
			} else {
				$awl = $shwrap->stock_weiswrap;
			}

			if ($terpakaiweis > $request->terpakaiweisw) {
				$jkw = $terpakaiweis - $request->terpakaiweisw;
				$hasile = $awl + $jkw;
			} elseif ($terpakaiweis < $request->terpakaiweisw) {
				$jkw = $request->terpakaiweisw - $terpakaiweis;
				$hasile = $awl - $jkw;
			} else {
				$hasile = $awl;
			}

			if ($batang > $request->batangjml) {
				$nah = $batang - $request->batangjml;
				$jksme = $shpres->jml_pres + $nah;
				$hasilw = $shwrap->jml_wrap - $nah;
			} elseif ($batang < $request->batangjml) {
				$nah = $request->batangjml - $batang;
				$jksme = $shpres->jml_pres - $nah;
				$hasilw = $shwrap->jml_wrap + $nah;
			} else {
				$nah = $request->batangjml;
				$jksme = $shpres->jml_pres;
				$hasilw = $shwrap->jml_wrap;
			}

			if ($rejectbatang > $request->batangreject) {
				$jke = $rejectbatang - $request->batangreject;
				$ikkmw = $jksme + $jke;
			} elseif ($rejectbatang < $request->batangreject) {
				$jke = $request->batangreject - $rejectbatang;
				$ikkmw = $jksme - $jke;
			} else {
				$ikkmw = $jksme;
			}

			// ->update(['jml_pres' => $kkn])
			$slash = Stock_pressing::find($request->idpressing);
			$slash->jml_pres = $ikkmw;
			$slash->save();

			$query1 = Stock_probaku::find($request->idprobaku);
			$query1->jml_produksi = $hasil;
			$query1->save();

			$query2 = Stock_wrapping::find($request->idwrapping);
			$query2->jml_wrap = $hasilw;
			$query2->stock_weiswrap = $hasile;
			$query2->save();
		}
	}

	public function loop_upwrap($request,$id,$batang = '',$terpoakaipro = '')
	{
		$ceh = Wrapping::where([['stock_probaku_id','=',$request->idprobaku],['stock_wrapping_id','=',$request->idwrapping],['stock_pressing_id','=',$request->idpressing],['id','>',$id]]);
		if ($ceh->count() > 0) {
			$reg = $ceh->get();
			for ($i=0; $i < $ceh->count(); $i++) {
				$show = Wrapping::where([['stock_probaku_id','=',$request->idprobaku],['stock_wrapping_id','=',$request->idwrapping],['stock_pressing_id','=',$request->idpressing],['id','<',$reg[$i]->id]])->orderBy('id','desc')->take('1');
				if ($show->count() > 0) {
					$first = $show->first();
					$jebpro = $first->sisaprow - $reg[$i]->terpakaiprow;
					$bwrpro = $first->sisaweisw + $reg[$i]->weiswmasuk - $reg[$i]->terpakaiweisb;
					$hsie = $first->hasil_akhirw - ($reg[$i]->tambah_wrap + $reg[$i]->batangreject);
				} else {
					if ($batang == '' || $terpoakaipro == '') {
						$awal = $reg[$i]->sisaprow + $reg[$i]->terpakaiprow;
						$awl1 = $reg[$i]->hasil_akhirw + $reg[$i]->tambah_wrap + $reg[$i]->batangreject;
						$jebpro = $awal - $reg[$i]->terpakaiprow;
						$bwrpro = 0 + $reg[$i]->weiswmasuk - $reg[$i]->terpakaiweisb;
						$hsie = $awl1 - ($reg[$i]->tambah_wrap + $reg[$i]->batangreject);
					} else {
						$awal = $reg[$i]->sisaprow + $reg[$i]->terpakaiprow + $terpoakaipro;
						$jebpro = $awal - $reg[$i]->terpakaiprow;
						$bwrpro = 0 + $reg[$i]->weiswmasuk - $reg[$i]->terpakaiweisb;
						$hsie = $batang - ($reg[$i]->tambah_wrap + $reg[$i]->batangreject);
					}
				}
				$update = Wrapping::where('id',$reg[$i]->id)->update(['sisaprow' => $jebpro,'sisaweisw' => $bwrpro,'hasil_akhirw' => $hsie]);
			}
		}
	}

}
