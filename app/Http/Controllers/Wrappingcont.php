<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use DataTables;
use Auth;
use App\Wrapping;
use App\Stock_filling;
use App\Stock_binding;
use App\Stock_pressing;
use App\Stock_wrapping;
use App\Stock_probaku;

use App\Http\Controllers\Back_wrapping;

class Wrappingcont extends Controller
{
	public function __construct()
	{
		$this->middleware('auth');
		$this->produksi = $res = new Back_wrapping;
	}
	public function searchweiprowrap(Request $request)
	{
		$jenk = $this->produksi->sprowrap($request);
		echo json_encode($jenk);
	}

	public function getdatawrap()
	{
		$query = DB::table('wrapping')
		->select(DB::raw('wrapping.*,jenis.id_jenis,produk.id_produk,jenis.jenis,produk.produk,(SELECT berat_jadi FROM sub_produk WHERE produk_id_produk = produk.id_produk ORDER BY id_sub_produk ASC LIMIT 1) AS berat_jadi'))
		->join('stock_wrapping','wrapping.stock_wrapping_id','=','stock_wrapping.id')
		->join('jenis','stock_wrapping.jenis_has_produk_id_jenis','=','jenis.id_jenis')
		->join('produk','stock_wrapping.jenis_has_produk_id_produk','=','produk.id_produk')
		->get();
		return Datatables::of($query)
		->addIndexColumn()
		->addColumn('stockawal', function ($query) {
			$awl = $query->sisaprow + $query->terpakaiprow;
			return $awl;
		})
		->addColumn('stockawalweis', function ($query) {
			$awl = $query->sisaweisw + $query->terpakaiweisw - $query->weiswmasuk;
			return $awl;
		})
		->addColumn('awlbatang', function ($query) {
			$awl = $query->hasil_akhirw + $query->tambah_wrap + $query->batangreject;
			return $awl;
		})
		->make(true);
	}

	public function datawrapping()
	{
		$data['jenis'] = DB::table('jenis')->select('id_jenis','jenis')->get();
		return view('home.produksi.wrapping.datawrapping',$data);
	}

	public function reject_wrapping($jenis,$value,$status)
	{
		$query1 = Stock_probaku::where([['jenis_id_jenis','=',$jenis],['kategori_id_kategori','=',1]])->first();
		$query2 = Stock_probaku::where([['jenis_id_jenis','=',$jenis],['kategori_id_kategori','=',3]])->first();

		switch ($status) {
			case 'insert':
			$filler = $query1->jml_bahanbaku + $value['filler1'];
			$omblad = $query2->jml_bahanbaku + $value['omblad'];
			break;
			
			case 'update':
			if ($value['filler1before'] > $value['filler1']) {
				$hej = $value['filler1before'] - $value['filler1'];
				$ty = $query1->jml_bahanbaku - $hej;
			} elseif ($value['filler1before'] < $value['filler1']) {
				$hej = $value['filler1'] - $value['filler1before'];
				$ty = $query1->jml_bahanbaku + $hej;
			} else {
				$ty = $query1->jml_bahanbaku;
			}

			if ($value['ombladbefore'] > $value['omblad']) {
				$jke = $value['ombladbefore'] - $value['omblad'];
				$yu = $query2->jml_bahanbaku - $jke;
			} elseif ($value['ombladbefore'] < $value['omblad']) {
				$jke = $value['omblad'] - $value['ombladbefore'];
				$yu = $query2->jml_bahanbaku + $jke;
			} else {
				$yu = $query2->jml_bahanbaku;
			}

			$filler = $ty;
			$omblad = $yu;
			break;

			case 'delete':
			$filler = $query1->jml_bahanbaku - $value['filler1'];
			$omblad = $query2->jml_bahanbaku - $value['omblad'];
			break;

			default:
			break;
		}

		$save1 = Stock_probaku::find($query1->id);
		$save1->jml_bahanbaku = $filler;
		$save1->save();
		$save2 = Stock_probaku::find($query2->id);
		$save2->jml_bahanbaku = $omblad;
		$save2->save();
	}

	public function addwrapping(Request $request)
	{
		DB::beginTransaction();
		$hpro = $request->stockpro - $request->terpakaipro;
		$qq = $this->produksi->stockwrap($request);

		$eql = DB::table('stock_probaku')->where([['jenis_id_jenis','=',$request->jenis],['kategori_id_kategori','=',4],['id','=',$request->idprobaku]])->update(['jml_produksi' => $hpro]);

		$data = [
			'weiswmasuk' => $request->masukweisw,
			'terpakaiprow' => $request->terpakaipro,
			'terpakaiweisw' => $request->terpakaiweisw,
			'sisaprow' => $hpro,
			'sisaweisw' => $qq['stockwies'],
			'tambah_wrap' => $request->batangjml,
			'batangreject' => $request->batangreject,
			'reject_filling' => $request->reject_filling,
			'reject_binding' => $request->reject_binding,
			'hasil_akhirw' => $qq['jmlbat'],
			'keterangan_wrap' => $request->ket,
			'tanggal_wrap' => $request->tanggal_wrapping,
			'stock_probaku_id' => $request->idprobaku,
			'stock_wrapping_id' => $qq['idwrapping'],
			'stock_pressing_id' => $qq['idpressing'],
			'author_session' => Auth::user()->id,
			'created_at' => date('Y-m-d H:i:s'),
			'log' => "INSERT"
		];
		$query1 = Wrapping::insert($data);
		if ($query1) {
			$value['filler1'] = $request->reject_filling * 2;
			$value['omblad'] = $request->reject_binding;
			$this->reject_wrapping($request->jenis,$value,'insert');
			$alert = 'Wrapping berhasil ditambahkan!';
		} else {
			$alert = 'Wrapping gagal ditambahkan!';
		}
		echo json_encode($alert);
		DB::commit();
	}

	public function uppwrapping(Request $request,$id,$masukweis,$terpoakaipro,$terpakaiweis,$rejectbatang,$rejectfill,$rejectbind,$batang)
	{
		DB::beginTransaction();
		$sdrwen = $request->stockpro - $request->terpakaipro;
		$aspqlw = $request->stockweisw + $request->masukweisw - $request->terpakaiweisw;
		$hale = $request->stockbatang - ($request->batangjml + $request->batangreject);

		$kdnk = $this->produksi->ifuppstockwrap($request,$masukweis,$terpoakaipro,$terpakaiweis,$rejectbatang,$batang);

		$data = [
			'weiswmasuk' => $request->masukweisw,
			'terpakaiprow' => $request->terpakaipro,
			'terpakaiweisw' => $request->terpakaiweisw,
			'sisaprow' => $sdrwen,
			'sisaweisw' => $aspqlw,
			'tambah_wrap' => $request->batangjml,
			'batangreject' => $request->batangreject,
			'reject_filling' => $request->reject_filling,
			'reject_binding' => $request->reject_binding,
			'hasil_akhirw' => $hale,
			'keterangan_wrap' => $request->ket,
			'tanggal_wrap' => $request->tanggal_wrapping,
			'author_session' => Auth::user()->id,
			'log' => "UPDATE"
		];
		$query1 = Wrapping::where('id',$id)->update($data);
		if ($query1) {
			$value['filler1before'] = $rejectfill;
			$value['ombladbefore'] = $rejectbind;

			$value['filler1'] = $request->reject_filling;
			$value['omblad'] = $request->reject_binding;
			$this->reject_wrapping($request->jenis,$value,'update');

			$this->produksi->loop_upwrap($request,$id);
			$alert = 'Wrapping ID: '.$id.' berhasil diupdate!';
		} else {
			$alert = 'Wrapping ID: '.$id.' gagal diupdate!';
		}
		echo json_encode($alert);
		DB::commit();
	}

	public function delwrapping(Request $request,$id,$masukweis,$terpakaipro,$terpakaiweis,$rejectbatang,$rejectfill,$rejectbind,$batang)
	{
		DB::beginTransaction();
		$query = Stock_probaku::where('id',$request->idprobaku)->first();
		$qusrt = Stock_pressing::where('id',$request->idpressing)->first();
		$werty = Stock_wrapping::where('id',$request->idwrapping)->first();

		$pro = $query->jml_produksi + $terpakaipro;
		$batangpres = $qusrt->jml_pres + $batang + $rejectbatang;
		$weis = $werty->stock_weiswrap - $masukweis + $terpakaiweis;
		$batang1 = $werty->jml_wrap - $batang;

		$update1 = Stock_probaku::find($request->idprobaku);
		$update1->jml_produksi = $pro;
		$update1->save();
		$update2 = Stock_pressing::find($request->idpressing);
		$update2->jml_pres = $batangpres;
		$update2->save();
		$update3 = Stock_wrapping::find($request->idwrapping);
		$update3->jml_wrap = $batang1;
		$update3->stock_weiswrap = $weis;
		$update3->save();

		$query1 = Wrapping::where('id',$id)->delete();
		if ($query1) {
			$value['filler1'] = $rejectfill;
			$value['omblad'] = $rejectbind;
			$this->reject_wrapping($request->jenis,$value,'delete');

			$this->produksi->loop_upwrap($request,$id,$request->stockbatang,$terpakaipro);
			$alert = 'Wrapping ID : '.$id.' berhasil dihapus!';
		} else {
			$alert = 'Wrapping ID : '.$id.' gagal dihapus!';
		}
		echo json_encode($alert);
		DB::commit();      
	}

}
