@extends('../../layout')

@section('title')
<title>BIN - ERP : Data Pressing</title>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/dmuy/MDTimePicker/mdtimepicker.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css" />
@endsection

@section('content')
<div class="row page-titles">
	<div class="col-md-5 align-self-center">
		<h3 class="text-themecolor">Pressing</h3>
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a href="javascript:void(0)">Proses</a></li>
			<li class="breadcrumb-item active">Data Pressing</li>
		</ol>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="row">
			<div class="col-md-6">
				<legend>Data Pressing</legend>
			</div>
			<div class="col-md-2 offset-md-4">
				<button type="button" data-link="{{ route('pressing.post') }}" class="btn btn-success btn-rounded btn-icons " id="tambah-pressing">Tambah</button>
			</div>
			<div class="table-responsive">
				<table id="table-pressing" class="display nowrap table table-hover table-striped table-bordered">
					<thead>
						<tr>
							<th>No.</th>
							<th>Tanggal</th>
							<th>Produk</th>
							<th>Jenis</th>
							<th>Durasi</th>
							<th>Batang Binding</th>
							<th>Tambah Batang</th>
							<th>JML</th>
							<th>Ket</th>
							<th>Aksi</th>
						</tr>
					</thead>
				</table>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="modal-pressing" tabindex="-1" role="dialog" aria-labelledby="title-tembakau" aria-hidden="true" style="display: none;">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="card">
				<div class="card-header">
					<div class="modal-header">
						<h4 class="modal-title" id="title"></h4>
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					</div>
				</div>
				<div class="card-body">
					<form id="form-pressing" method="POST">
						<div class="modal-body row">
							<div class="form-group col-md-12">
								<label class="control-label">Tanggal</label>
								<input type="text" class="form-control date_sam" id="tanggal_pressing">
							</div>
							<div class="form-group col-md-6">
								<label class="control-label">Jenis</label>
								<select class="form-control" id="jenispres"></select>
								<input type="hidden" id="jenis_inpres">
							</div>
							<div class="form-group col-md-6">
								<label class="control-label">Produk</label>
								<select class="form-control" id="produkpres"></select>
								<input type="hidden" id="produk_inpres">
							</div>
							<div class="form-group col-md-12" id="stock_showitpres">
								<button type="button" onclick="seacrh_datapres()" class="btn btn-primary btn-xs btn-block">Search</button>
								<input type="hidden" id="idbindingpres">
								<input type="hidden" id="idpressing">
								<input type="hidden" id="idjnpropres">
							</div>
							<div class="form-group col-md-12">
								<label class="control-label">Batang Binding</label>
								<input type="number" class="form-control" id="batangbind" placeholder="Jml Batang Binding" readonly>
							</div>
							<div class="form-group col-md-12">
								<label class="control-label">Batang Cerutu Yang Dihasilkan</label>
								<input type="number" class="form-control" id="batangjmlpres" placeholder="Jml Batang">
							</div>
							<div class="form-group col-md-12">
								<label class="control-label">Lama Proses</label>
								<input type="text" class="form-control" id="durasipres" placeholder="Durasi">
							</div>
							<div class="form-group col-md-12">
								<label class="control-label">Keterangan</label>
								<textarea class="form-control" id="ketpres" placeholder="Keterangan"></textarea>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" onclick="save_methodpres()" class="btn btn-primary waves-effect text-left">Save</button>
							<button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Close</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('javascript')
<script src="{{ asset('node_modules/moment/moment.js') }}">
</script><script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
<script src="{{ asset('node_modules/datatables/datatables.min.js') }}"></script>
<script src="https://cdn.jsdelivr.net/gh/dmuy/MDTimePicker/mdtimepicker.js
"></script>
<script>
	$('#stock_showitpres').hide();
	let token = $("meta[name='csrf-token']").attr("content");

	const jenis = {!! json_encode($jenis) !!};
	jenis.unshift({id_jenis: '',jenis: ''});

	var table = $('#table-pressing').DataTable({
		"lengthMenu": [
		[3,5,10, 25, 50, 100, 150, 200, -1],
		[3,5,10, 25, 50, 100, 150, 200, "All"]
		],
		"order": [
		[0, 'desc']
		],
		processing: true,
		serverSide: true,
		ajax: '/datapressing/json',
		columns: [
		{ data: 'DT_RowIndex', name: 'DT_RowIndex' },
		{ data: 'tanggal_pres', name: 'tanggal_pres' },
		{ data: 'produk', name: 'produk' },
		{ data: 'jenis', name: 'jenis' },
		{ data: 'lama', name: 'lama' },
		{ data: 'awlbatang', name: 'awlbatang' },
		{ data: 'tambah_pres', name: 'tambah_pres' },
		{ data: 'hasil_akhirp', name: 'hasil_akhirp' },
		{ data: 'keterangan_pres', name: 'keterangan_pres' },
		{
			data: null,
			"bSortable": false,
			mRender: function (o) { 
				return '<button type="button" class="btn btn-info btn-rounded btn-xs" id="update-pressing" data-title="Update Pressing | ID: '+ o.id +'" data-link="/datapressing/upp/'+ o.id +'/'+ o.tambah_pres +'" data-tanggal_pressing="'+ o.tanggal_pres +'" data-jenis="'+ o.id_jenis +'" data-produk="'+ o.id_produk +'" data-durasi="'+ o.lama +'" data-ket="'+ o.keterangan_pres +'" data-stockbatang="'+ o.awlbatang +'" data-batang="'+ o.tambah_pres +'" data-idpressing="'+ o.stock_pressing_id +'" data-idbinding="'+ o.stock_binding_id +'"><i class="mdi mdi-dots-horizontal"></i></button><button type="button" class="btn btn-danger btn-rounded btn-xs" id="delete-pressing" data-link="/datapressing/del/'+ o.id +'/'+ o.tambah_pres +'" data-idpressing="'+ o.stock_pressing_id +'" data-idbinding="'+ o.stock_binding_id +'"><i class="fa fa-trash"></i></button>'; }
			},
			],
			"displayLength": 10,
		});
	$('#table-pressing tbody').on('click', 'tr.group', function() {
		var currentOrder = table.order()[0];
		if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
			table.order([2, 'desc']).draw();
		} else {
			table.order([2, 'asc']).draw();
		}
	});
	$('#table-pressing tbody').on('click', 'tr.group', function() {
		var currentOrder = table.order()[0];
		if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
			table.order([2, 'desc']).draw();
		} else {
			table.order([2, 'asc']).draw();
		}
	});

	function getdatasearchpres(position1) {
		let jenis = $('#jenis_inpres').val();
		let produk = $('#produk_inpres').val();

		$.ajax({
			url: '/datapressing/getsearchfill',
			type: 'POST',
			dataType: 'json',
			data: {
				"_token": token,
				produk: produk,
				jenis: jenis
			},
		})
		.done(function(data) {
			$('#idbindingpres').val(data.idbinding);
			$('#idpressing').val(data.idpressing);
			$('#' + position1).val(data.jml_bind);
		});
		
	}

	function ljenis(position,like) {
		let opt = '';
		for (let i = 0; i < jenis.length; i++) {
			opt += '<option value="'+ jenis[i].id_jenis +'"';
			if (jenis[i].id_jenis == like) {
				opt += 'selected';
			}
			opt += '>'+ jenis[i].jenis +'</option>';
		}
		$('#' + position).html(opt);
	}

	function lproduk(position,based,like) {
		$.ajax({
			url: '/datafilling/getproduk',
			type: 'POST',
			dataType: 'json',
			data: {
				"_token": token,
				jenis: based
			},
		})
		.done(function(data) {
			let opt = '<option value=""></option>';
			for(let i = 0; i < data.length; i++) {
				opt += '<option value="'+ data[i].id_produk +'"';
				if (data[i].id_produk == like) {
					opt += 'selected';
				}
				opt += ' data-id="'+ data[i].relid +'">'+ data[i].produk +'</option>';
			}
			$('#' + position).html(opt);
		});
	}

	function seacrh_datapres() {
		getdatasearchpres('batangbind');
	}

	function save_methodpres() {
		let link = $('#form-pressing').attr('action');
		let idbinding = $('#idbindingpres').val();
		let idpressing = $('#idpressing').val();
		let idjnpro = $('#idjnpropres').val();
		let tanggal_pressing = $('#tanggal_pressing').val();
		let jenis_in = $('#jenis_inpres').val();
		let produk_in = $('#produk_inpres').val();
		let stockbatang = $('#batangbind').val();
		let batangjml = $('#batangjmlpres').val();
		let durasi = $('#durasipres').val();
		let ket = $('#ketpres').val();
		$.ajax({
			url: link,
			type: 'POST',
			dataType: 'json',
			data: {
				"_token": token,
				tanggal_pressing: tanggal_pressing,
				jenis: jenis_in,
				produk: produk_in,
				durasi:durasi,
				stockbatang: stockbatang,
				batangjml: batangjml,
				ket:ket,
				idpressing: idpressing,
				idbinding: idbinding,
				idjnpro:idjnpro
			},
		})
		.done(function(data) {
			alert(data);
			// console.log(data);
			$('#table-pressing').DataTable().ajax.reload();
			$('#modal-pressing').modal('toggle');
		});
		
	}

	$(document).on('click', '#tambah-pressing', function(event) {
		let clik = $(this);
		$('.modal-header #title').text('Tambah pressing');
		$('#form-pressing').attr('action', clik.data('link'));
		$('#form-pressing').find('input').val('');
		$('#form-pressing').find('select').val('');
		$('#form-pressing').find('textarea').val('');
		$("#tanggal_pressing").datetimepicker({
			format: 'YYYY-MM-DD HH:mm:ss',
		}).data("DateTimePicker").date(new Date());
		$('#durasipres').mdtimepicker({format: 'hh:mm'});
		$('#stock_showitpres').fadeIn();
		$('#jenispres').removeAttr('disabled');
		$('#produkpres').removeAttr('disabled');
		ljenis('jenispres');
		$(document).on('change', '#jenispres', function(event) {
			let jam = $(this).val();
			$('#jenis_inpres').val(jam);
			lproduk('produkpres',jam);
			$(document).on('change', '#produkpres', function(event) {
				let pwe = $(this).val();
				$('#produk_inpres').val(pwe);
				let kk = $(this).find(':selected').data('id');
				$('#idjnpropres').val(kk);
			});
		});
		$('#modal-pressing').modal('show');
	});

	$(document).on('click', '#update-pressing', function(event) {
		let click = $(this);
		$('.modal-header #title').text(click.data('title'));
		$('#form-pressing').attr('action', click.data('link'));
		$('#form-pressing').find('input').val('');
		$('#form-pressing').find('select').val('');
		$('#form-pressing').find('textarea').val('');
		$('#stock_showitpres').hide();
		$('#jenispres').attr('disabled','disabled');
		$('#produkpres').attr('disabled','disabled');
		ljenis('jenispres',click.data('jenis'));
		lproduk('produkpres',click.data('jenis'),click.data('produk'));

		$("#tanggal_pressing").datetimepicker({
			format: 'YYYY-MM-DD HH:mm:ss',
		}).data("DateTimePicker").date(click.data('tanggal_pressing'));
		$('#durasipres').mdtimepicker({format: 'hh:mm'});
		$('#jenis_inpres').val(click.data('jenis'));
		$('#produk_inpres').val(click.data('produk'));
		$('#batangbind').val(click.data('stockbatang'));
		$('#batangjmlpres').val(click.data('batang'));
		$('#idbindingpres').val(click.data('idbinding'));
		$('#idpressing').val(click.data('idpressing'));
		$('#durasipres').val(click.data('durasi'));
		$('#ketpres').val(click.data('ketpres'));
		$('#modal-pressing').modal('show');
	});

	$(document).on('click', '#delete-pressing', function(event) {
		let jsb = confirm('Apakah anda yakin ingin menghapus data ini ?');
		if (jsb) {
			let link = $(this).data("link");
			let idpressing = $(this).data('idpressing');
			let idbinding = $(this).data('idbinding');
			$.ajax(
			{
				url: link,
				type: 'DELETE',
				data: {
					"_token": token,
					idpressing: idpressing,
					idbinding: idbinding,
				},
				success: function (data){
					alert(data);
					$('#table-pressing').DataTable().ajax.reload();
				}
			});
		}
	});
</script>
@endsection