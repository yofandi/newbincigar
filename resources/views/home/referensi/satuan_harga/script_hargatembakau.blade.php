<script>
	var input0 = document.getElementById('harga_satuan');
	input0.addEventListener('keyup', function(e)
	{
		input0.value = format_number(this.value, '');
	}
	);
	
	var table = $('#table-satuan').DataTable({
		"lengthMenu": [
		[3,5,10, 25, 50, 100, 150, 200, -1],
		[3,5,10, 25, 50, 100, 150, 200, "All"]
		],
		"order": [
		[0, 'desc']
		],
		processing: true,
		serverSide: true,
		ajax: '/api/hargatembakau/json',
		columns: [
		{ data: 'DT_RowIndex', name: 'DT_RowIndex' },
		{ data: 'tanggal_harga', name: 'tanggal_harga' },
		{ data: 'kode_htembakau', name: 'kode_htembakau' },
		{ 
			data: null,
			mRender: function(o) {
				return berat[o.satuan_berat];
			}
		},
		{ data: 'jenis', name: 'jenis' },
		{ data: 'kategori', name: 'kategori' },
		{ data: 'hargatembakau_for', name: 'hargatembakau_for' },
		{ data: 'keterangan_harga', name: 'keterangan_harga' },
		{	
			data: null,
			"bSortable": false,
			mRender: function(o) {
				$bind = '<button type="button" class="btn btn-xs btn-rounded btn-warning update-satuan" data-title="Update Harga Tembakau | ID '+ o.id +'" data-link="/api/hargatembakau/update/'+ o.id +'" data-kode="'+ o.kode_htembakau +'" data-satuan="'+ o.satuan_berat +'" data-tanggal_tembakau="'+ o.tanggal_harga +'" data-jenis="'+ o.id_jenis +'" data-kategori="'+ o.kategori_id_kategori +'" data-harga="'+ format_number(o.harga_tembakau, '') +'" data-ket="'+ o.keterangan_harga +'" data-count="'+ o.countdata +'"><i class="mdi mdi-dots-horizontal"></i></button>';
				$bind += '<button type="button" class="btn btn-danger btn-rounded btn-xs" id="delete-satuan" data-id="'+ o.id +'" data-count="'+ o.countdata +'"><i class="fa fa-trash"></i></button>';
				return $bind; 
			}
		},
		],
		"displayLength": 10,
	});
	$('#table-satuan tbody').on('click', 'tr.group', function() {
		var currentOrder = table.order()[0];
		if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
			table.order([2, 'desc']).draw();
		} else {
			table.order([2, 'asc']).draw();
		}
	});
	$('#table-satuan tbody').on('click', 'tr.group', function() {
		var currentOrder = table.order()[0];
		if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
			table.order([2, 'desc']).draw();
		} else {
			table.order([2, 'asc']).draw();
		}
	});

	function beratisi(position,like = '') {
		$hmtl = '';
		$.each(berat, function(index, value) {
			$hmtl += '<option value="'+ index +'"';
			if (like == index) { $hmtl += 'selected'; }
			$hmtl += '>'+ value +'</option>';
		});
		$('#' + position).html($hmtl);
	}

	function ljenis(position,like = '') {
		let opt = '';
		for (var i = 0; i < jenis.length; i++) {
			opt += '<option value="'+ jenis[i].id_jenis +'"';
			if (jenis[i].id_jenis == like) { opt += 'selected'; }
			opt += '>'+ jenis[i].jenis +'</option>';
		}
		$('#' + position).html(opt);
	}

	function lkategori(position,like = '') {
		let opt = '';
		for (var i = 0; i < kategori.length; i++) {
			opt += '<option value="'+ kategori[i].id_kategori +'"';
			if (kategori[i].id_kategori == like) { opt += 'selected'; }
			opt += '>'+ kategori[i].kategori +'</option>';
		}
		$('#' + position).html(opt);
	}

	function Refresh() {
		$('#table-satuan').DataTable().ajax.reload();
	}

	$(document).on('click', '#tambah-satuan', function(event) {
		let click = $(this);
		$('.modal-header #title').text('Tambah Harga Tembakau');
		$('#form-satuan').find('input').val('');
		$('#form-satuan').find('select').val('');
		$('#form-satuan').find('textarea').val('');
		$('#form-satuan').attr('action', click.data('link'));
		beratisi('sat_berat');
		ljenis('jenislah');
		lkategori('kategorilah');
		$("#tanggal_temabaku").datetimepicker({
			format: 'YYYY-MM-DD HH:mm:ss',
		}).data("DateTimePicker").date(new Date());
		$('#modal-satuan').modal('show');
	});

	$(document).on('click', '.update-satuan', function(event) {
		let click = $(this);
		$('.modal-header #title').text(click.data('title'));
		$('#form-satuan').find('input').val('');
		$('#form-satuan').find('select').val('');
		$('#form-satuan').find('textarea').val('');
		$('#form-satuan').attr('action', click.data('link'));
		beratisi('sat_berat', click.data('satuan'));
		ljenis('jenislah',click.data('jenis'));
		lkategori('kategorilah',click.data('kategori'));
		$("#tanggal_temabaku").datetimepicker({
			format: 'YYYY-MM-DD HH:mm:ss',
		}).data("DateTimePicker").date(click.data('tanggal_tembakau'));
		$('#kode_htembakau').val(click.data('kode'));
		$('#harga_satuan').val(click.data('harga'));
		$('#keterangan').val(click.data('ket'));
		$('#modal-satuan').modal('show');
	});

	function save_method() {
		let link = $('#form-satuan').attr('action');
		let tanggal_temabaku = $('#tanggal_temabaku').val();
		let kode_htembakau = $('#kode_htembakau').val();
		let jenis = $('#jenislah').val();
		let kategori = $('#kategorilah').val();
		let sat_berat = $('#sat_berat').val();
		let harga_satuan = $('#harga_satuan').val();
		let keterangan = $('#keterangan').val();
		$.ajax({
			url: link,
			type: 'POST',
			dataType: 'json',
			data: {
				"_token": token,
				tanggal_temabaku: tanggal_temabaku,
				kode_htembakau: kode_htembakau,
				jenis: jenis,
				kategori: kategori,
				satuan_berat: sat_berat,
				harga_satuan: harga_satuan,
				keterangan: keterangan,
			},
			beforeSend: function() {
				isProcessing = true;
				$('.spinner_nekr').fadeIn();
			},
			success: function(data) {
				$('.spinner_nekr').hide();
				isProcessing = false;
				alert(data);
				Refresh();
				$('#modal-satuan').modal('toggle');
			}
		});
	}

	$(document).on('click', '#delete-satuan', function(event) {
		let dje = confirm('Apakah anda yakin ingin menghapus data ini ?');
		if (dje) {
			if ($(this).data("count") > 0) {
				alert('Maaf.. Anda tidak bisa menghapus data ini, dikarenakan akan menimbulkan error pada aplikasi ini!!');
			} else {

				let id = $(this).data("id");
				$.ajax(
				{
					url: '/api/hargatembakau/delete/' + id,
					type: 'DELETE',
					data: {
						"_token": token,
					},
					beforeSend: function() {
						isProcessing = true;
						$('#loadingdelete').fadeIn();
					},
					success: function (data){
						$('#loadingdelete').hide();
						alert(data);
						$('#table-satuan').DataTable().ajax.reload();
						isProcessing = false;
					}
				});
			}
		}
	});
</script>