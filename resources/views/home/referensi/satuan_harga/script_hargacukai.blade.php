<script>
	var input2 = document.getElementById('harga_satcukai');
	input2.addEventListener('keyup', function(e)
	{
		input2.value = format_number(this.value, '');
	}
	);

	function Refresh_cukai() {
		$('#table-hargacukai').DataTable().ajax.reload();
	}

	var tablecukai = $('#table-hargacukai').DataTable({
		"lengthMenu": [
		[3,5,10, 25, 50, 100, 150, 200, -1],
		[3,5,10, 25, 50, 100, 150, 200, "All"]
		],
		"order": [
		[0, 'desc']
		],
		processing: true,
		serverSide: true,
		ajax: '/api/hargacukai/json',
		columns: [
		{ data: 'DT_RowIndex', name: 'DT_RowIndex' },
		{ data: 'tanggal_harga', name: 'tanggal_harga' },
		{ data: 'kode_cukai', name: 'kode_cukai' },
		{ data: 'produk', name: 'produk' },
		{ data: 'kodesubproduk', name: 'kodesubproduk' },
		{ data: 'harga_for', name: 'harga_for' },
		{ data: 'keterangan_hargacukai', name: 'keterangan_hargacukai' },
		{	
			data: null,
			"bSortable": false,
			mRender: function(o) {
				$bind = '<button type="button" class="btn btn-xs btn-rounded btn-warning update-hargacukai" data-title="Update Harga Cukai | ID '+ o.id +'" data-link="/api/hargacukai/update/'+ o.id +'" data-tanggal_cukai="'+ o.tanggal_harga +'" data-kode="'+ o.kode_cukai +'" data-produk="'+ o.id_produk +'" data-subproduk="'+ o.sub_produk_id +'" data-harga="'+ format_number(o.harga, '') +'" data-ket="'+ o.keterangan_hargacukai +'" data-count="'+ o.countdata +'"><i class="mdi mdi-dots-horizontal"></i></button>';
				$bind += '<button type="button" class="btn btn-danger btn-rounded btn-xs" id="delete-hargacukai" data-id="'+ o.id +'" data-count="'+ o.countdata +'"><i class="fa fa-trash"></i></button>';
				return $bind; 
			}
		},
		],
		"displayLength": 10,
	});
	$('#table-hargacukai tbody').on('click', 'tr.group', function() {
		var currentOrder = tablecukai.order()[0];
		if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
			tablecukai.order([2, 'desc']).draw();
		} else {
			table.order([2, 'asc']).draw();
		}
	});
	$('#table-hargacukai tbody').on('click', 'tr.group', function() {
		var currentOrder = tablecukai.order()[0];
		if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
			tablecukai.order([2, 'desc']).draw();
		} else {
			tablecukai.order([2, 'asc']).draw();
		}
	});

	function save_methodcukai() {
		let link = $('#form-hargacukai').attr('action');
		let tanggal_cukai = $('#tanggal_cukai').val();
		let kode_hcukai = $('#kode_hcukai').val();
		let produk = $('#produk-cukai').val();
		let subproduk = $('#subproduk-cukai').val();
		let harga_satuan = $('#harga_satcukai').val();
		let keterangan = $('#keterangan-cukai').val();
		$.ajax({
			url: link,
			type: 'POST',
			dataType: 'json',
			data: {
				"_token": token,
				tanggal_cukai: tanggal_cukai,
				kode_hcukai: kode_hcukai,
				produk: produk,
				subproduk: subproduk,
				harga_satuan: harga_satuan,
				keterangan: keterangan,
			},
			beforeSend: function() {
				isProcessing = true;
				$('.spinner_nekrcukai').fadeIn();
			},
			success: function(data) {
				$('.spinner_nekrcukai').hide();
				isProcessing = false;
				alert(data);
				Refresh_cukai();
				$('#modal-hargacukai').modal('toggle');
			}
		});
	}

	$(document).on('click', '#tambah-hargacukai', function(event) {
		let click = $(this);
		$('.modal-header #title-hargacukai').text('Tambah Harga Cukai');
		$('#form-hargacukai').find('input').val('');
		$('#form-hargacukai').find('select').val('');
		$('#form-hargacukai').find('textarea').val('');
		$('#form-hargacukai').attr('action', click.data('link'));
		lproduk('produk-cukai');
		$('#produk-cukai').change(function(event) {
			let valpro = $(this).val();
			lsubpro('subproduk-cukai',valpro);
		});
		$("#tanggal_cukai").datetimepicker({
			format: 'YYYY-MM-DD HH:mm:ss',
		}).data("DateTimePicker").date(new Date());
		$('#modal-hargacukai').modal('show');
	});

	$(document).on('click', '.update-hargacukai', function(event) {
		let click = $(this);
		$('.modal-header #title-hargacukai').text(click.data('title'));
		$('#form-hargacukai').find('input').val('');
		$('#form-hargacukai').find('select').val('');
		$('#form-hargacukai').find('textarea').val('');
		$('#form-hargacukai').attr('action', click.data('link'));
		lproduk('produk-cukai',click.data('produk'));
		lsubpro('subproduk-cukai',click.data('produk'),click.data('subproduk'));
		$('#harga_satcukai').val(click.data('harga'));
		$('#keterangan-cukai').val(click.data('ket'));
		$('#kode_hcukai').val(click.data('kode'));
		$("#tanggal_cukai").datetimepicker({
			format: 'YYYY-MM-DD HH:mm:ss',
		}).data("DateTimePicker").date(click.data('tanggal_cukai'));
		$('#modal-hargacukai').modal('show');
	});
	
	$(document).on('click', '#delete-hargacukai', function(event) {
		let dje = confirm('Apakah anda yakin ingin menghapus data ini ?');
		if (dje) {
			if ($(this).data("count") > 0) {
				alert('Maaf.. Anda tidak bisa menghapus data ini, dikarenakan akan menimbulkan error pada aplikasi ini!!');
			} else {

				let id = $(this).data("id");
				$.ajax(
				{
					url: '/api/hargacukai/delete/' + id,
					type: 'DELETE',
					data: {
						"_token": token,
					},
					beforeSend: function() {
						isProcessing = true;
						$('#loadingdeletecukai').fadeIn();
					},
					success: function (data){
						$('#loadingdeletecukai').hide();
						alert(data);
						Refresh_cukai();
						isProcessing = false;
					}
				});
			}
		}
	});
</script>