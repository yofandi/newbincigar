<script>
	$(function() {
		$('#loadingdelete').hide();
		$('.spinner_nekr').hide();
		$('#updateview').hide();

		var table = $('#table_reproduksi').DataTable({
			"lengthMenu": [
			[3,5,10, 25, 50, 100, 150, 200, -1],
			[3,5,10, 25, 50, 100, 150, 200, "All"]
			],
			"order": [
			[0, 'desc']
			],
			processing: true,
			serverSide: true,
			ajax: '/api/reproduksi/all',
			columns: [
			{ data: 'DT_RowIndex', name: 'DT_RowIndex' },
			{ data: 'tanggal_repro', name: 'tanggal_repro' },
			{ data: 'jenis', name: 'jenis' },
			{ data: 'produk', name: 'produk' },
			{ data: 'from_position', name: 'from_position' },
			{ data: 'keterangan', name: 'keterangan' },
			{
				data: null,
				"bSortable": false,
				mRender: function (o) { 
					$button = '<button type="button" class="btn btn-info btn-rounded btn-xs" id="update-nontembakau"';
					$button += ' data-link=""><i class="mdi mdi-dots-horizontal"></i></button>';
					$button += '<button type="button" class="btn btn-danger btn-rounded btn-xs" id="delete-nontembakau"';
					$button += ' data-link=""><i class="fa fa-trash"></i></button>';
					return $button;  }
				},
				],
				"displayLength": 10,
			});
		$('#table_reproduksi tbody').on('click', 'tr.group', function() {
			var currentOrder = table.order()[0];
			if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
				table.order([2, 'desc']).draw();
			} else {
				table.order([2, 'asc']).draw();
			}
		});
		$('#table_reproduksi tbody').on('click', 'tr.group', function() {
			var currentOrder = table.order()[0];
			if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
				table.order([2, 'desc']).draw();
			} else {
				table.order([2, 'asc']).draw();
			}
		});

		$(document).on('click', '#refresh_table', function(event) {
			$('#table_reproduksi').DataTable().ajax.reload();
		});

		$("#tanggal_repro").datetimepicker({
			format: 'YYYY-MM-DD HH:mm:ss',
		}).data("DateTimePicker").date(new Date());
	});
</script>