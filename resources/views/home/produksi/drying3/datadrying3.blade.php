@extends('../../layout')

@section('title')
<title>BIN - ERP : Data Humidor</title>
{{-- <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/dmuy/MDTimePicker/mdtimepicker.css"> --}}
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css" />
@endsection

@section('content')
<div class="row page-titles">
	<div class="col-md-5 align-self-center">
		<h3 class="text-themecolor">Humido</h3>
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a href="javascript:void(0)">Proses</a></li>
			<li class="breadcrumb-item active">Data Humidor</li>
		</ol>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="row">
			<div class="col-md-6">
				<legend>Data Humidor</legend>
			</div>
			<div class="col-md-2 offset-md-4">
				<button type="button" data-link="{{ route('drying3.post') }}" class="btn btn-success btn-rounded btn-icons " id="tambah-dry3">Tambah</button>
				<button type="button" onclick="Refresh_drying3()" class="btn btn-rounded btn-icons btn-secondary">Refresh</button>
			</div>
			<div class="col-md-12" align="center" id="loadingdeletedry3">
				<h5>Data tengah diproses. Mohon tunggu sejenak!</h5>
			</div>
			<div class="table-responsive">
				<table id="table-dry3" class="display nowrap table table-hover table-striped table-bordered">
					<thead>
						<tr>
							<th>No.</th>
							<th>Tanggal</th>
							<th>Produk</th>
							<th>Jenis</th>
							<th>Durasi</th>
							<th>Batang Cool</th>
							<th>Tambah Batang</th>
							<th>JML</th>
							<th>Ket</th>
							<th>Aksi</th>
						</tr>
					</thead>
				</table>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="modal-dry3" tabindex="-1" role="dialog" aria-labelledby="title-tembakau" aria-hidden="true" style="display: none;">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="card">
				<div class="card-header">
					<div class="modal-header">
						<h4 class="modal-title" id="title"></h4>
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					</div>
				</div>
				<div class="card-body">
					<form id="form-dry3" method="POST">
						<div class="modal-body row">
							<div class="form-group col-md-12 fa-3x spinner_nekrdry3" align="center">
								<i class="fas fa-spinner fa-spin"></i>
							</div>
							<div class="form-group col-md-12">
								<label class="control-label">Tanggal</label>
								<input type="text" class="form-control date_sam" id="tanggal_drying3">
							</div>
							<div class="form-group col-md-6">
								<label class="control-label">Jenis</label>
								<select class="form-control" id="jenis"></select>
								<input type="hidden" id="jenis_in">
							</div>
							<div class="form-group col-md-6">
								<label class="control-label">Produk</label>
								<select class="form-control" id="produk"></select>
								<input type="hidden" id="produk_in">
							</div>
							<div class="form-group col-md-12" id="stock_showit">
								<button type="button" onclick="seacrh_data()" class="btn btn-primary btn-xs btn-block">Search</button>
								<input type="hidden" id="idcool">
								<input type="hidden" id="iddrying3">
								<input type="hidden" id="idjnpro">
							</div>
							<div class="form-group col-md-12">
								<label class="control-label">Batang Cool</label>
								<input type="number" class="form-control" id="batangcool" placeholder="Jml Batang Cool" readonly>
							</div>
							<div class="form-group col-md-12">
								<label class="control-label">Batang Cerutu Yang Dihasilkan</label>
								<input type="number" class="form-control" id="batangjml" placeholder="Jml Batang">
							</div>
							<div class="form-group col-md-12">
								<label class="control-label">Lama Proses</label>
								<input type="text" class="form-control" id="durasi" placeholder="Durasi">
							</div>
							<div class="form-group col-md-12">
								<label class="control-label">Keterangan</label>
								<textarea class="form-control" id="ket" placeholder="Keterangan"></textarea>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" onclick="save_method()" class="btn btn-primary waves-effect text-left">Save</button>
							<button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Close</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('javascript')
<script src="{{ asset('node_modules/moment/moment.js') }}">
</script><script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
<script src="{{ asset('node_modules/datatables/datatables.min.js') }}"></script>
{{-- <script src="https://cdn.jsdelivr.net/gh/dmuy/MDTimePicker/mdtimepicker.js --}}
"></script>
<script>
	$('#stock_showit').hide();
	$('#loadingdeletedry3').hide();
	$('.spinner_nekrdry3').hide();

	let token = $("meta[name='csrf-token']").attr("content");

	const jenis = {!! json_encode($jenis) !!};
	jenis.unshift({id_jenis: '',jenis: ''});

	var table = $('#table-dry3').DataTable({
		"lengthMenu": [
		[3,5,10, 25, 50, 100, 150, 200, -1],
		[3,5,10, 25, 50, 100, 150, 200, "All"]
		],
		"order": [
		[0, 'desc']
		],
		processing: true,
		serverSide: true,
		ajax: '/datadrying3/json',
		columns: [
		{ data: 'DT_RowIndex', name: 'DT_RowIndex' },
		{ data: 'tanggal_dry3', name: 'tanggal_dry3' },
		{ data: 'produk', name: 'produk' },
		{ data: 'jenis', name: 'jenis' },
		{ data: 'lama_dry3', name: 'lama_dry3' },
		{ data: 'awlbatang', name: 'awlbatang' },
		{ data: 'tambah_dry3', name: 'tambah_dry3' },
		{ data: 'hasil_akhirdry3', name: 'hasil_akhirdry3' },
		{ data: 'keterangan_dry3', name: 'keterangan_dry3' },
		{
			data: null,
			"bSortable": false,
			mRender: function (o) { 
				return '<button type="button" class="btn btn-info btn-rounded btn-xs" id="update-fumigasi" data-title="Update Humidor | ID: '+ o.id +'" data-link="/datadrying3/upp/'+ o.id +'/'+ o.tambah_dry3 +'" data-tanggal_drying3="'+ o.tanggal_dry3 +'" data-jenis="'+ o.id_jenis +'" data-produk="'+ o.id_produk +'" data-durasi="'+ o.lama_dry3 +'" data-ket="'+ o.keterangan_dry3 +'" data-stockbatang="'+ o.awlbatang +'" data-batang="'+ o.tambah_dry3 +'" data-iddrying3="'+ o.stock_drying3_id +'" data-idcool="'+ o.stock_cool_id +'"><i class="mdi mdi-dots-horizontal"></i></button><button type="button" class="btn btn-danger btn-rounded btn-xs" id="delete-fumigasi" data-link="/datadrying3/del/'+ o.id +'/'+ o.tambah_dry3 +'" data-iddrying3="'+ o.stock_drying3_id +'" data-idcool="'+ o.stock_cool_id +'"><i class="fa fa-trash"></i></button>'; }
			},
			],
			"displayLength": 10,
		});
	$('#table-dry3 tbody').on('click', 'tr.group', function() {
		var currentOrder = table.order()[0];
		if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
			table.order([2, 'desc']).draw();
		} else {
			table.order([2, 'asc']).draw();
		}
	});
	$('#table-dry3 tbody').on('click', 'tr.group', function() {
		var currentOrder = table.order()[0];
		if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
			table.order([2, 'desc']).draw();
		} else {
			table.order([2, 'asc']).draw();
		}
	});

	function Refresh_drying3() {
		$('#table-dry3').DataTable().ajax.reload();
	}

	function getdatasearch(position1) {
		let jenis = $('#jenis_in').val();
		let produk = $('#produk_in').val();

		$.ajax({
			url: '/datadrying3/getsearchfill',
			type: 'POST',
			dataType: 'json',
			data: {
				"_token": token,
				produk: produk,
				jenis: jenis
			},
		})
		.done(function(data) {
			$('#idcool').val(data.idcool);
			$('#iddrying3').val(data.iddrying3);
			$('#' + position1).val(data.jml_cool);
		});
		
	}

	function ljenis(position,like) {
		let opt = '';
		for (let i = 0; i < jenis.length; i++) {
			opt += '<option value="'+ jenis[i].id_jenis +'"';
			if (jenis[i].id_jenis == like) {
				opt += 'selected';
			}
			opt += '>'+ jenis[i].jenis +'</option>';
		}
		$('#' + position).html(opt);
	}

	function lproduk(position,based,like) {
		$.ajax({
			url: '/datafilling/getproduk',
			type: 'POST',
			dataType: 'json',
			data: {
				"_token": token,
				jenis: based
			},
		})
		.done(function(data) {
			let opt = '<option value=""></option>';
			for(let i = 0; i < data.length; i++) {
				opt += '<option value="'+ data[i].id_produk +'"';
				if (data[i].id_produk == like) {
					opt += 'selected';
				}
				opt += ' data-id="'+ data[i].relid +'">'+ data[i].produk +'</option>';
			}
			$('#' + position).html(opt);
		});
	}

	function seacrh_data() {
		getdatasearch('batangcool');
	}

	function save_method() {
		let link = $('#form-dry3').attr('action');
		let idcool = $('#idcool').val();
		let iddrying3 = $('#iddrying3').val();
		let idjnpro = $('#idjnpro').val();
		let tanggal_drying3 = $('#tanggal_drying3').val();
		let jenis_in = $('#jenis_in').val();
		let produk_in = $('#produk_in').val();
		let stockbatang = $('#batangcool').val();
		let batangjml = $('#batangjml').val();
		let durasi = $('#durasi').val();
		let ket = $('#ket').val();
		$.ajax({
			url: link,
			type: 'POST',
			dataType: 'json',
			data: {
				"_token": token,
				tanggal_drying3: tanggal_drying3,
				jenis: jenis_in,
				produk: produk_in,
				durasi:durasi,
				stockbatang: stockbatang,
				batangjml: batangjml,
				ket:ket,
				iddrying3: iddrying3,
				idcool: idcool,
				idjnpro:idjnpro
			},
			beforeSend: function() {
				isProcessing = true;
				$('.spinner_nekrdry3').fadeIn();
			},
			success: function(data) {
				$('.spinner_nekrdry3').hide();
				isProcessing = false;
				alert(data);
				Refresh_drying3();
				$('#modal-dry3').modal('toggle');
			}
		});
		
	}

	$(document).on('click', '#tambah-dry3', function(event) {
		let clik = $(this);
		$('.modal-header #title').text('Tambah Humidor');
		$('#form-dry3').attr('action', clik.data('link'));
		$('#form-dry3').find('input').val('');
		$('#form-dry3').find('select').val('');
		$('#form-dry3').find('textarea').val('');
		$("#tanggal_drying3").datetimepicker({
			format: 'YYYY-MM-DD HH:mm:ss',
		}).data("DateTimePicker").date(new Date());
		// $('#durasi').mdtimepicker({format: 'hh:mm'});
		$('#stock_showit').fadeIn();
		$('#jenis').removeAttr('disabled');
		$('#produk').removeAttr('disabled');
		ljenis('jenis');
		$(document).on('change', '#jenis', function(event) {
			let jam = $(this).val();
			$('#jenis_in').val(jam);
			lproduk('produk',jam);
			$(document).on('change', '#produk', function(event) {
				let pwe = $(this).val();
				$('#produk_in').val(pwe);
				let kk = $(this).find(':selected').data('id');
				$('#idjnpro').val(kk);
			});
		});
		$('#modal-dry3').modal('show');
	});

	$(document).on('click', '#update-fumigasi', function(event) {
		let click = $(this);
		$('.modal-header #title').text(click.data('title'));
		$('#form-dry3').attr('action', click.data('link'));
		$('#form-dry3').find('input').val('');
		$('#form-dry3').find('select').val('');
		$('#form-dry3').find('textarea').val('');
		$('#stock_showit').hide();
		$('#jenis').attr('disabled','disabled');
		$('#produk').attr('disabled','disabled');
		ljenis('jenis',click.data('jenis'));
		lproduk('produk',click.data('jenis'),click.data('produk'));

		$("#tanggal_drying3").datetimepicker({
			format: 'YYYY-MM-DD HH:mm:ss',
		}).data("DateTimePicker").date(click.data('tanggal_drying3'));
		// $('#durasi').mdtimepicker({format: 'hh:mm'});
		$('#jenis_in').val(click.data('jenis'));
		$('#produk_in').val(click.data('produk'));
		$('#batangcool').val(click.data('stockbatang'));
		$('#batangjml').val(click.data('batang'));
		$('#idcool').val(click.data('idcool'));
		$('#iddrying3').val(click.data('iddrying3'));
		$('#durasi').val(click.data('durasi'));
		$('#ket').val(click.data('ket'));
		$('#modal-dry3').modal('show');
	});

	$(document).on('click', '#delete-fumigasi', function(event) {
		let bjs = confirm('Apakah anda yakin ingin menghapus data ini ?');
		if (bjs) {
			let link = $(this).data("link");
			let iddrying3 = $(this).data('iddrying3');
			let idcool = $(this).data('idcool');
			$.ajax(
			{
				url: link,
				type: 'DELETE',
				data: {
					"_token": token,
					iddrying3: iddrying3,
					idcool: idcool,
				},
				beforeSend: function() {
					isProcessing = true;
					$('#loadingdeletedry3').fadeIn();
				},
				success: function (data){
					$('#loadingdeletedry3').hide();
					isProcessing = false;	
					alert(data);
					Refresh_drying3();
				}
			});
		}
	});
</script>
@endsection