@extends('../../layout')

@section('title')
<title>BIN - ERP : Reproduksi</title>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css" />
@endsection

@section('content')
<div class="row page-titles">
	<div class="col-md-5 align-self-center">
		<h3 class="text-themecolor">Reproduksi</h3>
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a href="javascript:void(0)">Bahan baku</a></li>
			<li class="breadcrumb-item active">Reproduksi</li>
		</ol>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-body">
				<div class="row">
					<div class="col-md-6">
						<legend>Reproduksi</legend>
					</div>
					<div class="col-md-2 offset-md-4">
						<button type="button" class="btn btn-rounded btn-icons btn-secondary" id="refresh_table"><i class="mdi mdi-refresh"></i></button>
					</div>
					<div class="col-md-12" align="center" id="loadingdelete" style="display: none">
						<h5>Data tengah diproses. Mohon tunggu sejenak!</h5>
					</div>
					<div class="table-responsive">
						<table id="table_reproduksi" class="display nowrap table table-hover table-striped table-bordered">
							<thead>
								<tr>
									<th>No.</th>
									<th>Tanggal</th>
									<th>Jenis</th>
									<th>Produk</th>
									<th>Dari</th>
									<th>Keterangan</th>
									<th>Aksi</th>
								</tr>
							</thead>
						</table>
					</div>
				</div>
				<div class="row" id="updateview">
					<div class="col-md-12">
						<legend>Update Reproduksi</legend><br><br>
					</div>
					<div class="col-md-12">
						<form class="row" method="POST">
							<div class="form-group col-md-12">
								<label>Tanggal</label>
								<input type="text" class="form-control" id="tanggal_repro" name="tanggal" placeholder="Tanggal">
							</div>
							<div class="form-group col-md-12">
								<label>Asal</label>
								<input type="text" class="form-control" id="from_position" name="from_position" placeholder="Asal">
							</div>
							<div class="form-group col-md-12">
								<label>Produk</label>
								<div class="row">
									<div class="col-md-12">
										<div class="table-responsive">
											<table id="item_table" class="table table-hover table-striped">
												<thead>
													<tr>
														<th>No.</th>
														<th>Produk</th>
														<th>Filler 1</th>
														<th>Filler 2</th>
														<th>Omblad</th>
														<th>Dekblad</th>
														<th>Aksi</th>
													</tr>
												</thead>
												<tbody id="isi_table">
													<tr>
														<td></td>
														<td></td>
														<td></td>
														<td></td>
														<td></td>
														<td></td>
														<td></td>
													</tr>
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
							<div class="form-group col-md-12">
								<label>Status</label>
								<select class="form-control" name="status_check" id="status_check">
									<option value="Reject">Reject</option>
									<option value="Reproduksi">Reproduksi</option>
								</select>
							</div>
							<div class="form-group col-md-12">
								<label>Keterangan</label>
								<textarea class="form-control" id="keterangan" name="keterangan" placeholder="Keterangan">
								</textarea>
							</div>
							<div class="form-group col-md-12" align="right">
								<button type="button" class="btn btn-md btn-primary">Save</button>
								<button type="button" class="btn btn-md btn-secondary">Close</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('javascript')
<script src="{{ asset('node_modules/moment/moment.js') }}">
</script><script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
<script src="{{ asset('node_modules/datatables/datatables.min.js') }}"></script>
@include('home.bahan_baku.reproduksi.script_reproduksi')
@endsection