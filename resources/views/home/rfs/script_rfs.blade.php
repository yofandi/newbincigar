<script>
	$.extend(true, $.fn.datetimepicker.defaults, {
		icons: {
			time: 'far fa-clock',
			date: 'far fa-calendar',
			up: 'fas fa-arrow-up',
			down: 'fas fa-arrow-down',
			previous: 'fas fa-chevron-left',
			next: 'fas fa-chevron-right',
			today: 'fas fa-calendar-check',
			clear: 'far fa-trash-alt',
			close: 'far fa-times-circle'
		}
	});

	let token = $("meta[name='csrf-token']").attr("content");
	$('#rejectrfs').hide();
	$('#list-data').fadeIn();
	$('#add-data').hide();
	$('#intop-tambah').hide();
	$('#intop-update').hide();

	const produk = {!! json_encode($produk) !!}
	produk.unshift({id_produk: '',produk: ''});

	const level = {!! json_encode($level) !!}
	level.unshift({id: '',level: ''});

	function Refresh_rfs() {
		$('#table-rfs').DataTable().ajax.reload();
	}

	var table = $('#table-rfs').DataTable({
		"lengthMenu": [
		[3,5,10, 25, 50, 100, 150, 200, -1],
		[3,5,10, 25, 50, 100, 150, 200, "All"]
		],
		"order": [
		[0, 'desc']
		],
		processing: true,
		serverSide: true,
		ajax: '/api/rfs/data/json',
		columns: [
		{ data: 'DT_RowIndex', name: 'DT_RowIndex' },
		{ data: 'tanggal_rfs', name: 'tanggal_rfs' },
		{ data: 'username', name: 'username' },
		{ data: 'level', name: 'level' },
		{ data: 'keterangan_rfs', name: 'keterangan_rfs' },
		{
			data: null,
			"bSortable": false,
			mRender: function (o) { 
				if (o.status_terima == 0) {
					$color = 'light';
				} else if (o.status_terima == 1) {
					$color = 'info';
				} else if (o.status_terima == 2) {
					$color = 'danger';
				}
				return '<span class="badge badge-'+ $color +'">'+ o.status +'</span>'; 
			}
		},
		{
			data: null,
			"bSortable": false,
			mRender: function (o) { 
				// <button type="button" class="btn btn-info btn-rounded btn-xs" id="update-pass"></button>
				$button = '<a href="';
				if (o.status_terima == 1) {
					$button += 'javascript:void(0)';	
				} else {
					$button += '/datarfs/viewupdate/'+ o.id;
				}
				$button += '" class="btn btn-info btn-rounded btn-xs"><i class="mdi mdi-dots-horizontal"></i></a>';
				$button += '<button type="button" id="deleterfs" data-link="/datarfs/delete/'+ o.id +'" class="btn btn-danger btn-rounded btn-xs" id="delete-user" ';
				if (o.status_terima == 1) {
					$button += 'disabled';	
				}
				$button += '><i class="fa fa-trash"></i></button>';
				return $button; 
			}
		},
		],
		"displayLength": 10,
	});

	var table1 = $('#table-stock').DataTable({
		"lengthMenu": [
		[3,5,10, 25, -1],
		[3,5,10, 25, "All"]
		],
		"order": [
		[0, 'desc']
		],
		processing: true,
		serverSide: true,
		ajax: '/api/rfs/datastock/json',
		columns: [
		{ data: 'DT_RowIndex', name: 'DT_RowIndex' },
		{ data: 'sub_kode', name: 'sub_kode' },
		{ data: 'sub_produk', name: 'sub_produk' },
		{ data: 'jml_rfs', name: 'jml_rfs' },
		{ data: null,
			"bSortable": false,
			mRender: function (o) { 
				return '<input type="number" id="valsend" value="0">'; 
			} 
		},
		{
			data: null,
			"bSortable": false,
			mRender: function (o) { 
				return '<button type="button" id="send" data-id="'+ o.id_sub_produk +'" data-name="'+ o.sub_kode +' - '+ o.sub_produk +'" class="btn btn-sm btn-icons btn-outline-primary btn-rounded"><i class="mdi mdi-plus"></i></button>'; 
			}
		},
		],
		"displayLength": 5,
	});

	$("#tanggal_rfs").datetimepicker({
		format: 'YYYY-MM-DD HH:mm:ss',
	}).data("DateTimePicker").date(new Date());

	function lproduk(position,like) {
		let opt = '';
		for (let i = 0; i < produk.length; i++) {
			opt += '<option value="'+ produk[i].id_produk +'"';
			if (produk[i].id_produk == like) {
				opt += 'selected';
			}
			opt += '>'+ produk[i].produk +'</option>';
		}
		$('#' + position).html(opt);
	}

	function lsubproduk(position,based,liker = '') {
		$.ajax({
			url: '/api/Report/getsubproduk/json',
			type: 'POST',
			dataType: 'json',
			data: {
				"_token": token,
				produk: based
			},
		})
		.done(function(data) {
			let opt = '<option value=""></option>';
			for(let i = 0; i < data.length; i++) {
				opt += '<option value="'+ data[i].id_sub_produk +'" data-name="'+ data[i].sub_kode +' - '+ data[i].sub_produk +'" ';
				if (data[i].id_sub_produk == liker) {opt += 'selected ';}
				opt += '>'+ data[i].sub_kode +' - '+ data[i].sub_produk +'</option>';
			}
			$('#' + position).html(opt);
		});
	}

	$(document).on('click', '#tambah-rfs', function(event) {
		// $('#add-data').find('input[type=number]').val(0);
		// $('#add-data').find('select, textarea').val('');
		getsession('table-session');
		$('#loadingtambah_rfs').hide();
		$('#loadingpengiriman').hide();
		$('#loadingkeranjang').hide();
		$('#list-data').hide();
		$('#add-data').fadeIn();
		$('#intop-dta').removeClass('active');
		$('#intop-tambah').fadeIn();
		$('#intop-update').hide();
	});

	function back() {
		// $('#add-data').find('input[type=number]').val(0);
		// $('#add-data').find('select, textarea').val('');
		$('#loadingpengiriman').hide();
		$('#loadingkeranjang').hide();
		$('#list-data').fadeIn();
		$('#add-data').hide();
		$('#intop-tambah');
		$('#intop-update');
		$('#intop-dta').addClass('active');
		$('#intop-tambah').hide();
		$('#intop-update').hide();
	}
	$(document).on('click', '#back_tolist', function(event) {
		back();
	});

	loadlevel('level');
	function loadlevel(position,active = '') {
		opt = '';
		for (let i = 0; i < level.length; i++) {
			opt += '<option value="'+ level[i].id +'"';
			if (level[i].id == active) {
				opt +=  'selected';
			}
			opt += '>'+ level[i].level +'</option>';
		}
		$('#' + position).html(opt);
	}
	
	$(document).on('change', '#level', function(event) {
		loaduser('userbagan',$(this).val());
	});

	function loaduser(position,base,active = '') {
		$.ajax({
			url: '/api/rfs/getbaganuser',
			type: 'POST',
			dataType: 'json',
			data: {
				"_token": token,
				level: base
			},
		})
		.done(function(data) {
			let opt = '<option value=""></option>';
			for(let i = 0; i < data.length; i++) {
				opt += '<option value="'+ data[i].id +'" ';
				if (data[i].id == active) {opt += 'selected ';}
				opt += '>'+ data[i].username +'</option>';
			}
			$('#' + position).html(opt);
		});
	}

	function showforitem(post,produk,subproduk) {
		$.ajax({
			url: '/api/rejectrfs/item',
			type: 'POST',
			dataType: 'json',
			data: {
				"_token": token,
				produk: produk,
				subproduk: subproduk,
			},
		})
		.done(function(data) {
			$('#' + post).val(data.jml);
			$('#idstockrfscart').val(data.id);
		});
	}

	function getsession(posisi) {
		$.ajax({
			url: 'api/rejectrfs/get',
			type: 'GET',
			dataType: 'json',
		})
		.done(function(datas) {
			$no = 1;
			$html = '';
			if (datas.length > 0) {
				for (var i = 0; i < datas.length; i++) {
					
					$html += '<tr>';
					$html += '<td>'+$no+'</td>';
					$html += '<td>'+datas[i].name+'</td>';
					$html += '<td class="table_datasess" data-row_id="'+datas[i].rowId+'" data-column_name="qty" contenteditable>'+datas[i].qty+'</td>';
					$html += '<td>';
					$html += '<button type="button" class="btn btn-danger btn-sm" id="deletecart" data-link="/api/rejectrfs/delete/'+datas[i].rowId+'/'+datas[i].name+'"><i class="mdi mdi-delete-empty"></i></button>';
					$html += '</td>';
					$html += '</tr>';
					$no++;
				}
			} else {
				$html += '<tr>';
				$html += '<td colspan="4">No data in table</td>';
				$html += '</tr>';
			}
			$('#' + posisi).html($html);
		});
	}

	function getthis() {
		showforitem('stockjmlrfs_for',$('#produk_reject').val(),$('#subproduk_reject').val());
	}

	function change_position() {
		let div = $('#rejectrfs');
		$('.spinner_nekraddrfs').hide();
		$('#loadingdeletecart').hide();

		div.fadeToggle(500, function() {
			$('#rejectisi').find('input').val('');
			$('#rejectisi').find('select').val('');

			$("#tanggal_reject").datetimepicker({
				format: 'YYYY-MM-DD HH:mm:ss',
			}).data("DateTimePicker").date(new Date());
			lproduk('produk_reject');
			$(document).on('change', '#produk_reject', function(event) {
				let val = $(this).val();
				$('#produk_hidinrfs').val(val);
				lsubproduk('subproduk_reject',val);
				$(document).on('change', '#subproduk_reject', function(event) {
					let lke = $(this).val();
					let lep = $(this).find(':selected').data('name');

					$('#namesubproduk_hid').val(lep);
					$('#subproduk_hidinreject').val(lke);
				});
			});
			getsession('data_cart');
		});
	}

	$(document).on('blur', '.table_datasess', function(){
		var id = $(this).data('row_id');
		var table_column = $(this).data('column_name');
		var value = $(this).text();
		$.ajax({
			url: '/api/rejectrfs/uppcart',
			method:"PUT",
			data:{
				"_token": token,
				rowId:id,
				column_name:table_column,
				jml:value
			},
			beforeSend: function() {
				isProcessing = true;
				$('#loadingdeletecart').fadeIn();
				$('#loadingkeranjang').fadeIn();
			},
			success: function (data){
				$('#loadingdeletecart').hide();
				$('#loadingkeranjang').hide();
				isProcessing = false;
				alert(data);
				getsession('data_cart');
				getsession('table-session');
			}
		})
	});

	function save_sessionreject() {
		let idrfs = $('#idstockrfscart').val();
		let subproduk = $('#subproduk_reject').val();
		let name = $('#namesubproduk_hid').val();
		let stock = $('#stockjmlrfs_for').val();
		let jml = $('#jml_reject').val();
		
		$.ajax({
			url: '/api/rejectrfs/postcart',
			type: 'POST',
			dataType: 'json',
			data: {
				"_token": token,
				idrfs: idrfs,
				subproduk: subproduk,
				name: name,
				stock: stock,
				jml: jml,
			},
			beforeSend: function() {
				isProcessing = true;
				$('.spinner_nekraddrfs').fadeIn();
			},
			success: function (data){
				$('.spinner_nekraddrfs').hide();
				isProcessing = false;
				alert(data);
				getsession('data_cart');
				$('#rejectisi').find('input').val('');
				$('#rejectisi').find('select').val('');
				// change_position();
			},
		});
	}

	$(document).on('click', '#deletecart', function(event) {
		let hks = confirm('Apakah anda yakin ingin menghapus data ini ?');
		if (hks) {
			let click = $(this);
			$.ajax({
				url: click.data('link'),
				type: 'DELETE',
				dataType: 'json',
				data: {
					"_token": token,
				},
				beforeSend: function() {
					isProcessing = true;
					$('#loadingdeletecart').fadeIn();
					$('#loadingkeranjang').fadeIn();
				},
				success: function (data){
					$('#loadingdeletecart').hide();
					$('#loadingkeranjang').hide();
					isProcessing = false;
					alert(data);
					getsession('data_cart');
					getsession('table-session');
				}
			});
		}
	});

	function save_rejectrfs() {
		let tanggal_inrfs = $('#tanggal_reject').val();
		let keterangan = $('#keterangan').val();
		let pesan = $('#pesan').val();
		$.ajax({
			url: '/api/rejectrfs/postall',
			type: 'POST',
			dataType: 'json',
			data: {
				"_token": token,
				tanggal: tanggal_inrfs,
				keterangan: keterangan,
				pesan: pesan,
			},
			beforeSend: function() {
				isProcessing = true;
				$('.spinner_nekraddrfs').fadeIn();
			},
			success: function(data) {
				$('.spinner_nekraddrfs').hide();
				isProcessing = false;
				alert(data);
				change_position();
			}
		});

	}

	$(document).on('click', '#send', function(event) {
		let kei = $(this);
		let value = $('#valsend').val();
		$.ajax({
			url: '/api/kirimbagan/addcart',
			type: 'POST',
			dataType: 'json',
			data: {
				"_token": token,
				subproduk: kei.data('id'),
				name: kei.data('name'),
				jml: value,
			},
			beforeSend: function() {
				isProcessing = true;
				$('#loadingpengiriman').fadeIn();
				// $('#loadingkeranjang').hide();
			},
			success: function(data) {
				$('#loadingpengiriman').hide();
				alert(data);
				isProcessing = false;
				getsession('table-session');
				// $('#table-stock').DataTable().ajax.reload();
			},
		});
	});

	function tambah_rfs() {
		let tanggal_rfs = $('#tanggal_rfs').val();
		let level = $('#level').val();
		let userbagan = $('#userbagan').val();
		let ket_rfs = $('#ket_rfs').val();
		$.ajax({
			url: '/datarfs/post',
			type: 'POST',
			dataType: 'json',
			data: {
				"_token": token,
				tanggal_rfs: tanggal_rfs,
				level: level,
				userbagan: userbagan,
				ket_rfs: ket_rfs,
			},
			beforeSend: function() {
				isProcessing = true;
				$('#loadingtambah_rfs').fadeIn();
				// $('#loadingkeranjang').hide();
			},
			success: function(data) {
				$('#loadingtambah_rfs').hide();
				alert(data);
				isProcessing = false;
				getsession('table-session');
				back();
				$('#add-data').find('select, textarea').val('');
				$('#table-rfs').DataTable().ajax.reload();
			},
		});
	}

	$(document).on('click', '#deleterfs', function(event) {
		let data = $(this);
		$tyi = confirm('Apakah anda yakin ingin menghapus data ini ?');
		if ($tyi) {
			$.ajax({
				url: data.data('link'),
				type: 'DELETE',
				dataType: 'json',
				data: {
					"_token": token
				},
				beforeSend: function() {
					isProcessing = true;
					$('#loadingtambah_rfs').fadeIn();
				},
				success: function(data) {
					$('#loadingtambah_rfs').hide();
					alert(data);
					isProcessing = false;
					$('#table-rfs').DataTable().ajax.reload();
				},
			});
		}
	});
</script>