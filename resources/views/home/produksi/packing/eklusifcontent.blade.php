<div class="row" id="load_eklusif">
	<div class="form-group col-md-4">
		<label class="control-label">Kemasan</label>
		<input type="text" class="form-control" id="kemasaneks" placeholder="Kemasan" readonly>

		<input type="hidden" id="idkemasaneks">
		<input type="hidden" id="idpackingeks">
		<input type="hidden" id="idstokemasaneks">
		<input type="hidden" id="idcincineks">
		<input type="hidden" id="idstikereks">
	</div>
	<div class="form-group col-md-4">
		<label class="control-label">Stock Kemasan</label>
		<input type="number" class="form-control" id="stockkemasaneks" placeholder="Stock Kemasan" readonly>
	</div>
	<div class="form-group col-md-4">
		<label class="control-label">Stock Cincin</label>
		<input type="number" class="form-control" id="stockcincineks" placeholder="Stock Cincin" readonly>
	</div>
	<div class="form-group col-md-12">
		<label class="control-label">Stock Stiker</label>
		<div class="row">
			<div class="col-md-6">
				*luar
				<input type="number" class="form-control" id="stockluarstieks" placeholder="Stock Stiker Luar" readonly>
			</div>
			<div class="col-md-6">
				*dalam
				<input type="number" class="form-control" id="stockdalamstieks" placeholder="Stock Stiker Dalam" readonly>
			</div>
		</div>
	</div>
	<div class="form-group col-md-12" id="batangeks">
		<label class="control-label">Batang Quality Control</label>
		<div class="row">
			<div class="col-md-4">
				*Robusto
				<input type="number" class="form-control" id="batangrbs_eks" placeholder="Jml Robusto Quality Control" readonly>
			</div>
			<div class="col-md-4">
				*Half Corona
				<input type="number" class="form-control" id="batanghlf_eks" placeholder="Jml Half Corona Quality Control" readonly>
			</div>
			<div class="col-md-4">
				*Maumere
				<input type="number" class="form-control" id="batangmmr_eks" placeholder="Jml Maumere Quality Control" readonly>
			</div>
		</div>
	</div>
	<div class="form-group col-md-12">
		<label class="control-label">Stock Awal</label>
		<input type="number" class="form-control" id="stockawaleks" placeholder="Stock Awal" readonly>
	</div>
	<div class="form-group col-md-5">
		<label class="control-label">Packing</label>
		<input type="number" class="form-control" id="jmlpackingeks" placeholder="JML Packing">
	</div>
	<div class="form-group col-md-2" align="center">
		<br>
		<label class="control-label">X</label>
	</div>
	<div class="form-group col-md-5">
		<label class="control-label">Isi/Kemasan</label>
		<input type="text" class="form-control" id="isikemasaneks" placeholder="Isi Kemasan" readonly>
		*Robusto: 10; *Maumere: 5; *Half Corona: 5;
	</div><div class="form-group col-md-6">
		<label class="control-label">Terpakai Stiker Luar</label>
		<input type="number" class="form-control" id="terpakailuareks" placeholder="Stiker Terpakai Luar">
	</div>
	<div class="form-group col-md-6">
		<label class="control-label">Terpakai Stiker Dalam</label>
		<input type="number" class="form-control" id="terpakaidalameks" placeholder="Stiker Terpakai Dalam">
	</div>
	<div class="form-group col-md-6">
		<label class="control-label">Terpakai Kemasan</label>
		<input type="number" class="form-control" id="terpakaikemasaneks" placeholder="Terpakai Kemasan">
	</div>
	<div class="form-group col-md-6">
		<label class="control-label">Terpakai Cincin</label>
		<input type="number" class="form-control" id="terpakaicincineks" placeholder="Terpakai Cincin">
	</div>
	<div class="form-group col-md-12">
		<label class="control-label">Keterangan</label>
		<textarea class="form-control" id="ket_eks" placeholder="Keterangan"></textarea>
	</div>
</div>