<script>
	$('.spinner_nekr').hide();
	$('#hiddenval').hide();
	$('#loadingdelete').hide();
	let token = $("meta[name='csrf-token']").attr("content");
	const produk1 = {!! json_encode($produk) !!};
	produk1.unshift({id_produk:'',produk:''});

	var table = $('#table-cincin').DataTable({
		"lengthMenu": [
		[3,5,10, 25, 50, 100, 150, 200, -1],
		[3,5,10, 25, 50, 100, 150, 200, "All"]
		],
		"order": [
		[0, 'desc']
		],
		processing: true,
		serverSide: true,
		ajax: '/cincin/json',
		columns: [
		{ data: 'DT_RowIndex', name: 'DT_RowIndex' },
		{ data: 'produk', name: 'produk' },
		{ data: 'stock_qty_cincin', name: 'stock_qty_cincin' },
		{	
			data: null,
			"bSortable": false,
			mRender: function(o) {
				return '<button type="button" class="btn btn-xs btn-rounded btn-warning update-cincin" data-link="/cincin/update/'+ o.id_stock_cincin +'" data-title="Update Cincin | ID Produk: '+ o.produk_id_produk +'" data-produk="'+ o.produk_id_produk +'" data-stock="'+ o.stock_qty_cincin +'"><i class="mdi mdi-dots-horizontal"></i></button><button type="button" id="delete-cincin" data-id="'+ o.id_stock_cincin +'" class="btn btn-danger btn-rounded btn-xs"><i class="fa fa-trash"></i></button>'; 
			}
		},
		],
		"displayLength": 10,
	});
	$('#table-cincin tbody').on('click', 'tr.group', function() {
		var currentOrder = table.order()[0];
		if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
			table.order([2, 'desc']).draw();
		} else {
			table.order([2, 'asc']).draw();
		}
	});
	$('#table-cincin tbody').on('click', 'tr.group', function() {
		var currentOrder = table.order()[0];
		if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
			table.order([2, 'desc']).draw();
		} else {
			table.order([2, 'asc']).draw();
		}
	});

	function Refresh() {
		table.search('').draw();
	}

	function lproduk(position,like = '') {
		let opt = '';
		for (var i = 0; i < produk1.length; i++) {
			opt += '<option value="'+ produk1[i].id_produk +'"';
			if (produk1[i].id_produk == like) { opt += 'selected'; }
			opt += '>'+ produk1[i].produk +'</option>';
		}
		$('#' + position).html(opt);
	}

	function Refresh() {
		$('#table-cincin').DataTable().ajax.reload();
	}

	function save_method() {
		let link = $('#form-cincin').attr('action');
		let produk = $('#produk_in').val();
		let stock = $('#stock').val();
		let harga_cincin = $('#harga_cincin').val();

		$.ajax({
			url: link,
			type: 'POST',
			dataType: 'json',
			data: {
				"_token": token,
				produk: produk,
				stock: stock,
			},
			beforeSend: function() {
				isProcessing = true;
				$('.spinner_nekr').fadeIn();
			},
			success: function(data) {
				$('.spinner_nekr').hide();
				isProcessing = false;
				alert(data);
				$('#table-cincin').DataTable().ajax.reload();
				$('#modal-cincin').modal('toggle');
			}
		});
		
	}

	$(document).on('click', '#tambah-cincin', function(event) {
		let clik = $(this);
		$('#hiddenval').hide();
		$('.modal-header #title').text('Tambah Cincin');
		$('#form-cincin').find('input').val('');
		$('#form-cincin').find('select').val('');
		$('#produk-select').removeAttr('disabled');
		$('#form-cincin').attr('action', clik.data('link'));
		lproduk('produk-select');
		$(document).on('change', '#produk-select', function(event) {
			let cal = $(this).val();
			$('#produk_in').val(cal);
		});
		$('#modal-cincin').modal('show');
	});

	$(document).on('click', '.update-cincin', function(event) {
		let clik = $(this);
		$('#hiddenval').hide();
		$('.modal-header #title').text(clik.data('title'));
		$('#form-cincin').find('input').val('');
		$('#form-cincin').find('select').val('');
		$('#form-cincin').attr('action', clik.data('link'));
		$('#produk_in').val(clik.data('produk'));
		lproduk('produk-select',clik.data('produk'));
		$('#produk-select').attr('disabled','disabled');
		$('#stock').val(clik.data('stock'));
		$('#modal-cincin').modal('show');
	});

	$(document).on('click', '#delete-cincin', function(event) {
		let asbe = confirm('Apakah anda yakin ingin menghapus data ini ?');
		if (asbe) {
			let id = $(this).data("id");
			$.ajax(
			{
				url: '/cincin/delete/' + id,
				type: 'DELETE',
				data: {
					"_token": token,
				},
				beforeSend: function() { 
					isProcessing = true;
					$('#loadingdelete').fadeIn();
				},
				success: function (data){
					$('#loadingdelete').hide();
					isProcessing = false;
					alert(data);
					$('#table-cincin').DataTable().ajax.reload();
				}
			});
		}
	});
</script>