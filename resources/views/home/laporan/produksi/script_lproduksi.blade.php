<script>
	$.extend(true, $.fn.datetimepicker.defaults, {
		icons: {
			time: 'far fa-clock',
			date: 'far fa-calendar',
			up: 'fas fa-arrow-up',
			down: 'fas fa-arrow-down',
			previous: 'fas fa-chevron-left',
			next: 'fas fa-chevron-right',
			today: 'fas fa-calendar-check',
			clear: 'far fa-trash-alt',
			close: 'far fa-times-circle'
		}
	});

	function format_number(number, prefix, thousand_separator, decimal_separator)
	{
		var 	thousand_separator = thousand_separator || ',',
		decimal_separator = decimal_separator || '.',
		regex		= new RegExp('[^' + decimal_separator + '\\d]', 'g'),
		number_string = number.toString().replace(regex, ''),
		split	  = number_string.split(decimal_separator),
		rest 	  = split[0].length % 3,
		result 	  = split[0].substr(0, rest),
		thousands = split[0].substr(rest).match(/\d{3}/g);

		if (thousands) {
			separator = rest ? thousand_separator : '';
			result += separator + thousands.join(thousand_separator);
		}
		result = split[1] != undefined ? result + decimal_separator + split[1] : result;
		return prefix == undefined ? result : (result ? prefix + result : '');
	};

	let token = $("meta[name='csrf-token']").attr("content");
	let boot = $("link[name='bootstrap']").attr("href");
	$('#print_html').hide();
	$('#loading').hide();
	$('#pro-pack').hide();
	$('#pro-qc').hide();
	$('#kem-pack').hide();

	const kemasan = {!! json_encode($kemasan) !!}
	kemasan.unshift({id_kemasan: '',nama_kemasan: 'Semua'});

	$("#tgl_awl").datetimepicker({
		format: 'YYYY-MM-DD',
	}).data("DateTimePicker").date(new Date());
	$("#tgl_akhr").datetimepicker({
		format: 'YYYY-MM-DD',
	}).data("DateTimePicker").date(new Date());

	getkemasan('kemasan');

	function getsub_produk(position,produk) {
		$.ajax({
			url: '/api/Report/getsubproduk/json',
			type: 'POST',
			dataType: 'json',
			data: {
				"_token": token,
				produk: produk,
			},
		})
		.done(function(data) {
			let opt = '<option value="">Semua</option>';
			for(let i = 0; i < data.length; i++) {
				opt += '<option value="'+ data[i].id_sub_produk +'" data-kemasan="'+ data[i].kemasan_id_kemasan +'">'+ data[i].sub_kode +' - '+ data[i].sub_produk +'</option>';
			}
			$('#' + position).html(opt);
		});
	}

	function getkemasan(posisi,like = '') {
		let opt = '';
		for (var i = 0; i < kemasan.length; i++) {
			opt += '<option value="'+ kemasan[i].id_kemasan +'" ';
			if (like == kemasan[i].id_kemasan) {
				opt += 'selected';
			}
			opt += '>'+ kemasan[i].nama_kemasan +'</option>';
		}
		$('#' + posisi).html(opt);
	}

	function print() {
		var divToPrint = document.getElementById('print');
		var htmlToPrint = '' +
		'<style type="text/css">' +
		'@page {' +
		'size: A4 landscape;' +
		// 'margin: 10%;' +
		// 'margin-top: 0.5cm;' +
		// 'margin-bottom: 0.5cm;' +
		// 'margin-left: 2cm;' +
		// 'margin-right: 1cm;' +
		'}' +
		'body {' + 
		'font-family:Arial, Helvetica, sans-serif;'+
		'font-size:10px;'+
		'color:#000000;'+
		'}' +
		'table th, table td {' +
		'border:1px solid #000;' +
		'padding;2em;' +
		'}' +
		'</style>';
		htmlToPrint += divToPrint.outerHTML;
		newWin = window.open("");
		// newWin.document.write("<h3 align='center'>Print Page</h3>");
		newWin.document.write(htmlToPrint);
		newWin.print();
		newWin.close();
	}

	$(document).on('change', '#produk', function(event) {
		$lek = $(this).val();
		getsub_produk('sub_produk',$lek);

	});

	$(document).on('change', '#berdasarkan', function(event) {
		$anen = $(this).val();
		if ($anen == 1) {
			$('#pro-pack').fadeIn();
			$('#pro-qc').hide();
			$('#kem-pack').fadeIn();
		} else if ($anen == 2 || $anen == 3) {
			$('#pro-pack').hide();
			$('#pro-qc').fadeIn();
			$('#kem-pack').hide();
		} else {
			$('#pro-pack').hide();
			$('#pro-qc').hide();
			$('#kem-pack').hide();
		}
	});

	$('#sub_produk_erd').selectpicker();

	function tblepacking(data,awal,akhir) {
		$html ='<div class="col-md-12" align="center">';
		$html +='<div class="table-responsive">';
		$html +='<h3 class="text-center">LAPORAN PACKING</h3>';
		$html +='<h3 class="text-center">PT. BOSS IMAGE NUSANTARA</h3>';
		$html +='<h3 class="text-center">'+ awal +' S.d. '+ akhir +'</h3>';
		$html +='<table width="100%" class="table table-bordered table-striped">';
		$html +='<thead align="center" class="table-dark">';
		$html +='<tr>';
		$html +='<th>Tanggal</th>';
		$html +='<th>Produk</th>';
		$html +='<th>Kemasan</th>';
		$html +='<th>Jumlah (per Produk)</th>';
		$html +='<th>Jumlah (Batang)</th>';
		$html +='<th>Keterangan</th>';
		$html +='</tr>';
		$html +='</thead>';
		$html +='<tbody>';

		$jml = 0;
		$btg = 0;
		for (var i = 0; i < data.length; i++) {
			$html +='<tr>';
			$html +='<td>'+ data[i].tanggal_packing +'</td>';
			$html +='<td>'+ data[i].sub_kode +' - '+ data[i].sub_produk +'</td>';
			$html +='<td>'+ data[i].nama_kemasan +'</td>';
			$html +='<td>'+ data[i].tambah_packing +'</td>';
			$html +='<td>'+ data[i].batangpack +'</td>';
			$html +='<td>'+ data[i].keterangan_packing +'</td>';
			$html +='</tr>';

			$jml += parseInt(data[i].tambah_packing);
			$btg += parseInt(data[i].batangpack);
		}
		$html +='</tbody>';
		$html +='<tfoot>';
		$html +='<tr>';
		$html +='<td colspan="3" align="center">Jumlah</td>';
		$html +='<td>'+ $jml +'</td>';
		$html +='<td>'+ $btg +'</td>';
		$html +='<td></td>';
		$html +='</tr>';
		$html +='</tfoot>';
		$html +='</table>';
		$html +='</div>';
		$html +='<div class="col-sm-4"></div>';
		$html +='<div class="col-sm-4 hert" align="center" style="margin-left:600px; width:100%;">';
		$html +='Jember, ........................ <br><br><br><br><br><br>';
		$html +='(________________________)';
		$html +='</div>';
		$html +='</div>';
		$('#table_html').html($html);
	}

	function tble_wrapp(data,awal,akhir,jenis) {
		$html = '<div class="col-md-12" align="center">';
		$html += '<div class="table-responsive">';
		$html += '<h3 class="text-center">LAPORAN STOCK Wrapping CERUTU</h3>';
		$html += '<h3 class="text-center">PT. BOSS IMAGE NUSANTARA</h3>';
		$html += '<h3 class="text-center">'+ awal +' S.d. '+ akhir +'</h3>';
		$html += '<table width="100%" class="table table-bordered table-striped">';
		$html += '<thead align="center" class="table-dark">';
		$html += '<tr>';
		$html += '<th rowspan="2">No.</th>';
		if (jenis == 1) {
			$html += '<th rowspan="2">Tanggal</th>';
		}
		$html += '<th rowspan="2">Produk</th>';
		$html += '<th rowspan="2">Ring</th>';
		$html += '<th rowspan="2">Stock S/d kemarin</th>';
		$html += '<th rowspan="2">Wrapping S/d Hi</th>';
		$html += '<th rowspan="2">Jumlah</th>';
		$html += '<th colspan="2">Seleksi</th>';
		$html += '<th rowspan="2">Stock Hi (bt)</th>';
		$html += '<th rowspan="2">Keterangan</th>';
		$html += '</tr>';
		$html += '<tr>';
		$html += '<th>Sesuai</th>';
		$html += '<th>Tidak Sesuai</th>';
		$html += '</tr>';
		$html += '</thead>';
		$html += '<tbody>';

		$no = 1;
		$acpt = 0;
		$muta = 0;
		$stok = 0;
		for (var i = 0; i < data.length; i++) {
			$pet = parseInt(data[i].stockawal) + parseInt(data[i].sisaprow);
			$html += '<tr>';
			if (jenis == 1) {
				$html += '<td>'+ data[i].tanggal_wrap +'</td>';
			}
			$html += '<td>'+ $no +'</td>';
			$html += '<td>'+ data[i].produk +'</td>';
			$html += '<td>'+data[i].ring+'</td>';
			$html += '<td>'+ format_number(data[i].stockawal, '') +'</td>';
			$html += '<td>'+ format_number(data[i].sisaprow, '') +'</td>';
			$html += '<td>'+ format_number($pet, '') +'</td>';
			$html += '<td>'+ data[i].tambah_wrap +'</td>';
			$html += '<td>'+ data[i].batangreject +'</td>';
			$html += '<td>'+ data[i].hasil_akhirw +'</td>';
			$html += '<td>'+ data[i].keterangan_wrap +'</td>';
			$html += '</tr>';
			$no++;


			$acpt += parseInt(data[i].tambah_wrap);
			$muta += parseInt(data[i].batangreject);
			$stok += parseInt(data[i].hasil_akhirw);
		}

		$html += '</tbody>';
		$html += '<tfoot>';
		$html += '<tr>';
		$html += '<td colspan="2" align="center">Total</td>';
		if (jenis == 1) {
			$html += '<td></td>';
		}
		$html += '<td></td>';
		$html += '<td></td>';
		$html += '<td></td>';
		$html += '<td></td>';
		$html += '<td>'+ format_number($acpt, '') +'</td>';
		$html += '<td>'+ format_number($muta, '') +'</td>';
		$html += '<td>'+ format_number($stok, '') +'</td>';
		$html += '<td></td>';
		$html += '</tr>';
		$html += '</tfoot>';
		$html += '</table>';
		$html += '</div>';
		$html += '<div class="col-sm-4"></div>';
		$html += '<div class="col-sm-4 hert" align="center" style="margin-left:600px; width:100%;">';
		$html += 'Jember, ........................ <br><br><br><br><br><br>';
		$html += '(________________________)';
		$html += '</div>';
		$html += '</div>';

		$('#table_html').html($html);
	}

	function table_qc(data,awal,akhir,jenis) {
		
		$html = '<div class="table-responsive">';
		$html += '<h3 class="text-center">LAPORAN STOCK QC CERUTU</h3>';
		$html += '<h3 class="text-center">PT. BOSS IMAGE NUSANTARA</h3>';
		$html += '<h3 class="text-center">'+ awal +' S.d. '+ akhir +'</h3>';
		$html += '<table width="100%" class="table table-bordered table-striped">';
		$html += '<thead>';
		$html += '<tr>';
		$html += '<th rowspan="2">No.</th>';
		if (jenis == 1) {
			$html += '<th rowspan="2">Tanggal</th>';
		}
		$html += '<th rowspan="2">Merk</th>';
		$html += '<th rowspan="2">Stock S/d. Kemarin</th>';
		$html += '<th colspan="2">Seleksi</th>';
		$html += '<th rowspan="2">Stock Hi(bt)</th>';
		$html += '<th rowspan="2">Ketangan</th>';
		$html += '</tr>';
		$html += '<tr>';
		$html += '<th>Sesuai</th>';
		$html += '<th>Tidak Sesuai</th>';
		$html += '</tr>';
		$html += '</thead>';
		$html += '<tbody>';

		$no = 1;
		$acpt = 0;
		$muta = 0;
		$stok = 0;
		for (var i = 0; i < data.length; i++) {
			$html += '<tr>';
			$html += '<td>'+ $no +'</td>';
			if (jenis == 1) {
				$html += '<td>'+ data[i].tanggal_qc +'</td>';
			}
			$html += '<td>'+ data[i].produk +'</td>';
			$html += '<td>'+ data[i].stock_awl +'</td>';
			$html += '<td>'+ data[i].accept +'</td>';
			$html += '<td>'+ data[i].mutasi_batang +'</td>';
			$html += '<td>'+ data[i].hasil_qc +'</td>';
			$html += '<td>'+ data[i].keterangan_qc +'</td>';
			$html += '</tr>';

			$acpt += parseInt(data[i].accept);
			$muta += parseInt(data[i].mutasi_batang);
			$stok += parseInt(data[i].hasil_qc);

			$no++;
		}
		$html += '</tbody>';
		$html += '<tfoot>';
		$html += '<tr>';
		$html += '<td align="center">Total</td>';
		if (jenis == 1) {
			$html += '<td></td>';
		}
		$html += '<td></td>';
		$html += '<td></td>';
		$html += '<td></td>';
		$html += '<td>'+ $acpt +'</td>';
		$html += '<td>'+ $muta +'</td>';
		$html += '<td>'+ $stok +'</td>';
		$html += '<td></td>';
		$html += '</tr>';
		$html += '</table>';
		$html += '</div>';
		$('#table_html').html($html);
	}

	const connect = {
		0: '',
		1: '/api/Report1/laporanpacking/json',
		2: '/api/Report1/laporanwrapping/json',
		3: '/api/Report1/laporanquality/json',
	};

	function render() {
		let bahan = $('#berdasarkan').val();
		let tgl_awl = $('#tgl_awl').val();
		let tgl_akhr = $('#tgl_akhr').val();
		let produk = $('#produk').val();
		let sub_produk = $('#sub_produk').val();
		let sub_produk_erd = $('#sub_produk_erd').val();
		let kemasan = $('#kemasan').val();
		let men = connect[bahan];
		let jnslap = $('#jnslap').val();
		$.ajax({
			url: men,
			type: 'POST',
			dataType: 'json',
			data: {
				"_token": token,
				bahan: bahan,
				tgl_awl: tgl_awl,
				tgl_akhr: tgl_akhr,
				produk: produk,
				sub_produk: sub_produk,
				produk_erd: sub_produk_erd,
				kemasan: kemasan,
				jnslap:jnslap
			},
			beforeSend: function() {
				isProcessing = true;
				$('#loading').fadeIn();
			},
			success: function(data) {
				if (bahan == 1) {
					tblepacking(data,tgl_awl,tgl_akhr);
				} else if (bahan == 2) {
					tble_wrapp(data,tgl_awl,tgl_akhr,jnslap);
				} else if(bahan == 3) {
					table_qc(data,tgl_awl,tgl_akhr,jnslap);
				} else {
					alert('Pilih salah satulah!!');
				}
				$('#loading').hide();
				isProcessing = false;
				$('#print_html').fadeIn();
			}
		});
	}
</script>