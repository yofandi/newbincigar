<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;
use DataTables;

class Reproduksi extends Controller
{
	public function __construct()
	{
		$this->middleware('auth');
	}

	public function getreproduksiall()
	{
		$query = DB::table('repro_check')
		->select(
			'repro_check.*',
			'item_repro.filler1_value',
			'item_repro.omblad_value',
			'item_repro.dekblad_value',
			'item_repro.filler2_value',
			'produk.produk',
			'jenis.id_jenis',
			'jenis.jenis',
		)
		->join('item_repro','repro_check.id','=','item_repro.repro_check_id')
		->join('produk','item_repro.produk_id','=','produk.id_produk')
		->join('jenis_has_produk','produk.id_produk','=','jenis_has_produk.produk_id_produk')
		->join('jenis','jenis_has_produk.jenis_id_jenis','=','jenis.id_jenis')
		->get();
		return Datatables::of($query)
		->addIndexColumn()
		->make(true);
	}

	public function viewreproduksi()
	{
		$data['produk'] = DB::table('produk')->get();
		return view('home.bahan_baku.reproduksi.reproduksi',$data);	
	}
}
