<script>
	$('.spinner_nekr').hide();
	$('#loadingdelete').hide();
	let token = $("meta[name='csrf-token']").attr("content");
	$.extend(true, $.fn.datetimepicker.defaults, {
		icons: {
			time: 'far fa-clock',
			date: 'far fa-calendar',
			up: 'fas fa-arrow-up',
			down: 'fas fa-arrow-down',
			previous: 'fas fa-chevron-left',
			next: 'fas fa-chevron-right',
			today: 'fas fa-calendar-check',
			clear: 'far fa-trash-alt',
			close: 'far fa-times-circle'
		}
	});

	const produk = {!! json_encode($produk) !!}
	produk.unshift({id_produk: '',produk: ''});
	
	function format_number(number, prefix, thousand_separator, decimal_separator)
	{
		var 	thousand_separator = thousand_separator || ',',
		decimal_separator = decimal_separator || '.',
		regex		= new RegExp('[^' + decimal_separator + '\\d]', 'g'),
		number_string = number.toString().replace(regex, ''),
		split	  = number_string.split(decimal_separator),
		rest 	  = split[0].length % 3,
		result 	  = split[0].substr(0, rest),
		thousands = split[0].substr(rest).match(/\d{3}/g);

		if (thousands) {
			separator = rest ? thousand_separator : '';
			result += separator + thousands.join(thousand_separator);
		}
		result = split[1] != undefined ? result + decimal_separator + split[1] : result;
		return prefix == undefined ? result : (result ? prefix + result : '');
	};

	var table = $('#table_cincin').DataTable({
		"lengthMenu": [
		[3,5,10, 25, 50, 100, 150, 200, -1],
		[3,5,10, 25, 50, 100, 150, 200, "All"]
		],
		"order": [
		[0, 'desc']
		],
		processing: true,
		serverSide: true,
		ajax: '/data_cincin/json',
		columns: [
		{ data: 'DT_RowIndex', name: 'DT_RowIndex' },
		{ data: 'tanggal_cincin', name: 'tanggal_cincin' },
		{ data: 'produk', name: 'produk' },
		{ data: 'masuk', name: 'masuk' },
		{ data: 'terpakai', name: 'terpakai' },
		{ data: 'afkir', name: 'afkir' },
		{ data: 'sisa', name: 'sisa' },
		{ data: 'ket_cin', name: 'ket_cin' },
		{
			data: null,
			"bSortable": false,
			mRender: function (o) { 
				return '<button type="button" class="btn btn-info btn-rounded btn-xs" id="update-cincin" data-title="Update Stock Cincin | ID: '+ o.id +'" data-link="/data_cincin/upp/'+ o.id +'/'+ o.masuk +'/'+ o.terpakai +'/'+ o.afkir +'" data-id="'+ o.id +'" data-produk="'+ o.stock_cincin_produk_id_produk +'" data-hargaid="'+ o.harga_cincin_id +'"  data-tanggal_cincin="'+ o.tanggal_cincin +'" data-awlstok="'+ o.awlstok +'" data-masuk="'+ o.masuk +'" data-terpakai="'+ o.terpakai +'" data-afkir="'+ o.afkir +'" data-sisa="'+ o.sisa +'" data-ket_cin="'+ o.ket_cin +'"><i class="mdi mdi-dots-horizontal"></i></button><button type="button" class="btn btn-danger btn-rounded btn-xs" id="delete-cincin" data-link="/data_cincin/del/'+ o.id +'/'+ o.masuk +'/'+ o.terpakai +'/'+ o.afkir +'/'+ o.awlstok +'" data-id="'+ o.id +'" data-produk="'+ o.stock_cincin_produk_id_produk +'"><i class="fa fa-trash"></i></button>'; }
			},
			],
			"displayLength": 10,
		});
	$('#table_cincin tbody').on('click', 'tr.group', function() {
		var currentOrder = table.order()[0];
		if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
			table.order([2, 'desc']).draw();
		} else {
			table.order([2, 'asc']).draw();
		}
	});
	$('#table_cincin tbody').on('click', 'tr.group', function() {
		var currentOrder = table.order()[0];
		if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
			table.order([2, 'desc']).draw();
		} else {
			table.order([2, 'asc']).draw();
		}
	});

	function cekvalcincin($status,based,id = '') {
		$.ajax({
			url: '/api/data_cincin/setharga',
			type: 'POST',
			dataType: 'json',
			data: {
				"_token": token,
				val: $status,
				produk: based,
				id: id,
			},
		})
		.done(function(data) {
			$('#harga_cincin').val(format_number(data.harga, ''));
		});
	}

	function getstock($status,position,based) {
		$.ajax({
			url: '/data_cincin/getstock',
			type: 'POST',
			dataType: 'json',
			data: {
				"_token": token,
				id_produk: based
			},
		})
		.done(function(data) {
			$('#' + position).val(data['value']);
		});
	}

	function lproduk(position,like = "") {
		let opt = '';
		for (var i =  0; i < produk.length; i++) {
			opt += '<option value="'+ produk[i].id_produk +'"';
			if (produk[i].id_produk == like) { opt += 'selected'; }
			opt +='>'+ produk[i].produk +'</option>';
		}
		$('#' + position).html(opt);
	}

	function Refresh() {
		$('#table_cincin').DataTable().ajax.reload();
	}

	function save_method() {
		let link = $('#form-cincin').attr('action');
		let tanggal_cincin = $('#tanggal_cincin').val();
		let produk_in = $('#produk_in').val();
		let stock = $('#stock').val();
		let masuk = $('#masuk').val();
		let terpakaicincin = $('#terpakaicincin').val();
		let afkir = $('#afkir').val();
		let ket_cin = $('#ket_cin').val();
		$.ajax({
			url: link,
			type: 'POST',
			dataType: 'json',
			data: {
				"_token": token,
				tanggal_cincin: tanggal_cincin,
				produk: produk_in,
				stock: stock,
				masuk: masuk,
				terpakaicincin: terpakaicincin,
				afkir: afkir,
				ket_cin: ket_cin,
			},
			beforeSend: function() {
				isProcessing = true;
				$('.spinner_nekr').fadeIn();
			},
			success: function(data) {
				$('.spinner_nekr').hide();
				isProcessing = false;
				alert(data);
				$('#table_cincin').DataTable().ajax.reload();
				$('#modal-cincin').modal('toggle');
			}
		});
	}

	$(document).on('click', '#tambah-cincin', function(event) {
		let clik = $(this);
		$('.modal-header #title').text('Tambah Stock Cincin');
		$('#form-cincin').attr('action', clik.data('link'));
		$('#form-cincin').find('input[type=text]').val('');
		$('#form-cincin').find('input[type=number]').val(0);
		$('#form-cincin').find('select').val('');
		$('#form-cincin').find('textarea').val('');
		$('#produk').removeAttr('disabled');
		lproduk('produk');
		$("#tanggal_cincin").datetimepicker({
			format: 'YYYY-MM-DD HH:mm:ss',
		}).data("DateTimePicker").date(new Date());

		$('#produk').change(function(event) {
			let bal = $(this).val();
			$('#produk_in').val(bal);
			getstock('insert','stock',bal);
			cekvalcincin('0',bal);
		});		

		$('#modal-cincin').modal('show');
	});

	$(document).on('click', '#update-cincin', function(event) {
		let click = $(this);
		let id = $(this).data('id');
		let link = $(this).data('link');
		$('.modal-header #title').text(click.data('title'));
		$('#form-cincin').attr('action', link);
		$('#form-cincin').find('input[type=text]').val('');
		$('#form-cincin').find('input[type=number]').val(0);
		$('#form-cincin').find('select').val('');
		$('#form-cincin').find('textarea').val('');
		$('#produk').attr('disabled','disabled');

		lproduk('produk', $(this).data('produk'));
		cekvalcincin('1',$(this).data('produk'),$(this).data('hargaid'));
		$("#tanggal_cincin").datetimepicker({
			format: 'YYYY-MM-DD HH:mm:ss',
		}).data("DateTimePicker").date($(this).data('tanggal_cincin'));

		$('#produk_in').val(click.data('produk'));
		$('#stock').val(click.data('awlstok'));
		$('#masuk').val(click.data('masuk'));
		$('#terpakaicincin').val(click.data('terpakai'));
		$('#afkir').val(click.data('afkir'));
		$('#ket_cin').val(click.data('ket_cin'));

		$('#modal-cincin').modal('show');	
	});

	$(document).on('click', '#delete-cincin', function(event) {
		let dbhe = confirm('Apakah anda yakin ingin menghapus data ini ?');
		if (dbhe) {
			let id = $(this).data("id");
			let link = $(this).data("link");
			let token = $("meta[name='csrf-token']").attr("content");
			let produk_in = $(this).data('produk');
			$.ajax(
			{
				url: link,
				type: 'DELETE',
				data: {
					"_token": token,
					produk: produk_in,
				},
				beforeSend: function() { 
					isProcessing = true;
					$('#loadingdelete').fadeIn();
				},
				success: function (data){
					$('#loadingdelete').hide();
					isProcessing = false;
					alert(data);
					$('#table_cincin').DataTable().ajax.reload();
				}
			});
		}
	});
</script>