<div class="modal fade" id="modal-fumigasi" tabindex="-1" role="dialog" aria-labelledby="title-tembakau" aria-hidden="true" style="display: none;">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="card">
				<div class="card-header">
					<div class="modal-header">
						<h4 class="modal-title" id="title"></h4>
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					</div>
				</div>
				<div class="card-body">
					<form id="form-fumigasi" method="POST">
						<div class="modal-body row">
							<div class="form-group col-md-12 fa-3x spinner_nekrfumi" align="center">
								<i class="fas fa-spinner fa-spin"></i>
							</div>
							<div class="form-group col-md-12">
								<label class="control-label">Tanggal</label>
								<input type="text" class="form-control date_sam" id="tanggal_fumigasi">
							</div>
							<div class="form-group col-md-6">
								<label class="control-label">Jenis</label>
								<select class="form-control" id="jenisfumi"></select>
								<input type="hidden" id="jenis_infumi">
							</div>
							<div class="form-group col-md-6">
								<label class="control-label">Produk</label>
								<select class="form-control" id="produkfumi"></select>
								<input type="hidden" id="produk_infumi">
							</div>
							<div class="form-group col-md-12" id="stock_showitfumi">
								<button type="button" onclick="seacrh_datafumi()" class="btn btn-primary btn-xs btn-block">Search</button>
								<input type="hidden" id="iddrying2fumi">
								<input type="hidden" id="idfumigasi">
								<input type="hidden" id="idjnprofumi">
							</div>
							<div class="form-group col-md-12">
								<label class="control-label">Batang Drying2</label>
								<input type="number" class="form-control" id="batangdry2fumi" placeholder="Jml Batang Drying2" readonly>
							</div>
							<div class="form-group col-md-12">
								<label class="control-label">Batang Cerutu Yang Dihasilkan</label>
								<input type="number" class="form-control" id="batangjmlfumi" placeholder="Jml Batang">
							</div>
							<div class="form-group col-md-12">
								<label class="control-label">Lama Proses</label>
								<input type="text" class="form-control" id="durasifumi" placeholder="Durasi">
							</div>
							<div class="form-group col-md-12">
								<label class="control-label">Keterangan</label>
								<textarea class="form-control" id="ketfumi" placeholder="Keterangan"></textarea>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" onclick="save_methodfumi()" class="btn btn-primary waves-effect text-left">Save</button>
							<button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Close</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>