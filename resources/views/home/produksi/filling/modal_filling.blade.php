<div class="modal fade" id="modal-filling" tabindex="-1" role="dialog" aria-labelledby="title-tembakau" aria-hidden="true" style="display: none;">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="card">
				<div class="card-header">
					<div class="modal-header">
						<h4 class="modal-title" id="title"></h4>
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					</div>
				</div>
				<div class="card-body">
					<form id="form-filling" method="POST">
						<div class="modal-body row">
							<div class="form-group col-md-12 fa-3x spinner_nekrfill" align="center">
								<i class="fas fa-spinner fa-spin"></i>
							</div>
							<div class="form-group col-md-12">
								<label class="control-label">Tanggal</label>
								<input type="text" class="form-control date_sam" id="tanggal_filling">
							</div>
							<div class="form-group col-md-6">
								<label class="control-label">Jenis</label>
								<select class="form-control" id="jenis"></select>
								<input type="hidden" id="jenis_in">
							</div>
							<div class="form-group col-md-6">
								<label class="control-label">Produk</label>
								<select class="form-control" id="produk"></select>
								<input type="hidden" id="produk_in">
							</div>
							<div class="form-group col-md-12" id="stock_showitfill">
								<button type="button" onclick="seacrh_datafill()" class="btn btn-primary btn-xs btn-block">Search</button>
								<input type="hidden" id="idprobaku">
								<input type="hidden" id="idfilling">
								<input type="hidden" id="idjnpro">
							</div>
							<div class="form-group col-md-6">
								<label class="control-label">Stock Produksi (Saat ini/Awal :*Gram)</label>
								<input type="number" class="form-control" step="any" id="stockpro" value="0" placeholder="Stock (Saat ini/Awal)" readonly>
							</div>
							<div class="form-group col-md-6">
								<label class="control-label">Stock Weisf (*Gram)</label>
								<input type="number" class="form-control" step="any" id="stockweisf" value="0" placeholder="Stock Weisf" readonly>
							</div>
							<div class="form-group col-md-12">
								<label class="control-label">Terpakai Produksi (*Gram)</label>
								<input type="number" class="form-control" step="any" id="terpakaipro" placeholder="Stock Produksi Terpakai">
							</div>
							<div class="form-group col-md-6">
								<label class="control-label">Masuk Weis (*Gram)</label>
								<input type="number" class="form-control" step="any" id="masukweisf" placeholder="Stock Masuk Weisf">
							</div>
							<div class="form-group col-md-6">
								<label class="control-label">Terpakai Weisf (*Gram)</label>
								<input type="number" class="form-control" step="any" id="terpakaiweisf" placeholder="Stock Produksi Weisf">
							</div>
							<div class="form-group col-md-12">
								<label class="control-label">Yang Dihasilkan (*Gram)</label>
								<input type="number" class="form-control" id="batangjml" placeholder="Jml Gram">
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" onclick="save_methodfilling()" class="btn btn-primary waves-effect text-left">Save</button>
							<button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Close</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>