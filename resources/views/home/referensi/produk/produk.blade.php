@extends('../../layout')

@section('title')
<title>BIN - ERP : Produk</title>
<link type="text/css" rel="stylesheet" href="{{ asset('node_modules/jsgrid/jsgrid.min.css') }}" />
<link type="text/css" rel="stylesheet" href="{{ asset('node_modules/jsgrid/jsgrid-theme.min.css') }}" />
<style type="text/css">
	.hide
	{
		display:none;
	}
</style>
@endsection

@section('content')
<div class="row page-titles">
	<div class="col-md-5 align-self-center">
		<h3 class="text-themecolor">Produk</h3>
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a href="javascript:void(0)">Referensi</a></li>
			<li class="breadcrumb-item active">Produk</li>
		</ol>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-body">
				<div class="row">
					<div class="col-md-8"></div>
					<div class="col-md-2 offset-md-2">
						<button type="button" onclick="Refresh()" class="btn btn-xs btn-icons btn-secondary">Refresh</button>
					</div>
				</div>
				<h4 class="card-title">Produk</h4>
				<div id="grid_produk"></div>
			</div>
		</div>
	</div>
	<div class="col-md-12">
		<div class="card">
			<div class="card-body">
				<div class="row">
					<div class="col-md-2">
						<h4 class="card-title">Sub Produk</h4>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<input type="text" class="form-control form-control-sm" id="name-produk-search" readonly>
						</div>
					</div>
					<div class="col-md-4 offset-md-2" align="right">
						<div class="form-group">
							<button type="button" id="tambah-sub" class="btn btn-success btn-xs btn-rounded" data-link="{{ route('subproduk.add') }}">Tambah</button>
							<button type="button" onclick="Refresh_sub()" class="btn btn-secondary btn-xs btn-rounded">Refresh</button>
						</div>
					</div>
					<div class="col-md-12" align="center" id="loadingdelete">
						<h5>Data tengah diproses. Mohon tunggu sejenak!</h5>
					</div>
					<div class="col-md-12">
						<div class="table-responsive">
							<table id="table-sub" class="display nowrap table table-hover table-striped table-bordered">
								<thead>
									<th>No.</th>
									<th>Kode Produk</th>
									<th>Kode Sub</th>
									<th>Nama Sub</th>
									<th>Ring</th>
									<th>Kemasan</th>
									<th>Isi</th>
									<th>HJE</th>
									<th>Tarif</th>
									<th>Berat (*gram)</th>
									<th>Aksi</th>
								</thead>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="modal-produk" tabindex="-1" role="dialog" aria-labelledby="title" aria-hidden="true" style="display: none;">
	<div class="modal-dialog modal-xl">
		<div class="modal-content">
			<div class="card">
				<div class="card-header">
					<div class="modal-header">
						<h4 class="modal-title" id="title"></h4>
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					</div>
				</div>
				<div class="card-body">
					<form id="form-produk" method="POST">
						<div class="modal-body row">
							<div class="form-group col-md-12 fa-3x spinner_nekr" align="center">
								<i class="fas fa-spinner fa-spin"></i>
							</div>
							<div class="form-group col-md-12">
								<label class="control-label">Produk</label>
								<input type="text" class="form-control" id="nama-produk" placeholder="Nama Produk" readonly>
								<input type="hidden" id="idproduk-hidden">
							</div>
							<div class="form-group col-md-6">
								<label class="control-label">Kode Sub</label>
								<input type="text" class="form-control hjet" id="kode-sub" placeholder="Kode Sub">
							</div>
							<div class="form-group col-md-6">
								<label class="control-label">Sub Produk</label>
								<input type="text" class="form-control hjet" id="nama-sub" placeholder="Sub Produk">
							</div>
							<div class="form-group col-md-4">
								<label class="control-label">Harga Jual Eceran (HJE)</label>
								<input type="text" class="form-control hjet" id="sub-hje" placeholder="HJE..">
							</div>
							<div class="form-group col-md-4">
								<label class="control-label">Isi</label>
								<input type="text" class="form-control hjet" id="sub-isi" placeholder="Isi...">
							</div>
							<div class="form-group col-md-4">
								<label class="control-label">Ring</label>
								<input type="number" class="form-control hjet" id="ring" placeholder="Ring...">
							</div>
							<div class="form-group col-md-6">
								<label class="control-label">Berat Cerutu (:Gram)</label>
								<input type="number" class="form-control" id="berat" placeholder="....Gram">
							</div>
							<div class="form-group col-md-6">
								<label class="control-label">Tarif</label>
								<input type="text" class="form-control hjet" id="sub-tarif" placeholder="Tarif">
							</div>
							<div class="form-group col-md-6">
								<label class="control-label">Merek Cukai</label>
								<select class="form-control" id="merek-cukai"></select>
							</div>
							<div class="form-group col-md-6">
								<label class="control-label">Kemasan</label>
								<select class="form-control" id="sub-kemasan"></select>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" onclick="save_method()" class="btn btn-primary waves-effect text-left">Save</button>
							<button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Close</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('javascript')
<script type="text/javascript" src="{{ asset('node_modules/jsgrid/jsgrid.min.js') }}"></script>
<script src="{{ asset('node_modules/datatables/datatables.min.js') }}"></script>
@include('home.referensi.produk.script_produk')
@endsection