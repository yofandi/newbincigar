<script>
	$('.spinner_nekr').hide();
	$('#hiddenval').hide();
	$('#loadingdelete').hide();
	let token = $("meta[name='csrf-token']").attr("content");
	const produk1 = {!! json_encode($produk) !!};
	produk1.unshift({id_produk:'',produk:'Pilih Produk'});
	const kemasan = {!! json_encode($kemasan) !!};
	kemasan.unshift({id_kemasan:'',nama_kemasan:'Pilih Kemasan'});

	var table = $('#table-kemasan').DataTable({
		"lengthMenu": [
		[3,5,10, 25, 50, 100, 150, 200, -1],
		[3,5,10, 25, 50, 100, 150, 200, "All"]
		],
		"order": [
		[0, 'desc']
		],
		processing: true,
		serverSide: true,
		ajax: '/stockkemasan/json',
		columns: [
		{ data: 'DT_RowIndex', name: 'DT_RowIndex' },
		{ data: 'produk', name: 'produk' },
		{ data: 'kodeproduk', name: 'kodeproduk' },
		{ data: 'nama_kemasan', name: 'nama_kemasan' },
		{ data: 'stock_kemasan', name: 'stock_kemasan' },
		{ 
			data: null,
			mRender: function function_name(a) {
				return format_number(a.harga_satuan, '');
			}
		},
		{	
			data: null,
			"bSortable": false,
			mRender: function(o) {
				return '<button type="button" class="btn btn-xs btn-rounded btn-warning update-kemasan" data-title="Update Stock Kemasan | ID Produk: '+ o.id_produk +'" data-link="/stockkemasan/upp/'+ o.id_stock_kemasan +'" data-produk="'+ o.id_produk +'" data-subproduk="'+ o.id_sub_produk +'" data-kemasan="'+ o.id_kemasan +'" data-nama_kemasan="'+ o.nama_kemasan +'" data-stock="'+ o.stock_kemasan +'"><i class="mdi mdi-dots-horizontal"></i></button><button type="button" class="btn btn-danger btn-rounded btn-xs" id="delete-kemasan" data-id="'+ o.id_stock_kemasan +'"><i class="fa fa-trash"></i></button>'; 
			}
		},
		],
		"displayLength": 10,
	});
	$('#table-kemasan tbody').on('click', 'tr.group', function() {
		var currentOrder = table.order()[0];
		if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
			table.order([2, 'desc']).draw();
		} else {
			table.order([2, 'asc']).draw();
		}
	});
	$('#table-kemasan tbody').on('click', 'tr.group', function() {
		var currentOrder = table.order()[0];
		if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
			table.order([2, 'desc']).draw();
		} else {
			table.order([2, 'asc']).draw();
		}
	});

	// jsGrid.setDefaults({
	// 	tableClass: "jsgrid-table table table-striped table-hover"
	// });
	// jsGrid.setDefaults("text", {
	// 	_createTextBox: function() {
	// 		return $("<input>").attr("type", "text").attr("class", "form-control input-sm")
	// 	}
	// });
	// jsGrid.setDefaults("number", {
	// 	_createTextBox: function() {
	// 		return $("<input>").attr("type", "number").attr("class", "form-control input-sm")
	// 	}
	// });
	// jsGrid.setDefaults("textarea", {
	// 	_createTextBox: function() {
	// 		return $("<input>").attr("type", "textarea").attr("class", "form-control")
	// 	}
	// });
	// jsGrid.setDefaults("control", {
	// 	_createGridButton: function(cls, tooltip, clickHandler) {
	// 		var grid = this._grid;
	// 		return $("<button>").addClass(this.buttonClass).addClass(cls).attr({
	// 			type: "button",
	// 			title: tooltip
	// 		}).on("click", function(e) {
	// 			clickHandler(grid, e)
	// 		})
	// 	}
	// });
	// jsGrid.setDefaults("select", {
	// 	_createSelect: function() {
	// 		var $result = $("<select>").attr("class", "form-control input-sm"),
	// 		valueField = this.valueField,
	// 		textField = this.textField,
	// 		selectedIndex = this.selectedIndex;
	// 		return $.each(this.items, function(index, item) {
	// 			var value = valueField ? item[valueField] : index,
	// 			text = textField ? item[textField] : item,
	// 			$option = $("<option>").attr("value", value).text(text).appendTo($result);
	// 			$option.prop("selected", selectedIndex === index)
	// 		}), $result
	// 	}
	// });
	// $('#grid_kemasan').jsGrid({
	// 	height: "500px",
	// 	width: "100%",
	// 	filtering: true,
	// 	filterable:true,
	// 	editing: true,
	// 	inserting: true,
	// 	sorting: true,
	// 	paging: true,
	// 	autoload: true,
	// 	pageSize: 15,
	// 	pageButtonCount: 5,
	// 	deleteConfirm: "Apakah anda yakin ingin menghapus data ini ?",
	// 	controller: {
	// 		loadData: function(filter){
	// 			return $.ajax({
	// 				type: "GET",
	// 				dataType: 'json',
	// 				url: "/kemasan/json1",
	// 				data: filter
	// 			});
	// 		},
	// 		insertItem: function(item){
	// 			item._token = $('meta[name="csrf-token"]').attr('content');
	// 			return $.ajax({
	// 				type: "POST",
	// 				url: "/kemasan/post",
	// 				data:item
	// 			});
	// 		},
	// 		updateItem: function(item){
	// 			item._token = $('meta[name="csrf-token"]').attr('content');
	// 			return $.ajax({
	// 				type: "PUT",
	// 				url: "/kemasan/upp",
	// 				data: item
	// 			});
	// 		},
	// 		deleteItem: function(item){
	// 			item._token = $('meta[name="csrf-token"]').attr('content');
	// 			return $.ajax({
	// 				type: "DELETE",
	// 				url: "/kemasan/delete",
	// 				data: item
	// 			});
	// 		},
	// 	},

	// 	fields: [
	// 	{
	// 		name: "id_kemasan",
	// 		title: "ID",
	// 		type: "hidden",
	// 		width: 150
	// 	},
	// 	{
	// 		name: "nama_kemasan",
	// 		title: "Kemasan",
	// 		type: "text", 
	// 		width: 150
	// 	},
	// 	{
	// 		type: "control",
	// 		width: 100,
	// 		itemTemplate: function function_name(value, item) {
	// 			var $result = jsGrid.fields.control.prototype.itemTemplate.apply(this, arguments);
	// 			var $customButton = $('<button type="button" class="btn btn-success btn-xs btn-rounded btn-icons"><i class="fa fa-check"></i></button>')
	// 			.click(function(e) {
	// 				$('#cari-kem').val(item.nama_kemasan);
	// 				table.columns( 2 ).search(item.nama_kemasan).draw();
	// 				e.stopPropagation();
	// 			});

	// 			return $result.add($customButton);
	// 		}
	// 	}
	// 	]
	// });

	// function Refresh_jsgrid() {
	// 	$("#grid_kemasan").jsGrid("loadData");
	// }

	function Refresh_datatables() {
		$('#cari-kem').val('');
		table.search('').draw();
	}

	function lproduk(position,like = '') {
		let opt = '';
		for (var i = 0; i < produk1.length; i++) {
			opt += '<option value="'+ produk1[i].id_produk +'"';
			if (produk1[i].id_produk == like) { opt += 'selected'; }
			opt += '>'+ produk1[i].produk +'</option>';
		}
		$('#' + position).html(opt);
	}

	function lsubpro(position,base,like = '') {
		$.ajax({
			url: '/cukai/subproduk/json',
			type: 'POST',
			dataType: 'json',
			data: {
				"_token": token,
				produk: base
			},
		})
		.done(function(data) {
			let opt = '<option value="">Pilih Sub</option>';
			for (var i = 0; i < data.length; i++) {
				opt += '<option value="'+ data[i].id_sub_produk +'"';
				if (data[i].id_sub_produk == like) { opt += 'selected'; }
				opt += ' data-idkemasan="'+ data[i].id_kemasan +'" data-nama_kemasan="'+ data[i].nama_kemasan +'">'+ data[i].sub_kode +' - '+ data[i].sub_produk +'</option>';
			}
			$('#' + position).html(opt);		
		});
	}

	function save_method() {
		let link = $('#form-stockkem').attr('action');
		let produk_in = $('#produk_in').val();
		let subproduk = $('#sub_produk_in').val();
		let kemasan_in = $('#kemasan_in').val();
		let stock = $('#stock').val();
		let harga_satuan = $('#harga_satuan').val();
		$.ajax({
			url: link,
			type: 'POST',
			dataType: 'json',
			data: {
				"_token": token,
				produk: produk_in,
				subproduk: subproduk,
				id_kemasan: kemasan_in,
				stock_kemasan1: stock,
			},
			beforeSend: function() {
				isProcessing = true;
				$('.spinner_nekr').fadeIn();
			},
			success: function(data) {
				$('.spinner_nekr').hide();
				isProcessing = false;
				alert(data);
				$('#table-kemasan').DataTable().ajax.reload();
				$('#modal-stockkem').modal('toggle');
			}
		});
		
	}

	$(document).on('click', '#tambah-stockkem', function(event) {
		let clik = $(this);
		$('.modal-header #title').text('Tambah Kemasan');
		$('#form-stockkem').find('input').val('');
		$('#form-stockkem').find('select').val('');
		$('#produk').removeAttr('disabled');
		$('#sub_produk').removeAttr('disabled');
		$('#kemasan').removeAttr('readonly');
		$('#form-stockkem').attr('action', clik.data('link'));
		lproduk('produk');
		$(document).on('change', '#produk', function(event) {
			let val = $(this).val();
			$('#produk_in').val(val);
			lsubpro('sub_produk',val);
			$(document).on('change', '#sub_produk', function(event) {
				let cal = $(this).val();
				$('#sub_produk_in').val(cal);
				$('#kemasan').val($(this).find(':selected').data('nama_kemasan'));
				$('#kemasan_in').val($(this).find(':selected').data('idkemasan'));
			});
		});
		$('#modal-stockkem').modal('show');
	});

	$(document).on('click', '.update-kemasan', function(event) {
		let clik = $(this);
		$('.modal-header #title').text(clik.data('title'));
		$('#form-stockkem').find('input').val('');
		$('#form-stockkem').find('select').val('');
		$('#form-stockkem').attr('action', clik.data('link'));
		lproduk('produk',clik.data('produk'));
		lsubpro('sub_produk',clik.data('subproduk'));

		$('#produk').attr('disabled','disabled');
		$('#sub_produk').attr('disabled','disabled');
		$('#kemasan').attr('readonly','readonly');

		$('#produk_in').val(clik.data('produk'));
		$('#sub_produk_in').val(clik.data('subproduk'));
		$('#kemasan').val(clik.data('nama_kemasan'));
		$('#kemasan_in').val(clik.data('kemasan'));
		$('#stock').val(clik.data('stock'));
		$('#harga_satuan').val(clik.data('harga_satuan'));
		$('#modal-stockkem').modal('show');
	});

	$(document).on('click', '#delete-kemasan', function(event) {
		let dje = confirm('Apakah anda yakin ingin menghapus data ini ?');
		if (dje) {
			let id = $(this).data("id");
			let token = $("meta[name='csrf-token']").attr("content");
			$.ajax(
			{
				url: '/stockkemasan/delete/' + id,
				type: 'DELETE',
				data: {
					"_token": token,
				},
				beforeSend: function() {
					isProcessing = true;
					$('#loadingdelete').fadeIn();
				},
				success: function (data){
					$('#loadingdelete').hide();
					alert(data);
					$('#table-kemasan').DataTable().ajax.reload();
					isProcessing = false;
				}
			});
		}
	});
</script>