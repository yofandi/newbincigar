<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Crypt;
use Auth;
use DataTables;
use App\User;

class Satuan_Harga extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function satuan_harga()
    {
        $data['berat'] = [1 => 'Kilogram',2 => 'Gram'];
        $data['jenis'] = DB::table('jenis')
        ->select('id_jenis','jenis')
        ->get();
        $data['kategori'] = DB::table('kategori')
        ->select('id_kategori','kategori')
        ->get();
        $data['produk'] = DB::table('produk')->get();
        return view('home.referensi.satuan_harga.satuan_harga',$data);
    }

    public function getdatasatuan()
    {
        $query = DB::table('satuan_harga')
        ->select(DB::raw('satuan_harga.*,COUNT(history_bahanmasuk.id) AS countdata,jenis.id_jenis,jenis.jenis,kategori.kategori'))
        ->leftJoin('history_bahanmasuk', 'satuan_harga.id', '=', 'history_bahanmasuk.satuan_harga_id')
        ->join('jenis','satuan_harga.jenis_id_jenis','=','jenis.id_jenis')
        ->join('kategori','satuan_harga.kategori_id_kategori','=','kategori.id_kategori')
        ->orderBy('satuan_harga.id', 'asc')
        ->groupBy('satuan_harga.id')
        ->get();
        return Datatables::of($query)
        ->addIndexColumn()
        ->addColumn('hargatembakau_for', function ($query) {
            $awl = number_format((float)$query->harga_tembakau,2,',','.');
            return $awl;
        })
        ->make(true);
    }

    public function addsatuan_harga(Request $request)
    {
        $hargasatuan = intval(preg_replace('/[^\d.]/', '', $request->harga_satuan));
        $data = [
            'kode_htembakau' => $request->kode_htembakau,
            'tanggal_harga' => $request->tanggal_temabaku,
            'satuan_berat' => $request->satuan_berat,
            'jenis_id_jenis' => $request->jenis,
            'kategori_id_kategori' => $request->kategori,
            'harga_tembakau' => $hargasatuan,
            'keterangan_harga' => $request->keterangan,
            'created_at' => date('Y-m-d H:i:s'),
        ];

        $query = DB::table('satuan_harga')->insert($data);
        $alert = ( $query ? 'Data harga berhasil ditambahkan' : 'Data harga gagal ditambahkan');
        echo json_encode($alert);
    }

    public function updatesatuan_harga(Request $request,$id)
    {
        $hargasatuan = intval(preg_replace('/[^\d.]/', '', $request->harga_satuan));
        $data = [
            'kode_htembakau' => $request->kode_htembakau,
            'tanggal_harga' => $request->tanggal_temabaku,
            'satuan_berat' => $request->satuan_berat,
            'jenis_id_jenis' => $request->jenis,
            'kategori_id_kategori' => $request->kategori,
            'harga_tembakau' => $hargasatuan,
            'keterangan_harga' => $request->keterangan,
            'created_at' => date('Y-m-d H:i:s'),
        ];

        $query = DB::table('satuan_harga')->where('id',$id)->update($data);
        $alert = ( $query ? 'Data harga ID: '.$id.' berhasil diupdate' : 'Data harga ID: '.$id.' gagal diupdate');
        echo json_encode($alert);
    }

    public function deletesatuan_harga($id)
    {
        $query = DB::table('satuan_harga')->where('id',$id)->delete();
        $alert = ( $query ? 'Data harga ID: '.$id.' berhasil dihapus' : 'Data harga ID: '.$id.' gagal dihapus');
        echo json_encode($alert);
    }

    public function getdatahargacincin()
    {
        $query = DB::table('harga_cincin')
        ->select(DB::raw('harga_cincin.*,COUNT(history_cincin.id) AS countdata,produk.id_produk,produk.produk'))
        ->join('produk','harga_cincin.produk_id_produk','=','produk.id_produk')
        ->leftJoin('history_cincin','harga_cincin.id','=','history_cincin.harga_cincin_id')
        ->orderBy('harga_cincin.tanggal_harga')
        ->groupBy('harga_cincin.id')
        ->get();
        return Datatables::of($query)
        ->addIndexColumn()
        ->addColumn('harga_for', function ($query) {
            $awl = number_format((float)$query->harga,2,',','.');
            return $awl;
        })
        ->make(true);
    }

    public function addhargacincin(Request $request)
    {
        $hargasatuan = intval(preg_replace('/[^\d.]/', '', $request->harga_satuan));
        $data = [
            'kode_hcincin' => $request->kode_hcincin,
            'produk_id_produk' => $request->produk,
            'tanggal_harga' => $request->tanggal_cincin,
            'harga' => $hargasatuan,
            'keterangan_hargacin' => $request->keterangan,
            'created_at' => date('Y-m-d H:i:s'),
        ];

        $query = DB::table('harga_cincin')->insert($data);
        $alert = ( $query ? 'Data harga cincin berhasil ditambahkan' : 'Data harga cincin gagal ditambahkan');
        echo json_encode($alert);
    }

    public function updatehargacincin(Request $request,$id)
    {
        $hargasatuan = intval(preg_replace('/[^\d.]/', '', $request->harga_satuan));
        $data = [
            'kode_hcincin' => $request->kode_hcincin,
            'produk_id_produk' => $request->produk,
            'tanggal_harga' => $request->tanggal_cincin,
            'harga' => $hargasatuan,
            'keterangan_hargacin' => $request->keterangan,
        ];

        $query = DB::table('harga_cincin')->where('id',$id)->update($data);
        $alert = ( $query ? 'Data harga cincin ID: '.$id.' berhasil diupdate' : 'Data harga cincin ID: '.$id.' gagal diupdate');
        echo json_encode($alert);
    }

    public function deletehargacincin($id)
    {
        $query = DB::table('harga_cincin')->where('id',$id)->delete();
        $alert = ( $query ? 'Data harga cincin ID: '.$id.' berhasil dihapus' : 'Data harga cincin ID: '.$id.' gagal dihapus');
        echo json_encode($alert);
    }

    public function getdatahargacukai()
    {
        $query = DB::table('harga_cukai')
        ->select(DB::raw('harga_cukai.*,COUNT(history_cukai.id) AS countdata,produk.id_produk,produk.produk,sub_produk.sub_kode,sub_produk.sub_produk,merek_cukai.kode_cukai'))
        ->join('sub_produk','harga_cukai.sub_produk_id','=','sub_produk.id_sub_produk')
        ->join('produk','sub_produk.produk_id_produk','=','produk.id_produk')
        ->join('merek_cukai','sub_produk.merek_cukai_id','=','merek_cukai.id')
        ->leftJoin('history_cukai','harga_cukai.id','=','history_cukai.harga_cukai_id')
        ->orderBy('harga_cukai.tanggal_harga')
        ->groupBy('harga_cukai.id')
        ->get();
        return Datatables::of($query)
        ->addIndexColumn()
        ->addColumn('kodesubproduk', function ($query) {
            $awl = $query->sub_kode.' - '.$query->sub_produk;
            return $awl;
        })
        ->addColumn('harga_for', function ($query) {
            $awl = number_format((float)$query->harga,2,',','.');
            return $awl;
        })
        ->make(true);
    }

    public function addhargacukai(Request $request)
    {
        $hargasatuan = intval(preg_replace('/[^\d.]/', '', $request->harga_satuan));
        $data = [
            'sub_produk_id' => $request->subproduk,
            'tanggal_harga' => $request->tanggal_cukai,
            // 'kode_hcukai' => $request->kode_hcukai,
            'harga' => $hargasatuan,
            'keterangan_hargacukai' => $request->keterangan,
            'created_at' => date('Y-m-d H:i:s'),
        ];

        $query = DB::table('harga_cukai')->insert($data);
        $alert = ( $query ? 'Data harga cukai berhasil ditambahkan' : 'Data harga cukai gagal ditambahkan');
        echo json_encode($alert);
    }

    public function updatehargacukai(Request $request,$id)
    {
        $hargasatuan = intval(preg_replace('/[^\d.]/', '', $request->harga_satuan));
        $data = [
            'sub_produk_id' => $request->subproduk,
            'tanggal_harga' => $request->tanggal_cukai,
            // 'kode_hcukai' => $request->kode_hcukai,
            'harga' => $hargasatuan,
            'keterangan_hargacukai' => $request->keterangan,
        ];

        $query = DB::table('harga_cukai')->where('id',$id)->update($data);
        $alert = ( $query ? 'Data harga cukai ID: '.$id.' berhasil diupdate' : 'Data harga cukai ID: '.$id.' gagal diupdate');
        echo json_encode($alert);

    }

    public function deletehargacukai($id)
    {
        $query = DB::table('harga_cukai')->where('id',$id)->delete();
        $alert = ( $query ? 'Data harga cukai ID: '.$id.' berhasil dihapus' : 'Data harga cukai ID: '.$id.' gagal dihapus');
        echo json_encode($alert);
    }

    public function getdatahargastiker()
    {
        $query = DB::table('harga_stiker')
        ->select(DB::raw('harga_stiker.*,COUNT(history_stiker.id) AS countdata,produk.id_produk,produk.produk,sub_produk.sub_kode,sub_produk.sub_produk'))
        ->join('sub_produk','harga_stiker.sub_produk_id','=','sub_produk.id_sub_produk')
        ->join('produk','sub_produk.produk_id_produk','=','produk.id_produk')
        ->leftJoin('history_stiker','harga_stiker.id','=','history_stiker.harga_stiker_id')
        ->orderBy('harga_stiker.tanggal_harga')
        ->groupBy('harga_stiker.id')
        ->get();
        return Datatables::of($query)
        ->addIndexColumn()
        ->addColumn('kodesubproduk', function ($query) {
            $awl = $query->sub_kode.' - '.$query->sub_produk;
            return $awl;
        })
        ->addColumn('harga_luar_for', function ($query) {
            $awl = number_format((float)$query->harga_luar,2,',','.');
            return $awl;
        })
        ->addColumn('harga_dalam_for', function ($query) {
            $awl = number_format((float)$query->harga_dalam,2,',','.');
            return $awl;
        })
        ->make(true);
    }

    public function addhargastiker(Request $request)
    {
        $hargasatuanl = intval(preg_replace('/[^\d.]/', '', $request->harga_satuanl));
        $hargasatuand = intval(preg_replace('/[^\d.]/', '', $request->harga_satuand));
        $data = [
            'kode_hstiker' => $request->kode_hstiker,
            'sub_produk_id' => $request->subproduk,
            'tanggal_harga' => $request->tanggal_stiker,
            'harga_luar' => $hargasatuanl,
            'harga_dalam' => $hargasatuand,
            'keterangan_hargasti' => $request->keterangan,
            'created_at' => date('Y-m-d H:i:s'),
        ];

        $query = DB::table('harga_stiker')->insert($data);
        $alert = ( $query ? 'Data harga stiker berhasil ditambahkan' : 'Data harga stiker gagal ditambahkan');
        echo json_encode($alert);

    }

    public function updatehargastiker(Request $request,$id)
    {
        $hargasatuanl = intval(preg_replace('/[^\d.]/', '', $request->harga_satuanl));
        $hargasatuand = intval(preg_replace('/[^\d.]/', '', $request->harga_satuand));
        $data = [
            'kode_hstiker' => $request->kode_hstiker,
            'sub_produk_id' => $request->subproduk,
            'tanggal_harga' => $request->tanggal_stiker,
            'harga_luar' => $hargasatuanl,
            'harga_dalam' => $hargasatuand,
            'keterangan_hargasti' => $request->keterangan,
        ];

        $query = DB::table('harga_stiker')->where('id',$id)->update($data);
        $alert = ( $query ? 'Data harga stiker ID: '.$id.' berhasil diupdate' : 'Data harga stiker ID: '.$id.' gagal diupdate');
        echo json_encode($alert);

    }

    public function deletehargastiker($id)
    {
        $query = DB::table('harga_stiker')->where('id',$id)->delete();
        $alert = ( $query ? 'Data harga stiker ID: '.$id.' berhasil dihapus' : 'Data harga stiker ID: '.$id.' gagal dihapus');
        echo json_encode($alert);

    }

    public function getdatahargakemasan()
    {
        $query = DB::table('harga_kemasan')
        ->select(DB::raw('harga_kemasan.*,COUNT(history_kemasan.id) AS countdata,produk.id_produk,produk.produk,sub_produk.sub_kode,sub_produk.sub_produk'))
        ->join('sub_produk','harga_kemasan.sub_produk_id','=','sub_produk.id_sub_produk')
        ->join('produk','sub_produk.produk_id_produk','=','produk.id_produk')
        ->leftJoin('history_kemasan','harga_kemasan.id','=','history_kemasan.harga_kemasan_id')
        ->orderBy('harga_kemasan.tanggal_harga')
        ->groupBy('harga_kemasan.id')
        ->get();
        return Datatables::of($query)
        ->addIndexColumn()
        ->addColumn('kodesubproduk', function ($query) {
            $awl = $query->sub_kode.' - '.$query->sub_produk;
            return $awl;
        })
        ->addColumn('harga_kemasan_for', function ($query) {
            $awl = number_format((float)$query->harga,2,',','.');
            return $awl;
        })
        ->make(true);
    }

    public function addhargakemasan(Request $request)
    {
        $hargasatuan = intval(preg_replace('/[^\d.]/', '', $request->harga_satuan));
        $data = [
            'kode_hkemasan' => $request->kode_hkemasan,
            'sub_produk_id' => $request->subproduk,
            'tanggal_harga' => $request->tanggal_kemasan,
            'harga' => $hargasatuan,
            'keterangan_hargakem' => $request->keterangan,
            'created_at' => date('Y-m-d H:i:s'),
        ];

        $query = DB::table('harga_kemasan')->insert($data);
        $alert = ( $query ? 'Data harga kemasan berhasil ditambahkan' : 'Data harga kemasan gagal ditambahkan');
        echo json_encode($alert);
    }

    public function updatehargakemasan(Request $request,$id)
    {
        $hargasatuan = intval(preg_replace('/[^\d.]/', '', $request->harga_satuan));
        $data = [
            'kode_hkemasan' => $request->kode_hkemasan,
            'sub_produk_id' => $request->subproduk,
            'tanggal_harga' => $request->tanggal_kemasan,
            'harga' => $hargasatuan,
            'keterangan_hargakem' => $request->keterangan,
        ];

        $query = DB::table('harga_kemasan')->where('id',$id)->update($data);
        $alert = ( $query ? 'Data harga kemasan ID: '.$id.' berhasil diupdate' : 'Data harga kemasan ID: '.$id.' gagal diupdate');
        echo json_encode($alert);
    }

    public function deletehargakemasan($id)
    {
        $query = DB::table('harga_kemasan')->where('id',$id)->delete();
        $alert = ( $query ? 'Data harga kemasan ID: '.$id.' berhasil dihapus' : 'Data harga kemasan ID: '.$id.' gagal dihapus');
        echo json_encode($alert);
    }
}
