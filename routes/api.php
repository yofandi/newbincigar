<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['middleware' => ['web', 'auth', 'roles']], function() {
    /* dashboard */
    Route::get('/notiflimitbahan', 'Notification@getlimitbahanbaku');
    Route::get('/dashboard', 'Dashboard@getlimitbahan');
    Route::put('/dashboard/limit/put', 'Dashboard@puteditlimit');
    //----//
    /* aksesoris */
    Route::get('/aksesoris', 'HomeController@getaksesoris');
    Route::post('/aksesoris/post', 'HomeController@addaksesoris');
    Route::put('/aksesoris/put', 'HomeController@putaksesoris');
    Route::delete('/aksesoris/del', 'HomeController@delaksesoris'); 
    Route::get('/aksesoris/has/json', 'HomeController@getakshassub');

    Route::post('/has_aksesoris/post', 'HomeController@addhassub')->name('has_aksesoris.post');
    Route::post('/has_aksesoris/upp/{id}', 'HomeController@upphassub')->name('has_aksesoris.upp');
    Route::delete('/has_aksesoris/del/{id}', 'HomeController@deletehassub');
    /**/
    /* merek cukai */
    Route::get('/merek_cukai', 'HomeController@getmerek_cukai');
    Route::post('/merek_cukai/post', 'HomeController@addmerek_cukai');
    Route::put('/merek_cukai/put', 'HomeController@putmerek_cukai');
    Route::delete('/merek_cukai/del', 'HomeController@delmerek_cukai'); 
    /**/
    /* bahan baku */
    Route::post('/data_tembakau/setharga', 'Bahan_baku@getpricetembakau');
    Route::post('/data_cincin/setharga', 'Bahan_baku@getpricecincin');
    /* referensi stiker sub produk */
    Route::post('/kemasan/subproduk', 'HomeController@ceksubprodukkem');

    /* referensi satuan harga */
    Route::get('/hargatembakau/json', 'Satuan_Harga@getdatasatuan');
    Route::post('/hargatembakau/post', 'Satuan_Harga@addsatuan_harga')->name('api.posthargatembakau');
    Route::post('/hargatembakau/update/{id}', 'Satuan_Harga@updatesatuan_harga');
    Route::delete('/hargatembakau/delete/{id}', 'Satuan_Harga@deletesatuan_harga');

    Route::get('/hargacincin/json', 'Satuan_Harga@getdatahargacincin');
    Route::post('/hargacincin/post', 'Satuan_Harga@addhargacincin')->name('api.posthargacincin');
    Route::post('/hargacincin/update/{id}', 'Satuan_Harga@updatehargacincin');
    Route::delete('/hargacincin/delete/{id}', 'Satuan_Harga@deletehargacincin');

    Route::get('/hargacukai/json', 'Satuan_Harga@getdatahargacukai');
    Route::post('/hargacukai/post', 'Satuan_Harga@addhargacukai')->name('api.posthargacukai');
    Route::post('/hargacukai/update/{id}', 'Satuan_Harga@updatehargacukai');
    Route::delete('/hargacukai/delete/{id}', 'Satuan_Harga@deletehargacukai');

    Route::get('/hargastiker/json', 'Satuan_Harga@getdatahargastiker');
    Route::post('/hargastiker/post', 'Satuan_Harga@addhargastiker')->name('api.posthargastiker');
    Route::post('/hargastiker/update/{id}', 'Satuan_Harga@updatehargastiker');
    Route::delete('/hargastiker/delete/{id}', 'Satuan_Harga@deletehargastiker');

    Route::get('/hargakemasan/json', 'Satuan_Harga@getdatahargakemasan');
    Route::post('/hargakemasan/post', 'Satuan_Harga@addhargakemasan')->name('api.posthargakemasan');
    Route::post('/hargakemasan/update/{id}', 'Satuan_Harga@updatehargakemasan');
    Route::delete('/hargakemasan/delete/{id}', 'Satuan_Harga@deletehargakemasan');

    /* notifikasi packing ke rfs dan sebaliknya*/
    Route::get('/noc_inrfs/json', 'Notification@getlistnoc');
    Route::get('/noc_inrfs', 'Notification@getnocin')->name('nocrfs.data');
    Route::post('/noc_inrfs/post/{id}', 'Notification@change_noc')->name('nocrfs.post.noc');
    Route::get('/noc_inrfs/all', 'Notification@getsessioncart');

    Route::delete('/noc_items/delitems/{id}/{name}', 'Notification@deleteitemcart');

    Route::get('/noc_items/{idnoc_inrfs}/all', 'Notification@getnoc_items');
    Route::post('/noc_items/add/{idinrfs}', 'Notification@add_item_nocinrfs');
    Route::get('/noc_items/reject', 'Notification@getcartreject');
    Route::post('/noc_items/reject/post', 'Notification@addcartreject');
    Route::post('/noc_items/reject/upp', 'Notification@updatecartreject');
    Route::delete('/noc_items/reject/{rowId}/{name}/del', 'Notification@deletecartreject');
    Route::get('/noc_items/restore', 'Notification@restoreitemcart');
    Route::post('/noc_items/resend/{id}', 'Notification@resendreject');
    Route::get('/noc_items/nyobak', 'Notification@ceknyobak');
    /*------*/

    /* RFS dan Bagan */
    Route::get('/noc_rfsbagan', 'Notification@getnotifrfsbagan')->name('nocrfsbagan.data');
    Route::get('/noc_rfsbagan/list/json', 'Notification@getlistrfsbagan');
    Route::get('/noc_rfsbagan/items/json/{id}', 'Notification@show_rfsbaganitem');
    /*------*/

    /* tampilkan subproduk */
    Route::post('/Report/getsubproduk/json', 'Report@getsubproduk');
    Route::post('/Report/laporancincin/json', 'Report@getlaporancincin');
    Route::post('/Report/laporankemasan/json', 'Report@getlaporankemasan');
    Route::post('/Report/laporancukai/json', 'Report@getlaporancukai');
    Route::post('/Report/laporanstiker/json', 'Report@getlaporanstiker');
    Route::post('/Report/laporantembakau/json', 'Report@getlaporantembakau');

    /* laporan produksi */
    Route::post('/Report1/laporanwrapping/json', 'Report1@getlaporanwrappping');
    Route::post('/Report1/laporanpacking/json', 'Report1@getlaporanpacking');
    Route::post('/Report1/laporanquality/json', 'Report1@getlaporanquality');
    //-- -- //

    /* data rfs */
    Route::post('/rejectrfs/item', 'Rfs@getquanity');
    Route::get('/rejectrfs/get', 'Rfs@getsessioncartreject');
    Route::post('/rejectrfs/postcart', 'Rfs@inrfs_addcartreject');
    Route::put('/rejectrfs/uppcart', 'Rfs@inrfs_updcartreject');
    Route::delete('/rejectrfs/delete/{rowId}/{name}', 'Rfs@inrfs_deletereject');
    Route::post('/rejectrfs/postall', 'Rfs@addrejectall');

    Route::post('/kirimbagan/addcart', 'Rfs@addsessioncartrfs_bagan');
    Route::get('/rfs/data/nyobak', 'Rfs@rfsinsert');

    Route::post('/rfs/getbaganuser', 'Rfs@getlevelbagan');
    Route::get('/rfs/data/json', 'Rfs@getdatarfs')->name('rfs.data');
    Route::get('/rfs/datastock/json', 'Rfs@getstockrfs')->name('rfs.stock');
    Route::get('/rfs/dataupdate/keranjangitems/{id}', 'Rfs@getitemsupdate');
    Route::post('/rfs/data/addkeranjangitems', 'Rfs@addnewitem_object')->name('rfs.postitem');
    Route::put('/rfs/data/uppkeranjangitems', 'Rfs@uppnewitem_object');
    Route::delete('/rfs/data/delkeranjangitems/{id}', 'Rfs@delnewitem_object');

    // data get non tembakau //
    Route::post('/data_nontembakau/show', 'Bahan_nontembakau@getnontembakauval');
    Route::post('/data_nontembakau/some', 'Bahan_nontembakau@getsomenontembakau');
    Route::get('/data_nontembakau/json', 'Bahan_nontembakau@gettabledatanontembakau');
    Route::post('/data_nontembakau/post', 'Bahan_nontembakau@add_nontembakau')->name('nontembakau.post');
    Route::post('/data_nontembakau/upp/{id}', 'Bahan_nontembakau@update_nontembakau')->name('nontembakau.upp');

    Route::get('/data_nontembakau/getaks', 'Bahan_nontembakau@showhistoryaksesoris');
    Route::post('/data_nontembakau/uppaks/{id}', 'Bahan_nontembakau@update_aksesoris');
    Route::delete('/data_nontembakau/delaks/{id}', 'Bahan_nontembakau@delete_aksesoris');
    //***//
    // data get  //
    Route::get('/reproduksi/all', 'Reproduksi@getreproduksiall');
    //***//
});
