<script>
	$('#stock_showitbind').hide();
	$('#loadingdeletebind').hide();
	$('.spinner_nekrbind').hide();
	var tablebind = $('#table-binding').DataTable({
		"lengthMenu": [
		[3,5,10, 25, 50, 100, 150, 200, -1],
		[3,5,10, 25, 50, 100, 150, 200, "All"]
		],
		"order": [
		[0, 'desc']
		],
		processing: true,
		serverSide: true,
		ajax: '/databinding/json',
		columns: [
		{ data: 'DT_RowIndex', name: 'DT_RowIndex' },
		{ data: 'tanggal_bind', name: 'tanggal_bind' },
		{ data: 'produk', name: 'produk' },
		{ data: 'jenis', name: 'jenis' },
		{ data: 'stockawal', name: 'stockawal' },
		{ data: 'weisbmasuk', name: 'weisbmasuk' },
		{ data: 'terpakaiprob', name: 'terpakaiprob' },
		{ data: 'terpakaiweisb', name: 'terpakaiweisb' },
		{ data: 'sisaprob', name: 'sisaprob' },
		{ data: 'awlbatang', name: 'awlbatang' },
		{ data: 'terpakai_filling', name: 'terpakai_filling' },
		{ data: 'hasil_bind', name: 'hasil_bind' },
		{
			data: null,
			"bSortable": false,
			mRender: function (o) { 
				return '<button type="button" class="btn btn-info btn-rounded btn-xs" id="update-binding" data-title="Update binding | ID: '+ o.id +'" data-link="/databinding/upp/'+ o.id +'/'+ o.weisbmasuk +'/'+ o.terpakaiprob +'/'+ o.terpakaiweisb +'/'+ o.terpakai_filling +'/'+o.hasil_bind +'" data-tanggal_binding="'+ o.tanggal_bind +'" data-jenis="'+ o.id_jenis +'" data-produk="'+ o.id_produk +'" data-stockpro="'+ o.stockawal +'" data-stockweis="'+ o.stockawalweis +'" data-stockbatang="'+ o.awlbatang +'" data-masukweis="'+ o.weisbmasuk +'" data-terpakaiprob="'+ o.terpakaiprob +'" data-terpakaiweisb="'+ o.terpakaiweisb +'" data-terpakaifilling="'+ o.terpakai_filling +'" data-batang="'+ o.hasil_bind +'" data-idprobaku="'+ o.stock_probaku_id +'" data-idfilling="'+ o.stock_filling_id +'" data-idbinding="'+ o.stock_binding_id +'"><i class="mdi mdi-dots-horizontal"></i></button><button type="button" class="btn btn-danger btn-rounded btn-xs" id="delete-binding" data-link="/databinding/del/'+ o.id +'/'+ o.weisbmasuk +'/'+ o.terpakaiprob +'/'+ o.terpakaiweisb +'/'+ o.terpakai_filling +'/'+ o.hasil_bind +'" data-idprobaku="'+ o.stock_probaku_id +'" data-idfilling="'+ o.stock_filling_id +'" data-idbinding="'+ o.stock_binding_id +'" data-awlfill="'+ o.awlbatang +'"><i class="fa fa-trash"></i></button>'; }
			},
			],
			"displayLength": 10,
		});
	$('#table-binding tbody').on('click', 'tr.group', function() {
		var currentOrder = tablebind.order()[0];
		if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
			tablebind.order([2, 'desc']).draw();
		} else {
			tablebind.order([2, 'asc']).draw();
		}
	});
	$('#table-binding tbody').on('click', 'tr.group', function() {
		var currentOrder = tablebind.order()[0];
		if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
			tablebind.order([2, 'desc']).draw();
		} else {
			tablebind.order([2, 'asc']).draw();
		}
	});

	function getdatasearchbind(position1,position2,position3) {
		let jenis = $('#jenis_inbind').val();
		let produk = $('#produk_inbind').val();

		$.ajax({
			url: '/databinding/getsearchfill',
			type: 'POST',
			dataType: 'json',
			data: {
				"_token": token,
				produk: produk,
				jenis: jenis
			},
		})
		.done(function(data) {
			$('#idfillingbind').val(data.idfilling);
			$('#idbinding').val(data.idbinding);
			$('#idprobakubind').val(data.id);
			$('#' + position1).val(data.jml_produksi);
			$('#' + position2).val(data.stock_weisb);
			$('#' + position3).val(data.jml_fill);
		});
		
	}

	function Refresh_binding() {
		$('#table-binding').DataTable().ajax.reload();
	}

	// binding proses
	function seacrh_databind() {
		getdatasearchbind('stockprobind','stockweisb','batangfill');
	}

	function save_methodbind() {
		let link = $('#form-binding').attr('action');
		let idprobaku = $('#idprobakubind').val();
		let idbinding = $('#idbinding').val();
		let idfilling = $('#idfillingbind').val();
		let idjnpro = $('#idjnprobind').val();
		let tanggal_binding = $('#tanggal_binding').val();
		let jenis_in = $('#jenis_inbind').val();
		let produk_in = $('#produk_inbind').val();
		let stockpro = $('#stockprobind').val();
		let stockweisb = $('#stockweisb').val();
		let stockbatang = $('#batangfill').val();
		let masukweisb = $('#masukweisb').val();
		let terpakaipro = $('#terpakaiprobind').val();
		let terpakaiweisb = $('#terpakaiweisb').val();
		let terpakaifilling = $('#terpakaifilling').val();
		let batangjml = $('#batangjmlbind').val();
		$.ajax({
			url: link,
			type: 'POST',
			dataType: 'json',
			data: {
				"_token": token,
				tanggal_binding: tanggal_binding,
				jenis: jenis_in,
				produk: produk_in,
				stockpro: stockpro,
				stockweisb: stockweisb,
				stockbatang: stockbatang,
				masukweisb:masukweisb,
				terpakaipro: terpakaipro,
				terpakaiweisb: terpakaiweisb,
				batangjml: batangjml,
				terpakaifilling: terpakaifilling,
				idprobaku: idprobaku,
				idfilling: idfilling,
				idbinding: idbinding,
				idjnpro:idjnpro
			},
			beforeSend: function() {
				isProcessing = true;
				$('.spinner_nekrbind').fadeIn();
			},
			success: function(data) {
				$('.spinner_nekrbind').hide();
				isProcessing = false;
				alert(data);
				Refresh_binding();
				$('#modal-binding').modal('toggle');
			},
		});
	}


	$(document).on('click', '#tambah-binding', function(event) {
		let clik = $(this);
		$('.modal-header #title').text('Tambah binding');
		$('#form-binding').attr('action', clik.data('link'));
		$('#form-binding').find('input').val('');
		$('#form-binding').find('select').val('');
		$("#tanggal_binding").datetimepicker({
			format: 'YYYY-MM-DD HH:mm:ss',
		}).data("DateTimePicker").date(new Date());
		$('#stock_showitbind').fadeIn();
		$('#jenisbind').removeAttr('disabled');
		$('#produkbind').removeAttr('disabled');
		ljenis('jenisbind');
		$(document).on('change', '#jenisbind', function(event) {
			let jam = $(this).val();
			$('#jenis_inbind').val(jam);
			lproduk('produkbind',jam);
			$(document).on('change', '#produkbind', function(event) {
				let pwe = $(this).val();
				$('#produk_inbind').val(pwe);
				let kk = $(this).find(':selected').data('id');
				$('#idjnprobind').val(kk);
			});
		});
		$('#modal-binding').modal('show');
	});

	$(document).on('click', '#update-binding', function(event) {
		let click = $(this);
		$('.modal-header #title').text(click.data('title'));
		$('#form-binding').attr('action', click.data('link'));
		$('#form-binding').find('input').val('');
		$('#form-binding').find('select').val('');
		$('#stock_showitbind').hide();
		$('#jenisbind').attr('disabled','disabled');
		$('#produkbind').attr('disabled','disabled');
		ljenis('jenisbind',click.data('jenis'));
		lproduk('produkbind',click.data('jenis'),click.data('produk'));

		$("#tanggal_binding").datetimepicker({
			format: 'YYYY-MM-DD HH:mm:ss',
		}).data("DateTimePicker").date(click.data('tanggal_binding'));
		$('#jenis_inbind').val(click.data('jenis'));
		$('#produk_inbind').val(click.data('produk'));
		$('#stockprobind').val(click.data('stockpro'));
		$('#stockweisb').val(click.data('stockweis'));
		$('#batangfill').val(click.data('stockbatang'));
		$('#masukweisb').val(click.data('masukweis'));
		$('#terpakaiprobind').val(click.data('terpakaiprob'));
		$('#terpakaiweisb').val(click.data('terpakaiweisb'));
		$('#terpakaifilling').val(click.data('terpakaifilling'));
		$('#batangjmlbind').val(click.data('batang'));
		$('#idprobakubind').val(click.data('idprobaku'));
		$('#idbinding').val(click.data('idbinding'));
		$('#idfillingbind').val(click.data('idfilling'));
		$('#modal-binding').modal('show');
	});

	$(document).on('click', '#delete-binding', function(event) {
		let snjs = confirm('Apakah anda yakin ingin menghapus data ini ?');
		if (snjs) {
			let link = $(this).data("link");
			let idprobaku = $(this).data('idprobaku');
			let idfilling = $(this).data('idfilling');
			let idbinding = $(this).data('idbinding');
			let awlfill = $(this).data('awlfill');
			$.ajax(
			{
				url: link,
				type: 'DELETE',
				data: {
					"_token": token,
					idprobaku: idprobaku,
					idfilling: idfilling,
					idbinding: idbinding,
					awlfill:awlfill
				},
				beforeSend: function() {
					isProcessing = true;
					$('#loadingdeletebind').fadeIn();
				},
				success: function (data){
					$('#loadingdeletebind').hide();
					isProcessing = false;
					alert(data);
					Refresh_binding();
				}
			});
		}
	});
// binding ending
</script>