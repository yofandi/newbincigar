<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use DataTables;
use Auth;
use App\Cool;
use App\Stock_fumigasi;
use App\Stock_cool;

use App\Http\Controllers\Back_cool;

class Coolcont extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->produksi = $res = new Back_cool;
    }
    public function searchweiprocool(Request $request)
    {
        $bnerf = $this->produksi->sprocool($request);   
        echo json_encode($bnerf);
    }

    public function getdatacool()
    {
        $query = DB::table('cool')
        ->select('cool.*','jenis.id_jenis','produk.id_produk','jenis.jenis','produk.produk')
        ->join('stock_cool','cool.stock_cool_id','=','stock_cool.id')
        ->join('jenis','stock_cool.jenis_has_produk_id_jenis','=','jenis.id_jenis')
        ->join('produk','stock_cool.jenis_has_produk_id_produk','=','produk.id_produk')
        ->get();
        return Datatables::of($query)
        ->addIndexColumn()
        ->addColumn('awlbatang', function ($query) {
            $awl = $query->hasil_akhircool + $query->tambah_cool;
            return $awl;
        })
        ->make(true);
    }

    public function datacool()
    {
        $data['jenis'] = DB::table('jenis')->select('id_jenis','jenis')->get();
        return view('home.produksi.cool.datacool',$data);
    }

    public function addcool(Request $request)
    {
        DB::beginTransaction();

        $qq = $this->produksi->stockcool($request);

        $data = [
            'tambah_cool' => $request->batangjml,
            'hasil_akhircool' => $qq['jml_fum'],
            'lama_cool' => $request->durasi,
            'keterangan_cool' => $request->ket,
            'tanggal_cool' => $request->tanggal_cool,
            'stock_cool_id' => $qq['idcool'],
            'stock_fumigasi_id' => $qq['idfumigasi'],
            'author_session' => Auth::user()->id,
            'created_at' => date('Y-m-d H:i:s'),
            'log' => "INSERT"
        ];
        $query1 = Cool::insert($data);
        if ($query1) {
            $alert = 'Cool berhasil ditambahkan!';
        } else {
            $alert = 'Cool gagal ditambahkan!';
        }
        echo json_encode($alert);
        DB::commit();
    }

    public function uppcool(Request $request,$id,$batang)
    {
        DB::beginTransaction();
        $jwty = $request->stockbatang - $request->batangjml;
        $this->produksi->ifuppstockcool($request,$batang);

        $data = [
            'tambah_cool' => $request->batangjml,
            'hasil_akhircool' => $jwty,
            'lama_cool' => $request->durasi,
            'keterangan_cool' => $request->ket,
            'tanggal_cool' => $request->tanggal_cool,
            'author_session' => Auth::user()->id,
            'log' => "UPDATE"
        ];
        $query1 = Cool::where('id',$id)->update($data);
        if ($query1) {
            $this->produksi->loop_upcool($request,$id);
            $alert = 'Cool ID: '.$id.' berhasil diupdate!';
        } else {
            $alert = 'Cool ID: '.$id.' gagal diupdate!';
        }
        echo json_encode($alert);
        DB::commit();
    }

    public function delcool(Request $request,$id,$batang)
    {
        DB::beginTransaction();
        $werty = Stock_fumigasi::where('id',$request->idfumigasi)->first();
        $qusrt = Stock_cool::where('id',$request->idcool)->first();

        $batangfumi = $werty->jml_fum + $batang;
        $batangcool = $qusrt->jml_cool - $batang;

        $update3 = Stock_fumigasi::find($request->idfumigasi);
        $update3->jml_fum = $batangfumi;
        $update3->save();
        $update2 = Stock_cool::find($request->idcool);
        $update2->jml_cool = $batangcool;
        $update2->save();

        $query1 = Cool::where('id',$id)->delete();
        if ($query1) {
            $this->produksi->loop_upcool($request,$id,$batang);
            $alert = 'Cool ID : '.$id.' berhasil dihapus!';
        } else {
            $alert = 'Cool ID : '.$id.' gagal dihapus!';
        }
        echo json_encode($alert);
        DB::commit();      
    }

}
