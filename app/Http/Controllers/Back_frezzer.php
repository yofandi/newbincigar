<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Frezer;
use App\Stock_drying1;
use App\Stock_frezer;

class Back_frezzer extends Controller
{
	public function sprofre($request)
	{
		$qusrt = DB::table('stock_drying1')
		->select('stock_drying1.id','stock_drying1.jml_dry1')
		->join('jenis_has_produk','stock_drying1.jenis_has_produk_id_jnpro','=','jenis_has_produk.id_jnpro')
		->where([['jenis_has_produk.jenis_id_jenis','=',$request->jenis],['jenis_has_produk.produk_id_produk','=',$request->produk]]);

		$qusrt1 = DB::table('stock_frezer')
		->select('stock_frezer.id','stock_frezer.jml_fre')
		->join('jenis_has_produk','stock_frezer.jenis_has_produk_id_jnpro','=','jenis_has_produk.id_jnpro')
		->where([['jenis_has_produk.jenis_id_jenis','=',$request->jenis],['jenis_has_produk.produk_id_produk','=',$request->produk]]);

		if ($qusrt->count() > 0) {
			$aem = $qusrt->first();
			$qer = $aem->jml_dry1;
			$qwc = $aem->id;
		} else { $qer = 0; $qwc = '';}

		if ($qusrt1->count() > 0) {
			$aem1 = $qusrt1->first();
			$ber = $aem1->jml_fre;
			$jkd = $aem1->id;
		} else { $ber = 0; $jkd = ''; }
		$array = ['jml_dry1' => $qer,'jml_fre' => $ber,'iddrying1' => $qwc,'idfrezzer' => $jkd];
		return $array;
	}

	public function stockfre($request)
	{
		$bang = $request->stockbatang - $request->batangjml;
		$cekwrt = stock_drying1::find($request->iddrying1);
		$cekwrt->jml_dry1 = $bang;
		$cekwrt->save();
		$resultidwrt = $cekwrt->id;

		if ($request->idfrezzer != '') {
			$id = $request->idfrezzer; 
			$show = Stock_frezer::where('id',$id)->first();
			$jml = $show->jml_fre + $request->batangjml;

			$cek = Stock_frezer::find($id);
			$cek->jml_fre = $jml;
			$cek->save();

			$resultid = $cek->id;
		} else {
			$jml = $request->batangjml;
			$save = new Stock_frezer;

			$save->jml_fre = $jml;
			$save->jenis_has_produk_id_jnpro = $request->idjnpro;
			$save->jenis_has_produk_id_jenis = $request->jenis;
			$save->jenis_has_produk_id_produk = $request->produk;
			$save->save();
			$resultid = $save->id;
		}
		return ['iddrying1' => $resultidwrt,'idfrezzer' => $resultid,'jml_dry1' => $bang,'jml_fre' => $jml];
	}

	public function ifuppstockfre($request,$batang)
	{
		if ($batang != $request->batangjml) {
			$shdry1 = Stock_drying1::where('id',$request->iddrying1)->first();
			$shfre = Stock_frezer::where('id',$request->idfrezzer)->first();

			if ($batang > $request->batangjml) {
				$nah = $batang - $request->batangjml;
				$hasilw = $shfre->jml_fre - $nah;
				$slash = Stock_drying1::find($request->iddrying1);
				$slash->jml_dry1 = $shdry1->jml_dry1 + $nah;
				$slash->save();
			} elseif ($batang < $request->batangjml) {
				$nah = $request->batangjml - $batang;
				$hasilw = $shfre->jml_fre + $nah;
				$slash = Stock_drying1::find($request->iddrying1);
				$slash->jml_dry1 = $shdry1->jml_dry1 - $nah;
				$slash->save();
			} else {
				$hasilw = $shfre->jml_fre;
			}

			$query2 = Stock_frezer::find($request->idfrezzer);
			$query2->jml_fre = $hasilw;
			$query2->save();
		}
	}

	public function loop_upfre($request,$id,$batang = '')
	{
		$ceh = Frezer::where([['stock_frezer_id','=',$request->idfrezzer],['stock_drying1_id','=',$request->iddrying1],['id','>',$id]]);
		if ($ceh->count() > 0) {
			$reg = $ceh->get();
			for ($i=0; $i < $ceh->count(); $i++) {
				$show = Frezer::where([['stock_frezer_id','=',$request->idfrezzer],['stock_drying1_id','=',$request->iddrying1],['id','<',$reg[$i]->id]])->orderBy('id','desc')->take('1');
				if ($show->count() > 0) {
					$first = $show->first();
					$hsie = $first->hasil_akhirfre - $reg[$i]->tambah_fre;
				} else {
					if ($batang == '') {
						$awl1 = $reg[$i]->hasil_akhirfre + $reg[$i]->tambah_fre;
						$hsie = $awl1 - $reg[$i]->tambah_fre;
					} else {
						$awl1 = $reg[$i]->hasil_akhirfre + $reg[$i]->tambah_fre + $batang;
						$hsie = $awl1 - $reg[$i]->tambah_fre;
					}
				}
				$update = Frezer::where('id',$reg[$i]->id)->update(['hasil_akhirfre' => $hsie]);
			}
		}
	}

}
