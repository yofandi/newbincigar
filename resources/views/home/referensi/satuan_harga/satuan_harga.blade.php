@extends('../../layout')

@section('title')
<title>BIN - ERP : Harga</title>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css" />
@endsection

@section('content')
<div class="row page-titles">
	<div class="col-md-5 align-self-center">
		<h3 class="text-themecolor">Harga</h3>
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a href="javascript:void(0)">Referensi</a></li>
			<li class="breadcrumb-item active">Harga</li>
		</ol>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="card">
			<ul class="nav nav-tabs customtab" role="tablist">
				<li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#harga_tembakau" role="tab" aria-selected="true"><span class="hidden-sm-up"><i class="ti-home"></i></span> <span class="hidden-xs-down">Harga Tembakau</span></a> </li>

				<li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#harga_cincin" role="tab" aria-selected="false"><span class="hidden-sm-up"><i class="ti-email"></i></span> <span class="hidden-xs-down">Harga Cincin</span></a> </li>

				<li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#harga_cukai" role="tab" aria-selected="false"><span class="hidden-sm-up"><i class="ti-email"></i></span> <span class="hidden-xs-down">Harga Cukai</span></a> </li>

				<li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#harga_stiker" role="tab" aria-selected="false"><span class="hidden-sm-up"><i class="ti-email"></i></span> <span class="hidden-xs-down">Harga Stiker</span></a> </li>

				<li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#harga_kemasan" role="tab" aria-selected="false"><span class="hidden-sm-up"><i class="ti-email"></i></span> <span class="hidden-xs-down">Harga Kemasan</span></a> </li>
			</ul>
			<div class="tab-content">
				<div class="tab-pane active p-20" id="harga_tembakau" role="tabpanel">
					@include('home.referensi.satuan_harga.tharga_tembakau')
				</div>
				<div class="tab-pane p-20" id="harga_cincin" role="tabpanel">
					@include('home.referensi.satuan_harga.tharga_cincin')
				</div>
				<div class="tab-pane p-20" id="harga_cukai" role="tabpanel">
					@include('home.referensi.satuan_harga.tharga_cukai')
				</div>
				<div class="tab-pane p-20" id="harga_stiker" role="tabpanel">
					@include('home.referensi.satuan_harga.tharga_stiker')
				</div>
				<div class="tab-pane p-20" id="harga_kemasan" role="tabpanel">
					@include('home.referensi.satuan_harga.tharga_kemasan')
				</div>
			</div>
		</div>
	</div>
</div>
@include('home.referensi.satuan_harga.modal_hargatembakau')
@include('home.referensi.satuan_harga.modal_hargacincin')
@include('home.referensi.satuan_harga.modal_hargacukai')
@include('home.referensi.satuan_harga.modal_hargastiker')
@include('home.referensi.satuan_harga.modal_hargakemasan')

<div class="modal fade" id="modal-cobacin" tabindex="-1" role="dialog" aria-labelledby="title" aria-hidden="true" style="display: none;">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<div class="card">
				<div class="card-header">
					<div class="modal-header">
						<h4 class="modal-title" id="">Tambah</h4>
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					</div>
				</div>
				<div class="card-body">
					<form id="form-coba" method="POST">
						<div class="modal-body row">
							<div class="form-group col-md-12 fa-3x spinner_nekr" align="center">
								<i class="fas fa-spinner fa-spin"></i>
							</div>
							<div class="form-group col-md-12">
								<label class="control-label">Tanggal</label>
								<input type="text" class="form-control" id="tanggal_cobacin" placeholder="Tanggal">
							</div>
							<div class="form-group col-md-12">
								<label class="control-label">Produk</label>
								<input type="text" class="form-control" id="" placeholder="Produk">
							</div>
							<div class="form-group col-md-12">
								<label class="control-label">Harga /satuan</label>
								<input type="text" class="form-control" id="hargacoba">
							</div>
							<div class="form-group col-md-12">
								<label class="control-label">Keterangan</label>
								<textarea id="" class="form-control" placeholder="keterangan"></textarea>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" onclick="" class="btn btn-primary waves-effect text-left">Save</button>
							<button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Close</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection

@section('javascript')
<script src="{{ asset('node_modules/moment/moment.js') }}">
</script><script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
<script src="{{ asset('node_modules/datatables/datatables.min.js') }}"></script>
{{-- <script src="{{ asset('node_modules/jquery.number/jquery.number.min.js') }}"></script> --}}
@include('home.referensi.satuan_harga.script_satuan')
@include('home.referensi.satuan_harga.script_hargatembakau')
@include('home.referensi.satuan_harga.script_hargacincin')
@include('home.referensi.satuan_harga.script_hargacukai')
@include('home.referensi.satuan_harga.script_hargastiker')
@include('home.referensi.satuan_harga.script_hargakemasan')
@endsection