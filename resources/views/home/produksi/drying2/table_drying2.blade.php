<div class="row">
	<div class="col-md-6">
		<legend>Data Drying 2</legend>
	</div>
	<div class="col-md-2 offset-md-4">
		<button type="button" data-link="{{ route('drying2.post') }}" class="btn btn-success btn-rounded btn-icons " id="tambah-drying2">Tambah</button>
		<button type="button" onclick="Refresh_drying2()" class="btn btn-rounded btn-icons btn-secondary">Refresh</button>
	</div>
	<div class="col-md-12" align="center" id="loadingdeletedry2">
		<h5>Data tengah diproses. Mohon tunggu sejenak!</h5>
	</div>
	<div class="table-responsive">
		<table id="table-drying2" width="100%" class="display nowrap table table-hover table-striped table-bordered">
			<thead>
				<tr>
					<th>No.</th>
					<th>Tanggal</th>
					<th>Produk</th>
					<th>Jenis</th>
					<th>Durasi</th>
					<th>Batang Frezzer</th>
					<th>Tambah Batang</th>
					<th>JML</th>
					<th>Ket</th>
					<th>Aksi</th>
				</tr>
			</thead>
		</table>
	</div>
</div>