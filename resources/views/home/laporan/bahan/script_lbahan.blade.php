<script>
	$.extend(true, $.fn.datetimepicker.defaults, {
		icons: {
			time: 'far fa-clock',
			date: 'far fa-calendar',
			up: 'fas fa-arrow-up',
			down: 'fas fa-arrow-down',
			previous: 'fas fa-chevron-left',
			next: 'fas fa-chevron-right',
			today: 'fas fa-calendar-check',
			clear: 'far fa-trash-alt',
			close: 'far fa-times-circle'
		}
	});

	$('#produk').selectpicker();
	$('#sub_produk').selectpicker();

	function format_number(number, prefix, thousand_separator, decimal_separator)
	{
		var 	thousand_separator = thousand_separator || ',',
		decimal_separator = decimal_separator || '.',
		regex		= new RegExp('[^' + decimal_separator + '\\d]', 'g'),
		number_string = number.toString().replace(regex, ''),
		split	  = number_string.split(decimal_separator),
		rest 	  = split[0].length % 3,
		result 	  = split[0].substr(0, rest),
		thousands = split[0].substr(rest).match(/\d{3}/g);

		if (thousands) {
			separator = rest ? thousand_separator : '';
			result += separator + thousands.join(thousand_separator);
		}
		result = split[1] != undefined ? result + decimal_separator + split[1] : result;
		return prefix == undefined ? result : (result ? prefix + result : '');
	};

	let token = $("meta[name='csrf-token']").attr("content");
	let boot = $("link[name='bootstrap']").attr("href");
	$('#kemasan-hidden').hide();
	$('#sub-hidden').hide();
	$('#print_html').hide();
	$('#tanggalawld').hide();
	$('#pjnr').hide();
	$('#tanggalakhrd').hide();
	$('#produknedf').hide();
	$('#loading').hide();
	$('#satuanberat').hide();
	$('#tahunselect').hide();
	$('#jenislaporan').hide();

	function print() {
		var divToPrint = document.getElementById('print');
		var htmlToPrint = '' +
		'<style type="text/css">' +
		'@page {' +
		'size: A4 landscape;' +
		// 'margin: 10%;' +
		// 'margin-top: 0.5cm;' +
		// 'margin-bottom: 0.5cm;' +
		// 'margin-left: 2cm;' +
		// 'margin-right: 1cm;' +
		'}' +
		'body {' + 
		'font-family:Arial, Helvetica, sans-serif;'+
		'font-size:10px;'+
		'color:#000000;'+
		'}' +
		'table th, table td {' +
		'border:1px solid #000;' +
		'padding;2em;' +
		'}' +
		'</style>';
		htmlToPrint += divToPrint.outerHTML;
		newWin = window.open("");
		// newWin.document.write("<h3 align='center'>Print Page</h3>");
		newWin.document.write(htmlToPrint);
		newWin.print();
		newWin.close();
	}
	
	$("#tgl_awl").datetimepicker({
		format: 'YYYY-MM-DD',
	}).data("DateTimePicker").date(new Date());
	$("#tgl_akhr").datetimepicker({
		format: 'YYYY-MM-DD',
	}).data("DateTimePicker").date(new Date());

	const berat = {!! json_encode($berat) !!};

	function getsub_produk(position,produk) {
		$.ajax({
			url: '/api/Report/getsubproduk/json',
			type: 'POST',
			dataType: 'json',
			data: {
				"_token": token,
				produk: produk,
			},
		})
		.done(function(data) {
			// <option value="">Semua</option>
			let opt = '';
			for(let i = 0; i < data.length; i++) {
				opt += '<option value="'+ data[i].id_sub_produk +'">'+ data[i].sub_kode +' - '+ data[i].sub_produk +'</option>';
			}
			$('#' + position).html(opt).selectpicker('refresh');
		});
	}

	function beratisi(position,like = '') {
		$hmtl = '';
		$.each(berat, function(index, value) {
			$hmtl += '<option value="'+ index +'"';
			if (like == index) { $hmtl += 'selected'; }
			$hmtl += '>'+ value +'</option>';
		});
		$('#' + position).html($hmtl);
	}

	function isicin(data,awal,akhir,jenis) {
		let html = '';		
		html +='<div class="col-md-12" align="center">';
		html +='<div class="table-responsive">';
		html +='<h3 class="text-center">DAFTAR '+ data.title +' CERUTU</h3>';
		html +='<h3 class="text-center">'+ awal +' S.d. '+ akhir +'</h3>';
		html +='<h3 class="text-center">PT. BOSS IMAGE NUSANTARA</h3>';
		html +='<table width="100%" class="table table-bordered table-striped">';
		html +='<thead align="center" class="table-dark">';
		html +='<tr>';
		if (jenis == 1) {
			html +='<th rowspan="2">Tanggal</th>';
		}
		html +='<th rowspan="2">Uraian</th>';
		html +='<th rowspan="2">Harga</th>';
		html +='<th rowspan="2">Awal</th>';
		html +='<th rowspan="2"h>Masuk</th>';
		html +='<th rowspan="2">Keluar</th>';
		html +='<th rowspan="2">Afkir</th>';
		html +='<th colspan="2">Jumlah</th>';
		html +='</tr>';
		html +='<tr>';
		html +='<th>Pcs</th>';
		html +='<th>Rupiah</th>';
		html +='</tr>';
		html +='</thead>';
		html +='<tbody>';
		for (var i = 0; i < data.uraian.length; i++) {
			html +='<tr>';
			if (jenis == 1) {
				html +='<td class="text-center">'+ data.tanggal[i] +'</td>';
			}
			html +='<td class="text-center">'+ data.uraian[i] +'</td>';
			html +='<td class="text-right">'+ data.harga[i] +'</td>';
			html +='<td class="text-right">'+ data.awal[i] +'</td>';
			html +='<td class="text-right">'+ data.masuk[i] +'</td>';
			html +='<td class="text-right">'+ data.keluar[i] +'</td>';
			html +='<td class="text-right">'+ data.afkir[i] +'</td>';
			html +='<td class="text-right">'+ data.Pcs[i] +'</td>';
			html +='<td class="text-right">'+ data.totalrupiah[i] +'</td>';
			html +='</tr>';
		}
		html +='</tbody>';
		html +='<tfoot>';
		html +='<tr>';
		html +='<th>Jumlah</th>';
		if (jenis == 1) {
			html +='<th></th>';
		}
		html +='<th></th>';
		html +='<th class="text-right">'+ data.tot1 +'</th>';
		html +='<th class="text-right">'+ data.tot2 +'</th>';
		html +='<th class="text-right">'+ data.tot3 +'</th>';
		html +='<th class="text-right">'+ data.tot4 +'</th>';
		html +='<th class="text-right">'+ data.tot5 +'</th>';
		html +='<th class="text-right">'+ data.tot6 +'</th>';
		html +='</tr>';
		html +='</tfoot>';
		html +='</table>';
		html +='</div>';
		html +='</div>';

		$('#table_html').html(html);
	}

	function isisam(data,awal,akhir,jenis) {
		let html = '';		
		html +='<div class="col-md-12" align="center">';
		html +='<div class="table-responsive">';
		html +='<h3 class="text-center">DAFTAR '+ data.title +' CERUTU</h3>';
		html +='<h3 class="text-center">'+ awal +' S.d. '+ akhir +'</h3>';
		html +='<h3 class="text-center">PT. BOSS IMAGE NUSANTARA</h3>';
		html +='<table width="100%" class="table table-bordered table-striped">';
		html +='<thead align="center" class="table-dark">';
		html +='<tr>';
		if (jenis == 1) {
			html +='<th rowspan="2">Tanggal</th>';
		}
		html +='<th rowspan="2">Uraian</th>';
		html +='<th rowspan="2">Harga</th>';
		html +='<th rowspan="2">Awal</th>';
		html +='<th rowspan="2"h>Masuk</th>';
		html +='<th rowspan="2">Keluar</th>';
		html +='<th colspan="2">Jumlah</th>';
		html +='</tr>';
		html +='<tr>';
		html +='<th>Pcs</th>';
		html +='<th>Rupiah</th>';
		html +='</tr>';
		html +='</thead>';
		html +='<tbody>';
		for (var i = 0; i < data.uraian.length; i++) {
			html +='<tr>';
			if (jenis == 1) {
				html +='<td class="text-center">'+ data.tanggal[i] +'</td>';
			}
			html +='<td class="text-center">'+ data.uraian[i] +'</td>';
			html +='<td class="text-right">'+ data.harga[i] +'</td>';
			html +='<td class="text-right">'+ data.awal[i] +'</td>';
			html +='<td class="text-right">'+ data.masuk[i] +'</td>';
			html +='<td class="text-right">'+ data.keluar[i] +'</td>';
			html +='<td class="text-right">'+ data.Pcs[i] +'</td>';
			html +='<td class="text-right">'+ data.totprice[i] +'</td>';
			html +='</tr>';
		}
		html +='</tbody>';
		html +='<tfoot>';
		html +='<tr>';
		if (jenis == 1) {
			html +='<th></th>';
		}
		html +='<th>Jumlah</th>';
		html +='<th></th>';
		html +='<th class="text-right">'+ data.tot1 +'</th>';
		html +='<th class="text-right">'+ data.tot2 +'</th>';
		html +='<th class="text-right">'+ data.tot3 +'</th>';
		html +='<th class="text-right">'+ data.tot4 +'</th>';
		html +='<th class="text-right">'+ data.tot5 +'</th>';
		html +='</tr>';
		html +='</tfoot>';
		html +='</table>';
		html +='</div>';
		html +='</div>';

		$('#table_html').html(html);
	}

	function isisti(data,awal,akhir,jenis) {
		let html = '';
		html += '<div class="col-md-12" align="center">';
		html += '<div class="table-responsive">';
		html += '<h3 class="text-center">DAFTAR STIKER CERUTU</h3>';
		html +='<h3 class="text-center">'+ awal +' S.d. '+ akhir +'</h3>';
		html += '<h3 class="text-center">PT. BOSS IMAGE NUSANTARA</h3>';
		html += '<table width="100%" class="table table-bordered table-striped">';
		html += '<thead align="center" class="table-dark">';
		html += '<tr>';
		if (jenis == 1) {
			html += '<th rowspan="2">Tanggal</th>';
		}
		html += '<th colspan="2">Uraian</th>';
		html += '<th colspan="6">Stiker Luar</th>';
		html += '<th colspan="6">Stiker Dalam</th>';
		html += '<th colspan="5">Jumlah</th>';
		html += '</tr>';
		html += '<tr>';
		html += '<th colspan="2">Merk</th>';
		html += '<th>Harga</th>';
		html += '<th>Awal</th>';
		html += '<th>Masuk</th>';
		html += '<th>Keluar</th>';
		html += '<th>Stok</th>';
		html += '<th>Rupiah</th>';
		html += '<th>Harga</th>';
		html += '<th>Awal</th>';
		html += '<th>Masuk</th>';
		html += '<th>Keluar</th>';
		html += '<th>Stok</th>';
		html += '<th>Rupiah</th>';
		html += '<th>Awal</th>';
		html += '<th>Masuk</th>';
		html += '<th>Keluar</th>';
		html += '<th>Stok</th>';
		html += '<th>Rupiah</th>';
		html += '</tr>';
		html += '</thead>';
		html += '<tbody class="text-right">';
		for (var i = 0; i < data.isi.length ; i++) {
			html += '<tr>';
			if (jenis == 1) {
				html += '<td>'+ data['isi'][i].tanggal +'</td>';
			}

			html += '<td class="text-center">'+ data['isi'][i].kode_produk +'</td>';

			if (jenis == 1 || jenis == 0) {
				html += '<td class="text-center">'+ data['isi'][i].sub_kode +' - '+ data['isi'][i].sub_produk +'</td>';
			} else {
				html += '<td></td>';
			}
			html += '<td>'+ data['isi'][i].harga_luar +'</td>';
			html += '<td>'+ data['isi'][i].awal_luar +'</td>';
			html += '<td class="text-right">'+ data['isi'][i].masuk_luar +'</td>';
			html += '<td class="text-right">'+ data['isi'][i].pakai_luar +'</td>';
			html += '<td class="text-right">'+ data['isi'][i].hasil_luar +'</td>';
			html += '<td>'+ data['isi'][i].hargaluar +'</td>';
			html += '<td>'+ data['isi'][i].harga_dalam +'</td>';
			html += '<td>'+ data['isi'][i].awal_dalam +'</td>';
			html += '<td class="text-right">'+ data['isi'][i].masuk_dalam +'</td>';
			html += '<td class="text-right">'+ data['isi'][i].pakai_dalam +'</td>';
			html += '<td class="text-right">'+ data['isi'][i].hasil_dalam +'</td>';
			html += '<td>'+ data['isi'][i].hargadalam +'</td>';
			html += '<td>'+ data['isi'][i].awal_total +'</td>';
			html += '<td class="text-right">'+ data['isi'][i].masuk_total +'</td>';
			html += '<td class="text-right">'+ data['isi'][i].pakai_total +'</td>';
			html += '<td class="text-right">'+ data['isi'][i].hasil_total +'</td>';
			html += '<td>'+ data['isi'][i].hargatotal +'</td>';
			html += '</tr>';
		}
		html += '</tbody>';
		html += '<tfoot>';
		html += '<tr>';
		if (jenis == 1) {
			html += '<th></th>';
		}
		html += '<th colspan="2">Jumlah</th>';
		html += '<th></th>';
		html += '<th class="text-right">'+ data['totawl_l'] +'</th>';
		html += '<th class="text-right">'+ data['totmsk_l'] +'</th>';
		html += '<th class="text-right">'+ data['totpka_l'] +'</th>';
		html += '<th class="text-right">'+ data['tothsl_l'] +'</th>';
		html += '<th>'+ data['tothargaluar'] +'</th>';
		html += '<th></th>';
		html += '<th class="text-right">'+ data['totawl_d'] +'</th>';
		html += '<th class="text-right">'+ data['totmsk_d'] +'</th>';
		html += '<th class="text-right">'+ data['totpka_d'] +'</th>';
		html += '<th class="text-right">'+ data['tothsl_d'] +'</th>';
		html += '<th>'+ data['tothargadalam'] +'</th>';
		html += '<th class="text-right">'+ data['totjmlawl_j'] +'</th>';
		html += '<th class="text-right">'+ data['totjmlmsk_j'] +'</th>';
		html += '<th class="text-right">'+ data['totjmlpka_j'] +'</th>';
		html += '<th class="text-right">'+ data['totjmlhsl_j'] +'</th>';
		html += '<th>'+ data['totjmlharga'] +'</th>';
		html += '</tr>';
		html += '</tfoot>';
		html += '</table>';
		html += '</div>';
		html += '</div>';

		$('#table_html').html(html);
	}

	function isitembakau(data) {
		let html = '';
		html += '<div class="col-md-12" align="center">';
		html +='<h3 class="text-center">DAFTAR STOK BAHAN BAKU</h3>';
		html +='<h3 class="text-center">PT. BOSS IMAGE NUSANTARA</h3>';
		html +='<h3 class="text-center">'+ data['nametag'] +'</h3>';
		html += '<div class="table-responsive">';
		html += '<table class="table table-bordered table-striped">';
		html += '<thead class="table-dark" align="center">';
		html += '<tr>';
		html += '<th rowspan="2">TANGGAL</th>';
		html += '<th colspan="4">DEKBLAD</th>';
		html += '<th colspan="4">OMBLAD</th>';
		html += '<th colspan="4">FILLER 1</th>';
		html += '<th colspan="4">FILLER 2</th>';
		html += '<th rowspan="2" colspan="2">TOTAL PEMAKAIAN</th>';
		html += '<th rowspan="2" colspan="2">SISA BAHAN BAKU</th>';
		html += '</tr>';
		html += '<tr>';
		html += '<th colspan="2">KELUAR</th>';
		html += '<th colspan="2">SISA</th>';
		html += '<th colspan="2">KELUAR</th>';
		html += '<th colspan="2">SISA</th>';
		html += '<th colspan="2">KELUAR</th>';
		html += '<th colspan="2">SISA</th>';
		html += '<th colspan="2">KELUAR</th>';
		html += '<th colspan="2">SISA</th>';
		html += '</tr>';
		html += '<th>';
		html += '<th>'+ data['namasatuan'] +'</th>';
		html += '<th>JUMLAH</th>';
		html += '<th>'+ data['namasatuan'] +'</th>';
		html += '<th>JUMLAH</th>';
		html += '<th>'+ data['namasatuan'] +'</th>';
		html += '<th>JUMLAH</th>';
		html += '<th>'+ data['namasatuan'] +'</th>';
		html += '<th>JUMLAH</th>';
		html += '<th>'+ data['namasatuan'] +'</th>';
		html += '<th>JUMLAH</th>';
		html += '<th>'+ data['namasatuan'] +'</th>';
		html += '<th>JUMLAH</th>';
		html += '<th>'+ data['namasatuan'] +'</th>';
		html += '<th>JUMLAH</th>';
		html += '<th>'+ data['namasatuan'] +'</th>';
		html += '<th>JUMLAH</th>';
		html += '<th>'+ data['namasatuan'] +'</th>';
		html += '<th>JUMLAH</th>';
		html += '<th>'+ data['namasatuan'] +'</th>';
		html += '<th>JUMLAH</th>';
		html += '</th>';
		html += '</thead>';
		html +='<tbody>';
		for (var i =  0; i < data['count']; i++) {

			$gh =  parseInt(data['dekblad']['sisa'][i]) + parseInt(data['omblad']['sisa'][i]) + parseInt(data['filler2']['sisa'][i]) + parseInt(data['filler1']['sisa'][i]);
			$bar = parseInt(data['dekblad']['keluar'][i]) + parseInt(data['omblad']['keluar'][i]) + parseInt(data['filler2']['keluar'][i]) + parseInt(data['filler1']['keluar'][i]);

			html += '<tr align="right">';
			html +='<td>'+ data['tanggal'][i] +'</td>';
			html +='<td>'+ data['dekblad']['keluar'][i] +'</td>';
			html +='<td>'+ data['dekblad']['hargakeluar'][i] +'</td>';
			html +='<td>'+ data['dekblad']['sisa'][i] +'</td>';
			html +='<td>'+ data['dekblad']['hargasisa'][i] +'</td>';
			html +='<td>'+ data['omblad']['keluar'][i] +'</td>';
			html +='<td>'+ data['omblad']['hargakeluar'][i] +'</td>';
			html +='<td>'+ data['omblad']['sisa'][i] +'</td>';
			html +='<td>'+ data['omblad']['hargasisa'][i] +'</td>';
			html +='<td>'+ data['filler1']['keluar'][i] +'</td>';
			html +='<td>'+ data['filler1']['hargakeluar'][i] +'</td>';
			html +='<td>'+ data['filler1']['sisa'][i] +'</td>';
			html +='<td>'+ data['filler1']['hargasisa'][i] +'</td>';
			html +='<td>'+ data['filler2']['keluar'][i] +'</td>';
			html +='<td>'+ data['filler2']['hargakeluar'][i] +'</td>';
			html +='<td>'+ data['filler2']['sisa'][i] +'</td>';
			html +='<td>'+ data['filler2']['hargasisa'][i] +'</td>';
			html +='<td>'+ data['total_pakai'][i] +'</td>';
			html +='<td>'+ data['total_uangpakai'][i] +'</td>';
			html +='<td>'+ data['total_sisa'][i] +'</td>';
			html +='<td>'+ data['total_uangsisa'][i] +'</td>';
			html += '</tr>';
		}
		html +='</tbody>';
		html +='<tfoot>';
		html += '<tr align="right">';
		html += '<th>TOTAL</th>';

		// html += '<th>'+ format_number(data['rowtotdekbpakai'], '') +'</th>';
		// html += '<th>'+ data['rowtotdekbpakaiuang'] +'</th>';
		// html += '<th>'+ format_number(data['rowtotdekbsisa'], '') +'</th>';
		// html += '<th>'+ data['rowtotdekbsisauang'] +'</th>';
		// html += '<th>'+ format_number(data['rowtotombldpakai'], '') +'</th>';
		// html += '<th>'+ data['rowtotombldpakaiuang'] +'</th>';
		// html += '<th>'+ format_number(data['rowtotombldsisa'], '') +'</th>';
		// html += '<th>'+ data['rowtotombldsisauang'] +'</th>';
		// html += '<th>'+ format_number(data['rowtotfill1pakai'], '') +'</th>';
		// html += '<th>'+ data['rowtotfill1pakaiuang'] +'</th>';
		// html += '<th>'+ format_number(data['rowtotfill1sisa'], '') +'</th>';
		// html += '<th>'+ data['rowtotfill1sisauang'] +'</th>';
		// html += '<th>'+ format_number(data['rowtotfill2pakai'], '') +'</th>';
		// html += '<th>'+ data['rowtotfill2pakaiuang'] +'</th>';
		// html += '<th>'+ format_number(data['rowtotfill2sisa'], '') +'</th>';
		// html += '<th>'+ data['rowtotfill2sisauang'] +'</th>';
		// html += '<th>'+ format_number(data['columtotpakai'], '') +'</th>';
		// html += '<th>'+ data['columtotuangpakai'] +'</th>';
		// html += '<th>'+ format_number(data['columtotsisa'], '') +'</th>';
		// html += '<th>'+ data['columtotuangsisa'] +'</th>';

		html += '<th>'+ data['rowtotdekbpakai'] +'</th>';
		html += '<th>'+ data['rowtotdekbpakaiuang'] +'</th>';
		html += '<th>'+ data['rowtotdekbsisa'] +'</th>';
		html += '<th>'+ data['rowtotdekbsisauang'] +'</th>';
		html += '<th>'+ data['rowtotombldpakai'] +'</th>';
		html += '<th>'+ data['rowtotombldpakaiuang'] +'</th>';
		html += '<th>'+ data['rowtotombldsisa'] +'</th>';
		html += '<th>'+ data['rowtotombldsisauang'] +'</th>';
		html += '<th>'+ data['rowtotfill1pakai'] +'</th>';
		html += '<th>'+ data['rowtotfill1pakaiuang'] +'</th>';
		html += '<th>'+ data['rowtotfill1sisa'] +'</th>';
		html += '<th>'+ data['rowtotfill1sisauang'] +'</th>';
		html += '<th>'+ data['rowtotfill2pakai'] +'</th>';
		html += '<th>'+ data['rowtotfill2pakaiuang'] +'</th>';
		html += '<th>'+ data['rowtotfill2sisa'] +'</th>';
		html += '<th>'+ data['rowtotfill2sisauang'] +'</th>';
		html += '<th>'+ data['columtotpakai'] +'</th>';
		html += '<th>'+ data['columtotuangpakai'] +'</th>';
		html += '<th>'+ data['columtotsisa'] +'</th>';
		html += '<th>'+ data['columtotuangsisa'] +'</th>';
		html += '</tr>';
		html +='</tfoot>';
		html +='</table>';
		html += '</div>;';
		html += '</div>;';
		$('#table_html').html(html);
	}

	$(document).on('change', '#bahan', function(event) {
		let beg = $(this).val();
		$('option[value="2"]', '#jnslap').remove();
		if (beg == 4) {
			$('#tanggalawld').fadeIn();
			$('#jnslap').append('<option value="2">Group</option>');
			$('#jenislaporan').fadeIn();
			$('#satuanberat').hide();
			$('#pjnr').fadeIn();
			$('#tanggalakhrd').fadeIn();
			$('#produknedf').fadeIn();
			$('#sub-hidden').hide();
			$('#kemasan-hidden').fadeIn();
			$('#tahunselect').hide();
		} else if (beg == 1) {
			$('#tanggalawld').fadeIn();
			$('#jnslap').append('<option value="2">Group</option>');
			$('#jenislaporan').fadeIn();
			$('#satuanberat').hide();
			$('#pjnr').fadeIn();
			$('#sub-hidden').hide();
			$('#tanggalakhrd').fadeIn();
			$('#produknedf').fadeIn();
			$('#kemasan-hidden').hide();
			$('#tahunselect').hide();
		} else if (beg == 5) {
			$('#tanggalawld').hide();
			$('#jnslap').append('<option value="2">Group</option>');
			$('#jenislaporan').hide();
			$('#satuanberat').hide();
			$('#pjnr').hide();
			$('#sub-hidden').hide();
			$('#tanggalakhrd').hide();
			$('#produknedf').hide();
			$('#kemasan-hidden').hide();
			$('#tahunselect').hide();
		} else if(beg == 0) {
			$('#tanggalawld').hide();
			$('option[value="2"]', '#jnslap').remove();
			$('#pjnr').hide();
			$('#tanggalakhrd').hide();
			$('#tahunselect').fadeIn();
			$('#jenislaporan').fadeIn();
			$('#satuanberat').fadeIn();
			$('#produknedf').hide();
			$('#sub-hidden').hide();
			$('#kemasan-hidden').hide();
		} else {
			$('#tanggalawld').fadeIn();
			$('#jnslap').append('<option value="2">Group</option>');
			$('#jenislaporan').fadeIn();
			$('#satuanberat').hide();
			$('#pjnr').fadeIn();
			$('#sub-hidden').hide();
			$('#tanggalakhrd').fadeIn();
			$('#produknedf').fadeIn();
			$('#kemasan-hidden').hide();
			$('#tahunselect').hide();
		}
	});

	$(document).on('change', '#produk', function(event) {
		let ke = $(this).val();
		getsub_produk('sub_produk',ke);
	});

	$('#jnslap').change(function(event) {
		let jke = $(this).val();
		let na = $('#bahan').val();
		if (jke == 0) {
			if (na == 5 || na == 0) {
				$('#tahunselect').fadeIn();
				$('#tanggalawld').hide();
				$('#pjnr').hide();
				$('#tanggalakhrd').hide();
			}
		} else {
			$('#tahunselect').hide();
			$('#tanggalawld').fadeIn();
			$('#pjnr').fadeIn();
			$('#tanggalakhrd').fadeIn();
		}
	});

	const connect = {
		5: '',
		0: '/api/Report/laporantembakau/json',
		1: '/api/Report/laporancincin/json',
		2: '/api/Report/laporanstiker/json',
		3: '/api/Report/laporancukai/json',
		4: '/api/Report/laporankemasan/json',
	};

	function render() {
		let bahan = $('#bahan').val();
		let tgl_awl = $('#tgl_awl').val();
		let tgl_akhr = $('#tgl_akhr').val();
		let produk = $('#produk').val();
		let sub_produk = $('#sub_produk').val();
		let kemasan = $('#kemasan').val();
		let satuanberat = $('#satuanberatcont').val();
		let tahun = $('#tahuncont').val();
		let jnslap = $('#jnslap').val();
		let men = connect[bahan];
		$.ajax({
			url: men,
			type: 'POST',
			dataType: 'json',
			data: {
				"_token": token,
				bahan: bahan,
				tgl_awl: tgl_awl,
				tgl_akhr: tgl_akhr,
				produk: produk,
				sub_produk: sub_produk,
				kemasan: kemasan,
				satuanberat: satuanberat,
				tahun: tahun,
				jnslap: jnslap,
			},
			beforeSend: function() {
				isProcessing = true;
				$('#loading').fadeIn();
			},
			success: function(data) {
				if (bahan == 1 || bahan == 4) {
					isicin(data,tgl_awl,tgl_akhr,jnslap);
				} else if(bahan == 3) {
					isisam(data,tgl_awl,tgl_akhr,jnslap);
				} else if(bahan == 2) {
					isisti(data,tgl_awl,tgl_akhr,jnslap);
				} else if(bahan == 0) {
					isitembakau(data)
				} else {
				}
				$('#loading').hide();
				isProcessing = false;
				$('#print_html').fadeIn();
			}
		});
	}
</script>