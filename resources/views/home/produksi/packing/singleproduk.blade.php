<div class="row" id="load_single">
	<div class="form-group col-md-12">
		<label class="control-label">Kemasan</label>
		<input type="text" class="form-control" id="kemasan" placeholder="Kemasan" readonly>

		<input type="hidden" id="idcukai">
		<input type="hidden" id="idqc">
		<input type="hidden" id="idkemasan">
		<input type="hidden" id="idpacking">
		<input type="hidden" id="idstokemasan">
		<input type="hidden" id="idcincin">
		<input type="hidden" id="idstiker">
	</div>
	<div class="form-group col-md-4">
		<label class="control-label">Stock Kemasan</label>
		<input type="number" class="form-control" id="stockkemasan" placeholder="Stock Kemasan" readonly>
	</div>
	<div class="form-group col-md-4">
		<label class="control-label">Stock Cukai</label>
		<input type="number" class="form-control" id="stockcukai" placeholder="Stock Cukai" readonly>
	</div>
	<div class="form-group col-md-4">
		<label class="control-label">Stock Cincin</label>
		<input type="number" class="form-control" id="stockcincin" placeholder="Stock Cincin" readonly>
	</div>
	<div class="form-group col-md-12">
		<label class="control-label">Stock Stiker</label>
		<div class="row">
			<div class="col-md-6">
				*luar
				<input type="number" class="form-control" id="stockluarsti" placeholder="Stock Stiker Luar" readonly>
			</div>
			<div class="col-md-6">
				*dalam
				<input type="number" class="form-control" id="stockdalamsti" placeholder="Stock Stiker Dalam" readonly>
			</div>
		</div>
	</div>
	<div class="form-group col-md-12" id="batangsingle">
		<label class="control-label">Batang Quality Control</label>
		<input type="number" class="form-control" id="batangqc" placeholder="Jml Batang Quality Control" readonly>
	</div>
	<div class="form-group col-md-12">
		<label class="control-label">Stock Awal Packing</label>
		<input type="number" class="form-control" id="stockawal" placeholder="Stock Awal Packing" readonly>
	</div>
	<div class="form-group col-md-5">
		<label class="control-label">Packing</label>
		<input type="number" class="form-control" id="jmlpacking" placeholder="JML Packing">
	</div>
	<div class="form-group col-md-2" align="center">
		<br>
		<label class="control-label">X</label>
	</div>
	<div class="form-group col-md-5">
		<label class="control-label">Isi/Kemasan</label>
		<input type="text" class="form-control" id="isikemasan" placeholder="Isi Kemasan" readonly>
	</div>
	<div class="form-group col-md-6">
		<label class="control-label">Terpakai Cukai Lama</label>
		<input type="number" style="display: none" id="masuksame" value="0">
		<input type="number" class="form-control" id="terpakailama" placeholder="Cukai Terpakai Lama">
	</div>
	<div class="form-group col-md-6">
		<label class="control-label">Terpakai Cukai Baru</label>
		<input type="number" class="form-control" id="terpakaibaru" placeholder="Cukai Terpakai Baru">
	</div>
	<div class="form-group col-md-6">
		<label class="control-label">Terpakai Stiker Luar</label>
		<input type="number" class="form-control" id="terpakailuar" placeholder="Stiker Terpakai Luar">
	</div>
	<div class="form-group col-md-6">
		<label class="control-label">Terpakai Stiker Dalam</label>
		<input type="number" class="form-control" id="terpakaidalam" placeholder="Stiker Terpakai Baru">
	</div>
	<div class="form-group col-md-6">
		<label class="control-label">Terpakai Kemasan</label>
		<input type="number" class="form-control" id="terpakaikemasan" placeholder="Terpakai Kemasan">
	</div>
	<div class="form-group col-md-6">
		<label class="control-label">Terpakai Cincin</label>
		<input type="number" class="form-control" id="terpakaicincin" placeholder="Terpakai Cincin">
	</div>
	<div class="form-group col-md-12">
		<label class="control-label">Keterangan</label>
		<textarea class="form-control" id="ket" placeholder="Keterangan"></textarea>
	</div>
</div>