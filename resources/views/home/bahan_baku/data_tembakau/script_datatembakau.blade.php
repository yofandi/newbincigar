<script>
	$.extend(true, $.fn.datetimepicker.defaults, {
		icons: {
			time: 'far fa-clock',
			date: 'far fa-calendar',
			up: 'fas fa-arrow-up',
			down: 'fas fa-arrow-down',
			previous: 'fas fa-chevron-left',
			next: 'fas fa-chevron-right',
			today: 'fas fa-calendar-check',
			clear: 'far fa-trash-alt',
			close: 'far fa-times-circle'
		}
	});

	$('.spinner_nekr').hide();
	$('#loadingdelete').hide();

	const jenis = {!! json_encode($jenis) !!}
	jenis.unshift({id_jenis: '',kode_jenis: '',jenis: '',satuan_berat: '' , harga_tembakau: ''});


	const berat = {!! json_encode($berat) !!};
	const kategori = {!! json_encode($kategori) !!};
	kategori.unshift({id_kategori: '',kategori: ''});

	const asal = ['Tanam Sendiri','Beli Eksternal'];

	function format_number(number, prefix, thousand_separator, decimal_separator)
	{
		var 	thousand_separator = thousand_separator || ',',
		decimal_separator = decimal_separator || '.',
		regex		= new RegExp('[^' + decimal_separator + '\\d]', 'g'),
		number_string = number.toString().replace(regex, ''),
		split	  = number_string.split(decimal_separator),
		rest 	  = split[0].length % 3,
		result 	  = split[0].substr(0, rest),
		thousands = split[0].substr(rest).match(/\d{3}/g);

		if (thousands) {
			separator = rest ? thousand_separator : '';
			result += separator + thousands.join(thousand_separator);
		}
		result = split[1] != undefined ? result + decimal_separator + split[1] : result;
		return prefix == undefined ? result : (result ? prefix + result : '');
	};

	var table = $('#table_tembakau').DataTable({
		"lengthMenu": [
		[3,5,10, 25, 50, 100, 150, 200, -1],
		[3,5,10, 25, 50, 100, 150, 200, "All"]
		],
		"order": [
		[0, 'desc']
		],
		processing: true,
		serverSide: true,
		ajax: '/data_tembakau/json',
		columns: [
		{ data: 'DT_RowIndex', name: 'DT_RowIndex' },
		{ data: 'tanggal_in', name: 'tanggal_in' },
		// { data: 'asal', name: 'asal' },
		{ data: 'kode_jenis', name: 'kode_jenis' },
		{ data: 'kategori', name: 'kategori' },
		{ data: 'stock_masuk', name: 'stock_masuk' },
		{ data: 'diterima', name: 'diterima' },
		{ data: 'diproduksi', name: 'diproduksi' },
		{ data: 'hari_ini', name: 'hari_ini' },
		{ data: 'ket_his', name: 'ketket_his' },
		{
			data: null,
			"bSortable": false,
			mRender: function (o) { 
				return '<button type="button" class="btn btn-info btn-rounded btn-xs" id="update-tembakau" data-title="Update Stock Tembakau | ID: '+ o.id +'" data-link="/data_tembakau/upp/'+ o.id +'/'+ o.diterima +'/'+ o.diproduksi +'" data-tanggal_in="'+ o.tanggal_in +'" data-asal="'+ o.asal +'" data-hargaid="'+ o.satuan_harga_id +'" data-jenis="'+ o.jenis_id_jenis +'" data-kategori="'+ o.kategori_id_kategori +'" data-awlstok="'+ o.awlstok +'" data-masuk="'+ o.stock_masuk +'" data-diterima="'+ o.diterima +'" data-diproduksi="'+ o.diproduksi +'" data-hari_ini="'+ o.hari_ini +'" data-ket_his="'+ o.ket_his +'"><i class="mdi mdi-dots-horizontal"></i></button><button type="button" class="btn btn-danger btn-rounded btn-xs" id="delete-tembakau" data-link="/data_tembakau/del/'+ o.id +'/'+ o.diterima +'/'+ o.diproduksi +'/'+ o.awlstok +'" data-id="'+ o.id +'" data-jenis="'+ o.jenis_id_jenis +'" data-kategori="'+ o.kategori_id_kategori +'"><i class="fa fa-trash"></i></button>'; }
			},
			],
			"displayLength": 10,
		});
	$('#table_tembakau tbody').on('click', 'tr.group', function() {
		var currentOrder = table.order()[0];
		if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
			table.order([2, 'desc']).draw();
		} else {
			table.order([2, 'asc']).draw();
		}
	});
	$('#table_tembakau tbody').on('click', 'tr.group', function() {
		var currentOrder = table.order()[0];
		if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
			table.order([2, 'desc']).draw();
		} else {
			table.order([2, 'asc']).draw();
		}
	});

	function ljenis(position,like = '') {
		let opt = '';
		for (var i = 0; i < jenis.length; i++) {
			opt += '<option value="'+ jenis[i].id_jenis +'"';
			if (jenis[i].id_jenis == like) { opt += 'selected'; }
			opt += '>'+ jenis[i].kode_jenis +' - '+ jenis[i].jenis +'</option>';
		}
		$('#' + position).html(opt);
	}

	function lkategori(position,like = '') {
		let opt = '';
		for (var i = 0; i < kategori.length; i++) {
			opt += '<option value="'+ kategori[i].id_kategori +'"';
			if (kategori[i].id_kategori == like) { opt += 'selected'; }
			opt += '>'+ kategori[i].kategori +'</option>';
		}
		$('#' + position).html(opt);
	}

	function lasal(position,like = '') {
		let opt = '<option value=""></option>';
		for (var i = 0; i < asal.length; i++) {
			opt += '<option value="'+ asal[i] +'"';
			if (asal[i] == like) { opt += 'selected'; }
			opt += '>'+ asal[i] +'</option>';
		}
		$('#' + position).html(opt);
	}

	function sendval(changed,position) {
		$('#' + changed).change(function(event) {
			let val = $(this).val();
			$('#' + position).val(val);
		});
	}

	function setprice(val,base,barde,id = '') {
		$.ajax({
			url: '/api/data_tembakau/setharga',
			type: 'POST',
			dataType: 'json',
			data: {
				"_token": $("meta[name='csrf-token']").attr("content"),
				id: id,
				val: val,
				jenis: base,
				kategori: barde
			},
		})
		.done(function(data) {
			if (val == 0) {
				$('#name-sat').text(berat[data.satuan_berat]);
				$('#harga-tembakauset').val(format_number(data.harga_tembakau, ''));
				$('#awlstok').val(data.stock_awal);
			} else {
				$('#name-sat').text(berat[data.satuan_berat]);
				$('#harga-tembakauset').val(format_number(data.harga_tembakau, ''));
			}
		});
	}

	function Refresh() {
		$('#table_tembakau').DataTable().ajax.reload();
	}

	function clikthisshowmoney() {
		setprice(0,$('#jenis_in').val(),$('#kategori_in').val());
	}

	function save_method() {
		let token = $("meta[name='csrf-token']").attr("content");
		let link = $('#form-tembakau').attr('action');
		let tanggal_in = $('#tanggal_in').val();
		let jenis = $('#jenis_in').val();
		let kategori = $('#kategori_in').val();
		let asal = $('#asal_in').val();
		let st_masuk = $('#st_masuk').val();
		let st_terima = $('#st_terima').val();
		let st_produksi = $('#st_produksi').val();
		let keterangan = $('#keterangan').val();
		let awlstok = $('#awlstok').val();

		$.ajax({
			url: link,
			type: 'POST',
			dataType: 'json',
			data: {
				"_token": token,
				tanggal_in: tanggal_in,
				jenis: jenis,
				kategori: kategori,
				asal: asal,
				st_masuk: st_masuk,
				st_terima: st_terima,
				st_produksi: st_produksi,
				keterangan: keterangan,
				awlstok: awlstok,
			},
			beforeSend: function() {
				isProcessing = true;
				$('.spinner_nekr').fadeIn();
			},
			success: function(data) {
				$('.spinner_nekr').hide();
				isProcessing = false;
				alert(data);
				$('#table_tembakau').DataTable().ajax.reload();
				$('#modal-tembakau').modal('toggle');
			}
		});
		
	}

	$(document).on('click', '#tambah-tembakau', function(event) {
		let clik = $(this);
		$('.modal-header #title').text('Tambah Stock Tembakau');
		$('#form-tembakau').attr('action', clik.data('link'));
		$('#form-tembakau').find('input[type=text]').val('');
		$('#form-tembakau').find('input[type=number]').val(0);
		$('#form-tembakau').find('select').val('');
		$('#form-tembakau').find('textarea').val('');

		ljenis("jenis");
		lkategori("kategori");
		lasal("asal");

		$('#jenis').removeAttr('disabled');
		$('#kategori').removeAttr('disabled');
		$('#bittonval').removeAttr('disabled');
		
		sendval('jenis','jenis_in');
		$('#jenis').change(function(event) {
			let jd = $(this).val();	
		});

		sendval('kategori','kategori_in');
		sendval('asal','asal_in');

		$("#tanggal_in").datetimepicker({
			format: 'YYYY-MM-DD HH:mm:ss',
		}).data("DateTimePicker").date(new Date());
		$('#modal-tembakau').modal('show');
	});

	$(document).on('click', '#update-tembakau', function(event) {
		let click = $(this);
		$('.modal-header #title').text(click.data('title'));
		$('#form-tembakau').attr('action', click.data('link'));
		$('#form-tembakau').find('input[type=text]').val('');
		$('#form-tembakau').find('input[type=number]').val(0);
		$('#form-tembakau').find('select').val('');
		$('#form-tembakau').find('textarea').val('');
		$('#st_masuk').val(click.data('masuk'));
		$('#st_terima').val(click.data('diterima'));
		$('#st_produksi').val(click.data('diproduksi'));
		$('#keterangan').val(click.data('ket_his'));
		$('#jenis_in').val(click.data('jenis'));
		$('#kategori_in').val(click.data('kategori'));
		$('#asal_in').val(click.data('asal'));
		$('#awlstok').val(click.data('awlstok'));

		ljenis("jenis",click.data('jenis'));
		setprice(1,click.data('jenis'),click.data('kategori'),click.data('hargaid'));

		lkategori("kategori",click.data('kategori'));
		lasal("asal",click.data('asal'));

		$('#jenis').attr('disabled','disabled');
		$('#kategori').attr('disabled','disabled');
		$('#bittonval').attr('disabled', 'disabled');

		$("#tanggal_in").datetimepicker({
			format: 'YYYY-MM-DD HH:mm:ss',
		}).data("DateTimePicker").date(click.data('tanggal_in'));
		$('#modal-tembakau').modal('show');
	});

	$(document).on('click', '#delete-tembakau', function(event) {
		let dse = confirm('Apakah anda yakin ingin menghapus data ini ?');
		if (dse) {
			let id = $(this).data("id");
			let link = $(this).data("link");
			let token = $("meta[name='csrf-token']").attr("content");
			let jenis = $(this).data('jenis');
			let kategori = $(this).data('kategori');
			$.ajax(
			{
				url: link,
				type: 'DELETE',
				data: {
					"_token": token,
					jenis: jenis,
					kategori: kategori
				},
				beforeSend: function() { 
					isProcessing = true;
					$('#loadingdelete').fadeIn();
				},
				success: function (data){
					$('#loadingdelete').hide();
					isProcessing = false;
					alert(data);
					$('#table_tembakau').DataTable().ajax.reload();
				}
			});
		}
	});
</script>