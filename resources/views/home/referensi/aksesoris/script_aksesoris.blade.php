<script>
	let token = $("meta[name='csrf-token']").attr("content");
	$('.spinner_nekr').hide();
	$('#loadingdelete').hide();
	const produk = {!! json_encode($produk) !!}
	produk.unshift({id_produk: '',kode_produk: '',produk: ''});

	$(document).on('change', '#produk', function(event) {
		let kje = $( this ).val();
		lsubproduk('subproduk',kje);			
	});

	function lproduk(position,like = "") {
		let opt = '';
		for (var i =  0; i < produk.length; i++) {
			opt += '<option value="'+ produk[i].id_produk +'"';
			if (produk[i].id_produk == like) { opt += 'selected'; }
			opt +='>'+ produk[i].kode_produk +' - '+ produk[i].produk +'</option>';
		}
		$('#' + position).html(opt);
	}

	function lsubproduk(position,based,like = "") {
		$.ajax({
			url: '/api/Report/getsubproduk/json',
			type: 'POST',
			dataType: 'json',
			data: {
				"_token": token,
				produk: based
			},
		})
		.done(function(data) {
			let opt = '<option value=""></option>';
			for (var i = 0; i < data.length; i++) {
				opt += '<option value="'+ data[i].id_sub_produk +'"';
				if (data[i].id_sub_produk == like) { opt += 'selected'; }
				opt += '>'+ data[i].sub_kode +' - '+ data[i].sub_produk +'</option>';
			}
			$('#' + position).html(opt);
		});
	}

	function aksesoris(position,like = "") {
		$.ajax({
			url: '/api/aksesoris',
			type: 'GET',
			dataType: 'json',
			data: {
				"_token": token,
				kode_aksesoris: '',
				aksesoris: '',
			},
		})
		.done(function(data) {
			let opt = '<option value=""></option>';
			for (var i = 0; i < data.length; i++) {
				opt += '<option value="'+ data[i].id +'"';
				if (data[i].id == like) { opt += 'selected'; }
				opt += '>'+ data[i].kode_aksesoris +' - '+ data[i].aksesoris +'</option>';
			}
			$('#' + position).html(opt);
		});
	}

	jsGrid.setDefaults({
		tableClass: "jsgrid-table table table-striped table-hover"
	});
	jsGrid.setDefaults("text", {
		_createTextBox: function() {
			return $("<input>").attr("type", "text").attr("class", "form-control input-sm")
		}
	});
	jsGrid.setDefaults("number", {
		_createTextBox: function() {
			return $("<input>").attr("type", "number").attr("class", "form-control input-sm")
		}
	});
	jsGrid.setDefaults("textarea", {
		_createTextBox: function() {
			return $("<input>").attr("type", "textarea").attr("class", "form-control")
		}
	});
	jsGrid.setDefaults("control", {
		_createGridButton: function(cls, tooltip, clickHandler) {
			var grid = this._grid;
			return $("<button>").addClass(this.buttonClass).addClass(cls).attr({
				type: "button",
				title: tooltip
			}).on("click", function(e) {
				clickHandler(grid, e)
			})
		}
	});
	jsGrid.setDefaults("select", {
		_createSelect: function() {
			var $result = $("<select>").attr("class", "form-control input-sm"),
			valueField = this.valueField,
			textField = this.textField,
			selectedIndex = this.selectedIndex;
			return $.each(this.items, function(index, item) {
				var value = valueField ? item[valueField] : index,
				text = textField ? item[textField] : item,
				$option = $("<option>").attr("value", value).text(text).appendTo($result);
				$option.prop("selected", selectedIndex === index)
			}), $result
		}
	});
	$('#grid_aksesoris').jsGrid({
		height: "500px",
		width: "100%",
		filtering: true,
		filterable:true,
		editing: true,
		inserting: true,
		sorting: true,
		paging: true,
		autoload: true,
		pageSize: 15,
		pageButtonCount: 5,
		deleteConfirm: "Apakah anda yakin ingin menghapus data ini ?",
		controller: {
			loadData: function(filter){
				return $.ajax({
					type: "GET",
					dataType: 'json',
					url: '/api/aksesoris',
					data: filter
				});
			},
			insertItem: function(item){
				item._token = $('meta[name="csrf-token"]').attr('content');
				return $.ajax({
					type: "POST",
					url: '/api/aksesoris/post',
					data:item
				});
			},
			updateItem: function(item){
				item._token = $('meta[name="csrf-token"]').attr('content');
				return $.ajax({
					type: "PUT",
					url: '/api/aksesoris/put',
					data: item
				});
			},
			deleteItem: function(item){
				item._token = $('meta[name="csrf-token"]').attr('content');
				return $.ajax({
					type: "DELETE",
					url: '/api/aksesoris/del',
					data: item
				});
			},
		},

		fields: [
		{
			name: "id",
			title: "ID",
			type: "hidden",
			css: 'hide',
			width: 50
		},
		{
			name: "kode_aksesoris",
			title: "Kode Aksesoris",
			type: "text", 
			width: 50
		},
		{
			name: "aksesoris",
			title: "Aksesoris",
			type: "text", 
			width: 50
		},
		{
			type: "control"
		}
		]
	});
	
	function Refresh() {
		$("#grid_aksesoris").jsGrid("loadData");
	}

	var table = $('#table-hassubs').DataTable({
		"lengthMenu": [
		[3,5,10, 25, 50, 100, 150, 200, -1],
		[3,5,10, 25, 50, 100, 150, 200, "All"]
		],
		"order": [
		[0, 'desc']
		],
		processing: true,
		serverSide: true,
		ajax: '/api/aksesoris/has/json',
		columns: [
		{ data: 'DT_RowIndex', name: 'DT_RowIndex' },
		{ data: 'sub_kode', name: 'sub_kode'},
		{ data: 'sub_produk', name: 'sub_produk'},
		{ data: 'aksesoris', name: 'aksesoris'},
		{	
			data: null,
			"bSortable": false,
			mRender: function(o) {
				$button = '<button type="button" class="btn btn-info btn-rounded btn-xs" id="update-hassubproduk" data-link="/api/has_aksesoris/upp/'+ o.idaksesoris +'" data-id="'+ o.idaksesoris +'" data-kode_spesial="'+ o.kode_spesial +'" data-produk_id="'+ o.produk_id_produk +'" data-sub_produk_id="'+ o.id_sub_produk +'" data-aksesoris_id="'+ o.aksesoris_id +'"><i class="mdi mdi-dots-horizontal"></i></button>';
				$button += '<button type="button" class="btn btn-danger btn-rounded btn-xs" id="delete-hassubproduk" data-link="/api/has_aksesoris/del/'+ o.idaksesoris +'"><i class="fa fa-trash"></i></button>';
				return $button; 
			}
		},
		],
		"displayLength": 10,
	});
	$('#table-hassubs').on('click', 'tr.group', function() {
		var currentOrder = table.order()[0];
		if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
			table.order([2, 'desc']).draw();
		} else {
			table.order([2, 'asc']).draw();
		}
	});
	$('#table-hassubs').on('click', 'tr.group', function() {
		var currentOrder = table.order()[0];
		if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
			table.order([2, 'desc']).draw();
		} else {
			table.order([2, 'asc']).draw();
		}
	});

	$(document).on('click', '#tambah-hassub', function(event) {
		let clik = $(this);
		$('#form-hassubpro').find('select').val('');
		$('#form-hassubpro').find('input[type=text]').val('');

		$('#showhidd').fadeIn();
		$('.modal-header #title').text('Tambah Aksesoris has Subproduk');		 
		$('#form-hassubpro').attr('action', clik.data('link'));

		lproduk('produk');
		aksesoris('aksesoris');

		$('#modal-hassubpro').modal('show');
	});

	$(document).on('click', '#update-hassubproduk', function(event) {
		let clik = $(this);
		$('#form-hassubpro').find('select').val('');
		$('#form-hassubpro').find('input[type=text]').val('');

		$('#showhidd').fadeIn();
		$('.modal-header #title').text('Update Aksesoris has Subproduk: ID '+clik.data('id'));		 
		$('#form-hassubpro').attr('action', clik.data('link'));

		$('kode_relasi').val(clik.data('kode_spesial'));
		lproduk('produk',clik.data('produk_id'));
		lsubproduk('subproduk',clik.data('produk_id'),clik.data('sub_produk_id'));
		aksesoris('aksesoris',clik.data('aksesoris_id'));

		$('#modal-hassubpro').modal('show');
	});
	$(document).on('click', '#refreshit', function(event) {
		$('#table-hassubs').DataTable().ajax.reload();
	});

	$(document).on('click', '#submit_hasaksesoris', function(event) {
		let link = $('#form-hassubpro').attr('action');
		var mydata = $("form#form-hassubpro").serialize();
		$.ajax({
			headers: {
				'X-CSRF-TOKEN': token
			},
			url: link,
			type: 'POST',
			dataType: 'json',
			data: mydata,
			beforeSend: function() {
				isProcessing = true;
				$('.spinner_nekr').fadeIn();
			},
			success: function(data) {
				$('.spinner_nekr').hide();
				isProcessing = false;
				alert(data);
				$('#table-hassubs').DataTable().ajax.reload();
				$('#modal-hassubpro').modal('toggle');
			}
		});

	});

	$(document).on('click', '#delete-hassubproduk', function(event) {
		let dbhe = confirm('Apakah anda yakin ingin menghapus data ini ?');
		if (dbhe) {
			let link = $( this ).data('link');
			$.ajax({

				headers: {
					'X-CSRF-TOKEN': token
				},
				url: link,
				type: 'DELETE',
				dataType: 'json',
				beforeSend: function() {
					isProcessing = true;
					$('#loadingdelete').fadeIn();
				},
				success: function(data) {
					$('#loadingdelete').hide();
					isProcessing = false;
					alert(data);
					$('#table-hassubs').DataTable().ajax.reload();
				}
			});
		}
	});
</script>