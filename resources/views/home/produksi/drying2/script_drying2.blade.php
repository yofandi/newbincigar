<script>
	$('#stock_showitdry2').hide();
	$('#loadingdeletedry2').hide();
	$('.spinner_nekrdry2').hide();

	var tabledry2 = $('#table-drying2').DataTable({
		"lengthMenu": [
		[3,5,10, 25, 50, 100, 150, 200, -1],
		[3,5,10, 25, 50, 100, 150, 200, "All"]
		],
		"order": [
		[0, 'desc']
		],
		processing: true,
		serverSide: true,
		ajax: '/datadrying2/json',
		columns: [
		{ data: 'DT_RowIndex', name: 'DT_RowIndex' },
		{ data: 'tanggal_dry2', name: 'tanggal_dry2' },
		{ data: 'produk', name: 'produk' },
		{ data: 'jenis', name: 'jenis' },
		{ data: 'lama_dry2', name: 'lama_dry2' },
		{ data: 'awlbatang', name: 'awlbatang' },
		{ data: 'tambah_dry2', name: 'tambah_dry2' },
		{ data: 'hasil_akhirdry2', name: 'hasil_akhirdry2' },
		{ data: 'keterangan_dry2', name: 'keterangan_dry2' },
		{
			data: null,
			"bSortable": false,
			mRender: function (o) { 
				return '<button type="button" class="btn btn-info btn-rounded btn-xs" id="update-drying2" data-title="Update Drying2 | ID: '+ o.id +'" data-link="/datadrying2/upp/'+ o.id +'/'+ o.tambah_dry2 +'" data-tanggal_drying2="'+ o.tanggal_dry2 +'" data-jenis="'+ o.id_jenis +'" data-produk="'+ o.id_produk +'" data-durasi="'+ o.lama_dry2 +'" data-ket="'+ o.keterangan_dry2 +'" data-stockbatang="'+ o.awlbatang +'" data-batang="'+ o.tambah_dry2 +'" data-idfrezzer="'+ o.stock_frezer_id +'" data-iddrying2="'+ o.stock_drying2_id +'"><i class="mdi mdi-dots-horizontal"></i></button><button type="button" class="btn btn-danger btn-rounded btn-xs" id="delete-drying2" data-link="/datadrying2/del/'+ o.id +'/'+ o.tambah_dry2 +'" data-idfrezzer="'+ o.stock_frezer_id +'" data-iddrying2="'+ o.stock_drying2_id +'"><i class="fa fa-trash"></i></button>'; }
			},
			],
			"displayLength": 10,
		});
	$('#table-drying2 tbody').on('click', 'tr.group', function() {
		var currentOrder = tabledry2.order()[0];
		if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
			tabledry2.order([2, 'desc']).draw();
		} else {
			tabledry2.order([2, 'asc']).draw();
		}
	});
	$('#table-drying2 tbody').on('click', 'tr.group', function() {
		var currentOrder = tabledry2.order()[0];
		if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
			tabledry2.order([2, 'desc']).draw();
		} else {
			tabledry2.order([2, 'asc']).draw();
		}
	});

	function Refresh_drying2() {
		$('#table-drying2').DataTable().ajax.reload();
	}

	function getdatasearchdry2(position1) {
		let jenis = $('#jenis_indry2').val();
		let produk = $('#produk_indry2').val();

		$.ajax({
			url: '/datadrying2/getsearchfill',
			type: 'POST',
			dataType: 'json',
			data: {
				"_token": token,
				produk: produk,
				jenis: jenis
			},
		})
		.done(function(data) {
			$('#iddrying2').val(data.iddrying2);
			$('#idfrezzerdry2').val(data.idfrezzer);
			$('#' + position1).val(data.jml_fre);
		});
		
	}

	function seacrh_datadry2() {
		getdatasearchdry2('batangfre');
	}

	function save_methoddry2() {
		let link = $('#form-drying2').attr('action');
		let iddrying2 = $('#iddrying2').val();
		let idfrezzer = $('#idfrezzerdry2').val();
		let idjnpro = $('#idjnprodry2').val();
		let tanggal_drying2 = $('#tanggal_drying2').val();
		let jenis_in = $('#jenis_indry2').val();
		let produk_in = $('#produk_indry2').val();
		let stockbatang = $('#batangfre').val();
		let batangjml = $('#batangjmldry2').val();
		let durasi = $('#durasidry2').val();
		let ket = $('#ketdry2').val();
		$.ajax({
			url: link,
			type: 'POST',
			dataType: 'json',
			data: {
				"_token": token,
				tanggal_drying2: tanggal_drying2,
				jenis: jenis_in,
				produk: produk_in,
				durasi:durasi,
				stockbatang: stockbatang,
				batangjml: batangjml,
				ket:ket,
				idfrezzer: idfrezzer,
				iddrying2: iddrying2,
				idjnpro:idjnpro
			},
			beforeSend: function() {
				isProcessing = true;
				$('.spinner_nekrdry2').fadeIn();
			},
			success: function(data) {
				$('.spinner_nekrdry2').hide();
				isProcessing = false;
				alert(data);
				Refresh_drying2();
				$('#modal-drying2').modal('toggle');
			}
		});
		
	}

	$(document).on('click', '#tambah-drying2', function(event) {
		let clik = $(this);
		$('.modal-header #title').text('Tambah drying2');
		$('#form-drying2').attr('action', clik.data('link'));
		$('#form-drying2').find('input').val('');
		$('#form-drying2').find('select').val('');
		$('#form-drying2').find('textarea').val('');
		$("#tanggal_drying2").datetimepicker({
			format: 'YYYY-MM-DD HH:mm:ss',
		}).data("DateTimePicker").date(new Date());
		// $('#durasidry2').mdtimepicker({format: 'hh:mm'});
		$('#stock_showitdry2').fadeIn();
		$('#jenisdry2').removeAttr('disabled');
		$('#produkdry2').removeAttr('disabled');
		ljenis('jenisdry2');
		$(document).on('change', '#jenisdry2', function(event) {
			let jam = $(this).val();
			$('#jenis_indry2').val(jam);
			lproduk('produkdry2',jam);
			$(document).on('change', '#produkdry2', function(event) {
				let pwe = $(this).val();
				$('#produk_indry2').val(pwe);
				let kk = $(this).find(':selected').data('id');
				$('#idjnprodry2').val(kk);
			});
		});
		$('#modal-drying2').modal('show');
	});

	$(document).on('click', '#update-drying2', function(event) {
		let click = $(this);
		$('.modal-header #title').text(click.data('title'));
		$('#form-drying2').attr('action', click.data('link'));
		$('#form-drying2').find('input').val('');
		$('#form-drying2').find('select').val('');
		$('#form-drying2').find('textarea').val('');
		$('#stock_showitdry2').hide();
		$('#jenisdry2').attr('disabled','disabled');
		$('#produkdry2').attr('disabled','disabled');
		ljenis('jenisdry2',click.data('jenis'));
		lproduk('produkdry2',click.data('jenis'),click.data('produk'));

		$("#tanggal_drying2").datetimepicker({
			format: 'YYYY-MM-DD HH:mm:ss',
		}).data("DateTimePicker").date(click.data('tanggal_drying2'));
		// $('#durasidry2').mdtimepicker({format: 'hh:mm'});
		$('#jenis_indry2').val(click.data('jenis'));
		$('#produk_indry2').val(click.data('produk'));
		$('#batangfre').val(click.data('stockbatang'));
		$('#batangjmldry2').val(click.data('batang'));
		$('#iddrying2').val(click.data('iddrying2'));
		$('#idfrezzerdry2').val(click.data('idfrezzer'));
		$('#durasidry2').val(click.data('durasi'));
		$('#ketdry2').val(click.data('ket'));
		$('#modal-drying2').modal('show');
	});

	$(document).on('click', '#delete-drying2', function(event) {
		let hjs = confirm('Apakah anda yakin ingin menghapus data ini ?');
		if (hjs) {
			let link = $(this).data("link");
			let idfrezzer = $(this).data('idfrezzer');
			let iddrying2 = $(this).data('iddrying2');
			$.ajax(
			{
				url: link,
				type: 'DELETE',
				data: {
					"_token": token,
					idfrezzer: idfrezzer,
					iddrying2: iddrying2,
				},
				beforeSend: function() {
					$('#loadingdeletedry2').fadeIn();
					isProcessing = true;
				},
				success: function (data){
					$('#loadingdeletedry2').hide();
					isProcessing = false;
					alert(data);
					Refresh_drying2();
				}
			});
		}
	});
</script>