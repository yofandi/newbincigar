<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use DataTables;
use Auth;
use App\Frezer;
use App\Stock_drying1;
use App\Stock_frezer;

use App\Http\Controllers\Back_frezzer;

class Frezzercont extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->produksi = $res = new Back_frezzer;
    }
    
    public function searchweiprofre(Request $request)
    {
        $fert = $this->produksi->sprofre($request);
        echo json_encode($fert);
    }

    public function getdatafre()
    {
        $query = DB::table('frezer')
        ->select('frezer.*','jenis.id_jenis','produk.id_produk','jenis.jenis','produk.produk')
        ->join('stock_frezer','frezer.stock_frezer_id','=','stock_frezer.id')
        ->join('jenis','stock_frezer.jenis_has_produk_id_jenis','=','jenis.id_jenis')
        ->join('produk','stock_frezer.jenis_has_produk_id_produk','=','produk.id_produk')
        ->get();
        return Datatables::of($query)
        ->addIndexColumn()
        ->addColumn('awlbatang', function ($query) {
            $awl = $query->hasil_akhirfre + $query->tambah_fre;
            return $awl;
        })
        ->make(true);
    }

    public function datafrezer()
    {
        $data['jenis'] = DB::table('jenis')->select('id_jenis','jenis')->get();
        return view('home.produksi.freezer.datafreezer',$data);
    }

    public function addfrezer(Request $request)
    {
        DB::beginTransaction();

        $qq = $this->produksi->stockfre($request);

        $data = [
            'tambah_fre' => $request->batangjml,
            'hasil_akhirfre' => $qq['jml_dry1'],
            'lama_fre' => $request->durasi,
            'keterengan_fre' => $request->ket,
            'tanggal_fre' => $request->tanggal_frezzer,
            'stock_frezer_id' => $qq['idfrezzer'],
            'stock_drying1_id' => $qq['iddrying1'],
            'author_session' => Auth::user()->id,
            'created_at' => date('Y-m-d H:i:s'),
            'log' => "INSERT"
        ];
        $query1 = Frezer::insert($data);
        if ($query1) {
            $alert = 'Frezer berhasil ditambahkan!';
        } else {
            $alert = 'Frezer gagal ditambahkan!';
        }
        echo json_encode($alert);
        DB::commit();
    }

    public function uppfrezer(Request $request,$id,$batang)
    {
        DB::beginTransaction();
        $jwty = $request->stockbatang - $request->batangjml;
        $this->produksi->ifuppstockfre($request,$batang);

        $data = [
            'tambah_fre' => $request->batangjml,
            'hasil_akhirfre' => $jwty,
            'lama_fre' => $request->durasi,
            'keterengan_fre' => $request->ket,
            'tanggal_fre' => $request->tanggal_frezzer,
            'author_session' => Auth::user()->id,
            'log' => "UPDATE"
        ];
        $query1 = Frezer::where('id',$id)->update($data);
        if ($query1) {
            $this->produksi->loop_upfre($request,$id);
            $alert = 'Frezer ID: '.$id.' berhasil diupdate!';
        } else {
            $alert = 'Frezer ID: '.$id.' gagal diupdate!';
        }
        echo json_encode($alert);
        DB::commit();
    }

    public function delfrezer(Request $request,$id,$batang)
    {
        DB::beginTransaction();
        $werty = Stock_drying1::where('id',$request->iddrying1)->first();
        $qusrt = Stock_frezer::where('id',$request->idfrezzer)->first();

        $batangfre = $qusrt->jml_fre - $batang;
        $batangdry1 = $werty->jml_dry1 + $batang;

        $update3 = Stock_drying1::find($request->iddrying1);
        $update3->jml_dry1 = $batangdry1;
        $update3->save();
        $update2 = Stock_frezer::find($request->idfrezzer);
        $update2->jml_fre = $batangfre;
        $update2->save();

        $query1 = Frezer::where('id',$id)->delete();
        if ($query1) {
            $this->produksi->loop_upfre($request,$id,$batang);
            $alert = 'Frezer ID : '.$id.' berhasil dihapus!';
        } else {
            $alert = 'Frezer ID : '.$id.' gagal dihapus!';
        }
        echo json_encode($alert);
        DB::commit();      
    }

}
