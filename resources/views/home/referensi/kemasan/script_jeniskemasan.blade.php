<script>
	jsGrid.setDefaults({
		tableClass: "jsgrid-table table table-striped table-hover"
	});
	jsGrid.setDefaults("text", {
		_createTextBox: function() {
			return $("<input>").attr("type", "text").attr("class", "form-control input-sm")
		}
	});
	jsGrid.setDefaults("number", {
		_createTextBox: function() {
			return $("<input>").attr("type", "number").attr("class", "form-control input-sm")
		}
	});
	jsGrid.setDefaults("textarea", {
		_createTextBox: function() {
			return $("<input>").attr("type", "textarea").attr("class", "form-control")
		}
	});
	jsGrid.setDefaults("control", {
		_createGridButton: function(cls, tooltip, clickHandler) {
			var grid = this._grid;
			return $("<button>").addClass(this.buttonClass).addClass(cls).attr({
				type: "button",
				title: tooltip
			}).on("click", function(e) {
				clickHandler(grid, e)
			})
		}
	});
	jsGrid.setDefaults("select", {
		_createSelect: function() {
			var $result = $("<select>").attr("class", "form-control input-sm"),
			valueField = this.valueField,
			textField = this.textField,
			selectedIndex = this.selectedIndex;
			return $.each(this.items, function(index, item) {
				var value = valueField ? item[valueField] : index,
				text = textField ? item[textField] : item,
				$option = $("<option>").attr("value", value).text(text).appendTo($result);
				$option.prop("selected", selectedIndex === index)
			}), $result
		}
	});
	$('#grid_kemasan').jsGrid({
		height: "500px",
		width: "100%",
		filtering: true,
		filterable:true,
		editing: true,
		inserting: true,
		sorting: true,
		paging: true,
		autoload: true,
		pageSize: 5,
		pageButtonCount: 5,
		deleteConfirm: "Apakah anda yakin ingin menghapus data ini ?",
		controller: {
			loadData: function(filter){
				return $.ajax({
					type: "GET",
					dataType: 'json',
					url: "/kemasan/json1",
					data: filter
				});
			},
			insertItem: function(item){
				item._token = $('meta[name="csrf-token"]').attr('content');
				return $.ajax({
					type: "POST",
					url: "/kemasan/post",
					data:item
				});
			},
			updateItem: function(item){
				item._token = $('meta[name="csrf-token"]').attr('content');
				return $.ajax({
					type: "PUT",
					url: "/kemasan/upp",
					data: item
				});
			},
			deleteItem: function(item){
				item._token = $('meta[name="csrf-token"]').attr('content');
				return $.ajax({
					type: "DELETE",
					url: "/kemasan/delete",
					data: item
				});
			},
		},

		fields: [
		{
			name: "id_kemasan",
			title: "ID",
			type: "hidden",
			css: 'hide',
			width: 50
		},
		{
			name: "kode_kemasan",
			title: "Kode Packaging",
			type: "text", 
			width: 50
		},
		{
			name: "nama_kemasan",
			title: "Jenis Packaging",
			type: "text", 
			width: 50
		},
		{
			type: "control",
			width: 50,
			itemTemplate: function function_name(value, item) {
				var $result = jsGrid.fields.control.prototype.itemTemplate.apply(this, arguments);
				var $customButton = $('<button type="button" class="btn btn-success btn-xs btn-rounded btn-icons"><i class="fa fa-check"></i></button>')
				.click(function(e) {
					$('#cari-kem').val(item.nama_kemasan);
					table.columns( 2 ).search(item.nama_kemasan).draw();
					e.stopPropagation();
				});

				return $result.add($customButton);
			}
		}
		]
	});

	function Refresh_jsgrid() {
		$("#grid_kemasan").jsGrid("loadData");
	}
</script>