<div class="row">
	<div class="col-md-4">
		<h4 class="card-title">Harga Tembakau</h4>
	</div>
	<div class="col-md-2 offset-md-6">
		<button type="button" class="btn btn-xs btn-success" id="tambah-satuan" data-link="{{ route('api.posthargatembakau') }}">Tambah</button>
		<button type="button" onclick="Refresh()" class="btn btn-xs btn-icons btn-secondary">Refresh</button>
	</div>
	<div class="col-md-12" align="center" id="loadingdelete">
		<h5>Data tengah diproses. Mohon tunggu sejenak!</h5>
	</div>
	<div class="table-responsive">
		<table id="table-satuan" class="display nowrap table table-hover table-striped table-bordered">
			<thead>
				<th>No.</th>
				<th>Tanggal</th>
				<th>Kode Harga Tembakau</th>
				<th>Satuan Berat</th>
				<th>Jenis</th>
				<th>Kategori</th>
				<th>Harga Tembakau</th>
				<th>Keterangan</th>
				<th>Aksi</th>
			</thead>
		</table>
	</div>
</div>