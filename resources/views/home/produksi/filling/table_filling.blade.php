<div class="row">
	<div class="col-md-6">
		<legend>Data Filling</legend>
	</div>
	<div class="col-md-2 offset-md-4">
		<button type="button" data-link="{{ route('filling.post') }}" class="btn btn-success btn-rounded btn-icons " id="tambah-filling">Tambah</button>
		<button type="button" onclick="Refresh_filling()" class="btn btn-rounded btn-icons btn-secondary">Refresh</button>
	</div>
	<div class="col-md-12" align="center" id="loadingdeletefill">
		<h5>Data tengah diproses. Mohon tunggu sejenak!</h5>
	</div>
	<div class="table-responsive">
		<table id="table-filling" class="display nowrap table table-hover table-striped table-bordered">
			<thead>
				<tr>
					<th>No.</th>
					<th>Tanggal</th>
					<th>Produk</th>
					<th>Jenis</th>
					<th>Stock (*Gram)</th>
					<th>Masuk (Weis)()</th>
					<th>Terpakai (Produksi)</th>
					<th>Terpakai (Weis)</th>
					<th>Sisa</th>
					<th>Yang Dihasilkan(*Gram)</th>
					<th>Aksi</th>
				</tr>
			</thead>
		</table>
	</div>
</div>