<div class="row">
	<div class="col-md-4">
		<h4 class="card-title">Harga Cukai</h4>
	</div>
	<div class="col-md-2 offset-md-6">
		<button type="button" class="btn btn-xs btn-success" id="tambah-hargacukai" data-link="{{ route('api.posthargacukai') }}">Tambah</button>
		<button type="button" onclick="Refresh_cukai()" class="btn btn-xs btn-icons btn-secondary">Refresh</button>
	</div>
	<div class="col-md-12" align="center" id="loadingdeletecukai">
		<h5>Data tengah diproses. Mohon tunggu sejenak!</h5>
	</div>
	<div class="table-responsive">
		<table id="table-hargacukai" class="display nowrap table table-hover table-striped table-bordered" width="100%">
			<thead>
				<th>No.</th>
				<th>Tanggal</th>
				<th>Kode Cukai</th>
				<th>Produk</th>
				<th>Sub Produk</th>
				<th>Harga Cukai</th>
				<th>Keterangan</th>
				<th>Aksi</th>
			</thead>
		</table>
	</div>
</div>