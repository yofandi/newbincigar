<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class History_cukai extends Model
{
    protected $table = 'history_cukai';

    protected $fillable = [
    	'masuk_cukai',
    	'terpakai_cukailama',
        'terpakai_cukaibaru',
    	'sisa_cukai',
        'tanggal_cukai',
        'ket_hiscukai',
    	'stock_cukai_id_stock_cukai',
    	'stock_cukai_id_sub_produk',
    	'stock_cukai_produk_id_produk',
        'harga_cukai_id',
    	'packing_id_packing',
    	'author_session',
    	'created_at',
    	'updated_at',
    	'log'
    ];
}
