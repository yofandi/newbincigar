<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use DataTables;
use Auth;
use App\Drying1;
use App\Stock_wrapping;
use App\Stock_drying1;

use App\Http\Controllers\Back_drying1;

class Drying1cont extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->produksi = $res = new Back_drying1;
    }
        
    public function searchweiprodry1(Request $request)
    {
        $wert = $this->produksi->sprodry1($request);
        echo json_encode($wert);
    }

    public function getdatadry1()
    {
        $query = DB::table('drying1')
        ->select('drying1.*','jenis.id_jenis','produk.id_produk','jenis.jenis','produk.produk')
        ->join('stock_drying1','drying1.stock_drying1_id','=','stock_drying1.id')
        ->join('jenis','stock_drying1.jenis_has_produk_id_jenis','=','jenis.id_jenis')
        ->join('produk','stock_drying1.jenis_has_produk_id_produk','=','produk.id_produk')
        ->get();
        return Datatables::of($query)
        ->addIndexColumn()
        ->addColumn('awlbatang', function ($query) {
            $awl = $query->hasil_akhirdry1 + $query->tambah_dry1;
            return $awl;
        })
        ->make(true);
    }

    public function datadrying1()
    {
        $data['jenis'] = DB::table('jenis')->select('id_jenis','jenis')->get();
        return view('home.produksi.drying1.datadrying1',$data);
    }

    public function adddrying1(Request $request)
    {
        DB::beginTransaction();

        $qq = $this->produksi->stockdry1($request);

        $data = [
            'tambah_dry1' => $request->batangjml,
            'hasil_akhirdry1' => $qq['jml_wrap'],
            'lama_dry1' => $request->durasi,
            'keterangan_dry1' => $request->ket,
            'tanggal_dry1' => $request->tanggal_drying1,
            'stock_wrapping_id' => $qq['idwrapping'],
            'stock_drying1_id' => $qq['iddrying1'],
            'author_session' => Auth::user()->id,
            'created_at' => date('Y-m-d H:i:s'),
            'log' => "INSERT"
        ];
        $query1 = Drying1::insert($data);
        if ($query1) {
            $alert = 'Drying 1 berhasil ditambahkan!';
        } else {
            $alert = 'Drying 1 gagal ditambahkan!';
        }
        echo json_encode($alert);
        DB::commit();
    }

    public function uppdrying1(Request $request,$id,$batang)
    {
        DB::beginTransaction();
        $jwty = $request->stockbatang - $request->batangjml;
        $this->produksi->ifuppstockdry1($request,$batang);

        $data = [
            'tambah_dry1' => $request->batangjml,
            'hasil_akhirdry1' => $jwty,
            'lama_dry1' => $request->durasi,
            'keterangan_dry1' => $request->ket,
            'tanggal_dry1' => $request->tanggal_drying1,
            'author_session' => Auth::user()->id,
            'log' => "UPDATE"
        ];
        $query1 = Drying1::where('id',$id)->update($data);
        if ($query1) {
            $this->produksi->loop_updry1($request,$id);
            $alert = 'Drying 1 ID: '.$id.' berhasil diupdate!';
        } else {
            $alert = 'Drying 1 ID: '.$id.' gagal diupdate!';
        }
        echo json_encode($alert);
        DB::commit();
    }

    public function deldrying1(Request $request,$id,$batang)
    {
        DB::beginTransaction();
        $werty = Stock_wrapping::where('id',$request->idwrapping)->first();
        $qusrt = Stock_drying1::where('id',$request->iddrying1)->first();

        $batangdry1 = $qusrt->jml_dry1 - $batang;
        $batangwrap = $werty->jml_wrap + $batang;

        $update3 = Stock_wrapping::find($request->idwrapping);
        $update3->jml_wrap = $batangwrap;
        $update3->save();
        $update2 = Stock_drying1::find($request->iddrying1);
        $update2->jml_dry1 = $batangdry1;
        $update2->save();

        $query1 = Drying1::where('id',$id)->delete();
        if ($query1) {
            $this->produksi->loop_updry1($request,$id,$batang);
            $alert = 'Drying 1 ID : '.$id.' berhasil dihapus!';
        } else {
            $alert = 'Drying 1 ID : '.$id.' gagal dihapus!';
        }
        echo json_encode($alert);
        DB::commit();      
    }

}
