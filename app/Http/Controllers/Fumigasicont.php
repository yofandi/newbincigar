<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use DataTables;
use Auth;
use App\Fumigasi;
use App\Stock_drying2;
use App\Stock_fumigasi;

use App\Http\Controllers\Back_fumigasi;

class Fumigasicont extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->produksi = $res = new Back_fumigasi;
    }
    public function searchweiprofumi(Request $request)
    {
        $wbhrj = $this->produksi->sprofumi($request);
        echo json_encode($wbhrj);
    }

    public function getdatafumi()
    {
        $query = DB::table('fumigasi')
        ->select('fumigasi.*','jenis.id_jenis','produk.id_produk','jenis.jenis','produk.produk')
        ->join('stock_fumigasi','fumigasi.stock_fumigasi_id','=','stock_fumigasi.id')
        ->join('jenis','stock_fumigasi.jenis_has_produk_id_jenis','=','jenis.id_jenis')
        ->join('produk','stock_fumigasi.jenis_has_produk_id_produk','=','produk.id_produk')
        ->get();
        return Datatables::of($query)
        ->addIndexColumn()
        ->addColumn('awlbatang', function ($query) {
            $awl = $query->hasil_akhirfum + $query->tambah_fum;
            return $awl;
        })
        ->make(true);
    }

    public function datafumigasi()
    {
        $data['jenis'] = DB::table('jenis')->select('id_jenis','jenis')->get();
        return view('home.produksi.fumigasi.datafumigasi',$data);
    }

    public function addfumigasi(Request $request)
    {
        DB::beginTransaction();

        $qq = $this->produksi->stockfumi($request);

        $data = [
            'tambah_fum' => $request->batangjml,
            'hasil_akhirfum' => $qq['jml_dry2'],
            'lama_fum' => $request->durasi,
            'keterangan_fum' => $request->ket,
            'tanggal_fum' => $request->tanggal_fumigasi,
            'stock_fumigasi_id' => $qq['idfumigasi'],
            'stock_drying2_id' => $qq['iddrying2'],
            'author_session' => Auth::user()->id,
            'created_at' => date('Y-m-d H:i:s'),
            'log' => "INSERT"
        ];
        $query1 = Fumigasi::insert($data);
        if ($query1) {
            $alert = 'Fumigasi berhasil ditambahkan!';
        } else {
            $alert = 'Fumigasi gagal ditambahkan!';
        }
        echo json_encode($alert);
        DB::commit();
    }

    public function uppfumigasi(Request $request,$id,$batang)
    {
        DB::beginTransaction();
        $jwty = $request->stockbatang - $request->batangjml;
        $this->produksi->ifuppstockfumi($request,$batang);

        $data = [
            'tambah_fum' => $request->batangjml,
            'hasil_akhirfum' => $jwty,
            'lama_fum' => $request->durasi,
            'keterangan_fum' => $request->ket,
            'tanggal_fum' => $request->tanggal_fumigasi,
            'author_session' => Auth::user()->id,
            'log' => "UPDATE"
        ];
        $query1 = Fumigasi::where('id',$id)->update($data);
        if ($query1) {
            $this->produksi->loop_upfumi($request,$id);
            $alert = 'Fumigasi ID: '.$id.' berhasil diupdate!';
        } else {
            $alert = 'Fumigasi ID: '.$id.' gagal diupdate!';
        }
        echo json_encode($alert);
        DB::commit();
    }

    public function delfumigasi(Request $request,$id,$batang)
    {
        DB::beginTransaction();
        $werty = Stock_drying2::where('id',$request->iddrying2)->first();
        $qusrt = Stock_fumigasi::where('id',$request->idfumigasi)->first();

        $batangdry2 = $werty->jml_dry2 + $batang;
        $batangfumi = $qusrt->jml_fum - $batang;

        $update3 = Stock_drying2::find($request->iddrying2);
        $update3->jml_dry2 = $batangdry2;
        $update3->save();
        $update2 = Stock_fumigasi::find($request->idfumigasi);
        $update2->jml_fum = $batangfumi;
        $update2->save();

        $query1 = Fumigasi::where('id',$id)->delete();
        if ($query1) {
            $this->produksi->loop_upfumi($request,$id,$batang);
            $alert = 'Fumigasi ID : '.$id.' berhasil dihapus!';
        } else {
            $alert = 'Fumigasi ID : '.$id.' gagal dihapus!';
        }
        echo json_encode($alert);
        DB::commit();      
    }

}
