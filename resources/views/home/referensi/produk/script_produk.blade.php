<script>
	$('.spinner_nekr').hide();
	$('#loadingdelete').hide();
	let token = $("meta[name='csrf-token']").attr("content");
	const jenis = {!! json_encode($jenis) !!}
	jenis.unshift({id_jenis: '',jenis: ''});

	const merek_cukai = {!! json_encode($merek_cukai) !!}

	function format_number(number, prefix, thousand_separator, decimal_separator)
	{
		var 	thousand_separator = thousand_separator || ',',
		decimal_separator = decimal_separator || '.',
		regex		= new RegExp('[^' + decimal_separator + '\\d]', 'g'),
		number_string = number.replace(regex, '').toString(),
		split	  = number_string.split(decimal_separator),
		rest 	  = split[0].length % 3,
		result 	  = split[0].substr(0, rest),
		thousands = split[0].substr(rest).match(/\d{3}/g);

		if (thousands) {
			separator = rest ? thousand_separator : '';
			result += separator + thousands.join(thousand_separator);
		}
		result = split[1] != undefined ? result + decimal_separator + split[1] : result;
		return prefix == undefined ? result : (result ? prefix + result : '');
	};

	var input0 = document.getElementById('sub-hje');
	input0.addEventListener('keyup', function(e)
	{
		input0.value = format_number(this.value, '');
	});

	var input1 = document.getElementById('sub-tarif');
	input1.addEventListener('keyup', function(e)
	{
		input1.value = format_number(this.value, '');
	});

	function lkemasan(position,like = '') {
		$.ajax({
			url: '/kemasan/json',
			type: 'GET',
			dataType: 'json',
		})
		.done(function(data) {
			let opt = '<option value="">--- Pilih  ---</option>';
			for (var i = 0; i < data.length; i++) {
				opt += '<option value="'+ data[i].id_kemasan +'"';
				if (data[i].id_kemasan == like) { opt += 'selected'; }
				opt += '>'+ data[i].nama_kemasan +'</option>';
			}
			$('#' + position).html(opt);
		});
	}

	function lmerek(position,like = '') {
		let opt = '<option value="">--- Pilih  ---</option>';
		for (var i = 0; i < merek_cukai.length; i++) {
			opt += '<option value="'+ merek_cukai[i].id +'"';
			if (merek_cukai[i].id == like) { opt += 'selected'; }
			opt += '>'+ merek_cukai[i].kode_cukai +' - '+ merek_cukai[i].nama_cukai +'</option>';
		}
		$('#' + position).html(opt);
	}

	var table = $('#table-sub').DataTable({
		"lengthMenu": [
		[3,5,10, 25, 50, 100, 150, 200, -1],
		[3,5,10, 25, 50, 100, 150, 200, "All"]
		],
		"order": [
		[0, 'desc']
		],
		processing: true,
		serverSide: true,
		ajax: '/subproduk/json',
		columns: [
		{ data: 'DT_RowIndex', name: 'DT_RowIndex' },
		{ data: 'kode_produk', name: 'kode_produk' },
		{ data: 'sub_kode', name: 'sub_kode' },
		{ data: 'sub_produk', name: 'sub_produk' },
		{ data: 'ring', name: 'ring' },
		{ data: 'nama_kemasan', name: 'nama_kemasan' },
		{ data: 'isi', name: 'isi' },
		{ data: 'hje', render: $.fn.dataTable.render.number(',', '.', 0, '')},
		{ data: 'tarif', render: $.fn.dataTable.render.number(',', '.', 0, '')},
		{ data: 'berat_jadi', name: 'berat_jadi'},
		{	
			data: null,
			"bSortable": false,
			mRender: function(o) {
				return '<button type="button" class="btn btn-xs btn-rounded btn-warning update-sub" data-title="Update Sub Produk : ID - '+ o.id_sub_produk +'"  data-link="/subproduk/update/'+ o.id_sub_produk +'" data-idpro="'+ o.produk_id_produk +'" data-namepro="'+ o.produk +'" data-kodesub="'+ o.sub_kode +'" data-namesub="'+ o.sub_produk +'" data-kemasub="'+ o.kemasan_id_kemasan +'" data-merekcukai="'+ o.merek_cukai_id +'" data-tarifsub="'+ format_number(o.tarif, '') +'" data-hjesub="'+ format_number(o.hje, '') +'" data-isisub="'+ o.isi +'" data-berat="'+ o.berat_jadi +'" data-ring="'+ o.ring +'"><i class="mdi mdi-dots-horizontal"></i></button><button type="button" class="btn btn-danger btn-rounded btn-xs" data-id="'+ o.id_sub_produk +'" id="delete-sub"><i class="fa fa-trash"></i></button>'; 
			}
		},
		],
		"displayLength": 10,
	});
	$('#table-sub tbody').on('click', 'tr.group', function() {
		var currentOrder = table.order()[0];
		if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
			table.order([2, 'desc']).draw();
		} else {
			table.order([2, 'asc']).draw();
		}
	});
	$('#table-sub tbody').on('click', 'tr.group', function() {
		var currentOrder = table.order()[0];
		if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
			table.order([2, 'desc']).draw();
		} else {
			table.order([2, 'asc']).draw();
		}
	});

	jsGrid.setDefaults({
		tableClass: "jsgrid-table table table-striped table-hover"
	});
	jsGrid.setDefaults("text", {
		_createTextBox: function() {
			return $("<input>").attr("type", "text").attr("class", "form-control input-sm")
		}
	});
	jsGrid.setDefaults("number", {
		_createTextBox: function() {
			return $("<input>").attr("type", "number").attr("class", "form-control input-sm")
		}
	});
	jsGrid.setDefaults("textarea", {
		_createTextBox: function() {
			return $("<input>").attr("type", "textarea").attr("class", "form-control")
		}
	});
	jsGrid.setDefaults("control", {
		_createGridButton: function(cls, tooltip, clickHandler) {
			var grid = this._grid;
			return $("<button>").addClass(this.buttonClass).addClass(cls).attr({
				type: "button",
				title: tooltip
			}).on("click", function(e) {
				clickHandler(grid, e)
			})
		}
	});
	jsGrid.setDefaults("select", {
		_createSelect: function() {
			var $result = $("<select>").attr("class", "form-control input-sm"),
			valueField = this.valueField,
			textField = this.textField,
			selectedIndex = this.selectedIndex;
			return $.each(this.items, function(index, item) {
				var value = valueField ? item[valueField] : index,
				text = textField ? item[textField] : item,
				$option = $("<option>").attr("value", value).text(text).appendTo($result);
				$option.prop("selected", selectedIndex === index)
			}), $result
		}
	});
	$('#grid_produk').jsGrid({
		height: "500px",
		width: "100%",
		filtering: true,
		filterable:true,
		editing: true,
		inserting: true,
		sorting: true,
		paging: true,
		autoload: true,
		pageSize: 15,
		pageButtonCount: 5,
		deleteConfirm: "Apakah anda yakin ingin menghapus data ini ?",
		controller: {
			loadData: function(filter){
				return $.ajax({
					type: "GET",
					dataType: 'json',
					url: "/produk/json",
					data: filter
				});
			},
			insertItem: function(item){
				item._token = $('meta[name="csrf-token"]').attr('content');
				return $.ajax({
					type: "POST",
					url: "/produk/add",
					data:item
				});
			},
			updateItem: function(item){
				item._token = $('meta[name="csrf-token"]').attr('content');
				return $.ajax({
					type: "PUT",
					url: "/produk/put",
					data: item
				});
			},
			deleteItem: function(item){
				item._token = $('meta[name="csrf-token"]').attr('content');
				return $.ajax({
					type: "DELETE",
					url: "/produk/delete",
					data: item
				});
			},
		},

		fields: [
		{
			name: "id_jnpro",
			title: "ID jenis Has produk",
			type: "hidden",
			css: 'hide',
			width: 150
		},
		{
			name: "kode_produk",
			title: "Kode Produk",
			type: "text",
			width: 50
		},
		{
			name: "id_jenis",
			title: "Jenis",
			type: "select",
			items: jenis, valueField: "id_jenis", textField: "jenis",
			width: 50
		},
		{
			name: "produk",
			title: "Produk",
			type: "text", 
			width: 50
		},
		{
			type: "control",
			width: 50,
			itemTemplate: function function_name(value, item) {
				var $result = jsGrid.fields.control.prototype.itemTemplate.apply(this, arguments);
				var $customButton = $('<button type="button" class="btn btn-success btn-xs btn-rounded btn-icons"><i class="fa fa-check"></i></button>')
				.click(function(e) {
					// alert("name produk: " + item.produk);
					$('#nama-produk').val(item.produk);
					$('#idproduk-hidden').val(item.id_produk);
					$('#name-produk-search').val(item.produk);
					table.columns( 1 ).search(item.kode_produk).draw();
					e.stopPropagation();
				});

				return $result.add($customButton);
			}
		}
		]
	});

	function Refresh() {
		$("#grid_produk").jsGrid("loadData");
	}

	function Refresh_sub() {
		$('#nama-produk').val('');
		$('#idproduk-hidden').val('');
		$("#name-produk-search").val('');
		table.columns( 1 ).search('').draw();
		// $('#table-sub').DataTable().ajax.reload();
	}
	$(document).on('click', '#tambah-sub', function(event) {
		let clik = $(this);
		$('.modal-header #title').text('Tambah Sub Produk');
		$('#form-produk').find('input.hjet').val('');
		$('#form-produk').find('select').val('');
		$('#form-produk').attr('action', clik.data('link'));
		lkemasan('sub-kemasan');
		lmerek('merek-cukai');
		$('#modal-produk').modal('show');
	});

	$(document).on('click', '.update-sub', function(event) {
		let clik = $(this);
		$('.modal-header #title').text(clik.data('title'));
		$('#form-produk').find('input').val('');
		$('#form-produk').find('select').val('');
		$('#form-produk').attr('action', clik.data('link'));
		$('#nama-produk').val(clik.data('namepro'));
		$('#idproduk-hidden').val(clik.data('idpro'));
		$('#kode-sub').val(clik.data('kodesub'));
		$('#nama-sub').val(clik.data('namesub'));
		$('#sub-hje').val(clik.data('hjesub'));
		$('#sub-isi').val(clik.data('isisub'));
		$('#sub-tarif').val(clik.data('tarifsub'));
		lkemasan('sub-kemasan',clik.data('kemasub'));
		lmerek('merek-cukai',clik.data('merekcukai'));
		$('#berat').val(clik.data('berat'));
		$('#ring').val(clik.data('ring'));
		$('#modal-produk').modal('show');
	});

	$(document).on('click', '#delete-sub', function() {
		let jety = confirm('Apakah anda yakin ingin menghapus data ini ?');
		if (jety) {
			let id = $(this).data("id");
			let token = $("meta[name='csrf-token']").attr("content");
			$.ajax(
			{
				url: '/subproduk/delete/' + id,
				type: 'DELETE',
				data: {
					"_token": token,
				},
				beforeSend: function() {
					isProcessing = true;
					$('#loadingdelete').fadeIn();
				},
				success: function (data){
					$('#loadingdelete').hide();
					isProcessing = false;
					alert(data);
					$('#table-sub').DataTable().ajax.reload();
				}
			});
		}
	});

	function save_method() {
		let token = $("meta[name='csrf-token']").attr("content");
		let link = $('#form-produk').attr('action');
		let id_produk = $('#idproduk-hidden').val();
		let sub_kode = $('#kode-sub').val();
		let sub_name = $('#nama-sub').val();
		let sub_hje = $('#sub-hje').val();
		let sub_isi = $('#sub-isi').val();
		let sub_tarif = $('#sub-tarif').val();
		let sub_kemasan = $('#sub-kemasan').val();
		let berat = $('#berat').val();
		let ring = $('#ring').val();
		let merekcukai = $('#merek-cukai').val();

		$.ajax({
			url: link,
			type: 'POST',
			dataType: 'json',
			data: {
				"_token": token,
				id_produk:id_produk,
				sub_kode:sub_kode,
				sub_name:sub_name,
				sub_hje:sub_hje,
				sub_isi:sub_isi,
				sub_tarif:sub_tarif,
				sub_kemasan:sub_kemasan,
				berat: berat,
				ring:ring,
				merekcukai:merekcukai
			},
			beforeSend: function() {
				isProcessing = true;
				$('.spinner_nekr').fadeIn();
			},
			success: function(data) {
				$('.spinner_nekr').hide();
				isProcessing = false;
				alert(data);
				$('#table-sub').DataTable().ajax.reload();
				$('#modal-produk').modal('toggle');
			}
		});
	}
</script>