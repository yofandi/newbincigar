<div class="row">
	<div class="col-md-6">
		<legend>Data Wrapping</legend>
	</div>
	<div class="col-md-2 offset-md-4">
		<button type="button" data-link="{{ route('wrapping.post') }}" class="btn btn-success btn-rounded btn-icons " id="tambah-wrapping">Tambah</button>
		<button type="button" onclick="Refresh_wrapping()" class="btn btn-rounded btn-icons btn-secondary">Refresh</button>
	</div>
	<div class="col-md-12" align="center" id="loadingdeletewrap">
		<h5>Data tengah diproses. Mohon tunggu sejenak!</h5>
	</div>
	<div class="table-responsive">
		<table id="table-wrapping" width="100%" class="display nowrap table table-hover table-striped table-bordered">
			<thead>
				<tr>
					<th>No.</th>
					<th>Tanggal</th>
					<th>Produk</th>
					<th>Jenis</th>
					<th>Stock</th>
					<th>Masuk (Weis)</th>
					<th>Terpakai (Produksi)</th>
					<th>Terpakai (Weis)</th>
					<th>Sisa</th>
					<th>Batang Filling</th>
					<th>Jml Batang</th>
					<th>Reject</th>
					<th>ket</th>
					<th>Aksi</th>
				</tr>
			</thead>
		</table>
	</div>
</div>