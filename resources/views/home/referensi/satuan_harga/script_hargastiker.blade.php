<script>
	var input3 = document.getElementById('harga_satlstiker');
	input3.addEventListener('keyup', function(e)
	{
		input3.value = format_number(this.value, '');
	}
	);

	var input4 = document.getElementById('harga_satdstiker');
	input4.addEventListener('keyup', function(e)
	{
		input4.value = format_number(this.value, '');
	}
	);

	function Refresh_stiker() {
		$('#table-hargastiker').DataTable().ajax.reload();
	}

	var tablestiker = $('#table-hargastiker').DataTable({
		"lengthMenu": [
		[3,5,10, 25, 50, 100, 150, 200, -1],
		[3,5,10, 25, 50, 100, 150, 200, "All"]
		],
		"order": [
		[0, 'desc']
		],
		processing: true,
		serverSide: true,
		ajax: '/api/hargastiker/json',
		columns: [
		{ data: 'DT_RowIndex', name: 'DT_RowIndex' },
		{ data: 'tanggal_harga', name: 'tanggal_harga' },
		{ data: 'kode_hstiker', name: 'kode_hstiker' },
		{ data: 'produk', name: 'produk' },
		{ data: 'kodesubproduk', name: 'kodesubproduk' },
		{ data: 'harga_luar_for', name: 'harga_luar_for' },
		{ data: 'harga_dalam_for', name: 'harga_dalam_for' },
		{ data: 'keterangan_hargasti', name: 'keterangan_hargasti' },
		{	
			data: null,
			"bSortable": false,
			mRender: function(o) {
				$bind = '<button type="button" class="btn btn-xs btn-rounded btn-warning update-hargastiker" data-title="Update Harga Cukai | ID '+ o.id +'" data-link="/api/hargastiker/update/'+ o.id +'" data-tanggal_stiker="'+ o.tanggal_harga +'" data-kode="'+ o.kode_hstiker +'" data-produk="'+ o.id_produk +'" data-subproduk="'+ o.sub_produk_id +'" data-hargal="'+ format_number(o.harga_luar, '') +'" data-hargad="'+ format_number(o.harga_dalam, '') +'" data-ket="'+ o.keterangan_hargasti +'" data-count="'+ o.countdata +'"><i class="mdi mdi-dots-horizontal"></i></button>';
				$bind += '<button type="button" class="btn btn-danger btn-rounded btn-xs" id="delete-hargastiker" data-id="'+ o.id +'" data-count="'+ o.countdata +'"><i class="fa fa-trash"></i></button>';
				return $bind; 
			}
		},
		],
		"displayLength": 10,
	});
	$('#table-hargastiker tbody').on('click', 'tr.group', function() {
		var currentOrder = tablestiker.order()[0];
		if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
			tablestiker.order([2, 'desc']).draw();
		} else {
			table.order([2, 'asc']).draw();
		}
	});
	$('#table-hargastiker tbody').on('click', 'tr.group', function() {
		var currentOrder = tablestiker.order()[0];
		if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
			tablestiker.order([2, 'desc']).draw();
		} else {
			tablestiker.order([2, 'asc']).draw();
		}
	});

	function save_methodstiker() {
		let link = $('#form-hargastiker').attr('action');
		let tanggal_stiker = $('#tanggal_stiker').val();
		let kode_hstiker = $('#kode_hstiker').val();
		let produk = $('#produk-stiker').val();
		let subproduk = $('#subproduk-stiker').val();
		let harga_satuanl = $('#harga_satlstiker').val();
		let harga_satuand = $('#harga_satdstiker').val();
		let keterangan = $('#keterangan-stiker').val();
		$.ajax({
			url: link,
			type: 'POST',
			dataType: 'json',
			data: {
				"_token": token,
				tanggal_stiker: tanggal_stiker,
				kode_hstiker: kode_hstiker,
				produk: produk,
				subproduk: subproduk,
				harga_satuanl: harga_satuanl,
				harga_satuand: harga_satuand,
				keterangan: keterangan,
			},
			beforeSend: function() {
				isProcessing = true;
				$('.spinner_nekrsti').fadeIn();
			},
			success: function(data) {
				$('.spinner_nekrsti').hide();
				isProcessing = false;
				alert(data);
				Refresh_stiker();
				$('#modal-hargastiker').modal('toggle');
			}
		});
	}

	$(document).on('click', '#tambah-hargastiker', function(event) {
		let click = $(this);
		$('.modal-header #title-hargastiker').text('Tambah Harga Cukai');
		$('#form-hargastiker').find('input').val('');
		$('#form-hargastiker').find('select').val('');
		$('#form-hargastiker').find('textarea').val('');
		$('#form-hargastiker').attr('action', click.data('link'));
		lproduk('produk-stiker');
		$('#produk-stiker').change(function(event) {
			let valpro = $(this).val();
			lsubpro('subproduk-stiker',valpro);
		});
		$("#tanggal_stiker").datetimepicker({
			format: 'YYYY-MM-DD HH:mm:ss',
		}).data("DateTimePicker").date(new Date());
		$('#modal-hargastiker').modal('show');
	});

	$(document).on('click', '.update-hargastiker', function(event) {
		let click = $(this);
		$('.modal-header #title-hargastiker').text(click.data('title'));
		$('#form-hargastiker').find('input').val('');
		$('#form-hargastiker').find('select').val('');
		$('#form-hargastiker').find('textarea').val('');
		$('#form-hargastiker').attr('action', click.data('link'));
		lproduk('produk-stiker',click.data('produk'));
		lsubpro('subproduk-stiker',click.data('produk'),click.data('subproduk'));
		$('#harga_satlstiker').val(click.data('hargal'));
		$('#harga_satdstiker').val(click.data('hargad'));
		$('#keterangan-stiker').val(click.data('ket'));
		$('#kode_hstiker').val(click.data('kode'));
		$("#tanggal_stiker").datetimepicker({
			format: 'YYYY-MM-DD HH:mm:ss',
		}).data("DateTimePicker").date(click.data('tanggal_stiker'));
		$('#modal-hargastiker').modal('show');
	});
	
	$(document).on('click', '#delete-hargastiker', function(event) {
		let dje = confirm('Apakah anda yakin ingin menghapus data ini ?');
		if (dje) {
			if ($(this).data("count") > 0) {
				alert('Maaf.. Anda tidak bisa menghapus data ini, dikarenakan akan menimbulkan error pada aplikasi ini!!');
			} else {

				let id = $(this).data("id");
				$.ajax(
				{
					url: '/api/hargastiker/delete/' + id,
					type: 'DELETE',
					data: {
						"_token": token,
					},
					beforeSend: function() {
						isProcessing = true;
						$('#loadingdeletestiker').fadeIn();
					},
					success: function (data){
						$('#loadingdeletestiker').hide();
						alert(data);
						Refresh_stiker();
						isProcessing = false;
					}
				});
			}
		}
	});
</script>