<script>
	$('#showit').hide();
	$('.spinner_nekr').hide();
	$('#loadingdelete').hide();
	let token = $("meta[name='csrf-token']").attr("content");
	$.extend(true, $.fn.datetimepicker.defaults, {
		icons: {
			time: 'far fa-clock',
			date: 'far fa-calendar',
			up: 'fas fa-arrow-up',
			down: 'fas fa-arrow-down',
			previous: 'fas fa-chevron-left',
			next: 'fas fa-chevron-right',
			today: 'fas fa-calendar-check',
			clear: 'far fa-trash-alt',
			close: 'far fa-times-circle'
		}
	});

	const produk = {!! json_encode($produk) !!};
	produk.unshift({id_produk: '',produk: ''});

	const kemasan = {!! json_encode($kemasan) !!};
	kemasan.unshift({id_kemasan: '',nama_kemasan: ''});

	function format_number(number, prefix, thousand_separator, decimal_separator)
	{
		var 	thousand_separator = thousand_separator || ',',
		decimal_separator = decimal_separator || '.',
		regex		= new RegExp('[^' + decimal_separator + '\\d]', 'g'),
		number_string = number.toString().replace(regex, ''),
		split	  = number_string.split(decimal_separator),
		rest 	  = split[0].length % 3,
		result 	  = split[0].substr(0, rest),
		thousands = split[0].substr(rest).match(/\d{3}/g);

		if (thousands) {
			separator = rest ? thousand_separator : '';
			result += separator + thousands.join(thousand_separator);
		}
		result = split[1] != undefined ? result + decimal_separator + split[1] : result;
		return prefix == undefined ? result : (result ? prefix + result : '');
	};

	var table = $('#table_kemasan').DataTable({
		"lengthMenu": [
		[3,5,10, 25, 50, 100, 150, 200, -1],
		[3,5,10, 25, 50, 100, 150, 200, "All"]
		],
		"order": [
		[0, 'desc']
		],
		processing: true,
		serverSide: true,
		ajax: '/data_kemasan/json',
		columns: [
		{ data: 'DT_RowIndex', name: 'DT_RowIndex' },
		{ data: 'tanggal_hiskem', name: 'tanggal_hiskem' },
		{ data: 'produk', name: 'produk' },
		{ data: 'kodeproduk', name: 'kodeproduk' },
		{ data: 'nama_kemasan', name: 'nama_kemasan' },
		{ data: 'masuk_kemasan', name: 'masuk_kemasan' },
		{ data: 'terpakai_kemasan', name: 'terpakai_kemasan' },
		{ data: 'afkir_kemasan', name: 'afkir_kemasan' },
		{ data: 'stok_now', name: 'stok_now' },
		{ data: 'ket_kem', name: 'ket_kem' },
		{
			data: null,
			"bSortable": false,
			mRender: function (o) { 
				$masuk_kemasan = parseInt(o.masuk_kemasan);
				$terpakai_kemasan = parseInt(o.terpakai_kemasan);
				$afkir_kemasan = parseInt(o.afkir_kemasan);
				$stok_now = parseInt(o.stok_now);
				let stock = $stok_now - $masuk_kemasan + ($terpakai_kemasan + $afkir_kemasan);
				return '<button type="button" class="btn btn-info btn-rounded btn-xs" id="update-kemasan" data-title="Update Stock Kemasan | ID: '+ o.id +'" data-link="/data_kemasan/upp/'+ o.id +'/'+ o.masuk_kemasan +'/'+ o.terpakai_kemasan +'/'+ o.afkir_kemasan +'" data-id="'+ o.id +'" data-hargaid="'+ o.harga_kemasan_id +'" data-tanggal="'+ o.tanggal_hiskem +'" data-produk="'+ o.stock_kemasan_produk_id_produk +'" data-subproduk="'+ o.stock_kemasan_sub_produk_id +'" data-kemasan="'+ o.stock_kemasan_kemasan_id_kemasan +'" data-nama_kemasan="'+ o.nama_kemasan +'" data-stock="'+ stock +'" data-masuk="'+ o.masuk_kemasan +'" data-terpakai="'+ o.terpakai_kemasan +'" data-afkir="'+ o.afkir_kemasan +'" data-ket_kem="'+ o.ket_kem +'"><i class="mdi mdi-dots-horizontal"></i></button><button type="button" class="btn btn-danger btn-rounded btn-xs" id="delete-kemasan" data-link="/data_kemasan/del/'+ o.id +'/'+ o.masuk_kemasan +'/'+ o.terpakai_kemasan +'/'+ o.afkir_kemasan +'/'+ o.awlstok +'" data-id="'+ o.id +'" data-produk="'+ o.stock_kemasan_produk_id_produk +'" data-kemasan="'+ o.stock_kemasan_kemasan_id_kemasan +'"><i class="fa fa-trash"></i></button>'; }
			},
			],
			"displayLength": 10,
		});
	$('#table_kemasan tbody').on('click', 'tr.group', function() {
		var currentOrder = table.order()[0];
		if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
			table.order([2, 'desc']).draw();
		} else {
			table.order([2, 'asc']).draw();
		}
	});
	$('#table_kemasan tbody').on('click', 'tr.group', function() {
		var currentOrder = table.order()[0];
		if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
			table.order([2, 'desc']).draw();
		} else {
			table.order([2, 'asc']).draw();
		}
	});

	function lproduk(position,like = "") {
		let opt = '';
		for (var i =  0; i < produk.length; i++) {
			opt += '<option value="'+ produk[i].id_produk +'"';
			if (produk[i].id_produk == like) { opt += 'selected'; }
			opt +='>'+ produk[i].produk +'</option>';
		}
		$('#' + position).html(opt);
	}

	function lsubproduk(position,based,like = "") {
		$.ajax({
			url: '/data_cukai/getsubproduk',
			type: 'POST',
			dataType: 'json',
			data: {
				"_token": token,
				produk: based
			},
		})
		.done(function(data) {
			let opt = '<option value=""></option>';
			for (var i = 0; i < data.length; i++) {
				opt += '<option value="'+ data[i].id_sub_produk +'"';
				if (data[i].id_sub_produk == like) { opt += 'selected'; }
				opt += ' data-idkemasan="'+ data[i].id_kemasan +'"data-nama_kemasan="'+ data[i].nama_kemasan +'">'+ data[i].sub_kode +' - '+ data[i].sub_produk +'</option>';
			}
			$('#' + position).html(opt);
		});
	}

	function lkemasan(position,like = "") {
		let opt = '';
		for (var i =  0; i < kemasan.length; i++) {
			opt += '<option value="'+ kemasan[i].id_kemasan +'"';
			if (kemasan[i].id_kemasan == like) { opt += 'selected'; }
			opt +='>'+ kemasan[i].nama_kemasan +'</option>';
		}
		$('#' + position).html(opt);
	}

	function getseacrhdatarow(sts,produk_in,subproduk_in,val = 0,id = '') {
		$.ajax({
			url: '/data_kemasan/getstock',
			type: 'POST',
			dataType: 'json',
			data: {
				"_token": token,
				produk: produk_in,
				subproduk: subproduk_in,
				val: val,
				id: id,
			},
		})
		.done(function(data) {
			$('#harga_satuan').val(format_number(data.harga, ''));
			if (sts != 1) {
				$('#kemasan').val($('#subproduk').find(':selected').data('nama_kemasan'));
				$('#kemasan_in').val($('#subproduk').find(':selected').data('idkemasan'));
				$('#stock_awl').val(data['content']);
			}
		});
	}

	function search_datastock() {
		let produk_in = $('#produk_in').val();
		let subproduk_in = $('#subproduk_in').val();
		getseacrhdatarow(0,produk_in,subproduk_in);
	}

	function refresh() {
		$('#table_kemasan').DataTable().ajax.reload();
	}

	function save_method() {
		let link = $('#form-kemasan').attr('action');
		let tanggal_hiskem = $('#tanggal_hiskem').val();
		let produk_in = $('#produk_in').val();
		let subproduk_in = $('#subproduk_in').val();
		let kemasan_in = $('#kemasan_in').val();
		let stock_awl = $('#stock_awl').val();
		let masuk = $('#masuk').val();
		let terpakaikemasan = $('#terpakaikemasan').val();
		let afkir = $('#afkir').val();
		let ket_kem = $('#ket_kem').val();

		$.ajax({
			url: link,
			type: 'POST',
			dataType: 'json',
			data: {
				"_token": token,
				tanggal_hiskem : tanggal_hiskem,
				produk : produk_in,
				subproduk: subproduk_in,
				kemasan : kemasan_in,
				stock : stock_awl,
				masuk : masuk,
				terpakaikemasan : terpakaikemasan,
				afkir : afkir,
				ket_kem : ket_kem
			},
			beforeSend: function() {
				isProcessing = true;
				$('.spinner_nekr').fadeIn();
			},
			success: function(data) {
				$('.spinner_nekr').hide();
				isProcessing = false;
				alert(data);
				$('#table_kemasan').DataTable().ajax.reload();
				$('#modal-kemasan').modal('toggle');
			}
		});
		
	}

	$(document).on('click', '#tambah-kemasan', function(event) {
		let clik = $(this);
		$('.modal-header #title').text('Tambah Stock Kemasan');
		$('#form-kemasan').attr('action', clik.data('link'));
		$('#form-kemasan').find('input').val('');
		$('#form-kemasan').find('select').val('');
		$('#form-kemasan').find('textarea').val('');
		$('#showit').fadeIn();
		$('#produk').removeAttr('disabled');
		$('#subproduk').removeAttr('disabled');
		$('#kemasan').attr('readonly','readonly');
		lproduk('produk');
		$('#produk').change(function(event) {
			let beh = $(this).val();
			$('#produk_in').val(beh);
			lsubproduk('subproduk',beh);
			$('#subproduk').change(function(event) {
				let abe = $(this).val();
				$('#subproduk_in').val(abe);
			});
		});
		$("#tanggal_hiskem").datetimepicker({
			format: 'YYYY-MM-DD HH:mm:ss',
		}).data("DateTimePicker").date(new Date());
		$('#modal-kemasan').modal('show');
	});

	$(document).on('click', '#update-kemasan', function(event) {
		let clik = $(this);
		$('.modal-header #title').text(clik.data('title'));
		$('#form-kemasan').attr('action', clik.data('link'));
		$('#form-kemasan').find('input').val('');
		$('#form-kemasan').find('select').val('');
		$('#form-kemasan').find('textarea').val('');
		$('#showit').hide();
		$('#produk').attr('disabled','disabled');
		$('#subproduk').attr('disabled','disabled');
		$('#kemasan').attr('readonly');

		lproduk('produk',clik.data('produk'));
		lsubproduk('subproduk',clik.data('produk'),clik.data('subproduk'));
		getseacrhdatarow(1,clik.data('produk'),clik.data('subproduk'),1,clik.data('hargaid'));

		$("#tanggal_hiskem").datetimepicker({
			format: 'YYYY-MM-DD HH:mm:ss',
		}).data("DateTimePicker").date(clik.data('tanggal'));

		$('#produk_in').val(clik.data('produk'));
		$('#subproduk_in').val(clik.data('subproduk'));
		$('#kemasan').val(clik.data('nama_kemasan'));
		$('#kemasan_in').val(clik.data('kemasan'));
		$('#stock_awl').val(clik.data('stock'));
		$('#masuk').val(clik.data('masuk'));
		$('#terpakaikemasan').val(clik.data('terpakai'));
		$('#afkir').val(clik.data('afkir'));
		$('#ket_kem').val(clik.data('ket_kem'));

		$('#modal-kemasan').modal('show');
	});

	$(document).on('click', '#delete-kemasan', function(event) {
		let gsh = confirm('Apakah anda yakin ingin menghapus data ini ?');
		if (gsh) {
			let id = $(this).data("id");
			let link = $(this).data("link");
			let produk_in = $(this).data('produk');
			let kemasan_in = $(this).data('kemasan');
			let subproduk_in = $('#subproduk_in').val();
			$.ajax(
			{
				url: link,
				type: 'DELETE',
				data: {
					"_token": token,
					produk: produk_in,
					subproduk: subproduk_in,
					kemasan : kemasan_in,
				},
				beforeSend: function() {
					isProcessing = true;
					$('#loadingdelete').fadeIn();
				},
				success: function (data){
					$('#loadingdelete').hide();
					isProcessing = false;
					alert(data);
					$('#table_kemasan').DataTable().ajax.reload();
				}
			});
		}
	});
</script>