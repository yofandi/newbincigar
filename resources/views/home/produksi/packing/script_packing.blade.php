<script>
	$.extend(true, $.fn.datetimepicker.defaults, {
		icons: {
			time: 'far fa-clock',
			date: 'far fa-calendar',
			up: 'fas fa-arrow-up',
			down: 'fas fa-arrow-down',
			previous: 'fas fa-chevron-left',
			next: 'fas fa-chevron-right',
			today: 'fas fa-calendar-check',
			clear: 'far fa-trash-alt',
			close: 'far fa-times-circle'
		}
	});
	// tampilan yang dihidden
	$('#stock_showit').hide();
	$('#loadingdeletepack').hide();
	$('.spinner_nekrpack').hide();
	$('.spinner_nekraddrfs').hide();
	$('#loadingdeletecart').hide();
	$('.spinner_nekrcart').hide();
	$('#v_blend2').hide();

	$('#batangsingle').hide();
	$('#batangeks').hide();

	$('#load_single').hide();
	$('#load_eklusif').hide();
	$('#load_bos').hide();
	$('#load_hvn').hide();
	/* end */
	let token = $("meta[name='csrf-token']").attr("content");
	let auth = {{ Auth::user()->id }};

	const produk = {!! json_encode($produk) !!}
	produk.unshift({id_produk: '',produk: ''});

	// table packing
	var table = $('#table-packing').DataTable({
		"lengthMenu": [
		[3,5,10, 25, 50, 100, 150, 200, -1],
		[3,5,10, 25, 50, 100, 150, 200, "All"]
		],
		"order": [
		[0, 'desc']
		],
		processing: true,
		serverSide: true,
		ajax: '/datapacking/json',
		columns: [
		{ data: 'DT_RowIndex', name: 'DT_RowIndex' },
		{ data: 'tanggal_packing', name: 'tanggal_packing' },
		{ data: 'sub_kode', name: 'sub_kode' },
		{ data: 'sub_produk', name: 'sub_produk' },
		{ data: 'nama_kemasan', name: 'nama_kemasan' },
		{ data: 'isi', name: 'isi' },
		{ data: 'tambah_packing', name: 'tambah_packing' },
		{ data: 'hasil_packing', name: 'hasil_packing' },
		{ data: 'keterangan_packing', name: 'keterangan_packing' },
		{
			data: null,
			"bSortable": false,
			mRender: function (o) { 
				// ini untuk button update dan delete packing table
				$var = o.id +'/'+ o.idhiscukai +'/'+ o.tambah_packing +'/'+ o.masuk_cukai +'/'+ o.terpakai_cukailama +'/'+ o.terpakai_cukaibaru +'/'+ o.idhiskem +'/'+ o.masuk_kemasan +'/'+ o.terpakai_kemasan +'/'+ o.afkir_kemasan +'/'+ o.idhissti +'/'+ o.masuk_luar +'/'+ o.pakai_luar +'/'+ o.masuk_dalam +'/'+ o.pakai_dalam +'/'+ o.idhiscin +'/'+ o.masuk_cincin +'/'+ o.terpakai_cincin +'/'+ o.afkir_cincin;
				$var1 =  o.id +'/'+ o.stock_cukai_id_stock_cukai +'/'+ o.tambah_packing +'/'+ o.masuk_cukai +'/'+ o.terpakai_cukailama +'/'+ o.terpakai_cukaibaru +'/'+ o.stock_kemasan_id_stock_kemasan +'/'+ o.masuk_kemasan +'/'+ o.terpakai_kemasan +'/'+ o.afkir_kemasan +'/'+ o.stock_stiker_id_stock_stiker +'/'+ o.masuk_luar +'/'+ o.pakai_luar +'/'+ o.masuk_dalam +'/'+ o.pakai_dalam +'/'+ o.stock_cincin_id_stock_cincin +'/'+ o.masuk_cincin +'/'+ o.terpakai_cincin +'/'+ o.afkir_cincin +'/'+ o.stockawal +'/'+ o.stockawalcin +'/'+ o.stockawlluarsti +'/'+ o.stockawldalamsti +'/'+ o.stockawalkem;
				$button = '<button type="button" class="btn btn-info btn-rounded btn-xs" id="update-packing"';
				$button += ' data-title="Update Packing | ID: '+ o.id +'"';
				$button += ' data-link="/datapacking/upp/'+ $var +'"';
				$button += ' data-id="'+ o.id +'"';
				$button += ' data-tanggal_packing="'+ o.tanggal_packing +'"';
				$button += ' data-produk="'+ o.id_produk +'"';
				$button += ' data-subproduk="'+ o.id_sub_produk +'"';
				$button += ' data-idqc="'+ o.stock_qc_id +'"';
				$button += ' data-idpacking="'+ o.stock_packing_id +'"';
				$button += ' data-idkemasan="'+ o.id_kemasan +'"';
				$button += ' data-kemasan="'+ o.nama_kemasan +'"';
				$button += ' data-awlpacking="'+ o.awlpacking +'"';
				// $button += ' data-awlbatang="'+ o.awlbatang +'"';
				$button += ' data-jmlpack="'+ o.tambah_packing +'"';
				$button += ' data-isi="'+ o.isi +'"';
				$button += ' data-masuk="'+ o.masuk_cukai +'"';
				$button += ' data-terpakailama="'+ o.terpakai_cukailama +'"';
				$button += ' data-terpakaibaru="'+ o.terpakai_cukaibaru +'"';
				$button += ' data-terpakaikemasan="'+ o.terpakai_kemasan +'"';
				$button += ' data-terpakaicincin="'+ o.terpakai_cincin +'"';
				$button += ' data-stiluar="'+ o.pakai_luar +'"';
				$button += ' data-stidalam="'+ o.pakai_dalam +'"';
				$button += ' data-ket="'+ o.keterangan_packing +'"';
				// $button += ' data-hasil_batangpack="'+ o.hasil_batangpack_obj +'"';
				// id bahan baku
				$button += ' data-idcukai="'+ o.stock_cukai_id_stock_cukai +'"';
				$button += ' data-idstokemasan="'+ o.idhiskem +'"';
				$button += ' data-idstiker="'+ o.idhissti +'"';
				$button += ' data-idcincin="'+ o.idhiscin +'"';
				// stok bahan baku
				$button += ' data-stockawl="'+ o.stockawal +'"';
				$button += ' data-stockawalcin="'+ o.stockawalcin +'"';
				$button += ' data-stockawlluarsti="'+ o.stockawlluarsti +'"';
				$button += ' data-stockawldalamsti="'+ o.stockawldalamsti +'"';
				$button += ' data-stockawalkem="'+ o.stockawalkem +'"';
				$button += '>';
				$button += '<i class="mdi mdi-dots-horizontal"></i>';
				$button += '</button>';
				$button += '<button type="button" class="btn btn-danger btn-rounded btn-xs"';
				$button += ' id="delete-packing"';
				$button += ' data-link="/datapacking/del/'+ $var1 +'"';
				$button += ' data-id="'+ o.id +'"';
				$button += ' data-produk="'+ o.id_produk +'"';
				$button += ' data-subproduk="'+ o.id_sub_produk +'"';
				$button += ' data-idqc="'+ o.stock_qc_id +'"';
				$button += ' data-idpacking="'+ o.stock_packing_id +'"';
				$button += ' data-idcukai="'+ o.stock_cukai_id_stock_cukai +'"';
				$button += ' data-isi="'+ o.isi +'"';
				$button += '>';
				$button += '<i class="fa fa-trash"></i>';
				$button += '</button>';
				return $button; }
			},
			],
			"displayLength": 10,
		});
$('#table-packing tbody').on('click', 'tr.group', function() {
	var currentOrder = table.order()[0];
	if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
		table.order([2, 'desc']).draw();
	} else {
		table.order([2, 'asc']).draw();
	}
});
$('#table-packing tbody').on('click', 'tr.group', function() {
	var currentOrder = table.order()[0];
	if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
		table.order([2, 'desc']).draw();
	} else {
		table.order([2, 'asc']).draw();
	}
});

function Refresh_packing() {
	$('#table-packing').DataTable().ajax.reload();
}
// end

function empty_val() {
	$('#load_single').find('input, textarea').val('');
	$('#load_hvn').find('input, textarea').val('');
	$('#load_bos').find('input, textarea').val('');
	$('#load_eklusif').find('input, textarea').val('');
}


// form inputan packing
function single(position1,position2,position3,position4,position5,position6,position7,data) {
	$('#modal-button-packing').attr('onclick', 'save_method()');

	$('#idcukai').val(data['rety'].idcukai);
	$('#idqc').val(data['rety'].idqc);
	$('#idpacking').val(data['rety'].idpack);
	$('#kemasan').val(data['rety'].kemasan);
	$('#isikemasan').val(data['rety'].isi);
	$('#idkemasan').val(data['rety'].kemas_id);
	$('#idcincin').val(data['rety'].idcincin);
	$('#idstiker').val(data['rety'].idstiker);
	$('#idstokemasan').val(data['rety'].idkemasan);
	$('#' + position1).val(data['rety'].jmlcukai);
	$('#' + position2).val(data['rety'].jmlqc);
	$('#' + position3).val(data['rety'].jmlpack);
	$('#' + position4).val(data['rety'].jmlkemasan);
	$('#' + position5).val(data['rety'].jmlcincin);
	$('#' + position6).val(data['rety'].jmlluarsti);
	$('#' + position7).val(data['rety'].jmldalamsti);
}

function eklusif(data) {
	$('#idqceks').val(data['rety'].idqc);
	$('#idpackingeks').val(data['rety'].idpack);

	$('#idkemasaneks').val(data['rety'].kemas_id);
	$('#kemasaneks').val(data['rety'].kemasan);
	$('#isikemasaneks').val(data['rety'].isi);

	$('#stockawaleks').val(data['rety'].jmlpack);

	$('#idcincineks').val(data['rety'].idcincin);
	$('#stockcincineks').val(data['rety'].jmlcincin);

	$('#idstokemasaneks').val(data['rety'].idkemasan);
	$('#stockkemasaneks').val(data['rety'].jmlkemasan);

	$('#idstikereks').val(data['rety'].idstiker);
	$('#stockluarstieks').val(data['rety'].jmlluarsti);
	$('#stockdalamstieks').val(data['rety'].jmldalamsti);

	$('#batangrbs_eks').val(data['rety'].jmlqc[11]);
	$('#batanghlf_eks').val(data['rety'].jmlqc[13]);
	$('#batangmmr_eks').val(data['rety'].jmlqc[14]);
}

function bos(data) {
	$('#idpackingbos').val(data['rety'].idpack);

	$('#idkemasanbos').val(data['rety'].kemas_id);
	$('#kemasanbos').val(data['rety'].kemasan);
	$('#isikemasanbos').val(data['rety'].isi);

	$('#stockawalbos').val(data['rety'].jmlpack);

	$('#idcincinbos').val(data['rety'].idcincin);
	$('#stockcincinbos').val(data['rety'].jmlcincin);

	$('#idstokemasanbos').val(data['rety'].idkemasan);
	$('#stockkemasanbos').val(data['rety'].jmlkemasan);

	$('#idstikerbos').val(data['rety'].idstiker);
	$('#stockluarstibos').val(data['rety'].jmlluarsti);
	$('#stockdalamstibos').val(data['rety'].jmldalamsti);

	$tyu = {
		11 : '#batangrbs_bos',
		12 : '#batangcor_bos',
		13 : '#batanghlf_bos',
		15 : '#batangcgr_bos',
		16 : '#batangelnino_bos',
		19 : '#batangc99_bos'
	};
	$.each($tyu,function( index , key ) {
		$(key).val(data['rety'].jmlqc[index]);
	});
}

function havano(data) {
	$('#idpackinghvn').val(data['rety'].idpack);

	$('#idkemasanhvn').val(data['rety'].kemas_id);
	$('#kemasanhvn').val(data['rety'].kemasan);
	$('#isikemasanhvn').val(data['rety'].isi);

	$('#stockawalhvn').val(data['rety'].jmlpack);

	$('#idcincinhvn').val(data['rety'].idcincin);
	$('#stockcincinhvn').val(data['rety'].jmlcincin);

	$('#idstokemasanhvn').val(data['rety'].idkemasan);
	$('#stockkemasanhvn').val(data['rety'].jmlkemasan);

	$('#idstikerhvn').val(data['rety'].idstiker);
	$('#stockluarstihvn').val(data['rety'].jmlluarsti);
	$('#stockdalamstihvn').val(data['rety'].jmldalamsti);

	$tyu = {
		11 : '#batangrbs_hvn',
		12 : '#batangcor_hvn',
		13 : '#batanghlf_hvn',
		15 : '#batangcgr_hvn',
		16 : '#batangelnino_hvn',
	};
	$.each($tyu,function( index , key ) {
		$(key).val(data['rety'].jmlqc[index]);
	});	
}

function getdatasearch(position1,position2,position3,position4,position5,position6,position7) {
	let subproduk = $('#subproduk_in').val();
	let produk = $('#produk_in').val();

	$.ajax({
		url: '/datapacking/getsearchdata',
		type: 'POST',
		dataType: 'json',
		data: {
			"_token": token,
			produk: produk,
			subproduk: subproduk
		},
	})
	.done(function(data) {
		if (produk == '' || subproduk == '') { 
			empty_val();
			$('#load_single').hide();
			$('#load_eklusif').hide();
			$('#load_bos').hide();
			$('#load_hvn').hide();
		} else {
			if (data['stst'] == 'single') {
				$('#batangsingle').fadeIn();
				empty_val();
				single(position1,position2,position3,position4,position5,position6,position7,data);
				$('#load_bos').hide();
				$('#load_eklusif').hide();
				$('#load_single').fadeIn();
			} else  {
				if (data['stst'] == 'eklus') {
					$('#batangeks').fadeIn();
					empty_val();
					$('#load_hvn').hide();
					$('#load_bos').hide();
					eklusif(data);
					$('#load_eklusif').fadeIn();
				} else if (data['stst'] == 'bos') {
					empty_val();
					$('#load_hvn').hide();
					$('#load_eklusif').hide();
					bos(data);
					$('#load_bos').fadeIn();
				} else if (data['stst'] == 'havano') {
					empty_val();
					$('#load_bos').hide();
					$('#load_eklusif').hide();
					havano(data);
					$('#load_hvn').fadeIn();
				}
				$('#load_single').hide();
			}
		}
	});

}
// end

// bagian untuk kirim ke RFS
function getforinrfs(idpack,position) {
	let produk = $('#produk_inrfs').val();
	let subproduk = $('#subproduk_inrfs').val();

	$.ajax({
		url: '/datapacking/getsearchdata',
		type: 'POST',
		dataType: 'json',
		data: {
			"_token": token,
			produk: produk,
			subproduk: subproduk
		},
	})
	.done(function(data) {
		$('#' + idpack).val(data['rety'].idpack);
		$('#' + position).val(data['rety'].jmlpack);
		// if (data['stst'] == 'single') {
		// } else  {
		// 	if (data['stst'] == 'eklus') {
		// 	} else if (data['stst'] == 'bos') {
		// 	} else if (data['stst'] == 'havano') {
		// 	}
		// }
	});
	
}
// end

// bagian tambah dan update packing
function lproduk(position,like) {
	let opt = '';
	for (let i = 0; i < produk.length; i++) {
		opt += '<option value="'+ produk[i].id_produk +'"';
		if (produk[i].id_produk == like) {
			opt += 'selected';
		}
		opt += '>'+ produk[i].produk +'</option>';
	}
	$('#' + position).html(opt);
}

function lsubproduk(position,based,liker = '') {
	$.ajax({
		url: '/datapacking/getsubproduk',
		type: 'POST',
		dataType: 'json',
		data: {
			"_token": token,
			produk: based
		},
	})
	.done(function(data) {
		let opt = '<option value=""></option>';
		for(let i = 0; i < data.length; i++) {
			opt += '<option value="'+ data[i].id_sub_produk +'" data-name="'+ data[i].sub_kode +' - '+ data[i].sub_produk +'" ';
			if (data[i].id_sub_produk == liker) {opt += 'selected ';}
			opt += '>'+ data[i].sub_kode +' - '+ data[i].sub_produk +'</option>';
		}
		$('#' + position).html(opt);
	});
}
// end

// table session cart kirim RFS
function getsession() {
	$.ajax({
		url: 'api/noc_inrfs/all',
		type: 'GET',
		dataType: 'json',
	})
	.done(function(datas) {
		$('#data_cart').html(datas);
	});
}
// end

// bagian button untuk cari data dibagian tambah packing
function seacrh_data() {
	getdatasearch('stockcukai','batangqc','stockawal','stockkemasan','stockcincin','stockluarsti','stockdalamsti');
}
// end

// bagian button untuk cari data dibagian tambah produk kirim rfs
function showforinrfs() {
	getforinrfs('idpackinginrfs','stockjmlpack_for');
}
// end

// method untuk save dan update data packing
function save_eklusif(link,tanggal_packing,subproduk_in,produk_in) {
	let idkemasan = $('#idkemasaneks').val();
	let idpacking = $('#idpackingeks').val();

	let batangrobus = $('#batangrbs_eks').val();
	let batanghalf = $('#batanghlf_eks').val();
	let batangmaumere = $('#batangmmr_eks').val();

	let stockawal = $('#stockawaleks').val();
	let jmlpacking = $('#jmlpackingeks').val();
	let isikemasan = $('#isikemasaneks').val();
	let masuk = 0;
	let afkir = 0;
	let masukluar = 0;
	let masukdalam = 0;
	let ket = $('#ket_eks').val();
	// stok bahan baku
	let stockkemasan = $('#stockkemasaneks').val();
	let stockcincin = $('#stockcincineks').val();
	let stockluarsti = $('#stockluarstieks').val();
	let stockdalamsti = $('#stockdalamstieks').val();
	//ini buat kemasan
	let idstokemasan = $('#idstokemasaneks').val();
	let terpakaikemasan = $('#terpakaikemasaneks').val();
	//ini buat cincin
	let idcincin = $('#idcincineks').val();
	let terpakaicincin = $('#terpakaicincineks').val();
	//ini buat stiker
	let idstiker = $('#idstikereks').val();
	let terpakailuar = $('#terpakailuareks').val();
	let terpakaidalam = $('#terpakaidalameks').val();
	$.ajax({
		url: link,
		type: 'POST',
		dataType: 'json',
		data: {
			"_token": token,
			tanggal_packing: tanggal_packing,
			produk: produk_in,
			subproduk: subproduk_in,

			//batang robusto, half corona, maumere
			batangrobus: batangrobus,
			batanghalf: batanghalf,
			batangmaumere: batangmaumere,

			stockawal: stockawal,
			jmlpacking: jmlpacking,
			isikemasan: isikemasan,
			ket:ket,
			// id
			idpacking: idpacking,
			kemasan: idkemasan,
			idcincin: idcincin,
			idstiker: idstiker,
			idstokemasan: idstokemasan,
			// isi bahan baku
			masuk: masuk,
			afkir: afkir,
			terpakailuar: terpakailuar,
			terpakaidalam: terpakaidalam,
			terpakaikemasan: terpakaikemasan,
			terpakaicincin: terpakaicincin,
			masukluar: masukluar,
			masukdalam: masukdalam,
			//stok bahan baku
			stockkemasan: stockkemasan,
			stockcincin: stockcincin,
			stockluarsti: stockluarsti,
			stockdalamsti: stockdalamsti,
		},
		beforeSend: function() {
			isProcessing = true;
			$('.spinner_nekrpack').fadeIn();
		},
		success: function(data) {
			$('.spinner_nekrpack').hide();
			isProcessing = false;
			console.log(data);
			alert(data);
			Refresh_packing();
			$('#modal-packing').modal('toggle');
		}
	});
}

function save_bos(link,tanggal_packing,subproduk_in,produk_in) {
	let idpacking = $('#idpackingbos').val();

	let idkemasan = $('#idkemasanbos').val();

	let batangrbs_bos = $('#batangrbs_bos').val();
	let batangcor_bos = $('#batangcor_bos').val();
	let batanghlf_bos = $('#batanghlf_bos').val();
	let batangcgr_bos = $('#batangcgr_bos').val();
	let batangelnino_bos = $('#batangelnino_bos').val();
	let batangc99_bos = $('#batangc99_bos').val();

	let stockawal = $('#stockawalbos').val();
	let jmlpacking = $('#jmlpackingbos').val();
	let isikemasan = $('#isikemasanbos').val();
	let masuk = 0;
	let afkir = 0;
	let masukluar = 0;
	let masukdalam = 0;
	let ket = $('#ket_bos').val();
	// stok bahan baku
	let stockkemasan = $('#stockkemasanbos').val();
	let stockcincin = $('#stockcincinbos').val();
	let stockluarsti = $('#stockluarstibos').val();
	let stockdalamsti = $('#stockdalamstibos').val();
	//ini buat kemasan
	let idstokemasan = $('#idstokemasanbos').val();
	let terpakaikemasan = $('#terpakaikemasanbos').val();
	//ini buat cincin
	let idcincin = $('#idcincinbos').val();
	let terpakaicincin = $('#terpakaicincinbos').val();
	//ini buat stiker
	let idstiker = $('#idstikerbos').val();
	let terpakailuar = $('#terpakailuarbos').val();
	let terpakaidalam = $('#terpakaidalambos').val();

	$.ajax({
		url: link,
		type: 'POST',
		dataType: 'json',
		data: {
			"_token": token,
			tanggal_packing: tanggal_packing,
			produk: produk_in,
			subproduk: subproduk_in,

			//batang robusto, corona, half corona, cigar master, el nino, c99
			batangrbs_bos: batangrbs_bos,
			batangcor_bos: batangcor_bos,
			batanghlf_bos: batanghlf_bos,
			batangcgr_bos: batangcgr_bos,
			batangelnino_bos: batangelnino_bos,
			batangc99_bos: batangc99_bos,

			stockawal: stockawal,
			jmlpacking: jmlpacking,
			isikemasan: isikemasan,
			ket:ket,
			// id
			idpacking: idpacking,
			kemasan: idkemasan,
			idcincin: idcincin,
			idstiker: idstiker,
			idstokemasan: idstokemasan,
			// isi bahan baku
			masuk: masuk,
			afkir: afkir,
			terpakailuar: terpakailuar,
			terpakaidalam: terpakaidalam,
			terpakaikemasan: terpakaikemasan,
			terpakaicincin: terpakaicincin,
			masukluar: masukluar,
			masukdalam: masukdalam,
			//stok bahan baku
			stockkemasan: stockkemasan,
			stockcincin: stockcincin,
			stockluarsti: stockluarsti,
			stockdalamsti: stockdalamsti,
		},
		beforeSend: function() {
			isProcessing = true;
			$('.spinner_nekrpack').fadeIn();
		},
		success: function(data) {
			$('.spinner_nekrpack').hide();
			isProcessing = false;
			console.log(data);
			alert(data);
			Refresh_packing();
			$('#modal-packing').modal('toggle');
		}
	});
}

function save_havano(link,tanggal_packing,subproduk_in,produk_in) {
	let idpacking = $('#idpackinghvn').val();

	let idkemasan = $('#idkemasanhvn').val();

	let batangrbs_hvn = $('#batangrbs_hvn').val();
	let batangcor_hvn = $('#batangcor_hvn').val();
	let batanghlf_hvn = $('#batanghlf_hvn').val();
	let batangcgr_hvn = $('#batangcgr_hvn').val();
	let batangelnino_hvn = $('#batangelnino_hvn').val();

	let stockawal = $('#stockawalhvn').val();
	let jmlpacking = $('#jmlpackinghvn').val();
	let isikemasan = $('#isikemasanhvn').val();
	let masuk = 0;
	let afkir = 0;
	let masukluar = 0;
	let masukdalam = 0;
	let ket = $('#ket_hvn').val();
	// stok bahan baku
	let stockkemasan = $('#stockkemasanhvn').val();
	let stockcincin = $('#stockcincinhvn').val();
	let stockluarsti = $('#stockluarstihvn').val();
	let stockdalamsti = $('#stockdalamstihvn').val();
	//ini buat kemasan
	let idstokemasan = $('#idstokemasanhvn').val();
	let terpakaikemasan = $('#terpakaikemasanhvn').val();
	//ini buat cincin
	let idcincin = $('#idcincinhvn').val();
	let terpakaicincin = $('#terpakaicincinhvn').val();
	//ini buat stiker
	let idstiker = $('#idstikerhvn').val();
	let terpakailuar = $('#terpakailuarhvn').val();
	let terpakaidalam = $('#terpakaidalamhvn').val();

	$.ajax({
		url: link,
		type: 'POST',
		dataType: 'json',
		data: {
			"_token": token,
			tanggal_packing: tanggal_packing,
			produk: produk_in,
			subproduk: subproduk_in,

			//batang robusto, corona, half corona, cigar master, el nino, c99
			batangrbs_hvn: batangrbs_hvn,
			batangcor_hvn: batangcor_hvn,
			batanghlf_hvn: batanghlf_hvn,
			batangcgr_hvn: batangcgr_hvn,
			batangelnino_hvn: batangelnino_hvn,

			stockawal: stockawal,
			jmlpacking: jmlpacking,
			isikemasan: isikemasan,
			ket:ket,
			// id
			idpacking: idpacking,
			kemasan: idkemasan,
			idcincin: idcincin,
			idstiker: idstiker,
			idstokemasan: idstokemasan,
			// isi bahan baku
			masuk: masuk,
			afkir: afkir,
			terpakailuar: terpakailuar,
			terpakaidalam: terpakaidalam,
			terpakaikemasan: terpakaikemasan,
			terpakaicincin: terpakaicincin,
			masukluar: masukluar,
			masukdalam: masukdalam,
			//stok bahan baku
			stockkemasan: stockkemasan,
			stockcincin: stockcincin,
			stockluarsti: stockluarsti,
			stockdalamsti: stockdalamsti,
		},
		beforeSend: function() {
			isProcessing = true;
			$('.spinner_nekrpack').fadeIn();
		},
		success: function(data) {
			$('.spinner_nekrpack').hide();
			isProcessing = false;
			console.log(data);
			alert(data);
			Refresh_packing();
			$('#modal-packing').modal('toggle');
		}
	});
}

function save_single(link,tanggal_packing,subproduk_in,produk_in) {
	let idpacking = $('#idpacking').val();
	let idkemasan = $('#idkemasan').val();
	let batangqc = $('#batangqc').val();
	let stockawal = $('#stockawal').val();
	let jmlpacking = $('#jmlpacking').val();
	let isikemasan = $('#isikemasan').val();

	let idcukai = $('#idcukai').val();
	let idqc = $('#idqc').val();
	// $('#masuksame').val()
	let masuk = 0;
	let afkir = 0;
	let masukluar = 0;
	let masukdalam = 0;
	let terpakailama = $('#terpakailama').val();
	let terpakaibaru = $('#terpakaibaru').val();
	let ket = $('#ket').val();
	// stok bahan baku
	let stockcukai = $('#stockcukai').val();
	let stockkemasan = $('#stockkemasan').val();
	let stockcincin = $('#stockcincin').val();
	let stockluarsti = $('#stockluarsti').val();
	let stockdalamsti = $('#stockdalamsti').val();
	//ini buat kemasan
	let idstokemasan = $('#idstokemasan').val();
	let terpakaikemasan = $('#terpakaikemasan').val();
	//ini buat cincin
	let idcincin = $('#idcincin').val();
	let terpakaicincin = $('#terpakaicincin').val();
	//ini buat stiker
	let idstiker = $('#idstiker').val();
	let terpakailuar = $('#terpakailuar').val();
	let terpakaidalam = $('#terpakaidalam').val();
	$.ajax({
		url: link,
		type: 'POST',
		dataType: 'json',
		data: {
			"_token": token,
			tanggal_packing: tanggal_packing,
			produk: produk_in,
			subproduk: subproduk_in,
			batangqc: batangqc,
			stockawal: stockawal,
			jmlpacking: jmlpacking,
			isikemasan: isikemasan,
			ket:ket,
			// id
			idpacking: idpacking,
			idqc: idqc,
			idcukai: idcukai,
			kemasan: idkemasan,
			idcincin: idcincin,
			idstiker: idstiker,
			idstokemasan: idstokemasan,
			// isi bahan baku
			masuk: masuk,
			afkir: afkir,
			terpakailama: terpakailama,
			terpakaibaru: terpakaibaru,
			terpakailuar: terpakailuar,
			terpakaidalam: terpakaidalam,
			terpakaikemasan: terpakaikemasan,
			terpakaicincin: terpakaicincin,
			masukluar: masukluar,
			masukdalam: masukdalam,
			//stok bahan baku
			stockcukai: stockcukai,
			stockkemasan: stockkemasan,
			stockcincin: stockcincin,
			stockluarsti: stockluarsti,
			stockdalamsti: stockdalamsti,
		},
		beforeSend: function() {
			isProcessing = true;
			$('.spinner_nekrpack').fadeIn();
		},
		success: function(data) {
			$('.spinner_nekrpack').hide();
			isProcessing = false;
			alert(data);
			console.log(data);
			Refresh_packing();
			$('#modal-packing').modal('toggle');
		}
	});
}

function save_method() {
	let link = $('#form-packing').attr('action');
	let tanggal_packing = $('#tanggal_packing').val();

	let subproduk_in = $('#subproduk_in').val();
	let produk_in = $('#produk_in').val();
	if (produk_in != 54) {
		save_single(link,tanggal_packing,subproduk_in,produk_in);
	} else {
		if (subproduk_in == 47) {
			save_eklusif(link,tanggal_packing,subproduk_in,produk_in);
		} else if (subproduk_in == 48) {
			save_bos(link,tanggal_packing,subproduk_in,produk_in);
		} else if (subproduk_in == 49) {
			save_havano(link,tanggal_packing,subproduk_in,produk_in);
		}
	}
}
// end

// show adn hidden tampilan kirim RFS
function change_position() {
	let div = $('#v_blend2');

	div.fadeToggle(500, function() {
		$('#noc_inrfs').find('input').val('');
		$('#noc_inrfs').find('select').val('');

		$("#tanggal_inrfs").datetimepicker({
			format: 'YYYY-MM-DD HH:mm:ss',
		}).data("DateTimePicker").date(new Date());
		lproduk('produk_inrfs');
		$(document).on('change', '#produk_inrfs', function(event) {
			let val = $(this).val();
			$('#produk_hidinrfs').val(val);
			lsubproduk('subproduk_inrfs',val);
			$(document).on('change', '#subproduk_inrfs', function(event) {
				let lke = $(this).val();
				let lep = $(this).find(':selected').data('name');

				$('#namesubproduk_hid').val(lep);
				$('#subproduk_hidinrfs').val(lke);
			});
		});
		getsession();
	});
}
// end

// untuk simpan ketable session kirim rfs
function save_sessionker() {
		// let tanggal = $('#tanggal_inrfs').val();
		let idpacking = $('#idpackinginrfs').val();
		let subproduk = $('#subproduk_hidinrfs').val();
		let name = $('#namesubproduk_hid').val();
		let stock = $('#stockcukai_for').val();
		let jml = $('#jml_keluar').val();
		
		$.ajax({
			url: '/datapacking/addinrfs',
			type: 'POST',
			dataType: 'json',
			data: {
				"_token": token,
				// tanggal: tanggal,
				idpacking: idpacking,
				subproduk: subproduk,
				name: name,
				stock: stock,
				jml: jml,
			},
			beforeSend: function() {
				isProcessing = true;
				$('.spinner_nekraddrfs').fadeIn();
			},
			success: function (data){
				$('.spinner_nekraddrfs').hide();
				isProcessing = false;
				alert(data);
				getsession();
				$('#noc_inrfs').find('input').val('');
				$('#noc_inrfs').find('select').val('');
				// change_position();
			},
		});
	}
	// end 

	// simpan dan update data kirim rfs ke notifikasi
	function save_inrfs() {
		let tanggal_inrfs = $('#tanggal_inrfs').val();
		let keterangan = $('#keterangan').val();
		$.ajax({
			url: '/datapacking/saveinrfs',
			type: 'POST',
			dataType: 'json',
			data: {
				"_token": token,
				tanggal: tanggal_inrfs,
				keterangan: keterangan,
			},
			beforeSend: function() {
				isProcessing = true;
				$('.spinner_nekraddrfs').fadeIn();
			},
			success: function(data) {
				$('.spinner_nekraddrfs').hide();
				isProcessing = false;
				alert(data);
				change_position();
			}
		});
		
	}

	function update_sessionker() {
		let link = $('#link_updatecart').attr('action');
		let qty = $('#qtycart').val();
		$.ajax({
			url: link,
			type: 'POST',
			dataType: 'json',
			data: {
				"_token": token,
				jml: qty
			},
			beforeSend: function() {
				isProcessing = true;
				$('.spinner_nekrcart').fadeIn();
			},
			success: function (data){
				$('.spinner_nekrcart').hide();
				isProcessing = false;
				alert(data);
				getsession();
				$('#modal-cartpack').modal('toggle');
			}
		});
		
	}
	// end

	// button untuk tampikan save, update dan delete modal packing
	$(document).on('click', '#tambah-packing', function(event) {
		let clik = $(this);
		$('.modal-header #title').text('Tambah Packing');
		$('#form-packing').attr('action', clik.data('link'));
		$('#form-packing').find('input').val('');
		$('#form-packing').find('select').val('');
		$('#form-packing').find('textarea').val('');
		$("#tanggal_packing").datetimepicker({
			format: 'YYYY-MM-DD HH:mm:ss',
		}).data("DateTimePicker").date(new Date());
		$('#stock_showit').fadeIn();
		$('#produk').removeAttr('disabled');
		$('#subproduk').removeAttr('disabled');
		lproduk('produk');
		$(document).on('change', '#produk', function(event) {
			let jam = $(this).val();
			$('#produk_in').val(jam);
			lsubproduk('subproduk',jam);
			$(document).on('change', '#subproduk', function(event) {
				let pwe = $(this).val();
				$('#subproduk_in').val(pwe);
			});
		});
		$('#masuksame').val(0)
		$('#modal-packing').modal('show');
	});

	function showupdate_single(click) {
		$('#idqc').val(click.data('idqc'));
		$('#idpacking').val(click.data('idpacking'));
		$('#idkemasan').val(click.data('idkemasan'));
		$('#kemasan').val(click.data('kemasan'));
		$('#batangqc').val(click.data('awlbatang'));
		$('#jmlpacking').val(click.data('jmlpack'));
		$('#isikemasan').val(click.data('isi'));
		$('#masuksame').val(click.data('masuk'));
		$('#terpakailama').val(click.data('terpakailama'));
		$('#terpakaibaru').val(click.data('terpakaibaru'));

		$('#stockawal').val(click.data('awlpacking'));

		// id bahan baku
		$('#idcukai').val(click.data('idcukai'));
		$('#idstokemasan').val(click.data('idstokemasan'));
		$('#idcincin').val(click.data('idstiker'));
		$('#idstiker').val(click.data('idcincin'));

		// stock bahan baku
		$('#stockcukai').val(click.data('stockawl'));
		$('#stockcincin').val(click.data('stockawalcin'));
		$('#stockluarsti').val(click.data('stockawlluarsti'));
		$('#stockdalamsti').val(click.data('stockawldalamsti'));
		$('#stockkemasan').val(click.data('stockawalkem'));

		$('#terpakaikemasan').val(click.data('terpakaikemasan'));
		$('#terpakaicincin').val(click.data('terpakaicincin'));
		$('#terpakailuar').val(click.data('stiluar'));
		$('#terpakaidalam').val(click.data('stidalam'));
		$('#ket').val(click.data('ket'));
	}

	function showupdate_eklusif(click) {
		$('#idkemasaneks').val(click.data('idkemasan'));
		$('#kemasaneks').val(click.data('kemasan'));

		$('#idpackingeks').val(click.data('idpacking'));

		$('#stockkemasaneks').val(click.data('stockawalkem'));
		$('#stockcincineks').val(click.data('stockawalcin'));
		$('#stockluarstieks').val(click.data('stockawlluarsti'));
		$('#stockdalamstieks').val(click.data('stockawldalamsti'));

		// $batrobus = parseInt(click.data('jmlpack')) * 10;
		// $bathalf = parseInt(click.data('jmlpack')) * 5;
		// $batmmr = parseInt(click.data('jmlpack')) * 5;
		// $hasil = click.data('hasil_batangpack');

		$('#batangrbs_eks').val('');
		$('#batanghlf_eks').val('');
		$('#batangmmr_eks').val('');

		$('#stockawaleks').val(click.data('awlpacking'));

		$('#jmlpackingeks').val(click.data('jmlpack'));
		$('#isikemasaneks').val(click.data('isi'));

		$('#idstokemasaneks').val(click.data('idstokemasan'));
		$('#terpakaikemasaneks').val(click.data('terpakaikemasan'));

		$('#idcincineks').val(click.data('idcincin'));
		$('#terpakaicincineks').val(click.data('terpakaicincin'));

		$('#idstikereks').val(click.data('idstiker'));
		$('#terpakailuareks').val(click.data('stiluar'));
		$('#terpakaidalameks').val(click.data('stidalam'));

		$('#ket_eks').val(click.data('ket'));
	}

	function showupdate_bos(click) {
		$('#idkemasanbos').val(click.data('idkemasan'));
		$('#kemasanbos').val(click.data('kemasan'));

		$('#idpackingbos').val(click.data('idpacking'));

		$('#stockkemasanbos').val(click.data('stockawalkem'));
		$('#stockcincinbos').val(click.data('stockawalcin'));
		$('#stockluarstibos').val(click.data('stockawlluarsti'));
		$('#stockdalamstibos').val(click.data('stockawldalamsti'));

		$('#batangrbs_bos').val('');
		$('#batangcor_bos').val('');
		$('#batanghlf_bos').val('');
		$('#batangcgr_bos').val('');
		$('#batangelnino_bos').val('');
		$('#batangc99_bos').val('');

		$('#stockawalbos').val(click.data('awlpacking'));

		$('#jmlpackingbos').val(click.data('jmlpack'));
		$('#isikemasanbos').val(click.data('isi'));

		$('#idstokemasanbos').val(click.data('idstokemasan'));
		$('#terpakaikemasanbos').val(click.data('terpakaikemasan'));

		$('#idcincinbos').val(click.data('idcincin'));
		$('#terpakaicincinbos').val(click.data('terpakaicincin'));

		$('#idstikerbos').val(click.data('idstiker'));
		$('#terpakailuarbos').val(click.data('stiluar'));
		$('#terpakaidalambos').val(click.data('stidalam'));

		$('#ket_bos').val(click.data('ket'));
	}

	function showupdate_havano(click) {
		$('#idkemasanhvn').val(click.data('idkemasan'));
		$('#kemasanhvn').val(click.data('kemasan'));

		$('#idpackinghvn').val(click.data('idpacking'));

		$('#stockkemasanhvn').val(click.data('stockawalkem'));
		$('#stockcincinhvn').val(click.data('stockawalcin'));
		$('#stockluarstihvn').val(click.data('stockawlluarsti'));
		$('#stockdalamstihvn').val(click.data('stockawldalamsti'));

		$('#batangrbs_hvn').val('');
		$('#batangcor_hvn').val('');
		$('#batanghlf_hvn').val('');
		$('#batangcgr_hvn').val('');
		$('#batangelnino_hvn').val('');

		$('#stockawalhvn').val(click.data('awlpacking'));

		$('#jmlpackinghvn').val(click.data('jmlpack'));
		$('#isikemasanhvn').val(click.data('isi'));

		$('#idstokemasanhvn').val(click.data('idstokemasan'));
		$('#terpakaikemasanhvn').val(click.data('terpakaikemasan'));

		$('#idcincinhvn').val(click.data('idcincin'));
		$('#terpakaicincinhvn').val(click.data('terpakaicincin'));

		$('#idstikerhvn').val(click.data('idstiker'));
		$('#terpakailuarhvn').val(click.data('stiluar'));
		$('#terpakaidalamhvn').val(click.data('stidalam'));

		$('#ket_hvn').val(click.data('ket'));
	}

	$(document).on('click', '#update-packing', function(event) {
		empty_val();
		let click = $(this);
		$('.modal-header #title').text(click.data('title'));
		$('#form-packing').attr('action', click.data('link'));
		$('#form-packing').find('input').val('');
		$('#form-packing').find('select').val('');
		$('#form-packing').find('textarea').val('');

		$("#tanggal_packing").datetimepicker({
			format: 'YYYY-MM-DD HH:mm:ss',
		}).data("DateTimePicker").date(click.data('tanggal_packing'));

		lproduk('produk',click.data('produk'));
		lsubproduk('subproduk',click.data('produk'),click.data('subproduk'));
		$('#stock_showit').hide();
		$('#produk').attr('disabled','disabled');
		$('#subproduk').attr('disabled','disabled');
		$('#produk_in').val(click.data('produk'));
		$('#subproduk_in').val(click.data('subproduk'));

		if (click.data('produk') != 54) {
			$('#batangsingle').hide();
			$('#load_single').fadeIn();
			$('#load_eklusif').hide();
			$('#load_bos').hide();
			$('#load_hvn').hide();
			showupdate_single(click);
		} else {
			$('#batangeks').hide();
			$('#batangbos').hide();
			$('#batanghavano').hide();
			if (click.data('subproduk') == 47) {
				$('#load_single').hide();
				$('#load_eklusif').fadeIn();
				$('#load_bos').hide();
				$('#load_hvn').hide();
				showupdate_eklusif(click);
			} else if (click.data('subproduk') == 48) {
				$('#load_single').hide();
				$('#load_eklusif').hide();
				$('#load_bos').fadeIn();
				$('#load_hvn').hide();
				showupdate_bos(click);
			} else if (click.data('subproduk') == 49) {
				$('#load_single').hide();
				$('#load_eklusif').hide();
				$('#load_bos').hide();
				$('#load_hvn').fadeIn();
				showupdate_havano(click);
			}
		}
		$('#modal-packing').modal('show');
	});

	$(document).on('click', '#delete-packing', function(event) {
		let hks = confirm('Apakah anda yakin ingin menghapus data ini ?');
		if (hks) {
			let link = $(this).data("link");
			let subproduk_in = $(this).data('subproduk');
			let produk_in = $(this).data('produk');
			let idpacking = $(this).data('idpacking');
			let idqc = $(this).data('idqc');
			let isikemasan = $(this).data('isi');
			let idcukai = $(this).data('idcukai');
			$.ajax(
			{
				url: link,
				type: 'DELETE',
				data: {
					"_token": token,
					subproduk: subproduk_in,
					produk: produk_in,
					isikemasan: isikemasan,
					idpacking: idpacking,
					idqc: idqc,
					idcukai: idcukai
				},
				beforeSend: function() {
					isProcessing = true;
					$('#loadingdeletepack').fadeIn();
				},
				success: function (data){
					$('#loadingdeletepack').hide();
					isProcessing = false;
					alert(data);
					Refresh_packing();
				}
			});
		}
	});
	// end

	// update delete di cart kirim rfs
	$(document).on('click', '#updatecart', function(event) {
		let click = $(this);
		let title = 'Update QTY - ' + click.data('name');
		$('.modal-header #title-cart').text(title);
		$('#link_updatecart').attr('action', click.data('link'));
		$('#link_updatecart').find('input').val('');
		$('#qtycart').val(click.data('qty'));
		$('#modal-cartpack').modal('show');
	});

	$(document).on('click', '#deletecart', function(event) {
		let hks = confirm('Apakah anda yakin ingin menghapus data ini ?');
		if (hks) {
			let click = $(this);
			$.ajax({
				url: click.data('link'),
				type: 'DELETE',
				dataType: 'json',
				data: {
					"_token": token,
				},
				beforeSend: function() {
					isProcessing = true;
					$('#loadingdeletecart').fadeIn();
				},
				success: function (data){
					$('#loadingdeletecart').hide();
					isProcessing = false;
					alert(data);
					getsession();
				}
			});
		}
	});
	// end
</script>