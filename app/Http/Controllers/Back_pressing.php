<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;
use App\Pressing;
use App\Stock_binding;
use App\Stock_pressing;

class Back_pressing extends Controller
{
	public function spropres($request)
	{
		$qusrt = DB::table('stock_binding')
		->select('stock_binding.id','stock_binding.jml_bind')
		->join('jenis_has_produk','stock_binding.jenis_has_produk_id_jnpro','=','jenis_has_produk.id_jnpro')
		->where([['jenis_has_produk.jenis_id_jenis','=',$request->jenis],['jenis_has_produk.produk_id_produk','=',$request->produk]]);

		$qusrt1 = DB::table('stock_pressing')
		->select('stock_pressing.id','stock_pressing.jml_pres')
		->join('jenis_has_produk','stock_pressing.jenis_has_produk_id_jnpro','=','jenis_has_produk.id_jnpro')
		->where([['jenis_has_produk.jenis_id_jenis','=',$request->jenis],['jenis_has_produk.produk_id_produk','=',$request->produk]]);
		if ($qusrt->count() > 0) {
			$aem = $qusrt->first();
			$qer = $aem->jml_bind;
			$qwc = $aem->id;
		} else { $qer = 0; $qwc = '';}
		if ($qusrt1->count() > 0) {
			$aem1 = $qusrt1->first();
			$ber = $aem1->jml_pres;
			$jkd = $aem1->id;
		} else { $ber = 0; $jkd = ''; }
		$array = ['jml_bind' => $qer,'jml_pres' => $ber,'idbinding' => $qwc,'idpressing' => $jkd];
		return $array;
	}

	public function stockpres($request)
	{
		$bang = $request->stockbatang - $request->batangjml;
		$cekwrt = Stock_binding::find($request->idbinding);
		$cekwrt->jml_bind = $bang;
		$cekwrt->save();
		$resultidwrt = $cekwrt->id;
		if ($request->idpressing != '') {
			$id = $request->idpressing; 
			$show = Stock_pressing::where('id',$id)->first();
			$jml = $show->jml_pres + $request->batangjml;

			$cek = Stock_pressing::find($id);
			$cek->jml_pres = $jml;
			$cek->save();

			$resultid = $cek->id;
		} else {
			$jml = $request->batangjml;
			$save = new Stock_pressing;

			$save->jml_pres = $jml;
			$save->jenis_has_produk_id_jnpro = $request->idjnpro;
			$save->jenis_has_produk_id_jenis = $request->jenis;
			$save->jenis_has_produk_id_produk = $request->produk;
			$save->save();
			$resultid = $save->id;
		}
		return ['idbinding' => $resultidwrt,'idpressing' => $resultid,'jmlbind' => $bang,'jmlpres' => $jml];
	}

	public function ifuppstockpres($request,$batang)
	{
		if ($batang != $request->batangjml) {
			$shbind = Stock_binding::where('id',$request->idbinding)->first();
			$shpres = Stock_pressing::where('id',$request->idpressing)->first();

			if ($batang > $request->batangjml) {
				$nah = $batang - $request->batangjml;
				$hasilw = $shpres->jml_pres - $nah;
				$slash = Stock_binding::find($request->idbinding);
				$slash->jml_bind = $shbind->jml_bind + $nah;
				$slash->save();
			} elseif ($batang < $request->batangjml) {
				$nah = $request->batangjml - $batang;
				$hasilw = $shpres->jml_pres + $nah;
				$slash = Stock_binding::find($request->idbinding);
				$slash->jml_bind = $shbind->jml_bind - $nah;
				$slash->save();
			} else {
				$hasilw = $shpres->jml_pres;
			}
			$query2 = Stock_pressing::find($request->idpressing);
			$query2->jml_pres = $hasilw;
			$query2->save();
		}
	}

	public function loop_uppres($request,$id,$batang = '')
	{
		$ceh = Pressing::where([['stock_binding_id','=',$request->idbinding],['stock_pressing_id','=',$request->idpressing],['id','>',$id]]);
		if ($ceh->count() > 0) {
			$reg = $ceh->get();
			for ($i=0; $i < $ceh->count(); $i++) {
				$show = Pressing::where([['stock_binding_id','=',$request->idbinding],['stock_pressing_id','=',$request->idpressing],['id','<',$reg[$i]->id]])->orderBy('id','desc')->take('1');
				if ($show->count() > 0) {
					$first = $show->first();
					$hsie = $first->hasil_akhirp - $reg[$i]->tambah_pres;
				} else {
					if ($batang == '') {
						$awl1 = $reg[$i]->hasil_akhirp + $reg[$i]->tambah_pres;
						$hsie = $awl1 - $reg[$i]->tambah_pres;
					} else {
						$awl1 = $reg[$i]->hasil_akhirp + $reg[$i]->tambah_pres + $batang;
						$hsie = $awl1 - $reg[$i]->tambah_pres;
					}
				}
				$update = Pressing::where('id',$reg[$i]->id)->update(['hasil_akhirp' => $hsie]);
			}
		}
	}

}
