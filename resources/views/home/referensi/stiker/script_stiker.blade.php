<script>
	let token = $("meta[name='csrf-token']").attr("content");
	const produk1 = {!! json_encode($produk) !!};
	produk1.unshift({id_produk:'',produk:'Pilih'});
	$('.spinner_nekr').hide();
	$('#hiddenval').hide();
	$('#loadingdelete').hide();

	var table = $('#table-stiker').DataTable({
		"lengthMenu": [
		[3,5,10, 25, 50, 100, 150, 200, -1],
		[3,5,10, 25, 50, 100, 150, 200, "All"]
		],
		"order": [
		[0, 'desc']
		],
		processing: true,
		serverSide: true,
		ajax: '/stiker/json',
		columns: [
		{ data: 'DT_RowIndex', name: 'DT_RowIndex' },
		{ data: 'produk', name: 'produk' },
		{ data: 'kodeproduk', name: 'kodeproduk' },
		{ data: 'stock_luar', name: 'stock_luar' },
		{ data: 'stock_dalam', name: 'stock_dalam' },
		{	
			data: null,
			"bSortable": false,
			mRender: function(o) {
				return '<button type="button" class="btn btn-xs btn-rounded btn-warning update-stiker" data-title="Update Stiker | ID Produk: '+ o.produk +'" data-link="/stiker/upp/'+ o.id_stock_stiker +'" data-produk="'+ o.id_produk +'" data-subproduk="'+ o.id_sub_produk +'" data-stockluar="'+ o.stock_luar +'" data-stockdalam="'+ o.stock_dalam +'"><i class="mdi mdi-dots-horizontal"></i></button><button type="button" id="delete-stiker" data-id="'+ o.id_stock_stiker +'" class="btn btn-danger btn-rounded btn-xs"><i class="fa fa-trash"></i></button>'; 
			}
		},
		],
		"displayLength": 10,
	});
	$('#table-stiker tbody').on('click', 'tr.group', function() {
		var currentOrder = table.order()[0];
		if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
			table.order([2, 'desc']).draw();
		} else {
			table.order([2, 'asc']).draw();
		}
	});
	$('#table-stiker tbody').on('click', 'tr.group', function() {
		var currentOrder = table.order()[0];
		if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
			table.order([2, 'desc']).draw();
		} else {
			table.order([2, 'asc']).draw();
		}
	});

	function Refresh() {
		table.search('').draw();
	}

	function lproduk(position,like = '') {
		let opt = '';
		for (var i = 0; i < produk1.length; i++) {
			opt += '<option value="'+ produk1[i].id_produk +'"';
			if (produk1[i].id_produk == like) { opt += 'selected'; }
			opt += '>'+ produk1[i].produk +'</option>';
		}
		$('#' + position).html(opt);
	}

	function lsubpro(position,base,like = '') {
		$.ajax({
			url: '/cukai/subproduk/json',
			type: 'POST',
			dataType: 'json',
			data: {
				"_token": token,
				produk: base
			},
		})
		.done(function(data) {
			let opt = '<option value="">Pilih Sub</option>';
			for (var i = 0; i < data.length; i++) {
				opt += '<option value="'+ data[i].id_sub_produk +'"';
				if (data[i].id_sub_produk == like) { opt += 'selected'; }
				opt += '>'+ data[i].sub_kode +' - '+ data[i].sub_produk +'</option>';
			}
			$('#' + position).html(opt);		
		});
	}

	function save_method() {
		let link = $('#form-stiker').attr('action');
		let produk_in = $('#produk_in').val();
		let subproduk = $('#sub_produk_in').val();
		let stock_luar = $('#stock_luar').val();
		let stock_dalam = $('#stock_dalam').val();

		$.ajax({
			url: link,
			type: 'POST',
			dataType: 'json',
			data: {
				"_token": token,
				produk: produk_in,
				subproduk: subproduk,
				stock_luar: stock_luar,
				stock_dalam: stock_dalam,
			},
			beforeSend: function() {
				isProcessing = true;
				$('.spinner_nekr').fadeIn();
			},
			success: function(data) {
				$('.spinner_nekr').hide();
				isProcessing = false;
				alert(data);
				$('#table-stiker').DataTable().ajax.reload();
				$('#modal-stiker').modal('toggle');
			}
		});
		
	}

	$(document).on('click', '#tambah-stiker', function(event) {
		let clik = $(this);
		$('.modal-header #title').text('Tambah Stiker');
		$('#form-stiker').find('input').val('');
		$('#form-stiker').find('select').val('');
		$('#produk').removeAttr('disabled');
		$('#sub_produk').removeAttr('disabled');
		$('#form-stiker').attr('action', clik.data('link'));
		lproduk('produk');
		$(document).on('change', '#produk', function(event) {
			let val = $(this).val();
			$('#produk_in').val(val);
			lsubpro('sub_produk',val);
			$(document).on('change', '#sub_produk', function(event) {
				let cal = $(this).val();
			});
		});
		$('#modal-stiker').modal('show');
	});

	$(document).on('click', '.update-stiker', function(event) {
		let clik = $(this);
		$('.modal-header #title').text(clik.data('title'));
		$('#form-stiker').find('input').val('');
		$('#form-stiker').find('select').val('');
		lproduk('produk',clik.data('produk'));
		lsubpro('sub_produk',clik.data('produk'),clik.data('subproduk'));
		$('#produk').attr('disabled','disabled');
		$('#sub_produk').attr('disabled','disabled');
		$('#form-stiker').attr('action', clik.data('link'));
		$('#produk_in').val(clik.data('produk'));
		$('#sub_produk_in').val(clik.data('subproduk'));
		$('#stock_luar').val(clik.data('stockluar'));
		$('#stock_dalam').val(clik.data('stockdalam'));
		$('#modal-stiker').modal('show');
	});

	$(document).on('click', '#delete-stiker', function(event) {
		let hebh = confirm('Apakah anda yakin ingin menghapus data ini ?');
		if (hebh) {
			let id = $(this).data("id");
			let token = $("meta[name='csrf-token']").attr("content");
			$.ajax(
			{
				url: '/stiker/delete/' + id,
				type: 'DELETE',
				data: {
					"_token": token,
				},
				beforeSend: function() { 
					isProcessing = true;
					$('#loadingdelete').fadeIn();
				},
				success: function (data){
					$('#loadingdelete').hide();
					isProcessing = false;
					alert(data);
					$('#table-stiker').DataTable().ajax.reload();
				}
			});
		}
	});
</script>