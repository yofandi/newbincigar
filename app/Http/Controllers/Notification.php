<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use Auth;
use Cart;
use DataTables;
use App\Stock_qc;
use App\Noc_inrfs;
use App\Noc_items;
use App\Order;
use App\Items_object;
use App\Stock_packing;
use App\Stock_rfs;
use App\Stock_bagan;
use App\Value_reject;

class Notification extends Controller
{
	public function __construct()
	{
		$this->middleware('auth');
	}

	public function getlimitbahanbaku()
	{
		$query = DB::table(DB::raw('stock_probaku AS a'))
		->select(DB::raw('
			(CASE
			WHEN a.jml_bahanbaku <= a.minlimit_stock OR a.jml_bahanbaku >= a.limit_stock
			THEN 1
			ELSE 0
			END) AS namestatus,
			(CASE
			WHEN a.jml_bahanbaku <= a.minlimit_stock THEN "batasminim"
			WHEN a.jml_bahanbaku >= a.limit_stock THEN "batasmaxim"
			ELSE "amam_minim"
			END) AS name
			'))
		->get();
		return $query->toJson();
	}

	public function ceknyobak()
	{
		$query = Cart::content();
		return $query->toJson();
	}

	public function getsessioncart()
	{
		$no = 1;
		$html = '';
		$query = Cart::content();
		if ($query->count() > 0) {
			foreach ($query as $key) {
				$html .= '<tr>';
				$html .= '<td>'.$no.'</td>';
				$html .= '<td>'.$key->name.'</td>';
				$html .= '<td>'.$key->qty.'</td>';
				$html .= '<td>';
				$html .= '<button type="button" class="btn btn-warning btn-sm" id="updatecart" data-link="/datapacking/uppinrfs/'.$key->rowId.'/'.$key->name.'" data-rowId="'.$key->rowId.'" data-id="'.$key->id.'" data-name="'.$key->name.'" data-qty="'.$key->qty.'"><i class="mdi mdi-minus"></i></button>';
				$html .= '<button type="button" class="btn btn-danger btn-sm" id="deletecart" data-link="/datapacking/delinrfs/'.$key->rowId.'/'.$key->name.'"><i class="mdi mdi-delete-empty"></i></button>';
				$html .= '</td>';
				$html .= '</tr>';
				$no++;
			}
		} else {
			$html .= '<tr>';
			$html .= '<td colspan="4">No data in table</td>';
			$html .= '</tr>';
		}
		echo json_encode($html);
	}

	public function getnocin()
	{
		$level = Auth::user()->level_user_id_level;

		$query = DB::table('noc_inrfs')
		->select('noc_inrfs.*','users.name')
		->join('users','noc_inrfs.author_session','=','users.id');

		if (in_array(Auth::user()->level_user_id_level, [1,2])) {
		} else {
			if (Auth::user()->level_user_id_level == 6) {
				$query->where([['arah_dari','=',0],['status_inrfs','=',3]]);
				$query->orwhere(DB::raw('arah_dari = 1 AND status_inrfs IN (0,1,2,3,4)'));
			} elseif (Auth::user()->level_user_id_level == 5) {
				$query->where(DB::raw('arah_dari = 0 AND status_inrfs IN (0,1)'));
				$query->orwhere(DB::raw('arah_dari = 1 AND status_inrfs IN (0,1)'));
			} elseif (in_array(Auth::user()->level_user_id_level, [3,4])) {
				$query->where(DB::raw('arah_dari = 0 AND status_inrfs IN (3,4)'));
				$query->orwhere(DB::raw('arah_dari = 0 AND status_inrfs IN (0,1,2,3)'));
			}
		}
		$query->where([['noc_inrfs.status_lihat','=',0]]);
		$hey = $query->orderBy('noc_inrfs.tanggal_inrfs','desc')
		->get();

		$fgg = [];
		$row1 = [];
		foreach ($hey as $key) {
			$row1['link'] = '/noc_isintd/'.$key->id;
			$row1['status'] = $key->status_inrfs;
			switch ($key->arah_dari) {
				case 0:
				$row1['namestatus'] = "<h5>Ada barang dari </h5> packing";
				break;
				
				case 1:
				$row1['namestatus'] = "<h5>Ada barang dari </h5> RFR";
				break;
			}
			$row1['tanggal'] = $key->tanggal_inrfs;
			$fgg[] = $row1;
		}

		$martau = DB::table(DB::raw('stock_probaku AS a'))
		->select(DB::raw('
			(CASE
			WHEN a.jml_bahanbaku <= a.minlimit_stock OR a.jml_bahanbaku >= a.limit_stock
			THEN 1
			ELSE 0
			END) AS namestatus,
			(CASE
			WHEN a.jml_bahanbaku <= a.minlimit_stock THEN "batasminim"
			WHEN a.jml_bahanbaku >= a.limit_stock THEN "batasmaxim"
			ELSE "amam_minim"
			END) AS name,
			jenis.jenis,
			kategori.kategori
			'))
		->leftJoin('jenis','a.jenis_id_jenis','=','jenis.id_jenis')
		->leftJoin('kategori','a.kategori_id_kategori','=','kategori.id_kategori')
		->get();

		$row2 = [];
		foreach ($martau as $lue) {
			if ($lue->namestatus != 0) {
				$row2['link'] = 'javascript:void(0)';
				$row2['status'] = null;
				switch ($lue->name) {
					case "batasminim":
					$row2['namestatus'] = "<h5>MINIMUM</h5>".$lue->jenis.' - '.$lue->kategori;
					break;

					case "batasmaxim":
					$row2['namestatus'] = "<h5>MAXIMUM</h5>".$lue->jenis.' - '.$lue->kategori;
					break;
				}
				$row2['tanggal'] = '';
				$fgg[] = $row2;
			}
		}
		echo json_encode($fgg);
	}

	public function getnotifrfsbagan()
	{
		$in = [1,2,6,7,8,9,10,11];
		$query = DB::table('order')
		->select('order.*','level_user.level','users.username')
		->join('users','order.users_id','=','users.id')
		->join('level_user','users.level_user_id_level','=','level_user.id')
		->whereIn('level_user.id',$in);
		if (in_array(Auth::user()->level_user_id_level, [7,8,9,10,11])) {
			$query->where('order.users_id',Auth::user()->id);
		}
		if (Auth::user()->level_user_id_level == 6) {
			$query->where('order.status_lihatrfs',0);
		} elseif(in_array(Auth::user()->level_user_id_level, [7,8,9,10,11])) {
			$query->where('order.status_lihatbagan',0);
		} elseif (in_array(Auth::user()->level_user_id_level, [1,2])) {
			$query->where([['order.status_lihatrfs','=',0],['order.status_lihatbagan','=',0]]);
		}
		$gt = $query->get();
		return json_encode($gt);
	}

	public function getviewnoc($id)
	{
		$data['produk'] = DB::table('produk')->get();
		$data['option'] = [0 => 'Menunggu',1 => 'Verifikasi',2 => 'Tolak',3 => 'Terima',4 => 'Reproduksi'];
		$data['isi'] = DB::table('noc_inrfs')
		->select(
			DB::raw('noc_inrfs.*,users.name,(
				CASE
				WHEN noc_inrfs.arah_dari = 0
				THEN "Packing"
				ELSE "RFS"
				END 
			) AS asal')
		)
		->join('users','noc_inrfs.author_session','=','users.id')
		->where('noc_inrfs.id',$id)
		->first();
		return $data;
	}

	public function show_noc($id)
	{
		$yyie = [0,1,2,3];
		$get = Noc_inrfs::where('id',$id)->first();
		if (in_array($get->status_inrfs,$yyie)) {
			$rty = Noc_inrfs::find($id);
			$rty->status_lihat = 1;
			$rty->save();
			echo true;
			$data = $this->getviewnoc($id);
		} else {
			echo false;
			$data = $this->getviewnoc($id);
		}

		return view('home.notification.notif_nocrfs',$data);
	}

	public function getnoc_items($idnoc_inrfs)
	{
		$query = DB::table('noc_items')
		->select('noc_items.*',
			'stock_packing.id AS stock_packing_id',
			'sub_produk.isi',
			'sub_produk.sub_kode',
			'sub_produk.sub_produk',
			'value_reject.id AS idval',
			'value_reject.jml_packing',
			'value_reject.jml_uraian',
			'value_reject.convert_batang',
			'value_reject.jml_batangqc',
			'value_reject.jml_cincin',
			'value_reject.jml_stikerluar',
			'value_reject.jml_stikerdalam',
			'value_reject.jml_cukai',
			'value_reject.jml_kemasan',
			'value_reject.jml_fill1',
			'value_reject.jml_bind',
			'value_reject.jml_wrap',
			'value_reject.jml_fill2')
		->join('sub_produk','noc_items.sub_produk_id','=','sub_produk.id_sub_produk')
		->Leftjoin('value_reject','noc_items.id','=','value_reject.noc_items_id')
		->join('stock_packing','noc_items.sub_produk_id','=','stock_packing.sub_produk_id_sub_produk')
		->where([['noc_items.noc_inrfs_id',$idnoc_inrfs],['deleted_at',null]])
		->get();

		$data = [];
		$row = [];
		foreach ($query as $key) {
			$yje = unserialize($key->uraian);
			$row['id'] = $key->id;
			$row['stock_packing_id'] = $key->stock_packing_id;
			$row['sub_produk_id'] = $key->sub_produk_id;
			$row['noc_inrfs_id'] = $key->noc_inrfs_id;
			$row['jml_nocitm'] = $key->jml_nocitm;
			if ($key->idval != NULL) {
				$row['uraian'] = json_encode([
					$key->jml_packing,
					$key->jml_uraian,
					$key->convert_batang,
					$key->jml_batangqc,
					$key->jml_cincin,
					$key->jml_stikerluar,
					$key->jml_stikerdalam,
					$key->jml_cukai,
					$key->jml_kemasan,
					$key->jml_fill1,
					$key->jml_bind,
					$key->jml_wrap,
					$key->jml_fill2
				]);
			} else {
				$row['uraian'] = false;
			}

			$row['counturaian'] = strlen($key->uraian);
			$row['created_at'] = $key->created_at;
			$row['updated_at'] = $key->updated_at;
			$row['deleted_at'] = $key->deleted_at;

			$row['isi'] = $key->isi;
			$row['sub_kode'] = $key->sub_kode;
			$row['sub_produk'] = $key->sub_produk;
			$data[] = $row;
		}
		return json_encode($data);
	}

	public function getcartreject()
	{
		$no = 1;
		$html = '';
		$query = Cart::content();
		if ($query->count() > 0) {
			foreach ($query as $key) {
				$html .= '<tr>';
				$html .= '<td>'.$no.'</td>';
				$html .= '<td>'.$key->name.'</td>';
				$html .= '<td contenteditable class="tdatareject" data-rowid="'.$key->rowId.'" data-name="'.$key->name.'" data-column_name="qty">'.$key->qty.'</td>';
				$html .= '<td>';
				$html .= '<button type="button" class="btn btn-outline btn-danger btn-rounded btn-sm btn-icons" id="buttonreject" data-link="/api/noc_items/reject/'.$key->rowId.'/'.$key->name.'/del" data-rowId="'.$key->rowId.'"><i class="mdi mdi-delete-variant"></i></button>';
				$html .= '</td>';
				$html .= '</tr>';
				$no++;
			}
		} else {
			$html .= '<tr>';
			$html .= '<td colspan="4">No data in this table</td>';
			$html .= '</tr>';
		}
		echo json_encode($html);
	}

	public function deleteitemcart($id,$name)
	{
		$query = Noc_items::find($id)->delete();
		if ($query) {
			$alert = 'data sub produk '.$name.' berhasil dihapus';
		} else {
			$alert = 'data sub produk '.$name.' gagal dihapus';
		} 
		echo json_encode($alert);
	}

	public function restoreitemcart()
	{
		Noc_items::withTrashed()->restore();
		Cart::destroy();
	}

	public function addcartreject(Request $request)
	{
		$data = [
			'id' => $request->subproduk,
			'name' => $request->name,
			'qty' => $request->jml,
			'price' => 0,
			'options' => ['packing' => $request->idpacking]
		];
		$query = Cart::add($data);
		$if = $query ? 'data berhasil ditambahkan' : 'data gagal ditambahkan';
		return json_encode($if);
	}

	public function updatecartreject(Request $request)
	{
		$data = [
			$request->column_name => $request->jml
		];
		$query = Cart::update($request->rowId,$data);
		$if = $query ? 'data '.$request->name.' berhasil diupdate' : 'data '.$request->name.' gagal diupdate';
		return json_encode($if);
	}

	public function deletecartreject($rowId,$name)
	{
		$query = Cart::remove($rowId);
		$if = $query ? 'data '.$name.' gagal dihapus' : 'data '.$name.' berhasil dihapus';
		return json_encode($if);   
	}

	public function ifoptionnoc($id)
	{
		$hdne = Noc_items::where('noc_inrfs_id',$id)->get();
		foreach ($hdne as $key) {
			$jar = Stock_packing::where('sub_produk_id_sub_produk',$key->sub_produk_id)->first();	

			$jml = $jar->jml_packing - $key->jml_nocitm;

			$werto = Stock_rfs::where('sub_produk_id_sub_produk',$key->sub_produk_id);
			if ($werto->count() > 0) {
				$fgr = $werto->first();
				$jmeg = $fgr->jml_rfs + $key->jml_nocitm;
				$tery = Stock_rfs::find($fgr->id);
				$tery->jml_rfs = $jmeg;
				$tery->save();
			} else {
				$tery = new Stock_rfs;
				$tery->sub_produk_id_sub_produk = $key->sub_produk_id;
				$tery->jml_rfs = $key->jml_nocitm;
				$tery->save();
			}

			$qwrt = Stock_packing::find($jar->id);
			$qwrt->jml_packing = $jml;
			$qwrt->save();
		}
	}

	public function loop_insertrejectcart($swert,$id)
	{
		$ane = Cart::content();
		foreach ($ane as $e) {
			$mbd = DB::table('noc_items')
			->where([['sub_produk_id','=',$e->id],['noc_inrfs_id','=',$id]]);
			$ndmne = $mbd->count();
			if ($ndmne > 0) {
				$necr = $mbd->first();
				$lensea = $necr->jml_nocitm - $e->qty;
				Noc_items::where('id',$necr->id)->update(['jml_nocitm' => $lensea]);
			}
			DB::table('noc_items')->insert([
				'sub_produk_id' => $e->id,
				'noc_inrfs_id' => $swert,
				'jml_nocitm' => $e->qty
			]);
		}
	}

	public function addrejectinrfs($request,$id)
	{
		$wert = new Noc_inrfs;
		$wert->tanggal_inrfs = $request->tanggal;
		$wert->status_lihat = 0;
		$wert->status_inrfs = 2;
		$wert->keterangan = $request->ket_rej;
		$wert->author_session = Auth::user()->id;
		$wert->created_at = date('Y-m-d');
		$wert->log = 'INSERT';
		$wert->save();

		$this->loop_insertrejectcart($wert->id,$id);
	}

	public function deletetrash()
	{
		$rty = Noc_items::onlyTrashed()->get();
		$ghe = [];
		foreach ($rty as $delk) {
			array_push($ghe, $delk->id);
		}
		Noc_items::whereIn('id',$ghe)->forceDelete();
	}

	public function add_item_nocinrfs(Request $request,$id)
	{
		$cek = Noc_items::where([['sub_produk_id','=',$request->subproduk],['noc_inrfs_id','=',$id]]);
		if ($cek->count() > 0) {
			$ghet = $cek->first();
			$jml = $ghet->jml_nocitm + $request->jml_keluar;

			$query = Noc_items::find($ghet->id);
			$query->jml_nocitm = $jml;
			$query->save();
		} else {	
			$query = DB::table('noc_items')->insert([
				'sub_produk_id' => $request->subproduk,
				'noc_inrfs_id' => $id,
				'jml_nocitm' => $request->jml_keluar
			]);
		}
		$var = ($query? 'data berhasil ditambahkan' : 'data gagal ditambahkan');
		return json_encode($var);
	}

	public function query_sendreproduksi($id)
	{
		$table = DB::table('noc_items')
		->select(
			'noc_items.sub_produk_id',
			'noc_items.noc_inrfs_id',
			'noc_items.jml_nocitm',
			'value_reject.jml_packing',
			'value_reject.jml_uraian',
			'value_reject.convert_batang',
			'value_reject.jml_batangqc',
			'value_reject.jml_cincin',
			'value_reject.jml_stikerluar',
			'value_reject.jml_stikerdalam',
			'value_reject.jml_cukai',
			'value_reject.jml_kemasan',
			'value_reject.jml_fill1',
			'value_reject.jml_bind',
			'value_reject.jml_wrap',
			'value_reject.jml_fill2',
			'jenis_has_produk.jenis_id_jenis',
			'stock_packing.id AS idpacking',
			'stock_packing.jml_packing',
			'stock_qc.id AS idqc',
			'stock_qc.jml_qc',
			'stock_cincin.id_stock_cincin',
			'stock_cincin.stock_qty_cincin',
			'stock_cukai.id_stock_cukai',
			'stock_cukai.stock_qty_cukai',
			'stock_stiker.id_stock_stiker',
			'stock_stiker.stock_luar',
			'stock_stiker.stock_dalam',
			'stock_kemasan.id_stock_kemasan',
			'stock_kemasan.stock_kemasan'
		)
		->Leftjoin('value_reject','noc_items.id','=','value_reject.noc_items_id')
		->join('sub_produk','noc_items.sub_produk_id','=','sub_produk.id_sub_produk')
		->join('jenis_has_produk','sub_produk.produk_id_produk','=','jenis_has_produk.produk_id_produk')
		->join('stock_packing','sub_produk.id_sub_produk','=','stock_packing.sub_produk_id_sub_produk')
		->join('stock_qc','jenis_has_produk.id_jnpro','=','stock_qc.jenis_has_produk_id_jnpro')
		->Leftjoin('stock_cincin','sub_produk.produk_id_produk','=','stock_cincin.produk_id_produk')
		->Leftjoin('stock_cukai','sub_produk.id_sub_produk','=','stock_cukai.sub_produk_id_sub_produk')
		->Leftjoin('stock_stiker','sub_produk.id_sub_produk','=','stock_stiker.sub_produk_id')
		->Leftjoin('stock_kemasan','sub_produk.id_sub_produk','=','stock_kemasan.sub_produk_id')
		->where('noc_items.noc_inrfs_id',$id)
		->get();
		return $table;
	}

	public function update_stockrfs($akew,$anek)
	{
		$aknexm = Stock_rfs::where('sub_produk_id_sub_produk',$akew);
		if ($aknexm->count() > 0) {
			$lenae = $aknexm->first();
			$jmebw = $lenae->jml_rfs - $anek;

			$enke = Stock_rfs::find($lenae->id);
			$enke->jml_rfs = $jmebw;
			$enke->save();
		}
	}

	public function update_stockpack($skje,$asmek)
	{
		$ywer = Stock_packing::where('id',$skje);
		if ($ywer->count() > 0) {
			$tvb = $ywer->first();
			$kwb = $tvb->jml_packing + $asmek;

			$mnak = Stock_packing::find($skje);
			$mnak->jml_packing = $kwb;
			$mnak->save();
		}
	}

	public function update_stockqc($ndske,$jml)
	{
		$hjel = Stock_qc::where('id',$ndske);
		if ($hjel->count() > 0) {
			$and = $hjel->first();
			$men = $and->jml_qc + $jml;

			$tyev = Stock_qc::find($ndske);
			$tyev->jml_qc = $men;
			$tyev->save();
		}
	}

	public function update_stockcincin($skne,$dsmlme)
	{
		$mdsk = DB::table('stock_cincin')->where('id_stock_cincin',$skne);
		if ($mdsk->count() > 0) {
			$name = $mdsk->first();
			$jkan = $name->stock_qty_cincin + $dsmlme;

			DB::table('stock_cincin')->where('id_stock_cincin',$skne)->update(['stock_qty_cincin' => $jkan]);
		}
	}

	public function update_stockcukai($kem,$jmske)
	{
		$nbann = DB::table('stock_cukai')->where('id_stock_cukai',$kem);
		if ($nbann->count() > 0) {
			$nai = $nbann->first();
			$amnkre = $nai->stock_qty_cukai + $jmske;

			DB::table('stock_cukai')->where('id_stock_cukai',$kem)->update(['stock_qty_cukai' => $amnkre]);
		}
	}

	public function update_stockstiker($manj,$kakne,$nkake)
	{
		$hnae = DB::table('stock_stiker')->where('id_stock_stiker',$manj);
		if ($hnae->count() > 0) {
			$hane = $hnae->first();
			$amelw = $hane->stock_luar + $kakne;
			$mnake = $hane->stock_dalam + $nkake;

			DB::table('stock_stiker')->where('id_stock_stiker',$manj)->update(['stock_luar' => $amelw,'stock_dalam' => $mnake]);
		}
	}

	public function update_stockkemasan($makes,$nane)
	{
		$kanke = DB::table('stock_kemasan')->where('id_stock_kemasan',$makes);
		if ($kanke->count() > 0) {
			$anke = $kanke->first();
			$aese = $anke->stock_kemasan + $nane;

			DB::table('stock_kemasan')->where('id_stock_kemasan',$makes)->update(['stock_kemasan' => $aese]);
		}
	}

	public function update_stocktembakau($nakes,$val1,$val2,$val3,$val4)
	{
		$kakne1 = DB::table('stock_probaku')->where([['jenis_id_jenis','=',$nakes],['kategori_id_kategori','=',1]]);
		if ($kakne1->count() > 0) {
			$akewu1 = $kakne1->first();
			$anken1 = $akewu1->jml_bahanbaku + $val1;

			DB::table('stock_probaku')->where('id',$akewu1->id)->update(['jml_bahanbaku' => $anken1]);
		}
		$kakne2 = DB::table('stock_probaku')->where([['jenis_id_jenis','=',$nakes],['kategori_id_kategori','=',2]]);
		if ($kakne2->count() > 0) {
			$akewu2 = $kakne2->first();
			$anken2 = $akewu2->jml_bahanbaku + $val2;

			DB::table('stock_probaku')->where('id',$akewu2->id)->update(['jml_bahanbaku' => $anken2]);
		} else {
			DB::table('stock_probaku')->insert(['jml_produksi' => 0,'jml_bahanbaku' => $val2,'jenis_id_jenis' => $nakes,'kategori_id_kategori' => 2,'created_at' => date('Y-m-d H:i:s')]);
		}
		$kakne3 = DB::table('stock_probaku')->where([['jenis_id_jenis','=',$nakes],['kategori_id_kategori','=',3]]);
		if ($kakne3->count() > 0) {
			$akewu3 = $kakne3->first();
			$anken3 = $akewu3->jml_bahanbaku + $val3;

			DB::table('stock_probaku')->where('id',$akewu3->id)->update(['jml_bahanbaku' => $anken3]);
		}
		$kakne4 = DB::table('stock_probaku')->where([['jenis_id_jenis','=',$nakes],['kategori_id_kategori','=',4]]);
		if ($kakne4->count() > 0) {
			$akewu4 = $kakne4->first();
			$anken4 = $akewu4->jml_bahanbaku + $val4;

			DB::table('stock_probaku')->where('id',$akewu4->id)->update(['jml_bahanbaku' => $anken4]);
		}
	}

	public function send_reproduksipro($request,$id)
	{
		$query = $this->query_sendreproduksi($id);
		foreach ($query as $key) {
			$this->update_stockrfs($key->sub_produk_id,$key->jml_nocitm);
			$this->update_stockpack($key->idpacking,$key->jml_packing);
			$this->update_stockqc($key->idqc,$key->jml_batangqc);
			$this->update_stockcincin($key->id_stock_cincin,$key->jml_cincin);
			$this->update_stockcukai($key->id_stock_cukai,$key->jml_cukai);
			$this->update_stockstiker($key->id_stock_stiker,$key->jml_stikerluar,$key->jml_stikerdalam);
			$this->update_stockkemasan($key->id_stock_kemasan,$key->jml_kemasan);
			$this->update_stocktembakau($key->jenis_id_jenis,$key->jml_fill1,$key->jml_fill2,$key->jml_bind,$key->jml_wrap);
		}
	}

	public function change_noc(Request $request,$id)
	{
		DB::beginTransaction();
		if ($request->arah_dari != 1) {	
			$cek = Cart::content();
			if ($request->status_inrfs_in == 3) {
				if ($cek->count() > 0) {
					$this->addrejectinrfs($request,$id);
					Cart::destroy();
					$this->deletetrash();
				}
				$this->ifoptionnoc($id);
			}
		} else {
			if ($request->status_inrfs_in == 3) {
				$this->send_reproduksipro($request,$id);
			}
		}
		$data = ['tanggal_inrfs' => $request->tanggal_noc,
		'status_lihat' => 0,
		'status_inrfs' => $request->status_inrfs_in,
		'pesan_packing' =>  $request->pesan_packing,
		'pesan_qc' =>  $request->pesan_qc,
		'pesan_rfs' =>  $request->pesan_rfs];
		$query = Noc_inrfs::where('id',$id)->update($data);
		if ($query) {
			echo "<script>alert('Notifikasi ID: ".$id." berhasil diupdate'); window.location='".route('listtable')."'; </script>";
		} else {
			echo "<script>alert('Notifikasi ID: ".$id." gagal diupdate'); window.location='".route('listtable')."';</script>";
		}
		DB::commit();
	}

	public function resendreject(Request $request,$id)
	{
		$query = Noc_inrfs::where('id',$id)->update([
			'tanggal_inrfs' => $request->tanggal_noc,
			'status_lihat' => 0,
			'status_inrfs' => $request->status_inrfs,
			'keterangan' => $request->ket,
			'pesan_packing' =>  $request->pesan_packing,
			'pesan_qc' =>  $request->pesan_qc,
			'pesan_rfs' =>  $request->pesan_rfs]);
		if ($query) {
			echo '<script> alert("Data Notifikasi ID: '.$id.' berhasil diperbaharui"); window.location="'.URL::to('/listtable').'"</script>';
		} else {
			echo '<script> alert("Data Notifikasi ID: '.$id.' gagal diupdate"); window.location="'.URL::to('/listtable').'"</script>';
		}
	}

	public function save_uraianiteminrfs(Request $request,$id,$idinrfs)
	{
		$cek = Value_reject::where('noc_items_id',$id);
		if ($cek->count() > 0) {
			$hjeu = $cek->first();
			$query = Value_reject::find($hjeu->id);
		} else {
			$query = new Value_reject;
			$query->created_at = date('Y-m-d H:i:s');
			$query->noc_items_id = $id;
		}
		$query->jml_packing = $request->topacking;
		$query->jml_uraian = $request->touraian;
		$query->convert_batang = $request->conjml;
		$query->jml_batangqc = $request->toqc;
		$query->jml_cincin = $request->jmlcincin;
		$query->jml_stikerluar = $request->stiker_luar;
		$query->jml_stikerdalam = $request->stiker_dalam;
		$query->jml_cukai = $request->jmlcukai;
		$query->jml_kemasan = $request->jmlkemasan;
		$query->jml_fill1 = $request->fill1;
		$query->jml_bind = $request->bind;
		$query->jml_wrap = $request->wrap;
		$query->jml_fill2 = $request->fill2;
		if ($query->save()) {
			echo '<script> alert("Data Notifikasi berhasil diupdate"); window.location="'.URL::to('/noc_isintd/'.$idinrfs).'"</script>';
		} else {
			echo '<script> alert("Data Notifikasi gagal diupdate"); window.location="'.URL::to('/noc_isintd/'.$idinrfs).'"</script>';
		}
	}

	public function listtablenoc()
	{
		return view('home.notification.list_nocrfs');
	}

	public function listtablerfsbagan()
	{
		return view('home.notification.list_notif_rfsbagan');
	}

	public function getlistnoc()
	{
		$query = Noc_inrfs::orderBy('id','asc');
		if (in_array(Auth::user()->level_user_id_level, [1,2])) {
		} else {
			if (Auth::user()->level_user_id_level == 6) {
				$query->where([['arah_dari','=',0],['status_inrfs','=',3]]);
				$query->orwhere(DB::raw('arah_dari = 1 AND status_inrfs IN (0,1,2,3,4)'));
			} elseif (Auth::user()->level_user_id_level == 5) {
				$query->where(DB::raw('arah_dari = 0 AND status_inrfs IN (0,1)'));
				$query->orwhere(DB::raw('arah_dari = 1 AND status_inrfs IN (0,1)'));
			} elseif (in_array(Auth::user()->level_user_id_level, [3,4])) {
				$query->where(DB::raw('arah_dari = 0 AND status_inrfs IN (3,4)'));
				$query->orwhere(DB::raw('arah_dari = 0 AND status_inrfs IN (0,1,2,3)'));
			}
		}
		$an = $query->get();

		return Datatables::of($an)
		->addIndexColumn()
		->addColumn('asal', function ($query) {
			switch ($query->arah_dari) {
				case 0:
				$awl = 'Packing';
				break;

				case 1:
				$awl = 'RFS';
				break;
			}
			return $awl;
		})
		->addColumn('stlihat', function ($query) {
			switch ($query->status_inrfs) {
				case 0:
				$awl = 'Menunggu';
				break;

				case 1:
				$awl = 'Verifikasi';
				break;

				case 2:
				$awl = 'Ditolak';
				break;

				case 3:
				$awl = 'Diterima';
				break;

				case 4:
				$awl = 'Reproduksi';
				break;
			}
			return $awl;
		})
		->make(true);
	}

	public function getlistrfsbagan()
	{
		$query = DB::table('order')
		->select('order.*','users.username','level_user.level')
		->join('users','order.users_id','=','users.id')
		->join('level_user','users.level_user_id_level','=','level_user.id')
		->get();

		return Datatables::of($query)
		->addIndexColumn()
		->addColumn('status_row', function ($query) {
			switch ($query->verifikasi_rfs) {
				case 0:
				$awl = 'Menunggu';
				break;

				case 1:
				$awl = 'Acc';
				break;

				case 2:
				$awl = 'Masuk';
				break;
			}
			return $awl;
		})
		->addColumn('stterima', function ($query) {
			switch ($query->status_terima) {
				case 0:
				$kelm = 'Menunggu';
				break;

				case 1:
				$kelm = 'Diterima';
				break;

				case 2:
				$kelm = 'Ditolak';
				break;
			}
			return $kelm;
		})
		->make(true);   
	}

	public function show_rfsbaganview($id)
	{
		if (in_array(Auth::User()->level_user_id_level, [6])) {
			Order::where('id',$id)->update(['status_lihatrfs' => 1]);
		} elseif (in_array(Auth::User()->level_user_id_level, [7,8,9,10,11])) {
			Order::where('id',$id)->update(['status_lihatbagan' => 1]);
		} elseif (in_array(Auth::User()->level_user_id_level, [1,2])) {
			Order::where('id',$id)->update(['status_lihatrfs' => 1,'status_lihatbagan' => 1]);
		}
		
		$data['isi'] = DB::table('order')
		->select('order.*','level_user.level','users.username','users.level_user_id_level')
		->join('users','order.users_id','=','users.id')
		->join('level_user','users.level_user_id_level','=','level_user.id')
		->where('order.id',$id)
		->first();
		$data['verifikasi'] = [0 => 'Menunggu',1 => 'ACC',2 => 'Masuk'];
		$data['terima'] = [0 => 'Belum Diterima',1 => 'Diterima',2 => 'Tolak'];
		$data['level'] = DB::table('level_user')->select('id','level')->whereIn('id',[7,8,9,10,11])->get();
		$data['klasifikasi'] = [1,2,7,8,9,10,11];
		$data['produk'] = DB::table('produk')->get();
		return view('home.notification.notif_rfsbagan',$data);
	}

	public function show_rfsbaganitem($id)
	{
		$query = DB::table('items_object')
		->select('items_object.*','sub_produk.sub_kode','sub_produk.sub_produk')
		->join('sub_produk','items_object.sub_produk_id','=','sub_produk.id_sub_produk')
		->join('order','items_object.order_id','=','order.id')
		->where('order.id',$id)
		->get();
		return Datatables::of($query)
		->addIndexColumn()
		->addColumn('kodeproduk', function ($query) {
			$awl = $query->sub_kode.' - '.$query->sub_produk;
			return $awl;
		})
		->make(true);   
	}

	public function savein_bagan($subproduk,$users,$quantity)
	{
		$chek = Stock_bagan::where('sub_produk_id',$subproduk);
		if ($chek->count() > 0) {
			$jket = $chek->first();
			$ghet = $jket->stock_bagan + $quantity;
			$tyq = Stock_bagan::find($jket->id);
			$tyq->stock_bagan = $ghet;
		} else {
			$ghet = $quantity;
			$tyq = new Stock_bagan;
			$tyq->stock_bagan = $ghet;
			$tyq->sub_produk_id = $subproduk;
			$tyq->users_id = $users;
			$tyq->created_at = date('Y-m-d H:i:s');
		}
		$tyq->save();
	}

	public function potongstockrfs($id,$user)
	{
		$get = Items_object::where('order_id',$id)->get();
		foreach ($get as $key) {
			$getrfs = Stock_rfs::where('sub_produk_id_sub_produk',$key->sub_produk_id)->select('id','jml_rfs')->first();
			$jml = $getrfs->jml_rfs - $key->jml_item;

			$this->savein_bagan($key->sub_produk_id,$user,$key->jml_item);

			$rty = Stock_rfs::find($getrfs->id);
			$rty->jml_rfs = $jml;
			$rty->save();
		}
	}

	public function update_rfsbagan(Request $request,$id)
	{
		DB::beginTransaction();
		$yuen = Order::find($id);
		$yuen->tanggal_order = $request->tanggal_order;
		$yuen->keterangan_order = $request->ket_order;
		$yuen->verifikasi_rfs = $request->verifikasi_rfs_in;
		$yuen->status_terima = $request->status_terima_in;
		$yuen->users_id = $request->userbagan_in;
		$yuen->author_session = Auth::User()->id;
		$ty = $yuen->save();
		if ($ty) {
			if ($request->status_terima_in == 1) {
				$this->potongstockrfs($id,$request->userbagan_in);
			}
			echo "<script>alert('Notifikasi Bagan ID: berhasil diperbaharui'); window.location='".route('noc_rfsbagan.list')."'; </script>";
		} else {
			echo "<script>alert('Notifikasi Bagan ID: gagal diperbaharui'); window.location='".route('noc_rfsbagan.list')."';</script>";
		}
		DB::commit();
	}
}
