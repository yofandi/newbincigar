@extends('../../layout')

@section('title')
<title>BIN - ERP : Data Kemasan</title>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css" />
@endsection

@section('content')
<div class="row page-titles">
	<div class="col-md-5 align-self-center">
		<h3 class="text-themecolor">Data Kemasan</h3>
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a href="javascript:void(0)">Bahan baku</a></li>
			<li class="breadcrumb-item active">Data Kemasan</li>
		</ol>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="row">
			<div class="col-md-6">
				<legend>Stock Bahan (Kemasan)</legend>
			</div>
			<div class="col-md-2 offset-md-4">
				<button type="button" data-link="{{ route('datakemasan.post') }}" class="btn btn-success btn-rounded btn-icons" id="tambah-kemasan"><i class="fa fa-plus"></i></button>
				<button type="button" class="btn btn-secondary btn-rounded btn-icons" onclick="refresh()"><i class="mdi mdi-refresh"></i></button>
			</div>
			<div class="col-md-12" align="center" id="loadingdelete">
				<h5>Data tengah diproses. Mohon tunggu sejenak!</h5>
			</div>
			<div class="table-responsive">
				<table id="table_kemasan" class="display nowrap table table-sm table-hover table-striped table-bordered">
					<thead>
						<tr>
							<th>No.</th>
							<th>Tanggal</th>
							<th>Produk</th>
							<th>Sub Kode | Produk</th>
							<th>Kemasan</th>
							<th>Masuk</th>
							<th>Terpakai</th>
							<th>Afkir</th>
							<th>Sisa</th>
							<th>Ket</th>
							<th>Aksi</th>
						</tr>
					</thead>
				</table>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="modal-kemasan" tabindex="-1" role="dialog" aria-labelledby="title" aria-hidden="true" style="display: none;">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="card">
				<div class="card-header">
					<div class="modal-header">
						<h4 class="modal-title" id="title"></h4>
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					</div>
				</div>
				<div class="card-body">
					<form id="form-kemasan" method="POST">
						<div class="modal-body row">
							<div class="form-group col-md-12 fa-3x spinner_nekr" align="center">
								<i class="fas fa-spinner fa-spin"></i>
							</div>
							<div class="form-group col-md-12">
								<label class="control-label">Tanggal</label>
								<input type="text" class="form-control date_sam" id="tanggal_hiskem">
							</div>
							<div class="form-group col-md-6">
								<label class="control-label">Produk</label>
								<select class="form-control" id="produk"></select>
								<input type="hidden" id="produk_in">
							</div>
							<div class="form-group col-md-6">
								<label class="control-label">Subproduk</label>
								<select class="form-control" id="subproduk"></select>
								<input type="hidden" id="subproduk_in">
							</div>
							<div class="form-group col-md-12" id="showit">
								<button type="button" class="btn btn-xs btn-block btn-info" onclick="search_datastock()">Search</button>
							</div>
							<div class="form-group col-md-12">
								<label class="control-label">Kemasan</label>
								<input type="text" class="form-control" id="kemasan" placeholder="Kemasan">
								<input type="hidden" id="kemasan_in">
							</div>
							<div class="form-group col-md-6">
								<label class="control-label">Stock (Awal / Saat ini)</label>
								<input type="number" class="form-control" id="stock_awl" readonly>
							</div>
							<div class="form-group col-md-6" id="hiddenval">
								<label class="control-label">Harga Satuan</label>
								<input type="text" class="form-control" id="harga_satuan" placeholder="Harga.." readonly>
							</div>
							<div class="form-group col-md-4">
								<label class="control-label">Masuk</label>
								<input type="number" class="form-control" id="masuk" placeholder="Stock Masuk">
							</div>
							<div class="form-group col-md-4">
								<label class="control-label">Terpakai</label>
								<input type="number" class="form-control" id="terpakaikemasan" placeholder="Stock Terpakai">
							</div>
							<div class="form-group col-md-4">
								<label class="control-label">Afkir</label>
								<input type="number" class="form-control" id="afkir" placeholder="Stock Afkir">
							</div>
							<div class="form-group col-md-12">
								<label class="control-label">Ket</label>
								<textarea class="form-control" id="ket_kem" placeholder="Keterangan"></textarea>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" onclick="save_method()" class="btn btn-primary waves-effect text-left">Save</button>
							<button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Close</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('javascript')
<script src="{{ asset('node_modules/moment/moment.js') }}">
</script><script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
<script src="{{ asset('node_modules/datatables/datatables.min.js') }}"></script>
@include('home.bahan_baku.data_kemasan.script_kemasan')
@endsection