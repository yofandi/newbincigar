<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;
use App\Binding;
use App\Stock_filling;
use App\Stock_binding;
use App\Stock_probaku;

class Back_binding extends Controller
{
	public function sprobind($request)
	{
		$query = DB::table('stock_probaku')
		->select('stock_probaku.id','stock_probaku.jml_produksi')
		->join('jenis','stock_probaku.jenis_id_jenis','=','jenis.id_jenis')
		->join('kategori','stock_probaku.kategori_id_kategori','=','kategori.id_kategori')
		->where([['stock_probaku.jenis_id_jenis','=',$request->jenis],['stock_probaku.kategori_id_kategori','=',3]]);

		$qusrt = DB::table('stock_binding')
		->select('stock_binding.id','stock_binding.stock_weisb')
		->join('jenis_has_produk','stock_binding.jenis_has_produk_id_jnpro','=','jenis_has_produk.id_jnpro')
		->where([['jenis_has_produk.jenis_id_jenis','=',$request->jenis],['jenis_has_produk.produk_id_produk','=',$request->produk]]);

		$qusrt1 = DB::table('stock_filling')
		->select('stock_filling.id','stock_filling.jml_fill')
		->join('jenis_has_produk','stock_filling.jenis_has_produk_id_jnpro','=','jenis_has_produk.id_jnpro')
		->where([['jenis_has_produk.jenis_id_jenis','=',$request->jenis],['jenis_has_produk.produk_id_produk','=',$request->produk]]);
		if ($query->count() > 0) {
			$wert = $query->first();
			$wer = $wert->jml_produksi;
			$wqd = $wert->id;
		} else { $wer = 0; $wqd = '';}
		if ($qusrt->count() > 0) {
			$aem = $qusrt->first();
			$qer = $aem->stock_weisb;
			$qwc = $aem->id;
		} else { $qer = 0; $qwc = '';}

		if ($qusrt1->count() > 0) {
			$aem1 = $qusrt1->first();
			$ber = $aem1->jml_fill;
			$jkd = $aem1->id;
		} else { $ber = 0; $jkd = ''; }
		$array = ['jml_produksi' => $wer,'stock_weisb' => $qer,'jml_fill' => $ber,'id' => $wqd,'idbinding' => $qwc,'idfilling' => $jkd];
		return $array;
	}

	public function stockbind($request)
	{
		$bang = $request->stockbatang - $request->terpakaifilling;
		$cekfill = Stock_filling::find($request->idfilling);
		$cekfill->jml_fill = $bang;
		$cekfill->save();
		$resultidfill = $cekfill->id;
		if ($request->idbinding != '') {
			$id = $request->idbinding; 
			$show = Stock_binding::where('id',$id)->first();
			$jml = $show->jml_bind + $request->batangjml;
			$weis = $show->stock_weisb + $request->masukweisb - $request->terpakaiweisb;

			$cek = Stock_binding::find($id);
			$cek->jml_bind = $jml;
			$cek->stock_weisb = $weis;
			$cek->save();

			$resultid = $cek->id;
		} else {
			$jml = $request->batangjml;
			$weis = $request->stockweisb + $request->masukweisb - $request->terpakaiweisb;
			$save = new Stock_binding;

			$save->jml_bind = $jml; 
			$save->stock_weisb = $weis;
			$save->jenis_has_produk_id_jnpro = $request->idjnpro;
			$save->jenis_has_produk_id_jenis = $request->jenis;
			$save->jenis_has_produk_id_produk = $request->produk;
			$save->save();
			$resultid = $save->id;
		}
		return ['idfilling' => $resultidfill,'idbinding' => $resultid,'jmlbat' => $bang,'jmlbind' => $jml,'stockwies' => $weis];
	}

	public function ifuppstockbind($request,$masukweis,$terpoakaipro,$terpakaiweis,$terpakaifill,$batang)
	{
		if ($masukweis != $request->masukweisb || $terpoakaipro != $request->terpakaipro || $terpakaiweis != $request->terpakaiweisb || $terpakaifill != $request->terpakaifilling || $batang != $request->batangjml) {
			$shbaku = Stock_probaku::where('id',$request->idprobaku)->first();
			$shfill = Stock_filling::where('id',$request->idfilling)->first();
			$shbind = Stock_binding::where('id',$request->idbinding)->first();
			if ($terpoakaipro > $request->terpakaipro) {
				$whj = $terpoakaipro - $request->terpakaipro;
				$hasil = $shbaku->jml_produksi + $whj;
			} elseif ($terpoakaipro < $request->terpakaipro) {
				$whj = $request->terpakaipro - $terpoakaipro;
				$hasil = $shbaku->jml_produksi - $whj;
			} else {
				$hasil = $shbaku->jml_produksi;
			}

			if ($masukweis > $request->masukweisb) {
				$qnb = $masukweis - $request->masukweisb;
				$awl = $shbind->stock_weisb - $qnb;
			} elseif ($masukweis < $request->masukweisb) {
				$qnb = $request->masukweisb - $masukweis;
				$awl = $shbind->stock_weisb + $qnb;
			} else {
				$awl = $shbind->stock_weisb;
			}

			if ($terpakaiweis > $request->terpakaiweisb) {
				$jkw = $terpakaiweis - $request->terpakaiweisb;
				$hasile = $awl + $jkw;
			} elseif ($terpakaiweis < $request->terpakaiweisb) {
				$jkw = $request->terpakaiweisb - $terpakaiweis;
				$hasile = $awl - $jkw;
			} else {
				$hasile = $awl;
			}

			if ($terpakaifill > $request->terpakaifilling) {
				$akerni = $terpakaifill - $request->terpakaifilling;
				$ghe = $shfill->jml_fill + $akerni;
			} elseif ($terpakaifill < $request->terpakaifilling) {
				$akerni = $request->terpakaifilling - $terpakaifill;
				$ghe = $shfill->jml_fill - $akerni;
			} else {
				$ghe = $shfill->jml_fill;
			}
			
			if ($batang > $request->batangjml) {
				$nah = $batang - $request->batangjml;
				$hasilw = $shbind->jml_bind - $nah;
			} elseif ($batang < $request->batangjml) {
				$nah = $request->batangjml - $batang;
				$hasilw = $shbind->jml_bind + $nah;
			} else {
				$hasilw = $shbind->jml_bind;
			}
			
			$slash = Stock_filling::find($request->idfilling);
			$slash->jml_fill = $ghe;
			$slash->save();

			$query1 = Stock_probaku::find($request->idprobaku);
			$query1->jml_produksi = $hasil;
			$query1->save();

			$query2 = Stock_binding::find($request->idbinding);
			$query2->jml_bind = $hasilw;
			$query2->stock_weisb = $hasile;
			$query2->save();
		}
	}

	public function loop_upbind($request,$id,$batang = '',$terpoakaipro = '')
	{
		$ceh = Binding::where([['stock_probaku_id','=',$request->idprobaku],['stock_binding_id','=',$request->idbinding],['stock_filling_id','=',$request->idfilling],['id','>',$id]]);
		if ($ceh->count() > 0) {
			$reg = $ceh->get();
			for ($i=0; $i < $ceh->count(); $i++) {
				$show = Binding::where([['stock_probaku_id','=',$request->idprobaku],['stock_binding_id','=',$request->idbinding],['stock_filling_id','=',$request->idfilling],['id','<',$reg[$i]->id]])->orderBy('id','desc')->take('1');
				if ($show->count() > 0) {
					$first = $show->first();
					$jebpro = $first->sisaprob - $reg[$i]->terpakaiprob;
					$bwrpro = $first->sisaweisb + $reg[$i]->weisbmasuk - $reg[$i]->terpakaiweisb;
					$hsie = $first->hasil_filling - $reg[$i]->terpakai_filling;
				} else {
					if ($batang == '' && $terpoakaipro == '') {
						$awal = $reg[$i]->sisaprob + $reg[$i]->terpakaiprob;
						$awl1 = $reg[$i]->hasil_filling + $reg[$i]->terpakai_filling;

						$jebpro = $awal - $reg[$i]->terpakaiprob;
						$bwrpro = 0 + $reg[$i]->weisbmasuk - $reg[$i]->terpakaiweisb;
						$hsie = $awl1 - $reg[$i]->terpakai_filling;
					} else {
						$awal = $reg[$i]->sisaprob + $reg[$i]->terpakaiprob + $terpoakaipro;
						$awl1 = $batang;

						$jebpro = $awal - $reg[$i]->terpakaiprob;
						$bwrpro = 0 + $reg[$i]->weisbmasuk - $reg[$i]->terpakaiweisb;
						$hsie = $awl1 - $reg[$i]->terpakai_filling;
					}
				}
				$update = Binding::where('id',$reg[$i]->id)->update(['sisaprob' => $jebpro,'sisaweisb' => $bwrpro,'hasil_filling' => $hsie]);
			}
		}
	}

}
