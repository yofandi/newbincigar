<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use DataTables;
use Auth;
use App\Packing;

use App\Http\Controllers\Back_packing;

class Packingcont extends Controller
{
	public function __construct()
	{
		$this->middleware('auth');
		$this->produksi = new Back_packing;
	}

	// ini data untuk menampilkan data
	public function getdatapacking()
	{
		$query = DB::table('packing')
		->select(
			'packing.*',
			'produk.id_produk',
			'produk.produk',
			'sub_produk.id_sub_produk',
			'sub_produk.isi','sub_produk.sub_kode',
			'sub_produk.sub_produk',
			'kemasan.id_kemasan',
			'kemasan.nama_kemasan',
			'history_cukai.id as idhiscukai',
			'history_cukai.stock_cukai_id_stock_cukai',
			'history_cukai.masuk_cukai',
			'history_cukai.terpakai_cukailama',
			'history_cukai.terpakai_cukaibaru',
			'history_cukai.sisa_cukai',
			'history_kemasan.id as idhiskem',
			'history_kemasan.masuk_kemasan',
			'history_kemasan.stock_kemasan_id_stock_kemasan',
			'history_kemasan.terpakai_kemasan',
			'history_kemasan.afkir_kemasan',
			'history_kemasan.stok_now',
			'history_stiker.id as idhissti',
			'history_stiker.stock_stiker_id_stock_stiker',
			'history_stiker.masuk_luar',
			'history_stiker.pakai_luar',
			'history_stiker.hasil_luar',
			'history_stiker.masuk_dalam',
			'history_stiker.pakai_dalam',
			'history_stiker.hasil_dalam',
			'history_cincin.id as idhiscin',
			'history_cincin.stock_cincin_id_stock_cincin',
			'history_cincin.masuk as masuk_cincin',
			'history_cincin.terpakai as terpakai_cincin',
			'history_cincin.afkir as afkir_cincin',
			'history_cincin.sisa as sisa_cincin'
		)
		->join('stock_packing','packing.stock_packing_id','=','stock_packing.id')
		->join('sub_produk','stock_packing.sub_produk_id_sub_produk','=','sub_produk.id_sub_produk')
		->join('kemasan','sub_produk.kemasan_id_kemasan','=','kemasan.id_kemasan')
		->join('produk','sub_produk.produk_id_produk','=','produk.id_produk')
		->leftJoin('history_cukai','packing.id','=','history_cukai.packing_id_packing')
		->join('history_kemasan','packing.id','=','history_kemasan.packing_id')
		->join('history_stiker','packing.id','=','history_stiker.packing_id')
		->join('history_cincin','packing.id','=','history_cincin.packing_id')
		->get();
		return Datatables::of($query)
		->addIndexColumn()
		// ->addColumn('awlbatang', function ($query) {
		// 	$hj = unserialize($query->hasil_batangpack);
		// 	if ($query->id_produk != 54) {
		// 		$awl = $hj['hasil_packing'] + $query->batangpack;
		// 	} else {
		// 		switch ($query->id_sub_produk) {
		// 			case 47:
		// 			$awl = 'Rbs: '.$hj['hasil_packingcolect'][11].'; H.Cor: '.$hj['hasil_packingcolect'][13].', Mau: '.$hj['hasil_packingcolect'][14];
		// 			break;
		// 		}
		// 	}
		// 	return $awl;
		// })
		->addColumn('awlpacking', function ($query) {
			$awl = $query->hasil_packing - $query->tambah_packing;
			return $awl;
		})
		->addColumn('stockawal', function ($query) {
			$awl = $query->sisa_cukai - $query->masuk_cukai + ($query->terpakai_cukailama + $query->terpakai_cukaibaru);
			return $awl;
		})
		->addColumn('stockawalcin', function ($query) {
			$awl = $query->sisa_cincin - $query->masuk_cincin + ($query->terpakai_cincin + $query->afkir_cincin);
			return $awl;
		})
		->addColumn('stockawlluarsti', function ($query) {
			$luar = $query->hasil_luar - $query->masuk_luar + $query->pakai_luar;
			return $luar;
		})
		->addColumn('stockawldalamsti', function ($query) {
			$dalam = $query->hasil_dalam - $query->masuk_dalam + $query->pakai_dalam;
			return $dalam;
		})
		->addColumn('stockawalkem', function ($query) {
			$awl = $query->stok_now - $query->masuk_kemasan + ($query->terpakai_kemasan + $query->afkir_kemasan);
			return $awl;
		})
		// ->addColumn('hasil_batangpack_obj', function ($query) {
		// 	$awl = unserialize($query->hasil_batangpack);
		// 	return json_encode($awl);
		// })
		->make(true);
	}
	// end

	// untuk munculkan data packing search
	public function searchpackingdata(Request $request)
	{   
		switch ($request->produk) {
			case 54:
			switch ($request->subproduk) {
				case 47:
				$data['stst'] = 'eklus';
				$data['rety'] = $this->produksi->spropackeks($request);
				break;
				case 48:
				$data['stst'] = 'bos';
				$data['rety'] = $this->produksi->spropackbos($request);
				break;
				case 49:
				$data['stst'] = 'havano';
				$data['rety'] = $this->produksi->spropackhvn($request);
				break;
			}
			break;

			default:
			$data['stst'] = 'single';
			$data['rety'] = $this->produksi->spropack($request);
			break;
		}
		return json_encode($data);
	}
	// end

	public function datapacking()
	{
		$data['produk'] = DB::table('produk')->get();
		return view('home.produksi.packing.datapacking',$data);
	}

	// tambah data packing
	public function addpacking(Request $request)
	{
		DB::beginTransaction();
		if ($request->produk != 54) {
			$mnv = $request->jmlpacking * $request->isikemasan;
		} else {
			switch ($request->subproduk) {
				case 47:
				$mnv = $request->jmlpacking * ( 10 + 5 + 5 );            
				break;

				case 48:
				$mnv = $request->jmlpacking * ( 3 + 4 + 3 + 4 + 5 + 6 );   
				break;

				case 49:
				$mnv = $request->jmlpacking * ( 3 + 3 + 3 + 3 + 4 );   
				break;
			}
		}
		$qwerty = $this->produksi->stockpacking($request);

		$jke = new Packing;
		$jke->batangpack = $mnv;
		$jke->awalbatang = serialize($qwerty['hasilqc']);
		// $jke->hasil_batangpack = serialize($qwerty['hasilqc']);
		$jke->tambah_packing = $request->jmlpacking;
		$jke->hasil_packing = $qwerty['hasilpack'];
		$jke->keterangan_packing = $request->ket;
		$jke->tanggal_packing = $request->tanggal_packing;
		$jke->stock_packing_id = $qwerty['idpack'];
		$jke->stock_qc_id = $qwerty['idqc'];
		$jke->author_session = Auth::user()->id;
		$jke->created_at = date('Y-m-d H:i:s');
		$jke->log = "INSERT";
		$query1 = $jke->save();
		$id = $jke->id;

		if ($query1) {
			$this->produksi->attibut_lengpack($request,$qwerty,$id);            
			$alert = 'Packing berhasil ditambahkan!';
		} else {
			$alert = 'Packing gagal ditambahkan!';
		}
		echo json_encode($alert);
		DB::commit();    
	}
	// end

	// data update packing
	public function dataupdatepack($request,$id,$poet)
	{
		$tye = $this->produksi->ifupppacking($request,$id,$poet);
		$mnv = $request->jmlpacking * $request->isikemasan;
		$btg = $request->batangqc - $mnv;
		$enr = $request->stockawal + $request->jmlpacking;
        // hasil bahan baku
		$enjr = $request->stockcukai + $request->masuk - ($request->terpakailama + $request->terpakaibaru);
		$kenmbr = $request->stockkemasan + $request->masuk - ($request->terpakaikemasan + $request->afkir);
		$hbeghl = $request->stockluarsti + $request->masukluar - $request->terpakailuar;
		$hbeghd = $request->stockdalamsti + $request->masukdalam - $request->terpakaidalam;
		$dsbjwe = $request->stockcincin + $request->masuk - ($request->terpakaicincin + $request->afkir);

		$data = [
			'packing.batangpack' => $mnv,
			// 'packing.hasil_batangpack' => serialize($tye),
			'packing.tambah_packing' => $request->jmlpacking,
			'packing.hasil_packing' => $enr,
			'packing.keterangan_packing' => $request->ket,
			'packing.tanggal_packing' => $request->tanggal_packing,
			'packing.author_session' => Auth::user()->id,
			'packing.log' => 'UPDATE',
			'history_kemasan.tanggal_hiskem' => $request->tanggal_packing,
			'history_kemasan.masuk_kemasan' => $request->masuk,
			'history_kemasan.terpakai_kemasan' => $request->terpakaikemasan,
			'history_kemasan.afkir_kemasan' => $request->afkir,
			'history_kemasan.stok_now' => $kenmbr,
			'history_kemasan.ket_kem' => "Kemasan Terpakai (Packing): ".$request->tanggal_packing." based on ID packing: ".$id,
			'history_kemasan.author_session' => Auth::user()->id,
			'history_kemasan.log' => "UPDATE",
			'history_stiker.masuk_luar' => $request->masukluar,
			'history_stiker.pakai_luar' => $request->terpakailuar,
			'history_stiker.hasil_luar' => $hbeghl,
			'history_stiker.masuk_dalam' => $request->masukdalam,
			'history_stiker.pakai_dalam' => $request->terpakaidalam,
			'history_stiker.hasil_dalam' => $hbeghd,
			'history_stiker.tanggal_hisstik' => $request->tanggal_packing,
			'history_stiker.ket_sti' => "Stiker Terpakai (Packing): ".$request->tanggal_packing." based on ID packing: ".$id,
			'history_stiker.author_session' => Auth::user()->id,
			'history_stiker.log' => "UPDATE",
			'history_cincin.masuk' => $request->masuk,
			'history_cincin.terpakai' => $request->terpakaicincin,
			'history_cincin.afkir' => $request->afkir,
			'history_cincin.sisa' => $dsbjwe,
			'history_cincin.tanggal_cincin' => $request->tanggal_packing,
			'history_cincin.ket_cin' => "Cincin Terpakai (Packing): ".$request->tanggal_packing." based on ID packing: ".$id,
			'history_cincin.author_session' => Auth::user()->id,
			'history_cincin.log' => "UPDATE",
		];
		if ($request->produk != 54) {
			$data += [
				'history_cukai.masuk_cukai' => $request->masuk,
				'history_cukai.terpakai_cukailama' => $request->terpakailama,
				'history_cukai.terpakai_cukaibaru' => $request->terpakaibaru,
				'history_cukai.sisa_cukai' => $enjr,
				'history_cukai.tanggal_cukai' => $request->tanggal_packing,
				'history_cukai.ket_hiscukai' => "Cukai Terpakai (Packing): ".$request->tanggal_packing." based on ID packing: ".$id,
				'history_cukai.packing_id_packing' => $id,
				'history_cukai.author_session' => Auth::user()->id,
				'history_cukai.log' => 'UPDATE'
			];
		}
		return $data;
	}

	public function arrayforupdatepacking($request,$id,$idhiscuk,$jml,$masuk,$lama,$baru,$idhiskem,$masukkem,$terpakaikem,$afkirkem,$idhissti,$masukl,$pakail,$masukd,$pakaid,$idhiscin,$masukcin,$terpakaicin,$afkircin)
	{
		$jart = [
			'jml' => $jml,
			'idhiskem' => $idhiskem,
			'masukkem' => $masukkem,
			'terpakaikem' => $terpakaikem,
			'afkirkem' => $afkirkem,
			'idhissti' => $idhissti,
			'masukl' => $masukl,
			'pakail' => $pakail,
			'masukd' => $masukd,
			'pakaid' => $pakaid,
			'idhiscin' => $idhiscin,
			'masukcin' => $masukcin,
			'terpakaicin' => $terpakaicin,
			'afkircin' => $afkircin,
			'idhiscuk' => $idhiscuk,
			'masuk' => $masuk,
			'lama' => $lama,
			'baru' => $baru,
		];
		switch ($request->subproduk) {
			case 47:
			$idproert = [11,13,14];
			$isiquan = [10,5,5];
			$stockklej = [$request->batangrobus,$request->batanghalf,$request->batangmaumere
				];
			break;

			case 48:
			$idproert = [11,12,13,15,16,19];
			$isiquan = [3,4,3,4,5,6];
			$stockklej = [$request->batangrbs_bos,$request->batangcor_bos,$request->batanghlf_bos,$request->batangcgr_bos,$request->batangelnino_bos,$request->batangc99_bos];
			break;

			case 49:
			$idproert = [11,12,13,15,16];
			$isiquan = [3,3,3,3,4];
			$stockklej = [$request->batangrbs_hvn,$request->batangcor_hvn,$request->batanghlf_hvn,$request->batangcgr_hvn,$request->batangelnino_hvn];
			break;
		}
		if ($request->produk == 54) {
			$jart += [
				'idqcbaseproduk' => $idproert,
				'isi' => $isiquan,
				'jml_stockawl' => $stockklej
			];
		}
		return $jart;
	}

	public function upppacking(Request $request,$id,$idhiscuk,$jml,$masuk,$lama,$baru,$idhiskem,$masukkem,$terpakaikem,$afkirkem,$idhissti,$masukl,$pakail,$masukd,$pakaid,$idhiscin,$masukcin,$terpakaicin,$afkircin)
	{
		DB::beginTransaction();

		$jart = $this->arrayforupdatepacking($request,$id,$idhiscuk,$jml,$masuk,$lama,$baru,$idhiskem,$masukkem,$terpakaikem,$afkirkem,$idhissti,$masukl,$pakail,$masukd,$pakaid,$idhiscin,$masukcin,$terpakaicin,$afkircin);
		$ghety = $this->dataupdatepack($request,$id,$jart);

		$query = DB::table('packing')
		->join('history_kemasan','packing.id','=','history_kemasan.packing_id')
		->join('history_stiker','packing.id','=','history_stiker.packing_id')
		->join('history_cincin','packing.id','=','history_cincin.packing_id');

		if ($request->produk != 54) {
			$query->join('history_cukai','packing.id','=','history_cukai.packing_id_packing');
		}

		$query->where([['packing.id','=',$id]]);
		$shet = $query->update($ghety);
		if ($shet) {
			$this->produksi->loop_uppacking($request,$id);
			$this->produksi->attibut_updatelooppack($request,$jart['idhiscuk'],$jart['idhiskem'],$jart['idhissti'],$jart['idhiscin']);
			$alert = 'Packing ID: '.$id.' berhasil diupdate!';
		} else {
			$alert = 'Packing ID: '.$id.' gagal diupdate!';
		}
		echo json_encode($alert);
		DB::commit();    
	}
	// end

	// delete data packing
	public function arrayfordeletepacking($request,$id,$idstocukai,$jml,$masuk,$lama,$baru,$idstokem,$masukkem,$terpakaikem,$afkirkem,$idstosti,$masukl,$pakail,$masukd,$pakaid,$idstocin,$masukcin,$terpakaicin,$afkircin,$awlcukai,$awlcincin,$awlstiluar,$awlstidalam,$awlkemasan)
	{
		$mnv = $jml * $request->isikemasan;

		$jart = [
			'idstocukai' => $idstocukai,
			'jml' => $jml,
			'masuk' => $masuk,
			'lama' => $lama,
			'baru' => $baru,
			'idstokem' => $idstokem,
			'masukkem' => $masukkem,
			'terpakaikem' => $terpakaikem,
			'afkirkem' => $afkirkem,
			'idstosti' => $idstosti,
			'masukl' => $masukl,
			'pakail' => $pakail,
			'masukd' => $masukd,
			'pakaid' => $pakaid,
			'idstocin' => $idstocin,
			'masukcin' => $masukcin,
			'terpakaicin' => $terpakaicin,
			'afkircin' => $afkircin,
			'awlcukai' => $awlcukai,
			'awlcincin' => $awlcincin,
			'awlstiluar' => $awlstiluar,
			'awlstidalam' => $awlstidalam,
			'awlkemasan' => $awlkemasan,
			'mnv' => $mnv,
		];
		switch ($request->subproduk) {
			case 47:
			$idproert = [11,13,14];
			$isiquan = [10,5,5];
			$stockklej = [$request->batangrobus,$request->batanghalf,$request->batangmaumere
				];
			break;

			case 48:
			$idproert = [11,12,13,15,16,19];
			$isiquan = [3,4,3,4,5,6];
			$stockklej = [$request->batangrbs_bos,$request->batangcor_bos,$request->batanghlf_bos,$request->batangcgr_bos,$request->batangelnino_bos,$request->batangc99_bos];
			break;

			case 49:
			$idproert = [11,12,13,15,16];
			$isiquan = [3,3,3,3,4];
			$stockklej = [$request->batangrbs_hvn,$request->batangcor_hvn,$request->batanghlf_hvn,$request->batangcgr_hvn,$request->batangelnino_hvn];
			break;
		}
		if ($request->produk == 54) {
			$jart += [
				'idqcbaseproduk' => $idproert,
				'isi' => $isiquan,
				'jml_stockawl' => $stockklej
			];
		}
		return $jart;
	}

	public function delpacking(Request $request,$id,$idstocukai,$jml,$masuk,$lama,$baru,$idstokem,$masukkem,$terpakaikem,$afkirkem,$idstosti,$masukl,$pakail,$masukd,$pakaid,$idstocin,$masukcin,$terpakaicin,$afkircin,$awlcukai,$awlcincin,$awlstiluar,$awlstidalam,$awlkemasan)
	{
		DB::beginTransaction();
		$jart = $this->arrayfordeletepacking($request,$id,$idstocukai,$jml,$masuk,$lama,$baru,$idstokem,$masukkem,$terpakaikem,$afkirkem,$idstosti,$masukl,$pakail,$masukd,$pakaid,$idstocin,$masukcin,$terpakaicin,$afkircin,$awlcukai,$awlcincin,$awlstiluar,$awlstidalam,$awlkemasan);
		$this->produksi->updatestockwhendeletepack($request,$jart);
		
		$mnv = $jml * $request->isikemasan;

		$query = DB::table('packing')
		->where('id',$id)
		->delete();
		if ($query) {
			$this->produksi->loop_uppacking($request,$id,$jml,$mnv);
			$this->produksi->attibut_deletelooppack($request,$idstocukai,$idstokem,$idstosti,$idstocin,$awlcukai,$awlcincin,$awlstiluar,$awlstidalam,$awlkemasan);
			$alert = 'Packing ID : '.$id.' berhasil dihapus!';
		} else {
			$alert = 'Packing ID : '.$id.' gagal dihapus!';
		}
		echo json_encode($alert);
		DB::commit();    
	}
	// end

}
