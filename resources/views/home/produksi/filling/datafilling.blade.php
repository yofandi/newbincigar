@extends('../../layout')

@section('title')
<title>BIN - ERP : Data Filling</title>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css" />
@endsection

@section('content')
<div class="row page-titles">
	<div class="col-md-5 align-self-center">
		<h3 class="text-themecolor">Filling</h3>
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a href="javascript:void(0)">Proses</a></li>
			<li class="breadcrumb-item active">Data Filling</li>
		</ol>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="row">
			<div class="col-md-6">
				<legend>Data Filling</legend>
			</div>
			<div class="col-md-2 offset-md-4">
				<button type="button" data-link="{{ route('filling.post') }}" class="btn btn-success btn-rounded btn-icons " id="tambah-filling">Tambah</button>
			</div>
			<div class="table-responsive m-t-40">
				<table id="table-filling" class="display nowrap table table-hover table-striped table-bordered">
					<thead>
						<tr>
							<th>No.</th>
							<th>Tanggal</th>
							<th>Produk</th>
							<th>Jenis</th>
							<th>Stock</th>
							<th>Masuk (Weis)</th>
							<th>Terpakai (Produksi)</th>
							<th>Terpakai (Weis)</th>
							<th>Sisa</th>
							<th>Jml Batang</th>
							<th>Aksi</th>
						</tr>
					</thead>
				</table>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="modal-filling" tabindex="-1" role="dialog" aria-labelledby="title-tembakau" aria-hidden="true" style="display: none;">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="card">
				<div class="card-header">
					<div class="modal-header">
						<h4 class="modal-title" id="title"></h4>
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					</div>
				</div>
				<div class="card-body">
					<form id="form-filling" method="POST">
						<div class="modal-body row">
							<div class="form-group col-md-12">
								<label class="control-label">Tanggal</label>
								<input type="text" class="form-control date_sam" id="tanggal_filling">
							</div>
							<div class="form-group col-md-6">
								<label class="control-label">Jenis</label>
								<select class="form-control" id="jenis"></select>
								<input type="hidden" id="jenis_in">
							</div>
							<div class="form-group col-md-6">
								<label class="control-label">Produk</label>
								<select class="form-control" id="produk"></select>
								<input type="hidden" id="produk_in">
							</div>
							<div class="form-group col-md-12" id="stock_showit">
								<button type="button" onclick="seacrh_data()" class="btn btn-primary btn-xs btn-block">Search</button>
								<input type="hidden" id="idprobaku">
								<input type="hidden" id="idfilling">
								<input type="hidden" id="idjnpro">
							</div>
							<div class="form-group col-md-6">
								<label class="control-label">Stock Produksi (Saat ini/Awal :*Gram)</label>
								<input type="number" class="form-control" step="any" id="stockpro" value="0" placeholder="Stock (Saat ini/Awal)" readonly>
							</div>
							<div class="form-group col-md-6">
								<label class="control-label">Stock Weisf (*Gram)</label>
								<input type="number" class="form-control" step="any" id="stockweisf" value="0" placeholder="Stock Weisf" readonly>
							</div>
							<div class="form-group col-md-12">
								<label class="control-label">Terpakai Produksi (*Gram)</label>
								<input type="number" class="form-control" step="any" id="terpakaipro" placeholder="Stock Produksi Terpakai">
							</div>
							<div class="form-group col-md-6">
								<label class="control-label">Masuk Weis (*Gram)</label>
								<input type="number" class="form-control" step="any" id="masukweisf" placeholder="Stock Masuk Weisf">
							</div>
							<div class="form-group col-md-6">
								<label class="control-label">Terpakai Weisf (*Gram)</label>
								<input type="number" class="form-control" step="any" id="terpakaiweisf" placeholder="Stock Produksi Weisf">
							</div>
							<div class="form-group col-md-12">
								<label class="control-label">Batang Cerutu Yang Dihasilkan</label>
								<input type="number" class="form-control" id="batangjml" placeholder="Jml Batang">
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" onclick="save_method()" class="btn btn-primary waves-effect text-left">Save</button>
							<button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Close</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('javascript')
<script src="{{ asset('node_modules/moment/moment.js') }}">
</script><script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
<script src="{{ asset('node_modules/datatables/datatables.min.js') }}"></script>
<script>
	$('#stock_showit').hide();
	let token = $("meta[name='csrf-token']").attr("content");

	const jenis = {!! json_encode($jenis) !!};
	jenis.unshift({id_jenis: '',jenis: ''});

	var table = $('#table-filling').DataTable({
		"lengthMenu": [
		[3,5,10, 25, 50, 100, 150, 200, -1],
		[3,5,10, 25, 50, 100, 150, 200, "All"]
		],
		"order": [
		[0, 'desc']
		],
		processing: true,
		serverSide: true,
		ajax: '/datafilling/json',
		columns: [
		{ data: 'DT_RowIndex', name: 'DT_RowIndex' },
		{ data: 'tanggal_fill', name: 'tanggal_fill' },
		{ data: 'produk', name: 'produk' },
		{ data: 'jenis', name: 'jenis' },
		{ data: 'stockawal', name: 'stockawal' },
		{ data: 'weisfmasuk', name: 'weisfmasuk' },
		{ data: 'terpakaiprof', name: 'terpakaiprof' },
		{ data: 'terpakaiweisf', name: 'terpakaiweisf' },
		{ data: 'sisaprof', name: 'sisaprof' },
		{ data: 'hasil_fill', name: 'hasil_fill' },
		{
			data: null,
			"bSortable": false,
			mRender: function (o) { 
				return '<button type="button" class="btn btn-info btn-rounded btn-xs" id="update-filling" data-title="Update Filling | ID: '+ o.id +'" data-link="/datafilling/upp/'+ o.id +'/'+ o.weisfmasuk +'/'+ o.terpakaiprof +'/'+ o.terpakaiweisf +'/'+ o.hasil_fill +'" data-tanggal_filling="'+ o.tanggal_fill +'" data-jenis="'+ o.id_jenis +'" data-produk="'+ o.id_produk +'" data-stockpro="'+ o.stockawal +'" data-stockweis="'+ o.stockawalweis +'" data-masukweis="'+ o.weisfmasuk +'" data-terpakaiprof="'+ o.terpakaiprof +'" data-terpakaiweisf="'+ o.terpakaiweisf +'" data-batang="'+ o.hasil_fill +'" data-idprobaku="'+ o.stock_probaku_id +'" data-idfilling="'+ o.stock_filling_id +'"><i class="mdi mdi-dots-horizontal"></i></button><button type="button" class="btn btn-danger btn-rounded btn-xs" id="delete-filling" data-link="/datafilling/del/'+ o.id +'/'+ o.weisfmasuk +'/'+ o.terpakaiprof +'/'+ o.terpakaiweisf +'/'+ o.hasil_fill +'" data-idprobaku="'+ o.stock_probaku_id +'" data-idfilling="'+ o.stock_filling_id +'"><i class="fa fa-trash"></i></button>'; }
			},
			],
			"displayLength": 10,
		});
	$('#table-filling tbody').on('click', 'tr.group', function() {
		var currentOrder = table.order()[0];
		if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
			table.order([2, 'desc']).draw();
		} else {
			table.order([2, 'asc']).draw();
		}
	});
	$('#table-filling tbody').on('click', 'tr.group', function() {
		var currentOrder = table.order()[0];
		if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
			table.order([2, 'desc']).draw();
		} else {
			table.order([2, 'asc']).draw();
		}
	});

	function getdatasearch(position1,position2) {
		let jenis = $('#jenis_in').val();
		let produk = $('#produk_in').val();

		$.ajax({
			url: '/datafilling/getsearchfill',
			type: 'POST',
			dataType: 'json',
			data: {
				"_token": token,
				produk: produk,
				jenis: jenis
			},
		})
		.done(function(data) {
			$('#idfilling').val(data.idfilling);
			$('#idprobaku').val(data.id);
			$('#' + position1).val(data.jml_produksi);
			$('#' + position2).val(data.stock_weisf);
		});
		
	}

	function ljenis(position,like) {
		let opt = '';
		for (let i = 0; i < jenis.length; i++) {
			opt += '<option value="'+ jenis[i].id_jenis +'"';
			if (jenis[i].id_jenis == like) {
				opt += 'selected';
			}
			opt += '>'+ jenis[i].jenis +'</option>';
		}
		$('#' + position).html(opt);
	}

	function lproduk(position,based,like) {
		$.ajax({
			url: '/datafilling/getproduk',
			type: 'POST',
			dataType: 'json',
			data: {
				"_token": token,
				jenis: based
			},
		})
		.done(function(data) {
			let opt = '<option value=""></option>';
			for(let i = 0; i < data.length; i++) {
				opt += '<option value="'+ data[i].id_produk +'"';
				if (data[i].id_produk == like) {
					opt += 'selected';
				}
				opt += ' data-id="'+ data[i].relid +'">'+ data[i].produk +'</option>';
			}
			$('#' + position).html(opt);
		});
	}

	function seacrh_data() {
		getdatasearch('stockpro','stockweisf');
	}

	function save_method() {
		let link = $('#form-filling').attr('action');
		let idprobaku = $('#idprobaku').val();
		let idfilling = $('#idfilling').val();
		let tanggal_filling = $('#tanggal_filling').val();
		let jenis_in = $('#jenis_in').val();
		let produk_in = $('#produk_in').val();
		let stockpro = $('#stockpro').val();
		let stockweisf = $('#stockweisf').val();
		let masukweisf = $('#masukweisf').val();
		let terpakaipro = $('#terpakaipro').val();
		let terpakaiweisf = $('#terpakaiweisf').val();
		let batangjml = $('#batangjml').val();
		let idjnpro = $('#idjnpro').val();
		$.ajax({
			url: link,
			type: 'POST',
			dataType: 'json',
			data: {
				"_token": token,
				tanggal_filling: tanggal_filling,
				jenis: jenis_in,
				produk: produk_in,
				stockpro: stockpro,
				stockweisf: stockweisf,
				masukweisf:masukweisf,
				terpakaipro: terpakaipro,
				terpakaiweisf: terpakaiweisf,
				batangjml: batangjml,
				idprobaku: idprobaku,
				idfilling: idfilling,
				idjnpro:idjnpro
			},
		})
		.done(function(data) {
			alert(data);
			// console.log(data);
			$('#table-filling').DataTable().ajax.reload();
			$('#modal-filling').modal('toggle');
		});
		
	}

	$(document).on('click', '#tambah-filling', function(event) {
		let clik = $(this);
		$('.modal-header #title').text('Tambah filling');
		$('#form-filling').attr('action', clik.data('link'));
		$('#form-filling').find('input').val('');
		$('#form-filling').find('select').val('');
		$("#tanggal_filling").datetimepicker({
			format: 'YYYY-MM-DD HH:mm:ss',
		}).data("DateTimePicker").date(new Date());
		$('#stock_showit').fadeIn();
		$('#jenis').removeAttr('disabled');
		$('#produk').removeAttr('disabled');
		ljenis('jenis');
		$(document).on('change', '#jenis', function(event) {
			let jam = $(this).val();
			$('#jenis_in').val(jam);
			lproduk('produk',jam);
			$(document).on('change', '#produk', function(event) {
				let pwe = $(this).val();
				$('#produk_in').val(pwe);
				let kk = $(this).find(':selected').data('id');
				$('#idjnpro').val(kk);
			});
		});
		$('#modal-filling').modal('show');
	});

	$(document).on('click', '#update-filling', function(event) {
		let click = $(this);
		$('.modal-header #title').text(click.data('title'));
		$('#form-filling').attr('action', click.data('link'));
		$('#form-filling').find('input').val('');
		$('#form-filling').find('select').val('');
		$('#stock_showit').hide();
		$('#jenis').attr('disabled','disabled');
		$('#produk').attr('disabled','disabled');
		ljenis('jenis',click.data('jenis'));
		lproduk('produk',click.data('jenis'),click.data('produk'));

		$("#tanggal_filling").datetimepicker({
			format: 'YYYY-MM-DD HH:mm:ss',
		}).data("DateTimePicker").date(click.data('tanggal_filling'));
		$('#jenis_in').val(click.data('jenis'));
		$('#produk_in').val(click.data('produk'));
		$('#stockpro').val(click.data('stockpro'));
		$('#stockweisf').val(click.data('stockweis'));
		$('#masukweisf').val(click.data('masukweis'));
		$('#terpakaipro').val(click.data('terpakaiprof'));
		$('#terpakaiweisf').val(click.data('terpakaiweisf'));
		$('#batangjml').val(click.data('batang'));
		$('#idprobaku').val(click.data('idprobaku'));
		$('#idfilling').val(click.data('idfilling'));

		$('#modal-filling').modal('show');
	});

	$(document).on('click', '#delete-filling', function(event) {
		let sjn = confirm('Apakah anda yakin ingin menghapus data ini ?');
		if (sjn) {
			let link = $(this).data("link");
			let idprobaku = $(this).data('idprobaku');
			let idfilling = $(this).data('idfilling');
			$.ajax(
			{
				url: link,
				type: 'DELETE',
				data: {
					"_token": token,
					idprobaku: idprobaku,
					idfilling: idfilling,
				},
				success: function (data){
					alert(data);
					$('#table-filling').DataTable().ajax.reload();
				}
			});
		}
	});
</script>
@endsection