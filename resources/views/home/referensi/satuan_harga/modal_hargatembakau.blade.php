<div class="modal fade" id="modal-satuan" tabindex="-1" role="dialog" aria-labelledby="title" aria-hidden="true" style="display: none;">
	<div class="modal-dialog modal-xl">
		<div class="modal-content">
			<div class="card">
				<div class="card-header">
					<div class="modal-header">
						<h4 class="modal-title" id="title"></h4>
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					</div>
				</div>
				<div class="card-body">
					<form id="form-satuan" method="POST">
						<div class="modal-body row">
							<div class="form-group col-md-12 fa-3x spinner_nekr" align="center">
								<i class="fas fa-spinner fa-spin"></i>
							</div>
							<div class="form-group col-md-12">
								<label class="control-label">Tanggal</label>
								<input type="text" class="form-control" id="tanggal_temabaku">
							</div>
							<div class="form-group col-md-12">
								<label class="control-label">Kode Harga Tembakau</label>
								<input type="text" class="form-control" id="kode_htembakau" placeholder="Kode Harga Tembakau">
							</div>
							<div class="form-group col-md-6">
								<label class="control-label">Jenis</label>
								<select class="form-control" id="jenislah"></select>
							</div>
							<div class="form-group col-md-6">
								<label class="control-label">Kategori</label>
								<select class="form-control" id="kategorilah"></select>
							</div>
							<div class="form-group col-md-12">
								<label class="control-label">Satuan Berat</label>
								<select class="form-control" id="sat_berat"></select>
								{{-- <input type="text" class="form-control" id="sat_berat" placeholder="Satuan Berat"> --}}
							</div>
							<div class="form-group col-md-12">
								<label class="control-label">Harga Tembakau /satuan</label>
								<input type="text" class="form-control" id="harga_satuan">
							</div>
							<div class="form-group col-md-12">
								<label class="control-label">Keterangan</label>
								<textarea id="keterangan" class="form-control" placeholder="keterangan"></textarea>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" onclick="save_method()" class="btn btn-primary waves-effect text-left">Save</button>
							<button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Close</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>