@extends('../../layout')

@section('title')
<title>BIN - ERP : Setting</title>
@endsection

@section('content')
<div class="row page-titles">
	<div class="col-md-5 align-self-center">
		<h3 class="text-themecolor">Setting</h3>
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a href="javascript:void(0)">Referensi</a></li>
			<li class="breadcrumb-item active">Setting</li>
		</ol>
	</div>
</div>
<div class="row">
	<div class="col-md-12 m-t-30">
		<div class="card">
			<div class="card-body">
				<div class="row">
					<div class="col-md-4">
						<h4 class="card-title">Setting</h4>
					</div>
					<div class="col-md-4 offset-md-4">
						<button type="button" onclick="load_setting()" class="btn btn-xs btn-secondary">Refresh</button>
					</div>
				</div>
				<form class="row" id="form-setting" method="POST" action="{{ route('setting.post') }}">
					@csrf
					<div class="form-group col-md-12">
						<label class="control-label">Direktur Utama :</label>
						<input type="hidden" id="idsetting">
						<input type="text" class="form-control" id="direk_uta" placeholder="Direktur Utama">
					</div>
					<div class="form-group col-md-12">
						<label class="control-label">Direktur Operasional :</label>
						<input type="text" class="form-control" id="direk_ops" placeholder="Direktur Operasional">
					</div>
					<div class="form-group col-md-12">
						<label class="control-label">Kabag.Produksi :</label>
						<input type="text" class="form-control" id="kabag_pro" placeholder="Kabag. Produksi">
					</div>
					<div class="form-group col-md-12">
						<label class="control-label">Quality Control :</label>
						<input type="text" class="form-control" id="quality" placeholder="Quality Control">
					</div>
					<div class="form-group col-md-12">
						<label class="control-label">Pengiriman :</label>
						<input type="text" class="form-control" id="rfs" placeholder="Pengiriman">
					</div>
					<div class="form-group col-md-12">
						<button type="button" onclick="save_method()" class="btn btn-info btn-md">Save</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
@endsection

@section('javascript')
<script>
	let token = $("meta[name='csrf-token']").attr("content");

	load_setting();
	function load_setting() {
		$.ajax({
			url: '/setting/json',
			type: 'GET',
			dataType: 'json',
		})
		.done(function(data) {
			$('#idsetting').val(data.idsetting);
			$('#direk_uta').val(data.direktur_utama);
			$('#direk_ops').val(data.direktur_operasional);
			$('#kabag_pro').val(data.kabag_produksi);
			$('#quality').val(data.quality_control);
			$('#rfs').val(data.ready_for_sale);
		});
		
	}

	function save_method() {
		let link = $('#form-setting').attr('action');
		let idsetting = $('#idsetting').val();
		let direk_uta = $('#direk_uta').val();
		let direk_ops = $('#direk_ops').val();
		let kabag_pro = $('#kabag_pro').val();
		let quality = $('#quality').val();
		let rfs = $('#rfs').val();

		$.ajax({
			url: link,
			type: 'POST',
			dataType: 'json',
			data: {
				"_token": token,
				idsetting: idsetting,
				direk_uta: direk_uta,
				direk_ops: direk_ops,
				kabag_pro: kabag_pro,
				quality: quality,
				rfs: rfs
			},
		})
		.done(function(data) {
			alert(data);
			load_setting();
		});
	}
</script>
@endsection