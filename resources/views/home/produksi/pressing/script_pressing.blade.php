<script>
	$('#stock_showitpres').hide();
	$('#loadingdeletepres').hide();
	$('.spinner_nekrpres').hide();
	var tablepres = $('#table-pressing').DataTable({
		"lengthMenu": [
		[3,5,10, 25, 50, 100, 150, 200, -1],
		[3,5,10, 25, 50, 100, 150, 200, "All"]
		],
		"order": [
		[0, 'desc']
		],
		processing: true,
		serverSide: true,
		ajax: '/datapressing/json',
		columns: [
		{ data: 'DT_RowIndex', name: 'DT_RowIndex' },
		{ data: 'tanggal_pres', name: 'tanggal_pres' },
		{ data: 'produk', name: 'produk' },
		{ data: 'jenis', name: 'jenis' },
		{ data: 'lama', name: 'lama' },
		{ data: 'awlbatang', name: 'awlbatang' },
		{ data: 'tambah_pres', name: 'tambah_pres' },
		{ data: 'hasil_akhirp', name: 'hasil_akhirp' },
		{ data: 'keterangan_pres', name: 'keterangan_pres' },
		{
			data: null,
			"bSortable": false,
			mRender: function (o) { 
				return '<button type="button" class="btn btn-info btn-rounded btn-xs" id="update-pressing" data-title="Update Pressing | ID: '+ o.id +'" data-link="/datapressing/upp/'+ o.id +'/'+ o.tambah_pres +'" data-tanggal_pressing="'+ o.tanggal_pres +'" data-jenis="'+ o.id_jenis +'" data-produk="'+ o.id_produk +'" data-durasi="'+ o.lama +'" data-ket="'+ o.keterangan_pres +'" data-stockbatang="'+ o.awlbatang +'" data-batang="'+ o.tambah_pres +'" data-idpressing="'+ o.stock_pressing_id +'" data-idbinding="'+ o.stock_binding_id +'"><i class="mdi mdi-dots-horizontal"></i></button><button type="button" class="btn btn-danger btn-rounded btn-xs" id="delete-pressing" data-link="/datapressing/del/'+ o.id +'/'+ o.tambah_pres +'" data-idpressing="'+ o.stock_pressing_id +'" data-idbinding="'+ o.stock_binding_id +'"><i class="fa fa-trash"></i></button>'; }
			},
			],
			"displayLength": 10,
		});
	$('#table-pressing tbody').on('click', 'tr.group', function() {
		var currentOrder = tablepres.order()[0];
		if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
			tablepres.order([2, 'desc']).draw();
		} else {
			tablepres.order([2, 'asc']).draw();
		}
	});
	$('#table-pressing tbody').on('click', 'tr.group', function() {
		var currentOrder = tablepres.order()[0];
		if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
			tablepres.order([2, 'desc']).draw();
		} else {
			tablepres.order([2, 'asc']).draw();
		}
	});

	function Refresh_pressing() {
		$('#table-pressing').DataTable().ajax.reload();
	}

	function getdatasearchpres(position1) {
		let jenis = $('#jenis_inpres').val();
		let produk = $('#produk_inpres').val();

		$.ajax({
			url: '/datapressing/getsearchfill',
			type: 'POST',
			dataType: 'json',
			data: {
				"_token": token,
				produk: produk,
				jenis: jenis
			},
		})
		.done(function(data) {
			$('#idbindingpres').val(data.idbinding);
			$('#idpressing').val(data.idpressing);
			$('#' + position1).val(data.jml_bind);
		});
		
	}

	function seacrh_datapres() {
		getdatasearchpres('batangbind');
	}

	function save_methodpres() {
		let link = $('#form-pressing').attr('action');
		let idbinding = $('#idbindingpres').val();
		let idpressing = $('#idpressing').val();
		let idjnpro = $('#idjnpropres').val();
		let tanggal_pressing = $('#tanggal_pressing').val();
		let jenis_in = $('#jenis_inpres').val();
		let produk_in = $('#produk_inpres').val();
		let stockbatang = $('#batangbind').val();
		let batangjml = $('#batangjmlpres').val();
		let durasi = $('#durasipres').val();
		let ket = $('#ketpres').val();
		$.ajax({
			url: link,
			type: 'POST',
			dataType: 'json',
			data: {
				"_token": token,
				tanggal_pressing: tanggal_pressing,
				jenis: jenis_in,
				produk: produk_in,
				durasi:durasi,
				stockbatang: stockbatang,
				batangjml: batangjml,
				ket:ket,
				idpressing: idpressing,
				idbinding: idbinding,
				idjnpro:idjnpro
			},
			beforeSend: function() {
				isProcessing = true;
				$('.spinner_nekrpres').fadeIn();
			},
			success: function(data) {
				$('.spinner_nekrpres').hide();
				isProcessing = false;
				alert(data);
				Refresh_pressing();
				$('#modal-pressing').modal('toggle');
			}
		});
	}

	$(document).on('click', '#tambah-pressing', function(event) {
		let clik = $(this);
		$('.modal-header #title').text('Tambah pressing');
		$('#form-pressing').attr('action', clik.data('link'));
		$('#form-pressing').find('input').val('');
		$('#form-pressing').find('select').val('');
		$('#form-pressing').find('textarea').val('');
		$("#tanggal_pressing").datetimepicker({
			format: 'YYYY-MM-DD HH:mm:ss',
		}).data("DateTimePicker").date(new Date());
		// $('#durasipres').mdtimepicker({format: 'hh:mm'});
		$('#stock_showitpres').fadeIn();
		$('#jenispres').removeAttr('disabled');
		$('#produkpres').removeAttr('disabled');
		ljenis('jenispres');
		$(document).on('change', '#jenispres', function(event) {
			let jam = $(this).val();
			$('#jenis_inpres').val(jam);
			lproduk('produkpres',jam);
			$(document).on('change', '#produkpres', function(event) {
				let pwe = $(this).val();
				$('#produk_inpres').val(pwe);
				let kk = $(this).find(':selected').data('id');
				$('#idjnpropres').val(kk);
			});
		});
		$('#modal-pressing').modal('show');
	});

	$(document).on('click', '#update-pressing', function(event) {
		let click = $(this);
		$('.modal-header #title').text(click.data('title'));
		$('#form-pressing').attr('action', click.data('link'));
		$('#form-pressing').find('input').val('');
		$('#form-pressing').find('select').val('');
		$('#form-pressing').find('textarea').val('');
		$('#stock_showitpres').hide();
		$('#jenispres').attr('disabled','disabled');
		$('#produkpres').attr('disabled','disabled');
		ljenis('jenispres',click.data('jenis'));
		lproduk('produkpres',click.data('jenis'),click.data('produk'));

		$("#tanggal_pressing").datetimepicker({
			format: 'YYYY-MM-DD HH:mm:ss',
		}).data("DateTimePicker").date(click.data('tanggal_pressing'));
		// $('#durasipres').mdtimepicker({format: 'hh:mm'});
		$('#jenis_inpres').val(click.data('jenis'));
		$('#produk_inpres').val(click.data('produk'));
		$('#batangbind').val(click.data('stockbatang'));
		$('#batangjmlpres').val(click.data('batang'));
		$('#idbindingpres').val(click.data('idbinding'));
		$('#idpressing').val(click.data('idpressing'));
		$('#durasipres').val(click.data('durasi'));
		$('#ketpres').val(click.data('ketpres'));
		$('#modal-pressing').modal('show');
	});

	$(document).on('click', '#delete-pressing', function(event) {
		let jsb = confirm('Apakah anda yakin ingin menghapus data ini ?');
		if (jsb) {
			let link = $(this).data("link");
			let idpressing = $(this).data('idpressing');
			let idbinding = $(this).data('idbinding');
			$.ajax(
			{
				url: link,
				type: 'DELETE',
				data: {
					"_token": token,
					idpressing: idpressing,
					idbinding: idbinding,
				},
				beforeSend: function() {
					isProcessing = true;
					$('#loadingdeletepres').fadeIn();
				},
				success: function (data){
					$('#loadingdeletepres').hide();
					isProcessing = false;
					alert(data);
					Refresh_pressing();
				}
			});
		}
	});
</script>