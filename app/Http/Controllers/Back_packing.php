<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;
use App\Packing;
use App\Stock_qc;
use App\Stock_packing;

use App\History_cincin;
use App\History_stiker;
use App\History_cukai;
use App\History_kemasan;

use App\Http\Controllers\Back_bahanbaku;

class Back_packing extends Controller
{
	public function __construct()
	{
		$this->bahan = $res = new Back_bahanbaku;
	}

	public function spropack($request) 
	{
		$result = DB::table('stock_cukai')->where([['sub_produk_id_sub_produk','=',$request->subproduk],['sub_produk_produk_id_produk','=',$request->produk]]);

		$qusrt1 = DB::table('stock_qc')
		->select('stock_qc.id','stock_qc.jml_qc')
		->join('jenis_has_produk','stock_qc.jenis_has_produk_id_jnpro','=','jenis_has_produk.id_jnpro')
		->where([['jenis_has_produk.produk_id_produk','=',$request->produk]]);

		$weylp = DB::table('stock_packing')->where([['sub_produk_id_sub_produk','=',$request->subproduk]]);

		$per = DB::table('sub_produk')
		->select('sub_produk.isi','kemasan.id_kemasan','kemasan.nama_kemasan')
		->join('kemasan','sub_produk.kemasan_id_kemasan','=','kemasan.id_kemasan')
		->join('produk','sub_produk.produk_id_produk','=','produk.id_produk')
		->where('id_sub_produk',$request->subproduk);

		if ($result->count() > 0) {
			$cek = $result->first();
			$hasil = $cek->stock_qty_cukai;
			$idcukai = $cek->id_stock_cukai;
		} else {
			$hasil = 0; $idcukai = '';
		}

		if ($qusrt1->count() > 0) {
			$aem1 = $qusrt1->first();
			$ber = $aem1->jml_qc;
			$jkd = $aem1->id;
		} else { $ber = 0; $jkd = ''; }

		if ($weylp->count() > 0) {
			$qop = $weylp->first();
			$kjf = $qop->jml_packing;
			$pejj = $qop->id;
		} else {
			$kjf = 0; $pejj = '';
		}

		if ($per->count() > 0) {
			$ajk = $per->first();

			$ghr = DB::table('stock_kemasan')->where([['kemasan_id_kemasan','=',$ajk->id_kemasan],['produk_id_produk','=',$request->produk],['sub_produk_id','=',$request->subproduk]]);
			$getkem = $ghr->first();
			$geh = DB::table('stock_cincin')->where('produk_id_produk',$request->produk);
			$getcin = $geh->first();
			$hkr = DB::table('stock_stiker')->where([['produk_id_produk','=',$request->produk],['sub_produk_id','=',$request->subproduk]]);
			$getstiker = $hkr->first();

			$idkem = $ghr->count() > 0 ?$getkem->id_stock_kemasan : '';
			$ghf = $ghr->count() > 0 ? $getkem->stock_kemasan : 0;
			$idcin = $geh->count() > 0 ? $getcin->id_stock_cincin : 0;
			$hdb = $geh->count() > 0 ? $getcin->stock_qty_cincin : 0;
			if ($hkr->count() > 0) {
				$idstiker = $getstiker->id_stock_stiker;
				$hdm = $getstiker->stock_luar;
				$hdk = $getstiker->stock_dalam;
			} else {
				$idstiker = '';
				$hdm = 0;
				$hdk = 0;
			}
			$kemas_id = $ajk->id_kemasan;
			$isi = $ajk->isi;
			$kemasan = $ajk->nama_kemasan;
		} else {
			$idkem = '';
			$ghf = 0;
			$idcin = '';
			$hdb = 0;
			$idstiker = '';
			$hdm = 0;
			$hdk = 0;
			$kemas_id = '';
			$isi = 'Tidak Diketahui';
			$kemasan = 'Kosong';
		}
		return ['idcukai' => $idcukai,'jmlcukai' => $hasil,'idkemasan' => $idkem,'jmlkemasan' => $ghf,'idcincin' => $idcin,'jmlcincin' => $hdb,'idstiker' => $idstiker,'jmlluarsti' => $hdm,'jmldalamsti' => $hdk,'idqc' => $jkd,'jmlqc' => $ber,'idpack' => $pejj,'jmlpack' => $kjf,'kemas_id' => $kemas_id,'isi' => $isi,'kemasan' => $kemasan];
	}

	public function spropackeks($request)
	{
		$gh = DB::table('stock_qc')->select('id','jml_qc')->where('jenis_has_produk_id_produk',11);
		$robus = $gh->first();
		$ghe = DB::table('stock_qc')->select('id','jml_qc')->where('jenis_has_produk_id_produk',13);
		$half = $ghe->first();
		$nks = DB::table('stock_qc')->select('id','jml_qc')->where('jenis_has_produk_id_produk',14);
		$mau = $nks->first();

		$weylp = DB::table('stock_packing')->where([['sub_produk_id_sub_produk','=',$request->subproduk]]);

		$per = DB::table('sub_produk')
		->select('sub_produk.isi','kemasan.id_kemasan','kemasan.nama_kemasan')
		->join('kemasan','sub_produk.kemasan_id_kemasan','=','kemasan.id_kemasan')
		->join('produk','sub_produk.produk_id_produk','=','produk.id_produk')
		->where('id_sub_produk',$request->subproduk);


		$idbar[11] = ($gh->count() == '' ? '' : $robus->id);
		$jmlqunt[11] = ($gh->count() == '' ? 0 : $robus->jml_qc);

		$idbar[13] = ($ghe->count() == '' ? '' : $half->id);
		$jmlqunt[13] = ($ghe->count() == '' ? 0 : $half->jml_qc);

		$idbar[14] = ($nks->count() == '' ? '' : $mau->id);
		$jmlqunt[14] = ($nks->count() == '' ? 0 : $mau->jml_qc);


		if ($weylp->count() > 0) {
			$qop = $weylp->first();
			$kjf = $qop->jml_packing;
			$pejj = $qop->id;
		} else {
			$kjf = 0; $pejj = '';
		}

		if ($per->count() > 0) {
			$ajk = $per->first();

			$ghr = DB::table('stock_kemasan')->where([['kemasan_id_kemasan','=',$ajk->id_kemasan],['produk_id_produk','=',$request->produk],['sub_produk_id','=',$request->subproduk]]);
			$getkem = $ghr->first();
			$geh = DB::table('stock_cincin')->where('produk_id_produk',$request->produk);
			$getcin = $geh->first();
			$hkr = DB::table('stock_stiker')->where([['produk_id_produk','=',$request->produk],['sub_produk_id','=',$request->subproduk]]);
			$getstiker = $hkr->first();
			$idkem = $ghr->count() > 0 ?$getkem->id_stock_kemasan : '';
			$ghf = $ghr->count() > 0 ? $getkem->stock_kemasan : 0;
			$idcin = $geh->count() > 0 ? $getcin->id_stock_cincin : 0;
			$hdb = $geh->count() > 0 ? $getcin->stock_qty_cincin : 0;

			if ($hkr->count() > 0) {
				$idstiker = $getstiker->id_stock_stiker;
				$hdm = $getstiker->stock_luar;
				$hdk = $getstiker->stock_dalam;
			} else {
				$idstiker = '';
				$hdm = 0;
				$hdk = 0;
			}
			$kemas_id = $ajk->id_kemasan;
			$isi = $ajk->isi;
			$kemasan = $ajk->nama_kemasan;
		} else {
			$idkem = '';
			$ghf = 0;
			$idcin = '';
			$hdb = 0;
			$idstiker = '';
			$hdm = 0;
			$hdk = 0;
			$kemas_id = '';
			$isi = 'Tidak Diketahui';
			$kemasan = 'Kosong';
		}
		return ['idkemasan' => $idkem,'jmlkemasan' => $ghf,
		'idcincin' => $idcin,'jmlcincin' => $hdb,
		'idstiker' => $idstiker,'jmlluarsti' => $hdm,'jmldalamsti' => $hdk,
		'idqc' => $idbar,'jmlqc' => $jmlqunt,
		'idpack' => $pejj,'jmlpack' => $kjf,
		'kemas_id' => $kemas_id,'isi' => $isi,'kemasan' => $kemasan];
	}

	public function spropackbos($request)
	{
		$snje = [11,12,13,15,16,19];
		for ($i=0; $i < count($snje); $i++) { 
			$gh[$i] = DB::table('stock_qc')->select('id','jml_qc')->where('jenis_has_produk_id_produk',$snje[$i]);
			$robus[$i] = $gh[$i]->first();

			$idbar[$snje[$i]] = ($gh[$i]->count() == '' ? '' : $robus[$i]->id);
			$jmlqunt[$snje[$i]] = ($gh[$i]->count() == '' ? 0 : $robus[$i]->jml_qc);
		}

		$weylp = DB::table('stock_packing')->where([['sub_produk_id_sub_produk','=',$request->subproduk]]);

		$per = DB::table('sub_produk')
		->select('sub_produk.isi','kemasan.id_kemasan','kemasan.nama_kemasan')
		->join('kemasan','sub_produk.kemasan_id_kemasan','=','kemasan.id_kemasan')
		->join('produk','sub_produk.produk_id_produk','=','produk.id_produk')
		->where('id_sub_produk',$request->subproduk);

		if ($weylp->count() > 0) {
			$qop = $weylp->first();
			$kjf = $qop->jml_packing;
			$pejj = $qop->id;
		} else {
			$kjf = 0; $pejj = '';
		}

		if ($per->count() > 0) {
			$ajk = $per->first();

			$ghr = DB::table('stock_kemasan')->where([['kemasan_id_kemasan','=',$ajk->id_kemasan],['produk_id_produk','=',$request->produk],['sub_produk_id','=',$request->subproduk]]);
			$getkem = $ghr->first();
			$geh = DB::table('stock_cincin')->where('produk_id_produk',$request->produk);
			$getcin = $geh->first();
			$hkr = DB::table('stock_stiker')->where([['produk_id_produk','=',$request->produk],['sub_produk_id','=',$request->subproduk]]);
			$getstiker = $hkr->first();
			$idkem = $ghr->count() > 0 ?$getkem->id_stock_kemasan : '';
			$ghf = $ghr->count() > 0 ? $getkem->stock_kemasan : 0;
			$idcin = $geh->count() > 0 ? $getcin->id_stock_cincin : 0;
			$hdb = $geh->count() > 0 ? $getcin->stock_qty_cincin : 0;

			if ($hkr->count() > 0) {
				$idstiker = $getstiker->id_stock_stiker;
				$hdm = $getstiker->stock_luar;
				$hdk = $getstiker->stock_dalam;
			} else {
				$idstiker = '';
				$hdm = 0;
				$hdk = 0;
			}
			$kemas_id = $ajk->id_kemasan;
			$isi = $ajk->isi;
			$kemasan = $ajk->nama_kemasan;
		} else {
			$idkem = '';
			$ghf = 0;
			$idcin = '';
			$hdb = 0;
			$idstiker = '';
			$hdm = 0;
			$hdk = 0;
			$kemas_id = '';
			$isi = 'Tidak Diketahui';
			$kemasan = 'Kosong';
		}
		return ['idkemasan' => $idkem,'jmlkemasan' => $ghf,
		'idcincin' => $idcin,'jmlcincin' => $hdb,
		'idstiker' => $idstiker,'jmlluarsti' => $hdm,'jmldalamsti' => $hdk,
		'idqc' => $idbar,'jmlqc' => $jmlqunt,
		'idpack' => $pejj,'jmlpack' => $kjf,
		'kemas_id' => $kemas_id,'isi' => $isi,'kemasan' => $kemasan];
	}

	public function spropackhvn($request)
	{
		$snje = [11,12,13,15,16];
		for ($i=0; $i < count($snje); $i++) { 
			$gh[$i] = DB::table('stock_qc')->select('id','jml_qc')->where('jenis_has_produk_id_produk',$snje[$i]);
			$robus[$i] = $gh[$i]->first();

			$idbar[$snje[$i]] = ($gh[$i]->count() == '' ? '' : $robus[$i]->id);
			$jmlqunt[$snje[$i]] = ($gh[$i]->count() == '' ? 0 : $robus[$i]->jml_qc);
		}

		$weylp = DB::table('stock_packing')->where([['sub_produk_id_sub_produk','=',$request->subproduk]]);

		$per = DB::table('sub_produk')
		->select('sub_produk.isi','kemasan.id_kemasan','kemasan.nama_kemasan')
		->join('kemasan','sub_produk.kemasan_id_kemasan','=','kemasan.id_kemasan')
		->join('produk','sub_produk.produk_id_produk','=','produk.id_produk')
		->where('id_sub_produk',$request->subproduk);

		if ($weylp->count() > 0) {
			$qop = $weylp->first();
			$kjf = $qop->jml_packing;
			$pejj = $qop->id;
		} else {
			$kjf = 0; $pejj = '';
		}

		if ($per->count() > 0) {
			$ajk = $per->first();

			$ghr = DB::table('stock_kemasan')->where([['kemasan_id_kemasan','=',$ajk->id_kemasan],['produk_id_produk','=',$request->produk],['sub_produk_id','=',$request->subproduk]]);
			$getkem = $ghr->first();
			$geh = DB::table('stock_cincin')->where('produk_id_produk',$request->produk);
			$getcin = $geh->first();
			$hkr = DB::table('stock_stiker')->where([['produk_id_produk','=',$request->produk],['sub_produk_id','=',$request->subproduk]]);
			$getstiker = $hkr->first();
			$idkem = $ghr->count() > 0 ?$getkem->id_stock_kemasan : '';
			$ghf = $ghr->count() > 0 ? $getkem->stock_kemasan : 0;
			$idcin = $geh->count() > 0 ? $getcin->id_stock_cincin : 0;
			$hdb = $geh->count() > 0 ? $getcin->stock_qty_cincin : 0;

			if ($hkr->count() > 0) {
				$idstiker = $getstiker->id_stock_stiker;
				$hdm = $getstiker->stock_luar;
				$hdk = $getstiker->stock_dalam;
			} else {
				$idstiker = '';
				$hdm = 0;
				$hdk = 0;
			}
			$kemas_id = $ajk->id_kemasan;
			$isi = $ajk->isi;
			$kemasan = $ajk->nama_kemasan;
		} else {
			$idkem = '';
			$ghf = 0;
			$idcin = '';
			$hdb = 0;
			$idstiker = '';
			$hdm = 0;
			$hdk = 0;
			$kemas_id = '';
			$isi = 'Tidak Diketahui';
			$kemasan = 'Kosong';
		}
		return ['idkemasan' => $idkem,'jmlkemasan' => $ghf,
		'idcincin' => $idcin,'jmlcincin' => $hdb,
		'idstiker' => $idstiker,'jmlluarsti' => $hdm,'jmldalamsti' => $hdk,
		'idqc' => $idbar,'jmlqc' => $jmlqunt,
		'idpack' => $pejj,'jmlpack' => $kjf,
		'kemas_id' => $kemas_id,'isi' => $isi,'kemasan' => $kemasan];
	}

	// public function changekstockqc($request,$jkse,$jorty,$bat)
	// {
	// 	$btg['hasil_packing'] = 0;
	// 	$btg['hasil_packingcolect'] = [];
	// 	for ($i=0; $i < count($jkse); $i++) {
	// 		$dtb = $request->jmlpacking * $jorty[$i];
	// 		$adjke = $bat[$i] - $dtb;

	// 		$resultqu = Stock_qc::where('jenis_has_produk_id_produk',$jkse[$i])->update(['jml_qc' => $adjke]);

	// 		$btg['hasil_packingcolect'][$jkse[$i]] = $adjke;
	// 	}
	// 	return $btg;
	// }

	public function changetockqc($request,$id,$isi,$stockbat)
	{
		// $btg['hasil_packing'] = 0;
		// $btg['hasil_packingcolect'] = [];
		$artq['awal_packing'] = 0;
		$artq['awal_packingcolect'] = [];
		for ($i=0; $i < count($id); $i++) {
			$dtb = $request->jmlpacking * $isi[$i];
			$keprt = $stockbat[$i] - $dtb;

			$resultqu = Stock_qc::where('jenis_has_produk_id_produk',$id[$i])->update(['jml_qc' => $keprt]);

			// $btg['hasil_packingcolect'][$id[$i]] = $keprt;
			$artq['awal_packingcolect'][] = $stockbat[$i];
		}
		return $artq;
	}

	// public function changebostockqc($request,$id,$isi,$stockbat)
	// {
	// 	$btg['hasil_packing'] = 0;
	// 	$btg['hasil_packingcolect'] = [];
	// 	for ($i=0; $i < count($id); $i++) {
	// 		$dtb = $request->jmlpacking * $isi[$i];
	// 		$keprt = $stockbat[$i] - $dtb;

	// 		$resultqu = Stock_qc::where('jenis_has_produk_id_produk',$id[$i])->update(['jml_qc' => $keprt]);

	// 		$btg['hasil_packingcolect'][$id[$i]] = $keprt;
	// 	}
	// 	return $btg;
	// }

	public function stockpacking($request)
	{
		$res = new Back_bahanbaku;
		$resultcin = $res->stockcincin($request);
		$resultsti = $res->stockstiker($request);
		$resultkem = $res->stockkemasan($request);
		if ($request->produk != 54) {
			$resultcu = $res->stockcukai($request);

			$wy = $request->jmlpacking * $request->isikemasan;
			$kenk = $request->batangqc - $wy;
			// $btg['hasil_packing'] = $kenk;
			$artq['awal_packing'] = $request->batangqc;
			$resultqu = Stock_qc::find($request->idqc);
			$resultqu->jml_qc = $kenk;
			$resultqu->save();
			$idegqc = $resultqu->id;
		} else {
			switch ($request->subproduk) {
				case 47:
				$jkse = [11,13,14];
				$jorty = [10,5,5];
				$bat = [$request->batangrobus,$request->batanghalf,$request->batangmaumere];
				$idegqc = NULL;
				$artq = $this->changetockqc($request,$jkse,$jorty,$bat);
				break;

				case 48:
				$jkse = [11,12,13,15,16,19];
				$jorty = [3,4,3,4,5,6];
				$bat = [$request->batangrbs_bos,$request->batangcor_bos,$request->batanghlf_bos,$request->batangcgr_bos,$request->batangelnino_bos,$request->batangc99_bos];
				$idegqc = NULL;
				$artq = $this->changetockqc($request,$jkse,$jorty,$bat);
				break;

				case 49:
				$jkse = [11,12,13,15,16];
				$jorty = [3,3,3,3,4];
				$bat = [$request->batangrbs_hvn,$request->batangcor_hvn,$request->batanghlf_hvn,$request->batangcgr_hvn,$request->batangelnino_hvn];
				$idegqc = NULL;
				$artq = $this->changetockqc($request,$jkse,$jorty,$bat);
				break;
			}
		}

		if ($request->idpacking != '') {
			$pey = Stock_packing::where('id',$request->idpacking)->first();
			$jam = $pey->jml_packing + $request->jmlpacking;
			$resultpc = Stock_packing::find($request->idpacking);
			$resultpc->jml_packing = $jam;
			$resultpc->save();
			$idpackwp = $resultpc->id;
		} else {
			$resultpc = new Stock_packing;
			$jam = $request->jmlpacking;
			$resultpc->jml_packing = $jam;
			$resultpc->sub_produk_id_sub_produk = $request->subproduk;
			$resultpc->save();
			$idpackwp = $resultpc->id;
		}
		$hsjde = [
			'idcincin' => $resultcin['resultid'],'hasilcincin' => $resultcin['hasil'],
			'idstiker' => $resultsti['resultid'],'hasilluarstiker' => $resultsti['hasilluar'],'hasildalamstiker' => $resultsti['hasildalam'],
			'idkemasan' => $resultkem['resultid'],'hasilkemasan' => $resultkem['hasil'],
			'idqc' => $idegqc,'hasilqc' => $artq,
			'idpack' => $idpackwp,'hasilpack' => $jam
		];
		if ($request->produk != 54) {
			$hsjde += ['idcukai' => $resultcu['resultid'],'hasilcukai' => $resultcu['hasil']];
		}
		return $hsjde;
	}

	public function insertdatacukaipack($request,$idcukai,$result,$id)
	{
		$nejder = DB::table('harga_cukai')->where('sub_produk_id',$request->subproduk)->orderBy('id','desc')->take(1)->first();
		$data = [
			'masuk_cukai' => $request->masuk,
			'terpakai_cukailama' => $request->terpakailama,
			'terpakai_cukaibaru' => $request->terpakaibaru,
			'sisa_cukai' => $result,
			'tanggal_cukai' => $request->tanggal_packing,
			'ket_hiscukai' => "Cukai Terpakai (Packing): ".$request->tanggal_packing." based on ID packing: ".$id,
			'stock_cukai_id_stock_cukai' => $idcukai,
			'stock_cukai_id_sub_produk' => $request->subproduk,
			'stock_cukai_produk_id_produk' => $request->produk,
			'harga_cukai_id' => $nejder->id,
			'packing_id_packing' => $id,
			'author_session' => Auth::user()->id,
			'created_at' => date('Y-m-d H:i:s'),
			'log' => 'INSERT',
		];
		History_cukai::insert($data);
	}

	public function insertdatacincinpack($request,$idcincin,$result,$id)
	{
		$gejrt = DB::table('harga_cincin')->where('produk_id_produk',$request->produk)->orderBy('id','desc')->take(1)->first();
		$data = [
			'masuk' => $request->masuk,
			'terpakai' => $request->terpakaicincin,
			'afkir' => $request->afkir,
			'sisa' => $result,
			'tanggal_cincin' => $request->tanggal_packing,
			'ket_cin' => "Cincin Terpakai (Packing): ".$request->tanggal_packing." based on ID packing: ".$id,
			'stock_cincin_id_stock_cincin' => $idcincin,
			'stock_cincin_produk_id_produk' => $request->produk,
			'harga_cincin_id' => $gejrt->id,
			'packing_id' => $id,
			'author_session' => Auth::user()->id,
			'created_at' => date('Y-m-d H:i:s'),
			'log' => "INSERT"
		];
		History_cincin::insert($data);
	}

	public function insertdatakemasanpack($request,$idkemasan,$result,$id)
	{
		$nehbr = DB::table('harga_kemasan')->where('sub_produk_id',$request->subproduk)->orderBy('id','desc')->take(1)->first();
		$data = [
			'tanggal_hiskem' => $request->tanggal_packing,
			'masuk_kemasan' => $request->masuk,
			'terpakai_kemasan' => $request->terpakaikemasan,
			'afkir_kemasan' => $request->afkir,
			'stok_now' => $result,
			'ket_kem' => "Kemasan Terpakai (Packing): ".$request->tanggal_packing." based on ID packing: ".$id,
			'stock_kemasan_id_stock_kemasan' => $idkemasan,
			'stock_kemasan_kemasan_id_kemasan' => $request->kemasan,
			'stock_kemasan_sub_produk_id' => $request->subproduk,
			'stock_kemasan_produk_id_produk' => $request->produk,
			'harga_kemasan_id' => $nehbr->id,
			'packing_id' => $id,
			'author_session' => Auth::user()->id,
			'created_at' => date('Y-m-d H:i:s'),
			'log' => "INSERT"
		];
		History_kemasan::insert($data);
	}

	public function insertdatastikerpack($request,$idstiker,$resultlr,$resultdl,$id)
	{
		$jejel = DB::table('harga_stiker')->where('sub_produk_id',$request->subproduk)->orderBy('id','desc')->take(1)->first();
		$data = [
			'stock_stiker_id_stock_stiker' => $idstiker,
			'stock_stiker_sub_produk_id' => $request->subproduk,
			'stock_stiker_produk_id_produk' => $request->produk,
			'harga_stiker_id' => $jejel->id,
			'packing_id' => $id,
			'masuk_luar' => $request->masukluar,
			'pakai_luar' => $request->terpakailuar,
			'hasil_luar' => $resultlr,
			'masuk_dalam' => $request->masukdalam,
			'pakai_dalam' => $request->terpakaidalam,
			'hasil_dalam' => $resultdl,
			'tanggal_hisstik' => $request->tanggal_packing,
			'ket_sti' => "Stiker Terpakai (Packing): ".$request->tanggal_packing." based on ID packing: ".$id,
			'author_session' => Auth::user()->id,
			'created_at' => date('Y-m-d H:i:s'),
			'log' => "INSERT"
		];
		History_stiker::insert($data);
	}

	public function attibut_lengpack($request,$array,$idpack)
	{
		if ($request->produk != 54) {
			$this->insertdatacukaipack($request,$array['idcukai'],$array['hasilcukai'],$idpack); 
		}
		$this->insertdatacincinpack($request,$array['idcincin'],$array['hasilcincin'],$idpack);
		$this->insertdatakemasanpack($request,$array['idkemasan'],$array['hasilkemasan'],$idpack);
		$this->insertdatastikerpack($request,$array['idstiker'],$array['hasilluarstiker'],$array['hasildalamstiker'],$idpack);
	}

	public function attibut_updatelengpack($request,$array)
	{
		if ($request->produk != 54) {
			$this->bahan->ifuppcukai($request,$request->idcukai,$array['masuk'],$array['lama'],$array['baru']);
		}
		$this->bahan->ifuppkemasan($request,$request->idstokemasan,$array['masukkem'],$array['terpakaikem'],$array['afkirkem']);
		$this->bahan->ifuppstiker($request,$request->idstiker,$array['masukl'],$array['masukd'],$array['pakail'],$array['pakaid']);
		$this->bahan->ifuppcincin($request,$array['masukcin'],$array['terpakaicin'],$array['afkircin']);
	}

	public function singleifupdate($request,$id,$array)
	{
		$poe = Stock_qc::where('id',$request->idqc)->first();

		$poie = $request->jmlpacking * $request->isikemasan;
		$yet = $array['jml'] * $request->isikemasan;

		if ($array['jml'] > $request->jmlpacking) {
			$oep = $yet - $poie;
			$hasilqc = $poe->jml_qc + $oep;
		} elseif ($array['jml'] < $request->jmlpacking) {
			$oep = $poie - $yet;
			$hasilqc = $poe->jml_qc - $oep;
		} else {
			$hasilqc = $poe->jml_qc;
		}

		$ert = Stock_qc::find($request->idqc);
		$ert->jml_qc = $hasilqc;
		$ert->save();

		return ['hasil_packing' => $hasilqc];
	}

	// public function eksifupdate($request,$array)
	// {
	// 	$btg['hasil_packing'] = 0;
	// 	$btg['hasil_packingcolect'] = [];
	// 	for ($i=0; $i < count($array['idqcbaseproduk']); $i++) { 
	// 		$poe = Stock_qc::where('jenis_has_produk_id_produk',$array['idqcbaseproduk'][$i])->first();

	// 		$poie = $request->jmlpacking * $array['isieks'][$i];
	// 		$yet = $array['jml'] * $array['isieks'][$i];

	// 		if ($array['jml'] > $request->jmlpacking) {
	// 			$oep = $yet - $poie;
	// 			$hasilqc = $poe->jml_qc + $oep;
	// 		} elseif ($array['jml'] < $request->jmlpacking) {
	// 			$oep = $poie - $yet;
	// 			$hasilqc = $poe->jml_qc - $oep;
	// 		} else {
	// 			$hasilqc = $poe->jml_qc;
	// 		}
	// 		$ert = Stock_qc::where('jenis_has_produk_id_produk',$array['idqcbaseproduk'][$i])->update(['jml_qc' => $hasilqc]);

	// 		$btg['hasil_packingcolect'][] = $hasilqc;
	// 	}
	// 	return $btg;
	// }

	public function colectifupdate($request,$array)
	{
		$btg['hasil_packing'] = 0;
		$btg['hasil_packingcolect'] = [];
		for ($i=0; $i < count($array['idqcbaseproduk']); $i++) {
			$poe = Stock_qc::where('jenis_has_produk_id_produk',$array['idqcbaseproduk'][$i])->first();

			$poie = $request->jmlpacking * $array['isi'][$i];
			$yet = $array['jml'] * $array['isi'][$i];

			if ($array['jml'] > $request->jmlpacking) {
				$oep = $yet - $poie;
				$hasilqc = $poe->jml_qc + $oep;
			} elseif ($array['jml'] < $request->jmlpacking) {
				$oep = $poie - $yet;
				$hasilqc = $poe->jml_qc - $oep;
			} else {
				$hasilqc = $poe->jml_qc;
			}
			$ert = Stock_qc::where('jenis_has_produk_id_produk',$array['idqcbaseproduk'][$i])->update(['jml_qc' => $hasilqc]);

			$btg['hasil_packingcolect'][] = $hasilqc;
		}
		return $btg;
	}

	public function ifupppacking($request,$id,$array)
	{
      // $id,$jml,$masuk,$lama,$baru
		$this->attibut_updatelengpack($request,$array);
		if ($array['jml'] != $request->jmlpacking) {
			if ($request->produk != 54) {
				$btg = $this->singleifupdate($request,$id,$array);			
			} else {
				switch ($request->subproduk) {
					case 47:
					$btg = $this->colectifupdate($request,$array);
					break;
					case 48:
					$btg = $this->colectifupdate($request,$array);
					break;
					case 49:
					$btg = $this->colectifupdate($request,$array);
					break;
				}
			}

			$jwn = Stock_packing::where('id',$request->idpacking)->first();
			if ($array['jml'] > $request->jmlpacking) {
				$hr = $array['jml'] - $request->jmlpacking;
				$hasilpacking = $jwn->jml_packing - $hr;
			} elseif ($array['jml'] < $request->jmlpacking) {
				$hr = $request->jmlpacking - $array['jml'];
				$hasilpacking = $jwn->jml_packing + $hr;
			} else {
				$hasilpacking = $jwn->jml_packing;
			}
			$uwer = Stock_packing::find($request->idpacking);
			$uwer->jml_packing = $hasilpacking;
			$uwer->save();	
		}
		return $btg;		
	}

	public function attibut_updatelooppack($request,$idhiscuk,$idhiskem,$idhissti,$idhiscin)
	{
		if ($request->produk != 54) {
			$this->bahan->loop_upcukai($request,$idhiscuk);
		}
		$this->bahan->loop_upkemasan($request,$idhiskem);
		$this->bahan->loop_upstiker($request,$idhissti);
		$this->bahan->loop_upcincin($request,$idhiscin);
	}

	public function loop_uppacking($request,$id,$jml = '',$btgpack = '')
	{
		$ceh = Packing::where([['stock_packing_id','=',$request->idpacking],['stock_qc_id','=',$request->idqc],['id','>',$id]]);
		if ($ceh->count() > 0) {
			$reg = $ceh->get();
			for ($i=0; $i < $ceh->count(); $i++) {
				$show = Packing::where([['stock_packing_id','=',$request->idpacking],['stock_qc_id','=',$request->idqc],['id','<',$reg[$i]->id]])->orderBy('id','desc')->take('1');
				if ($show->count() > 0) {
					$first = $show->first();
					$hsie = $first->hasil_packing + $reg[$i]->tambah_packing;
					// $pegj = $first->hasil_batangpack - $reg[$i]->batangpack;
				} else {
					if ($jml == '') {
						$awl1 = $reg[$i]->hasil_packing - $reg[$i]->tambah_packing;
						$hsie = $awl1 + $reg[$i]->tambah_packing;

						// $qalw = $reg[$i]->hasil_batangpack + $reg[$i]->batangpack;
						// $pegj = $qalw - $reg[$i]->batangpack;
					} else {
						$awl1 = $reg[$i]->hasil_packing - $reg[$i]->tambah_packing - $jml;
						$hsie = $awl1 + $reg[$i]->tambah_packing;

						// $qalw = $reg[$i]->hasil_batangpack + $reg[$i]->batangpack + $btgpack;
						// $pegj = $qalw - $reg[$i]->batangpack;
					}
				}
				// ['hasil_batangpack' => $pegj,
				$update = Packing::where('id',$reg[$i]->id)->update(['hasil_packing' => $hsie]);
			}
		}
	}

	// public function delekschange($request,$array)
	// {
	// 	for ($i=0; $i < count($array['idqcbaseproduk']); $i++) { 
	// 		$poe = Stock_qc::where('jenis_has_produk_id_produk',$array['idqcbaseproduk'][$i])->first();

	// 		$hnjr = $array['jml'] * $array['isieks'][$i];
	// 		$btg = $poe->jml_qc + $hnjr;
	// 		Stock_qc::where('id',$poe->id)->update(['jml_qc' => $btg]);
	// 	}
	// }

	public function delchangestockqc($request,$array)
	{
		for ($i=0; $i < count($array['idqcbaseproduk']); $i++) { 
			$poe = Stock_qc::where('jenis_has_produk_id_produk',$array['idqcbaseproduk'][$i])->first();

			$hnjr = $array['jml'] * $array['isi'][$i];
			$btg = $poe->jml_qc + $hnjr;
			Stock_qc::where('id',$poe->id)->update(['jml_qc' => $btg]);
		}
	}

	public function deletestockpack($request,$array)
	{
		if ($request->produk != 54) {
			$poe = Stock_qc::where('id',$request->idqc)->first();
			$btg = $poe->jml_qc + $array['mnv'];
			Stock_qc::where('id',$request->idqc)->update(['jml_qc' => $btg]);
		} else {
			switch ($request->subproduk) {
				case 47:
				$this->delchangestockqc($request,$array);
				break;
				case 48:
				$this->delchangestockqc($request,$array);
				break;
				case 49:
				$this->delchangestockqc($request,$array);
				break;
			}
		}	
	}

	public function updatestockwhendeletepack($request,$array)
	{
		$jwn = Stock_packing::where('id',$request->idpacking)->first();
		$kenj = DB::table('stock_kemasan')->where([['id_stock_kemasan','=',$array['idstokem']]])->first();
		$dsjrk = DB::table('stock_stiker')->where([['id_stock_stiker','=',$array['idstosti']]])->first();
		$jekwr = DB::table('stock_cincin')->where([['id_stock_cincin','=',$array['idstocin']]])->first();

		// ini untuk stock qc dan packing bngst
		$enr = $jwn->jml_packing - $array['jml'];
		// end

		// ini untuk stock kemasan,stiker dan cincin bngst
		$ban = $kenj->stock_kemasan - $array['masukkem'] + ($array['terpakaikem'] + $array['afkirkem']);
		$jkes = $dsjrk->stock_luar - $array['masukl'] + $array['pakail'];
		$ser = $dsjrk->stock_dalam - $array['masukd'] + $array['pakaid'];
		$denko = $jekwr->stock_qty_cincin - $array['masukcin'] + ($array['terpakaicin'] + $array['afkircin']);
		// end

		$this->deletestockpack($request,$array);

		Stock_packing::where('id',$request->idpacking)->update(['jml_packing' => $enr]);

		// ini untuk stock cukai bngst
		if ($request->produk != 54) {
			$uyr = DB::table('stock_cukai')->where([['id_stock_cukai','=',$request->idcukai]])->first();
			$enjr = $uyr->stock_qty_cukai - $array['masuk'] + ($array['lama'] + $array['baru']);

			DB::table('stock_cukai')->where([['id_stock_cukai','=',$request->idcukai]])->update(['stock_qty_cukai' => $enjr]);
		}
		DB::table('stock_kemasan')->where('id_stock_kemasan',$kenj->id_stock_kemasan)->update(['stock_kemasan' => $ban]);
		DB::table('stock_stiker')->where('id_stock_stiker',$dsjrk->id_stock_stiker)->update(['stock_luar' => $jkes,'stock_dalam' => $ser]);
		DB::table('stock_cincin')->where('id_stock_cincin',$jekwr->id_stock_cincin)->update(['stock_qty_cincin' => $denko]);
	}

	public function attibut_deletelooppack($request,$idhiscuk,$idhiskem,$idhissti,$idhiscin,$awlcukai,$awlcincin,$awlstiluar,$awlstidalam,$awlkemasan)
	{
		if ($request->produk != 54) {
			$this->bahan->loop_upcukai($request,$idhiscuk,$awlcukai);
		}
		$this->bahan->loop_upkemasan($request,$idhiskem,$awlkemasan);
		$this->bahan->loop_upstiker($request,$idhissti,$awlstiluar,$awlstidalam);
		$this->bahan->loop_upcincin($request,$idhiscin,$awlkemasan);
	}
}
