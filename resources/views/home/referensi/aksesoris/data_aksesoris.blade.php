@extends('../../layout')

@section('title')
<title>BIN - ERP : Aksesoris</title>
<link type="text/css" rel="stylesheet" href="{{ asset('node_modules/jsgrid/jsgrid.min.css') }}" />
<link type="text/css" rel="stylesheet" href="{{ asset('node_modules/jsgrid/jsgrid-theme.min.css') }}" />
<style type="text/css">
	.hide
	{
		display:none;
	}
</style>
@endsection

@section('content')
<div class="row page-titles">
	<div class="col-md-5 align-self-center">
		<h3 class="text-themecolor">Aksesoris</h3>
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a href="javascript:void(0)">Referensi</a></li>
			<li class="breadcrumb-item active">Aksesoris</li>
		</ol>
	</div>
</div>
<div class="row">
	<div class="col-md-12 m-t-30">
		<div class="card">
			<div class="card-body">
				<h4 class="card-title">Aksesoris</h4>
				<div class="row">
					<div class="col-md-8"></div>
					<div class="col-md-2 offset-md-2">
						<button type="button" onclick="Refresh()" class="btn btn-xs btn-icons btn-secondary">Refresh</button>
					</div>
				</div>
				<div id="grid_aksesoris"></div>
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-body">
				<h4 class="card-title">Aksesoris has Sub Produk</h4>
				<div class="row">
					<div class="col-md-6"></div>
					<div class="col-md-4 offset-md-2" align="right">
						<div class="form-group">
							<button type="button" id="tambah-hassub" class="btn btn-success btn-xs btn-rounded" data-link="{{ route('has_aksesoris.post') }}"><i class="fa fa-plus"></i></button>
							<button type="button" id="refreshit" class="btn btn-secondary btn-xs btn-rounded"><i class="mdi mdi-refresh"></i></button>
						</div>
					</div>
					<div class="col-md-12" align="center" id="loadingdelete">
						<h5>Data tengah diproses. Mohon tunggu sejenak!</h5>
					</div>
					<div class="col-md-12">
						<div class="table-responsive">
							<table id="table-hassubs" class="display nowrap table table-hover table-striped table-bordered">
								<thead>
									<tr>
										<th>No</th>
										<th>Sub Kode</th>
										<th>Sub Produk</th>
										<th>Aksesoris</th>
										<th>Aksi</th>
									</tr>
								</thead>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="modal-hassubpro" tabindex="-1" role="dialog" aria-labelledby="title" aria-hidden="true" style="display: none;">
	<div class="modal-dialog modal-xl">
		<div class="modal-content">
			<div class="card">
				<div class="card-header">
					<div class="modal-header">
						<h4 class="modal-title" id="title"></h4>
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					</div>
				</div>
				<div class="card-body">
					<form id="form-hassubpro" method="POST">
						<div class="modal-body row">
							<div class="form-group col-md-12 fa-3x spinner_nekr" align="center">
								<i class="fas fa-spinner fa-spin"></i>
							</div>
							<div class="form-group col-md-12">
								<label class="control-label">Kode Relasi</label>
								<input type="text" class="form-control" id="kode_relasi" name="kode_relasi" placeholder="Kode Relas">
							</div>
							<div class="form-group col-md-6">
								<label class="control-label">Produk</label>
								<select class="form-control" name="produk" id="produk">
								</select>
							</div>
							<div class="form-group col-md-6">
								<label class="control-label">Sub Produk</label>
								<select class="form-control" name="subproduk" id="subproduk">
								</select>
							</div>
							<div class="form-group col-md-12">
								<label class="control-label">Aksesoris</label>
								<select class="form-control" name="aksesoris" id="aksesoris">
								</select>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" id="submit_hasaksesoris" class="btn btn-primary waves-effect text-left">Save</button>
							<button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Close</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('javascript')
<script type="text/javascript" src="{{ asset('node_modules/jsgrid/jsgrid.min.js') }}"></script>
<script src="{{ asset('node_modules/datatables/datatables.min.js') }}"></script>
@include('home.referensi.aksesoris.script_aksesoris')
@endsection