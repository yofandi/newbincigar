<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('images/BIN.png') }}">
  <!-- Scripts -->
  <script src="{{ asset('js/app.js') }}" defer></script>

  <!-- Fonts -->
  <link rel="dns-prefetch" href="//fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

  <!-- Styles -->
  <link href="{{ asset('css/app.css') }}" rel="stylesheet">

  <title>AdminWrap - Easy to Customize Bootstrap 4 Admin Template</title>
  <link href="{{ asset('node_modules/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
  <link href="{{ asset('css/pages/login-register-lock.css') }}" rel="stylesheet">
  <link href="{{ asset('css/style.css') }}" rel="stylesheet">
  <link href="{{ asset('css/colors/default.css') }}" id="theme" rel="stylesheet">
</head>

<body>
  <div class="preloader">
    <div class="loader">
      <div class="loader__figure"></div>
      <p class="loader__label">BIN - ERP App</p>
    </div>
  </div>
  <section id="wrapper" class="login-register login-sidebar" style="background-image:url({{ asset('images/BIN-2017New-Company-ProfileIND230217rev1-15.jpg') }});">
    <div class="login-box card">
      <div class="card-body">
        <form class="form-horizontal form-material" method="post" id="loginform" action="{{ route('login') }}">
          @csrf

          <a href="javascript:void(0)" class="text-center db"><img src="{{ asset('images/BIN.png') }}" alt="Home" width="140" height="auto" /><br/>{{-- <img src="{{ asset('images/post.png') }}" alt="Home" /> --}}</a>
          <div class="form-group m-t-40">
            <div class="col-xs-12">
              <input class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" type="text" name="email" placeholder="Email" required autofocus>

              @if ($errors->has('email'))
              <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('email') }}</strong>
              </span>
              @endif
            </div>
          </div>
          <div class="form-group">
            <div class="col-xs-12">
              <input class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" type="password" name="password" required="" placeholder="Password">

              @if ($errors->has('password'))
              <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('password') }}</strong>
              </span>
              @endif
            </div>
          </div>
          <div class="form-group row">
            <div class="col-md-12">
              <div class="checkbox checkbox-primary pull-left p-t-0">
                <input id="checkbox-signup remember" type="checkbox" class="filled-in chk-col-light-blue"  name="remember" {{ old('remember') ? 'checked' : '' }}>

                <label class="form-check-label" for="checkbox-signup remember">{{ __('Remember Me') }}</label>
              </div>
              <a href="javascript:void(0)" id="to-recover" class="text-dark pull-right"><i class="fa fa-lock m-r-5"></i> Forgot pwd?</a> </div>
            </div>
            <div class="form-group text-center m-t-20">
              <div class="col-xs-12">
                <button class="btn btn-info btn-lg btn-block text-uppercase btn-rounded" type="submit">Log In</button>
              </div>
            </div>
            <div class="row">
              <div class="col-xs-12 col-sm-12 col-md-12 m-t-10 text-center">
                <div class="social"><a href="javascript:void(0)" class="btn  btn-facebook" data-toggle="tooltip" title="Login with Facebook"> <i aria-hidden="true" class="fab fa-facebook"></i> </a> <a href="javascript:void(0)" class="btn btn-googleplus" data-toggle="tooltip" title="Login with Google"> <i aria-hidden="true" class="fab fa-google-plus-g"></i> </a> </div>
              </div>
            </div>
            <div class="form-group m-b-0">
              <div class="col-sm-12 text-center">
                Don't have an account? <a href="pages-register2.html" class="text-primary m-l-5"><b>Sign Up</b></a>
              </div>
            </div>
          </form>
          <form class="form-horizontal" id="recoverform" action="https://wrappixel.com/demos/admin-templates/admin-wrap/main/index.html">
            <div class="form-group ">
              <div class="col-xs-12">
                <h3>Recover Password</h3>
                <p class="text-muted">Enter your Email and instructions will be sent to you! </p>
              </div>
            </div>
            <div class="form-group ">
              <div class="col-xs-12">
                <input class="form-control" type="text" required="" placeholder="Email">
              </div>
            </div>
            <div class="form-group text-center m-t-20">
              <div class="col-xs-12">
                <button class="btn btn-primary btn-lg btn-block text-uppercase waves-effect waves-light" type="reset">Reset</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </section>
    <script src="{{ asset('node_modules/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('node_modules/bootstrap/js/popper.min.js') }}"></script>
    <script src="{{ asset('node_modules/bootstrap/js/bootstrap.min.js') }}"></script>
    <script type="text/javascript">
      $(function() {
        $(".preloader").fadeOut();
      });
      $(function() {
        $('[data-toggle="tooltip"]').tooltip()
      });
      $('#to-recover').on("click", function() {
        $("#loginform").slideUp();
        $("#recoverform").fadeIn();
      });
    </script>

  </body>
  </html>