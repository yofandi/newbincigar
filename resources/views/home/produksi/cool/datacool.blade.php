@extends('../../layout')

@section('title')
<title>BIN - ERP : Data Cool Storage</title>
{{-- <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/dmuy/MDTimePicker/mdtimepicker.css"> --}}
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css" />
@endsection

@section('content')
<div class="row page-titles">
	<div class="col-md-5 align-self-center">
		<h3 class="text-themecolor">Cool Storage</h3>
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a href="javascript:void(0)">Proses</a></li>
			<li class="breadcrumb-item active">Data Cool Storage</li>
		</ol>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="row">
			<div class="col-md-6">
				<legend>Data Cool Storage</legend>
			</div>
			<div class="col-md-2 offset-md-4">
				<button type="button" data-link="{{ route('cool.post') }}" class="btn btn-success btn-rounded btn-icons " id="tambah-cool">Tambah</button>
				<button type="button" onclick="Refresh_cool()" class="btn btn-rounded btn-icons btn-secondary">Refresh</button>
			</div>
			<div class="col-md-12" align="center" id="loadingdeletecool">
				<h5>Data tengah diproses. Mohon tunggu sejenak!</h5>
			</div>
			<div class="table-responsive">
				<table id="table-cool" class="display nowrap table table-hover table-striped table-bordered">
					<thead>
						<tr>
							<th>No.</th>
							<th>Tanggal</th>
							<th>Produk</th>
							<th>Jenis</th>
							<th>Durasi</th>
							<th>Batang Fumigasi</th>
							<th>Tambah Batang</th>
							<th>JML</th>
							<th>Ket</th>
							<th>Aksi</th>
						</tr>
					</thead>
				</table>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="modal-cool" tabindex="-1" role="dialog" aria-labelledby="title-tembakau" aria-hidden="true" style="display: none;">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="card">
				<div class="card-header">
					<div class="modal-header">
						<h4 class="modal-title" id="title"></h4>
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					</div>
				</div>
				<div class="card-body">
					<form id="form-cool" method="POST">
						<div class="modal-body row">
							<div class="form-group col-md-12 fa-3x spinner_nekrcool" align="center">
								<i class="fas fa-spinner fa-spin"></i>
							</div>
							<div class="form-group col-md-12">
								<label class="control-label">Tanggal</label>
								<input type="text" class="form-control date_sam" id="tanggal_cool">
							</div>
							<div class="form-group col-md-6">
								<label class="control-label">Jenis</label>
								<select class="form-control" id="jenis"></select>
								<input type="hidden" id="jenis_in">
							</div>
							<div class="form-group col-md-6">
								<label class="control-label">Produk</label>
								<select class="form-control" id="produk"></select>
								<input type="hidden" id="produk_in">
							</div>
							<div class="form-group col-md-12" id="stock_showit">
								<button type="button" onclick="seacrh_data()" class="btn btn-primary btn-xs btn-block">Search</button>
								<input type="hidden" id="idcool">
								<input type="hidden" id="idfumigasi">
								<input type="hidden" id="idjnpro">
							</div>
							<div class="form-group col-md-12">
								<label class="control-label">Batang Fumigasi</label>
								<input type="number" class="form-control" id="batangfumi" placeholder="Jml Batang Fumigasi" readonly>
							</div>
							<div class="form-group col-md-12">
								<label class="control-label">Batang Cerutu Yang Dihasilkan</label>
								<input type="number" class="form-control" id="batangjml" placeholder="Jml Batang">
							</div>
							<div class="form-group col-md-12" style="display: none">
								<label class="control-label">Lama Proses</label>
								<input type="text" class="form-control" id="durasi" placeholder="Durasi">
							</div>
							<div class="form-group col-md-12">
								<label class="control-label">Keterangan</label>
								<textarea class="form-control" id="ket" placeholder="Keterangan"></textarea>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" onclick="save_method()" class="btn btn-primary waves-effect text-left">Save</button>
							<button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Close</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('javascript')
<script src="{{ asset('node_modules/moment/moment.js') }}">
</script><script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
<script src="{{ asset('node_modules/datatables/datatables.min.js') }}"></script>
{{-- <script src="https://cdn.jsdelivr.net/gh/dmuy/MDTimePicker/mdtimepicker.js --}}
"></script>
<script>
	$('#stock_showit').hide();
	$('#loadingdeletecool').hide();
	$('.spinner_nekrcool').hide();
	let token = $("meta[name='csrf-token']").attr("content");

	const jenis = {!! json_encode($jenis) !!};
	jenis.unshift({id_jenis: '',jenis: ''});

	var table = $('#table-cool').DataTable({
		"lengthMenu": [
		[3,5,10, 25, 50, 100, 150, 200, -1],
		[3,5,10, 25, 50, 100, 150, 200, "All"]
		],
		"order": [
		[0, 'desc']
		],
		processing: true,
		serverSide: true,
		ajax: '/datacool/json',
		columns: [
		{ data: 'DT_RowIndex', name: 'DT_RowIndex' },
		{ data: 'tanggal_cool', name: 'tanggal_cool' },
		{ data: 'produk', name: 'produk' },
		{ data: 'jenis', name: 'jenis' },
		{ data: 'lama_cool', name: 'lama_cool' },
		{ data: 'awlbatang', name: 'awlbatang' },
		{ data: 'tambah_cool', name: 'tambah_cool' },
		{ data: 'hasil_akhircool', name: 'hasil_akhircool' },
		{ data: 'keterangan_cool', name: 'keterangan_cool' },
		{
			data: null,
			"bSortable": false,
			mRender: function (o) { 
				return '<button type="button" class="btn btn-info btn-rounded btn-xs" id="update-fumigasi" data-title="Update Cool | ID: '+ o.id +'" data-link="/datacool/upp/'+ o.id +'/'+ o.tambah_cool +'" data-tanggal_cool="'+ o.tanggal_cool +'" data-jenis="'+ o.id_jenis +'" data-produk="'+ o.id_produk +'" data-durasi="'+ o.lama_cool +'" data-ket="'+ o.keterangan_cool +'" data-stockbatang="'+ o.awlbatang +'" data-batang="'+ o.tambah_cool +'" data-idfumigasi="'+ o.stock_fumigasi_id +'" data-idcool="'+ o.stock_cool_id +'"><i class="mdi mdi-dots-horizontal"></i></button><button type="button" class="btn btn-danger btn-rounded btn-xs" id="delete-fumigasi" data-link="/datacool/del/'+ o.id +'/'+ o.tambah_cool +'" data-idfumigasi="'+ o.stock_fumigasi_id +'" data-idcool="'+ o.stock_cool_id +'"><i class="fa fa-trash"></i></button>'; }
			},
			],
			"displayLength": 10,
		});
	$('#table-cool tbody').on('click', 'tr.group', function() {
		var currentOrder = table.order()[0];
		if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
			table.order([2, 'desc']).draw();
		} else {
			table.order([2, 'asc']).draw();
		}
	});
	$('#table-cool tbody').on('click', 'tr.group', function() {
		var currentOrder = table.order()[0];
		if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
			table.order([2, 'desc']).draw();
		} else {
			table.order([2, 'asc']).draw();
		}
	});

	function Refresh_cool() {
		$('#table-cool').DataTable().ajax.reload();
	}

	function getdatasearch(position1) {
		let jenis = $('#jenis_in').val();
		let produk = $('#produk_in').val();

		$.ajax({
			url: '/datacool/getsearchfill',
			type: 'POST',
			dataType: 'json',
			data: {
				"_token": token,
				produk: produk,
				jenis: jenis
			},
		})
		.done(function(data) {
			$('#idcool').val(data.idcool);
			$('#idfumigasi').val(data.idfumigasi);
			$('#' + position1).val(data.jml_fum);
		});
		
	}

	function ljenis(position,like) {
		let opt = '';
		for (let i = 0; i < jenis.length; i++) {
			opt += '<option value="'+ jenis[i].id_jenis +'"';
			if (jenis[i].id_jenis == like) {
				opt += 'selected';
			}
			opt += '>'+ jenis[i].jenis +'</option>';
		}
		$('#' + position).html(opt);
	}

	function lproduk(position,based,like) {
		$.ajax({
			url: '/datafilling/getproduk',
			type: 'POST',
			dataType: 'json',
			data: {
				"_token": token,
				jenis: based
			},
		})
		.done(function(data) {
			let opt = '<option value=""></option>';
			for(let i = 0; i < data.length; i++) {
				opt += '<option value="'+ data[i].id_produk +'"';
				if (data[i].id_produk == like) {
					opt += 'selected';
				}
				opt += ' data-id="'+ data[i].relid +'">'+ data[i].produk +'</option>';
			}
			$('#' + position).html(opt);
		});
	}

	function seacrh_data() {
		getdatasearch('batangfumi');
	}

	function save_method() {
		let link = $('#form-cool').attr('action');
		let idcool = $('#idcool').val();
		let idfumigasi = $('#idfumigasi').val();
		let idjnpro = $('#idjnpro').val();
		let tanggal_cool = $('#tanggal_cool').val();
		let jenis_in = $('#jenis_in').val();
		let produk_in = $('#produk_in').val();
		let stockbatang = $('#batangfumi').val();
		let batangjml = $('#batangjml').val();
		let durasi = $('#durasi').val();
		let ket = $('#ket').val();
		$.ajax({
			url: link,
			type: 'POST',
			dataType: 'json',
			data: {
				"_token": token,
				tanggal_cool: tanggal_cool,
				jenis: jenis_in,
				produk: produk_in,
				durasi:durasi,
				stockbatang: stockbatang,
				batangjml: batangjml,
				ket:ket,
				idfumigasi: idfumigasi,
				idcool: idcool,
				idjnpro:idjnpro
			},
			beforeSend: function() {
				isProcessing = true;
				$('.spinner_nekrcool').fadeIn();
			},
			success: function(data) {
				$('.spinner_nekrcool').hide();
				isProcessing = false;
				alert(data);
				Refresh_cool();
				$('#modal-cool').modal('toggle');
			}
		});
		
	}

	$(document).on('click', '#tambah-cool', function(event) {
		let clik = $(this);
		$('.modal-header #title').text('Tambah Cool');
		$('#form-cool').attr('action', clik.data('link'));
		$('#form-cool').find('input').val('');
		$('#form-cool').find('select').val('');
		$('#form-cool').find('textarea').val('');
		$("#tanggal_cool").datetimepicker({
			format: 'YYYY-MM-DD HH:mm:ss',
		}).data("DateTimePicker").date(new Date());
		// $('#durasi').mdtimepicker({format: 'hh:mm'});
		$('#stock_showit').fadeIn();
		$('#jenis').removeAttr('disabled');
		$('#produk').removeAttr('disabled');
		ljenis('jenis');
		$(document).on('change', '#jenis', function(event) {
			let jam = $(this).val();
			$('#jenis_in').val(jam);
			lproduk('produk',jam);
			$(document).on('change', '#produk', function(event) {
				let pwe = $(this).val();
				$('#produk_in').val(pwe);
				let kk = $(this).find(':selected').data('id');
				$('#idjnpro').val(kk);
			});
		});
		$('#modal-cool').modal('show');
	});

	$(document).on('click', '#update-fumigasi', function(event) {
		let click = $(this);
		$('.modal-header #title').text(click.data('title'));
		$('#form-cool').attr('action', click.data('link'));
		$('#form-cool').find('input').val('');
		$('#form-cool').find('select').val('');
		$('#form-cool').find('textarea').val('');
		$('#stock_showit').hide();
		$('#jenis').attr('disabled','disabled');
		$('#produk').attr('disabled','disabled');
		ljenis('jenis',click.data('jenis'));
		lproduk('produk',click.data('jenis'),click.data('produk'));

		$("#tanggal_cool").datetimepicker({
			format: 'YYYY-MM-DD HH:mm:ss',
		}).data("DateTimePicker").date(click.data('tanggal_cool'));
		// $('#durasi').mdtimepicker({format: 'hh:mm'});
		$('#jenis_in').val(click.data('jenis'));
		$('#produk_in').val(click.data('produk'));
		$('#batangfumi').val(click.data('stockbatang'));
		$('#batangjml').val(click.data('batang'));
		$('#idcool').val(click.data('idcool'));
		$('#idfumigasi').val(click.data('idfumigasi'));
		$('#durasi').val(click.data('durasi'));
		$('#ket').val(click.data('ket'));
		$('#modal-cool').modal('show');
	});

	$(document).on('click', '#delete-fumigasi', function(event) {
		let gsj = confirm('Apakah anda yakin ingin menghapus data ini ?');
		if (gsj) {
			let link = $(this).data("link");
			let idfumigasi = $(this).data('idfumigasi');
			let idcool = $(this).data('idcool');
			$.ajax(
			{
				url: link,
				type: 'DELETE',
				data: {
					"_token": token,
					idfumigasi: idfumigasi,
					idcool: idcool,
				},
				beforeSend: function() {
					isProcessing = true;
					$('#loadingdeletecool').fadeIn();
				},
				success: function (data){
					$('#loadingdeletecool').hide();``
					isProcessing = false;
					alert(data);
					Refresh_cool();
				}
			});
		}
	});
</script>
@endsection