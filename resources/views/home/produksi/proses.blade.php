@extends('../../layout')

@section('title')
<title>BIN - ERP : Proses Cerutu</title>
{{-- <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/dmuy/MDTimePicker/mdtimepicker.css"> --}}
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css" />
@endsection

@section('content')
<div class="row page-titles">
	<div class="col-md-5 align-self-center">
		<h3 class="text-themecolor">Proses Cerutu</h3>
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a href="javascript:void(0)">Proses</a></li>
			<li class="breadcrumb-item active" >Proses Cerutu</li>
		</ol>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="card">
			<ul class="nav nav-tabs customtab" role="tablist">
				<li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#filling" role="tab" aria-selected="true"><span class="hidden-sm-up"><i class="ti-home"></i></span> <span class="hidden-xs-down">Filling</span></a> </li>

				<li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#binding" role="tab" aria-selected="false"><span class="hidden-sm-up"><i class="ti-user"></i></span> <span class="hidden-xs-down">Binding</span></a> </li>

				<li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#pressing" role="tab" aria-selected="false"><span class="hidden-sm-up"><i class="ti-email"></i></span> <span class="hidden-xs-down">Pressing</span></a> </li>

				<li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#wrapping" role="tab" aria-selected="false"><span class="hidden-sm-up"><i class="ti-email"></i></span> <span class="hidden-xs-down">Wrapping</span></a> </li>

				<li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#drying1" role="tab" aria-selected="false"><span class="hidden-sm-up"><i class="ti-email"></i></span> <span class="hidden-xs-down">Drying 1</span></a> </li>

				<li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#frezzer" role="tab" aria-selected="false"><span class="hidden-sm-up"><i class="ti-email"></i></span> <span class="hidden-xs-down">Frezzer</span></a> </li>

				<li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#drying2" role="tab" aria-selected="false"><span class="hidden-sm-up"><i class="ti-email"></i></span> <span class="hidden-xs-down">Drying 2</span></a> </li>

				<li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#fumigasi" role="tab" aria-selected="false"><span class="hidden-sm-up"><i class="ti-email"></i></span> <span class="hidden-xs-down">Fumigasi</span></a> </li>
			</ul>
			<div class="tab-content">
				<div class="tab-pane active" id="filling" role="tabpanel">
					<div class="p-20">
						@include('home.produksi.filling.table_filling')
					</div>
				</div>
				<div class="tab-pane p-20" id="binding" role="tabpanel">
					@include('home.produksi.binding.table_binding')
				</div>
				<div class="tab-pane p-20" id="pressing" role="tabpanel">
					@include('home.produksi.pressing.table_pressing')
				</div>
				<div class="tab-pane p-20" id="wrapping" role="tabpanel">
					@include('home.produksi.wrapping.table_wrapping')
				</div>
				<div class="tab-pane p-20" id="drying1" role="tabpanel">
					@include('home.produksi.drying1.table_drying1')
				</div>
				<div class="tab-pane p-20" id="frezzer" role="tabpanel">
					@include('home.produksi.freezer.table_frezzer')
				</div>
				<div class="tab-pane p-20" id="drying2" role="tabpanel">
					@include('home.produksi.drying2.table_drying2')
				</div>
				<div class="tab-pane p-20" id="fumigasi" role="tabpanel">
					@include('home.produksi.fumigasi.table_fumigasi')
				</div>
			</div>
		</div>
	</div>
</div>
{{-- filling modal --}}
@include('home.produksi.filling.modal_filling')
{{-- Binding modal --}}
@include('home.produksi.binding.modal_binding')
{{-- Pressing modal --}}
@include('home.produksi.pressing.modal_pressing')
{{-- Wrapping modal --}}
@include('home.produksi.wrapping.modal_wrapping')
{{-- Drying modal --}}
@include('home.produksi.drying1.modal_drying1')
{{-- Frezzer modal --}}
@include('home.produksi.freezer.modal_frezzer')
{{-- Drying2 modal --}}
@include('home.produksi.drying2.modal_drying2')
{{-- Fumigasi modal --}}
@include('home.produksi.fumigasi.modal_fumigasi')
@endsection

@section('javascript')
<script src="{{ asset('node_modules/moment/moment.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
<script src="{{ asset('node_modules/datatables/datatables.min.js') }}"></script>
{{-- <script src="https://cdn.jsdelivr.net/gh/dmuy/MDTimePicker/mdtimepicker.js --}}
"></script>
<script>
	let token = $("meta[name='csrf-token']").attr("content");

	const jenis = {!! json_encode($jenis) !!};
	jenis.unshift({id_jenis: '',jenis: ''});

	function ljenis(position,like) {
		let opt = '';
		for (let i = 0; i < jenis.length; i++) {
			opt += '<option value="'+ jenis[i].id_jenis +'"';
			if (jenis[i].id_jenis == like) {
				opt += 'selected';
			}
			opt += '>'+ jenis[i].jenis +'</option>';
		}
		$('#' + position).html(opt);
	}

	function lproduk(position,based,like) {
		$.ajax({
			url: '/datafilling/getproduk',
			type: 'POST',
			dataType: 'json',
			data: {
				"_token": token,
				jenis: based
			},
		})
		.done(function(data) {
			let opt = '<option value=""></option>';
			for(let i = 0; i < data.length; i++) {
				opt += '<option value="'+ data[i].id_produk +'"';
				if (data[i].id_produk == like) {
					opt += 'selected';
				}
				opt += ' data-id="'+ data[i].relid +'">'+ data[i].produk +'</option>';
			}
			$('#' + position).html(opt);
		});
	}
</script>
{{-- filling script --}}
@include('home.produksi.filling.script_flling')
{{-- Binding script--}}
@include('home.produksi.binding.script_binding')
{{-- Pressing script--}}
@include('home.produksi.pressing.script_pressing')
{{-- Wrapping script--}}
@include('home.produksi.wrapping.script_wrapping')
{{-- Drying1 script--}}
@include('home.produksi.drying1.script_drying1')
{{-- Frezzer script--}}
@include('home.produksi.freezer.script_frezzer')
{{-- Drying2 script--}}
@include('home.produksi.drying2.script_drying2')
{{-- Fumigasi script --}}
@include('home.produksi.fumigasi.script_fumigasi')
@endsection