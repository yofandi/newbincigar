<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;
use DataTables;
use App\User;
use App\Stock_probaku;
use App\History_bahanmasuk;
use App\History_cincin;
use App\History_stiker;
use App\History_cukai;
use App\History_kemasan;

use App\Http\Controllers\Back_bahanbaku;

class Bahan_baku extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
        $this->back_bahanbaku = new Back_bahanbaku;
    }

    public function data_tembakau()
    {
        $data['berat'] = [1 => 'Kilogram',2 => 'Gram'];
        $data['jenis'] = DB::table('jenis')->get();
        $data['kategori'] = DB::table('kategori')->get();
        return view('home.bahan_baku.data_tembakau.data_tembakau',$data);
    }

    public function getpricetembakau(Request $request)
    {
        $getsat = DB::table('satuan_harga')->where([['jenis_id_jenis','=',$request->jenis],['kategori_id_kategori','=',$request->kategori]]);
        $hjsel = Stock_probaku::where([['jenis_id_jenis','=',$request->jenis],['kategori_id_kategori','=',$request->kategori]]);

        if ($request->val != 0) {
            $getsat->where('id',$request->id);
        }
        $jwy = $getsat->orderBy('id','desc')->take(1);
        if ($jwy->count() > 0) {
            $tey = $jwy->first();
            $jek = ['satuan_berat' => $tey->satuan_berat, 'harga_tembakau' => $tey->harga_tembakau];
        } else {
            $jek = ['satuan_berat' => '', 'harga_tembakau' => 0];
        }
        if ($hjsel->count() > 0) {
            $koan = $hjsel->first();
            $jek += ['stock_awal' => $koan->jml_bahanbaku];
        } else {
            $jek += ['stock_awal' => 0];
        }
        return json_encode($jek); 
    }

    public function getdatacigar()
    {
        $query = DB::table('history_bahanmasuk')
        ->select("history_bahanmasuk.*",'kategori.kategori','jenis.kode_jenis','jenis.jenis')
        ->leftJoin('kategori','history_bahanmasuk.kategori_id_kategori','=','kategori.id_kategori')
        ->leftJoin('jenis','history_bahanmasuk.jenis_id_jenis','=','jenis.id_jenis')
        ->orderBy('history_bahanmasuk.tanggal_in','asc')
        ->get();
        return Datatables::of($query)
        ->addIndexColumn()
        ->addColumn('awlstok', function ($query) {
            $awl = $query->hari_ini + $query->diproduksi - $query->diterima;
            return $awl;
        })
        ->make(true);
    }

    public function addtembakau(Request $request)
    {
        DB::beginTransaction();
        $getsat = DB::table('satuan_harga')->where([['jenis_id_jenis','=',$request->jenis],['kategori_id_kategori','=',$request->kategori]])->orderBy('id','desc')->take(1);
        $bas = $getsat->first();
        if ($getsat->count() > 0) {
            $k = $this->back_bahanbaku->stockpert($request);
            if ($request->awlstok != 0) {
                $hele = $request->awlstok + $request->st_terima - $request->st_produksi;
            } else {
                $hele = $request->st_terima - $request->st_produksi;
            }

            $data = [
                'tanggal_in' => $request->tanggal_in,
                'asal' => $request->asal,
                'stock_masuk' => $request->st_masuk,
                'diterima' => $request->st_terima,
                'diproduksi' => $request->st_produksi,
                'hari_ini' => $hele,
                'ket_his' => $request->keterangan,
                'jenis_id_jenis' => $request->jenis,
                'kategori_id_kategori' => $request->kategori,
                'stock_probaku_id' => $k['queryid'],
                'satuan_harga_id' => $bas->id,
                'author_session' => Auth::User()->id,
                'created_at' => date('Y-m-d H:i:s'),
                'log' => "INSERT"
            ];

            $query1 = History_bahanmasuk::insert($data);
            if ($query1) {
                $alert = 'Stock Temabakau berhasil ditambahkan!';
            } else {
                $alert = 'Stock Temabakau gagal ditambahkan!';
            }
        } else {
            $alert = 'Maaf anda tidak bisa menambahkan data tembakau karena nilai harga tembakau direferensi kosong.. Mohon dicek kembali!!';
        }
        echo json_encode($alert);
        DB::commit();
    }

    public function upptembakau(Request $request,$id,$terima,$produksi)
    {
        DB::beginTransaction();

        // $his  = History_bahanmasuk::where([['jenis_id_jenis','=',$request->jenis],['kategori_id_kategori','=',$request->kategori],['id','<',$id]])->orderBy('id','desc')->take('1');
        // if ($his->count() > 0) {
        //     $r = $his->first();
        // } else {
        //     $hari_ini = $request->st_terima - $request->st_produksi;
        // }
        $hari_ini = $request->awlstok + $request->st_terima - $request->st_produksi;

        $this->back_bahanbaku->ifupptembakau($request,$terima,$produksi);

        $data = [
            'tanggal_in' => $request->tanggal_in,
            'asal' => $request->asal,
            'stock_masuk' => $request->st_masuk,
            'diterima' => $request->st_terima,
            'diproduksi' => $request->st_produksi,
            'hari_ini' => $hari_ini,
            'ket_his' => $request->keterangan,
            'author_session' => Auth::User()->id,
            'created_at' => date('Y-m-d H:i:s'),
            'log' => "UPDATE"
        ];

        $query1 = History_bahanmasuk::where('id',$id)->update($data);
        if ($query1) {
            $this->back_bahanbaku->loop_updcigar($request,$id);
            $alert = 'Stock Temabakau  ID: '.$id.' berhasil diupdate!';
        } else {
            $alert = 'Stock Temabakau  ID: '.$id.' gagal diupdate!';
        }
        echo json_encode($alert);
        DB::commit();   
    }

    public function deltembakau(Request $request,$id,$terima,$produksi,$value)
    {
        DB::beginTransaction();
        $query = Stock_probaku::where([['jenis_id_jenis','=',$request->jenis],['kategori_id_kategori','=',$request->kategori]])->first();

        $ban = $query->jml_bahanbaku - $terima;
        $pro = $query->jml_produksi - $produksi;

        $this->back_bahanbaku->stockspec($request,['jml_bahanbaku' => $ban,'jml_produksi' => $pro]);

        $query1 = History_bahanmasuk::where('id',$id)->delete();
        if ($query1) {
            $this->back_bahanbaku->loop_updcigar($request,$id,$value);
            $alert = 'Stock Temabakau  ID: '.$id.' berhasil diupdate!';
        } else {
            $alert = 'Stock Temabakau  ID: '.$id.' gagal diupdate!';
        }
        echo json_encode($alert);
        DB::commit();      
    }

    public function data_cincin()
    {
        $data['produk'] = DB::table('produk')->get();
        return view('home.bahan_baku.data_cincin.data_cincin',$data);
    }

    public function getdatacincin()
    {
        $query = DB::table('history_cincin')
        ->select("history_cincin.*","produk.produk")
        ->join('produk','history_cincin.stock_cincin_produk_id_produk','=','produk.id_produk')
        ->get();
        return Datatables::of($query)
        ->addIndexColumn()
        ->addColumn('awlstok', function ($query) {
            $awl = $query->sisa - $query->masuk + ($query->terpakai + $query->afkir);
            return $awl;
        })
        ->make(true);
    }

    public function getpricecincin(Request $request)
    {
        $getsat = DB::table('harga_cincin')->where('produk_id_produk',$request->produk);
        if ($request->val != 0) {
            $getsat->where('id',$request->id);
        }
        $jwy = $getsat->orderBy('id','desc')->take(1);
        if ($jwy->count() > 0) {
            $tey = $jwy->first();
            $jek = ['harga' => $tey->harga];
        } else {
            $jek = ['harga' => 0];
        }
        return json_encode($jek); 
    }

    public function showstcin(Request $request)
    {
        $cek = DB::table('stock_cincin')->where([['produk_id_produk','=',$request->id_produk]]);
        $get = $cek->first();
        if ($cek->count() > 0) {
            $variable = $get->stock_qty_cincin;
        } else {
            $variable = 0;
        }
        echo json_encode(['count' => $cek->count(),'value' => $variable]);
    }

    public function addcincin(Request $request)
    {
        DB::beginTransaction();
        $gejrt = DB::table('harga_cincin')->where('produk_id_produk',$request->produk)->orderBy('id','desc')->take(1);
        if ($gejrt->count() > 0) {
            $rtye = $gejrt->first();
            $getsome = $this->back_bahanbaku->stockcincin($request);

            $data = [
                'masuk' => $request->masuk,
                'terpakai' => $request->terpakaicincin,
                'afkir' => $request->afkir,
                'sisa' => $getsome['hasil'],
                'tanggal_cincin' => $request->tanggal_cincin,
                'ket_cin' => $request->ket_cin,
                'stock_cincin_id_stock_cincin' => $getsome['resultid'],
                'stock_cincin_produk_id_produk' => $request->produk,
                'harga_cincin_id' => $rtye->id,
                'author_session' => Auth::user()->id,
                'created_at' => date('Y-m-d H:i:s'),
                'log' => "INSERT"
            ];
            $query = History_cincin::insert($data);
            if ($query) {
                $alert = 'Stock Cincin berhasil ditambahkan!';
            } else {
                $alert = 'Stock Cincin gagal ditambahkan!';
            }
        } else {
            $alert = 'Maaf anda tidak bisa menambahkan data cincin karena nilai harga cincin direferensi kosong.. Mohon dicek kembali!';
        }
        echo json_encode($alert);
        DB::commit();   
    }

    public function uppcincin(Request $request,$id,$masuk,$terpakai,$afkir)
    {
        DB::beginTransaction();
        $this->back_bahanbaku->ifuppcincin($request,$masuk,$terpakai,$afkir);
        $hasil = $request->stock + $request->masuk - ($request->terpakaicincin + $request->afkir);

        $data = [
            'masuk' => $request->masuk,
            'terpakai' => $request->terpakaicincin,
            'afkir' => $request->afkir,
            'sisa' => $hasil,
            'tanggal_cincin' => $request->tanggal_cincin,
            'ket_cin' => $request->ket_cin,
            'author_session' => Auth::user()->id,
            'log' => "UPDATE"
        ];

        $query1 = History_cincin::where('id',$id)->update($data);
        if ($query1) {
            $this->back_bahanbaku->loop_upcincin($request,$id);
            $alert = 'Stock Cincin  ID: '.$id.' berhasil diupdate!';
        } else {
            $alert = 'Stock Cincin  ID: '.$id.' gagal diupdate!';
        }
        echo json_encode($alert);
        DB::commit();   
    }

    public function delcincin(Request $request,$id,$masuk,$terpakai,$afkir,$stockad)
    {
        DB::beginTransaction();
        $query = DB::table('stock_cincin')->where([['produk_id_produk','=',$request->produk]])->first();

        $ban = $query->stock_qty_cincin - $masuk + ($terpakai + $afkir);

        $data = ['stock_qty_cincin' => $ban];
        $query = DB::table('stock_cincin')->where('id_stock_cincin',$query->id_stock_cincin)->update($data);

        $query1 = History_cincin::where('id',$id)->delete();
        if ($query1) {
            $this->back_bahanbaku->loop_upcincin($request,$id,$stockad);
            $alert = 'Stock Temabakau  ID: '.$id.' berhasil diupdate!';
        } else {
            $alert = 'Stock Temabakau  ID: '.$id.' gagal diupdate!';
        }
        echo json_encode($alert);
        DB::commit();         
    }

    public function data_stiker()
    {
        $data['produk'] = DB::table('produk')->get();
        return view('home.bahan_baku.data_stiker.data_stiker',$data);
    }

    public function getdatastiker()
    {
        $query = DB::table('history_stiker')
        ->select("history_stiker.*","produk.produk","sub_produk.id_sub_produk","sub_produk.sub_kode","sub_produk.sub_produk")
        ->join('produk','history_stiker.stock_stiker_produk_id_produk','=','produk.id_produk')
        ->join('sub_produk','history_stiker.stock_stiker_sub_produk_id','=','sub_produk.id_sub_produk')
        ->get();
        return Datatables::of($query)
        ->addColumn('awlstockluar', function ($query) {
            $luar = $query->hasil_luar - $query->masuk_luar + $query->pakai_luar;
            return $luar;
        })
        ->addColumn('awlstockdalam', function ($query) {
            $dalam = $query->hasil_dalam - $query->masuk_dalam + $query->pakai_dalam;
            return $dalam;
        })
        ->addColumn('masuk_tot', function ($query) {
            $masuktot = $query->masuk_luar + $query->masuk_dalam;
            return $masuktot;
        })
        ->addColumn('pakai_tot', function ($query) {
            $terpakaitot = $query->pakai_luar + $query->pakai_dalam;
            return $terpakaitot;
        })
        ->addColumn('hasil_tot', function ($query) {
            $hasiltot = $query->hasil_luar + $query->hasil_dalam;
            return $hasiltot;
        })
        ->addColumn('kodeproduk', function ($query) {
            $commit = $query->sub_kode.' | '.$query->sub_produk;
            return $commit;
        })
        ->addIndexColumn()
        ->make(true);
    }

    public function getpricestiker($request)
    {
        $kejrb = DB::table('harga_stiker')->where('sub_produk_id',$request->subproduk);
        if ($request->val != 0) {
            $kejrb->where('id',$request->id);
        }
        $jwy = $kejrb->orderBy('id','desc')->take(1);
        if ($jwy->count() > 0) {
            $tey = $jwy->first();
            $jek = ['hargaluar' => $tey->harga_luar,'hargadalam' => $tey->harga_dalam];
        } else {
            $jek = ['hargaluar' => 0,'hargadalam' => 0];
        }
        return $jek; 
    }

    public function showststi(Request $request)
    {
        $cek = DB::table('stock_stiker')->where([['sub_produk_id','=',$request->subproduk],['produk_id_produk','=',$request->id_produk]]);
        $get = $cek->first();
        if ($cek->count() > 0) {
            $variable1 = $get->stock_luar;
            $variable2 = $get->stock_dalam;
        } else {
            $variable1 = 0;
            $variable2 = 0;
        }
        $fge = $this->getpricestiker($request);
        echo json_encode(['count' => $cek->count(),'luar' => $variable1,'dalam' => $variable2,'hargaluar' => $fge['hargaluar'],'hargadalam' => $fge['hargadalam']]);
    }

    public function addstiker(Request $request)
    {
        DB::beginTransaction();
        $jejel = DB::table('harga_stiker')->where('sub_produk_id',$request->subproduk)->orderBy('id','desc')->take(1);
        if ($jejel->count() > 0) {
            $rtyu = $jejel->first();
            $hasil = $request->stockluar + $request->masukluar - $request->terpakailuar;
            $hasil1 = $request->stockdalam + $request->masukdalam - $request->terpakaidalam;

            $wel = $this->back_bahanbaku->stockstiker($request);

            $data = [
                'stock_stiker_id_stock_stiker' => $wel['resultid'],
                'stock_stiker_sub_produk_id' => $request->subproduk,
                'stock_stiker_produk_id_produk' => $request->produk,
                'harga_stiker_id' => $rtyu->id,
                'masuk_luar' => $request->masukluar,
                'pakai_luar' => $request->terpakailuar,
                'hasil_luar' => $hasil,
                'masuk_dalam' => $request->masukdalam,
                'pakai_dalam' => $request->terpakaidalam,
                'hasil_dalam' => $hasil1,
                'tanggal_hisstik' => $request->tanggal_hisstik,
                'ket_sti' => $request->ket_sti,
                'author_session' => Auth::user()->id,
                'created_at' => date('Y-m-d H:i:s'),
                'log' => "INSERT"
            ];
            $query = History_stiker::insert($data);
            if ($query) {
                $alert = 'Stock Stiker berhasil ditambahkan!';
            } else {
                $alert = 'Stock Stiker gagal ditambahkan!';
            }
        } else {
            $alert = 'Maaf anda tidak bisa menambahkan data stiker karena nilai harga stiker direferensi kosong.. Mohon dicek kembali!';
        }
        echo json_encode($alert);

        DB::commit();   
    }

    public function uppstiker(Request $request,$id,$masukl,$masukd,$pakail,$pakaid)
    {
        DB::beginTransaction();

        $this->back_bahanbaku->ifuppstiker($request,$id,$masukl,$masukd,$pakail,$pakaid);

        $hasil = $request->stockluar + $request->masukluar - $request->terpakailuar;
        $hasil1 = $request->stockdalam + $request->masukdalam - $request->terpakaidalam;

        $data = [
            'masuk_luar' => $request->masukluar,
            'pakai_luar' => $request->terpakailuar,
            'hasil_luar' => $hasil,
            'masuk_dalam' => $request->masukdalam,
            'pakai_dalam' => $request->terpakaidalam,
            'hasil_dalam' => $hasil1,
            'tanggal_hisstik' => $request->tanggal_hisstik,
            'ket_sti' => $request->ket_sti,
            'author_session' => Auth::user()->id,
            'log' => "UPDATE"
        ];

        $query = History_stiker::where('id',$id)->update($data);
        if ($query) {
            $this->back_bahanbaku->loop_upstiker($request,$id);
            $alert = 'Stock Stiker ID: '.$id.' berhasil diupdate!';
        } else {
            $alert = 'Stock Stiker ID: '.$id.' gagal diupdate!';
        }
        echo json_encode($alert);

        DB::commit();    
    }

    public function delstiker(Request $request,$id,$masukl,$masukd,$pakail,$pakaid,$stokawll,$stokawld)
    {
        DB::beginTransaction();
        $query = DB::table('stock_stiker')->where([['sub_produk_id','=',$request->subproduk],['produk_id_produk','=',$request->produk]])->first();

        $ban = $query->stock_luar - $masukl + $pakail;
        $ser = $query->stock_dalam - $masukd + $pakaid;

        $data = [
            'stock_luar' => $ban,
            'stock_dalam' => $ser
        ];
        $query = DB::table('stock_stiker')->where('id_stock_stiker',$query->id_stock_stiker)->update($data);

        $query1 = History_stiker::where('id',$id)->delete();
        if ($query1) {
            $this->back_bahanbaku->loop_upstiker($request,$id,$stokawll,$stokawld);
            $alert = 'Stock Temabakau  ID: '.$id.' berhasil diupdate!';
        } else {
            $alert = 'Stock Temabakau  ID: '.$id.' gagal diupdate!';
        }
        echo json_encode($alert);
        DB::commit();         
    }

    public function data_kemasan()
    {
        $data['produk'] = DB::table('produk')->get();
        $data['kemasan'] = DB::table('kemasan')->get();
        return view('home.bahan_baku.data_kemasan.data_kemasan',$data);
    }

    public function getpricekemasan($request)
    {
        $getsat = DB::table('harga_kemasan')->where('sub_produk_id',$request->subproduk);
        if ($request->val != 0) {
            $getsat->where('id',$request->id);
        }
        $jwy = $getsat->orderBy('id','desc')->take(1);
        if ($jwy->count() > 0) {
            $tey = $jwy->first();
            $jek = ['harga' => $tey->harga];
        } else {
            $jek = ['harga' => 0];
        }
        return $jek; 
    }

    public function getstokem(Request $request)
    {
        $query = DB::table('stock_kemasan')->where([['sub_produk_id','=',$request->subproduk],['produk_id_produk','=',$request->produk]]);
        if ($query->count() > 0) {
            $set = $query->first();
            $awl = $set->stock_kemasan;
        } else {
            $awl = 0;
        }
        $teyvr = $this->getpricekemasan($request);
        return json_encode(['count' => $query->count(),'content' => $awl,'harga' => $teyvr['harga']]);
    }

    public function getdatakemasan()
    {
        $query = DB::table('history_kemasan')
        ->select("history_kemasan.*","produk.produk","kemasan.nama_kemasan","sub_produk.sub_kode","sub_produk.sub_produk")
        ->join('produk','history_kemasan.stock_kemasan_produk_id_produk','=','produk.id_produk')
        ->join('sub_produk','history_kemasan.stock_kemasan_sub_produk_id','=','sub_produk.id_sub_produk')
        ->join('kemasan','history_kemasan.stock_kemasan_kemasan_id_kemasan','=','kemasan.id_kemasan')
        ->get();
        return Datatables::of($query)
        ->addIndexColumn()
        ->addColumn('kodeproduk',function($ope)
        {
            $gje = $ope->sub_kode.' | '.$ope->sub_produk;
            return $gje;
        })
        ->addColumn('awlstok', function ($query) {
            $awl = $query->stok_now - $query->masuk_kemasan + ($query->terpakai_kemasan + $query->afkir_kemasan);
            return $awl;
        })
        ->make(true);
    }

    public function addkemasan(Request $request)
    {
        DB::beginTransaction();
        $nehbr = DB::table('harga_kemasan')->where('sub_produk_id',$request->subproduk)->orderBy('id','desc')->take(1);
        if ($nehbr->count() > 0) {
            $retw = $nehbr->first();
            $resultid = $this->back_bahanbaku->stockkemasan($request);   
            $hasil = $request->stock + $request->masuk - ($request->terpakaikemasan + $request->afkir);

            $data = [
                'tanggal_hiskem' => $request->tanggal_hiskem,
                'masuk_kemasan' => $request->masuk,
                'terpakai_kemasan' => $request->terpakaikemasan,
                'afkir_kemasan' => $request->afkir,
                'stok_now' => $hasil,
                'stock_kemasan_id_stock_kemasan' => $resultid['resultid'],
                'stock_kemasan_kemasan_id_kemasan' => $request->kemasan,
                'stock_kemasan_sub_produk_id' => $request->subproduk,
                'stock_kemasan_produk_id_produk' => $request->produk,
                'harga_kemasan_id' => $retw->id,
                'ket_kem' => $request->ket_kem,
                'author_session' => Auth::user()->id,
                'created_at' => date('Y-m-d H:i:s'),
                'log' => "INSERT"
            ];
            $query = History_kemasan::insert($data);
            if ($query) {
                $alert = 'Stock Kemasan berhasil ditambahkan!';
            } else {
                $alert = 'Stock Kemasan gagal ditambahkan!';
            }
        } else{
            $alert = 'Maaf anda tidak bisa menambahkan data kemasan karena nilai harga kemasan direferensi kosong.. Mohon dicek kembali!';
        }
        echo json_encode($alert);

        DB::commit();  
    }

    public function uppkemasan(Request $request,$id,$masuk,$terpakai,$afkir)
    {
        DB::beginTransaction();

        $this->back_bahanbaku->ifuppkemasan($request,$id,$masuk,$terpakai,$afkir);
        $hasil = $request->stock + $request->masuk - ($request->terpakaikemasan + $request->afkir);

        $data = [
            'tanggal_hiskem' => $request->tanggal_hiskem,
            'masuk_kemasan' => $request->masuk,
            'terpakai_kemasan' => $request->terpakaikemasan,
            'afkir_kemasan' => $request->afkir,
            'stok_now' => $hasil,
            'ket_kem' => $request->ket_kem,
            'author_session' => Auth::user()->id,
            'log' => "UPDATE"
        ];
        $query = History_kemasan::where('id',$id)->update($data);
        if ($query) {
            $this->back_bahanbaku->loop_upkemasan($request,$id);
            $alert = 'Stock Kemasan ID: '.$id.' berhasil diupdate!';
        } else {
            $alert = 'Stock Kemasan ID: '.$id.' gagal diupdate!';
        }
        echo json_encode($alert);
        DB::commit();     
    }

    public function delkemasan(Request $request,$id,$masuk,$terpakai,$afkir,$stock)
    {
        DB::beginTransaction();
        $query = DB::table('stock_kemasan')->where([['kemasan_id_kemasan','=',$request->kemasan],['sub_produk_id','=',$request->subproduk],['produk_id_produk','=',$request->produk]])->first();

        $ban = $query->stock_kemasan - $masuk + ($terpakai + $afkir);

        $data = [
            'stock_kemasan' => $ban
        ];
        $query = DB::table('stock_kemasan')->where('id_stock_kemasan',$query->id_stock_kemasan)->update($data);

        $query1 = History_kemasan::where('id',$id)->delete();
        if ($query1) {
            $this->back_bahanbaku->loop_upkemasan($request,$id,$stock);
            $alert = 'Stock Kemasan  ID: '.$id.' berhasil dihapus!';
        } else {
            $alert = 'Stock Kemasan  ID: '.$id.' gagal dihapus!';
        }
        echo json_encode($alert);
        DB::commit();    
    }

    public function data_cukai()
    {
        $data['produk'] = DB::table('produk')->get();
        return view('home.bahan_baku.data_cukai.data_cukai',$data);
    }

    public function getsubonbase(Request $request)
    {
        $query = DB::table('sub_produk')
        ->join('kemasan','sub_produk.kemasan_id_kemasan','=','kemasan.id_kemasan')
        ->where([['produk_id_produk','=',$request->produk]])
        ->get();
        echo json_encode($query);
    }

    public function getpricecukai($request)
    {
        $getsat = DB::table('harga_cukai')->where('sub_produk_id',$request->subproduk);
        if ($request->val != 0) {
            $getsat->where('id',$request->id);
        }
        $jwy = $getsat->orderBy('id','desc')->take(1);
        if ($jwy->count() > 0) {
            $tey = $jwy->first();
            $jek = ['harga' => $tey->harga];
        } else {
            $jek = ['harga' => 0];
        }
        return $jek; 
    }

    public function getstoksukairow(Request $request)
    {
        $result = DB::table('stock_cukai')->where([['sub_produk_id_sub_produk','=',$request->subproduk],['sub_produk_produk_id_produk','=',$request->produk]]);
        if ($result->count() > 0) {
            $cek = $result->first();
            $hasil = $cek->stock_qty_cukai;
        } else {
            $hasil = 0;
        }
        $gher = $this->getpricecukai($request);
        $array = ['hasil' => $hasil,'harga' => $gher['harga']];
        return json_encode($array);
    }

    public function getdatacukai()
    {
        $query = DB::table('history_cukai')
        ->select("history_cukai.*",'produk.produk','sub_produk.sub_kode','sub_produk.sub_produk')
        ->leftJoin('produk','history_cukai.stock_cukai_produk_id_produk','=','produk.id_produk')
        ->leftJoin('sub_produk','history_cukai.stock_cukai_id_sub_produk','=','sub_produk.id_sub_produk')
        ->get();
        return Datatables::of($query)
        ->addIndexColumn()
        ->addColumn('sub_sam', function($query)
        {
            return $query->sub_kode.' - '.$query->sub_produk;
        })
        ->addColumn('stockawal', function ($query) {
            $awl = $query->sisa_cukai - $query->masuk_cukai + ($query->terpakai_cukailama + $query->terpakai_cukaibaru);
            return $awl;
        })
        ->make(true);
    }

    public function addcukai(Request $request)
    {
        DB::beginTransaction();
        $nejder = DB::table('harga_cukai')->where('sub_produk_id',$request->subproduk)->orderBy('id','desc')->take(1);
        if ($nejder->count() > 0) {
            $blade = $nejder->first();
            $result = $this->back_bahanbaku->stockcukai($request);
            $data = [
                'masuk_cukai' => $request->masuk,
                'terpakai_cukailama' => $request->terpakailama,
                'terpakai_cukaibaru' => $request->terpakaibaru,
                'sisa_cukai' => $result['hasil'],
                'tanggal_cukai' => $request->tanggal_cukai,
                'ket_hiscukai' => $request->ket,
                'stock_cukai_id_stock_cukai' => $result['resultid'],
                'stock_cukai_id_sub_produk' => $request->subproduk,
                'stock_cukai_produk_id_produk' => $request->produk,
                'harga_cukai_id' => $blade->id,
                'author_session' => Auth::user()->id,
                'created_at' => date('Y-m-d H:i:s'),
                'log' => 'INSERT',
            ];
            $query = History_cukai::insert($data);
            if ($query) {
                $alert = 'Stock Cukai berhasil ditambahkan!';
            } else {
                $alert = 'Stock Cukai gagal ditambahkan!';
            }
        } else {
            $alert = 'Maaf anda tidak bisa menambahkan data cukai karena nilai harga cukai direferensi kosong.. Mohon dicek kembali!';
        }
        echo json_encode($alert);
        DB::commit();
    }

    public function uppcukai(Request $request,$id,$masuk,$lama,$baru)
    {
        DB::beginTransaction();

        $this->back_bahanbaku->ifuppcukai($request,$id,$masuk,$lama,$baru);
        $hasil = $request->stock + $request->masuk - ($request->terpakailama + $request->terpakaibaru);

        $data = [
            'masuk_cukai' => $request->masuk,
            'terpakai_cukailama' => $request->terpakailama,
            'terpakai_cukaibaru' => $request->terpakaibaru,
            'sisa_cukai' => $hasil,
            'tanggal_cukai' => $request->tanggal_cukai,
            'ket_hiscukai' => $request->ket,
            'author_session' => Auth::user()->id,
            'log' => 'UPDATE',
        ];
        $query = History_cukai::where('id',$id)->update($data);
        if ($query) {
            $this->back_bahanbaku->loop_upcukai($request,$id);
            $alert = 'Stock Cukai berhasil ID: '.$id.' diupdate!';
        } else {
            $alert = 'Stock Cukai gagal ID: '.$id.' diupdate!';
        }
        echo json_encode($alert);
        DB::commit();
    }

    public function delcukai(Request $request,$id,$masuk,$lama,$baru,$stokcne)
    {
        DB::beginTransaction();
        $query = DB::table('stock_cukai')->where([['sub_produk_id_sub_produk','=',$request->subproduk],['sub_produk_produk_id_produk','=',$request->produk]])->first();

        $ban = $query->stock_qty_cukai - $masuk + ($lama + $baru);

        $data = [
            'stock_qty_cukai' => $ban
        ];
        $query = DB::table('stock_cukai')->where('id_stock_cukai',$query->id_stock_cukai)->update($data);

        $query1 = History_cukai::where('id',$id)->delete();
        if ($query1) {
            $this->back_bahanbaku->loop_upcukai($request,$id,$stokcne);
            $alert = 'Stock Cukai  ID: '.$id.' berhasil dihapus!';
        } else {
            $alert = 'Stock Cukai  ID: '.$id.' gagal dihapus!';
        }
        echo json_encode($alert);
        DB::commit();    
    }
}