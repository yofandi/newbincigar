<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RFS_HIS extends Model
{
	protected $table = 'rfs_his';

	public function joinuser()
	{
        return $this->belongsTo('App\User','users_id');
	}
}
