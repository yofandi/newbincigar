<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Stock_probaku;
use App\History_bahanmasuk;
use App\History_cincin;
use App\History_stiker;
use App\History_cukai;
use App\History_kemasan;

class Back_bahanbaku extends Controller
{
    public function stockspec($request,$data)
    {
        $query = Stock_probaku::where([['jenis_id_jenis','=',$request->jenis],['kategori_id_kategori','=',$request->kategori]])->first();
        $query->update($data);
        // $name => $value
        return ['resultid' => $query->id];
    }

    public function stocklis($request,$produksi,$bahan)
    {
    	$query = Stock_probaku::where([['jenis_id_jenis','=',$request->jenis],['kategori_id_kategori','=',$request->kategori]])->first();
        $query->update(['jml_produksi' => $produksi,'jml_bahanbaku' => $bahan]);
        return ['resultid' => $query->id];
    }

    public function stockpert($request)
    {
    	$cek = Stock_probaku::where([['jenis_id_jenis','=',$request->jenis],['kategori_id_kategori','=',$request->kategori]]);
        if ($cek->count() > 0) {
            $get = $cek->first();
            $produksi = $request->st_produksi + $get->jml_produksi;
            $bahan = $get->jml_bahanbaku + $request->st_terima - $request->st_produksi;
            $poe = $this->stocklis($request,$produksi,$bahan);
            $queryid = $poe['resultid'];
        } else {            
            $bahan = $request->st_terima - $request->st_produksi;
            $query = new Stock_probaku;
            $query->jml_produksi = $request->st_produksi;
            $query->jml_bahanbaku = $bahan;
            $query->kategori_id_kategori = $request->kategori;
            $query->jenis_id_jenis = $request->jenis;

            $query->save();
            $queryid = $query->id;
        }
        return ['queryid' => $queryid,'hasil' => $bahan];
    }

    public function runfirst($query,$request,$terima)
    {
        if ($terima > $request->st_terima) {
            $hsl = $terima - $request->st_terima;

            $bar = $query->jml_bahanbaku - $hsl;

            $po = ['jml_bahanbaku' => $bar];

            $this->stockspec($request,$po);

        } else if ($terima < $request->st_terima) {
            $hsl = $request->st_terima - $terima;

            $bar = $query->jml_bahanbaku + $hsl;


            $po = ['jml_bahanbaku' => $bar];

            $this->stockspec($request,$po);
        }
        return true;
    }

    public function ifupptembakau($request,$terima,$produksi)
    {
        $query = Stock_probaku::where([['jenis_id_jenis','=',$request->jenis],['kategori_id_kategori','=',$request->kategori]])->first();
        if ($terima != $request->st_terima || $produksi != $request->st_produksi) {
            $first = $this->runfirst($query,$request,$terima);
            if ($first == true) {
                if ($produksi > $request->st_produksi) {
                    $hsl1 = $produksi - $request->st_produksi;
                    $aem = $query->jml_bahanbaku + $hsl1;
                    $car = $query->jml_produksi - $hsl1;

                    $sae = ['jml_produksi' => $car,'jml_bahanbaku' => $aem];

                    $this->stockspec($request,$sae);
                } else if ($produksi < $request->st_produksi) {
                    $hsl1 = $request->st_produksi - $produksi;
                    $aem = $query->jml_bahanbaku - $hsl1;
                    $car = $query->jml_produksi + $hsl1;

                    $sae = ['jml_produksi' => $car,'jml_bahanbaku' => $aem];

                    $this->stockspec($request,$sae);
                }
            }
        }
    }

    public function loop_updcigar($request,$id,$value = '')
    {
        $his  = History_bahanmasuk::where([['jenis_id_jenis','=',$request->jenis],['kategori_id_kategori','=',$request->kategori],['id','>',$id]]);
        if ($his->count() > 0) {
            $reg = $his->get();
            for ($i=0; $i < $his->count(); $i++) { 
                $shoe  = History_bahanmasuk::where([['jenis_id_jenis','=',$request->jenis],['kategori_id_kategori','=',$request->kategori],['id','<',$reg[$i]->id]])->orderBy('id','desc')->take('1');
                if ($shoe->count() > 0) {
                    $jkp = $shoe->first();
                    $today = $jkp->hari_ini + $reg[$i]->diterima - $reg[$i]->diproduksi;
                } else {
                    if ($value == '') {
                        $hejka = $reg[$i]->hari_ini - $reg[$i]->diterima + $reg[$i]->diproduksi;
                        $today = $hejka + $reg[$i]->diterima - $reg[$i]->diproduksi;
                    } else {
                        $today = $value + $reg[$i]->diterima - $reg[$i]->diproduksi;
                    }
                }

                $update = History_bahanmasuk::find($reg[$i]->id);
                $update->hari_ini = $today;
                $update->save();
            }
        }
    }

    public function stockcincin($request)
    {
        $cek = DB::table('stock_cincin')->where([['produk_id_produk','=',$request->produk]]);
        if ($cek->count() > 0) {
            $get = $cek->first();
            $hasil = $get->stock_qty_cincin + $request->masuk - ($request->terpakaicincin + $request->afkir);
            $data = ['stock_qty_cincin' => $hasil];
            $query = DB::table('stock_cincin')->where('id_stock_cincin',$get->id_stock_cincin)->update($data);
            $resultid = $get->id_stock_cincin;
        } else {
            $hasil = 0 + $request->masuk - ($request->terpakaicincin + $request->afkir);
            $data = [
                'stock_qty_cincin' => $hasil,
                'produk_id_produk' => $request->produk
            ];
            $query = DB::table('stock_cincin')->insertGetId($data);
            $resultid = $query;
        }
        return ['resultid' => $resultid,'hasil' => $hasil];
    }

    public function ifuppcincin($request,$masuk,$terpakai,$afkir)
    {
        $cek = DB::table('stock_cincin')->where([['produk_id_produk','=',$request->produk]])->first();
        if ($masuk != $request->masuk || $terpakai != $request->terpakaicincin || $afkir != $request->afkir) {
            $min = $terpakai + $afkir;
            $man = $request->terpakaicincin + $request->afkir;

            if ($masuk > $request->masuk) {
                $masuksam = $masuk - $request->masuk;
                $har = $cek->stock_qty_cincin - $masuksam;
            } elseif ($masuk < $request->masuk) {
                $masuksam = $request->masuk - $masuk;
                $har = $cek->stock_qty_cincin + $masuksam;
            } else {
                $har = $cek->stock_qty_cincin;
            }
            $car = $har;
            if ($min > $man) {
                $das = $min - $man;
                $gh = $car + $das;
            } elseif ($min < $man) {
                $das = $man - $min;
                $gh = $car - $das;
            } else {
                $gh = $car - 0;
            }

            $retro = $gh;

            DB::table('stock_cincin')->where(['id_stock_cincin' => $cek->id_stock_cincin])->update(['stock_qty_cincin' => $retro]);
        }
    }

    public function loop_upcincin($request,$id,$stockawal = '')
    {
        $his  = History_cincin::where([['stock_cincin_produk_id_produk','=',$request->produk],['id','>',$id]]);
        if ($his->count() > 0) {
            $reg = $his->get();
            for ($i=0; $i < $his->count(); $i++) { 
                $shoe  = History_cincin::where([['stock_cincin_produk_id_produk','=',$request->produk],['id','<',$reg[$i]->id]])->orderBy('id','desc')->take('1');
                if ($shoe->count() > 0) {
                    $jkp = $shoe->first();
                    $today = $jkp->sisa + $reg[$i]->masuk - ($reg[$i]->terpakai + $reg[$i]->afkir);
                } else {
                    if ($stockawal == '') {
                        $awalcdd = $reg[$i]->sisa - $reg[$i]->masuk + ($reg[$i]->terpakai + $reg[$i]->afkir);
                        $today = $awalcdd + $reg[$i]->masuk - ($reg[$i]->terpakai + $reg[$i]->afkir);
                    } else {
                        $today = $stockawal + $reg[$i]->masuk - ($reg[$i]->terpakai + $reg[$i]->afkir);
                    }
                }

                $update = History_cincin::find($reg[$i]->id);
                $update->sisa = $today;
                $update->save();
            }
        }
    }

    public function stockstiker($request)
    {
        $cek = DB::table('stock_stiker')->where([['sub_produk_id','=',$request->subproduk],['produk_id_produk','=',$request->produk]]);
        if ($cek->count() > 0) {
            $get = $cek->first();
            $hasil = $get->stock_luar + $request->masukluar - $request->terpakailuar;
            $hasil1 = $get->stock_dalam + $request->masukdalam - $request->terpakaidalam;
            $data = [
                'stock_luar' => $hasil,
                'stock_dalam' => $hasil1
            ];
            $query = DB::table('stock_stiker')->where('id_stock_stiker',$get->id_stock_stiker)->update($data);
            $resultid = $get->id_stock_stiker;
        } else {
            $hasil = 0 + $request->masukluar - $request->terpakailuar;
            $hasil1 = 0 + $request->masukdalam - $request->terpakaidalam;
            $data = [
                'stock_luar' => $hasil,
                'stock_dalam' => $hasil1,
                'sub_produk_id' => $request->subproduk,
                'produk_id_produk' => $request->produk
            ];
            $query = DB::table('stock_stiker')->insertGetId($data);
            $resultid = $query;
        }
        return ['resultid' => $resultid,'hasilluar' => $hasil,'hasildalam' => $hasil1];   
    }

    public function ifuppstiker($request,$id,$masukl,$masukd,$pakail,$pakaid)
    {
        $cek = DB::table('stock_stiker')->where([['sub_produk_id','=',$request->subproduk],['produk_id_produk','=',$request->produk]])->first();
        if ($masukl != $request->masukluar || $masukd != $request->masukdalam || $pakail != $request->terpakailuar || $pakaid != $request->terpakaidalam) {
            if ($masukl > $request->masukluar) {
                $ml = $masukl - $request->masukluar;
                $stl = $cek->stock_luar - $ml;
            } elseif ($masukl < $request->masukluar) {
                $ml = $request->masukluar - $masukl;
                $stl = $cek->stock_luar + $ml;
            } else {
                $stl = $cek->stock_luar;
            }
            $rt = $stl;

            if ($pakail > $request->terpakailuar) {
                $pl = $pakail - $request->terpakailuar;
                $hasl = $rt + $pl;                
            } elseif ($pakail < $request->terpakailuar) {
                $pl = $request->terpakailuar - $pakail;
                $hasl = $rt - $pl;                
            } else {
                $hasl = $rt;                
            }

            if ($masukd > $request->masukdalam) {
                $md = $masukd - $request->masukdalam;
                $std = $cek->stock_dalam - $md;
            } elseif ($masukd < $request->masukdalam) {
                $md = $request->masukdalam - $masukd;
                $std = $cek->stock_dalam + $md;
            } else {
                $std = $cek->stock_dalam;
            }
            $tr = $std;

            if ($pakaid > $request->terpakaidalam) {
                $pd = $pakaid - $request->terpakaidalam;
                $hasd = $tr + $pd;
            } elseif ($pakaid < $request->terpakaidalam) {
                $pd = $request->terpakaidalam - $pakaid;
                $hasd = $tr - $pd;
            } else {
                $hasd = $tr;
            }

            DB::table('stock_stiker')->where('id_stock_stiker',$cek->id_stock_stiker)->update(['stock_luar' => $hasl,'stock_dalam' => $hasd]);
        }
    }

    public function loop_upstiker($request,$id,$stockluar = '',$stockdalam = '')
    {
        $cek = DB::table('history_stiker')->where([['stock_stiker_sub_produk_id','=',$request->subproduk],['stock_stiker_produk_id_produk','=',$request->produk],['id','>',$id]]);
        if ($cek->count() > 0) {
            $get = $cek->get();
            for ($i=0; $i < $cek->count(); $i++) {
                $ter = DB::table('history_stiker')->where([['stock_stiker_produk_id_produk','=',$request->produk],['id','<',$get[$i]->id]])->orderBy('id','desc')->take('1');
                if ($ter->count() > 0) {
                    $erd = $ter->first();
                    $hasil_luar = $erd->hasil_luar + $get[$i]->masuk_luar - $get[$i]->pakai_luar;
                    $hasil_dalam = $erd->hasil_dalam + $get[$i]->masuk_dalam - $get[$i]->pakai_dalam;
                } else {
                    if ($stockluar == '') {
                        $pe = 1;
                        $ajeb = $get[$i]->hasil_luar - $get[$i]->masuk_luar + $get[$i]->pakai_luar;
                        $hasil_luar = $ajeb + $get[$i]->masuk_luar - $get[$i]->pakai_luar;
                    } else {
                        $pe = 0;
                        $hasil_luar = $stockluar + $get[$i]->masuk_luar - $get[$i]->pakai_luar;
                    }
                    if ($stockdalam == '') {
                        $ke = 1;
                        $gahbe = $get[$i]->hasil_dalam + $get[$i]->masuk_dalam - $get[$i]->pakai_dalam;
                        $hasil_dalam = $gahbe + $get[$i]->masuk_dalam - $get[$i]->pakai_dalam;
                    } else {
                        $ke = 0;
                        $hasil_dalam = $stockdalam + $get[$i]->masuk_dalam - $get[$i]->pakai_dalam;
                    }
                }

                $query = History_stiker::find($get[$i]->id);
                $query->hasil_luar = $hasil_luar;
                $query->hasil_dalam = $hasil_dalam;

                $query->save();   
                return $cek->count();             
            }
        }
    }

    public function stockkemasan($request)
    {
        $cek = DB::table('stock_kemasan')->where([['kemasan_id_kemasan','=',$request->kemasan],['sub_produk_id','=',$request->subproduk],['produk_id_produk','=',$request->produk]]);
        if ($cek->count() > 0) {
            $get = $cek->first();
            $hasil = $get->stock_kemasan + $request->masuk - ($request->terpakaikemasan + $request->afkir);
            $data = [
                'stock_kemasan' => $hasil,
            ];
            $query = DB::table('stock_kemasan')->where('id_stock_kemasan',$get->id_stock_kemasan)->update($data);
            $resultid = $get->id_stock_kemasan;
        } else {
            $hasil = 0 + $request->masuk - ($request->terpakaikemasan + $request->afkir);
            $data = [
                'stock_kemasan' => $hasil,
                'kemasan_id_kemasan' => $request->kemasan,
                'sub_produk_id' => $request->subproduk,
                'produk_id_produk' => $request->produk,
            ];
            $query = DB::table('stock_kemasan')->insertGetId($data);
            $resultid = $query;
        }
        return ['resultid' => $resultid,'hasil' => $hasil];   
    }

    public function ifuppkemasan($request,$id,$masuk,$terpakai,$afkir)
    {
        $cek = DB::table('stock_kemasan')->where([['kemasan_id_kemasan','=',$request->kemasan],['sub_produk_id','=',$request->subproduk],['produk_id_produk','=',$request->produk]])->first();
        if ($masuk != $request->masuk || $terpakai != $request->terpakaikemasan || $afkir != $request->afkir) {
            if ($masuk > $request->masuk) {
                $ret = $masuk - $request->masuk;
                $ter = $cek->stock_kemasan - $ret;
            } elseif ($masuk < $request->masuk) {
                $ret = $request->masuk - $masuk;
                $ter = $cek->stock_kemasan + $ret;
            } else {
                $ter = $cek->stock_kemasan;
            }

            $por = $ter;

            $wq = $terpakai + $afkir;
            $qr = $request->terpakaikemasan + $request->afkir;

            if ($wq > $qr) {
                $slk = $wq - $qr;
                $hasil = $por + $slk;
            } elseif ($wq < $qr) {
                $slk = $qr - $wq;
                $hasil = $por - $slk;
            } else {
                $hasil = $por;
            }

            DB::table('stock_kemasan')->where('id_stock_kemasan',$cek->id_stock_kemasan)->update(['stock_kemasan' => $hasil]);
        }
    }

    public function loop_upkemasan($request,$id,$stockawal = '')
    {
        $cek = History_kemasan::where([['stock_kemasan_kemasan_id_kemasan','=',$request->kemasan],['stock_kemasan_sub_produk_id','=',$request->subproduk],['stock_kemasan_produk_id_produk','=',$request->produk],['id','>',$id]]);
        if ($cek->count() > 0) {
            $get = $cek->get();
            for ($i=0; $i < $cek->count(); $i++) {
                $zsf = History_kemasan::where([['stock_kemasan_kemasan_id_kemasan','=',$request->kemasan],['stock_kemasan_sub_produk_id','=',$request->subproduk],['stock_kemasan_produk_id_produk','=',$request->produk],['id','<',$get[$i]->id]])->orderBy('id','desc')->take('1');
                if ($zsf->count() > 0) {
                    $his = $zsf->first();
                    $hasil = $his->stok_now + $get[$i]->masuk_kemasan - ($get[$i]->terpakai_kemasan  +  $get[$i]->afkir_kemasan);
                } else {
                    if ($stockawal == '') {
                        $awalkem = $get[$i]->stok_now - $get[$i]->masuk_kemasan + ($get[$i]->terpakai_kemasan  +  $get[$i]->afkir_kemasan);
                        $hasil = $awalkem + $get[$i]->masuk_kemasan - ($get[$i]->terpakai_kemasan  +  $get[$i]->afkir_kemasan);
                    } else {
                        $hasil = $stockawal + $get[$i]->masuk_kemasan - ($get[$i]->terpakai_kemasan  +  $get[$i]->afkir_kemasan);
                    }
                }
                History_kemasan::where('id',$get[$i]->id)->update(['stok_now' => $hasil]);
            }
        }
    }

    public function stockcukai($request)
    {
        $cek = DB::table('stock_cukai')->where([['sub_produk_id_sub_produk','=',$request->subproduk],['sub_produk_produk_id_produk','=',$request->produk]]);
        if ($cek->count() > 0) {
            $get = $cek->first();
            $hasil = $get->stock_qty_cukai + $request->masuk - ($request->terpakailama + $request->terpakaibaru);
            $data = [
                'stock_qty_cukai' => $hasil,
            ];
            $query = DB::table('stock_cukai')->where('id_stock_cukai',$get->id_stock_cukai)->update($data);
            $resultid = $get->id_stock_cukai;
        } else {
            $hasil = 0 + $request->masuk - ($request->terpakailama + $request->terpakaibaru);
            $data = [
                'stock_qty_cukai' => $hasil,
                'sub_produk_id_sub_produk' => $request->subproduk,
                'sub_produk_produk_id_produk' => $request->produk,
            ];
            $query = DB::table('stock_cukai')->insertGetId($data);
            $resultid = $query;
        }
        return ['resultid' => $resultid,'hasil' => $hasil];   
    }

    public function ifuppcukai($request,$id,$masuk,$lama,$baru)
    {
        $cek = DB::table('stock_cukai')->where([['sub_produk_id_sub_produk','=',$request->subproduk],['sub_produk_produk_id_produk','=',$request->produk]])->first();
        if ($masuk != $request->masuk || $lama != $request->terpakailama || $baru != $request->terpakaibaru) {
            if ($masuk > $request->masuk) {
                $sel = $masuk - $request->masuk;
                $haw = $cek->stock_qty_cukai - $sel;
            } elseif ($masuk < $request->masuk) {
                $sel = $request->masuk - $masuk;
                $haw = $cek->stock_qty_cukai + $sel;
            } else {
                $haw = $cek->stock_qty_cukai;
            }
            $rer = $haw;

            $terpakaia = $lama + $baru;
            $terpakaib = $request->terpakailama + $request->terpakaibaru;

            if ($terpakaia > $terpakaib) {
                $les = $terpakaia - $terpakaib;
                $wah = $rer + $les;
            } elseif ($terpakaia < $terpakaib) {
                $les = $terpakaib - $terpakaia;
                $wah = $rer - $les;
            } else {
                $wah = $rer;
            }
            $hasil = $wah;
            DB::table('stock_cukai')->where('id_stock_cukai',$cek->id_stock_cukai)->update(['stock_qty_cukai' => $hasil]);
        }
    }

    public function loop_upcukai($request,$id,$stokawalen = '')
    {
        $kam = History_cukai::where([['stock_cukai_id_sub_produk','=',$request->subproduk],['stock_cukai_produk_id_produk','=',$request->produk],['id','>',$id]]);
        if ($kam->count() > 0) {
            $get = $kam->get();
            for ($i=0; $i < $kam->count(); $i++) {
                $wet = History_cukai::where([['stock_cukai_id_sub_produk','=',$request->subproduk],['stock_cukai_produk_id_produk','=',$request->produk],['id','<',$get[$i]->id]])->orderBy('id','desc')->take(1);
                if ($wet->count() > 0) {
                    $qqa = $wet->first();
                    $jqwt = $qqa->sisa_cukai + $get[$i]->masuk_cukai - ($get[$i]->terpakai_cukailama + $get[$i]->terpakai_cukaibaru);
                } else {
                    if ($stokawalen == '') {
                        $abej = $get[$i]->sisa_cukai - $get[$i]->masuk_cukai + ($get[$i]->terpakai_cukailama + $get[$i]->terpakai_cukaibaru);
                        $jqwt = $stokawalen + $get[$i]->masuk_cukai - ($get[$i]->terpakai_cukailama + $get[$i]->terpakai_cukaibaru);
                    } else {
                        $jqwt = $stokawalen + $get[$i]->masuk_cukai - ($get[$i]->terpakai_cukailama + $get[$i]->terpakai_cukaibaru);
                    }

                }
                $asjl = History_cukai::where('id',$get[$i]->id)->update(['sisa_cukai' => $jqwt]);
            }
        }
    }
}
