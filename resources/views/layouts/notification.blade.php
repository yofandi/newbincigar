<script>
    $(document).ready(function() {
        let authuser = {{ Auth::user()->level_user_id_level }};
        let rfslev = {!! json_encode($rfs) !!};
        let rfsbagan = {!! json_encode($rfsbagan) !!};

        if (rfslev.includes(authuser)) {
            intinval('/api/noc_inrfs');     
        }

        if (rfsbagan.includes(authuser)) {
            intinval_init();            
        }
    });

    function intinval(lmwne) {
        let = lmwne;
        setTimeout(function() {
            notification_rfs(lmwne);
            let refresh  = setInterval(function() {
                notification_rfs(lmwne);
            },15000);
        },5000);
    }

    function intinval_init() {
        setTimeout(function() {
            notifikasi_rfsbagan();
            let refresh  = setInterval(function() {
                notifikasi_rfsbagan();
            },15000);
        },5000);
    }

    function notification_rfs(lmwne) {
        $.ajax({
            url: lmwne,
            type: 'GET',
            dataType: 'json',
        })
        .done(function(data) {
            $html_nocrfs = '<a class="nav-link dropdown-toggle waves-effect waves-dark" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="icon-Bell"></i>';
            if (data.length) {
                $html_nocrfs += '<div class="notify"> <span class="heartbit"></span> <span class="point"></span> </div>';
            }
            $html_nocrfs += '</a>';
            $html_nocrfs += '<div class="dropdown-menu dropdown-menu-right mailbox animated bounceInDown">';
            $html_nocrfs +=  '<ul>';/*$html_nocrfs += '<li><div class="drop-title">Barang </div></li>';*/
            $html_nocrfs += '<li>';
            $html_nocrfs += '<div class="message-center">';

            for (var i = 0; i < data.length; i++) {
                $html_nocrfs += '<a href="'+ data[i].link +'">';
                $html_nocrfs += '<div class="btn btn-danger btn-circle"><i class="'; 
                switch (data[i].status) {
                    case 0:
                    $html_nocrfs += 'fa fa-archive';
                    $name_pendbr = 'Barang Check';
                    break;
                    case 1:
                    $html_nocrfs += 'fa fa-check';
                    $name_pendbr = 'Barang Diterima';
                    break;
                    case 2:
                    $html_nocrfs += 'fa fa-times';
                    $name_pendbr = 'Barang Ditolak';
                    break;
                    case 3:
                    $html_nocrfs += 'fa fa-check';
                    $name_pendbr = 'Barang Masuk';
                    break;
                    default:
                    $html_nocrfs += 'fa fa-times';
                    break;
                }
                $html_nocrfs += '"></i></div>';
                $html_nocrfs += '<div class="mail-contnet">';
                $html_nocrfs += ' <span class="mail-desc">'+data[i].namestatus+' '+ data[i].tanggal +'</span>';
                $html_nocrfs +='</div>';
                $html_nocrfs += '</a>';
            }

            $html_nocrfs += '</div>';
            $html_nocrfs += '</li>';
            $html_nocrfs += '<li><a class="nav-link text-center" href="/listtable"> <strong>Check all notifications</strong> <i class="fa fa-angle-right"></i></a></li>';
            $html_nocrfs +=  '</ul>';
            $html_nocrfs += '</div>';
            $('#noc_rfs').html($html_nocrfs);
        });
    }

    function notifikasi_rfsbagan() {
        $.ajax({
            url: '/api/noc_rfsbagan',
            type: 'GET',
            dataType: 'json',
        })
        .done(function(data) {
         $button = '<a class="nav-link dropdown-toggle waves-effect waves-dark" href="#" id="2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">';
         $button += '<i class="icon-Mail"></i>';
         if (data.length > 0) {
             $button += '<div class="notify"> <span class="heartbit"></span><span class="point"></span> </div>';
         }
         $button += '</a>';
         $button += '<div class="dropdown-menu mailbox dropdown-menu-right animated bounceInDown" aria-labelledby="2">';
         $button += '<ul>';
         $button += '<li>';
         if (data.length > 0) {
            $button += '<div class="drop-title">You have '+ data.length +' new messages</div>';
        }
        $button += '</li>';
        $button += '<li>';
        $button += '<div class="message-center">';
        for (var i = 0; i < data.length; i++) {
            // data[i].
            $button += '<a href="/listtable/rfsbagan/detail/'+ data[i].id +'">';
            $button += '<div class="btn btn-danger btn-circle"><i class="fa fa-plus"></i></div>';
            $button += '<div class="mail-contnet">';
            switch (data[i].verifikasi_rfs) {
                case 0:
                $button += '<h5>Order (Menunggu)</h5> ';
                break;

                case 1:
                $button += '<h5>Acc</h5> ';
                break;

                case 2:
                $button += '<h5>Barang Masuk</h5> ';
                break;
            }
            $button += '<span class="mail-desc">'+ data[i].keterangan_order +'</span> <span class="time"></span>';
            $button += '</div>';
            $button += '</a>';
        }

        $button += '</div>';
        $button += '</li>';
        $button += '<li>';
        $button += '<a class="nav-link text-center" href="/listtable/rfsbagan"> <strong>See all e-Mails</strong> <i class="fa fa-angle-right"></i> </a>';
        $button += '</li>';
        $button += '</ul>';
        $button += '</div>';
        $('#noc_rfsbagan').html($button);
    });
    }
</script>