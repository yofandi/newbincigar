<script>
	$('#stock_showit').hide();
	$('#loadingdeleteqc').hide();
	$('.spinner_nekrqc').hide();
	let token = $("meta[name='csrf-token']").attr("content");

	const jenis = {!! json_encode($jenis) !!};
	jenis.unshift({id_jenis: '',jenis: ''});

	var table = $('#table-qc').DataTable({
		"lengthMenu": [
		[3,5,10, 25, 50, 100, 150, 200, -1],
		[3,5,10, 25, 50, 100, 150, 200, "All"]
		],
		"order": [
		[0, 'desc']
		],
		processing: true,
		serverSide: true,
		ajax: '/dataqc/json',
		columns: [
		{ data: 'DT_RowIndex', name: 'DT_RowIndex' },
		{ data: 'tanggal_qc', name: 'tanggal_qc' },
		{ data: 'produk', name: 'produk' },
		{ data: 'jenis', name: 'jenis' },
		{ data: 'lama_qc', name: 'lama_qc' },
		{ data: 'awlbatang', name: 'awlbatang' },
		{ data: 'accept', name: 'accept' },
		{ data: 'mutasi_batang', name: 'mutasi_batang' },
		{ data: 'back_fill', name: 'back_fill' },
		{ data: 'back_lazio', name: 'back_lazio' },
		{ data: 'back_bind', name: 'back_bind' },
		{ data: 'back_wrap', name: 'back_wrap' },
		{ data: 'hasil_qc', name: 'hasil_qc' },
		{ data: 'keterangan_qc', name: 'keterangan_qc' },
		{
			data: null,
			"bSortable": false,
			mRender: function (o) { 
				return '<button type="button" class="btn btn-info btn-rounded btn-xs" id="update-fumigasi" data-title="Update Quality Control | ID: '+ o.id +'" data-link="/dataqc/upp/'+ o.id +'/'+ o.accept +'/'+ o.mutasi_batang +'/'+ o.back_fill +'/'+ o.back_bind +'/'+ o.back_wrap +'/'+ o.back_lazio +'" data-tanggal_qc="'+ o.tanggal_qc +'" data-jenis="'+ o.id_jenis +'" data-produk="'+ o.id_produk +'" data-durasi="'+ o.lama_qc +'" data-ket="'+ o.keterangan_qc +'" data-stockbatang="'+ o.awlbatang +'" data-batang="'+ o.accept +'" data-mutasi="'+ o.mutasi_batang +'" data-rejfill="'+ o.back_fill +'" data-rejbind="'+ o.back_bind +'" data-rejwrap="'+ o.back_wrap +'" data-rejlaz="'+ o.back_lazio +'" data-iddrying3="'+ o.stock_drying3_id +'" data-idqc="'+ o.stock_qc_id +'" data-berat="'+ o.berat_jadi +'"><i class="mdi mdi-dots-horizontal"></i></button><button type="button" class="btn btn-danger btn-rounded btn-xs" id="delete-fumigasi" data-link="/dataqc/del/'+ o.id +'/'+ o.accept +'/'+ o.mutasi_batang +'/'+ o.back_fill +'/'+ o.back_bind +'/'+ o.back_wrap +'/'+ o.back_lazio +'/'+ o.awlbatang +'" data-jenis="'+ o.id_jenis +'" data-produk="'+ o.id_produk +'" data-iddrying3="'+ o.stock_drying3_id +'" data-idqc="'+ o.stock_qc_id +'"><i class="fa fa-trash"></i></button>'; }
			},
			],
			"displayLength": 10,
		});
	$('#table-qc tbody').on('click', 'tr.group', function() {
		var currentOrder = table.order()[0];
		if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
			table.order([2, 'desc']).draw();
		} else {
			table.order([2, 'asc']).draw();
		}
	});
	$('#table-qc tbody').on('click', 'tr.group', function() {
		var currentOrder = table.order()[0];
		if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
			table.order([2, 'desc']).draw();
		} else {
			table.order([2, 'asc']).draw();
		}
	});

	$(document).on('keyup', '#mutasijml', function(event) {
		$kx = $('#beratperbtg').val();
		$leb = $(this).val();
		$le = $kx * $leb;
		$iw = $le * 2;
		$('#rejfill').val($iw);
		$('#rejlaz').val(0);
		$('#rejbind').val($le);
		$('#rejwrap').val($le);
	});

	function Refresh_qc() {
		$('#table-qc').DataTable().ajax.reload();
	}

	function getdatasearch(position1) {
		let jenis = $('#jenis_in').val();
		let produk = $('#produk_in').val();

		$.ajax({
			url: '/dataqc/getsearchfill',
			type: 'POST',
			dataType: 'json',
			data: {
				"_token": token,
				produk: produk,
				jenis: jenis
			},
		})
		.done(function(data) {
			$('#idqc').val(data.idqc);
			$('#iddrying3').val(data.iddrying3);
			$('#beratperbtg').val(data.berat_jadi);
			$('#' + position1).val(data.jml_dry3);
		});
		
	}

	function ljenis(position,like) {
		let opt = '';
		for (let i = 0; i < jenis.length; i++) {
			opt += '<option value="'+ jenis[i].id_jenis +'"';
			if (jenis[i].id_jenis == like) {
				opt += 'selected';
			}
			opt += '>'+ jenis[i].jenis +'</option>';
		}
		$('#' + position).html(opt);
	}

	function lproduk(position,based,like) {
		$.ajax({
			url: '/datafilling/getproduk',
			type: 'POST',
			dataType: 'json',
			data: {
				"_token": token,
				jenis: based
			},
		})
		.done(function(data) {
			let opt = '<option value=""></option>';
			for(let i = 0; i < data.length; i++) {
				opt += '<option value="'+ data[i].id_produk +'"';
				if (data[i].id_produk == like) {
					opt += 'selected';
				}
				opt += ' data-id="'+ data[i].relid +'">'+ data[i].produk +'</option>';
			}
			$('#' + position).html(opt);
		});
	}

	function seacrh_data() {
		getdatasearch('batangdry3');
	}

	function save_method() {
		let link = $('#form-qc').attr('action');
		let idqc = $('#idqc').val();
		let iddrying3 = $('#iddrying3').val();
		let idjnpro = $('#idjnpro').val();
		let tanggal_qc = $('#tanggal_qc').val();
		let jenis_in = $('#jenis_in').val();
		let produk_in = $('#produk_in').val();
		let stockbatang = $('#batangdry3').val();
		let batangjml = $('#batangjml').val();
		let mutasijml = $('#mutasijml').val();
		let rejfill = $('#rejfill').val();
		let rejbind = $('#rejbind').val();
		let rejwrap = $('#rejwrap').val();
		let rejlaz = $('#rejlaz').val();
		let durasi = $('#durasi').val();
		let ket = $('#ket').val();
		$.ajax({
			url: link,
			type: 'POST',
			dataType: 'json',
			data: {
				"_token": token,
				tanggal_qc: tanggal_qc,
				jenis: jenis_in,
				produk: produk_in,
				durasi:durasi,
				stockbatang: stockbatang,
				batangjml: batangjml,
				mutasijml: mutasijml,
				rejfill: rejfill,
				rejbind: rejbind,
				rejwrap: rejwrap,
				rejlaz: rejlaz,
				ket:ket,
				iddrying3: iddrying3,
				idqc: idqc,
				idjnpro:idjnpro
			},
			beforeSend: function() {
				isProcessing = true;
				$('.spinner_nekrqc').fadeIn();
			},
			success: function(data) {
				$('.spinner_nekrqc').hide();
				isProcessing = false;
				alert(data);
				Refresh_qc();
				$('#modal-qc').modal('toggle');
			}
		});
		
	}

	$(document).on('click', '#tambah-qc', function(event) {
		let clik = $(this);
		$('.modal-header #title').text('Tambah Quality Control');
		$('#form-qc').attr('action', clik.data('link'));
		$('#form-qc').find('input').val('');
		$('#form-qc').find('select').val('');
		$('#form-qc').find('textarea').val('');
		$("#tanggal_qc").datetimepicker({
			format: 'YYYY-MM-DD HH:mm:ss',
		}).data("DateTimePicker").date(new Date());
		// $('#durasi').mdtimepicker({format: 'hh:mm'});
		$('#stock_showit').fadeIn();
		$('#jenis').removeAttr('disabled');
		$('#produk').removeAttr('disabled');
		ljenis('jenis');
		$(document).on('change', '#jenis', function(event) {
			let jam = $(this).val();
			$('#jenis_in').val(jam);
			lproduk('produk',jam);
			$(document).on('change', '#produk', function(event) {
				let pwe = $(this).val();
				$('#produk_in').val(pwe);
				let kk = $(this).find(':selected').data('id');
				$('#idjnpro').val(kk);
			});
		});
		$('#modal-qc').modal('show');
	});

	$(document).on('click', '#update-fumigasi', function(event) {
		let click = $(this);
		$('.modal-header #title').text(click.data('title'));
		$('#form-qc').attr('action', click.data('link'));
		$('#form-qc').find('input').val('');
		$('#form-qc').find('select').val('');
		$('#form-qc').find('textarea').val('');
		$('#stock_showit').hide();
		$('#jenis').attr('disabled','disabled');
		$('#produk').attr('disabled','disabled');
		ljenis('jenis',click.data('jenis'));
		lproduk('produk',click.data('jenis'),click.data('produk'));

		$("#tanggal_qc").datetimepicker({
			format: 'YYYY-MM-DD HH:mm:ss',
		}).data("DateTimePicker").date(click.data('tanggal_qc'));
		// $('#durasi').mdtimepicker({format: 'hh:mm'});
		$('#jenis_in').val(click.data('jenis'));
		$('#produk_in').val(click.data('produk'));
		let kk = $('#produk_in').find(':selected').data('id');

		$('#beratperbtg').val(click.data('berat'));

		$('#idjnpro').val(kk);
		$('#batangdry3').val(click.data('stockbatang'));
		$('#batangjml').val(click.data('batang'));
		$('#mutasijml').val(click.data('mutasi'));
		$('#rejfill').val(click.data('rejfill'));
		$('#rejbind').val(click.data('rejbind'));
		$('#rejwrap').val(click.data('rejwrap'));
		$('#rejlaz').val(click.data('rejlaz'));
		$('#idqc').val(click.data('idqc'));
		$('#iddrying3').val(click.data('iddrying3'));
		$('#durasi').val(click.data('durasi'));
		$('#ket').val(click.data('ket'));
		$('#modal-qc').modal('show');
	});

	$(document).on('click', '#delete-fumigasi', function(event) {
		let knsb = confirm('Apakah anda yakin ingin menghapus data ini ?');
		if (knsb) {
			let link = $(this).data("link");
			let jenis_in = $(this).data('jenis');
			let produk_in = $(this).data('produk');
			let iddrying3 = $(this).data('iddrying3');
			let idqc = $(this).data('idqc');
			$.ajax(
			{
				url: link,
				type: 'DELETE',
				data: {
					"_token": token,
					jenis: jenis_in,
					produk: produk_in,
					iddrying3: iddrying3,
					idqc: idqc,
				},
				beforeSend: function() {
					isProcessing = true;
					$('#loadingdeleteqc').fadeIn();
				},
				success: function (data){
					$('#loadingdeleteqc').hide();
					isProcessing = false;
					alert(data);
					Refresh_qc();
				}
			});
		}
	});
</script>