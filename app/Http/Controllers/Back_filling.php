<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;
use App\Filling;
use App\Stock_filling;
use App\Stock_probaku;

class Back_filling extends Controller
{
	public function sprofill($request)
	{
		$query = DB::table('stock_probaku')
		->select('stock_probaku.id','stock_probaku.jml_produksi')
		->join('jenis','stock_probaku.jenis_id_jenis','=','jenis.id_jenis')
		->join('kategori','stock_probaku.kategori_id_kategori','=','kategori.id_kategori')
		->where([['stock_probaku.jenis_id_jenis','=',$request->jenis],['stock_probaku.kategori_id_kategori','=',1]]);

		$qusrt = DB::table('stock_filling')
		->select('stock_filling.id','stock_filling.stock_weisf')
		->join('jenis_has_produk','stock_filling.jenis_has_produk_id_jnpro','=','jenis_has_produk.id_jnpro')
		->where([['jenis_has_produk.jenis_id_jenis','=',$request->jenis],['jenis_has_produk.produk_id_produk','=',$request->produk]]);
		if ($query->count() > 0) {
			$wert = $query->first();
			$wer = $wert->jml_produksi;
			$wqd = $wert->id;
		} else { $wer = 0; $wqd = '';}
		if ($qusrt->count() > 0) {
			$aem = $qusrt->first();
			$qer = $aem->stock_weisf;
			$qwc = $aem->id;
		} else { $qer = 0; $qwc = '';}
		$array = ['jml_produksi' => $wer,'stock_weisf' => $qer,'id' => $wqd,'idfilling' => $qwc];
		return $array;
	}

	public function stockfill($request)
	{
		if ($request->idfilling != '') {
			$id = $request->idfilling; 
			$show = Stock_filling::where('id',$id)->first();
			$jml = $show->jml_fill + $request->batangjml;
			$weis = $show->stock_weisf + $request->masukweisf - $request->terpakaiweisf;

			$array = ['fill' => $jml,'weis' => $weis];
			$cek = Stock_filling::find($id);
			$cek->jml_fill = $jml;
			$cek->stock_weisf = $weis;
			$cek->save();
			$resultid = $cek->id;
		} else {
			$jml = $request->batangjml;
			$weis = $request->stockweisf + $request->masukweisf - $request->terpakaiweisf;
			$save = new Stock_filling;
			$save->jml_fill = $jml; 
			$save->stock_weisf = $weis;
			$save->jenis_has_produk_id_jnpro = $request->idjnpro;
			$save->jenis_has_produk_id_jenis = $request->jenis;
			$save->jenis_has_produk_id_produk = $request->produk;
			$save->save();
			$resultid = $save->id;
			$array = ['fill' => $jml,'weis' => $weis];
		}
		return $resultid;
	}

	public function ifuppstockfill($request,$masukweis,$terpoakaipro,$terpakaiweis,$batang)
	{
		if ($masukweis != $request->masukweisf || $terpoakaipro != $request->terpakaipro || $terpakaiweis != $request->terpakaiweisf || $batang != $request->batangjml) {
			$shbaku = Stock_probaku::where('id',$request->idprobaku)->first();
			$shfill = Stock_filling::where('id',$request->idfilling)->first();
			if ($terpoakaipro > $request->terpakaipro) {
				$whj = $terpoakaipro - $request->terpakaipro;
				$hasil = $shbaku->jml_produksi + $whj;
			} elseif ($terpoakaipro < $request->terpakaipro) {
				$whj = $request->terpakaipro - $terpoakaipro;
				$hasil = $shbaku->jml_produksi - $whj;
			} else {
				$hasil = $shbaku->jml_produksi;
			}

			if ($masukweis > $request->masukweisf) {
				$qnb = $masukweis - $request->masukweisf;
				$awl = $shfill->stock_weisf - $qnb;
			} elseif ($masukweis < $request->masukweisf) {
				$qnb = $request->masukweisf - $masukweis;
				$awl = $shfill->stock_weisf + $qnb;
			} else {
				$awl = $shfill->stock_weisf;
			}

			if ($terpakaiweis > $request->terpakaiweisf) {
				$jkw = $terpakaiweis - $request->terpakaiweisf;
				$hasile = $awl + $jkw;
			} elseif ($terpakaiweis < $request->terpakaiweisf) {
				$jkw = $request->terpakaiweisf - $terpakaiweis;
				$hasile = $awl - $jkw;
			} else {
				$hasile = $awl;
			}

			if ($batang > $request->batangjml) {
				$nah = $batang - $request->batangjml;
				$hasilw = $shfill->jml_fill - $nah;
			} elseif ($batang < $request->batangjml) {
				$nah = $request->batangjml - $batang;
				$hasilw = $shfill->jml_fill + $nah;
			} else {
				$hasilw = $shfill->jml_fill;
			}
			$query1 = Stock_probaku::where('id',$request->idprobaku)->update(['jml_produksi' => $hasil]);

			$query2 = Stock_filling::where('id',$request->idfilling)->update(['jml_fill' => $hasilw,'stock_weisf' => $hasile]);
		}
	}

	public function loop_upfill($request,$id,$terpoakaipro = '')
	{
		$ceh = Filling::where([['stock_probaku_id','=',$request->idprobaku],['stock_filling_id','=',$request->idfilling],['id','>',$id]]);
		if ($ceh->count() > 0) {
			$reg = $ceh->get();
			for ($i=0; $i < $ceh->count(); $i++) {
				$show = Filling::where([['stock_probaku_id','=',$request->idprobaku],['stock_filling_id','=',$request->idfilling],['id','<',$reg[$i]->id]])->orderBy('id','desc')->take('1');
				if ($show->count() > 0) {
					$first = $show->first();
					$jebpro = $first->sisaprof - $reg[$i]->terpakaiprof;
					$bwrpro = $first->sisaweisf + $reg[$i]->weisfmasuk - $reg[$i]->terpakaiweisf;
				} else {
					if ($terpoakaipro == '') {
						$awal = $reg[$i]->sisaprof + $reg[$i]->terpakaiprof;
						$jebpro = $awal - $reg[$i]->terpakaiprof;
						$bwrpro = 0 + $reg[$i]->weisfmasuk - $reg[$i]->terpakaiweisf;
					} else {
						$awal = $reg[$i]->sisaprof + $reg[$i]->terpakaiprof + $terpoakaipro;
						$jebpro = $awal - $reg[$i]->terpakaiprof;
						$bwrpro = 0 + $reg[$i]->weisfmasuk - $reg[$i]->terpakaiweisf;
					}
				}
				$update = Filling::find($reg[$i]->id);
				$update->sisaprof = $jebpro;
				$update->sisaweisf = $bwrpro;
				$update->save();
			}
		}
	}

}
