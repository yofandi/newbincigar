@extends('../../layout')

@section('title')
<title>BIN - ERP : Notifikasi Detail Bagan</title>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css" />
@endsection

@section('content')
<div class="row page-titles">
	{{-- Bagan: {{ $isi->level }}; User: {{ $isi->username }}; --}}
	<div class="col-md-5 align-self-center">
		<h3 class="text-themecolor">Notifikasi Detail</h3>
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a href="javascript:void(0)">Notifikasi</a></li>
			<li class="breadcrumb-item"><a href="{{ route('noc_rfsbagan.list') }}">List Notifikasi Bagan</a></li>
			<li class="breadcrumb-item active"><a href="javascript:void(0)">Notifikasi Detail</a></li>
		</ol>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-body">
				<div class="row">
					<div class="col-md-3">
						<legend align="center">
							@if (in_array(Auth::user()->level_user_id_level, [6]))
							@switch($isi->verifikasi_rfs)
							@case(0)
							Order Masuk
							@break

							@case(1)
							Order Acc
							@break

							@case(2)
							Barang Keluar
							@break
							@default
							@endswitch
							@elseif(in_array(Auth::user()->level_user_id_level, [7,8,9,10,11]))
							@switch($isi->verifikasi_rfs)
							@case(0)
							Order Barang
							@break

							@case(1)
							Order Acc
							@break

							@case(2)
							Barang Masuk
							@break
							@default
							@endswitch
							@else 
							Barang
							@endif
						</legend>
					</div>
					<div class="col-md-2 offset-md-7">
						@if ($isi->verifikasi_rfs != 2)
						<button type="button" id="optklick" class="btn btn-success btn-rounded btn-icons"><i class="fa fa-plus" aria-hidden="true"></i></button>
						@endif
					</div>
					<div class="col-md-12" align="center" id="loadingprosess">
						<h5>Data tengah diproses. Mohon tunggu sejenak!</h5>
					</div>
					<div class="col-md-12" id="tambahshow">
						<form id="form-tambahsub">
							<div class="row">
								<div class="col-md-12" align="center">
									<legend>Tambah Sub Produk</legend>
								</div>
								<div class="form-group col-md-6">
									<label class="control-label">Produk</label>
									<select class="form-control" id="produk_in"></select>
								</div>
								<div class="form-group col-md-6">
									<label class="control-label">Sub Produk</label>
									<select class="form-control" id="subproduk_in"></select>
								</div>
								<div class="form-group col-md-12">
									<button type="button" id="cari-data" class="btn btn-primary btn-xs btn-block">Cari</button>
								</div>
								<div class="form-group col-md-6">
									<label class="control-label">Jumlah Stock</label>
									<input type="number" class="form-control" id="stockjmlrfs" placeholder="Jumlah Stock" readonly>
								</div>
								<div class="form-group col-md-6">
									<label class="control-label">Jumlah Keluar</label>
									<input type="number" class="form-control" id="jml_keluar" placeholder="JML Keluar">
								</div>
								<div class="form-group col-md-12">
									<button type="button" id="tambah_itemsobject" class="btn btn-warning btn-sm btn-block">Tambah</button>
								</div>
							</div>
						</form>
					</div>
					<div class="table-responsive">
						<table class="display nowrap table table-hover table-striped table-bordered" id="items_order" width="100%">
							<thead>
								<tr class="table-primary">
									<th>No.</th>
									<th>Name</th>
									<th>Quantity</th>
									<th class="hidden">#</th>
								</tr>
							</thead>
						</table>
					</div>
				</div>
				<form class="row" method="POST" action="{{ route('noc_rfsbagan.detail', $isi->id) }}">
					@csrf
					<div class="form-group col-md-12">
						<label class="control-label">Tanggal</label>
						<input type="text" class="form-control" id="tanggal_order" name="tanggal_order" placeholder="Tanggal" value="{{ $isi->tanggal_order }}" @if (!in_array(Auth::user()->level_user_id_level, $klasifikasi) && !in_array($isi->verifikasi_rfs, [0,1]) || $isi->status_terima == 1)
						readonly 
						@endif>
					</div>
					<div class="form-group col-md-6">
						<label class="control-label">Level Bagan</label>
						<select class="form-control" name="level" id="level" 
						@if (!in_array(Auth::user()->level_user_id_level, $klasifikasi) && !in_array($isi->verifikasi_rfs, [0,1]) || $isi->status_terima == 1))
						disabled 
						@endif>
						<option value=""></option>
						@foreach ($level as $element)
						<option value="{{ $element->id }}"
							@if ($element->id == $isi->level_user_id_level)
							selected 
							@endif
							>{{ $element->level }}</option>
							@endforeach
						</select>

						<input type="hidden" name="level_in" id="level_in" value="{{ $isi->level_user_id_level }}">
					</div>
					<div class="form-group col-md-6">
						<label class="control-label">User Bagan</label>
						<select class="form-control" name="userbagan" id="userbagan">
						</select>

						<input type="hidden" name="userbagan_in" id="userbagan_in" value="{{ $isi->users_id }}">
					</div>
					<div class="form-group col-md-6">
						<label class="control-label">Status</label>
						<select class="form-control" name="verifikasi_rfs" id="verifikasi_rfs">
						</select>

						<input type="hidden" name="verifikasi_rfs_in" id="verifikasi_rfs_in" value="{{ $isi->verifikasi_rfs }}">
					</div>
					<div class="form-group col-md-6">
						<label class="control-label">Serah Tangan</label>
						<select class="form-control" name="status_terima" id="status_terima">
						</select>

						<input type="hidden" name="status_terima_in" id="status_terima_in" value="{{ $isi->status_terima }}">
					</div>
					<div class="form-group col-md-12">
						<label class="control-label">Keterangan</label>
						<textarea class="form-control" name="ket_order" id="ket_order" placeholder="Keterangan" @if (!in_array(Auth::user()->level_user_id_level, $klasifikasi) && !in_array($isi->verifikasi_rfs, [0,1]) || $isi->status_terima == 1)
							readonly 
							@endif>{{ $isi->keterangan_order }}</textarea>
						</div>
					</div>
					<div class="form-group col-md-12" align="right">
						<button type="submit" class="btn btn-info btn-lg waves-effect" 
						@if (!in_array(Auth::user()->level_user_id_level, $klasifikasi) && !in_array($isi->verifikasi_rfs, [0,1]) || $isi->status_terima == 1))
						disabled 
						@endif
						>Save</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	@endsection

	@section('javascript')
	<script src="{{ asset('node_modules/moment/moment.js') }}">
	</script><script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
	<script src="{{ asset('node_modules/datatables/datatables.min.js') }}"></script>
	@include('home.notification.script_notif_rfsbagan')
	@endsection