<script>
	$('#stock_showit').hide();
	$('#loadingdeletedry1').hide();
	$('.spinner_nekrdry1').hide();

	var tabledry1 = $('#table-drying1').DataTable({
		"lengthMenu": [
		[3,5,10, 25, 50, 100, 150, 200, -1],
		[3,5,10, 25, 50, 100, 150, 200, "All"]
		],
		"order": [
		[0, 'desc']
		],
		processing: true,
		serverSide: true,
		ajax: '/datadrying1/json',
		columns: [
		{ data: 'DT_RowIndex', name: 'DT_RowIndex' },
		{ data: 'tanggal_dry1', name: 'tanggal_dry1' },
		{ data: 'produk', name: 'produk' },
		{ data: 'jenis', name: 'jenis' },
		{ data: 'lama_dry1', name: 'lama_dry1' },
		{ data: 'awlbatang', name: 'awlbatang' },
		{ data: 'tambah_dry1', name: 'tambah_dry1' },
		{ data: 'hasil_akhirdry1', name: 'hasil_akhirdry1' },
		{ data: 'keterangan_dry1', name: 'keterangan_dry1' },
		{
			data: null,
			"bSortable": false,
			mRender: function (o) { 
				return '<button type="button" class="btn btn-info btn-rounded btn-xs" id="update-drying1" data-title="Update Drying 1 | ID: '+ o.id +'" data-link="/datadrying1/upp/'+ o.id +'/'+ o.tambah_dry1 +'" data-tanggal_drying1="'+ o.tanggal_dry1 +'" data-jenis="'+ o.id_jenis +'" data-produk="'+ o.id_produk +'" data-durasi="'+ o.lama_dry1 +'" data-ket="'+ o.keterangan_dry1 +'" data-stockbatang="'+ o.awlbatang +'" data-batang="'+ o.tambah_dry1 +'" data-iddrying1="'+ o.stock_drying1_id +'" data-idwrapping="'+ o.stock_wrapping_id +'"><i class="mdi mdi-dots-horizontal"></i></button><button type="button" class="btn btn-danger btn-rounded btn-xs" id="delete-drying1" data-link="/datadrying1/del/'+ o.id +'/'+ o.tambah_dry1 +'" data-iddrying1="'+ o.stock_drying1_id +'" data-idwrapping="'+ o.stock_wrapping_id +'"><i class="fa fa-trash"></i></button>'; }
			},
			],
			"displayLength": 10,
		});
	$('#table-drying1 tbody').on('click', 'tr.group', function() {
		var currentOrder = tabledry1.order()[0];
		if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
			tabledry1.order([2, 'desc']).draw();
		} else {
			tabledry1.order([2, 'asc']).draw();
		}
	});
	$('#table-drying1 tbody').on('click', 'tr.group', function() {
		var currentOrder = tabledry1.order()[0];
		if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
			tabledry1.order([2, 'desc']).draw();
		} else {
			tabledry1.order([2, 'asc']).draw();
		}
	});

	function getdatasearchdry1(position1) {
		let jenis = $('#jenis_indry1').val();
		let produk = $('#produk_indry1').val();

		$.ajax({
			url: '/datadrying1/getsearchfill',
			type: 'POST',
			dataType: 'json',
			data: {
				"_token": token,
				produk: produk,
				jenis: jenis
			},
		})
		.done(function(data) {
			$('#idwrappingdry1').val(data.idwrapping);
			$('#iddrying1').val(data.iddrying1);
			$('#' + position1).val(data.jml_wrap);
		});
		
	}

	function seacrh_datadry1() {
		getdatasearchdry1('batangwrap');
	}

	function Refresh_drying1() {
		$('#table-drying1').DataTable().ajax.reload();
	}

	function save_methoddry1() {
		let link = $('#form-drying1').attr('action');
		let idwrapping = $('#idwrappingdry1').val();
		let iddrying1 = $('#iddrying1').val();
		let idjnpro = $('#idjnprodry1').val();
		let tanggal_drying1 = $('#tanggal_drying1').val();
		let jenis_in = $('#jenis_indry1').val();
		let produk_in = $('#produk_indry1').val();
		let stockbatang = $('#batangwrap').val();
		let batangjml = $('#batangjmldry1').val();
		let durasi = $('#durasidry1').val();
		let ket = $('#ketdry1').val();
		$.ajax({
			url: link,
			type: 'POST',
			dataType: 'json',
			data: {
				"_token": token,
				tanggal_drying1: tanggal_drying1,
				jenis: jenis_in,
				produk: produk_in,
				durasi:durasi,
				stockbatang: stockbatang,
				batangjml: batangjml,
				ket:ket,
				iddrying1: iddrying1,
				idwrapping: idwrapping,
				idjnpro:idjnpro
			},
			beforeSend: function() {
				isProcessing = true;
				$('.spinner_nekrdry1').fadeIn();
			},
			success: function(data) {
				$('.spinner_nekrdry1').hide();
				isProcessing = false;
				alert(data);
				Refresh_drying1();
				$('#modal-drying1').modal('toggle');
			}
		});
		
	}

	$(document).on('click', '#tambah-drying1', function(event) {
		let clik = $(this);
		$('.modal-header #title').text('Tambah Drying 1');
		$('#form-drying1').attr('action', clik.data('link'));
		$('#form-drying1').find('input').val('');
		$('#form-drying1').find('select').val('');
		$('#form-drying1').find('textarea').val('');
		$("#tanggal_drying1").datetimepicker({
			format: 'YYYY-MM-DD HH:mm:ss',
		}).data("DateTimePicker").date(new Date());
		// $('#durasidry1').mdtimepicker({format: 'hh:mm'});
		$('#stock_showit').fadeIn();
		$('#jenisdry1').removeAttr('disabled');
		$('#produkdry1').removeAttr('disabled');
		ljenis('jenisdry1');
		$(document).on('change', '#jenisdry1', function(event) {
			let jam = $(this).val();
			$('#jenis_indry1').val(jam);
			lproduk('produkdry1',jam);
			$(document).on('change', '#produkdry1', function(event) {
				let pwe = $(this).val();
				$('#produk_indry1').val(pwe);
				let kk = $(this).find(':selected').data('id');
				$('#idjnprodry1').val(kk);
			});
		});
		$('#modal-drying1').modal('toggle');
	});

	$(document).on('click', '#update-drying1', function(event) {
		let click = $(this);
		$('.modal-header #title').text(click.data('title'));
		$('#form-drying1').attr('action', click.data('link'));
		$('#form-drying1').find('input').val('');
		$('#form-drying1').find('select').val('');
		$('#form-drying1').find('textarea').val('');
		$('#stock_showit').hide();
		$('#jenisdry1').attr('disabled','disabled');
		$('#produkdry1').attr('disabled','disabled');
		ljenis('jenisdry1',click.data('jenis'));
		lproduk('produkdry1',click.data('jenis'),click.data('produk'));

		$("#tanggal_drying1").datetimepicker({
			format: 'YYYY-MM-DD HH:mm:ss',
		}).data("DateTimePicker").date(click.data('tanggal_drying1'));
		// $('#durasidry1').mdtimepicker({format: 'hh:mm'});
		$('#jenis_indry1').val(click.data('jenis'));
		$('#produk_indry1').val(click.data('produk'));
		$('#batangwrap').val(click.data('stockbatang'));
		$('#batangjmldry1').val(click.data('batang'));
		$('#idwrappingdry1').val(click.data('idwrapping'));
		$('#iddrying1').val(click.data('iddrying1'));
		$('#durasidry1').val(click.data('durasi'));
		$('#ketdry1').val(click.data('ket'));
		$('#modal-drying1').modal('toggle');
	});

	$(document).on('click', '#delete-drying1', function(event) {
		let hsja = confirm('Apakah anda yakin ingin menghapus data ini ?');
		if (hsja) {
			let link = $(this).data("link");
			let iddrying1 = $(this).data('iddrying1');
			let idwrapping = $(this).data('idwrapping');
			$.ajax(
			{
				url: link,
				type: 'DELETE',
				data: {
					"_token": token,
					iddrying1: iddrying1,
					idwrapping: idwrapping,
				},
				beforeSend: function() {
					isProcessing = true;
					$('#loadingdeletedry1').fadeIn();
				},
				success: function (data){
					$('#loadingdeletedry1').hide();
					isProcessing = false;
					alert(data);
					Refresh_drying1();
				}
			});
		}
	});
</script>