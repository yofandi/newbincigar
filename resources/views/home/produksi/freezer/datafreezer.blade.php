@extends('../../layout')

@section('title')
<title>BIN - ERP : Data Frezzer</title>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/dmuy/MDTimePicker/mdtimepicker.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css" />
@endsection

@section('content')
<div class="row page-titles">
	<div class="col-md-5 align-self-center">
		<h3 class="text-themecolor">Frezzer</h3>
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a href="javascript:void(0)">Proses</a></li>
			<li class="breadcrumb-item active">Data Frezzer</li>
		</ol>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="row">
			<div class="col-md-6">
				<legend>Data Frezzer</legend>
			</div>
			<div class="col-md-2 offset-md-4">
				<button type="button" data-link="{{ route('freezer.post') }}" class="btn btn-success btn-rounded btn-icons " id="tambah-frezzer">Tambah</button>
			</div>
			<div class="table-responsive">
				<table id="table-frezzer" width="100%" class="display nowrap table table-hover table-striped table-bordered">
					<thead>
						<tr>
							<th>No.</th>
							<th>Tanggal</th>
							<th>Produk</th>
							<th>Jenis</th>
							<th>Durasi</th>
							<th>Batang Drying1</th>
							<th>Tambah Batang</th>
							<th>JML</th>
							<th>Ket</th>
							<th>Aksi</th>
						</tr>
					</thead>
				</table>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="modal-frezzer" tabindex="-1" role="dialog" aria-labelledby="title-tembakau" aria-hidden="true" style="display: none;">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="card">
				<div class="card-header">
					<div class="modal-header">
						<h4 class="modal-title" id="title"></h4>
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					</div>
				</div>
				<div class="card-body">
					<form id="form-frezzer" method="POST">
						<div class="modal-body row">
							<div class="form-group col-md-12">
								<label class="control-label">Tanggal</label>
								<input type="text" class="form-control date_sam" id="tanggal_frezzer">
							</div>
							<div class="form-group col-md-6">
								<label class="control-label">Jenis</label>
								<select class="form-control" id="jenisfrez"></select>
								<input type="hidden" id="jenis_infrez">
							</div>
							<div class="form-group col-md-6">
								<label class="control-label">Produk</label>
								<select class="form-control" id="produkfrez"></select>
								<input type="hidden" id="produk_infrez">
							</div>
							<div class="form-group col-md-12" id="stock_showitffrez">
								<button type="button" onclick="seacrh_datafrez()" class="btn btn-primary btn-xs btn-block">Search</button>
								<input type="hidden" id="iddrying1frez">
								<input type="hidden" id="idfrezzer">
								<input type="hidden" id="idjnprofrez">
							</div>
							<div class="form-group col-md-12">
								<label class="control-label">Batang Drying1</label>
								<input type="number" class="form-control" id="batangdry1" placeholder="Jml Batang Drying1" readonly>
							</div>
							<div class="form-group col-md-12">
								<label class="control-label">Batang Cerutu Yang Dihasilkan</label>
								<input type="number" class="form-control" id="batangjmlfrez" placeholder="Jml Batang">
							</div>
							<div class="form-group col-md-12">
								<label class="control-label">Lama Proses</label>
								<input type="text" class="form-control" id="durasifrez" placeholder="Durasi">
							</div>
							<div class="form-group col-md-12">
								<label class="control-label">Keterangan</label>
								<textarea class="form-control" id="ketfrez" placeholder="Keterangan"></textarea>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" onclick="save_methodfrez()" class="btn btn-primary waves-effect text-left">Save</button>
							<button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Close</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('javascript')
<script src="{{ asset('node_modules/moment/moment.js') }}">
</script><script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
<script src="{{ asset('node_modules/datatables/datatables.min.js') }}"></script>
<script src="https://cdn.jsdelivr.net/gh/dmuy/MDTimePicker/mdtimepicker.js
"></script>
<script>
	$('#stock_showitffrez').hide();
	let token = $("meta[name='csrf-token']").attr("content");

	const jenis = {!! json_encode($jenis) !!};
	jenis.unshift({id_jenis: '',jenis: ''});

	var table = $('#table-frezzer').DataTable({
		"lengthMenu": [
		[3,5,10, 25, 50, 100, 150, 200, -1],
		[3,5,10, 25, 50, 100, 150, 200, "All"]
		],
		"order": [
		[0, 'desc']
		],
		processing: true,
		serverSide: true,
		ajax: '/datafreezer/json',
		columns: [
		{ data: 'DT_RowIndex', name: 'DT_RowIndex' },
		{ data: 'tanggal_fre', name: 'tanggal_fre' },
		{ data: 'produk', name: 'produk' },
		{ data: 'jenis', name: 'jenis' },
		{ data: 'lama_fre', name: 'lama_fre' },
		{ data: 'awlbatang', name: 'awlbatang' },
		{ data: 'tambah_fre', name: 'tambah_fre' },
		{ data: 'hasil_akhirfre', name: 'hasil_akhirfre' },
		{ data: 'keterengan_fre', name: 'keterengan_fre' },
		{
			data: null,
			"bSortable": false,
			mRender: function (o) { 
				return '<button type="button" class="btn btn-info btn-rounded btn-xs" id="update-frezzer" data-title="Update Frezzer | ID: '+ o.id +'" data-link="/datafreezer/upp/'+ o.id +'/'+ o.tambah_fre +'" data-tanggal_frezzer="'+ o.tanggal_fre +'" data-jenis="'+ o.id_jenis +'" data-produk="'+ o.id_produk +'" data-durasi="'+ o.lama_fre +'" data-ket="'+ o.keterengan_fre +'" data-stockbatang="'+ o.awlbatang +'" data-batang="'+ o.tambah_fre +'" data-idfrezzer="'+ o.stock_frezer_id +'" data-iddrying1="'+ o.stock_drying1_id +'"><i class="mdi mdi-dots-horizontal"></i></button><button type="button" class="btn btn-danger btn-rounded btn-xs" id="delete-frezzer" data-link="/datafreezer/del/'+ o.id +'/'+ o.tambah_fre +'" data-idfrezzer="'+ o.stock_frezer_id +'" data-iddrying1="'+ o.stock_drying1_id +'"><i class="fa fa-trash"></i></button>'; }
			},
			],
			"displayLength": 10,
		});
	$('#table-frezzer tbody').on('click', 'tr.group', function() {
		var currentOrder = table.order()[0];
		if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
			table.order([2, 'desc']).draw();
		} else {
			table.order([2, 'asc']).draw();
		}
	});
	$('#table-frezzer tbody').on('click', 'tr.group', function() {
		var currentOrder = table.order()[0];
		if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
			table.order([2, 'desc']).draw();
		} else {
			table.order([2, 'asc']).draw();
		}
	});

	function getdatasearchfrez(position1) {
		let jenis = $('#jenis_infrez').val();
		let produk = $('#produk_infrez').val();

		$.ajax({
			url: '/datafreezer/getsearchfill',
			type: 'POST',
			dataType: 'json',
			data: {
				"_token": token,
				produk: produk,
				jenis: jenis
			},
		})
		.done(function(data) {
			$('#iddrying1frez').val(data.iddrying1);
			$('#idfrezzer').val(data.idfrezzer);
			$('#' + position1).val(data.jml_dry1);
		});
		
	}

	function ljenis(position,like) {
		let opt = '';
		for (let i = 0; i < jenis.length; i++) {
			opt += '<option value="'+ jenis[i].id_jenis +'"';
			if (jenis[i].id_jenis == like) {
				opt += 'selected';
			}
			opt += '>'+ jenis[i].jenis +'</option>';
		}
		$('#' + position).html(opt);
	}

	function lproduk(position,based,like) {
		$.ajax({
			url: '/datafilling/getproduk',
			type: 'POST',
			dataType: 'json',
			data: {
				"_token": token,
				jenis: based
			},
		})
		.done(function(data) {
			let opt = '<option value=""></option>';
			for(let i = 0; i < data.length; i++) {
				opt += '<option value="'+ data[i].id_produk +'"';
				if (data[i].id_produk == like) {
					opt += 'selected';
				}
				opt += ' data-id="'+ data[i].relid +'">'+ data[i].produk +'</option>';
			}
			$('#' + position).html(opt);
		});
	}

	function seacrh_datafrez() {
		getdatasearchfrez('batangdry1');
	}

	function save_methodfrez() {
		let link = $('#form-frezzer').attr('action');
		let iddrying1 = $('#iddrying1frez').val();
		let idfrezzer = $('#idfrezzer').val();
		let idjnpro = $('#idjnprofrez').val();
		let tanggal_frezzer = $('#tanggal_frezzer').val();
		let jenis_in = $('#jenis_infrez').val();
		let produk_in = $('#produk_infrez').val();
		let stockbatang = $('#batangdry1').val();
		let batangjml = $('#batangjmlfrez').val();
		let durasi = $('#durasifrez').val();
		let ket = $('#ketfrez').val();
		$.ajax({
			url: link,
			type: 'POST',
			dataType: 'json',
			data: {
				"_token": token,
				tanggal_frezzer: tanggal_frezzer,
				jenis: jenis_in,
				produk: produk_in,
				durasi:durasi,
				stockbatang: stockbatang,
				batangjml: batangjml,
				ket:ket,
				idfrezzer: idfrezzer,
				iddrying1: iddrying1,
				idjnpro:idjnpro
			},
		})
		.done(function(data) {
			alert(data);
			// console.log(data);
			$('#table-frezzer').DataTable().ajax.reload();
			$('#modal-frezzer').modal('toggle');
		});
		
	}

	$(document).on('click', '#tambah-frezzer', function(event) {
		let clik = $(this);
		$('.modal-header #title').text('Tambah Frezzer');
		$('#form-frezzer').attr('action', clik.data('link'));
		$('#form-frezzer').find('input').val('');
		$('#form-frezzer').find('select').val('');
		$('#form-frezzer').find('textarea').val('');
		$("#tanggal_frezzer").datetimepicker({
			format: 'YYYY-MM-DD HH:mm:ss',
		}).data("DateTimePicker").date(new Date());
		$('#durasifrez').mdtimepicker({format: 'hh:mm'});
		$('#stock_showitffrez').fadeIn();
		$('#jenisfrez').removeAttr('disabled');
		$('#produkfrez').removeAttr('disabled');
		ljenis('jenisfrez');
		$(document).on('change', '#jenisfrez', function(event) {
			let jam = $(this).val();
			$('#jenis_infrez').val(jam);
			lproduk('produkfrez',jam);
			$(document).on('change', '#produkfrez', function(event) {
				let pwe = $(this).val();
				$('#produk_infrez').val(pwe);
				let kk = $(this).find(':selected').data('id');
				$('#idjnprofrez').val(kk);
			});
		});
		$('#modal-frezzer').modal('show');
	});

	$(document).on('click', '#update-frezzer', function(event) {
		let click = $(this);
		$('.modal-header #title').text(click.data('title'));
		$('#form-frezzer').attr('action', click.data('link'));
		$('#form-frezzer').find('input').val('');
		$('#form-frezzer').find('select').val('');
		$('#form-frezzer').find('textarea').val('');
		$('#stock_showitffrez').hide();
		$('#jenisfrez').attr('disabled','disabled');
		$('#produkfrez').attr('disabled','disabled');
		ljenis('jenisfrez',click.data('jenis'));
		lproduk('produkfrez',click.data('jenis'),click.data('produk'));

		$("#tanggal_frezzer").datetimepicker({
			format: 'YYYY-MM-DD HH:mm:ss',
		}).data("DateTimePicker").date(click.data('tanggal_frezzer'));
		$('#durasifrez').mdtimepicker({format: 'hh:mm'});
		$('#jenis_infrez').val(click.data('jenis'));
		$('#produk_infrez').val(click.data('produk'));
		$('#batangdry1').val(click.data('stockbatang'));
		$('#batangjmlfrez').val(click.data('batang'));
		$('#iddrying1frez').val(click.data('iddrying1'));
		$('#idfrezzer').val(click.data('idfrezzer'));
		$('#durasifrez').val(click.data('durasi'));
		$('#ketfrez').val(click.data('ket'));
		$('#modal-frezzer').modal('show');
	});

	$(document).on('click', '#delete-frezzer', function(event) {
		let dnk = confirm('Apakah anda yakin ingin menghapus data ini ?');
		if (dnk) {
			let link = $(this).data("link");
			let idfrezzer = $(this).data('idfrezzer');
			let iddrying1 = $(this).data('iddrying1');
			$.ajax(
			{
				url: link,
				type: 'DELETE',
				data: {
					"_token": token,
					idfrezzer: idfrezzer,
					iddrying1: iddrying1,
				},
				success: function (data){
					alert(data);
					$('#table-frezzer').DataTable().ajax.reload();
				}
			});
		}
	});
</script>
@endsection