<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class History_stiker extends Model
{
    protected $table = 'history_stiker';

    protected $fillable = [
    	'stock_stiker_id_stock_stiker',
        'stock_stiker_sub_produk_id',
    	'stock_stiker_produk_id_produk',
        'harga_stiker_id',
        'packing_id',
    	'masuk_luar',
    	'pakai_luar',
    	'hasil_luar',
    	'masuk_dalam',
    	'pakai_dalam',
    	'hasil_dalam',
    	'tanggal_hisstik',
    	'ket_sti',
    	'author_session',
    	'created_at',
    	'updated_at',
    	'log',
    ];
}
