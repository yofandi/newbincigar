<script>
	$('#stock_showitffrez').hide();
	$('#loadingdeletefrez').hide();
	$('.spinner_nekrfrez').hide();

	var tablefrez = $('#table-frezzer').DataTable({
		"lengthMenu": [
		[3,5,10, 25, 50, 100, 150, 200, -1],
		[3,5,10, 25, 50, 100, 150, 200, "All"]
		],
		"order": [
		[0, 'desc']
		],
		processing: true,
		serverSide: true,
		ajax: '/datafreezer/json',
		columns: [
		{ data: 'DT_RowIndex', name: 'DT_RowIndex' },
		{ data: 'tanggal_fre', name: 'tanggal_fre' },
		{ data: 'produk', name: 'produk' },
		{ data: 'jenis', name: 'jenis' },
		{ data: 'lama_fre', name: 'lama_fre' },
		{ data: 'awlbatang', name: 'awlbatang' },
		{ data: 'tambah_fre', name: 'tambah_fre' },
		{ data: 'hasil_akhirfre', name: 'hasil_akhirfre' },
		{ data: 'keterengan_fre', name: 'keterengan_fre' },
		{
			data: null,
			"bSortable": false,
			mRender: function (o) { 
				return '<button type="button" class="btn btn-info btn-rounded btn-xs" id="update-frezzer" data-title="Update Frezzer | ID: '+ o.id +'" data-link="/datafreezer/upp/'+ o.id +'/'+ o.tambah_fre +'" data-tanggal_frezzer="'+ o.tanggal_fre +'" data-jenis="'+ o.id_jenis +'" data-produk="'+ o.id_produk +'" data-durasi="'+ o.lama_fre +'" data-ket="'+ o.keterengan_fre +'" data-stockbatang="'+ o.awlbatang +'" data-batang="'+ o.tambah_fre +'" data-idfrezzer="'+ o.stock_frezer_id +'" data-iddrying1="'+ o.stock_drying1_id +'"><i class="mdi mdi-dots-horizontal"></i></button><button type="button" class="btn btn-danger btn-rounded btn-xs" id="delete-frezzer" data-link="/datafreezer/del/'+ o.id +'/'+ o.tambah_fre +'" data-idfrezzer="'+ o.stock_frezer_id +'" data-iddrying1="'+ o.stock_drying1_id +'"><i class="fa fa-trash"></i></button>'; }
			},
			],
			"displayLength": 10,
		});
	$('#table-frezzer tbody').on('click', 'tr.group', function() {
		var currentOrder = tablefrez.order()[0];
		if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
			tablefrez.order([2, 'desc']).draw();
		} else {
			table.order([2, 'asc']).draw();
		}
	});
	$('#table-frezzer tbody').on('click', 'tr.group', function() {
		var currentOrder = tablefrez.order()[0];
		if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
			tablefrez.order([2, 'desc']).draw();
		} else {
			tablefrez.order([2, 'asc']).draw();
		}
	});

	function Refresh_frezzer() {
		$('#table-frezzer').DataTable().ajax.reload();
	}

	function getdatasearchfrez(position1) {
		let jenis = $('#jenis_infrez').val();
		let produk = $('#produk_infrez').val();

		$.ajax({
			url: '/datafreezer/getsearchfill',
			type: 'POST',
			dataType: 'json',
			data: {
				"_token": token,
				produk: produk,
				jenis: jenis
			},
		})
		.done(function(data) {
			$('#iddrying1frez').val(data.iddrying1);
			$('#idfrezzer').val(data.idfrezzer);
			$('#' + position1).val(data.jml_dry1);
		});
		
	}

	function seacrh_datafrez() {
		getdatasearchfrez('batangdry1');
	}

	function save_methodfrez() {
		let link = $('#form-frezzer').attr('action');
		let iddrying1 = $('#iddrying1frez').val();
		let idfrezzer = $('#idfrezzer').val();
		let idjnpro = $('#idjnprofrez').val();
		let tanggal_frezzer = $('#tanggal_frezzer').val();
		let jenis_in = $('#jenis_infrez').val();
		let produk_in = $('#produk_infrez').val();
		let stockbatang = $('#batangdry1').val();
		let batangjml = $('#batangjmlfrez').val();
		let durasi = $('#durasifrez').val();
		let ket = $('#ketfrez').val();
		$.ajax({
			url: link,
			type: 'POST',
			dataType: 'json',
			data: {
				"_token": token,
				tanggal_frezzer: tanggal_frezzer,
				jenis: jenis_in,
				produk: produk_in,
				durasi:durasi,
				stockbatang: stockbatang,
				batangjml: batangjml,
				ket:ket,
				idfrezzer: idfrezzer,
				iddrying1: iddrying1,
				idjnpro:idjnpro
			},
			beforeSend: function() {
				isProcessing = true;
				$('.spinner_nekrfrez').fadeIn();
			},
			success: function(data) {
				$('.spinner_nekrfrez').hide();
				isProcessing = false;
				alert(data);
				Refresh_frezzer();
				$('#modal-frezzer').modal('toggle');
			}
		});
		
	}

	$(document).on('click', '#tambah-frezzer', function(event) {
		let clik = $(this);
		$('.modal-header #title').text('Tambah Frezzer');
		$('#form-frezzer').attr('action', clik.data('link'));
		$('#form-frezzer').find('input').val('');
		$('#form-frezzer').find('select').val('');
		$('#form-frezzer').find('textarea').val('');
		$("#tanggal_frezzer").datetimepicker({
			format: 'YYYY-MM-DD HH:mm:ss',
		}).data("DateTimePicker").date(new Date());
		// $('#durasifrez').mdtimepicker({format: 'hh:mm'});
		$('#stock_showitffrez').fadeIn();
		$('#jenisfrez').removeAttr('disabled');
		$('#produkfrez').removeAttr('disabled');
		ljenis('jenisfrez');
		$(document).on('change', '#jenisfrez', function(event) {
			let jam = $(this).val();
			$('#jenis_infrez').val(jam);
			lproduk('produkfrez',jam);
			$(document).on('change', '#produkfrez', function(event) {
				let pwe = $(this).val();
				$('#produk_infrez').val(pwe);
				let kk = $(this).find(':selected').data('id');
				$('#idjnprofrez').val(kk);
			});
		});
		$('#modal-frezzer').modal('show');
	});

	$(document).on('click', '#update-frezzer', function(event) {
		let click = $(this);
		$('.modal-header #title').text(click.data('title'));
		$('#form-frezzer').attr('action', click.data('link'));
		$('#form-frezzer').find('input').val('');
		$('#form-frezzer').find('select').val('');
		$('#form-frezzer').find('textarea').val('');
		$('#stock_showitffrez').hide();
		$('#jenisfrez').attr('disabled','disabled');
		$('#produkfrez').attr('disabled','disabled');
		ljenis('jenisfrez',click.data('jenis'));
		lproduk('produkfrez',click.data('jenis'),click.data('produk'));

		$("#tanggal_frezzer").datetimepicker({
			format: 'YYYY-MM-DD HH:mm:ss',
		}).data("DateTimePicker").date(click.data('tanggal_frezzer'));
		// $('#durasifrez').mdtimepicker({format: 'hh:mm'});
		$('#jenis_infrez').val(click.data('jenis'));
		$('#produk_infrez').val(click.data('produk'));
		$('#batangdry1').val(click.data('stockbatang'));
		$('#batangjmlfrez').val(click.data('batang'));
		$('#iddrying1frez').val(click.data('iddrying1'));
		$('#idfrezzer').val(click.data('idfrezzer'));
		$('#durasifrez').val(click.data('durasi'));
		$('#ketfrez').val(click.data('ket'));
		$('#modal-frezzer').modal('show');
	});

	$(document).on('click', '#delete-frezzer', function(event) {
		let dnk = confirm('Apakah anda yakin ingin menghapus data ini ?');
		if (dnk) {
			let link = $(this).data("link");
			let idfrezzer = $(this).data('idfrezzer');
			let iddrying1 = $(this).data('iddrying1');
			$.ajax(
			{
				url: link,
				type: 'DELETE',
				data: {
					"_token": token,
					idfrezzer: idfrezzer,
					iddrying1: iddrying1,
				},
				beforeSend: function() {
					isProcessing = true;
					$('#loadingdeletefrez').fadeIn();
				},
				success: function (data){
					$('#loadingdeletefrez').hide();
					isProcessing = false;
					alert(data);
					Refresh_frezzer();
				}
			});
		}
	});
</script>