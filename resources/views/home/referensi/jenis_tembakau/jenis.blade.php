@extends('../../layout')

@section('title')
<title>BIN - ERP : Jenis Tembakau</title>
<link type="text/css" rel="stylesheet" href="{{ asset('node_modules/jsgrid/jsgrid.min.css') }}" />
<link type="text/css" rel="stylesheet" href="{{ asset('node_modules/jsgrid/jsgrid-theme.min.css') }}" />
<style type="text/css">
	.hide
	{
		display:none;
	}
</style>
@endsection

@section('content')
<div class="row page-titles">
	<div class="col-md-5 align-self-center">
		<h3 class="text-themecolor">Jenis Tembakau</h3>
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a href="javascript:void(0)">Referensi</a></li>
			<li class="breadcrumb-item active">Jenis</li>
		</ol>
	</div>
</div>
<div class="row">
	<div class="col-md-12 m-t-30">
		<div class="card">
			<div class="card-body">
				<h4 class="card-title">Jenis Tembakau</h4>
				<div class="row">
					<div class="col-md-8"></div>
					<div class="col-md-2 offset-md-2">
						<button type="button" onclick="Refresh()" class="btn btn-xs btn-icons btn-secondary">Refresh</button>
					</div>
				</div>
				<div id="grid_jntembakau"></div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('javascript')
<script type="text/javascript" src="{{ asset('node_modules/jsgrid/jsgrid.min.js') }}"></script>
<script>
	let token = $("meta[name='csrf-token']").attr("content");
	jsGrid.setDefaults({
		tableClass: "jsgrid-table table table-striped table-hover"
	});
	jsGrid.setDefaults("text", {
		_createTextBox: function() {
			return $("<input>").attr("type", "text").attr("class", "form-control input-sm")
		}
	});
	jsGrid.setDefaults("number", {
		_createTextBox: function() {
			return $("<input>").attr("type", "number").attr("class", "form-control input-sm")
		}
	});
	jsGrid.setDefaults("textarea", {
		_createTextBox: function() {
			return $("<input>").attr("type", "textarea").attr("class", "form-control")
		}
	});
	jsGrid.setDefaults("control", {
		_createGridButton: function(cls, tooltip, clickHandler) {
			var grid = this._grid;
			return $("<button>").addClass(this.buttonClass).addClass(cls).attr({
				type: "button",
				title: tooltip
			}).on("click", function(e) {
				clickHandler(grid, e)
			})
		}
	});
	jsGrid.setDefaults("select", {
		_createSelect: function() {
			var $result = $("<select>").attr("class", "form-control input-sm"),
			valueField = this.valueField,
			textField = this.textField,
			selectedIndex = this.selectedIndex;
			return $.each(this.items, function(index, item) {
				var value = valueField ? item[valueField] : index,
				text = textField ? item[textField] : item,
				$option = $("<option>").attr("value", value).text(text).appendTo($result);
				$option.prop("selected", selectedIndex === index)
			}), $result
		}
	});
	$('#grid_jntembakau').jsGrid({
		height: "500px",
		width: "100%",
		filtering: true,
		filterable:true,
		editing: true,
		inserting: true,
		sorting: true,
		paging: true,
		autoload: true,
		pageSize: 15,
		pageButtonCount: 5,
		deleteConfirm: "Apakah anda yakin ingin menghapus data ini ?",
		controller: {
			loadData: function(filter){
				return $.ajax({
					type: "GET",
					dataType: 'json',
					url: "/jenis/json",
					data: filter
				});
			},
			insertItem: function(item){
				item._token = $('meta[name="csrf-token"]').attr('content');
				return $.ajax({
					type: "POST",
					url: "/jenis/add",
					data:item
				});
			},
			updateItem: function(item){
				item._token = $('meta[name="csrf-token"]').attr('content');
				return $.ajax({
					type: "PUT",
					url: "/jenis/put",
					data: item
				});
			},
			deleteItem: function(item){
				item._token = $('meta[name="csrf-token"]').attr('content');
				return $.ajax({
					type: "DELETE",
					url: "/jenis/del",
					data: item
				});
			},
		},

		fields: [
		{
			name: "id_jenis",
			title: "ID",
			type: "hidden",
			css: 'hide',
			width: 50
		},
		{
			name: "kode_jenis",
			title: "Kode Jenis Tembakau",
			type: "text", 
			width: 50
		},
		{
			name: "jenis",
			title: "Jenis Tembakau",
			type: "text", 
			width: 50
		},
		{
			name: "deskripsi_jenis",
			title: "Deskripsi Jenis",
			type: "text", 
			width: 50
		},
		{
			name: "keterangan_jenis",
			title: "Keterangan",
			type: "text", 
			width: 50
		},
		{
			type: "control"
		}
		]
	});

	
	function Refresh() {
		$("#grid_jntembakau").jsGrid("loadData");
	}
</script>
@endsection