<div class="row" id="load_hvn">
	<div class="form-group col-md-4">
		<label class="control-label">Kemasan</label>
		<input type="text" class="form-control" id="kemasanhvn" placeholder="Kemasan" readonly>

		<input type="hidden" id="idkemasanhvn">
		<input type="hidden" id="idpackinghvn">
		<input type="hidden" id="idstokemasanhvn">
		<input type="hidden" id="idcincinhvn">
		<input type="hidden" id="idstikerhvn">
	</div>
	<div class="form-group col-md-4">
		<label class="control-label">Stock Kemasan</label>
		<input type="number" class="form-control" id="stockkemasanhvn" placeholder="Stock Kemasan" readonly>
	</div>
	<div class="form-group col-md-4">
		<label class="control-label">Stock Cincin</label>
		<input type="number" class="form-control" id="stockcincinhvn" placeholder="Stock Cincin" readonly>
	</div>
	<div class="form-group col-md-12">
		<label class="control-label">Stock Stiker</label>
		<div class="row">
			<div class="col-md-6">
				*luar
				<input type="number" class="form-control" id="stockluarstihvn" placeholder="Stock Stiker Luar" readonly>
			</div>
			<div class="col-md-6">
				*dalam
				<input type="number" class="form-control" id="stockdalamstihvn" placeholder="Stock Stiker Dalam" readonly>
			</div>
		</div>
	</div>
	<div class="form-group col-md-12" id="batanghavano">
		<label class="control-label">Batang Quality Control</label>
		<div class="row">
			<div class="col-md-3">
				*Robusto
				<input type="number" class="form-control" id="batangrbs_hvn" placeholder="Jml Robusto Quality Control" readonly>
			</div>
			<div class="col-md-2">
				*Corona
				<input type="number" class="form-control" id="batangcor_hvn" placeholder="Jml Corona Quality Control" readonly>
			</div>
			<div class="col-md-2">
				*Half Corona
				<input type="number" class="form-control" id="batanghlf_hvn" placeholder="Jml Half Corona Quality Control" readonly>
			</div>
			<div class="col-md-2">
				*Cigar Master
				<input type="number" class="form-control" id="batangcgr_hvn" placeholder="Jml Cigar Master Quality Control" readonly>
			</div>
			<div class="col-md-2">
				*El Nino
				<input type="number" class="form-control" id="batangelnino_hvn" placeholder="Jml El Nino Quality Control" readonly>
			</div>
		</div>
	</div>
	<div class="form-group col-md-12">
		<label class="control-label">Stock Awal</label>
		<input type="number" class="form-control" id="stockawalhvn" placeholder="Stock Awal" readonly>
	</div>
	<div class="form-group col-md-5">
		<label class="control-label">Packing</label>
		<input type="number" class="form-control" id="jmlpackinghvn" placeholder="JML Packing">
	</div>
	<div class="form-group col-md-2" align="center">
		<br>
		<label class="control-label">X</label>
	</div>
	<div class="form-group col-md-5">
		<label class="control-label">Isi/Kemasan</label>
		<input type="text" class="form-control" id="isikemasanhvn" placeholder="Isi Kemasan" readonly>
		*Robusto: 3; *Corona: 3; *Half Corona: 3; *Cigar Master: 3; *El Nino: 4;
	</div><div class="form-group col-md-6">
		<label class="control-label">Terpakai Stiker Luar</label>
		<input type="number" class="form-control" id="terpakailuarhvn" placeholder="Stiker Terpakai Luar">
	</div>
	<div class="form-group col-md-6">
		<label class="control-label">Terpakai Stiker Baru</label>
		<input type="number" class="form-control" id="terpakaidalamhvn" placeholder="Stiker Terpakai Baru">
	</div>
	<div class="form-group col-md-6">
		<label class="control-label">Terpakai Kemasan</label>
		<input type="number" class="form-control" id="terpakaikemasanhvn" placeholder="Terpakai Kemasan">
	</div>
	<div class="form-group col-md-6">
		<label class="control-label">Terpakai Cincin</label>
		<input type="number" class="form-control" id="terpakaicincinhvn" placeholder="Terpakai Cincin">
	</div>
	<div class="form-group col-md-12">
		<label class="control-label">Keterangan</label>
		<textarea class="form-control" id="ket_hvn" placeholder="Keterangan"></textarea>
	</div>
</div>