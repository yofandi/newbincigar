<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use DataTables;
use Auth;
use App\Drying2;
use App\Stock_frezer;
use App\Stock_drying2;

use App\Http\Controllers\Back_drying2;

class Drying2cont extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->produksi = $res = new Back_drying2;
    }
    public function searchweiprodry2(Request $request)
    {
        $yevs = $this->produksi->sprodry2($request);
        echo json_encode($yevs);
    }

    public function getdatadry2()
    {
        $query = DB::table('drying2')
        ->select('drying2.*','jenis.id_jenis','produk.id_produk','jenis.jenis','produk.produk')
        ->join('stock_drying2','drying2.stock_drying2_id','=','stock_drying2.id')
        ->join('jenis','stock_drying2.jenis_has_produk_id_jenis','=','jenis.id_jenis')
        ->join('produk','stock_drying2.jenis_has_produk_id_produk','=','produk.id_produk')
        ->get();
        return Datatables::of($query)
        ->addIndexColumn()
        ->addColumn('awlbatang', function ($query) {
            $awl = $query->hasil_akhirdry2 + $query->tambah_dry2;
            return $awl;
        })
        ->make(true);
    }

    public function datadrying2()
    {
        $data['jenis'] = DB::table('jenis')->select('id_jenis','jenis')->get();
        return view('home.produksi.drying2.datadrying2',$data);
    }

    public function adddrying2(Request $request)
    {
        DB::beginTransaction();

        $qq = $this->produksi->stockdry2($request);

        $data = [
            'tambah_dry2' => $request->batangjml,
            'hasil_akhirdry2' => $qq['jml_fre'],
            'lama_dry2' => $request->durasi,
            'keterangan_dry2' => $request->ket,
            'tanggal_dry2' => $request->tanggal_drying2,
            'stock_frezer_id' => $qq['idfrezzer'],
            'stock_drying2_id' => $qq['iddrying2'],
            'author_session' => Auth::user()->id,
            'created_at' => date('Y-m-d H:i:s'),
            'log' => "INSERT"
        ];
        $query1 = Drying2::insert($data);
        if ($query1) {
            $alert = 'Drying 2 berhasil ditambahkan!';
        } else {
            $alert = 'Drying 2 gagal ditambahkan!';
        }
        echo json_encode($alert);
        DB::commit();
    }

    public function uppdrying2(Request $request,$id,$batang)
    {
        DB::beginTransaction();
        $jwty = $request->stockbatang - $request->batangjml;
        $this->produksi->ifuppstockdry2($request,$batang);

        $data = [
            'tambah_dry2' => $request->batangjml,
            'hasil_akhirdry2' => $jwty,
            'lama_dry2' => $request->durasi,
            'keterangan_dry2' => $request->ket,
            'tanggal_dry2' => $request->tanggal_drying2,
            'author_session' => Auth::user()->id,
            'log' => "UPDATE"
        ];
        $query1 = Drying2::where('id',$id)->update($data);
        if ($query1) {
            $this->produksi->loop_updry2($request,$id);
            $alert = 'Drying 2 ID: '.$id.' berhasil diupdate!';
        } else {
            $alert = 'Drying 2 ID: '.$id.' gagal diupdate!';
        }
        echo json_encode($alert);
        DB::commit();
    }

    public function deldrying2(Request $request,$id,$batang)
    {
        DB::beginTransaction();
        $werty = Stock_frezer::where('id',$request->idfrezzer)->first();
        $qusrt = Stock_drying2::where('id',$request->iddrying2)->first();

        $batangfre = $werty->jml_fre + $batang;
        $batangdry2 = $qusrt->jml_dry2 - $batang;

        $update2 = Stock_frezer::find($request->idfrezzer);
        $update2->jml_fre = $batangfre;
        $update2->save();
        $update3 = Stock_drying2::find($request->iddrying2);
        $update3->jml_dry2 = $batangdry2;
        $update3->save();

        $query1 = Drying2::where('id',$id)->delete();
        if ($query1) {
            $this->produksi->loop_updry2($request,$id,$batang);
            $alert = 'Drying 2 ID : '.$id.' berhasil dihapus!';
        } else {
            $alert = 'Drying 2 ID : '.$id.' gagal dihapus!';
        }
        echo json_encode($alert);
        DB::commit();      
    }

}
