<script>
	let token = $("meta[name='csrf-token']").attr("content");
	$('.spinner_nekr').hide();
	$('#hiddenval').hide();
	$('#loadingdelete').hide();
	const produk1 = {!! json_encode($produk) !!};
	produk1.unshift({id_produk:'',produk:'Pilih'});

	var table = $('#table-cukai').DataTable({
		"lengthMenu": [
		[3,5,10, 25, 50, 100, 150, 200, -1],
		[3,5,10, 25, 50, 100, 150, 200, "All"]
		],
		"order": [
		[0, 'desc']
		],
		processing: true,
		serverSide: true,
		ajax: '/cukai/json',
		columns: [
		{ data: 'DT_RowIndex', name: 'DT_RowIndex' },
		{ data: 'produk', name: 'produk' },
		{ data: 'sub_kode', name: 'sub_kode' },
		{ data: 'sub_produk', name: 'sub_produk' },
		{ data: 'stock_qty_cukai', name: 'stock_qty_cukai' },
		{	
			data: null,
			"bSortable": false,
			mRender: function(o) {
				return '<button type="button" class="btn btn-xs btn-rounded btn-warning update-cukai" data-title="Update Cukai | ID Sub: '+ o.sub_produk_id_sub_produk +'" data-link="/cukai/update/'+ o.id_stock_cukai +'" data-produk="'+ o.id_produk +'" data-subproduk="'+ o.id_sub_produk +'" data-stock="'+ o.stock_qty_cukai +'"><i class="mdi mdi-dots-horizontal"></i></button><button type="button" id="delete-cukai" class="btn btn-danger btn-rounded btn-xs" data-id="'+ o.id_stock_cukai +'"><i class="fa fa-trash"></i></button>'; 
			}
		},
		],
		"displayLength": 10,
	});
	$('#table-cukai tbody').on('click', 'tr.group', function() {
		var currentOrder = table.order()[0];
		if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
			table.order([2, 'desc']).draw();
		} else {
			table.order([2, 'asc']).draw();
		}
	});
	$('#table-cukai tbody').on('click', 'tr.group', function() {
		var currentOrder = table.order()[0];
		if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
			table.order([2, 'desc']).draw();
		} else {
			table.order([2, 'asc']).draw();
		}
	});

	function Refresh() {
		table.search('').draw();
	}

	function lproduk(position,like = '') {
		let opt = '';
		for (var i = 0; i < produk1.length; i++) {
			opt += '<option value="'+ produk1[i].id_produk +'"';
			if (produk1[i].id_produk == like) { opt += 'selected'; }
			opt += '>'+ produk1[i].produk +'</option>';
		}
		$('#' + position).html(opt);
	}

	function lsubpro(position,base,like = '') {
		$.ajax({
			url: '/cukai/subproduk/json',
			type: 'POST',
			dataType: 'json',
			data: {
				"_token": token,
				produk: base
			},
		})
		.done(function(data) {
			let opt = '<option value="">Pilih Sub</option>';
			for (var i = 0; i < data.length; i++) {
				opt += '<option value="'+ data[i].id_sub_produk +'"';
				if (data[i].id_sub_produk == like) { opt += 'selected'; }
				opt += '>'+ data[i].sub_kode +' - '+ data[i].sub_produk +'</option>';
			}
			$('#' + position).html(opt);		
		});
	}

	function save_method() {
		let link = $('#form-cukai').attr('action');
		let produk = $('#produk_in').val();
		let sub_produk = $('#sub_produk_in').val();
		let stock = $('#stock').val();
		$.ajax({
			url: link,
			type: 'POST',
			dataType: 'json',
			data: {
				"_token": token,
				produk: produk,
				sub_produk: sub_produk,
				stock: stock,
				harga_cukai: harga_cukai,
			},
			beforeSend: function() {
				isProcessing = true;
				$('.spinner_nekr').fadeIn();
			},
			success: function(data) {
				$('.spinner_nekr').hide();
				isProcessing = false;
				alert(data);
				$('#table-cukai').DataTable().ajax.reload();
				$('#modal-cukai').modal('toggle');
			}
		});
	}

	$(document).on('click', '#tambah-cukai', function(event) {
		let clik = $(this);
		$('#hiddenval').hide();
		$('.modal-header #title').text('Tambah Cukai');
		$('#form-cukai').find('input').val('');
		$('#form-cukai').find('select').val('');
		$('#form-cukai').attr('action', clik.data('link'));
		$('#produk').removeAttr('disabled');
		$('#sub_produk').removeAttr('disabled');
		lproduk('produk');
		$(document).on('change', '#produk', function(event) {
			let val = $(this).val();
			$('#produk_in').val(val);
			$('#harga_cukai').val('');
			lsubpro('sub_produk',val);
			$(document).on('change', '#sub_produk', function(event) {
				let cal = $(this).val();
				$('#sub_produk_in').val(cal);
			});
		});
		$('#modal-cukai').modal('show');
	});

	$(document).on('click', '.update-cukai', function(event) {
		let clik = $(this);
		$('#hiddenval').hide();
		$('.modal-header #title').text(clik.data('title'));
		$('#form-cukai').find('input').val('');
		$('#form-cukai').find('select').val('');
		$('#form-cukai').attr('action', clik.data('link'));
		$('#produk_in').val(clik.data('produk'));
		$('#sub_produk_in').val(clik.data('subproduk'));
		lproduk('produk',clik.data('produk'));
		lsubpro('sub_produk',clik.data('produk'),clik.data('subproduk'));
		$('#stock').val(clik.data('stock'));
		$('#produk').attr('disabled','disabled');
		$('#sub_produk').attr('disabled','disabled');
		$('#modal-cukai').modal('show');
	});

	$(document).on('click', '#delete-cukai', function(event) {
		let dk = confirm('Apakah anda yakin ingin menghapus data ini ?');
		if (dk) {
			
			let id = $(this).data("id");
			$.ajax(
			{
				url: '/cukai/delete/' + id,
				type: 'DELETE',
				data: {
					"_token": token,
				},
				beforeSend: function() { 
					isProcessing = true;
					$('#loadingdelete').fadeIn();
				},
				success: function (data){
					$('#loadingdelete').hide();
					isProcessing = false;
					alert(data);
					$('#table-cukai').DataTable().ajax.reload();
				}
			});
		}
	});
</script>