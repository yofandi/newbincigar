@extends('../../layout')

@section('title')
<title>BIN - ERP : Data Quality Control</title>
{{-- <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/dmuy/MDTimePicker/mdtimepicker.css"> --}}
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css" />
@endsection

@section('content')
<div class="row page-titles">
	<div class="col-md-5 align-self-center">
		<h3 class="text-themecolor">Quality Control</h3>
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a href="javascript:void(0)">Proses</a></li>
			<li class="breadcrumb-item active">Data Quality Control</li>
		</ol>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="row">
			<div class="col-md-6">
				<legend>Data Quality Control</legend>
			</div>
			<div class="col-md-2 offset-md-4">
				<button type="button" data-link="{{ route('qc.post') }}" class="btn btn-success btn-rounded btn-icons " id="tambah-qc">Tambah</button>
				<button type="button" onclick="Refresh_qc()" class="btn btn-rounded btn-icons btn-secondary">Refresh</button>
			</div>
			<div class="col-md-12" align="center" id="loadingdeleteqc">
				<h5>Data tengah diproses. Mohon tunggu sejenak!</h5>
			</div>
			<div class="table-responsive m-t-40">
				<table id="table-qc" class="display nowrap table table-hover table-striped table-bordered">
					<thead>
						<tr align="center">
							<th rowspan="2">No.</th>
							<th rowspan="2">Tanggal</th>
							<th rowspan="2">Produk</th>
							<th rowspan="2">Jenis</th>
							<th rowspan="2">Durasi</th>
							<th rowspan="2">Batang Humidor</th>
							<th rowspan="2">Accept</th>
							<th rowspan="2">Mutasi</th>
							<th colspan="4" class="table-secondary">Reject (*Gram)</th>
							<th rowspan="2">JML</th>
							<th rowspan="2">Ket</th>
							<th rowspan="2">Aksi</th>
						</tr>
						<tr>
							<th class="table-success">Filler</th>
							<th class="table-danger">Lazio</th>
							<th class="table-info">Omblad</th>
							<th class="table-warning">Deklad</th>
						</tr>
					</thead>
				</table>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="modal-qc" tabindex="-1" role="dialog" aria-labelledby="title-tembakau" aria-hidden="true" style="display: none;">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="card">
				<div class="card-header">
					<div class="modal-header">
						<h4 class="modal-title" id="title"></h4>
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					</div>
				</div>
				<div class="card-body">
					<form id="form-qc" method="POST">
						<div class="modal-body row">
							<div class="form-group col-md-12 fa-3x spinner_nekrqc" align="center">
								<i class="fas fa-spinner fa-spin"></i>
							</div>
							<div class="form-group col-md-12">
								<label class="control-label">Tanggal</label>
								<input type="text" class="form-control date_sam" id="tanggal_qc">
							</div>
							<div class="form-group col-md-6">
								<label class="control-label">Jenis</label>
								<select class="form-control" id="jenis"></select>
								<input type="hidden" id="jenis_in">
							</div>
							<div class="form-group col-md-6">
								<label class="control-label">Produk</label>
								<select class="form-control" id="produk"></select>
								<input type="hidden" id="produk_in">
							</div>
							<div class="form-group col-md-12" id="stock_showit">
								<button type="button" onclick="seacrh_data()" class="btn btn-primary btn-xs btn-block">Search</button>
								<input type="hidden" id="idqc">
								<input type="hidden" id="iddrying3">
								<input type="hidden" id="idjnpro">
							</div>
							<div class="form-group col-md-6">
								<label class="control-label">Batang Drying 3</label>
								<input type="number" class="form-control" id="batangdry3" placeholder="Jml Batang Drying 3" readonly>
							</div>
							<div class="form-group col-md-6">
								<label class="control-label">Berat perbatang</label>
								<input type="number" class="form-control" step="any" id="beratperbtg" placeholder="Berat perbatang" readonly>
							</div>
							<div class="form-group col-md-6">
								<label class="control-label">Accept</label>
								<input type="number" class="form-control" id="batangjml" placeholder="Jml Batang">
							</div>
							<div class="form-group col-md-6">
								<label class="control-label">Mutasi</label>
								<input type="number" class="form-control" id="mutasijml" placeholder="Jml Batang">
							</div>
							<div class="form-group col-md-3">
								<label class="control-label">Reject Filling</label>
								<input type="number" class="form-control" step="any" id="rejfill" placeholder="Jml Gram">
								<small>*Otomatis dikali 2</small>
							</div>
							<div class="form-group col-md-3">
								<label class="control-label">Reject Lazio</label>
								<input type="number" class="form-control" step="any" id="rejlaz" placeholder="Jml Gram">
								<small> *Satuan Reject: Gram</small>
							</div>
							<div class="form-group col-md-3">
								<label class="control-label">Reject Binding</label>
								<input type="number" class="form-control" step="any" id="rejbind" placeholder="Jml Gram">
							</div>
							<div class="form-group col-md-3">
								<label class="control-label">Reject Wrapping</label>
								<input type="number" class="form-control" step="any" id="rejwrap" placeholder="Jml Gram">
							</div>
							<div class="form-group col-md-12">
								<label class="control-label">Lama Proses</label>
								<input type="text" class="form-control" id="durasi" placeholder="Durasi">
							</div>
							<div class="form-group col-md-12">
								<label class="control-label">Keterangan</label>
								<textarea class="form-control" id="ket" placeholder="Keterangan"></textarea>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" onclick="save_method()" class="btn btn-primary waves-effect text-left">Save</button>
							<button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Close</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('javascript')
<script src="{{ asset('node_modules/moment/moment.js') }}">
</script><script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
<script src="{{ asset('node_modules/datatables/datatables.min.js') }}"></script>
{{-- <script src="https://cdn.jsdelivr.net/gh/dmuy/MDTimePicker/mdtimepicker.js --}}
"></script>
@include('home.produksi.qc.script_dataqc')
@endsection