@extends('../../layout')

@section('title')
<title>BIN - ERP : Data Drying 1</title>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/dmuy/MDTimePicker/mdtimepicker.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css" />
@endsection

@section('content')
<div class="row page-titles">
	<div class="col-md-5 align-self-center">
		<h3 class="text-themecolor">Drying 1</h3>
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a href="javascript:void(0)">Proses</a></li>
			<li class="breadcrumb-item active">Data Drying 1</li>
		</ol>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="row">
			<div class="col-md-6">
				<legend>Data Drying 1</legend>
			</div>
			<div class="col-md-2 offset-md-4">
				<button type="button" data-link="{{ route('drying1.post') }}" class="btn btn-success btn-rounded btn-icons " id="tambah-drying1">Tambah</button>
			</div>
			<div class="table-responsive m-t-40">
				<table id="table-drying1" width="100%" class="display nowrap table table-hover table-striped table-bordered">
					<thead>
						<tr>
							<th>No.</th>
							<th>Tanggal</th>
							<th>Produk</th>
							<th>Jenis</th>
							<th>Durasi</th>
							<th>Batang Wrapping</th>
							<th>Tambah Batang</th>
							<th>JML</th>
							<th>Ket</th>
							<th>Aksi</th>
						</tr>
					</thead>
				</table>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="modal-drying1" tabindex="-1" role="dialog" aria-labelledby="title-tembakau" aria-hidden="true" style="display: none;">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="card">
				<div class="card-header">
					<div class="modal-header">
						<h4 class="modal-title" id="title"></h4>
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					</div>
				</div>
				<div class="card-body">
					<form id="form-drying1" method="POST">
						<div class="modal-body row">
							<div class="form-group col-md-12">
								<label class="control-label">Tanggal</label>
								<input type="text" class="form-control date_sam" id="tanggal_drying1">
							</div>
							<div class="form-group col-md-6">
								<label class="control-label">Jenis</label>
								<select class="form-control" id="jenisdry1"></select>
								<input type="hidden" id="jenis_indry1">
							</div>
							<div class="form-group col-md-6">
								<label class="control-label">Produk</label>
								<select class="form-control" id="produkdry1"></select>
								<input type="hidden" id="produk_indry1">
							</div>
							<div class="form-group col-md-12" id="stock_showit">
								<button type="button" onclick="seacrh_datadry1()" class="btn btn-primary btn-xs btn-block">Search</button>
								<input type="hidden" id="idwrappingdry1">
								<input type="hidden" id="iddrying1">
								<input type="hidden" id="idjnprodry1">
							</div>
							<div class="form-group col-md-12">
								<label class="control-label">Batang Wrapping</label>
								<input type="number" class="form-control" id="batangwrap" placeholder="Jml Batang Wrapping" readonly>
							</div>
							<div class="form-group col-md-12">
								<label class="control-label">Batang Cerutu Yang Dihasilkan</label>
								<input type="number" class="form-control" id="batangjmldry1" placeholder="Jml Batang">
							</div>
							<div class="form-group col-md-12">
								<label class="control-label">Lama Proses</label>
								<input type="text" class="form-control" id="durasidry1" placeholder="Durasi">
							</div>
							<div class="form-group col-md-12">
								<label class="control-label">Keterangan</label>
								<textarea class="form-control" id="ketdry1" placeholder="Keterangan"></textarea>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" onclick="save_methoddry1()" class="btn btn-primary waves-effect text-left">Save</button>
							<button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Close</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('javascript')
<script src="{{ asset('node_modules/moment/moment.js') }}">
</script><script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
<script src="{{ asset('node_modules/datatables/datatables.min.js') }}"></script>
<script src="https://cdn.jsdelivr.net/gh/dmuy/MDTimePicker/mdtimepicker.js
"></script>
<script>
	$('#stock_showit').hide();
	let token = $("meta[name='csrf-token']").attr("content");

	const jenis = {!! json_encode($jenis) !!};
	jenis.unshift({id_jenis: '',jenis: ''});

	var table = $('#table-drying1').DataTable({
		"lengthMenu": [
		[3,5,10, 25, 50, 100, 150, 200, -1],
		[3,5,10, 25, 50, 100, 150, 200, "All"]
		],
		"order": [
		[0, 'desc']
		],
		processing: true,
		serverSide: true,
		ajax: '/datadrying1/json',
		columns: [
		{ data: 'DT_RowIndex', name: 'DT_RowIndex' },
		{ data: 'tanggal_dry1', name: 'tanggal_dry1' },
		{ data: 'produk', name: 'produk' },
		{ data: 'jenis', name: 'jenis' },
		{ data: 'lama_dry1', name: 'lama_dry1' },
		{ data: 'awlbatang', name: 'awlbatang' },
		{ data: 'tambah_dry1', name: 'tambah_dry1' },
		{ data: 'hasil_akhirdry1', name: 'hasil_akhirdry1' },
		{ data: 'keterangan_dry1', name: 'keterangan_dry1' },
		{
			data: null,
			"bSortable": false,
			mRender: function (o) { 
				return '<button type="button" class="btn btn-info btn-rounded btn-xs" id="update-drying1" data-title="Update Drying 1 | ID: '+ o.id +'" data-link="/datadrying1/upp/'+ o.id +'/'+ o.tambah_dry1 +'" data-tanggal_drying1="'+ o.tanggal_dry1 +'" data-jenis="'+ o.id_jenis +'" data-produk="'+ o.id_produk +'" data-durasi="'+ o.lama_dry1 +'" data-ket="'+ o.keterangan_dry1 +'" data-stockbatang="'+ o.awlbatang +'" data-batang="'+ o.tambah_dry1 +'" data-iddrying1="'+ o.stock_drying1_id +'" data-idwrapping="'+ o.stock_wrapping_id +'"><i class="mdi mdi-dots-horizontal"></i></button><button type="button" class="btn btn-danger btn-rounded btn-xs" id="delete-drying1" data-link="/datadrying1/del/'+ o.id +'/'+ o.tambah_dry1 +'" data-iddrying1="'+ o.stock_drying1_id +'" data-idwrapping="'+ o.stock_wrapping_id +'"><i class="fa fa-trash"></i></button>'; }
			},
			],
			"displayLength": 10,
		});
	$('#table-drying1 tbody').on('click', 'tr.group', function() {
		var currentOrder = table.order()[0];
		if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
			table.order([2, 'desc']).draw();
		} else {
			table.order([2, 'asc']).draw();
		}
	});
	$('#table-drying1 tbody').on('click', 'tr.group', function() {
		var currentOrder = table.order()[0];
		if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
			table.order([2, 'desc']).draw();
		} else {
			table.order([2, 'asc']).draw();
		}
	});

	function getdatasearchdry1(position1) {
		let jenis = $('#jenis_indry1').val();
		let produk = $('#produk_indry1').val();

		$.ajax({
			url: '/datadrying1/getsearchfill',
			type: 'POST',
			dataType: 'json',
			data: {
				"_token": token,
				produk: produk,
				jenis: jenis
			},
		})
		.done(function(data) {
			$('#idwrappingdry1').val(data.idwrapping);
			$('#iddrying1').val(data.iddrying1);
			$('#' + position1).val(data.jml_wrap);
		});
		
	}

	function ljenis(position,like) {
		let opt = '';
		for (let i = 0; i < jenis.length; i++) {
			opt += '<option value="'+ jenis[i].id_jenis +'"';
			if (jenis[i].id_jenis == like) {
				opt += 'selected';
			}
			opt += '>'+ jenis[i].jenis +'</option>';
		}
		$('#' + position).html(opt);
	}

	function lproduk(position,based,like) {
		$.ajax({
			url: '/datafilling/getproduk',
			type: 'POST',
			dataType: 'json',
			data: {
				"_token": token,
				jenis: based
			},
		})
		.done(function(data) {
			let opt = '<option value=""></option>';
			for(let i = 0; i < data.length; i++) {
				opt += '<option value="'+ data[i].id_produk +'"';
				if (data[i].id_produk == like) {
					opt += 'selected';
				}
				opt += ' data-id="'+ data[i].relid +'">'+ data[i].produk +'</option>';
			}
			$('#' + position).html(opt);
		});
	}

	function seacrh_datadry1() {
		getdatasearchdry1('batangwrap');
	}

	function save_methoddry1() {
		let link = $('#form-drying1').attr('action');
		let idwrapping = $('#idwrappingdry1').val();
		let iddrying1 = $('#iddrying1').val();
		let idjnpro = $('#idjnprodry1').val();
		let tanggal_drying1 = $('#tanggal_drying1').val();
		let jenis_in = $('#jenis_indry1').val();
		let produk_in = $('#produk_indry1').val();
		let stockbatang = $('#batangwrap').val();
		let batangjml = $('#batangjmldry1').val();
		let durasi = $('#durasidry1').val();
		let ket = $('#ketdry1').val();
		$.ajax({
			url: link,
			type: 'POST',
			dataType: 'json',
			data: {
				"_token": token,
				tanggal_drying1: tanggal_drying1,
				jenis: jenis_in,
				produk: produk_in,
				durasi:durasi,
				stockbatang: stockbatang,
				batangjml: batangjml,
				ket:ket,
				iddrying1: iddrying1,
				idwrapping: idwrapping,
				idjnpro:idjnpro
			},
		})
		.done(function(data) {
			alert(data);
			// console.log(data);
			$('#table-drying1').DataTable().ajax.reload();
			$('#modal-drying1').modal('toggle');
		});
		
	}

	$(document).on('click', '#tambah-drying1', function(event) {
		let clik = $(this);
		$('.modal-header #title').text('Tambah Drying 1');
		$('#form-drying1').attr('action', clik.data('link'));
		$('#form-drying1').find('input').val('');
		$('#form-drying1').find('select').val('');
		$('#form-drying1').find('textarea').val('');
		$("#tanggal_drying1").datetimepicker({
			format: 'YYYY-MM-DD HH:mm:ss',
		}).data("DateTimePicker").date(new Date());
		$('#durasidry1').mdtimepicker({format: 'hh:mm'});
		$('#stock_showit').fadeIn();
		$('#jenisdry1').removeAttr('disabled');
		$('#produkdry1').removeAttr('disabled');
		ljenis('jenisdry1');
		$(document).on('change', '#jenisdry1', function(event) {
			let jam = $(this).val();
			$('#jenis_indry1').val(jam);
			lproduk('produkdry1',jam);
			$(document).on('change', '#produkdry1', function(event) {
				let pwe = $(this).val();
				$('#produk_indry1').val(pwe);
				let kk = $(this).find(':selected').data('id');
				$('#idjnprodry1').val(kk);
			});
		});
		$('#modal-drying1').modal('toggle');
	});

	$(document).on('click', '#update-drying1', function(event) {
		let click = $(this);
		$('.modal-header #title').text(click.data('title'));
		$('#form-drying1').attr('action', click.data('link'));
		$('#form-drying1').find('input').val('');
		$('#form-drying1').find('select').val('');
		$('#form-drying1').find('textarea').val('');
		$('#stock_showit').hide();
		$('#jenisdry1').attr('disabled','disabled');
		$('#produkdry1').attr('disabled','disabled');
		ljenis('jenisdry1',click.data('jenis'));
		lproduk('produkdry1',click.data('jenis'),click.data('produk'));

		$("#tanggal_drying1").datetimepicker({
			format: 'YYYY-MM-DD HH:mm:ss',
		}).data("DateTimePicker").date(click.data('tanggal_drying1'));
		$('#durasidry1').mdtimepicker({format: 'hh:mm'});
		$('#jenis_indry1').val(click.data('jenis'));
		$('#produk_indry1').val(click.data('produk'));
		$('#batangwrap').val(click.data('stockbatang'));
		$('#batangjmldry1').val(click.data('batang'));
		$('#idwrappingdry1').val(click.data('idwrapping'));
		$('#iddrying1').val(click.data('iddrying1'));
		$('#durasidry1').val(click.data('durasi'));
		$('#ketdry1').val(click.data('ket'));
		$('#modal-drying1').modal('toggle');
	});

	$(document).on('click', '#delete-drying1', function(event) {
		let hsja = confirm('Apakah anda yakin ingin menghapus data ini ?');
		if (hsja) {
			let link = $(this).data("link");
			let iddrying1 = $(this).data('iddrying1');
			let idwrapping = $(this).data('idwrapping');
			$.ajax(
			{
				url: link,
				type: 'DELETE',
				data: {
					"_token": token,
					iddrying1: iddrying1,
					idwrapping: idwrapping,
				},
				success: function (data){
					alert(data);
					$('#table-drying1').DataTable().ajax.reload();
				}
			});
		}
	});
</script>
@endsection