<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use DataTables;
use Auth;
use App\Binding;
use App\Stock_filling;
use App\Stock_binding;
use App\Stock_probaku;

use App\Http\Controllers\Back_binding;

class Bindingcont extends Controller
{
	public function __construct()
	{
		$this->middleware('auth');
		$this->produksi = $res = new Back_binding;
	}
	
	public function searchweiprobind(Request $request)
	{
		$iert = $this->produksi->sprobind($request);
		echo json_encode($iert);
	}

	public function getdatabind()
	{
		$query = DB::table('binding')
		->select('binding.*','jenis.id_jenis','produk.id_produk','jenis.jenis','produk.produk')
		->join('stock_binding','binding.stock_binding_id','=','stock_binding.id')
		->join('jenis','stock_binding.jenis_has_produk_id_jenis','=','jenis.id_jenis')
		->join('produk','stock_binding.jenis_has_produk_id_produk','=','produk.id_produk')
		->get();
		return Datatables::of($query)
		->addIndexColumn()
		->addColumn('stockawal', function ($query) {
			$awl = $query->sisaprob + $query->terpakaiprob;
			return $awl;
		})
		->addColumn('stockawalweis', function ($query) {
			$awl = $query->sisaweisb + $query->terpakaiweisb - $query->weisbmasuk;
			return $awl;
		})
		->addColumn('awlbatang', function ($query) {
			$awl = $query->hasil_filling + $query->terpakai_filling;
			return $awl;
		})
		->make(true);
	}

	public function databinding()
	{
		$data['jenis'] = DB::table('jenis')->select('id_jenis','jenis')->get();
		return view('home.produksi.binding.databinding',$data);
	}

	public function addbinding(Request $request)
	{
		DB::beginTransaction();
		$hpro = $request->stockpro - $request->terpakaipro;
		$qq = $this->produksi->stockbind($request);

		$eql = DB::table('stock_probaku')->where([['jenis_id_jenis','=',$request->jenis],['kategori_id_kategori','=',3]])->update(['jml_produksi' => $hpro]);

		$data = [
			'weisbmasuk' => $request->masukweisb,
			'terpakaiprob' => $request->terpakaipro,
			'terpakaiweisb' => $request->terpakaiweisb,
			'sisaprob' => $hpro,
			'sisaweisb' => $qq['stockwies'],
			'terpakai_filling' => $request->terpakaifilling,
			'hasil_filling' => $qq['jmlbat'],
			'hasil_bind' => $request->batangjml,
			// 'hasil_akhirb' => ,
			'tanggal_bind' => $request->tanggal_binding,
			'stock_binding_id' => $qq['idbinding'],
			'stock_filling_id' => $qq['idfilling'],
			'stock_probaku_id' => $request->idprobaku,
			'author_session' => Auth::user()->id,
			'created_at' => date('Y-m-d H:i:s'),
			'log' => "INSERT"
		];
		$query1 = Binding::insert($data);
		if ($query1) {
			$alert = 'Binding berhasil ditambahkan!';
		} else {
			$alert = 'Binding gagal ditambahkan!';
		}
		echo json_encode($alert);
		DB::commit();
	}

	public function uppbind(Request $request,$id,$masukweis,$terpoakaipro,$terpakaiweis,$terpakaifill,$batang)
	{
		DB::beginTransaction();
		$sdrwen = $request->stockpro - $request->terpakaipro;
		$aspqlw = $request->stockweisb + $request->masukweisb - $request->terpakaiweisb;
		$hale = $request->stockbatang - $request->terpakaifilling;

		$this->produksi->ifuppstockbind($request,$masukweis,$terpoakaipro,$terpakaiweis,$terpakaifill,$batang);

		$data = [
			'weisbmasuk' => $request->masukweisb,
			'terpakaiprob' => $request->terpakaipro,
			'terpakaiweisb' => $request->terpakaiweisb,
			'sisaprob' => $sdrwen,
			'sisaweisb' => $aspqlw,
			'terpakai_filling' => $request->terpakaifilling,
			'hasil_filling' => $hale,
			'hasil_bind' => $request->batangjml,
			// 'hasil_akhirb' => ,
			'tanggal_bind' => $request->tanggal_binding,
			'author_session' => Auth::user()->id,
			'log' => "UPDATE"
		];
		$query1 = Binding::where('id',$id)->update($data);
		if ($query1) {
			$this->produksi->loop_upbind($request,$id);
			$alert = 'Binding ID: '.$id.' berhasil diupdate!';
		} else {
			$alert = 'Binding ID: '.$id.' gagal diupdate!';
		}
		echo json_encode($alert);
		DB::commit();
	}

	public function delbind(Request $request,$id,$masukweis,$terpakaipro,$terpakaiweis,$terpakaifill,$batang)
	{
		DB::beginTransaction();
		$query = Stock_probaku::where('id',$request->idprobaku)->first();
		$qusrt = Stock_filling::where('id',$request->idfilling)->first();
		$werty = Stock_binding::where('id',$request->idbinding)->first();

		$pro = $query->jml_produksi + $terpakaipro;
		$batangfill = $qusrt->jml_fill + $terpakaifill;
		$weis = $werty->stock_weisb - $masukweis + $terpakaiweis;
		$batang = $werty->jml_bind - $batang;

		$update1 = Stock_probaku::find($request->idprobaku);
		$update1->jml_produksi = $pro;
		$update1->save();
		$update2 = Stock_filling::find($request->idfilling);
		$update2->jml_fill = $batangfill;
		$update2->save();
		$update3 = Stock_binding::find($request->idbinding);
		$update3->jml_bind = $batang;
		$update3->stock_weisb = $weis;
		$update3->save();

		$query1 = Binding::where('id',$id)->delete();
		if ($query1) {
			$this->produksi->loop_upbind($request,$id,$request->awlfill,$terpakaipro);
			$alert = 'Binding ID : '.$id.' berhasil dihapus!';
		} else {
			$alert = 'Binding ID : '.$id.' gagal dihapus!';
		}
		echo json_encode($alert);
		DB::commit();      
	}

}
