<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Cool;
use App\Stock_fumigasi;
use App\Stock_cool;

class Back_cool extends Controller
{
	public function sprocool($request)
	{
		$qusrt = DB::table('stock_fumigasi')
		->select('stock_fumigasi.id','stock_fumigasi.jml_fum')
		->join('jenis_has_produk','stock_fumigasi.jenis_has_produk_id_jnpro','=','jenis_has_produk.id_jnpro')
		->where([['jenis_has_produk.jenis_id_jenis','=',$request->jenis],['jenis_has_produk.produk_id_produk','=',$request->produk]]);

		$qusrt1 = DB::table('stock_cool')
		->select('stock_cool.id','stock_cool.jml_cool')
		->join('jenis_has_produk','stock_cool.jenis_has_produk_id_jnpro','=','jenis_has_produk.id_jnpro')
		->where([['jenis_has_produk.jenis_id_jenis','=',$request->jenis],['jenis_has_produk.produk_id_produk','=',$request->produk]]);

		if ($qusrt->count() > 0) {
			$aem = $qusrt->first();
			$qer = $aem->jml_fum;
			$qwc = $aem->id;
		} else { $qer = 0; $qwc = '';}

		if ($qusrt1->count() > 0) {
			$aem1 = $qusrt1->first();
			$ber = $aem1->jml_cool;
			$jkd = $aem1->id;
		} else { $ber = 0; $jkd = ''; }
		$array = ['jml_fum' => $qer,'jml_cool' => $ber,'idfumigasi' => $qwc,'idcool' => $jkd];
		return $array;
	}

	public function stockcool($request)
	{
		$bang = $request->stockbatang - $request->batangjml;
		$cekwrt = Stock_fumigasi::find($request->idfumigasi);
		$cekwrt->jml_fum = $bang;
		$cekwrt->save();
		$resultidwrt = $cekwrt->id;

		if ($request->idcool != '') {
			$id = $request->idcool; 
			$show = Stock_cool::where('id',$id)->first();
			$jml = $show->jml_cool + $request->batangjml;

			$cek = Stock_cool::find($id);
			$cek->jml_cool = $jml;
			$cek->save();

			$resultid = $cek->id;
		} else {
			$jml = $request->batangjml;
			$save = new Stock_cool;

			$save->jml_cool = $jml;
			$save->jenis_has_produk_id_jnpro = $request->idjnpro;
			$save->jenis_has_produk_id_jenis = $request->jenis;
			$save->jenis_has_produk_id_produk = $request->produk;
			$save->save();
			$resultid = $save->id;
		}
		return ['idfumigasi' => $resultidwrt,'idcool' => $resultid,'jml_fum' => $bang,'jml_cool' => $jml];
	}

	public function ifuppstockcool($request,$batang)
	{
		if ($batang != $request->batangjml) {
			$shcool = Stock_cool::where('id',$request->idcool)->first();
			$shfumi = Stock_fumigasi::where('id',$request->idfumigasi)->first();

			if ($batang > $request->batangjml) {
				$nah = $batang - $request->batangjml;
				$hasilw = $shcool->jml_cool - $nah;
				$slash = Stock_fumigasi::find($request->idfumigasi);
				$slash->jml_fum = $shfumi->jml_fum + $nah;
				$slash->save();
			} elseif ($batang < $request->batangjml) {
				$nah = $request->batangjml - $batang;
				$hasilw = $shcool->jml_cool + $nah;
				$slash = Stock_fumigasi::find($request->idfumigasi);
				$slash->jml_fum = $shfumi->jml_fum - $nah;
				$slash->save();
			} else {
				$hasilw = $shcool->jml_cool;
			}

			$query2 = Stock_cool::find($request->idcool);
			$query2->jml_cool = $hasilw;
			$query2->save();
		}
	}

	public function loop_upcool($request,$id,$batang = '')
	{
		$ceh = Cool::where([['stock_cool_id','=',$request->idcool],['stock_fumigasi_id','=',$request->idfumigasi],['id','>',$id]]);
		if ($ceh->count() > 0) {
			$reg = $ceh->get();
			for ($i=0; $i < $ceh->count(); $i++) {
				$show = Cool::where([['stock_cool_id','=',$request->idcool],['stock_fumigasi_id','=',$request->idfumigasi],['id','<',$reg[$i]->id]])->orderBy('id','desc')->take('1');
				if ($show->count() > 0) {
					$first = $show->first();
					$hsie = $first->hasil_akhircool - $reg[$i]->tambah_cool;
				} else {
					if ($batang == '') {
						$awl1 = $reg[$i]->hasil_akhircool + $reg[$i]->tambah_cool;
						$hsie = $awl1 - $reg[$i]->tambah_cool;
					} else {
						$awl1 = $reg[$i]->hasil_akhircool + $reg[$i]->tambah_cool + $batang;
						$hsie = $awl1 - $reg[$i]->tambah_cool;
					}
				}
				$update = Cool::where('id',$reg[$i]->id)->update(['hasil_akhircool' => $hsie]);
			}
		}
	}

}
