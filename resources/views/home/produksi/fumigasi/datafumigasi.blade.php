@extends('../../layout')

@section('title')
<title>BIN - ERP : Data Fumigasi</title>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/dmuy/MDTimePicker/mdtimepicker.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css" />
@endsection

@section('content')
<div class="row page-titles">
	<div class="col-md-5 align-self-center">
		<h3 class="text-themecolor">Fumigasi</h3>
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a href="javascript:void(0)">Proses</a></li>
			<li class="breadcrumb-item active">Data Fumigasi</li>
		</ol>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="row">
			<div class="col-md-6">
				<legend>Data Fumigasi</legend>
			</div>
			<div class="col-md-2 offset-md-4">
				<button type="button" data-link="{{ route('fumigasi.post') }}" class="btn btn-success btn-rounded btn-icons " id="tambah-fumigasi">Tambah</button>
			</div>
			<div class="table-responsive">
				<table id="table-fumigasi" width="100%" class="display nowrap table table-hover table-striped table-bordered">
					<thead>
						<tr>
							<th>No.</th>
							<th>Tanggal</th>
							<th>Produk</th>
							<th>Jenis</th>
							<th>Durasi</th>
							<th>Batang Drying2</th>
							<th>Tambah Batang</th>
							<th>JML</th>
							<th>Ket</th>
							<th>Aksi</th>
						</tr>
					</thead>
				</table>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="modal-fumigasi" tabindex="-1" role="dialog" aria-labelledby="title-tembakau" aria-hidden="true" style="display: none;">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="card">
				<div class="card-header">
					<div class="modal-header">
						<h4 class="modal-title" id="title"></h4>
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					</div>
				</div>
				<div class="card-body">
					<form id="form-fumigasi" method="POST">
						<div class="modal-body row">
							<div class="form-group col-md-12">
								<label class="control-label">Tanggal</label>
								<input type="text" class="form-control date_sam" id="tanggal_fumigasi">
							</div>
							<div class="form-group col-md-6">
								<label class="control-label">Jenis</label>
								<select class="form-control" id="jenisfumi"></select>
								<input type="hidden" id="jenis_infumi">
							</div>
							<div class="form-group col-md-6">
								<label class="control-label">Produk</label>
								<select class="form-control" id="produkfumi"></select>
								<input type="hidden" id="produk_infumi">
							</div>
							<div class="form-group col-md-12" id="stock_showitfumi">
								<button type="button" onclick="seacrh_datafumi()" class="btn btn-primary btn-xs btn-block">Search</button>
								<input type="hidden" id="iddrying2fumi">
								<input type="hidden" id="idfumigasi">
								<input type="hidden" id="idjnprofumi">
							</div>
							<div class="form-group col-md-12">
								<label class="control-label">Batang Drying2</label>
								<input type="number" class="form-control" id="batangdry2fumi" placeholder="Jml Batang Drying2" readonly>
							</div>
							<div class="form-group col-md-12">
								<label class="control-label">Batang Cerutu Yang Dihasilkan</label>
								<input type="number" class="form-control" id="batangjmlfumi" placeholder="Jml Batang">
							</div>
							<div class="form-group col-md-12">
								<label class="control-label">Lama Proses</label>
								<input type="text" class="form-control" id="durasifumi" placeholder="Durasi">
							</div>
							<div class="form-group col-md-12">
								<label class="control-label">Keterangan</label>
								<textarea class="form-control" id="ketfumi" placeholder="Keterangan"></textarea>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" onclick="save_methodfumi()" class="btn btn-primary waves-effect text-left">Save</button>
							<button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Close</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('javascript')
<script src="{{ asset('node_modules/moment/moment.js') }}">
</script><script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
<script src="{{ asset('node_modules/datatables/datatables.min.js') }}"></script>
<script src="https://cdn.jsdelivr.net/gh/dmuy/MDTimePicker/mdtimepicker.js
"></script>
<script>
	$('#stock_showitfumi').hide();
	let token = $("meta[name='csrf-token']").attr("content");

	const jenis = {!! json_encode($jenis) !!};
	jenis.unshift({id_jenis: '',jenis: ''});

	var table = $('#table-fumigasi').DataTable({
		"lengthMenu": [
		[3,5,10, 25, 50, 100, 150, 200, -1],
		[3,5,10, 25, 50, 100, 150, 200, "All"]
		],
		"order": [
		[0, 'desc']
		],
		processing: true,
		serverSide: true,
		ajax: '/datafumigasi/json',
		columns: [
		{ data: 'DT_RowIndex', name: 'DT_RowIndex' },
		{ data: 'tanggal_fum', name: 'tanggal_fum' },
		{ data: 'produk', name: 'produk' },
		{ data: 'jenis', name: 'jenis' },
		{ data: 'lama_fum', name: 'lama_fum' },
		{ data: 'awlbatang', name: 'awlbatang' },
		{ data: 'tambah_fum', name: 'tambah_fum' },
		{ data: 'hasil_akhirfum', name: 'hasil_akhirfum' },
		{ data: 'keterangan_fum', name: 'keterangan_fum' },
		{
			data: null,
			"bSortable": false,
			mRender: function (o) { 
				return '<button type="button" class="btn btn-info btn-rounded btn-xs" id="update-fumigasi" data-title="Update Fumigasi | ID: '+ o.id +'" data-link="/datafumigasi/upp/'+ o.id +'/'+ o.tambah_fum +'" data-tanggal_fumigasi="'+ o.tanggal_fum +'" data-jenis="'+ o.id_jenis +'" data-produk="'+ o.id_produk +'" data-durasi="'+ o.lama_fum +'" data-ket="'+ o.keterangan_fum +'" data-stockbatang="'+ o.awlbatang +'" data-batang="'+ o.tambah_fum +'" data-idfumigasi="'+ o.stock_fumigasi_id +'" data-iddrying2="'+ o.stock_drying2_id +'"><i class="mdi mdi-dots-horizontal"></i></button><button type="button" class="btn btn-danger btn-rounded btn-xs" id="delete-fumigasi" data-link="/datafumigasi/del/'+ o.id +'/'+ o.tambah_fum +'" data-idfumigasi="'+ o.stock_fumigasi_id +'" data-iddrying2="'+ o.stock_drying2_id +'"><i class="fa fa-trash"></i></button>'; }
			},
			],
			"displayLength": 10,
		});
	$('#table-fumigasi tbody').on('click', 'tr.group', function() {
		var currentOrder = table.order()[0];
		if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
			table.order([2, 'desc']).draw();
		} else {
			table.order([2, 'asc']).draw();
		}
	});
	$('#table-fumigasi tbody').on('click', 'tr.group', function() {
		var currentOrder = table.order()[0];
		if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
			table.order([2, 'desc']).draw();
		} else {
			table.order([2, 'asc']).draw();
		}
	});

	function getdatasearchfumi(position1) {
		let jenis = $('#jenis_infumi').val();
		let produk = $('#produk_infumi').val();

		$.ajax({
			url: '/datafumigasi/getsearchfill',
			type: 'POST',
			dataType: 'json',
			data: {
				"_token": token,
				produk: produk,
				jenis: jenis
			},
		})
		.done(function(data) {
			$('#iddrying2fumi').val(data.iddrying2);
			$('#idfumigasi').val(data.idfumigasi);
			$('#' + position1).val(data.jml_dry2);
		});
		
	}

	function ljenis(position,like) {
		let opt = '';
		for (let i = 0; i < jenis.length; i++) {
			opt += '<option value="'+ jenis[i].id_jenis +'"';
			if (jenis[i].id_jenis == like) {
				opt += 'selected';
			}
			opt += '>'+ jenis[i].jenis +'</option>';
		}
		$('#' + position).html(opt);
	}

	function lproduk(position,based,like) {
		$.ajax({
			url: '/datafilling/getproduk',
			type: 'POST',
			dataType: 'json',
			data: {
				"_token": token,
				jenis: based
			},
		})
		.done(function(data) {
			let opt = '<option value=""></option>';
			for(let i = 0; i < data.length; i++) {
				opt += '<option value="'+ data[i].id_produk +'"';
				if (data[i].id_produk == like) {
					opt += 'selected';
				}
				opt += ' data-id="'+ data[i].relid +'">'+ data[i].produk +'</option>';
			}
			$('#' + position).html(opt);
		});
	}

	function seacrh_datafumi() {
		getdatasearchfumi('batangdry2fumi');
	}

	function save_methodfumi() {
		let link = $('#form-fumigasi').attr('action');
		let iddrying2 = $('#iddrying2fumi').val();
		let idfumigasi = $('#idfumigasi').val();
		let idjnpro = $('#idjnprofumi').val();
		let tanggal_fumigasi = $('#tanggal_fumigasi').val();
		let jenis_in = $('#jenis_infumi').val();
		let produk_in = $('#produk_infumi').val();
		let stockbatang = $('#batangdry2fumi').val();
		let batangjml = $('#batangjmlfumi').val();
		let durasi = $('#durasifumi').val();
		let ket = $('#ketfumi').val();
		$.ajax({
			url: link,
			type: 'POST',
			dataType: 'json',
			data: {
				"_token": token,
				tanggal_fumigasi: tanggal_fumigasi,
				jenis: jenis_in,
				produk: produk_in,
				durasi:durasi,
				stockbatang: stockbatang,
				batangjml: batangjml,
				ket:ket,
				idfumigasi: idfumigasi,
				iddrying2: iddrying2,
				idjnpro:idjnpro
			},
		})
		.done(function(data) {
			alert(data);
			// console.log(data);
			$('#table-fumigasi').DataTable().ajax.reload();
			$('#modal-fumigasi').modal('toggle');
		});
		
	}

	$(document).on('click', '#tambah-fumigasi', function(event) {
		let clik = $(this);
		$('.modal-header #title').text('Tambah Fumigasi');
		$('#form-fumigasi').attr('action', clik.data('link'));
		$('#form-fumigasi').find('input').val('');
		$('#form-fumigasi').find('select').val('');
		$('#form-fumigasi').find('textarea').val('');
		$("#tanggal_fumigasi").datetimepicker({
			format: 'YYYY-MM-DD HH:mm:ss',
		}).data("DateTimePicker").date(new Date());
		$('#durasifumi').mdtimepicker({format: 'hh:mm'});
		$('#stock_showitfumi').fadeIn();
		$('#jenisfumi').removeAttr('disabled');
		$('#produkfumi').removeAttr('disabled');
		ljenis('jenisfumi');
		$(document).on('change', '#jenisfumi', function(event) {
			let jam = $(this).val();
			$('#jenis_infumi').val(jam);
			lproduk('produkfumi',jam);
			$(document).on('change', '#produkfumi', function(event) {
				let pwe = $(this).val();
				$('#produk_infumi').val(pwe);
				let kk = $(this).find(':selected').data('id');
				$('#idjnprofumi').val(kk);
			});
		});
		$('#modal-fumigasi').modal('show');
	});

	$(document).on('click', '#update-fumigasi', function(event) {
		let click = $(this);
		$('.modal-header #title').text(click.data('title'));
		$('#form-fumigasi').attr('action', click.data('link'));
		$('#form-fumigasi').find('input').val('');
		$('#form-fumigasi').find('select').val('');
		$('#form-fumigasi').find('textarea').val('');
		$('#stock_showitfumi').hide();
		$('#jenisfumi').attr('disabled','disabled');
		$('#produkfumi').attr('disabled','disabled');
		ljenis('jenisfumi',click.data('jenis'));
		lproduk('produkfumi',click.data('jenis'),click.data('produk'));

		$("#tanggal_fumigasi").datetimepicker({
			format: 'YYYY-MM-DD HH:mm:ss',
		}).data("DateTimePicker").date(click.data('tanggal_fumigasi'));
		$('#durasifumi').mdtimepicker({format: 'hh:mm'});
		$('#jenis_infumi').val(click.data('jenis'));
		$('#produk_infumi').val(click.data('produk'));
		$('#batangdry2fumi').val(click.data('stockbatang'));
		$('#batangjmlfumi').val(click.data('batang'));
		$('#iddrying2fumi').val(click.data('iddrying2'));
		$('#idfumigasi').val(click.data('idfumigasi'));
		$('#durasifumi').val(click.data('durasi'));
		$('#ketfumi').val(click.data('ket'));
		$('#modal-fumigasi').modal('show');
	});

	$(document).on('click', '#delete-fumigasi', function(event) {
		let hebg = confirm('Apakah anda yakin ingin menghapus data ini ?');
		if (hebg) {
			let link = $(this).data("link");
			let idfumigasi = $(this).data('idfumigasi');
			let iddrying2 = $(this).data('iddrying2');
			$.ajax(
			{
				url: link,
				type: 'DELETE',
				data: {
					"_token": token,
					idfumigasi: idfumigasi,
					iddrying2: iddrying2,
				},
				success: function (data){
					alert(data);
					$('#table-fumigasi').DataTable().ajax.reload();
				}
			});
		}
	});
</script>
@endsection