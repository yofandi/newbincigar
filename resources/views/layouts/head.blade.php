<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<!-- Tell the browser to be responsive to screen width -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- CSRF Token -->
<meta name="csrf-token" content="{{ csrf_token() }}">
<meta name="description" content="">
<meta name="author" content="">
<!-- Favicon icon -->
<link rel="icon" type="image/png" sizes="16x16" href="{{ asset('images/BIN.png') }}">
<!-- Bootstrap Core CSS -->
<link name="bootstrap" href="{{ asset('node_modules/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ asset('node_modules/perfect-scrollbar/css/perfect-scrollbar.css') }}" rel="stylesheet">
<!--Toaster Popup message CSS -->
<link href="{{ asset('node_modules/toast-master/css/jquery.toast.css') }}" rel="stylesheet">
<!-- Custom CSS -->
<link href="{{ asset('css/style.css') }}" rel="stylesheet">
<!-- Dashboard 1 Page CSS -->
<link href="{{ asset('css/pages/dashboard1.css') }}" rel="stylesheet">
<!-- You can change the theme colors from here -->
<link href="{{ asset('css/colors/default.css') }}" id="theme" rel="stylesheet">