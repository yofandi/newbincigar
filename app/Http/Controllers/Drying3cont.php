<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use DataTables;
use Auth;
use App\Drying3;
use App\Stock_cool;
use App\Stock_drying3;

use App\Http\Controllers\Back_drying3;

class Drying3cont extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->produksi = $res = new Back_drying3;
    }
    public function searchweiprodry3(Request $request)
    {
        $wqct = $this->produksi->sprodry3($request);
        echo json_encode($wqct);
    }

    public function getdatadry3()
    {
        $query = DB::table('drying3')
        ->select('drying3.*','jenis.id_jenis','produk.id_produk','jenis.jenis','produk.produk')
        ->join('stock_drying3','drying3.stock_drying3_id','=','stock_drying3.id')
        ->join('jenis','stock_drying3.jenis_has_produk_id_jenis','=','jenis.id_jenis')
        ->join('produk','stock_drying3.jenis_has_produk_id_produk','=','produk.id_produk')
        ->get();
        return Datatables::of($query)
        ->addIndexColumn()
        ->addColumn('awlbatang', function ($query) {
            $awl = $query->hasil_akhirdry3 + $query->tambah_dry3;
            return $awl;
        })
        ->make(true);
    }

    public function datadrying3()
    {
        $data['jenis'] = DB::table('jenis')->select('id_jenis','jenis')->get();
        return view('home.produksi.drying3.datadrying3',$data);
    }

    public function adddrying3(Request $request)
    {
        DB::beginTransaction();

        $qq = $this->produksi->stockdry3($request);

        $data = [
            'tambah_dry3' => $request->batangjml,
            'hasil_akhirdry3' => $qq['jml_cool'],
            'lama_dry3' => $request->durasi,
            'keterangan_dry3' => $request->ket,
            'tanggal_dry3' => $request->tanggal_drying3,
            'stock_drying3_id' => $qq['iddrying3'],
            'stock_cool_id' => $qq['idcool'],
            'author_session' => Auth::user()->id,
            'created_at' => date('Y-m-d H:i:s'),
            'log' => "INSERT"
        ];
        $query1 = Drying3::insert($data);
        if ($query1) {
            $alert = 'Drying 3 berhasil ditambahkan!';
        } else {
            $alert = 'Drying 3 gagal ditambahkan!';
        }
        echo json_encode($alert);
        DB::commit();
    }

    public function uppdrying3(Request $request,$id,$batang)
    {
        DB::beginTransaction();
        $jwty = $request->stockbatang - $request->batangjml;
        $this->produksi->ifuppstockdry3($request,$batang);

        $data = [
            'tambah_dry3' => $request->batangjml,
            'hasil_akhirdry3' => $jwty,
            'lama_dry3' => $request->durasi,
            'keterangan_dry3' => $request->ket,
            'tanggal_dry3' => $request->tanggal_drying3,
            'author_session' => Auth::user()->id,
            'created_at' => date('Y-m-d H:i:s'),
            'log' => "UPDATE"
        ];
        $query1 = Drying3::where('id',$id)->update($data);
        if ($query1) {
            $this->produksi->loop_updry3($request,$id);
            $alert = 'Drying 3 ID: '.$id.' berhasil diupdate!';
        } else {
            $alert = 'Drying 3 ID: '.$id.' gagal diupdate!';
        }
        echo json_encode($alert);
        DB::commit();
    }

    public function deldrying3(Request $request,$id,$batang)
    {
        DB::beginTransaction();
        $werty = Stock_cool::where('id',$request->idcool)->first();
        $qusrt = Stock_drying3::where('id',$request->iddrying3)->first();

        $batangcool = $werty->jml_cool + $batang;
        $batangdry3 = $qusrt->jml_dry3 - $batang;

        $update3 = Stock_cool::find($request->idcool);
        $update3->jml_cool = $batangcool;
        $update3->save();
        $update2 = Stock_drying3::find($request->iddrying3);
        $update2->jml_dry3 = $batangdry3;
        $update2->save();

        $query1 = Drying3::where('id',$id)->delete();
        if ($query1) {
            $this->produksi->loop_updry3($request,$id,$batang);
            $alert = 'Drying 3 ID : '.$id.' berhasil dihapus!';
        } else {
            $alert = 'Drying 3 ID : '.$id.' gagal dihapus!';
        }
        echo json_encode($alert);
        DB::commit();      
    }

}
