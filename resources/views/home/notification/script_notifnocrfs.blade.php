<script>
	$.extend(true, $.fn.datetimepicker.defaults, {
		icons: {
			time: 'far fa-clock',
			date: 'far fa-calendar',
			up: 'fas fa-arrow-up',
			down: 'fas fa-arrow-down',
			previous: 'fas fa-chevron-left',
			next: 'fas fa-chevron-right',
			today: 'fas fa-calendar-check',
			clear: 'far fa-trash-alt',
			close: 'far fa-times-circle'
		}
	});
	$('#loadingprosess').hide();
	$('#tambahshow').hide();	

	$('#tbale1').hide();
	$('#tbale2').hide();
	$('#ffger11').hide();
	$('#ffger21').hide();
	$('#ffger12').hide();
	$('#ffger22').hide();
	$('#ffger11').attr('class', 'col-md-6');
	$('#ffger21').attr('class', 'col-md-6');
	$('#ffger12').attr('class', 'col-md-6');
	$('#ffger22').attr('class', 'col-md-6');
	let expr = {{ $isi->status_inrfs }};
	let joe = {{ $isi->arah_dari }};

	let idnoc_inrfsjs = {{ $isi->id }};
	let token = $("meta[name='csrf-token']").attr("content");
	const produk = {!! json_encode($produk) !!}
	produk.unshift({id_produk: '',produk: ''});

	switch (expr) {
		case 0:
		// '/api/noc_items/resend/'+idnoc_inrfsjs
		$kenk = '/api/noc_inrfs/post/'+idnoc_inrfsjs;
		$('option[value="3"]', '#status').remove();
		$('option[value="4"]', '#status').remove();
		if (joe != 0) {
		} else {
		}

		$('#tanggal_noc').attr('readonly', 'readonly');

		$("#formsub").attr('action', $kenk);
		$('#ffger11').fadeIn();
		$('#ffger21').hide();
		$('#ffger12').fadeIn();
		$('#ffger22').hide();
		$('#ffger11').attr('class', 'col-md-12');
		$('#ffger21').attr('class', 'col-md-12');
		$('#ffger12').attr('class', 'col-md-12');
		$('#ffger22').attr('class', 'col-md-12');
		$('#tbale1').attr('class', 'col-md-12 table-responsive');
		$('#tbale1').fadeIn();
		$('#tbale2').attr('class', 'col-md-12 table-responsive');
		$('#tbale2').hide();
		break;

		case 1:
		$sdrg = '/api/noc_inrfs/post/'+idnoc_inrfsjs;
		$('#tanggal_noc').attr('readonly', 'readonly');

		$('option[value="0"]', '#status').remove();
		$('option[value="4"]', '#status').remove();

		$("#formsub").attr('action', $sdrg);
		if (joe != 0) {
			$('#ffger11').fadeIn();
			$('#ffger21').hide();
			$('#ffger12').fadeIn();
			$('#ffger22').hide();
			$('#ffger11').attr('class', 'col-md-12');
			$('#ffger21').attr('class', 'col-md-12');
			$('#ffger12').attr('class', 'col-md-12');
			$('#ffger22').attr('class', 'col-md-12');
			$('#tbale1').attr('class', 'col-md-12 table-responsive');
			$('#tbale1').fadeIn();
			$('#tbale2').attr('class', 'col-md-12 table-responsive');
			$('#tbale2').hide();
		} else {
			$('#ffger11').fadeIn();
			$('#ffger21').fadeIn();
			$('#ffger12').fadeIn();
			$('#ffger22').fadeIn();
			$('#ffger11').attr('class', 'col-md-6');
			$('#ffger21').attr('class', 'col-md-6');
			$('#ffger12').attr('class', 'col-md-6');
			$('#ffger22').attr('class', 'col-md-6');
			$('#tbale1').attr('class', 'col-md-6 table-responsive');
			$('#tbale1').fadeIn();
			$('#tbale2').attr('class', 'col-md-6 table-responsive');
			$('#tbale2').fadeIn();
			load();
		}
		break;

		case 2:
		$sfr = '/api/noc_items/resend/'+idnoc_inrfsjs;
		$('option[value="0"]', '#status').remove();
		$('option[value="3"]', '#status').remove();
		$('option[value="4"]', '#status').remove();
		if (joe != 0) {
		} else {
		}
		$("#formsub").attr('action', $sfr);
		$('#status').prepend('<option value="0">Kirim Ulang</option>');
		$('option[value="1"]', '#status').remove();
		$('#ffger11').fadeIn();
		$('#ffger21').hide();
		$('#ffger12').fadeIn();
		$('#ffger22').hide();
		$('#ffger11').attr('class', 'col-md-12');
		$('#ffger21').attr('class', 'col-md-12');
		$('#ffger12').attr('class', 'col-md-12');
		$('#ffger22').attr('class', 'col-md-12');
		$('#tbale1').attr('class', 'col-md-12 table-responsive');
		$('#tbale1').fadeIn();
		$('#tbale2').attr('class', 'col-md-12 table-responsive');
		$('#tbale2').hide();
		break;

		case 3:
		$kenk = '/api/noc_inrfs/post/'+idnoc_inrfsjs;
		if (joe != 0) {
			$('option[value="0"]', '#status').remove();
			$('option[value="3"]', '#status').remove();
		} else {
			$('option[value="0"]', '#status').remove();
			$('option[value="4"]', '#status').remove();
		}
		$('#tanggal_noc').attr('readonly', 'readonly');
		$("#formsub").attr('action', $kenk);
		$('#ffger11').fadeIn();
		$('#ffger21').hide();
		$('#ffger12').fadeIn();
		$('#ffger22').hide();
		$('#ffger11').attr('class', 'col-md-12');
		$('#ffger21').attr('class', 'col-md-12');
		$('#ffger12').attr('class', 'col-md-12');
		$('#ffger22').attr('class', 'col-md-12');
		$('#tbale1').attr('class', 'col-md-12 table-responsive');
		$('#tbale1').fadeIn();
		$('#tbale2').attr('class', 'col-md-12 table-responsive');
		$('#tbale2').hide();
		break;

		case 4:
		if (joe != 0) {
			$kenk = '/api/noc_inrfs/post/'+idnoc_inrfsjs;
			$('option[value="1"]', '#status').remove();
			$('option[value="0"]', '#status').remove();
		}
		$("#formsub").attr('action', $kenk);
		$('#ffger11').fadeIn();
		$('#ffger21').hide();
		$('#ffger12').fadeIn();
		$('#ffger22').hide();
		$('#ffger11').attr('class', 'col-md-12');
		$('#ffger21').attr('class', 'col-md-12');
		$('#ffger12').attr('class', 'col-md-12');
		$('#ffger22').attr('class', 'col-md-12');
		$('#tbale1').attr('class', 'col-md-12 table-responsive');
		$('#tbale1').fadeIn();
		$('#tbale2').attr('class', 'col-md-12 table-responsive');
		$('#tbale2').hide();
		break;
		default:
	}

	function restore() {
		$.ajax({
			url: '/api/noc_items/restore',
			type: 'GET',
			dataType: 'json',
			contentType : "application/json"
		})
		.done(function(data) {
		});

	}

	function load() {
		window.onbeforeunload = function () {
			return "Do you want to leave?"
		}

		window.onunload = function () {
			restore();
		};

	}

	function formsubmit() {
		window.onbeforeunload = null;
		window.onunload = null;
		$('#formsub').submit();
	}

	tablemasuk(idnoc_inrfsjs);
	tablereject();
	$("#tanggal_noc").datetimepicker({
		format: 'YYYY-MM-DD HH:mm:ss',
	});
	$("#tanggal_rej").datetimepicker({
		format: 'YYYY-MM-DD HH:mm:ss',
	});

	$(document).on('change', '#status', function(event) {
		let jakk = $(this).val();
		$('#status_inrfs_in').val(jakk);
	});

	function lproduk(position,like) {
		let opt = '';
		for (let i = 0; i < produk.length; i++) {
			opt += '<option value="'+ produk[i].id_produk +'"';
			if (produk[i].id_produk == like) {
				opt += 'selected';
			}
			opt += '>'+ produk[i].produk +'</option>';
		}
		$('#' + position).html(opt);
	}

	function lsubproduk(position,based,liker = '') {
		$.ajax({
			url: '/api/Report/getsubproduk/json',
			type: 'POST',
			dataType: 'json',
			data: {
				"_token": token,
				produk: based
			},
		})
		.done(function(data) {
			let opt = '<option value=""></option>';
			for(let i = 0; i < data.length; i++) {
				opt += '<option value="'+ data[i].id_sub_produk +'" data-name="'+ data[i].sub_kode +' - '+ data[i].sub_produk +'" ';
				if (data[i].id_sub_produk == liker) {opt += 'selected ';}
				opt += '>'+ data[i].sub_kode +' - '+ data[i].sub_produk +'</option>';
			}
			$('#' + position).html(opt);
		});
	}

	$(document).on('click', '#optklick', function(event) {
		let div = $('#tambahshow');

		div.fadeToggle(500, function() {
			$('#form-tambahsub').find('input, select, textarea').val('');
			lproduk('produk_in');
			$(document).on('change', '#produk_in', function(event) {
				let val = $(this).val();
				lsubproduk('subproduk_in',val);
			});
		});
	});

	$(document).on('click', '#cari-data', function(event) {
		let produk_in = $('#produk_in').val();
		let subproduk_in = $('#subproduk_in').val();
		if (joe == 0) {
			$nabe = '/datapacking/getsearchdata';
		} else {
			$nabe = '/api/rejectrfs/item';
		}
		$.ajax({
			url: $nabe,
			type: 'POST',
			dataType: 'json',
			data: {
				"_token": token,
				produk: produk_in,
				subproduk: subproduk_in,
			},
		})
		.done(function(data) {
			if (joe == 0) {
				$('#stockjmlrfs').val(data.rety.jmlpack);
			} else {
				$('#stockjmlrfs').val(data.jml);
			}
		});
	});

	$(document).on('click', '#tambah_itemsobject', function(event) {
		let subproduk_in = $('#subproduk_in').val();
		let jml_keluar = $('#jml_keluar').val();
		$.ajax({
			url: '/api/noc_items/add/' + idnoc_inrfsjs,
			type: 'POST',
			dataType: 'json',
			data: {
				"_token": token,
				subproduk: subproduk_in,
				jml_keluar: jml_keluar,
			},
			beforeSend: function() {
				isProcessing = true;
				$('#loadingprosess').fadeIn();
			},
			success: function(data) {
				$('#loadingprosess').hide();
				alert(data);
				isProcessing = false;
				tablemasuk('');
			}
		});
	});

	function tablemasuk(id) {
		$.ajax({
			url: '/api/noc_items/'+ idnoc_inrfsjs +'/all',
			type: 'GET',
			dataType: 'json',
		})
		.done(function(data) {
			let no = 1;
			let html = '';
			for (var i = 0; i < data.length; i++) {
				html += '<tr>';
				html += '<td>'+ no +'</td>';
				html += '<td>'+ data[i].sub_kode +' - '+ data[i].sub_produk +'</td>';
				html += '<td>'+ data[i].jml_nocitm +'</td>';
				html += '<td>';
				if ([1,3,4].includes(expr) && joe == 1) {
					html += '<button type="button" class="btn btn-outline btn-secondary btn-rounded btn-sm btn-icons" id="uraiankan" data-title="Uraikan Sub Produk| ID: '+ data[i].id +'" data-link="/noc_isintd/uraian/'+ data[i].id +'/'+ data[i].noc_inrfs_id +'" data-id="'+ data[i].id +'" data-isi="'+ data[i].isi +'" data-jml="'+ data[i].jml_nocitm +'" data-array="'+ data[i].uraian +'"><i class="fa fa-asterisk"></i></button>';
				} else {
					if ([1,2,3].includes(expr)) {
						html += '<button type="button" class="btn btn-outline btn-warning btn-rounded btn-sm btn-icons" id="sendrejecttocart" data-link="/api/noc_items/reject/post" data-title="Reject : '+ data[i].sub_kode +' - '+ data[i].sub_produk +'" data-id="'+ data[i].id +'" data-name="'+ data[i].sub_kode +' - '+ data[i].sub_produk +'" data-idpacking="'+ data[i].stock_packing_id +'" data-id_sub_produk="'+ data[i].sub_produk_id +'" data-qty="'+ data[i].jml_nocitm +'"><i class="mdi mdi-minus"></i></button>';
						html += '<button type="button" class="btn btn-danger btn-outline btn-rounded btn-sm btn-icons" id="deletecartitem" data-link="/api/noc_items/delitems/'+ data[i].id +'/'+ data[i].sub_kode +' - '+ data[i].sub_produk +'"><i class="mdi mdi-delete-variant"></i></button>';
					}
				}
				html += '</td>';
				html += '</tr>';
				no++;
			}
			$('#daftarrfsmasuk').html(html);
		});
	}

	function tablereject() {
		$.ajax({
			url: '/api/noc_items/reject',
			type: 'GET',
			dataType: 'json',
		})
		.done(function(data) {
			$('#daftarreject').html(data);
		});
	}	

	$(document).on('click', '#sendrejecttocart', function(event) {
		let aenk = $(this);
		$('.modal-header #title-noc_inrfs').text(aenk.data('title'));
		$('#form-noc_inrfs').attr('action',aenk.data('link'));
		$('#form-noc_inrfs').find('input').val('');
		$('#qty_noc_inrfs').val(aenk.data('qty'));
		$('#idnoc_itm').val(aenk.data('id'));
		$('#id_packing').val(aenk.data('idpacking'));
		$('#id_sub_produk').val(aenk.data('id_sub_produk'));
		$('#name_reject').val(aenk.data('name'));
		$('#modal-noc_inrfs').modal('toggle');
	});

	$(document).on('click', '#deletecartitem', function(event) {
		let link = $(this).data('link');
		let conf = confirm('Apakah anda yakin ingin menghapus data ini ?');
		if (conf) {
			$.ajax({
				url: link,
				type: 'DELETE',
				dataType: 'json',
				data: {
					"_token": token,
				},
			})
			.done(function(data) {
				tablemasuk(idnoc_inrfsjs);
				alert(data);
			});
		}

	});

	$(document).on('blur', '.tdatareject', function(event) {
		let rowId = $(this).data('rowid');
		let name = $(this).data('name');
		let column_name = $(this).data('column_name');
		let qty = $(this).text();
		$.ajax({
			url: '/api/noc_items/reject/upp',
			type: 'POST',
			dataType: 'json',
			data: {
				"_token": token,
				rowId: rowId,
				name: name,
				column_name: column_name,
				jml: qty,
			},
		})
		.done(function(data) {
			tablereject();
			alert(data);
		});
	});

	$(document).on('click', '#buttonreject', function(event) {
		let fgh = $(this);
		let conf = confirm('Apakah anda yakin ingin menghapus data ini ?');
		if (conf) {
			$.ajax({
				url: fgh.data('link'),
				type: 'DELETE',
				dataType: 'json',
				data: {
					"_token": token,
				},
			})
			.done(function(data) {
				tablereject();
				alert(data);
			});
		}

	});

	function savereject() {
		let link = $('#form-noc_inrfs').attr('action');
		let name_reject = $('#name_reject').val();
		let idnoc_itm = $('#idnoc_itm').val();
		let id_packing = $('#id_packing').val();
		let id_sub_produk = $('#id_sub_produk').val();
		let qty_noc_inrfs = $('#qty_noc_inrfs').val();
		$.ajax({
			url: link,
			type: 'POST',
			dataType: 'json',
			data: {
				"_token": token,
				name: name_reject,
				idpacking: id_packing,
				subproduk: id_sub_produk,
				jml: qty_noc_inrfs,
			},
		})
		.done(function(data) {
			tablemasuk(idnoc_inrfsjs);
			tablereject();
			alert(data);
			$('#modal-noc_inrfs').modal('toggle');
		});
	}

	$(document).on('click', '#uraiankan', function(event) {
		let tjenk = $(this);
		$('.modal-header #title-uraian').text(tjenk.data('title'));
		$('#form-uraian').find('input').val();
		$('#form-uraian').attr('action', tjenk.data('link'));
		if (tjenk.data('array') == false) {
		} else {
			$heu = tjenk.data('array');
			$('#topacking').val($heu[0]);
			$('#touraian').val($heu[1]);
			$('#conjml').val($heu[2]);
			$('#toqc').val($heu[3]);
			$('#jmlcincin').val($heu[4]);
			$('#stiker_luar').val($heu[5]);
			$('#stiker_dalam').val($heu[6]);
			$('#jmlcukai').val($heu[7]);
			$('#jmlkemasan').val($heu[8]);
			$('#fill1').val($heu[9]);
			$('#bind').val($heu[10]);
			$('#wrap').val($heu[11]);
			$('#fill2').val($heu[12]);
		}
		$('#iduraiansubproduk').val(tjenk.data('id'));
		$('#isi_uraian').val(tjenk.data('isi'));
		$('#jml_iteminrfs').val(tjenk.data('jml'));
		$('#modal-urai').modal('toggle');
	});

	$(document).on('keyup', '#touraian', function(event) {
		let lep = $(this).val();
		$kn = lep * $('#isi_uraian').val();
		$('#conjml').val($kn);
	});
</script>