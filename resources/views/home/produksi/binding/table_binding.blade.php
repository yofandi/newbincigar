<div class="row">
	<div class="col-md-6">
		<legend>Data Binding</legend>
	</div>
	<div class="col-md-2 offset-md-4">
		<button type="button" data-link="{{ route('binding.post') }}" class="btn btn-success btn-rounded btn-icons " id="tambah-binding">Tambah</button>
		<button type="button" onclick="Refresh_binding()" class="btn btn-rounded btn-icons btn-secondary">Refresh</button>
	</div>
	<div class="col-md-12" align="center" id="loadingdeletebind">
		<h5>Data tengah diproses. Mohon tunggu sejenak!</h5>
	</div>
	<div class="table-responsive">
		<table id="table-binding" width="100%" class="display nowrap table table-hover table-striped table-bordered">
			<thead>
				<tr>
					<th>No.</th>
					<th>Tanggal</th>
					<th>Produk</th>
					<th>Jenis</th>
					<th>Stock</th>
					<th>Masuk (Weis)</th>
					<th>Terpakai (Produksi)</th>
					<th>Terpakai (Weis)</th>
					<th>Sisa</th>
					<th>Awal Filling</th>
					<th>Terpakai Filling (*Gram)</th>
					<th>Jml Batang</th>
					<th>Aksi</th>
				</tr>
			</thead>
		</table>
	</div>
</div>