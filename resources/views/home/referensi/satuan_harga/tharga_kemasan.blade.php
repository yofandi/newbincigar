<div class="row">
	<div class="col-md-4">
		<h4 class="card-title">Harga Kemasan</h4>
	</div>
	<div class="col-md-2 offset-md-6">
		<button type="button" class="btn btn-xs btn-success" id="tambah-hargakemasan" data-link="{{ route('api.posthargakemasan') }}">Tambah</button>
		<button type="button" onclick="Refresh_kemasan()" class="btn btn-xs btn-icons btn-secondary">Refresh</button>
	</div>
	<div class="col-md-12" align="center" id="loadingdeletekemasan">
		<h5>Data tengah diproses. Mohon tunggu sejenak!</h5>
	</div>
	<div class="table-responsive">
		<table id="table-hargakemasan" class="display nowrap table table-hover table-striped table-bordered" width="100%">
			<thead>
				<th>No.</th>
				<th>Tanggal</th>
				<th>Kode Harga Kemasan</th>
				<th>Produk</th>
				<th>Sub Produk</th>
				<th>Harga Kemasan</th>
				<th>Keterangan</th>
				<th>Aksi</th>
			</thead>
		</table>
	</div>
</div>