@extends('../../layout')

@section('title')
<title>BIN - ERP : Data Wrapping</title>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css" />
@endsection

@section('content')
<div class="row page-titles">
	<div class="col-md-5 align-self-center">
		<h3 class="text-themecolor">Wrapping</h3>
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a href="javascript:void(0)">Proses</a></li>
			<li class="breadcrumb-item active">Data Wrapping</li>
		</ol>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="row">
			<div class="col-md-6">
				<legend>Data Wrapping</legend>
			</div>
			<div class="col-md-2 offset-md-4">
				<button type="button" data-link="{{ route('wrapping.post') }}" class="btn btn-success btn-rounded btn-icons " id="tambah-wrapping">Tambah</button>
			</div>
			<div class="table-responsive">
				<table id="table-wrapping" width="100%" class="display nowrap table table-hover table-striped table-bordered">
					<thead>
						<tr>
							<th>No.</th>
							<th>Tanggal</th>
							<th>Produk</th>
							<th>Jenis</th>
							<th>Stock</th>
							<th>Masuk (Weis)</th>
							<th>Terpakai (Produksi)</th>
							<th>Terpakai (Weis)</th>
							<th>Sisa</th>
							<th>Batang Filling</th>
							<th>Jml Batang</th>
							<th>ket</th>
							<th>Aksi</th>
						</tr>
					</thead>
				</table>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="modal-wrapping" tabindex="-1" role="dialog" aria-labelledby="title-tembakau" aria-hidden="true" style="display: none;">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="card">
				<div class="card-header">
					<div class="modal-header">
						<h4 class="modal-title" id="title"></h4>
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					</div>
				</div>
				<div class="card-body">
					<form id="form-wrapping" method="POST">
						<div class="modal-body row">
							<div class="form-group col-md-12 fa-3x spinner_nekrwrap" align="center">
								<i class="fas fa-spinner fa-spin"></i>
							</div>
							<div class="form-group col-md-12">
								<label class="control-label">Tanggal</label>
								<input type="text" class="form-control date_sam" id="tanggal_wrapping">
							</div>
							<div class="form-group col-md-6">
								<label class="control-label">Jenis</label>
								<select class="form-control" id="jeniswrap"></select>
								<input type="hidden" id="jenis_inwrap">
							</div>
							<div class="form-group col-md-6">
								<label class="control-label">Produk</label>
								<select class="form-control" id="produkwrap"></select>
								<input type="hidden" id="produk_inwrap">
							</div>
							<div class="form-group col-md-12" id="stock_showitwrap">
								<button type="button" onclick="seacrh_datawrap()" class="btn btn-primary btn-xs btn-block">Search</button>
								<input type="hidden" id="idprobakuwrap">
								<input type="hidden" id="idpressingwrap">
								<input type="hidden" id="idwrapping">
								<input type="hidden" id="idjnprowrap">
							</div>
							<div class="form-group col-md-5">
								<label class="control-label">Stock Produksi (DEKLAD: Gram)</label>
								<input type="number" class="form-control" step="any" id="stockprowrap" value="0" placeholder="Stock (Saat ini/Awal)" readonly>
							</div>
							<div class="form-group col-md-3">
								<label class="control-label">Stock Weis (*Gram)</label>
								<input type="number" class="form-control" step="any" id="stockweisw" value="0" placeholder="Stock Weis" readonly>
							</div>
							<div class="form-group col-md-4">
								<label class="control-label">Batang Pressing</label>
								<input type="number" class="form-control" id="batangpres" placeholder="Jml Batang Pressing" readonly>
							</div>
							<div class="form-group col-md-12">
								<label class="control-label">Terpakai Produksi (*Gram)</label>
								<input type="number" class="form-control" step="any" id="terpakaiprowrap" placeholder="Stock Produksi Terpakai">
							</div>
							<div class="form-group col-md-6">
								<label class="control-label">Masuk Weis (*Gram)</label>
								<input type="number" class="form-control" step="any" id="masukweisw" placeholder="Stock Masuk Weisf">
							</div>
							<div class="form-group col-md-6">
								<label class="control-label">Terpakai Weis (*Gram)</label>
								<input type="number" class="form-control" step="any" id="terpakaiweisw" placeholder="Stock Produksi Weisf">
							</div>
							<div class="form-group col-md-6">
								<label class="control-label">Reject Filling (*Gram)</label>
								<input type="number" class="form-control" step="any" id="reject_filling" placeholder="Reject Filling">
								<small>*Otomatis masuk ke Filler1 akan X 2</small>
							</div>
							<div class="form-group col-md-6">
								<label class="control-label">Reject Binding (*Gram)</label>
								<input type="number" class="form-control" step="any" id="reject_binding" placeholder="Reject Binding">
							</div>
							<div class="form-group col-md-12">
								<label class="control-label">Batang Cerutu Yang Dihasilkan</label>
								<input type="number" class="form-control" id="batangjmlwrap" placeholder="Jml Batang">
							</div>
							<div class="form-group col-md-12">
								<label class="control-label">Keterangan</label>
								<textarea class="form-control" id="ket" placeholder="Keterangan"></textarea>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" onclick="save_methodwrap()" class="btn btn-primary waves-effect text-left">Save</button>
							<button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Close</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('javascript')
<script src="{{ asset('node_modules/moment/moment.js') }}">
</script><script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
<script src="{{ asset('node_modules/datatables/datatables.min.js') }}"></script>
<script>
	$('#stock_showitwrap').hide();
	let token = $("meta[name='csrf-token']").attr("content");

	const jenis = {!! json_encode($jenis) !!};
	jenis.unshift({id_jenis: '',jenis: ''});

	var table = $('#table-wrapping').DataTable({
		"lengthMenu": [
		[3,5,10, 25, 50, 100, 150, 200, -1],
		[3,5,10, 25, 50, 100, 150, 200, "All"]
		],
		"order": [
		[0, 'desc']
		],
		processing: true,
		serverSide: true,
		ajax: '/datawrapping/json',
		columns: [
		{ data: 'DT_RowIndex', name: 'DT_RowIndex' },
		{ data: 'tanggal_wrap', name: 'tanggal_wrap' },
		{ data: 'produk', name: 'produk' },
		{ data: 'jenis', name: 'jenis' },
		{ data: 'stockawal', name: 'stockawal' },
		{ data: 'weiswmasuk', name: 'weiswmasuk' },
		{ data: 'terpakaiprow', name: 'terpakaiprow' },
		{ data: 'terpakaiweisw', name: 'terpakaiweisw' },
		{ data: 'sisaprow', name: 'sisaprow' },
		{ data: 'awlbatang', name: 'awlbatang' },
		{ data: 'tambah_wrap', name: 'tambah_wrap' },
		{ data: 'keterangan_wrap', name: 'keterangan_wrap' },
		{
			data: null,
			"bSortable": false,
			mRender: function (o) { 
				return '<button type="button" class="btn btn-info btn-rounded btn-xs" id="update-wrapping" data-title="Update Wrapping | ID: '+ o.id +'" data-link="/datawrapping/upp/'+ o.id +'/'+ o.weiswmasuk +'/'+ o.terpakaiprow +'/'+ o.terpakaiweisw +'/'+ o.tambah_wrap +'" data-tanggal_wrapping="'+ o.tanggal_wrap +'" data-jenis="'+ o.id_jenis +'" data-produk="'+ o.id_produk +'" data-stockpro="'+ o.stockawal +'" data-stockweis="'+ o.stockawalweis +'" data-stockbatang="'+ o.awlbatang +'" data-masukweis="'+ o.weiswmasuk +'" data-terpakaiprow="'+ o.terpakaiprow +'" data-terpakaiweisw="'+ o.terpakaiweisw +'" data-batang="'+ o.tambah_wrap +'" data-idprobaku="'+ o.stock_probaku_id +'" data-idpressing="'+ o.stock_pressing_id +'" data-idwrapping="'+ o.stock_wrapping_id +'" data-ket="'+ o.keterangan_wrap +'"><i class="mdi mdi-dots-horizontal"></i></button><button type="button" class="btn btn-danger btn-rounded btn-xs" id="delete-wrapping" data-link="/datawrapping/del/'+ o.id +'/'+ o.weiswmasuk +'/'+ o.terpakaiprow +'/'+ o.terpakaiweisw +'/'+ o.tambah_wrap +'" data-idprobaku="'+ o.stock_probaku_id +'" data-idpressing="'+ o.stock_pressing_id +'" data-idwrapping="'+ o.stock_wrapping_id +'"><i class="fa fa-trash"></i></button>'; }
			},
			],
			"displayLength": 10,
		});
	$('#table-wrapping tbody').on('click', 'tr.group', function() {
		var currentOrder = table.order()[0];
		if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
			table.order([2, 'desc']).draw();
		} else {
			table.order([2, 'asc']).draw();
		}
	});
	$('#table-wrapping tbody').on('click', 'tr.group', function() {
		var currentOrder = table.order()[0];
		if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
			table.order([2, 'desc']).draw();
		} else {
			table.order([2, 'asc']).draw();
		}
	});

	function getdatasearchwrap(position1,position2,position3) {
		let jenis = $('#jenis_inwrap').val();
		let produk = $('#produk_inwrap').val();

		$.ajax({
			url: '/datawrapping/getsearchfill',
			type: 'POST',
			dataType: 'json',
			data: {
				"_token": token,
				produk: produk,
				jenis: jenis
			},
		})
		.done(function(data) {
			$('#idpressingwrap').val(data.idpressing);
			$('#idwrapping').val(data.idwrapping);
			$('#idprobakuwrap').val(data.id);
			$('#' + position1).val(data.jml_produksi);
			$('#' + position2).val(data.stock_weiswrap);
			$('#' + position3).val(data.jml_pres);
		});
		
	}

	function ljenis(position,like) {
		let opt = '';
		for (let i = 0; i < jenis.length; i++) {
			opt += '<option value="'+ jenis[i].id_jenis +'"';
			if (jenis[i].id_jenis == like) {
				opt += 'selected';
			}
			opt += '>'+ jenis[i].jenis +'</option>';
		}
		$('#' + position).html(opt);
	}

	function lproduk(position,based,like) {
		$.ajax({
			url: '/datafilling/getproduk',
			type: 'POST',
			dataType: 'json',
			data: {
				"_token": token,
				jenis: based
			},
		})
		.done(function(data) {
			let opt = '<option value=""></option>';
			for(let i = 0; i < data.length; i++) {
				opt += '<option value="'+ data[i].id_produk +'"';
				if (data[i].id_produk == like) {
					opt += 'selected';
				}
				opt += ' data-id="'+ data[i].relid +'">'+ data[i].produk +'</option>';
			}
			$('#' + position).html(opt);
		});
	}

	function seacrh_datawrap() {
		getdatasearchwrap('stockprowrap','stockweisw','batangpres');
	}

	function save_methodwrap() {
		let link = $('#form-wrapping').attr('action');
		let idprobaku = $('#idprobakuwrap').val();
		let idwrapping = $('#idwrapping').val();
		let idpressing = $('#idpressingwrap').val();
		let idjnpro = $('#idjnprowrap').val();
		let tanggal_wrapping = $('#tanggal_wrapping').val();
		let jenis_in = $('#jenis_inwrap').val();
		let produk_in = $('#produk_inwrap').val();
		let stockpro = $('#stockprowrap').val();
		let stockweisw = $('#stockweisw').val();
		let stockbatang = $('#batangpres').val();
		let masukweisw = $('#masukweisw').val();
		let terpakaipro = $('#terpakaiprowrap').val();
		let terpakaiweisw = $('#terpakaiweisw').val();
		let batangjml = $('#batangjmlwrap').val();
		let ket = $('#ket').val();
		$.ajax({
			url: link,
			type: 'POST',
			dataType: 'json',
			data: {
				"_token": token,
				tanggal_wrapping: tanggal_wrapping,
				jenis: jenis_in,
				produk: produk_in,
				stockpro: stockpro,
				stockweisw: stockweisw,
				stockbatang: stockbatang,
				masukweisw:masukweisw,
				terpakaipro: terpakaipro,
				terpakaiweisw: terpakaiweisw,
				batangjml: batangjml,
				idprobaku: idprobaku,
				idpressing: idpressing,
				idwrapping: idwrapping,
				idjnpro:idjnpro,
				ket:ket
			},
		})
		.done(function(data) {
			alert(data);
			// console.log(data);
			$('#table-wrapping').DataTable().ajax.reload();
			$('#modal-wrapping').modal('toggle');
		});
		
	}

	$(document).on('click', '#tambah-wrapping', function(event) {
		let clik = $(this);
		$('.modal-header #title').text('Tambah Wrapping');
		$('#form-wrapping').attr('action', clik.data('link'));
		$('#form-wrapping').find('input').val('');
		$('#form-wrapping').find('select').val('');
		$('#form-wrapping').find('textarea').val('');
		$("#tanggal_wrapping").datetimepicker({
			format: 'YYYY-MM-DD HH:mm:ss',
		}).data("DateTimePicker").date(new Date());
		$('#stock_showitwrap').fadeIn();
		$('#jeniswrap').removeAttr('disabled');
		$('#produkwrap').removeAttr('disabled');
		ljenis('jeniswrap');
		$(document).on('change', '#jeniswrap', function(event) {
			let jam = $(this).val();
			$('#jenis_inwrap').val(jam);
			lproduk('produkwrap',jam);
			$(document).on('change', '#produkwrap', function(event) {
				let pwe = $(this).val();
				$('#produk_inwrap').val(pwe);
				let kk = $(this).find(':selected').data('id');
				$('#idjnprowrap').val(kk);
			});
		});
		$('#modal-wrapping').modal('show');
	});

	$(document).on('click', '#update-wrapping', function(event) {
		let click = $(this);
		$('.modal-header #title').text(click.data('title'));
		$('#form-wrapping').attr('action', click.data('link'));
		$('#form-wrapping').find('input').val('');
		$('#form-wrapping').find('select').val('');
		$('#form-wrapping').find('textarea').val('');
		$('#stock_showitwrap').hide();
		$('#jeniswrap').attr('disabled','disabled');
		$('#produkwrap').attr('disabled','disabled');
		ljenis('jeniswrap',click.data('jenis'));
		lproduk('produkwrap',click.data('jenis'),click.data('produk'));

		$("#tanggal_wrapping").datetimepicker({
			format: 'YYYY-MM-DD HH:mm:ss',
		}).data("DateTimePicker").date(click.data('tanggal_wrapping'));
		$('#jenis_inwrap').val(click.data('jenis'));
		$('#produk_inwrap').val(click.data('produk'));
		$('#stockprowrap').val(click.data('stockpro'));
		$('#stockweisw').val(click.data('stockweis'));
		$('#batangpres').val(click.data('stockbatang'));
		$('#masukweisw').val(click.data('masukweis'));
		$('#terpakaiprowrap').val(click.data('terpakaiprow'));
		$('#terpakaiweisw').val(click.data('terpakaiweisw'));
		$('#batangjmlwrap').val(click.data('batang'));
		$('#idprobakuwrap').val(click.data('idprobaku'));
		$('#idwrapping').val(click.data('idwrapping'));
		$('#idpressingwrap').val(click.data('idpressing'));
		$('#ket').val(click.data('ket'));
		$('#modal-wrapping').modal('show');
	});

	$(document).on('click', '#delete-wrapping', function(event) {
		let sbse = confirm('Apakah anda yakin ingin menghapus data ini ?');
		if (sbse) {
			let link = $(this).data("link");
			let idprobaku = $(this).data('idprobaku');
			let idpressing = $(this).data('idpressing');
			let idwrapping = $(this).data('idwrapping');
			$.ajax(
			{
				url: link,
				type: 'DELETE',
				data: {
					"_token": token,
					idprobaku: idprobaku,
					idpressing: idpressing,
					idwrapping: idwrapping,
				},
				success: function (data){
					alert(data);
					$('#table-wrapping').DataTable().ajax.reload();
				}
			});
		}
	});
</script>
@endsection