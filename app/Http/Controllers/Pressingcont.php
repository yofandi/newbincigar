<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use DataTables;
use Auth;
use App\Pressing;
use App\Stock_binding;
use App\Stock_pressing;
use App\Http\Controllers\Back_pressing;

class Pressingcont extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->produksi = $res = new Back_pressing;
    }
    public function searchweipropres(Request $request)
    {
        $bjek = $this->produksi->spropres($request);
        echo json_encode($bjek);
    }

    public function getdatapres()
    {
        $query = DB::table('pressing')
        ->select('pressing.*','jenis.id_jenis','produk.id_produk','jenis.jenis','produk.produk')
        ->join('stock_pressing','pressing.stock_pressing_id','=','stock_pressing.id')
        ->join('jenis','stock_pressing.jenis_has_produk_id_jenis','=','jenis.id_jenis')
        ->join('produk','stock_pressing.jenis_has_produk_id_produk','=','produk.id_produk')
        ->get();
        return Datatables::of($query)
        ->addIndexColumn()
        ->addColumn('awlbatang', function ($query) {
            $awl = $query->hasil_akhirp + $query->tambah_pres;
            return $awl;
        })
        ->make(true);
    }

    public function datapressing()
    {
        $data['jenis'] = DB::table('jenis')->select('id_jenis','jenis')->get();
        return view('home.produksi.pressing.datapressing',$data);
    }

    public function addpressing(Request $request)
    {
        DB::beginTransaction();

        $qq = $this->produksi->stockpres($request);

        $data = [
            'tambah_pres' => $request->batangjml,
            'hasil_akhirp' => $qq['jmlbind'],
            'lama' => $request->durasi,
            'keterangan_pres' => $request->ket,
            'tanggal_pres' => $request->tanggal_pressing,
            'stock_pressing_id' => $qq['idpressing'],
            'stock_binding_id' => $qq['idbinding'],
            'author_session' => Auth::user()->id,
            'created_at' => date('Y-m-d H:i:s'),
            'log' => "INSERT"
        ];
        $query1 = Pressing::insert($data);
        if ($query1) {
            $alert = 'Pressing berhasil ditambahkan!';
        } else {
            $alert = 'Pressing gagal ditambahkan!';
        }
        echo json_encode($alert);
        DB::commit();
    }

    public function upppressing(Request $request,$id,$batang)
    {
        DB::beginTransaction();
        $jwty = $request->stockbatang - $request->batangjml;
        $this->produksi->ifuppstockpres($request,$batang);

        $data = [
            'tambah_pres' => $request->batangjml,
            'hasil_akhirp' => $jwty,
            'lama' => $request->durasi,
            'keterangan_pres' => $request->ket,
            'tanggal_pres' => $request->tanggal_pressing,
            'author_session' => Auth::user()->id,
            'created_at' => date('Y-m-d H:i:s'),
            'log' => "INSERT"
        ];
        $query1 = Pressing::where('id',$id)->update($data);
        if ($query1) {
            $this->produksi->loop_uppres($request,$id);
            $alert = 'Pressing ID: '.$id.' berhasil diupdate!';
        } else {
            $alert = 'Pressing ID: '.$id.' gagal diupdate!';
        }
        echo json_encode($alert);
        DB::commit();
    }

    public function delpressing(Request $request,$id,$batang)
    {
        DB::beginTransaction();
        $werty = Stock_binding::where('id',$request->idbinding)->first();
        $qusrt = Stock_pressing::where('id',$request->idpressing)->first();

        $batangpres = $qusrt->jml_pres - $batang;
        $batangbind = $werty->jml_bind + $batang;

        $update3 = Stock_binding::find($request->idbinding);
        $update3->jml_bind = $batangbind;
        $update3->save();
        $update2 = Stock_pressing::find($request->idpressing);
        $update2->jml_pres = $batangpres;
        $update2->save();

        $query1 = Pressing::where('id',$id)->delete();
        if ($query1) {
            $this->produksi->loop_uppres($request,$id,$batang);
            $alert = 'Pressing ID : '.$id.' berhasil dihapus!';
        } else {
            $alert = 'Pressing ID : '.$id.' gagal dihapus!';
        }
        echo json_encode($alert);
        DB::commit();      
    }

}
