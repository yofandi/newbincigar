<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Drying3;
use App\Stock_cool;
use App\Stock_drying3;

class Back_drying3 extends Controller
{
public function sprodry3($request)
{
 $qusrt = DB::table('stock_cool')
 ->select('stock_cool.id','stock_cool.jml_cool')
 ->join('jenis_has_produk','stock_cool.jenis_has_produk_id_jnpro','=','jenis_has_produk.id_jnpro')
 ->where([['jenis_has_produk.jenis_id_jenis','=',$request->jenis],['jenis_has_produk.produk_id_produk','=',$request->produk]]);

 $qusrt1 = DB::table('stock_drying3')
 ->select('stock_drying3.id','stock_drying3.jml_dry3')
 ->join('jenis_has_produk','stock_drying3.jenis_has_produk_id_jnpro','=','jenis_has_produk.id_jnpro')
 ->where([['jenis_has_produk.jenis_id_jenis','=',$request->jenis],['jenis_has_produk.produk_id_produk','=',$request->produk]]);

 if ($qusrt->count() > 0) {
  $aem = $qusrt->first();
  $qer = $aem->jml_cool;
  $qwc = $aem->id;
} else { $qer = 0; $qwc = '';}

if ($qusrt1->count() > 0) {
  $aem1 = $qusrt1->first();
  $ber = $aem1->jml_dry3;
  $jkd = $aem1->id;
} else { $ber = 0; $jkd = ''; }
$array = ['jml_cool' => $qer,'jml_dry3' => $ber,'idcool' => $qwc,'iddrying3' => $jkd];
return $array;
}

public function stockdry3($request)
{
  $bang = $request->stockbatang - $request->batangjml;
  $cekwrt = Stock_cool::find($request->idcool);
  $cekwrt->jml_cool = $bang;
  $cekwrt->save();
  $resultidwrt = $cekwrt->id;

  if ($request->iddrying3 != '') {
    $id = $request->iddrying3; 
    $show = Stock_drying3::where('id',$id)->first();
    $jml = $show->jml_dry3 + $request->batangjml;

    $cek = Stock_drying3::find($id);
    $cek->jml_dry3 = $jml;
    $cek->save();

    $resultid = $cek->id;
  } else {
    $jml = $request->batangjml;
    $save = new Stock_drying3;

    $save->jml_dry3 = $jml;
    $save->jenis_has_produk_id_jnpro = $request->idjnpro;
    $save->jenis_has_produk_id_jenis = $request->jenis;
    $save->jenis_has_produk_id_produk = $request->produk;
    $save->save();
    $resultid = $save->id;
  }
  return ['idcool' => $resultidwrt,'iddrying3' => $resultid,'jml_cool' => $bang,'jml_dry3' => $jml];
}

public function ifuppstockdry3($request,$batang)
{
  if ($batang != $request->batangjml) {
    $shdry3 = Stock_drying3::where('id',$request->iddrying3)->first();
    $shcool = Stock_cool::where('id',$request->idcool)->first();

    if ($batang > $request->batangjml) {
      $nah = $batang - $request->batangjml;
      $hasilw = $shdry3->jml_dry3 - $nah;
      $slash = Stock_cool::find($request->idcool);
      $slash->jml_cool = $shcool->jml_cool + $nah;
      $slash->save();
    } elseif ($batang < $request->batangjml) {
      $nah = $request->batangjml - $batang;
      $hasilw = $shdry3->jml_dry3 + $nah;
      $slash = Stock_cool::find($request->idcool);
      $slash->jml_cool = $shcool->jml_cool - $nah;
      $slash->save();
    } else {
      $hasilw = $shdry3->jml_dry3;
    }

    $query2 = Stock_drying3::find($request->iddrying3);
    $query2->jml_dry3 = $hasilw;
    $query2->save();
  }
}

public function loop_updry3($request,$id,$batang = '')
{
  $ceh = Drying3::where([['stock_cool_id','=',$request->idcool],['stock_drying3_id','=',$request->iddrying3],['id','>',$id]]);
  if ($ceh->count() > 0) {
    $reg = $ceh->get();
    for ($i=0; $i < $ceh->count(); $i++) {
      $show = Drying3::where([['stock_cool_id','=',$request->idcool],['stock_drying3_id','=',$request->iddrying3],['id','<',$reg[$i]->id]])->orderBy('id','desc')->take('1');
      if ($show->count() > 0) {
        $first = $show->first();
        $hsie = $first->hasil_akhirdry3 - $reg[$i]->tambah_dry3;
      } else {
        if ($batang == '') {
          $awl1 = $reg[$i]->hasil_akhirdry3 + $reg[$i]->tambah_dry3;
          $hsie = $awl1 - $reg[$i]->tambah_dry3;
        } else {
          $awl1 = $reg[$i]->hasil_akhirdry3 + $reg[$i]->tambah_dry3 + $batang;
          $hsie = $awl1 - $reg[$i]->tambah_dry3;
        }
      }
      $update = Drying3::where('id',$reg[$i]->id)->update(['hasil_akhirdry3' => $hsie]);
    }
  }
}

}
