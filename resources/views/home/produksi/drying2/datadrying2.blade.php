@extends('../../layout')

@section('title')
<title>BIN - ERP : Data Drying 2</title>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/dmuy/MDTimePicker/mdtimepicker.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css" />
@endsection

@section('content')
<div class="row page-titles">
	<div class="col-md-5 align-self-center">
		<h3 class="text-themecolor">Drying 2</h3>
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a href="javascript:void(0)">Proses</a></li>
			<li class="breadcrumb-item active">Data Drying 2</li>
		</ol>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="row">
			<div class="col-md-6">
				<legend>Data Drying 2</legend>
			</div>
			<div class="col-md-2 offset-md-4">
				<button type="button" data-link="{{ route('drying2.post') }}" class="btn btn-success btn-rounded btn-icons " id="tambah-drying2">Tambah</button>
			</div>
			<div class="table-responsive">
				<table id="table-drying2" width="100%" class="display nowrap table table-hover table-striped table-bordered">
					<thead>
						<tr>
							<th>No.</th>
							<th>Tanggal</th>
							<th>Produk</th>
							<th>Jenis</th>
							<th>Durasi</th>
							<th>Batang Frezzer</th>
							<th>Tambah Batang</th>
							<th>JML</th>
							<th>Ket</th>
							<th>Aksi</th>
						</tr>
					</thead>
				</table>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="modal-drying2" tabindex="-1" role="dialog" aria-labelledby="title-tembakau" aria-hidden="true" style="display: none;">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="card">
				<div class="card-header">
					<div class="modal-header">
						<h4 class="modal-title" id="title"></h4>
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					</div>
				</div>
				<div class="card-body">
					<form id="form-drying2" method="POST">
						<div class="modal-body row">
							<div class="form-group col-md-12">
								<label class="control-label">Tanggal</label>
								<input type="text" class="form-control date_sam" id="tanggal_drying2">
							</div>
							<div class="form-group col-md-6">
								<label class="control-label">Jenis</label>
								<select class="form-control" id="jenisdry2"></select>
								<input type="hidden" id="jenis_indry2">
							</div>
							<div class="form-group col-md-6">
								<label class="control-label">Produk</label>
								<select class="form-control" id="produkdry2"></select>
								<input type="hidden" id="produk_indry2">
							</div>
							<div class="form-group col-md-12" id="stock_showitdry2">
								<button type="button" onclick="seacrh_datadry2()" class="btn btn-primary btn-xs btn-block">Search</button>
								<input type="hidden" id="iddrying2">
								<input type="hidden" id="idfrezzerdry2">
								<input type="hidden" id="idjnprodry2">
							</div>
							<div class="form-group col-md-12">
								<label class="control-label">Batang Freezer</label>
								<input type="number" class="form-control" id="batangfre" placeholder="Jml Batang Freezer" readonly>
							</div>
							<div class="form-group col-md-12">
								<label class="control-label">Batang Cerutu Yang Dihasilkan</label>
								<input type="number" class="form-control" id="batangjmldry2" placeholder="Jml Batang">
							</div>
							<div class="form-group col-md-12">
								<label class="control-label">Lama Proses</label>
								<input type="text" class="form-control" id="durasidry2" placeholder="Durasi">
							</div>
							<div class="form-group col-md-12">
								<label class="control-label">Keterangan</label>
								<textarea class="form-control" id="ketdry2" placeholder="Keterangan"></textarea>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" onclick="save_methoddry2()" class="btn btn-primary waves-effect text-left">Save</button>
							<button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Close</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('javascript')
<script src="{{ asset('node_modules/moment/moment.js') }}">
</script><script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
<script src="{{ asset('node_modules/datatables/datatables.min.js') }}"></script>
<script src="https://cdn.jsdelivr.net/gh/dmuy/MDTimePicker/mdtimepicker.js
"></script>
<script>
	$('#stock_showitdry2').hide();
	let token = $("meta[name='csrf-token']").attr("content");

	const jenis = {!! json_encode($jenis) !!};
	jenis.unshift({id_jenis: '',jenis: ''});

	var table = $('#table-drying2').DataTable({
		"lengthMenu": [
		[3,5,10, 25, 50, 100, 150, 200, -1],
		[3,5,10, 25, 50, 100, 150, 200, "All"]
		],
		"order": [
		[0, 'desc']
		],
		processing: true,
		serverSide: true,
		ajax: '/datadrying2/json',
		columns: [
		{ data: 'DT_RowIndex', name: 'DT_RowIndex' },
		{ data: 'tanggal_dry2', name: 'tanggal_dry2' },
		{ data: 'produk', name: 'produk' },
		{ data: 'jenis', name: 'jenis' },
		{ data: 'lama_dry2', name: 'lama_dry2' },
		{ data: 'awlbatang', name: 'awlbatang' },
		{ data: 'tambah_dry2', name: 'tambah_dry2' },
		{ data: 'hasil_akhirdry2', name: 'hasil_akhirdry2' },
		{ data: 'keterangan_dry2', name: 'keterangan_dry2' },
		{
			data: null,
			"bSortable": false,
			mRender: function (o) { 
				return '<button type="button" class="btn btn-info btn-rounded btn-xs" id="update-drying2" data-title="Update Drying2 | ID: '+ o.id +'" data-link="/datadrying2/upp/'+ o.id +'/'+ o.tambah_dry2 +'" data-tanggal_drying2="'+ o.tanggal_dry2 +'" data-jenis="'+ o.id_jenis +'" data-produk="'+ o.id_produk +'" data-durasi="'+ o.lama_dry2 +'" data-ket="'+ o.keterangan_dry2 +'" data-stockbatang="'+ o.awlbatang +'" data-batang="'+ o.tambah_dry2 +'" data-idfrezzer="'+ o.stock_frezer_id +'" data-iddrying2="'+ o.stock_drying2_id +'"><i class="mdi mdi-dots-horizontal"></i></button><button type="button" class="btn btn-danger btn-rounded btn-xs" id="delete-drying2" data-link="/datadrying2/del/'+ o.id +'/'+ o.tambah_dry2 +'" data-idfrezzer="'+ o.stock_frezer_id +'" data-iddrying2="'+ o.stock_drying2_id +'"><i class="fa fa-trash"></i></button>'; }
			},
			],
			"displayLength": 10,
		});
	$('#table-drying2 tbody').on('click', 'tr.group', function() {
		var currentOrder = table.order()[0];
		if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
			table.order([2, 'desc']).draw();
		} else {
			table.order([2, 'asc']).draw();
		}
	});
	$('#table-drying2 tbody').on('click', 'tr.group', function() {
		var currentOrder = table.order()[0];
		if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
			table.order([2, 'desc']).draw();
		} else {
			table.order([2, 'asc']).draw();
		}
	});

	function getdatasearchdry2(position1) {
		let jenis = $('#jenis_indry2').val();
		let produk = $('#produk_indry2').val();

		$.ajax({
			url: '/datadrying2/getsearchfill',
			type: 'POST',
			dataType: 'json',
			data: {
				"_token": token,
				produk: produk,
				jenis: jenis
			},
		})
		.done(function(data) {
			$('#iddrying2').val(data.iddrying2);
			$('#idfrezzerdry2').val(data.idfrezzer);
			$('#' + position1).val(data.jml_fre);
		});
		
	}

	function ljenis(position,like) {
		let opt = '';
		for (let i = 0; i < jenis.length; i++) {
			opt += '<option value="'+ jenis[i].id_jenis +'"';
			if (jenis[i].id_jenis == like) {
				opt += 'selected';
			}
			opt += '>'+ jenis[i].jenis +'</option>';
		}
		$('#' + position).html(opt);
	}

	function lproduk(position,based,like) {
		$.ajax({
			url: '/datafilling/getproduk',
			type: 'POST',
			dataType: 'json',
			data: {
				"_token": token,
				jenis: based
			},
		})
		.done(function(data) {
			let opt = '<option value=""></option>';
			for(let i = 0; i < data.length; i++) {
				opt += '<option value="'+ data[i].id_produk +'"';
				if (data[i].id_produk == like) {
					opt += 'selected';
				}
				opt += ' data-id="'+ data[i].relid +'">'+ data[i].produk +'</option>';
			}
			$('#' + position).html(opt);
		});
	}

	function seacrh_datadry2() {
		getdatasearchdry2('batangfre');
	}

	function save_methoddry2() {
		let link = $('#form-drying2').attr('action');
		let iddrying2 = $('#iddrying2').val();
		let idfrezzer = $('#idfrezzerdry2').val();
		let idjnpro = $('#idjnprodry2').val();
		let tanggal_drying2 = $('#tanggal_drying2').val();
		let jenis_in = $('#jenis_indry2').val();
		let produk_in = $('#produk_indry2').val();
		let stockbatang = $('#batangfre').val();
		let batangjml = $('#batangjmldry2').val();
		let durasi = $('#durasidry2').val();
		let ket = $('#ketdry2').val();
		$.ajax({
			url: link,
			type: 'POST',
			dataType: 'json',
			data: {
				"_token": token,
				tanggal_drying2: tanggal_drying2,
				jenis: jenis_in,
				produk: produk_in,
				durasi:durasi,
				stockbatang: stockbatang,
				batangjml: batangjml,
				ket:ket,
				idfrezzer: idfrezzer,
				iddrying2: iddrying2,
				idjnpro:idjnpro
			},
		})
		.done(function(data) {
			alert(data);
			// console.log(data);
			$('#table-drying2').DataTable().ajax.reload();
			$('#modal-drying2').modal('toggle');
		});
		
	}

	$(document).on('click', '#tambah-drying2', function(event) {
		let clik = $(this);
		$('.modal-header #title').text('Tambah drying2');
		$('#form-drying2').attr('action', clik.data('link'));
		$('#form-drying2').find('input').val('');
		$('#form-drying2').find('select').val('');
		$('#form-drying2').find('textarea').val('');
		$("#tanggal_drying2").datetimepicker({
			format: 'YYYY-MM-DD HH:mm:ss',
		}).data("DateTimePicker").date(new Date());
		$('#durasidry2').mdtimepicker({format: 'hh:mm'});
		$('#stock_showitdry2').fadeIn();
		$('#jenisdry2').removeAttr('disabled');
		$('#produkdry2').removeAttr('disabled');
		ljenis('jenisdry2');
		$(document).on('change', '#jenisdry2', function(event) {
			let jam = $(this).val();
			$('#jenis_indry2').val(jam);
			lproduk('produkdry2',jam);
			$(document).on('change', '#produkdry2', function(event) {
				let pwe = $(this).val();
				$('#produk_indry2').val(pwe);
				let kk = $(this).find(':selected').data('id');
				$('#idjnprodry2').val(kk);
			});
		});
		$('#modal-drying2').modal('show');
	});

	$(document).on('click', '#update-drying2', function(event) {
		let click = $(this);
		$('.modal-header #title').text(click.data('title'));
		$('#form-drying2').attr('action', click.data('link'));
		$('#form-drying2').find('input').val('');
		$('#form-drying2').find('select').val('');
		$('#form-drying2').find('textarea').val('');
		$('#stock_showitdry2').hide();
		$('#jenisdry2').attr('disabled','disabled');
		$('#produkdry2').attr('disabled','disabled');
		ljenis('jenisdry2',click.data('jenis'));
		lproduk('produkdry2',click.data('jenis'),click.data('produk'));

		$("#tanggal_drying2").datetimepicker({
			format: 'YYYY-MM-DD HH:mm:ss',
		}).data("DateTimePicker").date(click.data('tanggal_drying2'));
		$('#durasidry2').mdtimepicker({format: 'hh:mm'});
		$('#jenis_indry2').val(click.data('jenis'));
		$('#produk_indry2').val(click.data('produk'));
		$('#batangfre').val(click.data('stockbatang'));
		$('#batangjmldry2').val(click.data('batang'));
		$('#iddrying2').val(click.data('iddrying2'));
		$('#idfrezzerdry2').val(click.data('idfrezzer'));
		$('#durasidry2').val(click.data('durasi'));
		$('#ketdry2').val(click.data('ket'));
		$('#modal-drying2').modal('show');
	});

	$(document).on('click', '#delete-drying2', function(event) {
		let hjs = confirm('Apakah anda yakin ingin menghapus data ini ?');
		if (hjs) {
			let link = $(this).data("link");
			let idfrezzer = $(this).data('idfrezzer');
			let iddrying2 = $(this).data('iddrying2');
			$.ajax(
			{
				url: link,
				type: 'DELETE',
				data: {
					"_token": token,
					idfrezzer: idfrezzer,
					iddrying2: iddrying2,
				},
				success: function (data){
					alert(data);
					$('#table-drying2').DataTable().ajax.reload();
				}
			});
		}
	});
</script>
@endsection