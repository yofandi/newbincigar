@extends('../../layout')

@section('title')
<title>BIN - ERP : Merek Cukai</title>
<link type="text/css" rel="stylesheet" href="{{ asset('node_modules/jsgrid/jsgrid.min.css') }}" />
<link type="text/css" rel="stylesheet" href="{{ asset('node_modules/jsgrid/jsgrid-theme.min.css') }}" />
<style type="text/css">
	.hide
	{
		display:none;
	}
</style>
@endsection

@section('content')
<div class="row page-titles">
	<div class="col-md-5 align-self-center">
		<h3 class="text-themecolor">Merek Cukai</h3>
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a href="javascript:void(0)">Referensi</a></li>
			<li class="breadcrumb-item active">Jenis</li>
		</ol>
	</div>
</div>
<div class="row">
	<div class="col-md-12 m-t-30">
		<div class="card">
			<div class="card-body">
				<h4 class="card-title">Merek Cukai</h4>
				<div class="row">
					<div class="col-md-8"></div>
					<div class="col-md-2 offset-md-2">
						<button type="button" onclick="Refresh()" class="btn btn-xs btn-icons btn-secondary">Refresh</button>
					</div>
				</div>
				<div id="grid_merekcukau"></div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('javascript')
<script type="text/javascript" src="{{ asset('node_modules/jsgrid/jsgrid.min.js') }}"></script>
@include('home.referensi.merek_cukai.script_merek_cukai')
@endsection