<script>
	$('#stock_showitfumi').hide();
	$('#loadingdeletefumi').hide();
	$('.spinner_nekrfumi').hide();

	var tablefumi = $('#table-fumigasi').DataTable({
		"lengthMenu": [
		[3,5,10, 25, 50, 100, 150, 200, -1],
		[3,5,10, 25, 50, 100, 150, 200, "All"]
		],
		"order": [
		[0, 'desc']
		],
		processing: true,
		serverSide: true,
		ajax: '/datafumigasi/json',
		columns: [
		{ data: 'DT_RowIndex', name: 'DT_RowIndex' },
		{ data: 'tanggal_fum', name: 'tanggal_fum' },
		{ data: 'produk', name: 'produk' },
		{ data: 'jenis', name: 'jenis' },
		{ data: 'lama_fum', name: 'lama_fum' },
		{ data: 'awlbatang', name: 'awlbatang' },
		{ data: 'tambah_fum', name: 'tambah_fum' },
		{ data: 'hasil_akhirfum', name: 'hasil_akhirfum' },
		{ data: 'keterangan_fum', name: 'keterangan_fum' },
		{
			data: null,
			"bSortable": false,
			mRender: function (o) { 
				return '<button type="button" class="btn btn-info btn-rounded btn-xs" id="update-fumigasi" data-title="Update Fumigasi | ID: '+ o.id +'" data-link="/datafumigasi/upp/'+ o.id +'/'+ o.tambah_fum +'" data-tanggal_fumigasi="'+ o.tanggal_fum +'" data-jenis="'+ o.id_jenis +'" data-produk="'+ o.id_produk +'" data-durasi="'+ o.lama_fum +'" data-ket="'+ o.keterangan_fum +'" data-stockbatang="'+ o.awlbatang +'" data-batang="'+ o.tambah_fum +'" data-idfumigasi="'+ o.stock_fumigasi_id +'" data-iddrying2="'+ o.stock_drying2_id +'"><i class="mdi mdi-dots-horizontal"></i></button><button type="button" class="btn btn-danger btn-rounded btn-xs" id="delete-fumigasi" data-link="/datafumigasi/del/'+ o.id +'/'+ o.tambah_fum +'" data-idfumigasi="'+ o.stock_fumigasi_id +'" data-iddrying2="'+ o.stock_drying2_id +'"><i class="fa fa-trash"></i></button>'; }
			},
			],
			"displayLength": 10,
		});
	$('#table-fumigasi tbody').on('click', 'tr.group', function() {
		var currentOrder = tablefumi.order()[0];
		if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
			tablefumi.order([2, 'desc']).draw();
		} else {
			tablefumi.order([2, 'asc']).draw();
		}
	});
	$('#table-fumigasi tbody').on('click', 'tr.group', function() {
		var currentOrder = tablefumi.order()[0];
		if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
			tablefumi.order([2, 'desc']).draw();
		} else {
			tablefumi.order([2, 'asc']).draw();
		}
	});

	function Refresh_fumigasi() {
		$('#table-fumigasi').DataTable().ajax.reload();
	}

	function getdatasearchfumi(position1) {
		let jenis = $('#jenis_infumi').val();
		let produk = $('#produk_infumi').val();

		$.ajax({
			url: '/datafumigasi/getsearchfill',
			type: 'POST',
			dataType: 'json',
			data: {
				"_token": token,
				produk: produk,
				jenis: jenis
			},
		})
		.done(function(data) {
			$('#iddrying2fumi').val(data.iddrying2);
			$('#idfumigasi').val(data.idfumigasi);
			$('#' + position1).val(data.jml_dry2);
		});
		
	}

	function seacrh_datafumi() {
		getdatasearchfumi('batangdry2fumi');
	}

	function save_methodfumi() {
		let link = $('#form-fumigasi').attr('action');
		let iddrying2 = $('#iddrying2fumi').val();
		let idfumigasi = $('#idfumigasi').val();
		let idjnpro = $('#idjnprofumi').val();
		let tanggal_fumigasi = $('#tanggal_fumigasi').val();
		let jenis_in = $('#jenis_infumi').val();
		let produk_in = $('#produk_infumi').val();
		let stockbatang = $('#batangdry2fumi').val();
		let batangjml = $('#batangjmlfumi').val();
		let durasi = $('#durasifumi').val();
		let ket = $('#ketfumi').val();
		$.ajax({
			url: link,
			type: 'POST',
			dataType: 'json',
			data: {
				"_token": token,
				tanggal_fumigasi: tanggal_fumigasi,
				jenis: jenis_in,
				produk: produk_in,
				durasi:durasi,
				stockbatang: stockbatang,
				batangjml: batangjml,
				ket:ket,
				idfumigasi: idfumigasi,
				iddrying2: iddrying2,
				idjnpro:idjnpro
			},
			beforeSend: function() {
				isProcessing = true;
				$('.spinner_nekrfumi').fadeIn();
			},
			success: function(data) {
				$('.spinner_nekrfumi').hide();
				isProcessing = false;
				alert(data);
				$('#table-fumigasi').DataTable().ajax.reload();
				$('#modal-fumigasi').modal('toggle');
			}
		});
		
	}

	$(document).on('click', '#tambah-fumigasi', function(event) {
		let clik = $(this);
		$('.modal-header #title').text('Tambah Fumigasi');
		$('#form-fumigasi').attr('action', clik.data('link'));
		$('#form-fumigasi').find('input').val('');
		$('#form-fumigasi').find('select').val('');
		$('#form-fumigasi').find('textarea').val('');
		$("#tanggal_fumigasi").datetimepicker({
			format: 'YYYY-MM-DD HH:mm:ss',
		}).data("DateTimePicker").date(new Date());
		// $('#durasifumi').mdtimepicker({format: 'hh:mm'});
		$('#stock_showitfumi').fadeIn();
		$('#jenisfumi').removeAttr('disabled');
		$('#produkfumi').removeAttr('disabled');
		ljenis('jenisfumi');
		$(document).on('change', '#jenisfumi', function(event) {
			let jam = $(this).val();
			$('#jenis_infumi').val(jam);
			lproduk('produkfumi',jam);
			$(document).on('change', '#produkfumi', function(event) {
				let pwe = $(this).val();
				$('#produk_infumi').val(pwe);
				let kk = $(this).find(':selected').data('id');
				$('#idjnprofumi').val(kk);
			});
		});
		$('#modal-fumigasi').modal('show');
	});

	$(document).on('click', '#update-fumigasi', function(event) {
		let click = $(this);
		$('.modal-header #title').text(click.data('title'));
		$('#form-fumigasi').attr('action', click.data('link'));
		$('#form-fumigasi').find('input').val('');
		$('#form-fumigasi').find('select').val('');
		$('#form-fumigasi').find('textarea').val('');
		$('#stock_showitfumi').hide();
		$('#jenisfumi').attr('disabled','disabled');
		$('#produkfumi').attr('disabled','disabled');
		ljenis('jenisfumi',click.data('jenis'));
		lproduk('produkfumi',click.data('jenis'),click.data('produk'));

		$("#tanggal_fumigasi").datetimepicker({
			format: 'YYYY-MM-DD HH:mm:ss',
		}).data("DateTimePicker").date(click.data('tanggal_fumigasi'));
		// $('#durasifumi').mdtimepicker({format: 'hh:mm'});
		$('#jenis_infumi').val(click.data('jenis'));
		$('#produk_infumi').val(click.data('produk'));
		$('#batangdry2fumi').val(click.data('stockbatang'));
		$('#batangjmlfumi').val(click.data('batang'));
		$('#iddrying2fumi').val(click.data('iddrying2'));
		$('#idfumigasi').val(click.data('idfumigasi'));
		$('#durasifumi').val(click.data('durasi'));
		$('#ketfumi').val(click.data('ket'));
		$('#modal-fumigasi').modal('show');
	});

	$(document).on('click', '#delete-fumigasi', function(event) {
		let hebg = confirm('Apakah anda yakin ingin menghapus data ini ?');
		if (hebg) {
			let link = $(this).data("link");
			let idfumigasi = $(this).data('idfumigasi');
			let iddrying2 = $(this).data('iddrying2');
			$.ajax(
			{
				url: link,
				type: 'DELETE',
				data: {
					"_token": token,
					idfumigasi: idfumigasi,
					iddrying2: iddrying2,
				},
				beforeSend: function() {
					isProcessing = true;
					$('#loadingdeletefumi').fadeIn();
				},
				success: function (data){
					$('#loadingdeletefumi').hide();
					isProcessing = false;
					alert(data);
					$('#table-fumigasi').DataTable().ajax.reload();
				}
			});
		}
	});
</script>