<div class="modal fade" id="modal-hargastiker" tabindex="-1" role="dialog" aria-labelledby="title" aria-hidden="true" style="display: none;">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<div class="card">
				<div class="card-header">
					<div class="modal-header">
						<h4 class="modal-title" id="">Tambah</h4>
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					</div>
				</div>
				<div class="card-body">
					<form id="form-hargastiker" method="POST">
						<div class="modal-body row">
							<div class="form-group col-md-12 fa-3x spinner_nekrsti" align="center">
								<i class="fas fa-spinner fa-spin"></i>
							</div>
							<div class="form-group col-md-12">
								<label class="control-label">Tanggal</label>
								<input type="text" class="form-control" id="tanggal_stiker" placeholder="Tanggal">
							</div>
							<div class="form-group col-md-12">
								<label class="control-label">Kode Harga Stiker</label>
								<input type="text" class="form-control" id="kode_hstiker" placeholder="Kode Harga Stiker">
							</div>
							<div class="form-group col-md-6">
								<label class="control-label">Produk</label>
								<select class="form-control" id="produk-stiker"></select>
							</div>
							<div class="form-group col-md-6">
								<label class="control-label">Sub Produk</label>
								<select class="form-control" id="subproduk-stiker"></select>
							</div>
							<div class="form-group col-md-6">
								<label class="control-label">Harga Stiker Luar per satuan</label>
								<input type="text" class="form-control" id="harga_satlstiker">
							</div>
							<div class="form-group col-md-6">
								<label class="control-label">Harga Stiker Dalam per satuan</label>
								<input type="text" class="form-control" id="harga_satdstiker">
							</div>
							<div class="form-group col-md-12">
								<label class="control-label">Keterangan</label>
								<textarea id="keterangan-stiker" class="form-control" placeholder="keterangan"></textarea>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" onclick="save_methodstiker()" class="btn btn-primary waves-effect text-left">Save</button>
							<button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Close</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>