<script>
	$('#stock_showitfill').hide();
	$('#loadingdeletefill').hide();
	$('.spinner_nekrfill').hide();

	var tablefill = $('#table-filling').DataTable({
		"lengthMenu": [
		[3,5,10, 25, 50, 100, 150, 200, -1],
		[3,5,10, 25, 50, 100, 150, 200, "All"]
		],
		"order": [
		[0, 'desc']
		],
		processing: true,
		serverSide: true,
		ajax: '/datafilling/json',
		columns: [
		{ data: 'DT_RowIndex', name: 'DT_RowIndex' },
		{ data: 'tanggal_fill', name: 'tanggal_fill' },
		{ data: 'produk', name: 'produk' },
		{ data: 'jenis', name: 'jenis' },
		{ data: 'stockawal', name: 'stockawal' },
		{ data: 'weisfmasuk', name: 'weisfmasuk' },
		{ data: 'terpakaiprof', name: 'terpakaiprof' },
		{ data: 'terpakaiweisf', name: 'terpakaiweisf' },
		{ data: 'sisaprof', name: 'sisaprof' },
		{ data: 'hasil_fill', name: 'hasil_fill' },
		{
			data: null,
			"bSortable": false,
			mRender: function (o) { 
				return '<button type="button" class="btn btn-info btn-rounded btn-xs" id="update-filling" data-title="Update Filling | ID: '+ o.id +'" data-link="/datafilling/upp/'+ o.id +'/'+ o.weisfmasuk +'/'+ o.terpakaiprof +'/'+ o.terpakaiweisf +'/'+ o.hasil_fill +'" data-tanggal_filling="'+ o.tanggal_fill +'" data-jenis="'+ o.id_jenis +'" data-produk="'+ o.id_produk +'" data-stockpro="'+ o.stockawal +'" data-stockweis="'+ o.stockawalweis +'" data-masukweis="'+ o.weisfmasuk +'" data-terpakaiprof="'+ o.terpakaiprof +'" data-terpakaiweisf="'+ o.terpakaiweisf +'" data-batang="'+ o.hasil_fill +'" data-idprobaku="'+ o.stock_probaku_id +'" data-idfilling="'+ o.stock_filling_id +'"><i class="mdi mdi-dots-horizontal"></i></button><button type="button" class="btn btn-danger btn-rounded btn-xs" id="delete-filling" data-link="/datafilling/del/'+ o.id +'/'+ o.weisfmasuk +'/'+ o.terpakaiprof +'/'+ o.terpakaiweisf +'/'+ o.hasil_fill +'" data-idprobaku="'+ o.stock_probaku_id +'" data-idfilling="'+ o.stock_filling_id +'"><i class="fa fa-trash"></i></button>'; }
			},
			],
			"displayLength": 10,
		});
	$('#table-filling tbody').on('click', 'tr.group', function() {
		var currentOrder = tablefill.order()[0];
		if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
			tablefill.order([2, 'desc']).draw();
		} else {
			table.order([2, 'asc']).draw();
		}
	});
	$('#table-filling tbody').on('click', 'tr.group', function() {
		var currentOrder = tablefill.order()[0];
		if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
			tablefill.order([2, 'desc']).draw();
		} else {
			tablefill.order([2, 'asc']).draw();
		}
	});

	function Refresh_filling() {
		$('#table-filling').DataTable().ajax.reload();
	}

	function getdatasearchfill(position1,position2) {
		let jenis = $('#jenis_in').val();
		let produk = $('#produk_in').val();

		$.ajax({
			url: '/datafilling/getsearchfill',
			type: 'POST',
			dataType: 'json',
			data: {
				"_token": token,
				produk: produk,
				jenis: jenis
			},
		})
		.done(function(data) {
			$('#idfilling').val(data.idfilling);
			$('#idprobaku').val(data.id);
			$('#' + position1).val(data.jml_produksi);
			$('#' + position2).val(data.stock_weisf);
		});
		
	}
	// filling proses
	function seacrh_datafill() {
		getdatasearchfill('stockpro','stockweisf');
	}

	function save_methodfilling() {
		let link = $('#form-filling').attr('action');
		let idprobaku = $('#idprobaku').val();
		let idfilling = $('#idfilling').val();
		let tanggal_filling = $('#tanggal_filling').val();
		let jenis_in = $('#jenis_in').val();
		let produk_in = $('#produk_in').val();
		let stockpro = $('#stockpro').val();
		let stockweisf = $('#stockweisf').val();
		let masukweisf = $('#masukweisf').val();
		let terpakaipro = $('#terpakaipro').val();
		let terpakaiweisf = $('#terpakaiweisf').val();
		let batangjml = $('#batangjml').val();
		let idjnpro = $('#idjnpro').val();
		$.ajax({
			url: link,
			type: 'POST',
			dataType: 'json',
			data: {
				"_token": token,
				tanggal_filling: tanggal_filling,
				jenis: jenis_in,
				produk: produk_in,
				stockpro: stockpro,
				stockweisf: stockweisf,
				masukweisf:masukweisf,
				terpakaipro: terpakaipro,
				terpakaiweisf: terpakaiweisf,
				batangjml: batangjml,
				idprobaku: idprobaku,
				idfilling: idfilling,
				idjnpro:idjnpro
			},
			beforeSend: function() {
				isProcessing = true;
				$('.spinner_nekrfill').fadeIn();
			},
			success: function(data) {	
				$('.spinner_nekrfill').hide();
				isProcessing = false;
				alert(data);
				Refresh_filling();
				$('#modal-filling').modal('toggle');
			}
		});
	}

	$(document).on('click', '#tambah-filling', function(event) {
		let clik = $(this);
		$('.modal-header #title').text('Tambah filling');
		$('#form-filling').attr('action', clik.data('link'));
		$('#form-filling').find('input').val('');
		$('#form-filling').find('select').val('');
		$("#tanggal_filling").datetimepicker({
			format: 'YYYY-MM-DD HH:mm:ss',
		}).data("DateTimePicker").date(new Date());
		$('#stock_showitfill').fadeIn();
		$('#jenis').removeAttr('disabled');
		$('#produk').removeAttr('disabled');
		ljenis('jenis');
		$(document).on('change', '#jenis', function(event) {
			let jam = $(this).val();
			$('#jenis_in').val(jam);
			lproduk('produk',jam);
			$(document).on('change', '#produk', function(event) {
				let pwe = $(this).val();
				$('#produk_in').val(pwe);
				let kk = $(this).find(':selected').data('id');
				$('#idjnpro').val(kk);
			});
		});
		$('#modal-filling').modal('show');
	});

	$(document).on('click', '#update-filling', function(event) {
		let click = $(this);
		$('.modal-header #title').text(click.data('title'));
		$('#form-filling').attr('action', click.data('link'));
		$('#form-filling').find('input').val('');
		$('#form-filling').find('select').val('');
		$('#stock_showitfill').hide();
		$('#jenis').attr('disabled','disabled');
		$('#produk').attr('disabled','disabled');
		ljenis('jenis',click.data('jenis'));
		lproduk('produk',click.data('jenis'),click.data('produk'));

		$("#tanggal_filling").datetimepicker({
			format: 'YYYY-MM-DD HH:mm:ss',
		}).data("DateTimePicker").date(click.data('tanggal_filling'));
		$('#jenis_in').val(click.data('jenis'));
		$('#produk_in').val(click.data('produk'));
		$('#stockpro').val(click.data('stockpro'));
		$('#stockweisf').val(click.data('stockweis'));
		$('#masukweisf').val(click.data('masukweis'));
		$('#terpakaipro').val(click.data('terpakaiprof'));
		$('#terpakaiweisf').val(click.data('terpakaiweisf'));
		$('#batangjml').val(click.data('batang'));
		$('#idprobaku').val(click.data('idprobaku'));
		$('#idfilling').val(click.data('idfilling'));

		$('#modal-filling').modal('show');
	});

	$(document).on('click', '#delete-filling', function(event) {
		let sjn = confirm('Apakah anda yakin ingin menghapus data ini ?');
		if (sjn) {
			let link = $(this).data("link");
			let idprobaku = $(this).data('idprobaku');
			let idfilling = $(this).data('idfilling');
			$.ajax(
			{
				url: link,
				type: 'DELETE',
				data: {
					"_token": token,
					idprobaku: idprobaku,
					idfilling: idfilling,
				},
				beforeSend: function() {
					isProcessing = true;
					$('#loadingdeletefill').fadeIn();
				},
				success: function (data){
					$('#loadingdeletefill').hide();
					isProcessing = false;
					alert(data);
					Refresh_filling();
				}
			});
		}
	});
// filling ending
</script>