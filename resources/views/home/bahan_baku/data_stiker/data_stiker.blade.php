@extends('../../layout')

@section('title')
<title>BIN - ERP : Data Stiker</title>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css" />
@endsection

@section('content')
<div class="row page-titles">
	<div class="col-md-5 align-self-center">
		<h3 class="text-themecolor">Data Stiker</h3>
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a href="javascript:void(0)">Bahan baku</a></li>
			<li class="breadcrumb-item active">Data Stiker</li>
		</ol>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="row">
			<div class="col-md-6">
				<legend>Stock Bahan (Stiker)</legend>
			</div>
			<div class="col-md-2 offset-md-4">
				<button type="button" data-link="{{ route('datastiker.post') }}" class="btn btn-success btn-rounded btn-icons" id="tambah-stiker"><i class="fa fa-plus"></i></button>
				<button type="button" class="btn btn-secondary btn-rounded btn-icons" onclick="refresh()"><i class="mdi mdi-refresh"></i></button>
			</div>
			<div class="col-md-12" align="center" id="loadingdelete">
				<h5>Data tengah diproses. Mohon tunggu sejenak!</h5>
			</div>
			<div class="table-responsive">
				<table id="table_stiker" class="display nowrap table table-sm table-hover table-striped table-bordered">
					<thead>
						<tr align="center">
							<th rowspan ="2">No.</th>
							<th rowspan ="2">Tanggal</th>
							<th rowspan="2">Produk</th>
							<th rowspan="2">Sub Kode | Produk</th>
							<th colspan="3" class="table-primary">LUAR</th>
							<th colspan="3" class="table-success">DALAM</th>
							<th colspan="3" class="table-danger">TOTAL</th>
							<th rowspan="2" class="table-secondary">Ket</th>
							<th rowspan="2" class="table-secondary">Aksi</th>
						</tr>
						<tr align="center">
							<th class="table-primary">Masuk</th>
							<th class="table-primary">Terpakai</th>
							<th class="table-primary">Sisa</th>
							<th class="table-success">Masuk</th>
							<th class="table-success">Terpakai</th>
							<th class="table-success">Sisa</th>
							<th class="table-danger">Masuk</th>
							<th class="table-danger">Terpakai</th>
							<th class="table-danger">Sisa</th>
						</tr>
					</thead>
				</table>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="modal-stiker" tabindex="-1" role="dialog" aria-labelledby="title" aria-hidden="true" style="display: none;">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="card">
				<div class="card-header">
					<div class="modal-header">
						<h4 class="modal-title" id="title"></h4>
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					</div>
				</div>
				<div class="card-body">
					<form id="form-stiker" method="POST">
						<div class="modal-body row">
							<div class="form-group col-md-12 fa-3x spinner_nekr" align="center">
								<i class="fas fa-spinner fa-spin"></i>
							</div>
							<div class="form-group col-md-12">
								<label class="control-label">Tanggal</label>
								<input type="text" class="form-control date_sam" id="tanggal_hisstik">
							</div>
							<div class="form-group col-md-6">
								<label class="control-label">Produk</label>
								<select class="form-control" id="produk"></select>
								<input type="hidden" id="produk_in">
							</div>
							<div class="form-group col-md-6">
								<label class="control-label">Subproduk</label>
								<select class="form-control" id="subproduk"></select>
								<input type="hidden" id="subproduk_in">
							</div>
							<div class="form-group col-md-12">
								<button type="button" id="showhidd" onclick="searchdatastiker()" class="btn btn-sm btn-block btn-info">Cari</button>
							</div>
							<div class="form-group col-md-6">
								<label class="control-label">Stock Luar (Saat ini/Awal)</label>
								<input type="number" class="form-control" id="stockluar" value="0" placeholder="Stock Luar (Saat ini/Awal)" readonly>
							</div>
							<div class="form-group col-md-6">
								<label class="control-label">Stock Dalam (Saat ini/Awal)</label>
								<input type="number" class="form-control" id="stockdalam" value="0" placeholder="Stock Dalam (Saat ini/Awal)" readonly>
							</div>
							<div class="col-md-12" id="hiddenval">
								<div class="row">
									<div class="form-group col-md-6">
										<label class="control-label">Harga Stiker Luar</label>
										<input type="text" class="form-control" id="harga_luar" value="0" readonly>
									</div>
									<div class="form-group col-md-6">
										<label class="control-label">Harga Stiker Dalam</label>
										<input type="text" class="form-control" id="harga_dalam" value="0" readonly>
									</div>
								</div>
							</div>
							<div class="form-group col-md-6">
								<label class="control-label">Masuk (Luar)</label>
								<input type="number" class="form-control" id="masukluar" placeholder="Stock Masuk Luar">
							</div>
							<div class="form-group col-md-6">
								<label class="control-label">Masuk (Dalam)</label>
								<input type="number" class="form-control" id="masukdalam" placeholder="Stock Masuk Dalam">
							</div>
							<div class="form-group col-md-6">
								<label class="control-label">Terpakai (Luar)</label>
								<input type="number" class="form-control" id="terpakailuar" placeholder="Stock Terpakai Luar">
							</div>
							<div class="form-group col-md-6">
								<label class="control-label">Terpakai (Dalam)</label>
								<input type="number" class="form-control" id="terpakaidalam" placeholder="Stock Terpakai Dalam">
							</div>
							<div class="form-group col-md-12">
								<label class="control-label">Keterangan</label>
								<textarea class="form-control" id="ket_sti" placeholder="Keterangan"></textarea>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" onclick="save_method()" class="btn btn-primary waves-effect text-left">Save</button>
							<button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Close</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('javascript')
<script src="{{ asset('node_modules/moment/moment.js') }}">
</script><script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
<script src="{{ asset('node_modules/datatables/datatables.min.js') }}"></script>
@include('home.bahan_baku.data_stiker.script_stiker')
@endsection