<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Crypt;
use Auth;
use DataTables;
use App\User;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('layout');
    }

    public function loadlevel()
    {
        $table = DB::table('level_user')->get();
        echo json_encode($table);
    }

    public function profile()
    {
        $data['level'] = DB::table('level_user')->where('id', Auth::user()->level_user_id_level)->first();
        return view('home.manage_profile.profile',$data);
    }

    public function update_profile(Request $request,$id)
    {   
        $data = [
            'name' => $request->name,
            'username' => $request->username,
            'email' => $request->email,
            'no_hp' => $request->no_hp,
            'alamat_user' => $request->alamat_user
        ];
        $query = User::where('id',$id)
        ->update($data);
        if ($query > 0) {
            $request->session()->flash('alert-success', 'User was successful updated!');
        } else {
            $request->session()->flash('alert-danger', 'User was failure updated!');
        }
        return redirect()->route('profile.read');
    }

    public function doupload(Request $request,$id)
    {
        $cek = User::find($id);
        if ($cek->upload_foto != '' || !empty($cek->upload_foto)) {
            $link = public_path().'/images/app_user/'.$cek->upload_foto;
            File::delete($link);
        }
        request()->validate(['image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:10240']);
        if ($files = $request->file('image')) {
            $profileImage = 'bincigar' . date('YmdHis') . "." . $files->getClientOriginalExtension();

            Storage::disk('doupload')->put($profileImage,  File::get($files));

            $img = "$profileImage";
        }
        $query = User::where('id',$id)
        ->update(['upload_foto' => $img]);
        if ($query > 0) {
            $request->session()->flash('alert-success', 'User was successful updated Image!');
        } else {
            $request->session()->flash('alert-danger', 'User was failure updated Image!');
        }
        return redirect()->route('profile.read');
    }

    public function change_pass(Request $request,$id)
    {
        $cek = User::find($id);
        if (Hash::check( $request->old_pass,$cek->password)) {
            $query = User::where('id',$id)
            ->update(['password' => Hash::make($request->password)]);
            if ($query > 0) {
                $request->session()->flash('alert-success', 'Password was successful updated!');
            } else {
                $request->session()->flash('alert-danger', 'Password was failure updated!');
            }
        } else {
            $request->session()->flash('alert-danger', '
                Sorry the password you entered is incorrect. Please repeat again!');
        }
        return redirect()->route('profile.read');
    }

    public function user()
    {
        return view('home.referensi.user.user');
    }

    public function getuser()
    {
        $user = DB::select(DB::raw('SELECT (@cnt := @cnt + 1) AS rowNumber,t.*,level_user.level FROM users AS t LEFT JOIN level_user ON t.level_user_id_level = level_user.id CROSS JOIN (SELECT @cnt := 0) AS dummy ORDER BY t.id'));
        return Datatables::of($user)->make(true);
    }

    public function add_user(Request $request)
    {
        $data = [
            'name' => $request->name,
            'username' => $request->username,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'no_hp' => $request->no_hp,
            'alamat_user' => $request->alamat,
            'status_users' => $request->status,
            'level_user_id_level' => $request->level
        ];
        $query = DB::table('users')
        ->insert($data);
        if ($query) {
            $alert = 'User berhasil ditambahkan!';
        } else {
            $alert = 'User gagal ditambahkan!';
        }
        echo json_encode($alert);
    }

    public function update_user(Request $request,$id)
    {   
        $data = [
            'name' => $request->name,
            'username' => $request->username,
            'email' => $request->email,
            'no_hp' => $request->no_hp,
            'alamat_user' => $request->alamat,
            'status_users' => $request->status,
            'level_user_id_level' => $request->level
        ];
        $query = DB::table('users')
        ->where('id',$id)
        ->update($data);
        if ($query) {
            $alert = 'User berhasil diupdate!';
        } else {
            $alert = 'User gagal diupdate!';
        }
        echo json_encode($alert);
    }

    public function update_pass(Request $request,$id)
    {
        $data = [
            'password' => Hash::make($request->password)
        ];
        $query = DB::table('users')
        ->where('id',$id)
        ->update($data);
        if ($query) {
            $alert = 'ID: '. $id .'Password berhasil diupdate!';
        } else {
            $alert = 'ID: '. $id .'Password gagal diupdate!';
        }
        echo json_encode($alert);
    }

    public function delete_user($id)
    {
        $query = User::where('id',$id)->delete();
        if ($query) {
            $alert = 'ID: '. $id .' berhasil dihapus!';
        } else {
            $alert = 'ID: '. $id .' gagal dihapus!';
        }
        echo json_encode($alert);
    }

    public function produk()
    {
        $data['jenis'] = DB::table('jenis')->select('id_jenis','jenis')->get();
        $data['merek_cukai'] = DB::table('merek_cukai')->get();
        return view('home.referensi.produk.produk',$data);
    }

    public function getproduk(Request $request)
    {
        $query = DB::table('produk')
        ->leftJoin('jenis_has_produk', 'produk.id_produk', '=', 'jenis_has_produk.produk_id_produk')
        ->leftJoin('jenis', 'jenis_has_produk.jenis_id_jenis', '=', 'jenis.id_jenis')
        ->select('produk.*','jenis_has_produk.id_jnpro','jenis.id_jenis','jenis.jenis')
        ->where([['jenis.id_jenis','like',$request->id_jenis.'%'],['produk.id_produk','like',$request->id_produk.'%'],['produk.produk','like','%'.$request->produk.'%']])
        ->get();
        return $query->toArray();
    }

    public function insertproduk(Request $request)
    {
        DB::beginTransaction();
        $data = [
            'kode_produk' => $request->input('kode_produk'),
            'produk' => $request->input('produk')
        ];
        $query = DB::table('produk')
        ->insertGetId($data);

        DB::table('jenis_has_produk')
        ->insert(['jenis_id_jenis' => $request->id_jenis,'produk_id_produk' => $query]);
        DB::commit();
    }

    public function putproduk(Request $request)
    {
        DB::beginTransaction();
        $data = [
            'jenis_has_produk.jenis_id_jenis' => $request->id_jenis,
            'produk.kode_produk' => $request->input('kode_produk'),
            'produk.produk' => $request->produk
        ];
        $query = DB::table('produk')
        ->leftJoin('jenis_has_produk', 'produk.id_produk', '=', 'jenis_has_produk.produk_id_produk')
        ->leftJoin('jenis', 'jenis_has_produk.jenis_id_jenis', '=', 'jenis.id_jenis')
        ->where([['jenis_has_produk.id_jnpro',$request->id_jnpro],['produk.id_produk','=',$request->id_produk]])
        ->update($data);
        DB::commit();
    }

    public function delproduk(Request $request)
    {
        $query = DB::table('produk')
        ->where('id_produk',$request->id_produk)
        ->delete();
    }

    public function getkemasan()
    {
        $query = DB::table('kemasan')
        ->get();
        echo json_encode($query);
    }

    public function getsubproduk()
    {
        $query = DB::table('sub_produk')
        ->select('sub_produk.*','produk.kode_produk','produk.produk','kemasan.nama_kemasan')
        ->join('produk','sub_produk.produk_id_produk','produk.id_produk')
        ->join('kemasan','sub_produk.kemasan_id_kemasan','kemasan.id_kemasan')
        ->get();
        return Datatables::of($query)
        ->addIndexColumn()
        ->smart(true)
        ->make(true);
    }

    public function addsubproduk(Request $request)
    {
        $hje = intval(preg_replace('/[^\d.]/', '', $request->sub_hje));
        $tarif = intval(preg_replace('/[^\d.]/', '', $request->sub_tarif));
        $data = [
            'sub_kode' => $request->sub_kode,
            'sub_produk' => $request->sub_name,
            'produk_id_produk' => $request->id_produk,
            'kemasan_id_kemasan' => $request->sub_kemasan,
            'merek_cukai_id' => $request->merekcukai,
            'tarif' => $tarif,
            'hje' => $hje,
            'isi' => $request->sub_isi,
            'berat_jadi' => $request->berat,
            'ring' => $request->ring,
            'image' => NULL
        ];
        $query = DB::table('sub_produk')
        ->insert($data);
        if ($query) {
            $alert = 'Sub Produk berhasil ditambahkan!';
        } else {
            $alert = 'Sub Produk gagal ditambahkan!';
        }
        echo json_encode($alert);
    }

    public function updatesubproduk(Request $request,$id)
    {
        $hje = intval(preg_replace('/[^\d.]/', '', $request->sub_hje));
        $tarif = intval(preg_replace('/[^\d.]/', '', $request->sub_tarif));
        $data = [
            'sub_kode' => $request->sub_kode,
            'sub_produk' => $request->sub_name,
            'produk_id_produk' => $request->id_produk,
            'kemasan_id_kemasan' => $request->sub_kemasan,
            'merek_cukai_id' => $request->merekcukai,
            'tarif' => $tarif,
            'hje' => $hje,
            'isi' => $request->sub_isi,
            'berat_jadi' => $request->berat,
            'ring' => $request->ring,
            'image' => NULL
        ];
        $query = DB::table('sub_produk')
        ->where('id_sub_produk',$id)
        ->update($data);
        if ($query) {
            $alert = 'Sub Produk ID: '.$id.' berhasil diupdate!';
        } else {
            $alert = 'Sub Produk ID: '.$id.' gagal diupdate!';
        }
        echo json_encode($alert);
    }

    public function deletesubproduk($id)
    {
        $query = DB::table('sub_produk')->where('id_sub_produk',$id)->delete();
        if ($query) {
            $alert = 'ID: '. $id .' berhasil dihapus!';
        } else {
            $alert = 'ID: '. $id .' gagal dihapus!';
        }
        echo json_encode($alert);
    }

    public function jenis_tembakau()
    {
        return view('home.referensi.jenis_tembakau.jenis');
    }

    public function getjenis(Request $request)
    {
        $query = DB::table('jenis')
        ->where([['id_jenis','like','%'.$request->id_jenis.'%'],['jenis','like','%'.$request->jenis.'%'],])
        ->get();
        return $query->toArray();
    }

    public function addjenis(Request $request)
    {
        $data = [
            'kode_jenis' => $request->kode_jenis,
            'jenis' => $request->jenis,
            'deskripsi_jenis' => $request->deskripsi_jenis,
            'keterangan_jenis' => $request->keterangan_jenis
        ];
        $query = DB::table('jenis')
        ->insert($data);
    }

    public function putjenis(Request $request)
    {
        $data = [
            'kode_jenis' => $request->kode_jenis,
            'jenis' => $request->jenis,
            'deskripsi_jenis' => $request->deskripsi_jenis,
            'keterangan_jenis' => $request->keterangan_jenis
        ];
        $query = DB::table('jenis')
        ->where('id_jenis',$request->id_jenis)
        ->update($data);
    }

    public function deljenis(Request $request)
    {
        $query = DB::table('jenis')
        ->where('id_jenis',$request->id_jenis)
        ->delete();
    }

    public function kategori()
    {
        return view('home.referensi.kategori.kategori');
    }

    public function getkategori(Request $request)
    {
        $query = DB::table('kategori')
        ->where([['id_kategori','like','%'.$request->id_kategori.'%'],['kategori','like','%'.$request->kategori.'%'],])
        ->get();
        return $query->toArray();
    }

    public function addkategori(Request $request)
    {
        $data = [
            'kategori' => $request->kategori,
            'deskripsi_kategori' => $request->deskripsi_kategori,
            'keterangan_kategori' => $request->keterangan_kategori
        ];
        $query = DB::table('kategori')
        ->insert($data);
    }

    public function putkategori(Request $request)
    {
        $data = [
            'kategori' => $request->kategori,
            'deskripsi_kategori' => $request->deskripsi_kategori,
            'keterangan_kategori' => $request->keterangan_kategori
        ];
        $query = DB::table('kategori')
        ->where('id_kategori',$request->id_kategori)
        ->update($data);
    }

    public function deletekategori(Request $request)
    {
        $query = DB::table('kategori')
        ->where('id_kategori',$request->id_kategori)
        ->delete();
    }

    public function cincin()
    {
        $data['produk'] = DB::table('produk')
        ->select('id_produk','produk')
        ->get();
        return view('home.referensi.cincin.cincin',$data);
    }

    public function getcincin(Request $request)
    {
        $query = DB::table('stock_cincin')
        ->select('stock_cincin.*','produk.id_produk','produk.produk')
        ->join('produk', 'stock_cincin.produk_id_produk', '=', 'produk.id_produk')
        ->get();
        return Datatables::of($query)
        ->addIndexColumn()
        ->make(true);
    }

    public function addcincin(Request $request)
    {
        $cek = DB::table('stock_cincin')->where('produk_id_produk',$request->produk)->count();
        if ($cek > 0) {
            $da = DB::table('stock_cincin')
            ->where('produk_id_produk',$request->produk)
            ->first();

            $jum = $da->stock_qty_cincin + $request->stock;

            $data = [
                'stock_qty_cincin' => $jum
            ];
            $query = DB::table('stock_cincin')
            ->where('produk_id_produk',$request->produk)
            ->update($data);
            if ($query) {
                $alert = 'Stock cincin ID Produk: '.$request->produk.' berhasil diupdate!';
            } else {
                $alert = 'Stock cincin ID Produk: '.$request->produk.' gagal diupdate!';
            }
            echo json_encode($alert);
        } else {
            $data = [
                'stock_qty_cincin' => $request->stock,
                'produk_id_produk' => $request->produk
            ];
            $query = DB::table('stock_cincin')
            ->insert($data);
            if ($query) {
                $alert = 'Stock cincin berhasil ditambahkan!';
            } else {
                $alert = 'Stock cincin gagal ditambahkan!';
            }
            echo json_encode($alert);
        }
    }

    public function updatecincin(Request $request,$id)
    {
        $data = [
            'stock_qty_cincin' => $request->stock,
        ];
        $query = DB::table('stock_cincin')
        ->where('id_stock_cincin',$id)
        ->update($data);
        if ($query) {
            $alert = 'Stock cincin ID Produk: '.$request->produk.' berhasil diupdate!';
        } else {
            $alert = 'Stock cincin ID Produk: '.$request->produk.' gagal diupdate!';
        }
        echo json_encode($alert);
    }

    public function deletecincin($id)
    {
        $query = DB::table('stock_cincin')->where('id_stock_cincin',$id)->delete();
        if ($query) {
            $alert = 'ID Stock Cincin: '. $id .' berhasil dihapus!';
        } else {
            $alert = 'ID Stock Cincin: '. $id .' gagal dihapus!';
        }
        echo json_encode($alert);
    }

    public function cukai()
    {
        $data['produk'] = DB::table('produk')
        ->select('id_produk','produk')
        ->get();
        return view('home.referensi.cukai.cukai',$data);
    }

    public function getoptsubpro(Request $request)
    {
        $query = DB::table('sub_produk')
        ->select('sub_produk.id_sub_produk','sub_produk.sub_kode','sub_produk.sub_produk','kemasan.id_kemasan','kemasan.nama_kemasan')
        ->join('kemasan','sub_produk.kemasan_id_kemasan','=','kemasan.id_kemasan')
        ->where('sub_produk.produk_id_produk',$request->produk)
        ->get();
        echo json_encode($query);
    }

    public function getcukai()
    {
        $query = DB::table('stock_cukai')
        ->select('stock_cukai.*','produk.id_produk','produk.produk','sub_produk.id_sub_produk','sub_produk.sub_kode','sub_produk.sub_produk')
        ->join('produk', 'stock_cukai.sub_produk_produk_id_produk', '=', 'produk.id_produk')
        ->join('sub_produk', 'stock_cukai.sub_produk_id_sub_produk', '=', 'sub_produk.id_sub_produk')
        ->get();
        return Datatables::of($query)
        ->addIndexColumn()
        ->make(true);
    }

    public function addcukai(Request $request)
    {
        $cek = DB::table('stock_cukai')->where([['sub_produk_produk_id_produk','=',$request->produk],['sub_produk_id_sub_produk','=',$request->sub_produk]])->count();
        if ($cek > 0) {
            $da = DB::table('stock_cukai')->where([['sub_produk_produk_id_produk','=',$request->produk],['sub_produk_id_sub_produk','=',$request->sub_produk]])
            ->first();

            $jum = $da->stock_qty_cukai + $request->stock;

            $data = [
                'stock_qty_cukai' => $jum
            ];
            $query = DB::table('stock_cukai')
            ->where([['sub_produk_produk_id_produk','=',$request->produk],['sub_produk_id_sub_produk','=',$request->sub_produk]])
            ->update($data);
            if ($query) {
                $alert = 'Stock cukai ID Sub Produk: '.$request->sub_produk.' berhasil diupdate!';
            } else {
                $alert = 'Stock cukai ID Sub Produk: '.$request->sub_produk.' gagal diupdate!';
            }
            echo json_encode($alert);
        } else {
            $data = [
                'stock_qty_cukai' => $request->stock,
                'sub_produk_produk_id_produk' => $request->produk,
                'sub_produk_id_sub_produk' => $request->sub_produk
            ];
            $query = DB::table('stock_cukai')
            ->insert($data);
            if ($query) {
                $alert = 'Stock cukai berhasil ditambahkan!';
            } else {
                $alert = 'Stock cukai gagal ditambahkan!';
            }
            echo json_encode($alert);
        }
    }

    public function updatecukai(Request $request,$id)
    {
        $data = [
            'stock_qty_cukai' => $request->stock
        ];
        $query = DB::table('stock_cukai')
        ->where('id_stock_cukai',$id)
        ->update($data);
        if ($query) {
            $alert = 'Stock cincin ID Sub Produk: '.$request->sub_produk.' berhasil diupdate!';
        } else {
            $alert = 'Stock cincin ID Sub Produk: '.$request->sub_produk.' gagal diupdate!';
        }
        echo json_encode($alert);
    }

    public function deletecukai($id)
    {
        $query = DB::table('stock_cukai')->where('id_stock_cukai',$id)->delete();
        if ($query) {
            $alert = 'ID Stock Sub Produk: '. $id .' berhasil dihapus!';
        } else {
            $alert = 'ID Stock Sub Produk: '. $id .' gagal dihapus!';
        }
        echo json_encode($alert);
    }

    public function stiker()
    {
        $data['produk'] = DB::table('produk')
        ->select('id_produk','produk')
        ->get();
        return view('home.referensi.stiker.stiker',$data);
    }

    public function getstiker()
    {
        $query = DB::table('stock_stiker')
        ->select('stock_stiker.*','produk.id_produk','produk.produk','sub_produk.id_sub_produk','sub_produk.sub_kode','sub_produk.sub_produk')
        ->join('produk', 'stock_stiker.produk_id_produk', '=', 'produk.id_produk')
        ->join('sub_produk','stock_stiker.sub_produk_id','=','sub_produk.id_sub_produk')
        ->get();
        return Datatables::of($query)
        ->addIndexColumn()
        ->addColumn('kodeproduk',function($op)
        {
            $combine = $op->sub_kode.' | '.$op->sub_produk;
            return $combine;
        })
        ->make(true);
    }

    public function addstiker(Request $request)
    {
        $cek = DB::table('stock_stiker')->where([['produk_id_produk','=',$request->produk],['sub_produk_id','=',$request->subproduk]]);

        if ($cek->count() > 0) {
            $ghet = $cek->first();
            $da = DB::table('stock_stiker')
            ->where('produk_id_produk',$request->produk)
            ->first();

            $jum1 = $da->stock_luar + $request->stock_luar;
            $jum2 = $da->stock_dalam + $request->stock_dalam;

            $data = [
                'stock_luar' => $jum1,
                'stock_dalam' => $jum2,
            ];
            $query = DB::table('stock_stiker')->where('id_stock_stiker',$ghet->id_stock_stiker)->update($data);
            if ($query) {
                $alert = 'Stock Stiker ID Produk: '.$request->produk.' berhasil diupdate!';
            } else {
                $alert = 'Stock Stiker ID Produk: '.$request->produk.' gagal diupdate!';
            }
            echo json_encode($alert);
        } else {
            $data = [
                'stock_luar' => $request->stock_luar,
                'stock_dalam' => $request->stock_dalam,
                'sub_produk_id' => $request->subproduk,
                'produk_id_produk' => $request->produk
            ];
            $query = DB::table('stock_stiker')
            ->insert($data);
            if ($query) {
                $alert = 'Stock Stiker berhasil ditambahkan!';
            } else {
                $alert = 'Stock Stiker gagal ditambahkan!';
            }
            echo json_encode($alert);
        }
    }

    public function updatestiker(Request $request,$id)
    {
        $data = [
            'stock_luar' => $request->stock_luar,
            'stock_dalam' => $request->stock_dalam
        ];
        $query = DB::table('stock_stiker')
        ->where('id_stock_stiker',$id)
        ->update($data);
        if ($query) {
            $alert = 'Stock Stiker ID Produk: '.$request->produk.' berhasil diupdate!';
        } else {
            $alert = 'Stock Stiker ID Produk: '.$request->produk.' gagal diupdate!';
        }
        echo json_encode($alert);
    }

    public function deletestiker($id)
    {
        $query = DB::table('stock_stiker')->where('id_stock_stiker',$id)->delete();
        if ($query) {
            $alert = 'Stock Stiker| ID Produk: '. $id .' berhasil dihapus!';
        } else {
            $alert = 'Stock Stiker| ID Produk: '. $id .' gagal dihapus!';
        }
        echo json_encode($alert);
    }

    public function jenis_kemasan()
    {
        return view('home.referensi.kemasan.jenis_kemasan');
    }

    public function kemasan()
    {
        $data['produk'] = DB::table('produk')->select('id_produk','produk')->get();
        $data['kemasan'] = DB::table('kemasan')->select('id_kemasan','nama_kemasan')->get();
        return view('home.referensi.kemasan.kemasan',$data);
    }

    public function getkemasan_ks(Request $request)
    {
        $query = DB::table('kemasan')->where([['nama_kemasan','like','%'.$request->nama_kemasan.'%']])->get();
        return $query->toArray();
    }

    public function addkemasan(Request $request)
    {
        $data = [
            'kode_kemasan' => $request->kode_kemasan,
            'nama_kemasan' => $request->nama_kemasan
        ];
        $query = DB::table('kemasan')
        ->insert($data);
    }

    public function updatekemasan(Request $request)
    {
        $data = [
            'kode_kemasan' => $request->kode_kemasan,
            'nama_kemasan' => $request->nama_kemasan
        ];
        $query = DB::table('kemasan')
        ->where('id_kemasan',$request->id_kemasan)
        ->update($data);
    }

    public function deletekemasan(Request $request)
    {
        $query = DB::table('kemasan')
        ->where('id_kemasan',$request->id_kemasan)
        ->delete();
    }

    public function getstockkemasan()
    {
        $query = DB::table('stock_kemasan')
        ->select('stock_kemasan.*','produk.id_produk','produk.produk','kemasan.id_kemasan','kemasan.nama_kemasan','sub_produk.id_sub_produk','sub_produk.sub_kode','sub_produk.sub_produk')
        ->join('kemasan','stock_kemasan.kemasan_id_kemasan','=','kemasan.id_kemasan')
        ->join('produk','stock_kemasan.produk_id_produk','=','produk.id_produk')
        ->join('sub_produk','stock_kemasan.sub_produk_id','=','sub_produk.id_sub_produk')
        ->get();
        return Datatables::of($query)
        ->addIndexColumn()
        ->addColumn('kodeproduk', function($o)
        {
            $commit = $o->sub_kode.' | '.$o->sub_produk;
            return $commit; 
        })
        ->make(true);
    }

    public function ceksubprodukkem(Request $request)
    {
        $cek = DB::table('stock_kemasan')->where([['sub_produk_id','=',$request->subproduk],['produk_id_produk','=',$request->produk]])->count();
        return $cek;   
    }

    public function addstockkemasan(Request $request)
    {
        $cek = DB::table('stock_kemasan')
        ->where([['sub_produk_id','=',$request->subproduk],['kemasan_id_kemasan','=',$request->id_kemasan],['produk_id_produk','=',$request->produk]]);

        if ($cek->count() > 0) {
            $da = $cek->first();
            $jum = $da->stock_kemasan + $request->stock_kemasan1;

            $data = [
                'stock_kemasan' => $jum
            ];
            $query = DB::table('stock_kemasan')
            ->where('id_stock_kemasan',$da->id_stock_kemasan)
            ->update($data);
            if ($query) {
                $alert = 'Stock Kemasan ID Produk: '.$request->produk.' berhasil diupdate!';
            } else {
                $alert = 'Stock Kemasan ID Produk: '.$request->produk.' gagal diupdate!';
            }
            echo json_encode($alert);
        } else {
            $data = [
                'stock_kemasan' => $request->stock_kemasan1,
                'kemasan_id_kemasan' => $request->id_kemasan,
                'sub_produk_id' => $request->subproduk,
                'produk_id_produk' => $request->produk
            ];
            $query = DB::table('stock_kemasan')
            ->insert($data);
            if ($query) {
                $alert = 'Stock Kemasan berhasil ditambahkan!';
            } else {
                $alert = 'Stock Kemasan gagal ditambahkan!';
            }
            echo json_encode($alert);
        }
    }

    public function uppstockkemasan(Request $request,$id)
    {
        $data = [
            'stock_kemasan' => $request->stock_kemasan1,
        ];
        $query = DB::table('stock_kemasan')->where('id_stock_kemasan',$id)->update($data);
        if ($query) {
            $alert = 'Stock Kemasan ID Produk: '.$request->produk.' berhasil diupdate!';
        } else {
            $alert = 'Stock Kemasan ID Produk: '.$request->produk.' gagal diupdate!';
        }
        echo json_encode($alert);
    }

    public function delstockkemasan($id)
    {
        $query = DB::table('stock_kemasan')->where('id_stock_kemasan',$id)->delete();
        if ($query) {
            $alert = 'Stock Kemasan| ID Stock Kemasan: '. $id .' berhasil dihapus!';
        } else {
            $alert = 'Stock Kemasan| ID Stock Kemasan: '. $id .' gagal dihapus!';
        }
        echo json_encode($alert);
    }

    public function setting()
    {
        return view('home.referensi.setting.setting');
    }

    public function getsetting()
    {
        $query = DB::table('setting')->where('idsetting',1)->first();
        echo json_encode($query);
    }

    public function postsetting(Request $request)
    {
        $data = [
            'direktur_utama' => $request->direk_uta,
            'direktur_operasional' => $request->direk_ops,
            'kabag_produksi' => $request->kabag_pro,
            'quality_control' => $request->quality,
            'ready_for_sale' => $request->rfs
        ];

        $query = DB::table('setting')
        ->where('idsetting',$request->idsetting)
        ->update($data);
        if ($query) {
            $alert = 'Setting berhasil diupdate!';
        } else {
            $alert = 'Setting gagal diupdate!';
        }
        echo json_encode($alert);
    }

    public function aksesoris()
    {
        $data['produk'] = DB::table('produk')->get();
        return view('home.referensi.aksesoris.data_aksesoris',$data);
    }

    public function getaksesoris(Request $request)
    {
        $query = DB::table('aksesoris')
        ->where([['kode_aksesoris','like','%'.$request->kode_aksesoris.'%'],['aksesoris','like','%'.$request->aksesoris.'%'],])
        ->get();
        return $query->toArray();
    }

    public function addaksesoris(Request $request)
    {
        $data = [
            'kode_aksesoris' => $request->kode_aksesoris,
            'aksesoris' => $request->aksesoris
        ];
        $query = DB::table('aksesoris')
        ->insert($data);
    }

    public function putaksesoris(Request $request)
    {
        $data = [
            'kode_aksesoris' => $request->kode_aksesoris,
            'aksesoris' => $request->aksesoris
        ];
        $query = DB::table('aksesoris')
        ->where('id',$request->id)
        ->update($data);
    }

    public function delaksesoris(Request $request)
    {
        $query = DB::table('aksesoris')
        ->where('id',$request->id)
        ->delete();
    }

    public function getakshassub()
    {
        $query = DB::table('sub_produk')
        ->select(
            'sub_produk.*',
            'produk.kode_produk',
            'produk.produk',
            'sub_produk_has_aksesoris.id AS idaksesoris',
            'sub_produk_has_aksesoris.kode_spesial',
            'aksesoris.aksesoris',
            'sub_produk_has_aksesoris.aksesoris_id')
        ->join('produk','sub_produk.produk_id_produk','produk.id_produk')
        ->join('sub_produk_has_aksesoris','sub_produk.id_sub_produk','sub_produk_has_aksesoris.sub_produk_id')
        ->join('aksesoris','sub_produk_has_aksesoris.aksesoris_id','=','aksesoris.id')
        ->orderBy('produk.id_produk','DESC')
        ->get();
        return Datatables::of($query)
        ->addIndexColumn()
        ->smart(true)
        ->make(true);
    }

    public function addhassub(Request $request)
    {
        $cek = DB::table('sub_produk_has_aksesoris')
        ->where([
            ['sub_produk_id','=', $request->subproduk],
            ['aksesoris_id','=', $request->aksesoris]
        ])->count();
        if ($cek == 0) {

            $data = [
                'kode_spesial' => $request->kode_relasi,
                'sub_produk_id' => $request->subproduk,
                'aksesoris_id' => $request->aksesoris
            ];
            $query = DB::table('sub_produk_has_aksesoris')->insert($data);
            $R = ( $query ? 'berhasil' : 'gagal');
        } else {
            $R = 'data sudah ada!!';
        }
        return json_encode($R);
    }

    public function upphassub(Request $request,$id)
    {
        $data = [
            'kode_spesial' => $request->kode_relasi,
            'sub_produk_id' => $request->subproduk,
            'aksesoris_id' => $request->aksesoris
        ];
        $query = DB::table('sub_produk_has_aksesoris')->where('id',$id)->update($data);
        $R = ( $query ? 'berhasil' : 'gagal');
        return json_encode($R);   
    }

    public function deletehassub($id)
    {
        $query = DB::table('sub_produk_has_aksesoris')->where('id',$id)->delete();
        $R = ( $query ? 'berhasil' : 'gagal');
        return json_encode($R);   
    }

    public function merek_cukai()
    {
        return view('home.referensi.merek_cukai.merk_cukai');
    }

    public function getmerek_cukai(Request $request)
    {
        $query = DB::table('merek_cukai')
        ->where([['kode_cukai','like','%'.$request->kode_cukai.'%'],['nama_cukai','like','%'.$request->nama_cukai.'%'],])
        ->get();
        return $query->toArray();
    }

    public function addmerek_cukai(Request $request)
    {
        $data = [
            'kode_cukai' => $request->kode_cukai,
            'nama_cukai' => $request->nama_cukai,
            'keterangan' => $request->keterangan
        ];
        $query = DB::table('merek_cukai')
        ->insert($data);
    }

    public function putmerek_cukai(Request $request)
    {
        $data = [
            'kode_cukai' => $request->kode_cukai,
            'nama_cukai' => $request->nama_cukai,
            'keterangan' => $request->keterangan
        ];
        $query = DB::table('merek_cukai')
        ->where('id',$request->id)
        ->update($data);
    }

    public function delmerek_cukai(Request $request)
    {
        $query = DB::table('merek_cukai')
        ->where('id',$request->id)
        ->delete();
    }
}