<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use DataTables;
use Auth;
use App\Filling;
use App\Stock_filling;
use App\Stock_probaku;

use App\Http\Controllers\Back_filling;

class Fillingcont extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->produksi = $res = new Back_filling;
    }

    public function searchweiprofill(Request $request)
    {
    	$pert = $this->produksi->sprofill($request);
    	echo json_encode($pert);
    }

    public function getdatafill()
    {
        $query = DB::table('filling')
        ->select('filling.*','jenis.id_jenis','produk.id_produk','jenis.jenis','produk.produk')
        ->join('stock_filling','filling.stock_filling_id','=','stock_filling.id')
        ->join('jenis','stock_filling.jenis_has_produk_id_jenis','=','jenis.id_jenis')
        ->join('produk','stock_filling.jenis_has_produk_id_produk','=','produk.id_produk')
        ->get();
        return Datatables::of($query)
        ->addIndexColumn()
        ->addColumn('stockawal', function ($query) {
            $awl = $query->sisaprof + $query->terpakaiprof;
            return $awl;
        })
        ->addColumn('stockawalweis', function ($query) {
            $awl = $query->sisaweisf - $query->weisfmasuk + $query->terpakaiweisf;
            return $awl;
        })
        ->make(true);
    }

    public function datafilling()
    {
    	$data['jenis'] = DB::table('jenis')->get();
        return view('home.produksi.filling.datafilling',$data);
    }

    public function addfilling(Request $request)
    {
        DB::beginTransaction();
        $hpro = $request->stockpro - $request->terpakaipro;
        $wpro = $request->stockweisf + $request->masukweisf - $request->terpakaiweisf;
        $qq = $this->produksi->stockfill($request);

        $eql = DB::table('stock_probaku')->where([['jenis_id_jenis','=',$request->jenis],['kategori_id_kategori','=',1]])->update(['jml_produksi' => $hpro]);

        $data = [
            'weisfmasuk' => $request->masukweisf,
            'terpakaiprof' => $request->terpakaipro,
            'terpakaiweisf' => $request->terpakaiweisf,
            'sisaprof' => $hpro,
            'sisaweisf' => $wpro,
            'hasil_fill' => $request->batangjml,
            'tanggal_fill' => $request->tanggal_filling,
            'stock_probaku_id' => $request->idprobaku,
            'stock_filling_id' => $qq,
            'author_session' => Auth::user()->id,
            'created_at' => date('Y-m-d H:i:s'),
            'log' => "INSERT"
        ];
        $query1 = Filling::insert($data);
        if ($query1) {
            $alert = 'Filling berhasil ditambahkan!';
        } else {
            $alert = 'Filling gagal ditambahkan!';
        }
        echo json_encode($alert);
        DB::commit();
    }

    public function uppfill(Request $request,$id,$masukweis,$terpoakaipro,$terpakaiweis,$batang)
    {
        DB::beginTransaction();
        $hpro = $request->stockpro - $request->terpakaipro;
        $wpro = $request->stockweisf + $request->masukweisf - $request->terpakaiweisf;

        $this->produksi->ifuppstockfill($request,$masukweis,$terpoakaipro,$terpakaiweis,$batang);

        $data = [
            'weisfmasuk' => $request->masukweisf,
            'terpakaiprof' => $request->terpakaipro,
            'terpakaiweisf' => $request->terpakaiweisf,
            'sisaprof' => $hpro,
            'sisaweisf' => $wpro,
            'hasil_fill' => $request->batangjml,
            'author_session' => Auth::user()->id,
            'log' => "UPDATE"
        ];
        $query1 = Filling::where('id',$id)->update($data);
        if ($query1) {
            $this->produksi->loop_upfill($request,$id);
            $alert = 'Filling ID: '.$id.' berhasil diupdate!';
        } else {
            $alert = 'Filling ID: '.$id.' gagal diupdate!';
        }
        echo json_encode($alert);
        DB::commit();
    }

    public function delfill(Request $request,$id,$masukweis,$terpoakaipro,$terpakaiweis,$batang)
    {
        DB::beginTransaction();
        $query = Stock_probaku::where('id',$request->idprobaku)->first();
        $qusrt = Stock_filling::where('id',$request->idfilling)->first();

        $pro = $query->jml_produksi + $terpoakaipro;
        $weis = $qusrt->stock_weisf - $masukweis + $terpakaiweis;
        $batang = $qusrt->jml_fill - $batang;

        $update1 = Stock_probaku::find($request->idprobaku);
        $update1->jml_produksi = $pro;
        $update1->save();

        $update2 = Stock_filling::find($request->idfilling);
        $update2->jml_fill = $batang;
        $update2->stock_weisf = $weis;
        $update2->save();

        $query1 = Filling::where('id',$id)->delete();
        if ($query1) {
            $this->produksi->loop_upfill($request,$id,$terpoakaipro);
            $alert = 'Filling ID : '.$id.' berhasil dihapus!';
        } else {
            $alert = 'Filling ID : '.$id.' gagal dihapus!';
        }
        echo json_encode($alert);
        DB::commit();      
    }
}
