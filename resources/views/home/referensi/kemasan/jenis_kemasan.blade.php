@extends('../../layout')

@section('title')
<title>BIN - ERP : Jenis Packaging</title>
<link type="text/css" rel="stylesheet" href="{{ asset('node_modules/jsgrid/jsgrid.min.css') }}" />
<link type="text/css" rel="stylesheet" href="{{ asset('node_modules/jsgrid/jsgrid-theme.min.css') }}" />
<style type="text/css">
	.hide
	{
		display:none;
	}
</style>
@endsection

@section('content')
<div class="row page-titles">
	<div class="col-md-5 align-self-center">
		<h3 class="text-themecolor">Jenis Packaging</h3>
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a href="javascript:void(0)">Referensi</a></li>
			<li class="breadcrumb-item active">Jenis Packaging</li>
		</ol>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-body">
				<div class="row">
					<div class="col-md-8">
						<h4 class="card-title">Jenis Packaging</h4>
					</div>
					<div class="col-md-2 offset-md-2">
						<button type="button" onclick="Refresh_jsgrid()" class="btn btn-xs btn-icons btn-secondary">Refresh</button>
					</div>
				</div>
				<div id="grid_kemasan"></div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('javascript')
<script type="text/javascript" src="{{ asset('node_modules/jsgrid/jsgrid.min.js') }}"></script>
@include('home.referensi.kemasan.script_jeniskemasan')
@endsection