@extends('../../layout')

@section('title')
<title>BIN - ERP : Dashboard</title>
@endsection

@section('content')
<div class="row page-titles">
	<div class="col-md-12 align-self-center">
		<h3 class="text-themecolor">BEGIN</h3>
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a href="javascript:void(0)">Dashboard</a></li>
			<li class="breadcrumb-item active">BEGIN </li>
		</ol>
	</div>
</div>
<div class="row">
	<div class="col-lg-12">
		<div class="card">
			<div class="card-body">
				<div class="d-flex">
					<div>
						<h5 class="card-title">Limit Bahan Baku Tembakau</h5>
					</div>
					<div class="ml-auto"></div>
				</div>
				<div class="table-responsive m-t-20 no-wrap">
					<table id="table_limitbahan" class="table vm no-th-brd pro-of-month">
						<thead class="text-center">
							<tr>
								<th>#</th>
								<th align="center">Varietas & Kategori</th>
								<th>MIN Limit (*Gram)</th>
								<th>MAX Limit (*Gram)</th>
							</tr>
						</thead>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('javascript')
<script src="{{ asset('node_modules/datatables/datatables.min.js') }}"></script>
@include('home.dashboard.script_dashboard')
@endsection