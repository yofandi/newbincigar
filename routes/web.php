<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
	return view('welcome');
    // return view('layout');
    // return view('login');
    // \Artisan::call('route:clear');
    // \Artisan::call('config:clear');
    // \Artisan::call('cache:clear');
    // \Artisan::call('config:cache');
});

Auth::routes();

Route::get('/level', 'HomeController@loadlevel')->name('level.show');

/** user profile **/
Route::get('/profile/get', 'HomeController@profile')->name('profile.read');
Route::post('/profile/update/{id}', 'HomeController@update_profile')->name('profile.update');
Route::post('/profile/doupload/{id}', 'HomeController@doupload')->name('profile.upload');
Route::post('/profile/password/{id}',  'HomeController@change_pass')->name('profile.changepass');
/** ---- **/

Route::group(['middleware' => ['web', 'auth', 'roles']], function() {
	/** json **/
	Route::get('users/json','HomeController@getuser');
	Route::get('/produk/json', 'HomeController@getproduk');
	Route::get('/subproduk/json', 'HomeController@getsubproduk');
	Route::get('/jenis/json', 'HomeController@getjenis');
	Route::get('/kategori/json', 'HomeController@getkategori');
	Route::get('/cincin/json', 'HomeController@getcincin');
	Route::get('/cukai/json', 'HomeController@getcukai');
	Route::get('/stiker/json', 'HomeController@getstiker');
	Route::get('/kemasan/json1', 'HomeController@getkemasan_ks');
	Route::get('/stockkemasan/json', 'HomeController@getstockkemasan');
	Route::get('/setting/json', 'HomeController@getsetting')->name('setting.json');
	Route::get('/data_tembakau/json', 'Bahan_baku@getdatacigar');
	Route::get('/data_cincin/json', 'Bahan_baku@getdatacincin');
	Route::get('/data_stiker/json', 'Bahan_baku@getdatastiker');
	Route::get('/data_kemasan/json', 'Bahan_baku@getdatakemasan');
	Route::get('/data_cukai/json', 'Bahan_baku@getdatacukai');
	Route::get('/datafilling/json', 'Fillingcont@getdatafill');
	Route::get('/databinding/json', 'Bindingcont@getdatabind');
	Route::get('/datapressing/json', 'Pressingcont@getdatapres');
	Route::get('/datawrapping/json', 'Wrappingcont@getdatawrap');
	Route::get('/datadrying1/json', 'Drying1cont@getdatadry1');
	Route::get('/datafreezer/json', 'Frezzercont@getdatafre');
	Route::get('/datadrying2/json', 'Drying2cont@getdatadry2');
	Route::get('/datafumigasi/json', 'Fumigasicont@getdatafumi');
	Route::get('/datacool/json', 'Coolcont@getdatacool');
	Route::get('/datadrying3/json', 'Drying3cont@getdatadry3');
	Route::get('/dataqc/json', 'Qualitycont@getdataqc');
	Route::get('/datapacking/json', 'Packingcont@getdatapacking');
	/****/
	
	Route::get('/home', 'Dashboard@dashboard')->name('home');
	Route::get('/noc_isintd/{id}', 'Notification@show_noc')->name('noc_notif.detail');
	Route::post('/noc_isintd/uraian/{id}/{idinrfs}', 'Notification@save_uraianiteminrfs')->name('noc_notif.uraian');
	Route::get('/listtable', 'Notification@listtablenoc')->name('listtable');
	Route::get('/listtable/rfsbagan', 'Notification@listtablerfsbagan')->name('noc_rfsbagan.list');
	Route::get('/listtable/rfsbagan/detail/{id}', 'Notification@show_rfsbaganview')->name('noc_rfsbagan.detail');
	Route::post('/noc_rfsbagan/update/{id}', 'Notification@update_rfsbagan')->name('noc_rfsbagan.detail');

	Route::group(['roles' => '1'], function() {
		/** user management **/
		Route::get('/users', 'HomeController@user')->name('user');
		Route::post('/users/add', 'HomeController@add_user')->name('user.adduser');
		Route::post('/users/edit/{id}', 'HomeController@update_user')->name('user.uppuser');
		Route::post('/users/uppass/{id}', 'HomeController@update_pass')->name('user.uppass');
		Route::delete('/users/delete/{id}', 'HomeController@delete_user')->name('user.delete');
		/** ---- **/
		/** produk & sub produk **/
		Route::get('/produk', 'HomeController@produk')->name('produk');
		Route::post('/produk/add', 'HomeController@insertproduk')->name('produk.add');
		Route::put('/produk/put', 'HomeController@putproduk')->name('produk.put');
		Route::delete('/produk/delete', 'HomeController@delproduk')->name('produk.delete');
		/** --++-- **/
		Route::get('/kemasan/json', 'HomeController@getkemasan');
		Route::post('/subproduk/post', 'HomeController@addsubproduk')->name('subproduk.add');
		Route::post('/subproduk/update/{id}', 'HomeController@updatesubproduk')->name('subproduk.upp');
		Route::delete('/subproduk/delete/{id}', 'HomeController@deletesubproduk')->name('subproduk.del');
		/** ---- **/
		/** aksesoris **/
		Route::get('/aksesoris', 'HomeController@aksesoris')->name('aksesoris');
		/** ---- **/
		/** merek cukai **/
		Route::get('/merek_cukai', 'HomeController@merek_cukai')->name('merekcukai');
		/** ---- **/
		/** jenis tembakau **/
		Route::get('/jenis', 'HomeController@jenis_tembakau')->name('jenis');
		Route::post('/jenis/add', 'HomeController@addjenis')->name('jenis.add');
		Route::put('/jenis/put', 'HomeController@putjenis')->name('jenis.put');
		Route::delete('/jenis/del', 'HomeController@deljenis')->name('jenis.del');
		/** ---- **/
		/** jenis tembakau **/
		Route::get('/kategori', 'HomeController@kategori')->name('kategori');
		Route::post('/kategori/add', 'HomeController@addkategori')->name('kategori.add');
		Route::put('/kategori/put', 'HomeController@putkategori')->name('kategori.put');
		Route::delete('/kategori/del', 'HomeController@deletekategori')->name('kategori.del');
		/** ---- **/
		/** cincin **/
		Route::get('/cincin', 'HomeController@cincin')->name('cincin');
		Route::post('/cincin/post', 'HomeController@addcincin')->name('cincin.add');
		Route::post('/cincin/update/{id}', 'HomeController@updatecincin')->name('cincin.upp');
		Route::delete('/cincin/delete/{id}', 'HomeController@deletecincin')->name('cincin.del');
		/** ---- **/
		/** cukai **/
		Route::get('/cukai', 'HomeController@cukai')->name('cukai');
		Route::post('/cukai/subproduk/json', 'HomeController@getoptsubpro');
		Route::post('/cukai/post', 'HomeController@addcukai')->name('cukai.add');
		Route::post('/cukai/update/{id}', 'HomeController@updatecukai')->name('cukai.upp');
		Route::delete('/cukai/delete/{id}', 'HomeController@deletecukai')->name('cukai.del');
		/** ---- **/
		/** stiker **/
		Route::get('/stiker', 'HomeController@stiker')->name('stiker');
		Route::post('/stiker/post', 'HomeController@addstiker')->name('stiker.add');
		Route::post('/stiker/upp/{id}', 'HomeController@updatestiker')->name('stiker.upp');
		Route::delete('/stiker/delete/{id}', 'HomeController@deletestiker')->name('stiker.del');
		/** ---- **/
		/** kemasan **/
		Route::get('/jeniskemasan', 'HomeController@jenis_kemasan')->name('jenis.kemasan');
		Route::get('/kemasan', 'HomeController@kemasan')->name('kemasan');
		Route::post('/kemasan/post', 'HomeController@addkemasan')->name('kemasan.add');
		Route::put('/kemasan/upp', 'HomeController@updatekemasan')->name('kemasan.upp');
		Route::delete('/kemasan/delete', 'HomeController@deletekemasan')->name('kemasan.del');
		Route::post('/stockkemasan/post', 'HomeController@addstockkemasan')->name('stockkemasan.add');
		Route::post('/stockkemasan/upp/{id}', 'HomeController@uppstockkemasan')->name('stockkemasan.upp');
		Route::delete('/stockkemasan/delete/{id}', 'HomeController@delstockkemasan')->name('stockkemasan.del');
		/** ---- **/
		/** kemasan **/
		Route::get('/setting', 'HomeController@setting')->name('setting');
		Route::post('/setting/post', 'HomeController@postsetting')->name('setting.post');
		/** ---- **/
		/** satuan harga **/
		Route::get('/hargatembakau', 'Satuan_Harga@satuan_harga')->name('hargatembakau');
	});

	Route::group(['roles' => ['1','2','3']], function() {
		/** data tembakau **/
		Route::get('/data_tembakau', 'Bahan_baku@data_tembakau')->name('data.tembakau');
		Route::post('/data_tembakau/post', 'Bahan_baku@addtembakau')->name('data.post');
		Route::post('/data_tembakau/upp/{id}/{terima}/{produksi}', 'Bahan_baku@upptembakau')->name('data.upp');
		Route::delete('/data_tembakau/del/{id}/{terima}/{produksi}/{value}', 'Bahan_baku@deltembakau')->name('data.del');
		/** ---- **/
		/** data cincin **/
		Route::get('/data_cincin', 'Bahan_baku@data_cincin')->name('data.cincin');
		Route::post('/data_cincin/getstock', 'Bahan_baku@showstcin')->name('datacincin.getstock');
		Route::post('/data_cincin/post', 'Bahan_baku@addcincin')->name('datacincin.post');
		Route::post('/data_cincin/upp/{id}/{masuk}/{terpakai}/{afkir}', 'Bahan_baku@uppcincin')->name('datacincin.upp');
		Route::delete('/data_cincin/del/{id}/{masuk}/{terpakai}/{afkir}/{stock}', 'Bahan_baku@delcincin')->name('datacincin.del');
		/** ---- **/
		/** data stiker **/
		Route::get('/data_stiker', 'Bahan_baku@data_stiker')->name('data.stiker');
		Route::post('/data_stiker/getstock', 'Bahan_baku@showststi')->name('datastiker.getstock');
		Route::post('/data_stiker/post', 'Bahan_baku@addstiker')->name('datastiker.post');
		Route::post('/data_stiker/upp/{id}/{masukl}/{masukd}/{pakail}/{pakaid}', 'Bahan_baku@uppstiker')->name('datastiker.upp');
		Route::delete('/data_stiker/del/{id}/{masukl}/{masukd}/{pakail}/{pakaid}/{awalluar}/{awalbaru}', 'Bahan_baku@delstiker')->name('datastiker.del');
		/** ---- **/  
		/** data kemasan **/
		Route::get('/data_kemasan', 'Bahan_baku@data_kemasan')->name('data.kemasan');
		Route::post('/data_kemasan/getstock', 'Bahan_baku@getstokem')->name('datakemasan.getstock');
		Route::post('/data_kemasan/post', 'Bahan_baku@addkemasan')->name('datakemasan.post');
		Route::post('/data_kemasan/upp/{id}/{masuk}/{terpakai}/{afkir}', 'Bahan_baku@uppkemasan')->name('datakemasan.upp');
		Route::delete('/data_kemasan/del/{id}/{masuk}/{terpakai}/{afkir}/{stock}', 'Bahan_baku@delkemasan')->name('datakemasan.del');
		/** ---- **/
		/** data cukai **/
		Route::get('/data_cukai', 'Bahan_baku@data_cukai')->name('data.cukai');
		Route::post('/data_cukai/getsubproduk', 'Bahan_baku@getsubonbase');
		Route::post('/data_cukai/getstockrow', 'Bahan_baku@getstoksukairow');
		Route::post('/data_cukai/post', 'Bahan_baku@addcukai')->name('datacukai.post');
		Route::post('/data_cukai/upp/{id}/{masuk}/{lama}/{baru}', 'Bahan_baku@uppcukai')->name('datacukai.upp');
		Route::delete('/data_cukai/del/{id}/{masuk}/{lama}/{baru}/{stock}', 'Bahan_baku@delcukai')->name('datacukai.del');
		/** ---- **/  
		// data reproduksi
		Route::get('/reproduksi', 'Reproduksi@viewreproduksi')->name('data.reproduksi');
		/** ---- **/  
		// data non tembakau
		Route::get('/data_nontembakau', 'Bahan_nontembakau@viewnontembakau')->name('data.non_tembakau');
		/** ---- **/  
	});

	Route::group(['roles' => ['1','2','3','4','5']], function() {
		Route::get('/prosesproduksi', 'Produksi@prosesview')->name('produksi');
		/** data filling **/
		Route::get('/datafilling', 'Fillingcont@datafilling')->name('filling');
		Route::post('/datafilling/getproduk', 'Produksi@probase');
		Route::post('/datafilling/getsearchfill', 'Fillingcont@searchweiprofill');
		Route::post('/datafilling/post', 'Fillingcont@addfilling')->name('filling.post');
		Route::post('/datafilling/upp/{id}/{masukweis}/{terpoakaipro}/{terpakaiweis}/{batang}', 'Fillingcont@uppfill')->name('filling.upp');
		Route::delete('/datafilling/del/{id}/{masukweis}/{terpoakaipro}/{terpakaiweis}/{batang}','Fillingcont@delfill');
		/** ---- **/  
		/** data binding **/
		Route::get('/databinding', 'Bindingcont@databinding')->name('binding');
		Route::post('/databinding/getsearchfill', 'Bindingcont@searchweiprobind');
		Route::post('/databinding/post', 'Bindingcont@addbinding')->name('binding.post');
		Route::post('/databinding/upp/{id}/{masukweis}/{terpoakaipro}/{terpakaiweis}/{terpakaifill}/{batang}', 'Bindingcont@uppbind')->name('binding.upp');
		Route::delete('/databinding/del/{id}/{masukweis}/{terpoakaipro}/{terpakaiweis}/{terpakaifill}/{batang}', 'Bindingcont@delbind')->name('binding.del');
		/** ---- **/  
		/** data pressing **/
		Route::get('/datapressing', 'Pressingcont@datapressing')->name('pressing');
		Route::post('/datapressing/getsearchfill', 'Pressingcont@searchweipropres');
		Route::post('/datapressing/post', 'Pressingcont@addpressing')->name('pressing.post');
		Route::post('/datapressing/upp/{id}/{batang}', 'Pressingcont@upppressing')->name('pressing.upp');
		Route::delete('/datapressing/del/{id}/{batang}', 'Pressingcont@delpressing')->name('pressing.del');
		/** ---- **/  
		/** data wrapping **/
		Route::post('/datawrapping/getsearchfill', 'Wrappingcont@searchweiprowrap');
		Route::get('/datawrapping', 'Wrappingcont@datawrapping')->name('wrapping');
		Route::post('/datawrapping/post', 'Wrappingcont@addwrapping')->name('wrapping.post');
		Route::post('/datawrapping/upp/{id}/{masukweis}/{terpoakaipro}/{terpakaiweis}/{rejectbatang}/{rejectfill}/{rejectbind}/{batang}', 'Wrappingcont@uppwrapping')->name('wrapping.upp');
		Route::delete('/datawrapping/del/{id}/{masukweis}/{terpoakaipro}/{terpakaiweis}/{rejectbatang}/{rejectfill}/{rejectbind}/{batang}', 'Wrappingcont@delwrapping')->name('wrapping.del');
		/** ---- **/  
		/** data drying 1 **/
		Route::post('/datadrying1/getsearchfill', 'Drying1cont@searchweiprodry1');
		Route::get('/datadrying1', 'Drying1cont@datadrying1')->name('drying1');
		Route::post('/datadrying1/post', 'Drying1cont@adddrying1')->name('drying1.post');
		Route::post('/datadrying1/upp/{id}/{batang}', 'Drying1cont@uppdrying1')->name('drying1.upp');
		Route::delete('/datadrying1/del/{id}/{batang}', 'Drying1cont@deldrying1')->name('drying1.del');
		/** ---- **/  
		/** data freezer **/
		Route::post('/datafreezer/getsearchfill', 'Frezzercont@searchweiprofre');
		Route::get('/datafreezer', 'Frezzercont@datafrezer')->name('freezer');
		Route::post('/datafreezer/post', 'Frezzercont@addfrezer')->name('freezer.post');
		Route::post('/datafreezer/upp/{id}/{batang}', 'Frezzercont@uppfrezer')->name('freezer.upp');
		Route::delete('/datafreezer/del/{id}/{batang}', 'Frezzercont@delfrezer')->name('freezer.del');
		/** ---- **/  
		/** data drying 2 **/
		Route::post('/datadrying2/getsearchfill', 'Drying2cont@searchweiprodry2');
		Route::get('/datadrying2', 'Drying2cont@datadrying2')->name('drying2');
		Route::post('/datadrying2/post', 'Drying2cont@adddrying2')->name('drying2.post');
		Route::post('/datadrying2/upp/{id}/{batang}', 'Produksi@uppdrying2')->name('drying2.upp');
		Route::delete('/datadrying2/del/{id}/{batang}', 'Drying2cont@deldrying2')->name('drying2.del');
		/** ---- **/  
		/** data fumigasi **/
		Route::post('/datafumigasi/getsearchfill', 'Fumigasicont@searchweiprofumi');
		Route::get('/datafumigasi', 'Fumigasicont@datafumigasi')->name('fumigasi');
		Route::post('/datafumigasi/post', 'Fumigasicont@addfumigasi')->name('fumigasi.post');
		Route::post('/datafumigasi/upp/{id}/{batang}', 'Fumigasicont@uppfumigasi')->name('fumigasi.upp');
		Route::delete('/datafumigasi/del/{id}/{batang}', 'Produksi@delfumigasi')->name('fumigasi.del');
		/** ---- **/  
		/** data cool **/
		Route::post('/datacool/getsearchfill', 'Coolcont@searchweiprocool');
		Route::get('/datacool', 'Coolcont@datacool')->name('cool');
		Route::post('/datacool/post', 'Coolcont@addcool')->name('cool.post');
		Route::post('/datacool/upp/{id}/{batang}', 'Coolcont@uppcool')->name('cool.upp');
		Route::delete('/datacool/del/{id}/{batang}', 'Coolcont@delcool')->name('cool.del');
		/** ---- **/  
		/** data drying3 **/
		Route::post('/datadrying3/getsearchfill', 'Drying3cont@searchweiprodry3');
		Route::get('/datadrying3', 'Drying3cont@datadrying3')->name('drying3');
		Route::post('/datadrying3/post', 'Drying3cont@adddrying3')->name('drying3.post');
		Route::post('/datadrying3/upp/{id}/{batang}', 'Drying3cont@uppdrying3')->name('drying3.upp');
		Route::delete('/datadrying3/del/{id}/{batang}', 'Drying3cont@deldrying3')->name('drying3.del');
		/** ---- **/  
		/** data quality control **/
		Route::post('/dataqc/getsearchfill', 'Qualitycont@searchweiproqc');
		Route::get('/dataqc', 'Qualitycont@dataqc')->name('qc');
		Route::post('/dataqc/post', 'Qualitycont@addqc')->name('qc.post');
		Route::post('/dataqc/upp/{id}/{batang}/{mutasi}/{back_fill}/{back_bind}/{back_wrap}/{back_lazio}', 'Qualitycont@uppqc')->name('qc.upp');
		Route::delete('/dataqc/del/{id}/{batang}/{mutasi}/{back_fill}/{back_bind}/{back_wrap}/{back_lazio}/{awalstock}', 'Qualitycont@delqc')->name('qc.del');
		/** ---- **/  
		/** data packing **/
		Route::get('/datapacking', 'Packingcont@datapacking')->name('packing');
		Route::post('/datapacking/getsubproduk', 'Produksi@getsubproduk');
		Route::post('/datapacking/getsearchdata', 'Packingcont@searchpackingdata');
		Route::post('/datapacking/post', 'Packingcont@addpacking')->name('packing.post');

		Route::post('/datapacking/upp/{id}/{idhiscukai}/{tambah_packing}/{masuk_cukai}/{terpakai_cukailama}/{terpakai_cukaibaru}/{idhiskem}/{masuk_kemasan}/{terpakai_kemasan}/{afkir_kemasan}/{idhissti}/{masuk_luar}/{pakai_luar}/{masuk_dalam}/{pakai_dalam}/{idhiscin}/{masuk_cincin}/{terpakai_cincin}/{afkir_cincin}', 'Packingcont@upppacking')->name('packing.upp');

		Route::delete('/datapacking/del/{id}/{idstocukai}/{tambah_packing}/{masuk_cukai}/{terpakai_cukailama}/{terpakai_cukaibaru}/{idstokem}/{masuk_kemasan}/{terpakai_kemasan}/{afkir_kemasan}/{idstosti}/{masuk_luar}/{pakai_luar}/{masuk_dalam}/{pakai_dalam}/{idstocin}/{masuk_cincin}/{terpakai_cincin}/{afkir_cincin}/{awlcukai}/{awlcincin}/{awlstiluar}/{awlstidalam}/{awlkemasan}', 'Packingcont@delpacking')->name('packing.del');
		/** ---- **/  

		// kirim ke RFS di packing
		Route::post('/datapacking/addinrfs', 'Produksi@addsessioncart')->name('addinrfs.post');
		Route::post('/datapacking/uppinrfs/{rowId}/{name}', 'Produksi@updatesessioncart')->name('uppinrfs.post');	
		Route::delete('/datapacking/delinrfs/{rowId}/{name}', 'Produksi@deletesessioncart')->name('delinrfs.del');
		Route::post('/datapacking/saveinrfs', 'Produksi@addinrfs')->name('saveinrfs');
		/** ---- **/  
	});

	Route::group(['roles' => ['1','2','6']], function() {
		/** data rfs **/
		Route::get('/datarfs', 'Rfs@index')->name('rfs');
		Route::post('/datarfs/post', 'Rfs@rfsinsert')->name('rfs.post');
		Route::get('/datarfs/viewupdate/{id}', 'Rfs@viewupdate_rfs')->name('rfs.updateview');
		Route::post('/datarfs/update/{id}', 'Rfs@rfsupdate')->name('rfs.update');
		Route::delete('/datarfs/delete/{id}', 'Rfs@rfsdelete')->name('rfs.delete');
	});

	/** laporan semua **/
	Route::get('/laporan/bahan', 'Report@report_bahan')->name('laporam.bahan');
	Route::get('/laporan/produksi', 'Report1@report_produksi')->name('laporam.produksi');
});