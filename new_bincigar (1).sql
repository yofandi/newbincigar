-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jul 01, 2019 at 11:14 PM
-- Server version: 10.3.16-MariaDB
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `new_bincigar`
--

-- --------------------------------------------------------

--
-- Table structure for table `binding`
--

CREATE TABLE `binding` (
  `id` int(11) NOT NULL,
  `weisbmasuk` decimal(20,2) NOT NULL,
  `terpakaiprob` decimal(20,2) NOT NULL,
  `terpakaiweisb` decimal(20,2) NOT NULL,
  `sisaprob` decimal(20,2) NOT NULL,
  `sisaweisb` decimal(20,2) NOT NULL,
  `terpakai_filling` decimal(20,2) NOT NULL,
  `hasil_filling` decimal(20,2) NOT NULL,
  `hasil_bind` varchar(45) NOT NULL,
  `hasil_akhirb` varchar(45) NOT NULL,
  `tanggal_bind` varchar(45) NOT NULL,
  `stock_binding_id` int(11) NOT NULL,
  `stock_filling_id` int(11) NOT NULL,
  `stock_probaku_id` int(11) NOT NULL,
  `author_session` varchar(150) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT current_timestamp(),
  `log` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `binding`
--

INSERT INTO `binding` (`id`, `weisbmasuk`, `terpakaiprob`, `terpakaiweisb`, `sisaprob`, `sisaweisb`, `terpakai_filling`, `hasil_filling`, `hasil_bind`, `hasil_akhirb`, `tanggal_bind`, `stock_binding_id`, `stock_filling_id`, `stock_probaku_id`, `author_session`, `created_at`, `updated_at`, `log`) VALUES
(1, '0.00', '1000.00', '0.00', '89000.00', '0.00', '0.00', '0.00', '300', '0', '2019-06-18 11:28:03', 9, 9, 2, '1', '2019-06-18 04:28:36', '2019-06-18 04:28:36', 'INSERT'),
(2, '500.00', '500.00', '0.00', '88500.00', '0.00', '0.00', '0.00', '0', '500', '2019-06-18 16:14:17', 4, 4, 2, '1', '2019-06-18 09:15:01', '2019-06-18 09:15:01', 'INSERT'),
(3, '0.00', '1000.00', '0.00', '87500.00', '0.00', '0.00', '0.00', '500', '0', '2019-06-18 16:15:29', 4, 4, 2, '1', '2019-06-18 09:15:53', '2019-06-18 09:15:53', 'INSERT'),
(4, '500.00', '1000.00', '0.00', '86500.00', '0.00', '0.00', '0.00', '500', '0', '2019-06-18 16:15:58', 5, 5, 2, '1', '2019-06-18 09:16:33', '2019-06-18 09:16:33', 'INSERT'),
(5, '500.00', '1000.00', '0.00', '85500.00', '0.00', '0.00', '0.00', '500', '0', '2019-06-18 16:16:38', 6, 6, 2, '1', '2019-06-18 09:16:56', '2019-06-18 09:16:56', 'INSERT'),
(6, '500.00', '1000.00', '0.00', '84500.00', '0.00', '0.00', '0.00', '500', '0', '2019-06-18 16:17:02', 7, 7, 2, '1', '2019-06-18 09:17:25', '2019-06-18 09:17:25', 'INSERT'),
(7, '0.00', '1000.00', '200.00', '83500.00', '0.00', '0.00', '0.00', '1000', '15', '2019-06-27 18:50:48', 1, 1, 2, '1', '2019-06-27 11:51:13', '2019-06-27 11:51:13', 'INSERT'),
(10, '0.00', '50.00', '0.00', '83450.00', '0.00', '50.00', '450.00', '100', '', '2019-07-02 02:28:03', 11, 10, 2, '1', '2019-07-01 19:28:27', '2019-07-01 19:33:44', 'INSERT'),
(11, '0.00', '100.00', '0.00', '83350.00', '0.00', '100.00', '350.00', '200', '', '2019-07-02 02:32:05', 11, 10, 2, '1', '2019-07-01 19:32:31', '2019-07-01 19:33:44', 'INSERT');

-- --------------------------------------------------------

--
-- Table structure for table `cerutu_terjual`
--

CREATE TABLE `cerutu_terjual` (
  `id` int(11) NOT NULL,
  `penjualan_id` int(11) NOT NULL,
  `penjualan_customer_id_customer` int(11) NOT NULL,
  `stock_bagan_id` int(11) NOT NULL,
  `jml_terjual` int(20) NOT NULL,
  `diskon_terjual` int(20) NOT NULL,
  `total_terjual` varchar(45) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `cicil`
--

CREATE TABLE `cicil` (
  `id` int(11) NOT NULL,
  `penjualan_id` int(11) NOT NULL,
  `tanggal_cicil` datetime NOT NULL,
  `nominal_cicil` int(20) NOT NULL,
  `status_pembayar` varchar(20) NOT NULL,
  `status_lunas` varchar(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `cool`
--

CREATE TABLE `cool` (
  `id` int(11) NOT NULL,
  `tambah_cool` int(20) NOT NULL,
  `hasil_akhircool` int(20) NOT NULL,
  `lama_cool` tinytext DEFAULT NULL,
  `keterangan_cool` text DEFAULT NULL,
  `tanggal_cool` datetime NOT NULL,
  `stock_cool_id` int(11) NOT NULL,
  `stock_fumigasi_id` int(11) NOT NULL,
  `author_session` varchar(200) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT current_timestamp(),
  `log` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cool`
--

INSERT INTO `cool` (`id`, `tambah_cool`, `hasil_akhircool`, `lama_cool`, `keterangan_cool`, `tanggal_cool`, `stock_cool_id`, `stock_fumigasi_id`, `author_session`, `created_at`, `updated_at`, `log`) VALUES
(1, 500, 0, '02:32:00', 'sasef', '2019-06-18 16:32:25', 4, 4, '1', '2019-06-18 09:33:12', '2019-06-18 09:33:12', 'INSERT'),
(2, 500, 0, '02:45:00', 'adfdfdg', '2019-06-18 16:33:21', 5, 5, '1', '2019-06-18 09:34:10', '2019-06-18 09:34:10', 'INSERT'),
(3, 500, 0, '02:34:00', 'aslsj', '2019-06-18 16:34:16', 6, 7, '1', '2019-06-18 09:34:42', '2019-06-18 09:34:42', 'INSERT'),
(4, 500, 0, '02:45:00', 'shaknd', '2019-06-18 16:34:47', 7, 6, '1', '2019-06-18 09:35:11', '2019-06-18 09:35:11', 'INSERT'),
(5, 1005, 5, '03:00:00', NULL, '2019-06-27 19:09:58', 1, 1, '1', '2019-06-27 12:10:24', '2019-06-27 12:10:24', 'INSERT');

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE `customer` (
  `id_customer` int(11) NOT NULL,
  `nama_customer` varchar(150) NOT NULL,
  `no_telf` varchar(20) NOT NULL,
  `email` varchar(100) NOT NULL,
  `alamat_customer` tinytext DEFAULT NULL,
  `keterangan_customer` varchar(50) DEFAULT NULL,
  `users_id_users` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `drying1`
--

CREATE TABLE `drying1` (
  `id` int(11) NOT NULL,
  `tambah_dry1` int(11) NOT NULL,
  `hasil_akhirdry1` int(11) NOT NULL,
  `lama_dry1` tinytext NOT NULL,
  `keterangan_dry1` text DEFAULT NULL,
  `tanggal_dry1` datetime NOT NULL,
  `stock_wrapping_id` int(11) NOT NULL,
  `stock_drying1_id` int(11) NOT NULL,
  `author_session` varchar(45) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT current_timestamp(),
  `log` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `drying1`
--

INSERT INTO `drying1` (`id`, `tambah_dry1`, `hasil_akhirdry1`, `lama_dry1`, `keterangan_dry1`, `tanggal_dry1`, `stock_wrapping_id`, `stock_drying1_id`, `author_session`, `created_at`, `updated_at`, `log`) VALUES
(1, 500, 0, '06:22:00', 'sdsds', '2019-06-18 16:21:58', 6, 4, '1', '2019-06-18 09:22:22', '2019-06-18 09:22:22', 'INSERT'),
(2, 500, 500, '04:22:00', 'sasa', '2019-06-18 16:22:27', 7, 5, '1', '2019-06-18 09:22:51', '2019-06-18 09:22:51', 'INSERT'),
(3, 500, 500, '02:23:00', 'asanksnka', '2019-06-18 16:22:56', 8, 6, '1', '2019-06-18 09:23:21', '2019-06-18 09:23:21', 'INSERT'),
(4, 500, 500, '02:50:00', 'sahiksahik', '2019-06-18 16:23:25', 9, 7, '1', '2019-06-18 09:23:53', '2019-06-18 09:23:53', 'INSERT'),
(5, 1000, 5, '03:00:00', NULL, '2019-06-27 19:04:15', 1, 1, '1', '2019-06-27 12:04:55', '2019-06-27 12:04:55', 'INSERT');

-- --------------------------------------------------------

--
-- Table structure for table `drying2`
--

CREATE TABLE `drying2` (
  `id` int(11) NOT NULL,
  `tambah_dry2` int(20) NOT NULL,
  `hasil_akhirdry2` int(20) NOT NULL,
  `lama_dry2` tinytext NOT NULL,
  `keterangan_dry2` text DEFAULT NULL,
  `tanggal_dry2` datetime NOT NULL,
  `stock_frezer_id` int(11) NOT NULL,
  `stock_drying2_id` int(11) NOT NULL,
  `author_session` varchar(200) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT current_timestamp(),
  `log` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `drying2`
--

INSERT INTO `drying2` (`id`, `tambah_dry2`, `hasil_akhirdry2`, `lama_dry2`, `keterangan_dry2`, `tanggal_dry2`, `stock_frezer_id`, `stock_drying2_id`, `author_session`, `created_at`, `updated_at`, `log`) VALUES
(1, 500, 0, '02:27:00', 'asad', '2019-06-18 16:27:09', 4, 4, '1', '2019-06-18 09:27:46', '2019-06-18 09:27:46', 'INSERT'),
(2, 500, 0, '02:28:00', 'asfesdg', '2019-06-18 16:27:50', 5, 5, '1', '2019-06-18 09:28:18', '2019-06-18 09:28:18', 'INSERT'),
(3, 500, 0, '02:40:00', 'ahsjar', '2019-06-18 16:28:25', 6, 6, '1', '2019-06-18 09:28:54', '2019-06-18 09:28:54', 'INSERT'),
(4, 500, 0, '02:35:00', 'sasefcd', '2019-06-18 16:28:59', 7, 7, '1', '2019-06-18 09:29:30', '2019-06-18 09:29:30', 'INSERT'),
(5, 1000, 5, '03:00:00', NULL, '2019-06-27 19:08:10', 1, 1, '1', '2019-06-27 12:08:42', '2019-06-27 12:08:42', 'INSERT');

-- --------------------------------------------------------

--
-- Table structure for table `drying3`
--

CREATE TABLE `drying3` (
  `id` int(11) NOT NULL,
  `tambah_dry3` int(20) NOT NULL,
  `hasil_akhirdry3` int(20) NOT NULL,
  `lama_dry3` tinytext NOT NULL,
  `keterangan_dry3` text DEFAULT NULL,
  `tanggal_dry3` datetime NOT NULL,
  `stock_drying3_id` int(11) NOT NULL,
  `stock_cool_id` int(11) NOT NULL,
  `author_session` varchar(200) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT current_timestamp(),
  `log` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `drying3`
--

INSERT INTO `drying3` (`id`, `tambah_dry3`, `hasil_akhirdry3`, `lama_dry3`, `keterangan_dry3`, `tanggal_dry3`, `stock_drying3_id`, `stock_cool_id`, `author_session`, `created_at`, `updated_at`, `log`) VALUES
(1, 500, 0, '02:45:00', 'sadr', '2019-06-18 16:35:34', 4, 4, '1', '2019-06-18 09:36:06', '2019-06-18 09:36:06', 'INSERT'),
(2, 500, 0, '03:36:00', 'adscf', '2019-06-18 16:36:12', 5, 5, '1', '2019-06-18 09:36:38', '2019-06-18 09:36:38', 'INSERT'),
(3, 500, 0, '02:37:00', 'sdcef', '2019-06-18 16:36:46', 6, 6, '1', '2019-06-18 09:37:13', '2019-06-18 09:37:13', 'INSERT'),
(4, 500, 0, '03:37:00', 'sasfv', '2019-06-18 16:37:18', 7, 7, '1', '2019-06-18 09:37:46', '2019-06-18 09:37:46', 'INSERT'),
(5, 1005, 5, '03:00:00', NULL, '2019-06-27 19:11:17', 1, 1, '1', '2019-06-27 12:11:55', '2019-06-27 12:11:55', 'INSERT');

-- --------------------------------------------------------

--
-- Table structure for table `filling`
--

CREATE TABLE `filling` (
  `id` int(11) NOT NULL,
  `weisfmasuk` decimal(20,2) NOT NULL,
  `terpakaiprof` decimal(20,2) NOT NULL,
  `terpakaiweisf` decimal(20,2) NOT NULL,
  `sisaprof` decimal(20,2) NOT NULL,
  `sisaweisf` decimal(20,2) NOT NULL,
  `hasil_fill` decimal(20,2) NOT NULL,
  `tanggal_fill` datetime NOT NULL,
  `stock_probaku_id` int(11) NOT NULL,
  `stock_filling_id` int(11) NOT NULL,
  `author_session` varchar(150) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `log` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `filling`
--

INSERT INTO `filling` (`id`, `weisfmasuk`, `terpakaiprof`, `terpakaiweisf`, `sisaprof`, `sisaweisf`, `hasil_fill`, `tanggal_fill`, `stock_probaku_id`, `stock_filling_id`, `author_session`, `created_at`, `updated_at`, `log`) VALUES
(1, '0.00', '1000.00', '0.00', '89000.00', '0.00', '300.00', '2019-06-18 11:27:23', 1, 9, '1', '2019-06-18 04:27:57', '2019-06-18 04:27:57', 'INSERT'),
(2, '500.00', '1000.00', '0.00', '88000.00', '0.00', '500.00', '2019-06-18 16:11:03', 1, 4, '1', '2019-06-18 09:11:54', '2019-06-18 09:11:54', 'INSERT'),
(3, '500.00', '1000.00', '0.00', '87000.00', '0.00', '500.00', '2019-06-18 16:12:01', 1, 5, '1', '2019-06-18 09:12:32', '2019-06-18 09:12:32', 'INSERT'),
(4, '500.00', '1000.00', '0.00', '86000.00', '0.00', '500.00', '2019-06-18 16:12:38', 1, 6, '1', '2019-06-18 09:13:06', '2019-06-18 09:13:06', 'INSERT'),
(5, '500.00', '1000.00', '0.00', '85000.00', '0.00', '500.00', '2019-06-18 16:13:23', 1, 7, '1', '2019-06-18 09:13:46', '2019-06-18 09:13:46', 'INSERT'),
(6, '0.00', '1000.00', '200.00', '84025.00', '0.00', '1000.00', '2019-06-27 18:48:38', 1, 1, '1', '2019-06-27 11:50:36', NULL, 'INSERT'),
(7, '0.00', '1000.00', '0.00', '83025.00', '0.00', '500.00', '2019-07-02 01:28:13', 1, 10, '1', '2019-07-01 18:28:44', NULL, 'INSERT');

-- --------------------------------------------------------

--
-- Table structure for table `frezer`
--

CREATE TABLE `frezer` (
  `id` int(11) NOT NULL,
  `tambah_fre` int(20) NOT NULL,
  `hasil_akhirfre` int(20) NOT NULL,
  `lama_fre` tinytext NOT NULL,
  `keterengan_fre` text DEFAULT NULL,
  `tanggal_fre` datetime NOT NULL,
  `stock_frezer_id` int(11) NOT NULL,
  `stock_drying1_id` int(11) NOT NULL,
  `author_session` varchar(200) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT current_timestamp(),
  `log` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `frezer`
--

INSERT INTO `frezer` (`id`, `tambah_fre`, `hasil_akhirfre`, `lama_fre`, `keterengan_fre`, `tanggal_fre`, `stock_frezer_id`, `stock_drying1_id`, `author_session`, `created_at`, `updated_at`, `log`) VALUES
(1, 500, 500, '02:24:00', 'sasade', '2019-06-18 16:24:20', 4, 4, '1', '2019-06-18 09:24:58', '2019-06-18 09:24:58', 'INSERT'),
(2, 500, 0, '04:25:00', 'sasa', '2019-06-18 16:25:03', 5, 5, '1', '2019-06-18 09:25:35', '2019-06-18 09:25:35', 'INSERT'),
(3, 500, 0, '04:26:00', 'sasavd', '2019-06-18 16:25:40', 6, 6, '1', '2019-06-18 09:26:08', '2019-06-18 09:26:08', 'INSERT'),
(4, 500, 0, '03:26:00', 'dsdsw', '2019-06-18 16:26:14', 7, 7, '1', '2019-06-18 09:26:40', '2019-06-18 09:26:40', 'INSERT'),
(5, 1000, 5, '03:00:00', NULL, '2019-06-27 19:07:32', 1, 1, '1', '2019-06-27 12:08:01', '2019-06-27 12:08:01', 'INSERT');

-- --------------------------------------------------------

--
-- Table structure for table `fumigasi`
--

CREATE TABLE `fumigasi` (
  `id` int(11) NOT NULL,
  `tambah_fum` int(20) NOT NULL,
  `hasil_akhirfum` int(20) NOT NULL,
  `lama_fum` tinytext NOT NULL,
  `keterangan_fum` text DEFAULT NULL,
  `tanggal_fum` datetime NOT NULL,
  `stock_fumigasi_id` int(11) NOT NULL,
  `stock_drying2_id` int(11) NOT NULL,
  `author_session` varchar(200) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT current_timestamp(),
  `log` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fumigasi`
--

INSERT INTO `fumigasi` (`id`, `tambah_fum`, `hasil_akhirfum`, `lama_fum`, `keterangan_fum`, `tanggal_fum`, `stock_fumigasi_id`, `stock_drying2_id`, `author_session`, `created_at`, `updated_at`, `log`) VALUES
(1, 500, 0, '02:45:00', 'asaknmods', '2019-06-18 16:29:39', 4, 4, '1', '2019-06-18 09:30:06', '2019-06-18 09:30:07', 'INSERT'),
(2, 500, 0, '03:45:00', 'asjkaks', '2019-06-18 16:30:12', 5, 5, '1', '2019-06-18 09:30:41', '2019-06-18 09:30:41', 'INSERT'),
(3, 500, 0, '02:45:00', 'sagjnd', '2019-06-18 16:30:46', 6, 7, '1', '2019-06-18 09:31:12', '2019-06-18 09:31:12', 'INSERT'),
(4, 500, 0, '02:45:00', 'jsanj', '2019-06-18 16:31:17', 7, 6, '1', '2019-06-18 09:31:47', '2019-06-18 09:31:47', 'INSERT'),
(5, 1005, 0, '03:00:00', NULL, '2019-06-27 19:08:52', 1, 1, '1', '2019-06-27 12:09:15', '2019-06-27 12:09:15', 'INSERT');

-- --------------------------------------------------------

--
-- Table structure for table `harga_cincin`
--

CREATE TABLE `harga_cincin` (
  `id` int(11) NOT NULL,
  `produk_id_produk` int(11) NOT NULL,
  `tanggal_harga` datetime NOT NULL,
  `harga` decimal(45,10) NOT NULL,
  `keterangan_hargacin` tinytext DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `harga_cincin`
--

INSERT INTO `harga_cincin` (`id`, `produk_id_produk`, `tanggal_harga`, `harga`, `keterangan_hargacin`, `created_at`, `updated_at`) VALUES
(2, 12, '2019-06-06 00:44:36', '10000.0000000000', 'nsh', '2019-06-06 00:44:48', '2019-06-06 00:44:48'),
(3, 11, '2019-06-08 13:41:28', '2000.0000000000', 'ket', '2019-06-08 13:41:45', '2019-06-08 13:41:45'),
(4, 54, '2019-06-13 16:52:06', '1000.0000000000', 'jos', '2019-06-13 16:52:21', '2019-06-13 16:52:21');

-- --------------------------------------------------------

--
-- Table structure for table `harga_cukai`
--

CREATE TABLE `harga_cukai` (
  `id` int(11) NOT NULL,
  `sub_produk_id` int(11) NOT NULL,
  `tanggal_harga` datetime NOT NULL,
  `harga` decimal(45,10) NOT NULL,
  `keterangan_hargacukai` tinytext DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `harga_cukai`
--

INSERT INTO `harga_cukai` (`id`, `sub_produk_id`, `tanggal_harga`, `harga`, `keterangan_hargacukai`, `created_at`, `updated_at`) VALUES
(2, 7, '2019-06-07 14:55:36', '5000.0000000000', 'kan', '2019-06-07 14:56:00', NULL),
(3, 23, '2019-06-27 22:01:14', '100.0000000000', NULL, '2019-06-27 22:01:33', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `harga_kemasan`
--

CREATE TABLE `harga_kemasan` (
  `id` int(11) NOT NULL,
  `sub_produk_id` int(11) NOT NULL,
  `tanggal_harga` datetime NOT NULL,
  `harga` decimal(45,10) NOT NULL,
  `keterangan_hargakem` tinytext DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `harga_kemasan`
--

INSERT INTO `harga_kemasan` (`id`, `sub_produk_id`, `tanggal_harga`, `harga`, `keterangan_hargakem`, `created_at`, `updated_at`) VALUES
(2, 7, '2019-06-07 16:17:30', '15000.0000000000', 'jar', '2019-06-07 16:17:52', '2019-06-07 16:17:52'),
(3, 47, '2019-06-13 16:54:57', '1000.0000000000', 'kknsk', '2019-06-13 16:55:13', '2019-06-13 16:55:13'),
(4, 48, '2019-06-13 16:55:16', '1000.0000000000', 'nxks', '2019-06-13 16:55:29', '2019-06-13 16:55:29'),
(5, 49, '2019-06-13 16:55:34', '1000.0000000000', 'ajkdj', '2019-06-13 16:55:47', '2019-06-13 16:55:47'),
(6, 12, '2019-06-27 16:04:47', '100.0000000000', NULL, '2019-06-27 16:05:18', '2019-06-27 16:05:18');

-- --------------------------------------------------------

--
-- Table structure for table `harga_stiker`
--

CREATE TABLE `harga_stiker` (
  `id` int(11) NOT NULL,
  `sub_produk_id` int(11) NOT NULL,
  `tanggal_harga` datetime NOT NULL,
  `harga_dalam` decimal(45,10) NOT NULL,
  `harga_luar` decimal(45,10) NOT NULL,
  `keterangan_hargasti` tinytext DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `harga_stiker`
--

INSERT INTO `harga_stiker` (`id`, `sub_produk_id`, `tanggal_harga`, `harga_dalam`, `harga_luar`, `keterangan_hargasti`, `created_at`, `updated_at`) VALUES
(2, 7, '2019-06-07 15:44:56', '12000.0000000000', '10000.0000000000', 'jos', '2019-06-07 15:45:17', NULL),
(3, 47, '2019-06-13 16:53:21', '500.0000000000', '500.0000000000', 'jken', '2019-06-13 16:53:38', NULL),
(4, 48, '2019-06-13 16:53:42', '500.0000000000', '500.0000000000', 'jks', '2019-06-13 16:53:59', NULL),
(5, 49, '2019-06-13 16:54:09', '500.0000000000', '500.0000000000', 'ndkj', '2019-06-13 16:54:28', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `history_bahanmasuk`
--

CREATE TABLE `history_bahanmasuk` (
  `id` int(11) NOT NULL,
  `tanggal_in` datetime NOT NULL,
  `asal` varchar(100) NOT NULL,
  `stock_masuk` decimal(20,2) NOT NULL,
  `diterima` decimal(20,2) NOT NULL,
  `diproduksi` decimal(20,2) NOT NULL,
  `hari_ini` decimal(20,2) NOT NULL,
  `ket_his` tinytext DEFAULT NULL,
  `jenis_id_jenis` int(11) NOT NULL,
  `kategori_id_kategori` int(11) NOT NULL,
  `stock_probaku_id` int(11) NOT NULL,
  `satuan_harga_id` int(11) NOT NULL,
  `author_session` varchar(150) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT current_timestamp(),
  `log` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `history_bahanmasuk`
--

INSERT INTO `history_bahanmasuk` (`id`, `tanggal_in`, `asal`, `stock_masuk`, `diterima`, `diproduksi`, `hari_ini`, `ket_his`, `jenis_id_jenis`, `kategori_id_kategori`, `stock_probaku_id`, `satuan_harga_id`, `author_session`, `created_at`, `updated_at`, `log`) VALUES
(1, '2019-06-07 16:51:32', 'Beli Eksternal', '100.00', '100.00', '0.00', '100.00', 'jam', 8, 1, 1, 3, '1', '2019-06-07 09:53:33', '2019-06-07 09:53:33', 'INSERT'),
(2, '2019-06-07 19:02:25', 'Beli Eksternal', '0.00', '0.00', '10.00', '90.00', 'pndj', 8, 1, 1, 3, '1', '2019-06-07 12:02:51', '2019-06-07 12:02:52', 'INSERT'),
(3, '2019-06-08 13:33:53', 'Beli Eksternal', '100.00', '100.00', '10.00', '90.00', 'ken', 8, 3, 2, 4, '1', '2019-06-08 06:34:16', '2019-06-08 06:34:16', 'INSERT'),
(4, '2019-06-08 13:34:24', 'Beli Eksternal', '100.00', '100.00', '10.00', '90.00', 'jar', 8, 4, 3, 5, '1', '2019-06-08 06:34:49', '2019-06-08 06:34:49', 'INSERT'),
(5, '2019-06-08 13:36:33', 'Beli Eksternal', '6000.00', '6000.00', '5000.00', '1090.00', 'sip', 8, 1, 1, 3, '1', '2019-06-08 06:37:53', '2019-06-08 06:37:53', 'INSERT'),
(6, '2019-06-08 13:38:07', 'Beli Eksternal', '6000.00', '6000.00', '5000.00', '1090.00', 'jer', 8, 3, 2, 4, '1', '2019-06-08 06:38:43', '2019-06-08 06:38:43', 'INSERT'),
(7, '2019-06-08 13:38:48', 'Beli Eksternal', '6000.00', '6000.00', '5000.00', '1090.00', 'sip', 8, 4, 3, 5, '1', '2019-06-08 06:39:12', '2019-06-08 06:39:12', 'INSERT'),
(8, '2019-06-17 15:03:39', 'Tanam Sendiri', '100000.00', '100000.00', '100000.00', '1090.00', 'saswed', 8, 1, 1, 3, '1', '2019-06-17 08:04:27', '2019-06-17 08:04:27', 'INSERT'),
(9, '2019-06-17 15:04:40', 'Tanam Sendiri', '100000.00', '100000.00', '100000.00', '1090.00', 'sjksjdi', 8, 3, 2, 4, '1', '2019-06-17 08:05:03', '2019-06-17 08:05:03', 'INSERT'),
(10, '2019-06-17 15:05:07', 'Tanam Sendiri', '100000.00', '100000.00', '100000.00', '1090.00', 'dsdrd', 8, 4, 3, 5, '1', '2019-06-17 08:05:26', '2019-06-17 08:05:26', 'INSERT'),
(12, '2019-06-19 14:52:52', 'Tanam Sendiri', '0.00', '10.00', '0.00', '1100.00', 'saadcs', 8, 1, 1, 3, '1', '2019-06-19 09:23:26', '2019-06-19 09:23:26', 'UPDATE'),
(13, '2019-06-19 16:23:59', 'Beli Eksternal', '0.00', '5.00', '20.00', '1085.00', 'safe', 8, 1, 1, 3, '1', '2019-06-19 09:28:26', '2019-06-19 09:28:26', 'INSERT'),
(14, '2019-06-19 16:57:46', 'Beli Eksternal', '0.00', '0.00', '5.00', '1080.00', 'jd', 8, 1, 1, 3, '1', '2019-06-19 10:01:44', '2019-06-19 10:01:44', 'UPDATE');

-- --------------------------------------------------------

--
-- Table structure for table `history_cincin`
--

CREATE TABLE `history_cincin` (
  `id` int(11) NOT NULL,
  `masuk` int(15) NOT NULL,
  `terpakai` int(15) NOT NULL,
  `afkir` int(15) NOT NULL,
  `tanggal_cincin` datetime NOT NULL,
  `sisa` int(15) NOT NULL,
  `ket_cin` text DEFAULT NULL,
  `stock_cincin_id_stock_cincin` int(11) NOT NULL,
  `stock_cincin_produk_id_produk` int(11) NOT NULL,
  `harga_cincin_id` int(11) NOT NULL,
  `packing_id` int(11) DEFAULT NULL,
  `author_session` varchar(150) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `log` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `history_cincin`
--

INSERT INTO `history_cincin` (`id`, `masuk`, `terpakai`, `afkir`, `tanggal_cincin`, `sisa`, `ket_cin`, `stock_cincin_id_stock_cincin`, `stock_cincin_produk_id_produk`, `harga_cincin_id`, `packing_id`, `author_session`, `created_at`, `updated_at`, `log`) VALUES
(1, 130, 0, 0, '2019-06-27 20:48:58', 200, NULL, 2, 11, 3, NULL, '1', '2019-06-27 13:49:20', NULL, 'INSERT'),
(3, 0, 50, 0, '2019-06-28 03:10:40', 150, 'Cincin Terpakai (Packing): 2019-06-28 03:10:40 based on ID packing: 2', 2, 11, 3, 2, '1', '2019-06-27 20:11:20', NULL, 'INSERT');

-- --------------------------------------------------------

--
-- Table structure for table `history_cukai`
--

CREATE TABLE `history_cukai` (
  `id` int(11) NOT NULL,
  `masuk_cukai` int(15) NOT NULL,
  `terpakai_cukailama` int(15) NOT NULL,
  `terpakai_cukaibaru` int(15) NOT NULL,
  `sisa_cukai` int(15) NOT NULL,
  `tanggal_cukai` datetime NOT NULL,
  `ket_hiscukai` tinytext DEFAULT NULL,
  `stock_cukai_id_stock_cukai` int(11) NOT NULL,
  `stock_cukai_id_sub_produk` int(11) NOT NULL,
  `stock_cukai_produk_id_produk` int(11) NOT NULL,
  `harga_cukai_id` int(11) NOT NULL,
  `packing_id_packing` int(11) DEFAULT NULL,
  `author_session` varchar(150) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT current_timestamp(),
  `log` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `history_cukai`
--

INSERT INTO `history_cukai` (`id`, `masuk_cukai`, `terpakai_cukailama`, `terpakai_cukaibaru`, `sisa_cukai`, `tanggal_cukai`, `ket_hiscukai`, `stock_cukai_id_stock_cukai`, `stock_cukai_id_sub_produk`, `stock_cukai_produk_id_produk`, `harga_cukai_id`, `packing_id_packing`, `author_session`, `created_at`, `updated_at`, `log`) VALUES
(2, 100, 0, 0, 100, '2019-06-27 22:02:08', NULL, 2, 23, 18, 3, NULL, '1', '2019-06-27 15:02:25', '2019-06-27 15:02:25', 'INSERT'),
(3, 0, 0, 5, 471, '2019-06-28 03:10:40', 'Cukai Terpakai (Packing): 2019-06-28 03:10:40 based on ID packing: 2', 1, 7, 11, 2, 2, '1', '2019-06-27 20:11:20', '2019-06-27 20:11:20', 'INSERT');

-- --------------------------------------------------------

--
-- Table structure for table `history_kemasan`
--

CREATE TABLE `history_kemasan` (
  `id` int(11) NOT NULL,
  `tanggal_hiskem` datetime NOT NULL,
  `masuk_kemasan` int(15) NOT NULL,
  `terpakai_kemasan` int(15) NOT NULL,
  `afkir_kemasan` int(15) NOT NULL,
  `stok_now` int(15) NOT NULL,
  `ket_kem` text DEFAULT NULL,
  `stock_kemasan_id_stock_kemasan` int(11) NOT NULL,
  `stock_kemasan_sub_produk_id` int(11) NOT NULL,
  `stock_kemasan_produk_id_produk` int(11) NOT NULL,
  `stock_kemasan_kemasan_id_kemasan` int(11) NOT NULL,
  `harga_kemasan_id` int(11) NOT NULL,
  `packing_id` int(11) DEFAULT NULL,
  `author_session` varchar(150) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `log` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `history_kemasan`
--

INSERT INTO `history_kemasan` (`id`, `tanggal_hiskem`, `masuk_kemasan`, `terpakai_kemasan`, `afkir_kemasan`, `stok_now`, `ket_kem`, `stock_kemasan_id_stock_kemasan`, `stock_kemasan_sub_produk_id`, `stock_kemasan_produk_id_produk`, `stock_kemasan_kemasan_id_kemasan`, `harga_kemasan_id`, `packing_id`, `author_session`, `created_at`, `updated_at`, `log`) VALUES
(7, '2019-06-17 15:01:46', 500, 0, 0, 500, 'sjahie', 3, 48, 54, 1, 4, NULL, '1', '2019-06-17 08:02:07', '2019-06-17 08:02:07', 'INSERT'),
(10, '2019-06-19 09:57:42', 500, 0, 0, 500, 'nsj', 4, 49, 54, 1, 5, NULL, '1', '2019-06-19 02:58:02', '2019-06-19 02:58:02', 'INSERT'),
(14, '2019-06-28 03:10:40', 0, 5, 0, 273, 'Kemasan Terpakai (Packing): 2019-06-28 03:10:40 based on ID packing: 2', 1, 7, 11, 1, 2, 2, '1', '2019-06-27 20:11:20', NULL, 'INSERT');

-- --------------------------------------------------------

--
-- Table structure for table `history_stiker`
--

CREATE TABLE `history_stiker` (
  `id` int(11) NOT NULL,
  `stock_stiker_id_stock_stiker` int(20) NOT NULL,
  `stock_stiker_sub_produk_id` int(11) NOT NULL,
  `stock_stiker_produk_id_produk` int(11) NOT NULL,
  `harga_stiker_id` int(11) NOT NULL,
  `packing_id` int(11) DEFAULT NULL,
  `masuk_luar` int(15) NOT NULL,
  `pakai_luar` int(15) NOT NULL,
  `hasil_luar` int(15) NOT NULL,
  `masuk_dalam` int(15) NOT NULL,
  `pakai_dalam` int(15) NOT NULL,
  `hasil_dalam` int(15) NOT NULL,
  `ket_sti` text DEFAULT NULL,
  `tanggal_hisstik` datetime NOT NULL,
  `author_session` varchar(150) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT current_timestamp(),
  `log` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `history_stiker`
--

INSERT INTO `history_stiker` (`id`, `stock_stiker_id_stock_stiker`, `stock_stiker_sub_produk_id`, `stock_stiker_produk_id_produk`, `harga_stiker_id`, `packing_id`, `masuk_luar`, `pakai_luar`, `hasil_luar`, `masuk_dalam`, `pakai_dalam`, `hasil_dalam`, `ket_sti`, `tanggal_hisstik`, `author_session`, `created_at`, `updated_at`, `log`) VALUES
(7, 4, 48, 54, 4, NULL, 500, 0, 1000, 500, 0, 1000, 'sjask', '2019-06-17 15:00:55', '1', '2019-06-17 08:01:24', '2019-06-19 04:38:56', 'INSERT'),
(10, 5, 49, 54, 5, NULL, 500, 0, 1489, 500, 0, 1489, 'skakn', '2019-06-19 09:57:01', '1', '2019-06-19 02:57:25', '2019-06-19 04:38:17', 'INSERT'),
(11, 2, 7, 11, 2, NULL, 112, 0, 312, 112, 0, 312, NULL, '2019-06-27 20:47:20', '1', '2019-06-27 13:48:09', '2019-06-27 20:10:13', 'INSERT'),
(13, 2, 7, 11, 2, 2, 0, 5, 195, 0, 5, 195, 'Stiker Terpakai (Packing): 2019-06-28 03:10:40 based on ID packing: 2', '2019-06-28 03:10:40', '1', '2019-06-27 20:11:20', '2019-06-27 20:11:20', 'INSERT');

-- --------------------------------------------------------

--
-- Table structure for table `items_object`
--

CREATE TABLE `items_object` (
  `id` int(11) NOT NULL,
  `sub_produk_id` int(11) NOT NULL,
  `order_id` int(11) DEFAULT NULL,
  `jml_item` int(45) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jenis`
--

CREATE TABLE `jenis` (
  `id_jenis` int(11) NOT NULL,
  `jenis` varchar(45) NOT NULL,
  `deskripsi_jenis` tinytext DEFAULT NULL,
  `keterangan_jenis` tinytext DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jenis`
--

INSERT INTO `jenis` (`id_jenis`, `jenis`, `deskripsi_jenis`, `keterangan_jenis`) VALUES
(8, 'Tembakau Asalan', 'Tembakau Asalan', 'Tembakau Asalan');

-- --------------------------------------------------------

--
-- Table structure for table `jenis_has_produk`
--

CREATE TABLE `jenis_has_produk` (
  `id_jnpro` int(11) NOT NULL,
  `jenis_id_jenis` int(11) NOT NULL,
  `produk_id_produk` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jenis_has_produk`
--

INSERT INTO `jenis_has_produk` (`id_jnpro`, `jenis_id_jenis`, `produk_id_produk`) VALUES
(10, 3, 10),
(11, 8, 11),
(12, 8, 12),
(13, 8, 13),
(14, 8, 14),
(15, 8, 15),
(16, 8, 16),
(17, 8, 17),
(18, 8, 18),
(19, 8, 19),
(20, 8, 20),
(21, 8, 21),
(22, 8, 22),
(23, 8, 23),
(24, 8, 24),
(25, 8, 25),
(26, 8, 26),
(27, 8, 27),
(28, 8, 28),
(29, 8, 29),
(30, 8, 30),
(31, 8, 31),
(32, 8, 32),
(33, 8, 33),
(34, 8, 34),
(35, 8, 35),
(36, 8, 36),
(37, 8, 37),
(38, 8, 38),
(39, 8, 39),
(40, 8, 40),
(41, 8, 41),
(42, 8, 42),
(43, 8, 43),
(44, 8, 44),
(45, 8, 45),
(46, 8, 46),
(47, 8, 47),
(48, 8, 48),
(49, 8, 49),
(50, 8, 50),
(51, 8, 51),
(52, 8, 52),
(53, 8, 53),
(54, 8, 54);

-- --------------------------------------------------------

--
-- Table structure for table `kategori`
--

CREATE TABLE `kategori` (
  `id_kategori` int(11) NOT NULL,
  `kategori` varchar(45) NOT NULL,
  `deskripsi_kategori` tinytext DEFAULT NULL,
  `keterangan_kategori` tinytext DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kategori`
--

INSERT INTO `kategori` (`id_kategori`, `kategori`, `deskripsi_kategori`, `keterangan_kategori`) VALUES
(1, 'FILLER 1', NULL, NULL),
(2, 'FILLER 2', NULL, NULL),
(3, 'OMBLAD', NULL, NULL),
(4, 'DEKBLAD', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `kemasan`
--

CREATE TABLE `kemasan` (
  `id_kemasan` int(11) NOT NULL,
  `nama_kemasan` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kemasan`
--

INSERT INTO `kemasan` (`id_kemasan`, `nama_kemasan`) VALUES
(1, 'WOODEN'),
(2, 'PAPER'),
(3, 'PLASTIK'),
(4, 'FOIL'),
(5, 'LOS'),
(6, 'TUBE'),
(7, 'KOTAK SENG'),
(8, 'AKRILIK'),
(9, 'PAPER');

-- --------------------------------------------------------

--
-- Table structure for table `laporan`
--

CREATE TABLE `laporan` (
  `id_laporan` int(11) NOT NULL,
  `curah_hujan` varchar(45) NOT NULL,
  `pagi` varchar(45) NOT NULL,
  `siang` varchar(45) NOT NULL,
  `sore` varchar(45) NOT NULL,
  `bak_air` varchar(45) NOT NULL,
  `lasiotrap_lemari` varchar(45) NOT NULL,
  `ds` varchar(45) NOT NULL,
  `store` varchar(45) NOT NULL,
  `agent` varchar(45) NOT NULL,
  `call` varchar(45) NOT NULL,
  `efektif_call` varchar(45) NOT NULL,
  `noo` varchar(45) NOT NULL,
  `direksi` tinytext NOT NULL,
  `kesulitan` tinytext NOT NULL,
  `tanggal_laporan` datetime NOT NULL,
  `author_session` varchar(200) NOT NULL,
  `status_laporan` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `level_user`
--

CREATE TABLE `level_user` (
  `id` int(11) NOT NULL,
  `level` varchar(45) NOT NULL,
  `deskripsi` tinytext DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `level_user`
--

INSERT INTO `level_user` (`id`, `level`, `deskripsi`) VALUES
(1, 'SUPER ADMIN', NULL),
(2, 'MANAGER', NULL),
(3, 'BAHAN BAKU', NULL),
(4, 'PROSES PRODUKSI', NULL),
(5, 'QUALITY CONTROL', NULL),
(6, 'RFS', NULL),
(7, 'STORE', NULL),
(8, 'AGENT', NULL),
(9, 'PHRI', NULL),
(10, 'NON PHRI', NULL),
(11, 'EXPORT', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_04_10_095836_add_new_column_stock_probaku', 1),
(4, '2019_04_11_031528_add_new_column_history_cincin', 1),
(5, '2019_04_12_032214_add_new_column_history_cukai', 2),
(6, '2019_04_12_032522_add_new_column_history_cukai1', 3),
(7, '2019_04_12_034220_add_new_column_history_stiker', 4),
(8, '2019_04_12_154520_add_new_column_history_kemasan', 5),
(9, '2019_04_18_141155_addcolumnfilling', 6),
(10, '2019_04_19_151711_add_columnstockfilling', 7),
(11, '2019_04_22_111431_addcolumn_binding', 8),
(12, '2019_04_22_111634_addcolumn_stock_binding', 9),
(13, '2019_05_04_114325_create_notifications_table', 10);

-- --------------------------------------------------------

--
-- Table structure for table `noc_inrfs`
--

CREATE TABLE `noc_inrfs` (
  `id` int(11) NOT NULL,
  `tanggal_inrfs` datetime NOT NULL,
  `status_lihat` int(1) NOT NULL,
  `status_inrfs` int(1) NOT NULL,
  `arah_dari` int(1) NOT NULL,
  `keterangan` tinytext DEFAULT NULL,
  `pesan_packing` tinytext DEFAULT NULL,
  `pesan_qc` tinytext DEFAULT NULL,
  `pesan_rfs` tinytext DEFAULT NULL,
  `author_session` varchar(45) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `log` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `noc_inrfs`
--

INSERT INTO `noc_inrfs` (`id`, `tanggal_inrfs`, `status_lihat`, `status_inrfs`, `arah_dari`, `keterangan`, `pesan_packing`, `pesan_qc`, `pesan_rfs`, `author_session`, `created_at`, `updated_at`, `log`) VALUES
(2, '2019-06-20 17:36:11', 1, 3, 0, 'siend', 'siop', NULL, NULL, '1', '2019-06-19 17:00:00', '2019-06-25 20:56:44', 'INSERT'),
(3, '2019-06-20 17:52:52', 1, 3, 1, 'coba jak', 'cdgfgfbxhf', NULL, NULL, '1', '2019-06-19 17:00:00', '2019-06-25 19:32:28', 'INSERT'),
(4, '2019-06-26 02:57:15', 1, 3, 0, 'dari packing', NULL, NULL, NULL, '1', '2019-06-25 17:00:00', '2019-06-27 05:22:06', 'INSERT'),
(5, '2019-06-27 12:21:23', 1, 1, 0, 'vdgd', NULL, NULL, NULL, '1', '2019-06-26 17:00:00', '2019-06-27 05:47:57', 'INSERT'),
(6, '2019-06-28 15:32:43', 1, 3, 0, 'pale', NULL, NULL, NULL, '1', '2019-06-27 17:00:00', '2019-06-29 04:21:48', 'INSERT'),
(10, '2019-06-29 11:18:46', 0, 2, 0, 'kurang cocok', 'sadvf', 'sad de', 'ccdcd', '1', '2019-06-28 17:00:00', '2019-07-01 21:12:45', 'INSERT'),
(11, '2019-06-29 14:29:11', 1, 3, 1, 'sw', 'coba cek kembali', NULL, NULL, '1', '2019-06-28 17:00:00', '2019-07-01 19:42:45', 'INSERT');

-- --------------------------------------------------------

--
-- Table structure for table `noc_items`
--

CREATE TABLE `noc_items` (
  `id` int(11) NOT NULL,
  `sub_produk_id` int(11) NOT NULL,
  `noc_inrfs_id` int(11) NOT NULL,
  `jml_nocitm` int(20) NOT NULL,
  `uraian` tinytext DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `noc_items`
--

INSERT INTO `noc_items` (`id`, `sub_produk_id`, `noc_inrfs_id`, `jml_nocitm`, `uraian`, `created_at`, `updated_at`, `deleted_at`) VALUES
(2, 7, 2, 2, NULL, '2019-06-19 17:00:00', '2019-06-29 04:18:46', NULL),
(3, 7, 3, 2, NULL, '2019-06-19 17:00:00', '2019-06-29 04:18:46', NULL),
(4, 7, 4, 4, NULL, '2019-06-25 17:00:00', '2019-06-29 04:18:46', NULL),
(5, 7, 5, 5, NULL, '2019-06-26 17:00:00', '2019-06-29 04:18:46', NULL),
(6, 7, 6, 2, NULL, '2019-06-27 17:00:00', '2019-06-29 04:21:20', NULL),
(10, 7, 10, 2, NULL, NULL, '2019-06-29 04:21:20', NULL),
(11, 7, 11, 1, NULL, '2019-06-28 17:00:00', '2019-06-29 07:30:13', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `order`
--

CREATE TABLE `order` (
  `id` int(11) NOT NULL,
  `tanggal_order` datetime NOT NULL,
  `keterangan_order` tinytext NOT NULL,
  `status_lihatrfs` int(1) DEFAULT NULL,
  `verifikasi_rfs` int(1) DEFAULT NULL,
  `status_lihatbagan` int(1) DEFAULT NULL,
  `status_terima` int(1) NOT NULL,
  `users_id` int(11) NOT NULL,
  `rfs_id` int(11) DEFAULT NULL,
  `author_session` varchar(200) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `packing`
--

CREATE TABLE `packing` (
  `id` int(11) NOT NULL,
  `awalbatang` tinytext NOT NULL,
  `batangpack` int(20) NOT NULL,
  `hasil_batangpack` tinytext NOT NULL,
  `tambah_packing` int(20) NOT NULL,
  `hasil_packing` int(20) NOT NULL,
  `keterangan_packing` tinytext DEFAULT NULL,
  `tanggal_packing` datetime NOT NULL,
  `stock_packing_id` int(11) NOT NULL,
  `stock_qc_id` int(11) DEFAULT NULL,
  `author_session` varchar(200) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT current_timestamp(),
  `log` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `packing`
--

INSERT INTO `packing` (`id`, `awalbatang`, `batangpack`, `hasil_batangpack`, `tambah_packing`, `hasil_packing`, `keterangan_packing`, `tanggal_packing`, `stock_packing_id`, `stock_qc_id`, `author_session`, `created_at`, `updated_at`, `log`) VALUES
(2, '', 50, '', 5, 30, NULL, '2019-06-28 03:10:40', 6, 1, '1', '2019-06-27 20:11:20', '2019-06-27 20:11:20', 'INSERT');

-- --------------------------------------------------------

--
-- Table structure for table `penerimaan`
--

CREATE TABLE `penerimaan` (
  `id` int(11) NOT NULL,
  `penjualan_id` int(11) NOT NULL,
  `tanggal_penerimaan` datetime NOT NULL,
  `nominal_penerimaan` int(20) NOT NULL,
  `status_pembayaran` varchar(45) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `penjualan`
--

CREATE TABLE `penjualan` (
  `id` int(11) NOT NULL,
  `no_invoice` varchar(200) NOT NULL,
  `harga_cerutu` int(20) NOT NULL,
  `diskon_penjualan` int(20) NOT NULL,
  `ongkir_penjualan` int(20) NOT NULL,
  `total_penjualan` int(20) NOT NULL,
  `yang_dibayar` int(20) NOT NULL,
  `tanggal_penjualan` datetime NOT NULL,
  `customer_id_customer` int(11) NOT NULL,
  `lokasi_kirim` tinytext NOT NULL,
  `depature_date` datetime DEFAULT NULL,
  `vessel` varchar(100) DEFAULT NULL,
  `port_of_loading` varchar(100) DEFAULT NULL,
  `port_of_destination` varchar(100) DEFAULT NULL,
  `keterangan_penjualan` tinytext DEFAULT NULL,
  `status_pembayar` varchar(20) NOT NULL,
  `status_lunas` varchar(20) NOT NULL,
  `author_session` varchar(200) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `log` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pressing`
--

CREATE TABLE `pressing` (
  `id` int(11) NOT NULL,
  `tambah_pres` int(20) NOT NULL,
  `hasil_akhirp` int(20) NOT NULL,
  `lama` tinytext NOT NULL,
  `keterangan_pres` text DEFAULT NULL,
  `tanggal_pres` datetime NOT NULL,
  `stock_pressing_id` int(11) NOT NULL,
  `stock_binding_id` int(11) NOT NULL,
  `author_session` varchar(150) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT current_timestamp(),
  `log` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pressing`
--

INSERT INTO `pressing` (`id`, `tambah_pres`, `hasil_akhirp`, `lama`, `keterangan_pres`, `tanggal_pres`, `stock_pressing_id`, `stock_binding_id`, `author_session`, `created_at`, `updated_at`, `log`) VALUES
(1, 300, 0, '03:29:00', 'joank', '2019-06-18 11:28:42', 9, 9, '1', '2019-06-18 04:29:13', '2019-06-18 04:29:13', 'INSERT'),
(2, 500, 0, '02:17:00', 'jska', '2019-06-18 16:17:33', 4, 4, '1', '2019-06-18 09:18:00', '2019-06-18 09:18:00', 'INSERT'),
(3, 500, 0, '03:18:00', 'asad', '2019-06-18 16:18:04', 5, 5, '1', '2019-06-18 09:18:28', '2019-06-18 09:18:28', 'INSERT'),
(4, 500, 0, '03:18:00', 'sajk', '2019-06-18 16:18:33', 6, 6, '1', '2019-06-18 09:18:57', '2019-06-18 09:18:57', 'INSERT'),
(5, 500, 0, '04:19:00', 'sajk', '2019-06-18 16:19:01', 7, 7, '1', '2019-06-18 09:19:28', '2019-06-18 09:19:28', 'INSERT'),
(6, 1005, 0, '03:00:00', NULL, '2019-06-27 18:59:40', 1, 1, '1', '2019-07-01 18:03:25', '2019-07-01 18:03:25', 'INSERT');

-- --------------------------------------------------------

--
-- Table structure for table `produk`
--

CREATE TABLE `produk` (
  `id_produk` int(11) NOT NULL,
  `produk` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `produk`
--

INSERT INTO `produk` (`id_produk`, `produk`) VALUES
(11, 'Robusto'),
(12, 'Corona'),
(13, 'Half Corona'),
(14, 'Maumere'),
(15, 'Cigar Master'),
(16, 'El Nino'),
(17, 'Mondlicht'),
(18, 'Jember Cigar'),
(19, 'C99'),
(20, 'Sumatera'),
(21, 'Don Agusto'),
(22, 'El Bomba'),
(23, 'Don Juan'),
(24, 'Sakera'),
(25, 'Merubetiri'),
(26, 'NFC Green'),
(27, 'NFC Red'),
(28, 'SIGO 1'),
(29, 'SIGO 2'),
(30, 'SIGO 3'),
(31, 'SIGO 4'),
(32, 'SIGO 5'),
(33, 'SIGO 6'),
(34, 'Robustos'),
(35, 'HK 52'),
(36, 'HK 54'),
(37, 'HK 56'),
(38, 'Magic'),
(39, 'Genio'),
(40, 'R. Supremo'),
(41, 'Piramide'),
(42, 'Majestuso'),
(43, 'Horsetail'),
(44, 'Secretos'),
(45, 'Talama'),
(46, 'Tambo'),
(47, 'FR Ring 52'),
(48, 'FR Ring 60'),
(49, 'Java Blend'),
(50, '212'),
(51, 'Ceo Java'),
(52, 'Ceo Cappucino'),
(53, 'Grand Moonlight'),
(54, 'Colection');

-- --------------------------------------------------------

--
-- Table structure for table `quality_control`
--

CREATE TABLE `quality_control` (
  `id` int(11) NOT NULL,
  `accept` int(20) NOT NULL,
  `mutasi_batang` int(20) NOT NULL,
  `back_fill` decimal(20,2) NOT NULL,
  `back_bind` decimal(20,2) NOT NULL,
  `back_wrap` decimal(20,2) NOT NULL,
  `back_lazio` decimal(20,2) NOT NULL,
  `hasil_qc` int(20) NOT NULL,
  `keterangan_qc` text DEFAULT NULL,
  `lama_qc` tinytext NOT NULL,
  `tanggal_qc` datetime NOT NULL,
  `stock_qc_id` int(11) NOT NULL,
  `stock_drying3_id` int(11) NOT NULL,
  `author_session` varchar(200) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT current_timestamp(),
  `log` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `quality_control`
--

INSERT INTO `quality_control` (`id`, `accept`, `mutasi_batang`, `back_fill`, `back_bind`, `back_wrap`, `back_lazio`, `hasil_qc`, `keterangan_qc`, `lama_qc`, `tanggal_qc`, `stock_qc_id`, `stock_drying3_id`, `author_session`, `created_at`, `updated_at`, `log`) VALUES
(1, 500, 0, '0.00', '0.00', '0.00', '0.00', 0, 'sadfrv', '03:50:00', '2019-06-18 16:38:22', 4, 4, '1', '2019-06-18 09:48:55', '2019-06-18 09:48:55', 'INSERT'),
(2, 500, 0, '0.00', '0.00', '0.00', '0.00', 0, 'asa', '01:49:00', '2019-06-18 16:49:01', 5, 5, '1', '2019-06-18 09:49:54', '2019-06-18 09:49:54', 'INSERT'),
(3, 500, 0, '0.00', '0.00', '0.00', '0.00', 0, 'asadscd', '02:50:00', '2019-06-18 16:50:13', 6, 6, '1', '2019-06-18 09:50:51', '2019-06-18 09:50:51', 'INSERT'),
(4, 500, 0, '0.00', '0.00', '0.00', '0.00', 0, 'sdsd', '03:51:00', '2019-06-18 16:50:57', 7, 7, '1', '2019-06-18 09:51:34', '2019-06-18 09:51:34', 'INSERT'),
(7, 50, 0, '0.00', '0.00', '0.00', '0.00', 960, 'zxw', '03:00:00', '2019-06-27 20:33:45', 1, 1, '1', '2019-06-27 13:35:34', '2019-06-27 13:37:35', 'INSERT');

-- --------------------------------------------------------

--
-- Table structure for table `return`
--

CREATE TABLE `return` (
  `id_return` int(11) NOT NULL,
  `jenis_id_jenis` int(11) NOT NULL,
  `sub_produk_id_sub_produk` int(11) NOT NULL,
  `stock_bagan_id` int(11) DEFAULT NULL,
  `jml_forbagan` int(20) DEFAULT NULL,
  `tanggal_rtnbagan` datetime DEFAULT NULL,
  `ket_bagan` tinytext DEFAULT NULL,
  `usersidbagan` varchar(150) DEFAULT NULL,
  `stock_rfs_id` int(11) DEFAULT NULL,
  `jml_forrfs` int(20) DEFAULT NULL,
  `tanggal_rtnrfs` datetime DEFAULT NULL,
  `ket_rfs` tinytext DEFAULT NULL,
  `stock_qc_id` int(11) DEFAULT NULL,
  `stock_binding_id` int(11) DEFAULT NULL,
  `stock_wrapping_id` int(11) DEFAULT NULL,
  `stock_packing_id` int(11) DEFAULT NULL,
  `rtn_bind` int(20) DEFAULT NULL,
  `rtn_wrap` int(20) DEFAULT NULL,
  `rtn_pack` int(20) DEFAULT NULL,
  `rtn_lazio` int(20) DEFAULT NULL,
  `ket_qc` int(20) DEFAULT NULL,
  `tanggal_rtnqc` datetime DEFAULT NULL,
  `status_return` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rfs_his`
--

CREATE TABLE `rfs_his` (
  `id` int(11) NOT NULL,
  `tanggal_rfs` datetime NOT NULL,
  `keterangan_rfs` tinytext DEFAULT NULL,
  `users_id` int(11) NOT NULL,
  `author_session` varchar(200) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT current_timestamp(),
  `log` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `satuan_harga`
--

CREATE TABLE `satuan_harga` (
  `id` int(11) NOT NULL,
  `jenis_id_jenis` int(11) NOT NULL,
  `kategori_id_kategori` int(11) NOT NULL,
  `tanggal_harga` datetime NOT NULL,
  `satuan_berat` varchar(45) NOT NULL,
  `harga_tembakau` int(45) NOT NULL,
  `keterangan_harga` tinytext DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `satuan_harga`
--

INSERT INTO `satuan_harga` (`id`, `jenis_id_jenis`, `kategori_id_kategori`, `tanggal_harga`, `satuan_berat`, `harga_tembakau`, `keterangan_harga`, `created_at`, `updated_at`) VALUES
(3, 8, 1, '2019-06-05 12:25:00', '1', 0, 'nyobak', '2019-06-05 15:04:15', '2019-06-18 02:38:36'),
(4, 8, 3, '2019-06-18 09:48:25', '1', 0, 'sip', '2019-06-18 02:49:02', '2019-06-18 02:49:02'),
(5, 8, 4, '2019-06-18 10:15:43', '1', 0, 'CNJD', '2019-06-18 03:16:02', '2019-06-18 03:16:02');

-- --------------------------------------------------------

--
-- Table structure for table `setting`
--

CREATE TABLE `setting` (
  `idsetting` int(11) NOT NULL,
  `direktur_utama` varchar(100) DEFAULT NULL,
  `direktur_operasional` varchar(100) DEFAULT NULL,
  `kabag_produksi` varchar(100) DEFAULT NULL,
  `quality_control` varchar(100) DEFAULT NULL,
  `ready_for_sale` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `setting`
--

INSERT INTO `setting` (`idsetting`, `direktur_utama`, `direktur_operasional`, `kabag_produksi`, `quality_control`, `ready_for_sale`) VALUES
(1, 'Ir. H Febrian Ananta Kahar', 'Ir. H Imam Wahid Wahyudi', 'Slamet Wijaya', 'Risma', 'Citra Wahyu Lestari');

-- --------------------------------------------------------

--
-- Table structure for table `stock_bagan`
--

CREATE TABLE `stock_bagan` (
  `id` int(11) NOT NULL,
  `stock_bagan` int(20) NOT NULL,
  `sub_produk_id` int(11) NOT NULL,
  `users_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stock_bagan`
--

INSERT INTO `stock_bagan` (`id`, `stock_bagan`, `sub_produk_id`, `users_id`, `created_at`, `updated_at`) VALUES
(1, 2, 0, 20, '2019-06-23 08:40:00', '2019-06-23 08:42:15');

-- --------------------------------------------------------

--
-- Table structure for table `stock_binding`
--

CREATE TABLE `stock_binding` (
  `id` int(20) NOT NULL,
  `jml_bind` int(20) NOT NULL,
  `stock_weisb` decimal(20,2) NOT NULL,
  `jenis_has_produk_id_jnpro` int(11) NOT NULL,
  `jenis_has_produk_id_jenis` int(11) NOT NULL,
  `jenis_has_produk_id_produk` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stock_binding`
--

INSERT INTO `stock_binding` (`id`, `jml_bind`, `stock_weisb`, `jenis_has_produk_id_jnpro`, `jenis_has_produk_id_jenis`, `jenis_has_produk_id_produk`, `created_at`, `updated_at`) VALUES
(1, 0, '0.00', 11, 8, 11, '2019-06-08 07:13:50', '2019-06-27 12:00:58'),
(2, 5, '0.00', 13, 8, 13, '2019-06-13 12:48:34', '2019-06-13 12:49:07'),
(3, 0, '0.00', 14, 8, 14, '2019-06-13 15:39:59', '2019-06-13 15:40:32'),
(4, 0, '0.00', 12, 8, 12, '2019-06-17 08:09:32', '2019-06-18 09:18:00'),
(5, 0, '0.00', 15, 8, 15, '2019-06-17 08:10:05', '2019-06-18 09:18:28'),
(6, 0, '0.00', 16, 8, 16, '2019-06-17 08:10:36', '2019-06-18 09:18:57'),
(7, 0, '0.00', 19, 8, 19, '2019-06-17 08:11:04', '2019-06-18 09:19:28'),
(8, 5, '-500.00', 17, 8, 17, '2019-06-17 08:46:46', '2019-06-17 11:29:18'),
(9, 0, '0.00', 18, 8, 18, '2019-06-18 04:28:36', '2019-06-18 04:29:13'),
(11, 300, '0.00', 24, 8, 24, '2019-07-01 18:33:30', '2019-07-01 19:33:44');

-- --------------------------------------------------------

--
-- Table structure for table `stock_cincin`
--

CREATE TABLE `stock_cincin` (
  `id_stock_cincin` int(11) NOT NULL,
  `stock_qty_cincin` int(20) NOT NULL,
  `produk_id_produk` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stock_cincin`
--

INSERT INTO `stock_cincin` (`id_stock_cincin`, `stock_qty_cincin`, `produk_id_produk`) VALUES
(1, 15, 12),
(2, 150, 11),
(3, 340, 54);

-- --------------------------------------------------------

--
-- Table structure for table `stock_cool`
--

CREATE TABLE `stock_cool` (
  `id` int(11) NOT NULL,
  `jml_cool` int(20) NOT NULL,
  `jenis_has_produk_id_jnpro` int(11) NOT NULL,
  `jenis_has_produk_id_jenis` int(11) NOT NULL,
  `jenis_has_produk_id_produk` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stock_cool`
--

INSERT INTO `stock_cool` (`id`, `jml_cool`, `jenis_has_produk_id_jnpro`, `jenis_has_produk_id_jenis`, `jenis_has_produk_id_produk`, `created_at`, `updated_at`) VALUES
(1, 5, 11, 8, 11, '2019-06-08 14:00:43', '2019-07-01 19:39:48'),
(2, 0, 13, 8, 13, '2019-06-13 12:53:07', '2019-06-13 12:54:00'),
(3, 0, 14, 8, 14, '2019-06-13 15:44:25', '2019-06-13 15:45:23'),
(4, 0, 12, 8, 12, '2019-06-18 09:33:12', '2019-06-18 09:36:06'),
(5, 0, 15, 8, 15, '2019-06-18 09:34:10', '2019-06-18 09:36:38'),
(6, 0, 16, 8, 16, '2019-06-18 09:34:42', '2019-06-18 09:37:13'),
(7, 0, 19, 8, 19, '2019-06-18 09:35:11', '2019-06-18 09:37:46');

-- --------------------------------------------------------

--
-- Table structure for table `stock_cukai`
--

CREATE TABLE `stock_cukai` (
  `id_stock_cukai` int(11) NOT NULL,
  `stock_qty_cukai` int(20) NOT NULL,
  `sub_produk_id_sub_produk` int(11) NOT NULL,
  `sub_produk_produk_id_produk` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stock_cukai`
--

INSERT INTO `stock_cukai` (`id_stock_cukai`, `stock_qty_cukai`, `sub_produk_id_sub_produk`, `sub_produk_produk_id_produk`) VALUES
(1, 471, 7, 11),
(2, 100, 23, 18);

-- --------------------------------------------------------

--
-- Table structure for table `stock_drying1`
--

CREATE TABLE `stock_drying1` (
  `id` int(11) NOT NULL,
  `jml_dry1` int(20) NOT NULL,
  `jenis_has_produk_id_jnpro` int(11) NOT NULL,
  `jenis_has_produk_id_jenis` int(11) NOT NULL,
  `jenis_has_produk_id_produk` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stock_drying1`
--

INSERT INTO `stock_drying1` (`id`, `jml_dry1`, `jenis_has_produk_id_jnpro`, `jenis_has_produk_id_jenis`, `jenis_has_produk_id_produk`, `created_at`, `updated_at`) VALUES
(1, 5, 11, 8, 11, '2019-06-08 13:12:02', '2019-06-27 12:08:01'),
(2, 0, 13, 8, 13, '2019-06-13 12:50:32', '2019-06-13 12:51:13'),
(3, 0, 14, 8, 14, '2019-06-13 15:42:04', '2019-06-13 15:42:34'),
(4, 500, 12, 8, 12, '2019-06-17 11:58:25', '2019-06-18 09:24:58'),
(5, 0, 15, 8, 15, '2019-06-18 09:22:51', '2019-06-18 09:25:35'),
(6, 0, 16, 8, 16, '2019-06-18 09:23:21', '2019-06-18 09:26:08'),
(7, 0, 19, 8, 19, '2019-06-18 09:23:53', '2019-06-18 09:26:40');

-- --------------------------------------------------------

--
-- Table structure for table `stock_drying2`
--

CREATE TABLE `stock_drying2` (
  `id` int(11) NOT NULL,
  `jml_dry2` int(20) NOT NULL,
  `jenis_has_produk_id_jnpro` int(11) NOT NULL,
  `jenis_has_produk_id_jenis` int(11) NOT NULL,
  `jenis_has_produk_id_produk` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stock_drying2`
--

INSERT INTO `stock_drying2` (`id`, `jml_dry2`, `jenis_has_produk_id_jnpro`, `jenis_has_produk_id_jenis`, `jenis_has_produk_id_produk`, `created_at`, `updated_at`) VALUES
(1, 0, 11, 8, 11, '2019-06-08 13:38:58', '2019-06-27 12:09:15'),
(2, 0, 13, 8, 13, '2019-06-13 12:51:44', '2019-06-13 12:52:16'),
(3, 0, 14, 8, 14, '2019-06-13 15:43:05', '2019-06-13 15:43:34'),
(4, 0, 12, 8, 12, '2019-06-18 09:27:46', '2019-06-18 09:30:06'),
(5, 0, 15, 8, 15, '2019-06-18 09:28:18', '2019-06-18 09:30:41'),
(6, 0, 16, 8, 16, '2019-06-18 09:28:54', '2019-06-18 09:31:47'),
(7, 0, 19, 8, 19, '2019-06-18 09:29:30', '2019-06-18 09:31:12');

-- --------------------------------------------------------

--
-- Table structure for table `stock_drying3`
--

CREATE TABLE `stock_drying3` (
  `id` int(11) NOT NULL,
  `jml_dry3` int(20) NOT NULL,
  `jenis_has_produk_id_jnpro` int(11) NOT NULL,
  `jenis_has_produk_id_jenis` int(11) NOT NULL,
  `jenis_has_produk_id_produk` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stock_drying3`
--

INSERT INTO `stock_drying3` (`id`, `jml_dry3`, `jenis_has_produk_id_jnpro`, `jenis_has_produk_id_jenis`, `jenis_has_produk_id_produk`, `created_at`, `updated_at`) VALUES
(1, 960, 11, 8, 11, '2019-06-08 14:08:12', '2019-06-27 13:37:35'),
(2, 0, 13, 8, 13, '2019-06-13 12:54:00', '2019-06-13 12:55:02'),
(3, 0, 14, 8, 14, '2019-06-13 15:45:23', '2019-06-13 15:46:24'),
(4, 0, 12, 8, 12, '2019-06-18 09:36:06', '2019-06-18 09:48:55'),
(5, 0, 15, 8, 15, '2019-06-18 09:36:38', '2019-06-18 09:49:54'),
(6, 0, 16, 8, 16, '2019-06-18 09:37:13', '2019-06-18 09:50:51'),
(7, 0, 19, 8, 19, '2019-06-18 09:37:46', '2019-06-18 09:51:34');

-- --------------------------------------------------------

--
-- Table structure for table `stock_filling`
--

CREATE TABLE `stock_filling` (
  `id` int(20) NOT NULL,
  `jml_fill` decimal(20,2) NOT NULL,
  `stock_weisf` decimal(20,2) NOT NULL,
  `jenis_has_produk_id_jnpro` int(11) NOT NULL,
  `jenis_has_produk_id_jenis` int(11) NOT NULL,
  `jenis_has_produk_id_produk` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stock_filling`
--

INSERT INTO `stock_filling` (`id`, `jml_fill`, `stock_weisf`, `jenis_has_produk_id_jnpro`, `jenis_has_produk_id_jenis`, `jenis_has_produk_id_produk`, `created_at`, `updated_at`) VALUES
(1, '15.00', '0.00', 11, 8, 11, '2019-06-08 06:52:01', '2019-06-27 11:51:13'),
(2, '5.00', '0.00', 13, 8, 13, '2019-06-13 12:46:51', '2019-06-13 12:48:34'),
(3, '0.00', '0.00', 14, 8, 14, '2019-06-13 15:39:26', '2019-06-13 15:39:59'),
(4, '0.00', '0.00', 12, 8, 12, '2019-06-17 08:06:50', '2019-06-18 09:15:53'),
(5, '0.00', '0.00', 15, 8, 15, '2019-06-17 08:07:34', '2019-06-18 09:16:32'),
(6, '0.00', '0.00', 16, 8, 16, '2019-06-17 08:08:09', '2019-06-18 09:16:56'),
(7, '0.00', '0.00', 19, 8, 19, '2019-06-17 08:08:38', '2019-06-18 09:17:25'),
(8, '5.00', '0.00', 17, 8, 17, '2019-06-17 08:31:53', '2019-06-17 11:54:33'),
(9, '0.00', '0.00', 18, 8, 18, '2019-06-18 04:27:57', '2019-06-18 04:28:36'),
(10, '350.00', '0.00', 24, 8, 24, '2019-07-01 18:28:44', '2019-07-01 19:33:44');

-- --------------------------------------------------------

--
-- Table structure for table `stock_frezer`
--

CREATE TABLE `stock_frezer` (
  `id` int(11) NOT NULL,
  `jml_fre` int(11) NOT NULL,
  `jenis_has_produk_id_jnpro` int(11) NOT NULL,
  `jenis_has_produk_id_jenis` int(11) NOT NULL,
  `jenis_has_produk_id_produk` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stock_frezer`
--

INSERT INTO `stock_frezer` (`id`, `jml_fre`, `jenis_has_produk_id_jnpro`, `jenis_has_produk_id_jenis`, `jenis_has_produk_id_produk`, `created_at`, `updated_at`) VALUES
(1, 5, 11, 8, 11, '2019-06-08 13:30:58', '2019-06-27 12:08:42'),
(2, 0, 13, 8, 13, '2019-06-13 12:51:13', '2019-06-13 12:51:44'),
(3, 0, 14, 8, 14, '2019-06-13 15:42:34', '2019-06-13 15:43:05'),
(4, 0, 12, 8, 12, '2019-06-18 09:24:58', '2019-06-18 09:27:46'),
(5, 0, 15, 8, 15, '2019-06-18 09:25:35', '2019-06-18 09:28:18'),
(6, 0, 16, 8, 16, '2019-06-18 09:26:08', '2019-06-18 09:28:54'),
(7, 0, 19, 8, 19, '2019-06-18 09:26:40', '2019-06-18 09:29:30');

-- --------------------------------------------------------

--
-- Table structure for table `stock_fumigasi`
--

CREATE TABLE `stock_fumigasi` (
  `id` int(11) NOT NULL,
  `jml_fum` int(20) DEFAULT NULL,
  `jenis_has_produk_id_jnpro` int(11) NOT NULL,
  `jenis_has_produk_id_jenis` int(11) NOT NULL,
  `jenis_has_produk_id_produk` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stock_fumigasi`
--

INSERT INTO `stock_fumigasi` (`id`, `jml_fum`, `jenis_has_produk_id_jnpro`, `jenis_has_produk_id_jenis`, `jenis_has_produk_id_produk`, `created_at`, `updated_at`) VALUES
(1, 5, 11, 8, 11, '2019-06-08 13:47:15', '2019-07-01 19:39:48'),
(2, 0, 13, 8, 13, '2019-06-13 12:52:16', '2019-06-13 12:53:07'),
(3, 0, 14, 8, 14, '2019-06-13 15:43:34', '2019-06-13 15:44:25'),
(4, 0, 12, 8, 12, '2019-06-18 09:30:06', '2019-06-18 09:33:12'),
(5, 0, 15, 8, 15, '2019-06-18 09:30:41', '2019-06-18 09:34:10'),
(6, 0, 19, 8, 19, '2019-06-18 09:31:12', '2019-06-18 09:35:11'),
(7, 0, 16, 8, 16, '2019-06-18 09:31:47', '2019-06-18 09:34:42');

-- --------------------------------------------------------

--
-- Table structure for table `stock_kemasan`
--

CREATE TABLE `stock_kemasan` (
  `id_stock_kemasan` int(11) NOT NULL,
  `stock_kemasan` int(20) NOT NULL,
  `sub_produk_id` int(11) NOT NULL,
  `produk_id_produk` int(11) NOT NULL,
  `kemasan_id_kemasan` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stock_kemasan`
--

INSERT INTO `stock_kemasan` (`id_stock_kemasan`, `stock_kemasan`, `sub_produk_id`, `produk_id_produk`, `kemasan_id_kemasan`) VALUES
(1, 273, 7, 11, 1),
(2, 492, 47, 54, 1),
(3, 500, 48, 54, 1),
(4, 500, 49, 54, 1),
(5, 0, 12, 13, 6);

-- --------------------------------------------------------

--
-- Table structure for table `stock_packing`
--

CREATE TABLE `stock_packing` (
  `id` int(11) NOT NULL,
  `jml_packing` int(20) NOT NULL,
  `sub_produk_id_sub_produk` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stock_packing`
--

INSERT INTO `stock_packing` (`id`, `jml_packing`, `sub_produk_id_sub_produk`, `created_at`, `updated_at`) VALUES
(6, 56, 7, '2019-06-08 15:23:15', '2019-06-29 07:45:20'),
(11, 8, 47, '2019-06-14 11:03:02', '2019-06-19 04:39:03'),
(12, 0, 48, '2019-06-18 10:36:11', '2019-06-19 04:38:56'),
(13, 0, 49, '2019-06-19 04:30:21', '2019-06-19 04:38:17');

-- --------------------------------------------------------

--
-- Table structure for table `stock_pressing`
--

CREATE TABLE `stock_pressing` (
  `id` int(11) NOT NULL,
  `jml_pres` int(20) NOT NULL,
  `jenis_has_produk_id_jnpro` int(11) NOT NULL,
  `jenis_has_produk_id_jenis` int(11) NOT NULL,
  `jenis_has_produk_id_produk` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stock_pressing`
--

INSERT INTO `stock_pressing` (`id`, `jml_pres`, `jenis_has_produk_id_jnpro`, `jenis_has_produk_id_jenis`, `jenis_has_produk_id_produk`, `created_at`, `updated_at`) VALUES
(1, 0, 11, 8, 11, '2019-06-08 07:20:34', '2019-06-27 12:03:34'),
(2, 0, 13, 8, 13, '2019-06-13 12:49:07', '2019-06-13 12:49:51'),
(3, 0, 14, 8, 14, '2019-06-13 15:40:32', '2019-06-13 15:41:32'),
(4, 0, 12, 8, 12, '2019-06-17 08:12:03', '2019-06-18 09:20:08'),
(5, 0, 15, 8, 15, '2019-06-17 08:12:34', '2019-06-18 09:20:37'),
(6, 0, 16, 8, 16, '2019-06-17 08:13:13', '2019-06-18 09:21:16'),
(7, 0, 19, 8, 19, '2019-06-17 08:13:53', '2019-06-18 09:21:47'),
(8, 95, 17, 8, 17, '2019-06-17 08:53:53', '2019-06-17 11:54:33'),
(9, 320, 18, 8, 18, '2019-06-18 04:29:13', '2019-06-18 06:06:16');

-- --------------------------------------------------------

--
-- Table structure for table `stock_probaku`
--

CREATE TABLE `stock_probaku` (
  `id` int(11) NOT NULL,
  `jml_produksi` decimal(20,2) NOT NULL,
  `jml_bahanbaku` decimal(20,2) NOT NULL,
  `jenis_id_jenis` int(11) NOT NULL,
  `kategori_id_kategori` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stock_probaku`
--

INSERT INTO `stock_probaku` (`id`, `jml_produksi`, `jml_bahanbaku`, `jenis_id_jenis`, `kategori_id_kategori`, `created_at`, `updated_at`) VALUES
(1, '83025.00', '1290.00', 8, 1, '2019-06-07 09:53:33', '2019-06-28 07:46:25'),
(2, '83350.00', '1230.00', 8, 3, '2019-06-08 06:34:16', '2019-07-01 19:33:44'),
(3, '85000.00', '1190.00', 8, 4, '2019-06-08 06:34:49', '2019-06-28 07:46:25'),
(4, '0.00', '50.00', 8, 2, '2019-06-25 19:29:44', '2019-06-27 13:37:35');

-- --------------------------------------------------------

--
-- Table structure for table `stock_qc`
--

CREATE TABLE `stock_qc` (
  `id` int(11) NOT NULL,
  `jml_qc` int(20) NOT NULL,
  `jenis_has_produk_id_jnpro` int(11) NOT NULL,
  `jenis_has_produk_id_jenis` int(11) NOT NULL,
  `jenis_has_produk_id_produk` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stock_qc`
--

INSERT INTO `stock_qc` (`id`, `jml_qc`, `jenis_has_produk_id_jnpro`, `jenis_has_produk_id_jenis`, `jenis_has_produk_id_produk`, `created_at`, `updated_at`) VALUES
(1, 285, 11, 8, 11, '2019-06-08 14:14:53', '2019-06-29 07:45:20'),
(2, 455, 13, 8, 13, '2019-06-13 12:55:02', '2019-06-19 04:39:03'),
(3, 460, 14, 8, 14, '2019-06-13 15:46:24', '2019-06-19 04:39:03'),
(4, 500, 12, 8, 12, '2019-06-18 09:48:55', '2019-06-19 04:38:56'),
(5, 500, 15, 8, 15, '2019-06-18 09:49:54', '2019-06-19 04:38:56'),
(6, 500, 16, 8, 16, '2019-06-18 09:50:51', '2019-06-19 04:38:56'),
(7, 500, 19, 8, 19, '2019-06-18 09:51:34', '2019-06-19 04:38:56');

-- --------------------------------------------------------

--
-- Table structure for table `stock_rfs`
--

CREATE TABLE `stock_rfs` (
  `id` int(11) NOT NULL,
  `sub_produk_id_sub_produk` int(11) NOT NULL,
  `jml_rfs` int(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stock_rfs`
--

INSERT INTO `stock_rfs` (`id`, `sub_produk_id_sub_produk`, `jml_rfs`, `created_at`, `updated_at`) VALUES
(1, 7, 8, '2019-06-20 05:46:28', '2019-06-29 04:21:20');

-- --------------------------------------------------------

--
-- Table structure for table `stock_stiker`
--

CREATE TABLE `stock_stiker` (
  `id_stock_stiker` int(20) NOT NULL,
  `stock_luar` int(15) NOT NULL,
  `stock_dalam` int(15) NOT NULL,
  `sub_produk_id` int(11) NOT NULL,
  `produk_id_produk` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stock_stiker`
--

INSERT INTO `stock_stiker` (`id_stock_stiker`, `stock_luar`, `stock_dalam`, `sub_produk_id`, `produk_id_produk`) VALUES
(2, 195, 195, 7, 11),
(3, 492, 492, 47, 54),
(4, 500, 500, 48, 54),
(5, 500, 500, 49, 54);

-- --------------------------------------------------------

--
-- Table structure for table `stock_wrapping`
--

CREATE TABLE `stock_wrapping` (
  `id` int(11) NOT NULL,
  `jml_wrap` int(20) NOT NULL,
  `stock_weiswrap` decimal(20,2) NOT NULL,
  `jenis_has_produk_id_jnpro` int(11) NOT NULL,
  `jenis_has_produk_id_jenis` int(11) NOT NULL,
  `jenis_has_produk_id_produk` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stock_wrapping`
--

INSERT INTO `stock_wrapping` (`id`, `jml_wrap`, `stock_weiswrap`, `jenis_has_produk_id_jnpro`, `jenis_has_produk_id_jenis`, `jenis_has_produk_id_produk`, `created_at`, `updated_at`) VALUES
(1, 5, '0.00', 11, 8, 11, '2019-06-08 12:58:30', '2019-06-28 07:46:25'),
(2, 5, '0.00', 13, 8, 13, '2019-06-13 12:49:51', '2019-06-13 12:50:32'),
(3, 10, '0.00', 14, 8, 14, '2019-06-13 15:41:32', '2019-06-13 15:42:04'),
(5, 400, '0.00', 17, 8, 17, '2019-06-17 10:49:55', '2019-06-17 10:49:55'),
(6, 0, '0.00', 12, 8, 12, '2019-06-17 11:55:25', '2019-06-18 09:22:22'),
(7, 500, '0.00', 15, 8, 15, '2019-06-17 11:56:19', '2019-06-18 09:22:51'),
(8, 500, '0.00', 16, 8, 16, '2019-06-17 11:56:48', '2019-06-18 09:23:21'),
(9, 500, '0.00', 19, 8, 19, '2019-06-17 11:57:30', '2019-06-18 09:23:53'),
(10, 50, '0.00', 18, 8, 18, '2019-06-18 04:43:25', '2019-06-18 06:06:16');

-- --------------------------------------------------------

--
-- Table structure for table `sub_produk`
--

CREATE TABLE `sub_produk` (
  `id_sub_produk` int(11) NOT NULL,
  `sub_kode` varchar(50) NOT NULL,
  `sub_produk` varchar(50) NOT NULL,
  `produk_id_produk` int(11) NOT NULL,
  `kemasan_id_kemasan` int(11) NOT NULL,
  `tarif` varchar(100) NOT NULL,
  `hje` varchar(100) NOT NULL,
  `isi` int(20) NOT NULL,
  `berat_jadi` decimal(20,2) NOT NULL,
  `image` tinytext DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sub_produk`
--

INSERT INTO `sub_produk` (`id_sub_produk`, `sub_kode`, `sub_produk`, `produk_id_produk`, `kemasan_id_kemasan`, `tarif`, `hje`, `isi`, `berat_jadi`, `image`) VALUES
(7, 'BR10', 'Robusto BR10', 11, 1, '0', '540000', 10, '4.00', NULL),
(8, 'BC10', 'Corona BC10', 12, 1, '0', '95000', 10, '8.00', NULL),
(12, 'HC1', 'Half Corona HC1', 13, 6, '110000', '110000', 1, '4.00', NULL),
(13, 'BC1', 'Corona BC1', 12, 4, '0', '0', 1, '4.00', NULL),
(14, 'BC3', 'Corona BC3', 12, 1, '0', '0', 3, '4.00', NULL),
(15, 'BC45', 'Corona BC45', 12, 1, '0', '0', 45, '4.00', NULL),
(16, 'HC10', 'Half Corona HC10', 13, 1, '0', '0', 10, '4.00', NULL),
(17, 'BR12', 'Robusto BR12', 11, 1, '0', '0', 12, '4.00', NULL),
(18, 'M5', 'Maumere M5', 14, 2, '0', '0', 5, '4.00', NULL),
(19, 'BR3', 'Robusto BR3', 11, 2, '0', '0', 3, '4.00', NULL),
(20, 'DA10', 'Don Agusto DA10', 21, 1, '0', '0', 10, '4.00', NULL),
(21, 'DA5', 'Don Agusto DA5', 21, 1, '0', '0', 5, '4.00', NULL),
(22, 'DA1', 'Don Agusto DA1', 21, 6, '0', '80000', 1, '9.00', NULL),
(23, 'JC15', 'Jember Cigar JC15', 18, 1, '0', '0', 15, '4.00', NULL),
(24, 'JC12', 'Jember Cigar M JC12', 18, 1, '0', '0', 12, '4.00', NULL),
(25, 'MD20', 'Mondlicht W MD20', 17, 1, '0', '0', 20, '4.00', NULL),
(26, 'MD20', 'Mondlicht MD20', 17, 2, '0', '0', 20, '4.00', NULL),
(27, 'M10', 'Maumere M10', 14, 1, '0', '0', 10, '4.00', NULL),
(28, 'BC5', 'Corona BC5', 12, 2, '0', '0', 5, '4.00', NULL),
(29, 'BC5', 'Corona BC5', 12, 1, '0', '0', 5, '4.00', NULL),
(30, 'HC5', 'Half Corona HC5', 13, 2, '0', '0', 5, '4.00', NULL),
(31, 'CM16', 'Cigar Master CM16', 15, 1, '0', '0', 16, '4.00', NULL),
(32, 'M1', 'Maumere M1', 14, 6, '0', '0', 1, '4.00', NULL),
(33, 'EN20', 'El Nino EN20', 16, 1, '0', '0', 20, '4.00', NULL),
(34, 'EN5', 'El Nino EN5', 16, 2, '0', '0', 5, '4.00', NULL),
(35, 'C16', 'C99 C16', 19, 1, '0', '0', 16, '4.00', NULL),
(36, 'C20', 'C99 C20', 19, 8, '0', '0', 20, '4.00', NULL),
(37, 'ST80', 'Sumatra ST80', 20, 1, '0', '0', 80, '4.00', NULL),
(39, 'ZK3', 'Zakera ZK3', 24, 1, '0', '0', 3, '4.00', NULL),
(40, 'DJ3', 'Don Juan DJ3', 23, 1, '0', '0', 3, '4.00', NULL),
(41, 'EL5', 'El Bomba EL5', 22, 1, '0', '0', 5, '4.00', NULL),
(42, 'JC12', 'Jember Cigar C JC12', 18, 1, '0', '0', 12, '4.00', NULL),
(43, 'MB10', 'Merubetiri MB10', 25, 7, '0', '0', 10, '4.00', NULL),
(44, '2124', '212', 50, 2, '0', '0', 4, '4.00', NULL),
(45, 'NFCG4', 'NFC Green', 26, 2, '0', '0', 4, '4.00', NULL),
(46, 'NFCR4', 'NFC Red', 27, 2, '0', '0', 4, '4.00', NULL),
(47, 'EKLUS', 'Ekslusif Colection', 54, 1, '50000', '500000', 20, '4.00', NULL),
(48, 'BOS', 'Bos Colection', 54, 1, '10000', '500000', 25, '4.00', NULL),
(49, 'HAVANO', 'Havano Colection', 54, 1, '90000', '500000', 16, '4.00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `username` varchar(45) DEFAULT NULL,
  `email` varchar(200) NOT NULL,
  `password` varchar(225) NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `no_hp` varchar(15) DEFAULT NULL,
  `alamat_user` tinytext DEFAULT NULL,
  `penanda_email` int(2) DEFAULT NULL,
  `upload_foto` varchar(100) DEFAULT NULL,
  `status_users` varchar(45) NOT NULL,
  `level_user_id_level` int(11) NOT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `username`, `email`, `password`, `email_verified_at`, `no_hp`, `alamat_user`, `penanda_email`, `upload_foto`, `status_users`, `level_user_id_level`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Yofandi Riki Winata', 'yofandi', 'yofandirikiwinata24@gmail.com', '$2y$10$63V8RiIuZ9PawxuPN91uy.SJaH5irmPvTU4JujP2N8obJuIMRE9Ea', NULL, '085287299141', 'Tunjungrejo, Yosowilangun, Lumajang', NULL, 'bincigar20190320111603.jpg', 'ACTIVE', 1, 'xiCKoE06Uemhfqn6iLQpWpU1FvrGvDIsa577GgSZvkCyp15IFtHI4sMOFgfe', '2019-03-01 03:34:40', '2019-03-20 04:59:36'),
(3, 'admin', 'admin', 'admin@gmail.com', '$2y$10$Mdrt89MA9guZm5Ll8kcFqeeCOajbllU4uyE4tz1NqaMWLg.4o8Bge', NULL, '12345678', NULL, NULL, 'bincigar20190521104210.jpg', 'ACTIVE', 1, 'difTJ1HAY1Oh7uuIIGwv8MfZIEFbOgusKTmhUHRCiY6VK6KSlicMyMUshKfn', '2019-03-29 04:31:46', '2019-05-21 03:42:10'),
(12, 'Sri Wahyu Ningsih', 'yuni', 'yuni.ningsih@bincigar.com', '$2y$10$A5eim1LQG5k1hBhOGQWBle7HyHnjROaaW97RiPaYyMF0n0ZZ0HZti', NULL, NULL, 'Kasub. Administrasi', NULL, NULL, 'ACTIVE', 1, '9HLYlnFQQSgb7irHl9K7wd5gCiayzqpN83bxyzN1fsCcXuZPaWp07qUOmnrR', NULL, NULL),
(13, 'Ir. H. Febrian Ananta Kahar. MIAM', 'febrian', 'febrian.kahar@bincigar.com', '$2y$10$BBT6CEfSQkW5Vhup5UJCVeooB38SlTCwSdtRyvtHyNTntxrn5Hene', NULL, NULL, 'Komisaris', NULL, NULL, 'ACTIVE', 1, NULL, NULL, NULL),
(14, 'H. Agusta Jaka Purwana. SE', 'agusta', 'agusta.jaka@bincigar.com', '$2y$10$CRW0wVGkOdwX43xqMrbW7.CNJJe3/.AacXn4MQgwre5Xc/d2BdQyC', NULL, NULL, 'Komisaris', NULL, NULL, 'ACTIVE', 1, NULL, NULL, NULL),
(15, 'Ir. Imam Wahid Wahyudi', 'imam ww', 'imam.ww@bincigar.com', '$2y$10$PO.W0R1YqB/l71muleuWwuRw08LkQHrUcP6EtouirDVl7.XpnRXci', NULL, NULL, 'Direktur Operasional', NULL, NULL, 'ACTIVE', 1, NULL, NULL, NULL),
(16, 'Wahyu Agustin', 'yayuk', 'yayuk@bincigar.com', '$2y$10$YyHgLfaBy5WH.JLm/JbkR.OHp1wlMwum5L4DBLDA59.yYDWnuaFcC', NULL, NULL, 'Asisten Direksi', NULL, NULL, 'ACTIVE', 1, NULL, NULL, NULL),
(17, 'Wamilihanto Undik Jayadi', 'undik', 'undik.jayadi@bincigar.com', '$2y$10$FWLfIkvJY4c0UE55Z45xcOH7XnS3eaW.aSJ2znC7GgOsJEb2AP5IS', NULL, NULL, 'Kabag. Kantor', NULL, NULL, 'ACTIVE', 1, NULL, NULL, NULL),
(18, 'Inneke Agatha Putrie', 'inneke', 'inneke.agatha@bincigar.com', '$2y$10$9TKG0/ilb4GoP2HIwVQ/u.LwuOcBXEZA3SxljRw82tq7NLG375lQq', NULL, NULL, 'Kabag. Kesekretariatan', NULL, NULL, 'ACTIVE', 1, 'CoDHYDUwYmLyojZ63a7jCSa2XX0gWLw5jxCEaGN0iDzTTORlQVjlBGI3CI8o', NULL, '2019-05-25 08:02:30'),
(19, 'Nela Revoni', 'nela', 'nela.revoni@bincigar.com', '$2y$10$XsyeYPrQVMzBDqRVRNPkj.F.59tHFLNa0x2oswSOampZ.H2cWuIpW', NULL, NULL, 'Kabag. Keuangan', NULL, NULL, 'ACTIVE', 1, NULL, NULL, NULL),
(20, 'Sri Wahyuni', 'yuyun', 'sri.wahyuni@bincigar.com', '$2y$10$v2itHHkmTsj9tvM/IFRFB.5xUZE1cctXjseBYPS.0Szh5HrTgHZIO', NULL, NULL, 'Kabag. Store', NULL, NULL, 'ACTIVE', 7, NULL, NULL, NULL),
(21, 'Citra Wahyu Lestari', 'citra', 'citra.wahyu@bincigar.com', '$2y$10$QdwNWUUjCAdhCDBC8NTF9O5w/qCu/H71ju3dhAnlBVcih2jGoXP3S', NULL, NULL, 'Kabag. Pengiriman', NULL, NULL, 'ACTIVE', 6, '0QHDoGcBKSKy091B7kqWllq2ByHOxZbC4CyVx23xWKZrmXIPFFjDVmGcKn19', NULL, NULL),
(22, 'Risma Meiditasari', 'risma', 'risma.meiditasari@bincigar.com', '$2y$10$mAS0WW8JvOD7bKqMLA68BOZJkRGcwOdQPmtE95xhnyGBbzSJpPNte', NULL, NULL, 'Kabag. Pengendali Mutu', NULL, NULL, 'ACTIVE', 3, 'GPkpinTgaLN08x4JsCX4yCgvYlhu0Xy2mJf55jg1CJuO9AEj0wmSDBp98ZlV', NULL, NULL),
(23, 'Iman Santoso', 'iman', 'iman.santoso@bincigar.com', '$2y$10$Xmejxl/jiQ0nhYC5kqBPEelYywDT1IKl/lc6OMmiLPq0Y8.1Y8vGa', NULL, NULL, 'Kabag. Pemasaran PHRI', NULL, NULL, 'ACTIVE', 9, NULL, NULL, NULL),
(24, 'Doni Nugroho', 'doni', 'doni.nugroho@bincigar.com', '$2y$10$zmC7rQZ9qj7yBRKvgj/fwufliathnB.E8zcRgHWUOOoYWCV7LF2xO', NULL, NULL, 'Kabag. Pemasaran Non PHRI', NULL, NULL, 'ACTIVE', 10, NULL, NULL, NULL),
(25, 'M. Faturrozy', 'rosi', 'faturrozy@bincigar.com', '$2y$10$eN.yexzwTonjIZzjWtekuOP5DgQxAMwo1Ma.aRTgymd7r1Afpukq.', NULL, NULL, 'Kabag. Produksi Tangan', NULL, NULL, 'ACTIVE', 4, NULL, NULL, NULL),
(26, 'nambi', 'nambi', 'nambi@gmail.com', '$2y$10$tFDqT4GTi6gitQ.0r14BkeMh2R.HnZCdZcOdFijTv8uIpPwQq9xjq', NULL, '098713920837', 'coba bung', NULL, NULL, 'ACTIVE', 5, NULL, NULL, NULL),
(27, 'skansj', 'owjenc', '9239@cnjncdj', '$2y$10$BUiFPKNQy3vascmuCA7v5uSqrN8xrNnKiW0Z9auQd45ymwf7DZ.7m', NULL, '09402832', 'xs,skl', NULL, NULL, 'ACTIVE', 1, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `value_reject`
--

CREATE TABLE `value_reject` (
  `id` int(11) NOT NULL,
  `noc_items_id` int(11) DEFAULT NULL,
  `jml_packing` int(15) NOT NULL,
  `jml_uraian` int(15) NOT NULL,
  `convert_batang` int(15) NOT NULL,
  `jml_batangqc` int(15) NOT NULL,
  `jml_cincin` int(15) NOT NULL,
  `jml_stikerluar` int(15) NOT NULL,
  `jml_stikerdalam` int(15) NOT NULL,
  `jml_cukai` int(15) NOT NULL,
  `jml_kemasan` int(15) NOT NULL,
  `jml_fill1` int(15) NOT NULL,
  `jml_bind` int(15) NOT NULL,
  `jml_wrap` int(15) NOT NULL,
  `jml_fill2` int(15) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `value_reject`
--

INSERT INTO `value_reject` (`id`, `noc_items_id`, `jml_packing`, `jml_uraian`, `convert_batang`, `jml_batangqc`, `jml_cincin`, `jml_stikerluar`, `jml_stikerdalam`, `jml_cukai`, `jml_kemasan`, `jml_fill1`, `jml_bind`, `jml_wrap`, `jml_fill2`, `created_at`, `updated_at`) VALUES
(2, 3, 1, 1, 10, 5, 10, 1, 1, 1, 1, 50, 100, 100, 50, '2019-06-25 20:58:32', '2019-06-25 21:54:12'),
(3, 11, 0, 1, 10, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, '2019-06-29 14:39:25', '2019-06-29 14:39:25');

-- --------------------------------------------------------

--
-- Table structure for table `wrapping`
--

CREATE TABLE `wrapping` (
  `id` int(11) NOT NULL,
  `weiswmasuk` decimal(20,2) NOT NULL,
  `terpakaiprow` decimal(20,2) NOT NULL,
  `terpakaiweisw` decimal(20,2) NOT NULL,
  `sisaprow` decimal(20,2) NOT NULL,
  `sisaweisw` decimal(20,2) NOT NULL,
  `tambah_wrap` int(20) NOT NULL,
  `batangreject` int(20) NOT NULL,
  `reject_filling` int(20) DEFAULT NULL,
  `reject_binding` int(20) DEFAULT NULL,
  `hasil_akhirw` int(20) NOT NULL,
  `keterangan_wrap` tinytext DEFAULT NULL,
  `tanggal_wrap` datetime NOT NULL,
  `stock_probaku_id` int(11) NOT NULL,
  `stock_wrapping_id` int(11) NOT NULL,
  `stock_pressing_id` int(11) NOT NULL,
  `author_session` varchar(200) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT current_timestamp(),
  `log` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wrapping`
--

INSERT INTO `wrapping` (`id`, `weiswmasuk`, `terpakaiprow`, `terpakaiweisw`, `sisaprow`, `sisaweisw`, `tambah_wrap`, `batangreject`, `reject_filling`, `reject_binding`, `hasil_akhirw`, `keterangan_wrap`, `tanggal_wrap`, `stock_probaku_id`, `stock_wrapping_id`, `stock_pressing_id`, `author_session`, `created_at`, `updated_at`, `log`) VALUES
(13, '0.00', '500.00', '0.00', '90000.00', '0.00', 50, 0, 0, 0, 320, 'skanj', '2019-06-18 13:04:22', 3, 10, 9, '1', '2019-06-18 06:04:49', '2019-06-18 06:06:16', 'INSERT'),
(14, '0.00', '1000.00', '0.00', '89000.00', '0.00', 500, 0, 0, 0, 0, 'njsak', '2019-06-18 16:19:38', 3, 6, 4, '1', '2019-06-18 09:20:08', '2019-06-18 09:20:08', 'INSERT'),
(15, '0.00', '1000.00', '0.00', '88000.00', '0.00', 500, 0, 0, 0, 0, 'skaj', '2019-06-18 16:20:13', 3, 7, 5, '1', '2019-06-18 09:20:37', '2019-06-18 09:20:37', 'INSERT'),
(16, '0.00', '1000.00', '0.00', '87000.00', '0.00', 500, 0, 0, 0, 0, 'sabj', '2019-06-18 16:20:41', 3, 8, 6, '1', '2019-06-18 09:21:16', '2019-06-18 09:21:16', 'INSERT'),
(17, '0.00', '1000.00', '0.00', '86000.00', '0.00', 500, 0, 0, 0, 0, 'msj', '2019-06-18 16:21:22', 3, 9, 7, '1', '2019-06-18 09:21:47', '2019-06-18 09:21:47', 'INSERT'),
(18, '0.00', '1000.00', '200.00', '85000.00', '0.00', 1000, 10, 80, 40, 0, NULL, '2019-06-27 19:01:16', 3, 1, 1, '1', '2019-06-27 12:03:34', '2019-06-27 12:03:34', 'INSERT');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `binding`
--
ALTER TABLE `binding`
  ADD PRIMARY KEY (`id`,`stock_binding_id`,`stock_filling_id`,`stock_probaku_id`),
  ADD KEY `fk_binding_stock_binding1_idx` (`stock_binding_id`),
  ADD KEY `fk_binding_stock_filling1_idx` (`stock_filling_id`),
  ADD KEY `fk_binding_stock_probaku1_idx` (`stock_probaku_id`);

--
-- Indexes for table `cerutu_terjual`
--
ALTER TABLE `cerutu_terjual`
  ADD PRIMARY KEY (`id`,`penjualan_id`,`penjualan_customer_id_customer`),
  ADD KEY `fk_cerutu_terjual_penjualan1_idx` (`penjualan_id`,`penjualan_customer_id_customer`),
  ADD KEY `fk_cerutu_terjual_stock_bagan1_idx` (`stock_bagan_id`);

--
-- Indexes for table `cicil`
--
ALTER TABLE `cicil`
  ADD PRIMARY KEY (`id`,`penjualan_id`),
  ADD KEY `fk_cicil_penjualan1_idx` (`penjualan_id`);

--
-- Indexes for table `cool`
--
ALTER TABLE `cool`
  ADD PRIMARY KEY (`id`,`stock_cool_id`,`stock_fumigasi_id`),
  ADD KEY `fk_cool_stock_cool1_idx` (`stock_cool_id`),
  ADD KEY `fk_cool_stock_fumigasi1_idx` (`stock_fumigasi_id`);

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`id_customer`,`users_id_users`),
  ADD KEY `fk_customer_users1_idx` (`users_id_users`);

--
-- Indexes for table `drying1`
--
ALTER TABLE `drying1`
  ADD PRIMARY KEY (`id`,`stock_wrapping_id`,`stock_drying1_id`),
  ADD KEY `fk_drying1_stock_drying11_idx` (`stock_drying1_id`),
  ADD KEY `fk_drying1_stock_wrapping1_idx` (`stock_wrapping_id`);

--
-- Indexes for table `drying2`
--
ALTER TABLE `drying2`
  ADD PRIMARY KEY (`id`,`stock_frezer_id`,`stock_drying2_id`),
  ADD KEY `fk_drying2_stock_drying21_idx` (`stock_drying2_id`),
  ADD KEY `fk_drying2_stock_frezer1_idx` (`stock_frezer_id`);

--
-- Indexes for table `drying3`
--
ALTER TABLE `drying3`
  ADD PRIMARY KEY (`id`,`stock_drying3_id`,`stock_cool_id`),
  ADD KEY `fk_drying3_stock_drying31_idx` (`stock_drying3_id`),
  ADD KEY `fk_drying3_stock_cool1_idx` (`stock_cool_id`);

--
-- Indexes for table `filling`
--
ALTER TABLE `filling`
  ADD PRIMARY KEY (`id`,`stock_probaku_id`,`stock_filling_id`),
  ADD KEY `fk_filling_stock_filling1_idx` (`stock_filling_id`),
  ADD KEY `fk_filling_stock_probaku1_idx` (`stock_probaku_id`);

--
-- Indexes for table `frezer`
--
ALTER TABLE `frezer`
  ADD PRIMARY KEY (`id`,`stock_frezer_id`,`stock_drying1_id`),
  ADD KEY `fk_frezer_stock_frezer1_idx` (`stock_frezer_id`),
  ADD KEY `fk_frezer_stock_drying11_idx` (`stock_drying1_id`);

--
-- Indexes for table `fumigasi`
--
ALTER TABLE `fumigasi`
  ADD PRIMARY KEY (`id`,`stock_fumigasi_id`,`stock_drying2_id`),
  ADD KEY `fk_fumigasi_stock_fumigasi1_idx` (`stock_fumigasi_id`),
  ADD KEY `fk_fumigasi_stock_drying21_idx` (`stock_drying2_id`);

--
-- Indexes for table `harga_cincin`
--
ALTER TABLE `harga_cincin`
  ADD PRIMARY KEY (`id`,`produk_id_produk`),
  ADD KEY `fk_harga_cincin_produk1_idx` (`produk_id_produk`);

--
-- Indexes for table `harga_cukai`
--
ALTER TABLE `harga_cukai`
  ADD PRIMARY KEY (`id`,`sub_produk_id`),
  ADD KEY `fk_harga_cukai_sub_produk1_idx` (`sub_produk_id`);

--
-- Indexes for table `harga_kemasan`
--
ALTER TABLE `harga_kemasan`
  ADD PRIMARY KEY (`id`,`sub_produk_id`),
  ADD KEY `fk_harga_kemasan_sub_produk1_idx` (`sub_produk_id`);

--
-- Indexes for table `harga_stiker`
--
ALTER TABLE `harga_stiker`
  ADD PRIMARY KEY (`id`,`sub_produk_id`),
  ADD KEY `fk_harga_stiker_sub_produk1_idx` (`sub_produk_id`);

--
-- Indexes for table `history_bahanmasuk`
--
ALTER TABLE `history_bahanmasuk`
  ADD PRIMARY KEY (`id`,`jenis_id_jenis`,`kategori_id_kategori`,`stock_probaku_id`,`satuan_harga_id`,`author_session`),
  ADD KEY `fk_history_bahanmasuk_jenis1_idx` (`jenis_id_jenis`),
  ADD KEY `fk_history_bahanmasuk_kategori1_idx` (`kategori_id_kategori`),
  ADD KEY `fk_history_bahanmasuk_stock_probaku1_idx` (`stock_probaku_id`),
  ADD KEY `fk_history_bahanmasuk_satuan_harga1_idx` (`satuan_harga_id`);

--
-- Indexes for table `history_cincin`
--
ALTER TABLE `history_cincin`
  ADD PRIMARY KEY (`id`,`stock_cincin_id_stock_cincin`,`stock_cincin_produk_id_produk`,`harga_cincin_id`,`author_session`),
  ADD KEY `fk_history_cincin_stock_cincin1_idx` (`stock_cincin_id_stock_cincin`,`stock_cincin_produk_id_produk`),
  ADD KEY `fk_history_cincin_packing1_idx` (`packing_id`),
  ADD KEY `fk_history_cincin_harga_cincin1_idx` (`harga_cincin_id`);

--
-- Indexes for table `history_cukai`
--
ALTER TABLE `history_cukai`
  ADD PRIMARY KEY (`id`,`stock_cukai_id_stock_cukai`,`stock_cukai_id_sub_produk`,`stock_cukai_produk_id_produk`,`harga_cukai_id`),
  ADD KEY `fk_history_cukai_stock_cukai1_idx` (`stock_cukai_id_stock_cukai`,`stock_cukai_id_sub_produk`,`stock_cukai_produk_id_produk`),
  ADD KEY `fk_history_cukai_packing1_idx` (`packing_id_packing`),
  ADD KEY `fk_history_cukai_harga_cukai1_idx` (`harga_cukai_id`);

--
-- Indexes for table `history_kemasan`
--
ALTER TABLE `history_kemasan`
  ADD PRIMARY KEY (`id`,`stock_kemasan_id_stock_kemasan`,`stock_kemasan_sub_produk_id`,`stock_kemasan_produk_id_produk`,`stock_kemasan_kemasan_id_kemasan`,`harga_kemasan_id`),
  ADD KEY `fk_history_kemasan_packing1_idx` (`packing_id`),
  ADD KEY `fk_history_kemasan_stock_kemasan1_idx` (`stock_kemasan_id_stock_kemasan`,`stock_kemasan_sub_produk_id`,`stock_kemasan_produk_id_produk`,`stock_kemasan_kemasan_id_kemasan`),
  ADD KEY `fk_history_kemasan_harga_kemasan1_idx` (`harga_kemasan_id`);

--
-- Indexes for table `history_stiker`
--
ALTER TABLE `history_stiker`
  ADD PRIMARY KEY (`id`,`stock_stiker_id_stock_stiker`,`stock_stiker_sub_produk_id`,`stock_stiker_produk_id_produk`,`harga_stiker_id`),
  ADD KEY `fk_history_stiker_packing1_idx` (`packing_id`),
  ADD KEY `fk_history_stiker_stock_stiker1_idx` (`stock_stiker_id_stock_stiker`,`stock_stiker_sub_produk_id`,`stock_stiker_produk_id_produk`),
  ADD KEY `fk_history_stiker_harga_stiker1_idx` (`harga_stiker_id`);

--
-- Indexes for table `items_object`
--
ALTER TABLE `items_object`
  ADD PRIMARY KEY (`id`,`sub_produk_id`),
  ADD KEY `fk_rfs_items_sub_produk1_idx` (`sub_produk_id`),
  ADD KEY `fk_items_object_order1_idx` (`order_id`);

--
-- Indexes for table `jenis`
--
ALTER TABLE `jenis`
  ADD PRIMARY KEY (`id_jenis`);

--
-- Indexes for table `jenis_has_produk`
--
ALTER TABLE `jenis_has_produk`
  ADD PRIMARY KEY (`id_jnpro`,`jenis_id_jenis`,`produk_id_produk`),
  ADD KEY `fk_jenis_has_produk_produk1_idx` (`produk_id_produk`),
  ADD KEY `fk_jenis_has_produk_jenis1_idx` (`jenis_id_jenis`);

--
-- Indexes for table `kategori`
--
ALTER TABLE `kategori`
  ADD PRIMARY KEY (`id_kategori`);

--
-- Indexes for table `kemasan`
--
ALTER TABLE `kemasan`
  ADD PRIMARY KEY (`id_kemasan`);

--
-- Indexes for table `laporan`
--
ALTER TABLE `laporan`
  ADD PRIMARY KEY (`id_laporan`);

--
-- Indexes for table `level_user`
--
ALTER TABLE `level_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `noc_inrfs`
--
ALTER TABLE `noc_inrfs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `noc_items`
--
ALTER TABLE `noc_items`
  ADD PRIMARY KEY (`id`,`sub_produk_id`,`noc_inrfs_id`),
  ADD KEY `fk_noc_items_noc_inrfs1_idx` (`noc_inrfs_id`),
  ADD KEY `fk_noc_items_sub_produk1_idx` (`sub_produk_id`);

--
-- Indexes for table `order`
--
ALTER TABLE `order`
  ADD PRIMARY KEY (`id`,`users_id`),
  ADD KEY `fk_order_users1_idx` (`users_id`),
  ADD KEY `fk_order_rfs_his1_idx` (`rfs_id`);

--
-- Indexes for table `packing`
--
ALTER TABLE `packing`
  ADD PRIMARY KEY (`id`,`stock_packing_id`),
  ADD KEY `fk_packing_stock_qc1_idx` (`stock_qc_id`),
  ADD KEY `fk_packing_stock_packing1_idx` (`stock_packing_id`);

--
-- Indexes for table `penerimaan`
--
ALTER TABLE `penerimaan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_penerimaan_penjualan1_idx` (`penjualan_id`);

--
-- Indexes for table `penjualan`
--
ALTER TABLE `penjualan`
  ADD PRIMARY KEY (`id`,`customer_id_customer`),
  ADD KEY `fk_penjualan_customer1_idx` (`customer_id_customer`);

--
-- Indexes for table `pressing`
--
ALTER TABLE `pressing`
  ADD PRIMARY KEY (`id`,`stock_pressing_id`,`stock_binding_id`),
  ADD KEY `fk_pressing_stock_pressing1_idx` (`stock_pressing_id`),
  ADD KEY `fk_pressing_stock_binding1_idx` (`stock_binding_id`);

--
-- Indexes for table `produk`
--
ALTER TABLE `produk`
  ADD PRIMARY KEY (`id_produk`);

--
-- Indexes for table `quality_control`
--
ALTER TABLE `quality_control`
  ADD PRIMARY KEY (`id`,`stock_qc_id`,`stock_drying3_id`),
  ADD KEY `fk_quality_control_stock_qc1_idx` (`stock_qc_id`),
  ADD KEY `fk_quality_control_stock_drying31_idx` (`stock_drying3_id`);

--
-- Indexes for table `return`
--
ALTER TABLE `return`
  ADD PRIMARY KEY (`id_return`,`jenis_id_jenis`,`sub_produk_id_sub_produk`),
  ADD KEY `fk_return_sub_produk1_idx` (`sub_produk_id_sub_produk`),
  ADD KEY `fk_return_stock_rfs1_idx` (`stock_rfs_id`),
  ADD KEY `fk_return_jenis1_idx` (`jenis_id_jenis`),
  ADD KEY `fk_return_stock_packing1_idx` (`stock_packing_id`),
  ADD KEY `fk_return_stock_binding1_idx` (`stock_binding_id`),
  ADD KEY `fk_return_stock_qc1_idx` (`stock_qc_id`),
  ADD KEY `fk_return_stock_wrapping1_idx` (`stock_wrapping_id`),
  ADD KEY `fk_return_stock_bagan1_idx` (`stock_bagan_id`);

--
-- Indexes for table `rfs_his`
--
ALTER TABLE `rfs_his`
  ADD PRIMARY KEY (`id`,`users_id`),
  ADD KEY `fk_rfs_users1_idx` (`users_id`);

--
-- Indexes for table `satuan_harga`
--
ALTER TABLE `satuan_harga`
  ADD PRIMARY KEY (`id`,`jenis_id_jenis`,`kategori_id_kategori`),
  ADD KEY `fk_satuan_harga_jenis1_idx` (`jenis_id_jenis`),
  ADD KEY `fk_satuan_harga_kategori1_idx` (`kategori_id_kategori`);

--
-- Indexes for table `setting`
--
ALTER TABLE `setting`
  ADD PRIMARY KEY (`idsetting`);

--
-- Indexes for table `stock_bagan`
--
ALTER TABLE `stock_bagan`
  ADD PRIMARY KEY (`id`,`sub_produk_id`),
  ADD KEY `fk_stock_bagan_sub_produk1_idx` (`sub_produk_id`),
  ADD KEY `fk_stock_bagan_users1_idx` (`users_id`);

--
-- Indexes for table `stock_binding`
--
ALTER TABLE `stock_binding`
  ADD PRIMARY KEY (`id`,`jenis_has_produk_id_jnpro`,`jenis_has_produk_id_jenis`,`jenis_has_produk_id_produk`),
  ADD KEY `fk_stock_binding_jenis_has_produk1_idx` (`jenis_has_produk_id_jnpro`,`jenis_has_produk_id_jenis`,`jenis_has_produk_id_produk`);

--
-- Indexes for table `stock_cincin`
--
ALTER TABLE `stock_cincin`
  ADD PRIMARY KEY (`id_stock_cincin`,`produk_id_produk`),
  ADD KEY `fk_stock_cincin_produk1_idx` (`produk_id_produk`);

--
-- Indexes for table `stock_cool`
--
ALTER TABLE `stock_cool`
  ADD PRIMARY KEY (`id`,`jenis_has_produk_id_jnpro`,`jenis_has_produk_id_jenis`,`jenis_has_produk_id_produk`),
  ADD KEY `fk_stock_cool_jenis_has_produk1_idx` (`jenis_has_produk_id_jnpro`,`jenis_has_produk_id_jenis`,`jenis_has_produk_id_produk`);

--
-- Indexes for table `stock_cukai`
--
ALTER TABLE `stock_cukai`
  ADD PRIMARY KEY (`id_stock_cukai`,`sub_produk_id_sub_produk`,`sub_produk_produk_id_produk`),
  ADD KEY `fk_stock_cukai_sub_produk1_idx` (`sub_produk_id_sub_produk`,`sub_produk_produk_id_produk`);

--
-- Indexes for table `stock_drying1`
--
ALTER TABLE `stock_drying1`
  ADD PRIMARY KEY (`id`,`jenis_has_produk_id_jnpro`,`jenis_has_produk_id_jenis`,`jenis_has_produk_id_produk`),
  ADD KEY `fk_stock_drying1_jenis_has_produk1_idx` (`jenis_has_produk_id_jnpro`,`jenis_has_produk_id_jenis`,`jenis_has_produk_id_produk`);

--
-- Indexes for table `stock_drying2`
--
ALTER TABLE `stock_drying2`
  ADD PRIMARY KEY (`id`,`jenis_has_produk_id_jnpro`,`jenis_has_produk_id_jenis`,`jenis_has_produk_id_produk`),
  ADD KEY `fk_stock_drying2_jenis_has_produk1_idx` (`jenis_has_produk_id_jnpro`,`jenis_has_produk_id_jenis`,`jenis_has_produk_id_produk`);

--
-- Indexes for table `stock_drying3`
--
ALTER TABLE `stock_drying3`
  ADD PRIMARY KEY (`id`,`jenis_has_produk_id_jnpro`,`jenis_has_produk_id_jenis`,`jenis_has_produk_id_produk`),
  ADD KEY `fk_stock_drying3_jenis_has_produk1_idx` (`jenis_has_produk_id_jnpro`,`jenis_has_produk_id_jenis`,`jenis_has_produk_id_produk`);

--
-- Indexes for table `stock_filling`
--
ALTER TABLE `stock_filling`
  ADD PRIMARY KEY (`id`,`jenis_has_produk_id_jnpro`,`jenis_has_produk_id_jenis`,`jenis_has_produk_id_produk`),
  ADD KEY `fk_stock_filling_jenis_has_produk1_idx` (`jenis_has_produk_id_jnpro`,`jenis_has_produk_id_jenis`,`jenis_has_produk_id_produk`);

--
-- Indexes for table `stock_frezer`
--
ALTER TABLE `stock_frezer`
  ADD PRIMARY KEY (`id`,`jenis_has_produk_id_jnpro`,`jenis_has_produk_id_jenis`,`jenis_has_produk_id_produk`),
  ADD KEY `fk_stock_frezer_jenis_has_produk1_idx` (`jenis_has_produk_id_jnpro`,`jenis_has_produk_id_jenis`,`jenis_has_produk_id_produk`);

--
-- Indexes for table `stock_fumigasi`
--
ALTER TABLE `stock_fumigasi`
  ADD PRIMARY KEY (`id`,`jenis_has_produk_id_jnpro`,`jenis_has_produk_id_jenis`,`jenis_has_produk_id_produk`),
  ADD KEY `fk_stock_fumigasi_jenis_has_produk1_idx` (`jenis_has_produk_id_jnpro`,`jenis_has_produk_id_jenis`,`jenis_has_produk_id_produk`);

--
-- Indexes for table `stock_kemasan`
--
ALTER TABLE `stock_kemasan`
  ADD PRIMARY KEY (`id_stock_kemasan`,`sub_produk_id`,`produk_id_produk`,`kemasan_id_kemasan`),
  ADD KEY `fk_stock_kemasan_sub_produk1_idx` (`sub_produk_id`,`produk_id_produk`,`kemasan_id_kemasan`);

--
-- Indexes for table `stock_packing`
--
ALTER TABLE `stock_packing`
  ADD PRIMARY KEY (`id`,`sub_produk_id_sub_produk`),
  ADD KEY `fk_stock_packing_sub_produk1_idx` (`sub_produk_id_sub_produk`);

--
-- Indexes for table `stock_pressing`
--
ALTER TABLE `stock_pressing`
  ADD PRIMARY KEY (`id`,`jenis_has_produk_id_jnpro`,`jenis_has_produk_id_jenis`,`jenis_has_produk_id_produk`),
  ADD KEY `fk_stock_pressing_jenis_has_produk1_idx` (`jenis_has_produk_id_jnpro`,`jenis_has_produk_id_jenis`,`jenis_has_produk_id_produk`);

--
-- Indexes for table `stock_probaku`
--
ALTER TABLE `stock_probaku`
  ADD PRIMARY KEY (`id`,`jenis_id_jenis`,`kategori_id_kategori`),
  ADD KEY `fk_stock_probaku_jenis1_idx` (`jenis_id_jenis`),
  ADD KEY `fk_stock_probaku_kategori1_idx` (`kategori_id_kategori`);

--
-- Indexes for table `stock_qc`
--
ALTER TABLE `stock_qc`
  ADD PRIMARY KEY (`id`,`jenis_has_produk_id_jnpro`,`jenis_has_produk_id_jenis`,`jenis_has_produk_id_produk`),
  ADD KEY `fk_stock_cool_jenis_has_produk2_idx` (`jenis_has_produk_id_jnpro`,`jenis_has_produk_id_jenis`,`jenis_has_produk_id_produk`);

--
-- Indexes for table `stock_rfs`
--
ALTER TABLE `stock_rfs`
  ADD PRIMARY KEY (`id`,`sub_produk_id_sub_produk`),
  ADD KEY `fk_stock_rfs_sub_produk1_idx` (`sub_produk_id_sub_produk`);

--
-- Indexes for table `stock_stiker`
--
ALTER TABLE `stock_stiker`
  ADD PRIMARY KEY (`id_stock_stiker`,`sub_produk_id`,`produk_id_produk`),
  ADD KEY `fk_stock_stiker_sub_produk1_idx` (`sub_produk_id`,`produk_id_produk`);

--
-- Indexes for table `stock_wrapping`
--
ALTER TABLE `stock_wrapping`
  ADD PRIMARY KEY (`id`,`jenis_has_produk_id_jnpro`,`jenis_has_produk_id_jenis`,`jenis_has_produk_id_produk`),
  ADD KEY `fk_stock_wrapping_jenis_has_produk1_idx` (`jenis_has_produk_id_jnpro`,`jenis_has_produk_id_jenis`,`jenis_has_produk_id_produk`);

--
-- Indexes for table `sub_produk`
--
ALTER TABLE `sub_produk`
  ADD PRIMARY KEY (`id_sub_produk`,`produk_id_produk`,`kemasan_id_kemasan`),
  ADD KEY `fk_sub_produk_produk1_idx` (`produk_id_produk`),
  ADD KEY `fk_sub_produk_kemasan1_idx` (`kemasan_id_kemasan`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`,`level_user_id_level`),
  ADD UNIQUE KEY `email_UNIQUE` (`email`),
  ADD KEY `fk_users_level_user_idx` (`level_user_id_level`);

--
-- Indexes for table `value_reject`
--
ALTER TABLE `value_reject`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_value_reject_noc_items1_idx` (`noc_items_id`);

--
-- Indexes for table `wrapping`
--
ALTER TABLE `wrapping`
  ADD PRIMARY KEY (`id`,`stock_probaku_id`,`stock_wrapping_id`,`stock_pressing_id`),
  ADD KEY `fk_wrapping_stock_pressing1_idx` (`stock_pressing_id`),
  ADD KEY `fk_wrapping_stock_probaku1_idx` (`stock_probaku_id`),
  ADD KEY `fk_wrapping_stock_wrapping1_idx` (`stock_wrapping_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `binding`
--
ALTER TABLE `binding`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `cerutu_terjual`
--
ALTER TABLE `cerutu_terjual`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cicil`
--
ALTER TABLE `cicil`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cool`
--
ALTER TABLE `cool`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `drying1`
--
ALTER TABLE `drying1`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `drying2`
--
ALTER TABLE `drying2`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `drying3`
--
ALTER TABLE `drying3`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `filling`
--
ALTER TABLE `filling`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `frezer`
--
ALTER TABLE `frezer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `fumigasi`
--
ALTER TABLE `fumigasi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `harga_cincin`
--
ALTER TABLE `harga_cincin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `harga_cukai`
--
ALTER TABLE `harga_cukai`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `harga_kemasan`
--
ALTER TABLE `harga_kemasan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `harga_stiker`
--
ALTER TABLE `harga_stiker`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `history_bahanmasuk`
--
ALTER TABLE `history_bahanmasuk`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `history_cincin`
--
ALTER TABLE `history_cincin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `history_cukai`
--
ALTER TABLE `history_cukai`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `history_kemasan`
--
ALTER TABLE `history_kemasan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `history_stiker`
--
ALTER TABLE `history_stiker`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `items_object`
--
ALTER TABLE `items_object`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jenis`
--
ALTER TABLE `jenis`
  MODIFY `id_jenis` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `jenis_has_produk`
--
ALTER TABLE `jenis_has_produk`
  MODIFY `id_jnpro` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;

--
-- AUTO_INCREMENT for table `kategori`
--
ALTER TABLE `kategori`
  MODIFY `id_kategori` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `kemasan`
--
ALTER TABLE `kemasan`
  MODIFY `id_kemasan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `laporan`
--
ALTER TABLE `laporan`
  MODIFY `id_laporan` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `level_user`
--
ALTER TABLE `level_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `noc_inrfs`
--
ALTER TABLE `noc_inrfs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `noc_items`
--
ALTER TABLE `noc_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `order`
--
ALTER TABLE `order`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `packing`
--
ALTER TABLE `packing`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `penerimaan`
--
ALTER TABLE `penerimaan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `penjualan`
--
ALTER TABLE `penjualan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pressing`
--
ALTER TABLE `pressing`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `produk`
--
ALTER TABLE `produk`
  MODIFY `id_produk` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;

--
-- AUTO_INCREMENT for table `quality_control`
--
ALTER TABLE `quality_control`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `return`
--
ALTER TABLE `return`
  MODIFY `id_return` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rfs_his`
--
ALTER TABLE `rfs_his`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `satuan_harga`
--
ALTER TABLE `satuan_harga`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `setting`
--
ALTER TABLE `setting`
  MODIFY `idsetting` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `stock_bagan`
--
ALTER TABLE `stock_bagan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `stock_binding`
--
ALTER TABLE `stock_binding`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `stock_cincin`
--
ALTER TABLE `stock_cincin`
  MODIFY `id_stock_cincin` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `stock_cool`
--
ALTER TABLE `stock_cool`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `stock_cukai`
--
ALTER TABLE `stock_cukai`
  MODIFY `id_stock_cukai` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `stock_drying1`
--
ALTER TABLE `stock_drying1`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `stock_drying2`
--
ALTER TABLE `stock_drying2`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `stock_drying3`
--
ALTER TABLE `stock_drying3`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `stock_filling`
--
ALTER TABLE `stock_filling`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `stock_frezer`
--
ALTER TABLE `stock_frezer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `stock_fumigasi`
--
ALTER TABLE `stock_fumigasi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `stock_kemasan`
--
ALTER TABLE `stock_kemasan`
  MODIFY `id_stock_kemasan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `stock_packing`
--
ALTER TABLE `stock_packing`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `stock_pressing`
--
ALTER TABLE `stock_pressing`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `stock_probaku`
--
ALTER TABLE `stock_probaku`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `stock_qc`
--
ALTER TABLE `stock_qc`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `stock_rfs`
--
ALTER TABLE `stock_rfs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `stock_stiker`
--
ALTER TABLE `stock_stiker`
  MODIFY `id_stock_stiker` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `stock_wrapping`
--
ALTER TABLE `stock_wrapping`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `sub_produk`
--
ALTER TABLE `sub_produk`
  MODIFY `id_sub_produk` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `value_reject`
--
ALTER TABLE `value_reject`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `wrapping`
--
ALTER TABLE `wrapping`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `binding`
--
ALTER TABLE `binding`
  ADD CONSTRAINT `fk_binding_stock_binding1` FOREIGN KEY (`stock_binding_id`) REFERENCES `stock_binding` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_binding_stock_filling1` FOREIGN KEY (`stock_filling_id`) REFERENCES `stock_filling` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_binding_stock_probaku1` FOREIGN KEY (`stock_probaku_id`) REFERENCES `stock_probaku` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `cerutu_terjual`
--
ALTER TABLE `cerutu_terjual`
  ADD CONSTRAINT `fk_cerutu_terjual_penjualan1` FOREIGN KEY (`penjualan_id`,`penjualan_customer_id_customer`) REFERENCES `penjualan` (`id`, `customer_id_customer`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_cerutu_terjual_stock_bagan1` FOREIGN KEY (`stock_bagan_id`) REFERENCES `stock_bagan` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `cicil`
--
ALTER TABLE `cicil`
  ADD CONSTRAINT `fk_cicil_penjualan1` FOREIGN KEY (`penjualan_id`) REFERENCES `penjualan` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `cool`
--
ALTER TABLE `cool`
  ADD CONSTRAINT `fk_cool_stock_cool1` FOREIGN KEY (`stock_cool_id`) REFERENCES `stock_cool` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_cool_stock_fumigasi1` FOREIGN KEY (`stock_fumigasi_id`) REFERENCES `stock_fumigasi` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `customer`
--
ALTER TABLE `customer`
  ADD CONSTRAINT `fk_customer_users1` FOREIGN KEY (`users_id_users`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `drying1`
--
ALTER TABLE `drying1`
  ADD CONSTRAINT `fk_drying1_stock_drying11` FOREIGN KEY (`stock_drying1_id`) REFERENCES `stock_drying1` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_drying1_stock_wrapping1` FOREIGN KEY (`stock_wrapping_id`) REFERENCES `stock_wrapping` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `drying2`
--
ALTER TABLE `drying2`
  ADD CONSTRAINT `fk_drying2_stock_drying21` FOREIGN KEY (`stock_drying2_id`) REFERENCES `stock_drying2` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_drying2_stock_frezer1` FOREIGN KEY (`stock_frezer_id`) REFERENCES `stock_frezer` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `drying3`
--
ALTER TABLE `drying3`
  ADD CONSTRAINT `fk_drying3_stock_cool1` FOREIGN KEY (`stock_cool_id`) REFERENCES `stock_cool` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_drying3_stock_drying31` FOREIGN KEY (`stock_drying3_id`) REFERENCES `stock_drying3` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `filling`
--
ALTER TABLE `filling`
  ADD CONSTRAINT `fk_filling_stock_filling1` FOREIGN KEY (`stock_filling_id`) REFERENCES `stock_filling` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_filling_stock_probaku1` FOREIGN KEY (`stock_probaku_id`) REFERENCES `stock_probaku` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `frezer`
--
ALTER TABLE `frezer`
  ADD CONSTRAINT `fk_frezer_stock_drying11` FOREIGN KEY (`stock_drying1_id`) REFERENCES `stock_drying1` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_frezer_stock_frezer1` FOREIGN KEY (`stock_frezer_id`) REFERENCES `stock_frezer` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `fumigasi`
--
ALTER TABLE `fumigasi`
  ADD CONSTRAINT `fk_fumigasi_stock_drying21` FOREIGN KEY (`stock_drying2_id`) REFERENCES `stock_drying2` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_fumigasi_stock_fumigasi1` FOREIGN KEY (`stock_fumigasi_id`) REFERENCES `stock_fumigasi` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `harga_cincin`
--
ALTER TABLE `harga_cincin`
  ADD CONSTRAINT `fk_harga_cincin_produk1` FOREIGN KEY (`produk_id_produk`) REFERENCES `produk` (`id_produk`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `harga_cukai`
--
ALTER TABLE `harga_cukai`
  ADD CONSTRAINT `fk_harga_cukai_sub_produk1` FOREIGN KEY (`sub_produk_id`) REFERENCES `sub_produk` (`id_sub_produk`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `harga_kemasan`
--
ALTER TABLE `harga_kemasan`
  ADD CONSTRAINT `fk_harga_kemasan_sub_produk1` FOREIGN KEY (`sub_produk_id`) REFERENCES `sub_produk` (`id_sub_produk`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `harga_stiker`
--
ALTER TABLE `harga_stiker`
  ADD CONSTRAINT `fk_harga_stiker_sub_produk1` FOREIGN KEY (`sub_produk_id`) REFERENCES `sub_produk` (`id_sub_produk`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `history_bahanmasuk`
--
ALTER TABLE `history_bahanmasuk`
  ADD CONSTRAINT `fk_history_bahanmasuk_jenis1` FOREIGN KEY (`jenis_id_jenis`) REFERENCES `jenis` (`id_jenis`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_history_bahanmasuk_kategori1` FOREIGN KEY (`kategori_id_kategori`) REFERENCES `kategori` (`id_kategori`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_history_bahanmasuk_satuan_harga1` FOREIGN KEY (`satuan_harga_id`) REFERENCES `satuan_harga` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_history_bahanmasuk_stock_probaku1` FOREIGN KEY (`stock_probaku_id`) REFERENCES `stock_probaku` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `history_cincin`
--
ALTER TABLE `history_cincin`
  ADD CONSTRAINT `fk_history_cincin_harga_cincin1` FOREIGN KEY (`harga_cincin_id`) REFERENCES `harga_cincin` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_history_cincin_packing1` FOREIGN KEY (`packing_id`) REFERENCES `packing` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_history_cincin_stock_cincin1` FOREIGN KEY (`stock_cincin_id_stock_cincin`,`stock_cincin_produk_id_produk`) REFERENCES `stock_cincin` (`id_stock_cincin`, `produk_id_produk`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `history_cukai`
--
ALTER TABLE `history_cukai`
  ADD CONSTRAINT `fk_history_cukai_harga_cukai1` FOREIGN KEY (`harga_cukai_id`) REFERENCES `harga_cukai` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_history_cukai_packing1` FOREIGN KEY (`packing_id_packing`) REFERENCES `packing` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_history_cukai_stock_cukai1` FOREIGN KEY (`stock_cukai_id_stock_cukai`,`stock_cukai_id_sub_produk`,`stock_cukai_produk_id_produk`) REFERENCES `stock_cukai` (`id_stock_cukai`, `sub_produk_id_sub_produk`, `sub_produk_produk_id_produk`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `history_kemasan`
--
ALTER TABLE `history_kemasan`
  ADD CONSTRAINT `fk_history_kemasan_harga_kemasan1` FOREIGN KEY (`harga_kemasan_id`) REFERENCES `harga_kemasan` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_history_kemasan_packing1` FOREIGN KEY (`packing_id`) REFERENCES `packing` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_history_kemasan_stock_kemasan1` FOREIGN KEY (`stock_kemasan_id_stock_kemasan`,`stock_kemasan_sub_produk_id`,`stock_kemasan_produk_id_produk`,`stock_kemasan_kemasan_id_kemasan`) REFERENCES `stock_kemasan` (`id_stock_kemasan`, `sub_produk_id`, `produk_id_produk`, `kemasan_id_kemasan`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `history_stiker`
--
ALTER TABLE `history_stiker`
  ADD CONSTRAINT `fk_history_stiker_harga_stiker1` FOREIGN KEY (`harga_stiker_id`) REFERENCES `harga_stiker` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_history_stiker_packing1` FOREIGN KEY (`packing_id`) REFERENCES `packing` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_history_stiker_stock_stiker1` FOREIGN KEY (`stock_stiker_id_stock_stiker`,`stock_stiker_sub_produk_id`,`stock_stiker_produk_id_produk`) REFERENCES `stock_stiker` (`id_stock_stiker`, `sub_produk_id`, `produk_id_produk`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `items_object`
--
ALTER TABLE `items_object`
  ADD CONSTRAINT `fk_items_object_order1` FOREIGN KEY (`order_id`) REFERENCES `order` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_rfs_items_sub_produk1` FOREIGN KEY (`sub_produk_id`) REFERENCES `sub_produk` (`id_sub_produk`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `jenis_has_produk`
--
ALTER TABLE `jenis_has_produk`
  ADD CONSTRAINT `fk_jenis_has_produk_jenis1` FOREIGN KEY (`jenis_id_jenis`) REFERENCES `jenis` (`id_jenis`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_jenis_has_produk_produk1` FOREIGN KEY (`produk_id_produk`) REFERENCES `produk` (`id_produk`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `noc_items`
--
ALTER TABLE `noc_items`
  ADD CONSTRAINT `fk_noc_items_noc_inrfs1` FOREIGN KEY (`noc_inrfs_id`) REFERENCES `noc_inrfs` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_noc_items_sub_produk1` FOREIGN KEY (`sub_produk_id`) REFERENCES `sub_produk` (`id_sub_produk`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `order`
--
ALTER TABLE `order`
  ADD CONSTRAINT `fk_order_rfs_his1` FOREIGN KEY (`rfs_id`) REFERENCES `rfs_his` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_order_users1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `packing`
--
ALTER TABLE `packing`
  ADD CONSTRAINT `fk_packing_stock_packing1` FOREIGN KEY (`stock_packing_id`) REFERENCES `stock_packing` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_packing_stock_qc1` FOREIGN KEY (`stock_qc_id`) REFERENCES `stock_qc` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `penerimaan`
--
ALTER TABLE `penerimaan`
  ADD CONSTRAINT `fk_penerimaan_penjualan1` FOREIGN KEY (`penjualan_id`) REFERENCES `penjualan` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `penjualan`
--
ALTER TABLE `penjualan`
  ADD CONSTRAINT `fk_penjualan_customer1` FOREIGN KEY (`customer_id_customer`) REFERENCES `customer` (`id_customer`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `pressing`
--
ALTER TABLE `pressing`
  ADD CONSTRAINT `fk_pressing_stock_binding1` FOREIGN KEY (`stock_binding_id`) REFERENCES `stock_binding` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_pressing_stock_pressing1` FOREIGN KEY (`stock_pressing_id`) REFERENCES `stock_pressing` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `quality_control`
--
ALTER TABLE `quality_control`
  ADD CONSTRAINT `fk_quality_control_stock_drying31` FOREIGN KEY (`stock_drying3_id`) REFERENCES `stock_drying3` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_quality_control_stock_qc1` FOREIGN KEY (`stock_qc_id`) REFERENCES `stock_qc` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `return`
--
ALTER TABLE `return`
  ADD CONSTRAINT `fk_return_jenis1` FOREIGN KEY (`jenis_id_jenis`) REFERENCES `jenis` (`id_jenis`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_return_stock_bagan1` FOREIGN KEY (`stock_bagan_id`) REFERENCES `stock_bagan` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_return_stock_binding1` FOREIGN KEY (`stock_binding_id`) REFERENCES `stock_binding` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_return_stock_packing1` FOREIGN KEY (`stock_packing_id`) REFERENCES `stock_packing` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_return_stock_qc1` FOREIGN KEY (`stock_qc_id`) REFERENCES `stock_qc` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_return_stock_rfs1` FOREIGN KEY (`stock_rfs_id`) REFERENCES `stock_rfs` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_return_stock_wrapping1` FOREIGN KEY (`stock_wrapping_id`) REFERENCES `stock_wrapping` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_return_sub_produk1` FOREIGN KEY (`sub_produk_id_sub_produk`) REFERENCES `sub_produk` (`id_sub_produk`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `rfs_his`
--
ALTER TABLE `rfs_his`
  ADD CONSTRAINT `fk_rfs_users1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `satuan_harga`
--
ALTER TABLE `satuan_harga`
  ADD CONSTRAINT `fk_satuan_harga_jenis1` FOREIGN KEY (`jenis_id_jenis`) REFERENCES `jenis` (`id_jenis`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_satuan_harga_kategori1` FOREIGN KEY (`kategori_id_kategori`) REFERENCES `kategori` (`id_kategori`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `stock_bagan`
--
ALTER TABLE `stock_bagan`
  ADD CONSTRAINT `fk_stock_bagan_sub_produk1` FOREIGN KEY (`sub_produk_id`) REFERENCES `sub_produk` (`id_sub_produk`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_stock_bagan_users1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `stock_binding`
--
ALTER TABLE `stock_binding`
  ADD CONSTRAINT `fk_stock_binding_jenis_has_produk1` FOREIGN KEY (`jenis_has_produk_id_jnpro`,`jenis_has_produk_id_jenis`,`jenis_has_produk_id_produk`) REFERENCES `jenis_has_produk` (`id_jnpro`, `jenis_id_jenis`, `produk_id_produk`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `stock_cincin`
--
ALTER TABLE `stock_cincin`
  ADD CONSTRAINT `fk_stock_cincin_produk1` FOREIGN KEY (`produk_id_produk`) REFERENCES `produk` (`id_produk`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `stock_cool`
--
ALTER TABLE `stock_cool`
  ADD CONSTRAINT `fk_stock_cool_jenis_has_produk1` FOREIGN KEY (`jenis_has_produk_id_jnpro`,`jenis_has_produk_id_jenis`,`jenis_has_produk_id_produk`) REFERENCES `jenis_has_produk` (`id_jnpro`, `jenis_id_jenis`, `produk_id_produk`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `stock_cukai`
--
ALTER TABLE `stock_cukai`
  ADD CONSTRAINT `fk_stock_cukai_sub_produk1` FOREIGN KEY (`sub_produk_id_sub_produk`,`sub_produk_produk_id_produk`) REFERENCES `sub_produk` (`id_sub_produk`, `produk_id_produk`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `stock_drying1`
--
ALTER TABLE `stock_drying1`
  ADD CONSTRAINT `fk_stock_drying1_jenis_has_produk1` FOREIGN KEY (`jenis_has_produk_id_jnpro`,`jenis_has_produk_id_jenis`,`jenis_has_produk_id_produk`) REFERENCES `jenis_has_produk` (`id_jnpro`, `jenis_id_jenis`, `produk_id_produk`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `stock_drying2`
--
ALTER TABLE `stock_drying2`
  ADD CONSTRAINT `fk_stock_drying2_jenis_has_produk1` FOREIGN KEY (`jenis_has_produk_id_jnpro`,`jenis_has_produk_id_jenis`,`jenis_has_produk_id_produk`) REFERENCES `jenis_has_produk` (`id_jnpro`, `jenis_id_jenis`, `produk_id_produk`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `stock_drying3`
--
ALTER TABLE `stock_drying3`
  ADD CONSTRAINT `fk_stock_drying3_jenis_has_produk1` FOREIGN KEY (`jenis_has_produk_id_jnpro`,`jenis_has_produk_id_jenis`,`jenis_has_produk_id_produk`) REFERENCES `jenis_has_produk` (`id_jnpro`, `jenis_id_jenis`, `produk_id_produk`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `stock_filling`
--
ALTER TABLE `stock_filling`
  ADD CONSTRAINT `fk_stock_filling_jenis_has_produk1` FOREIGN KEY (`jenis_has_produk_id_jnpro`,`jenis_has_produk_id_jenis`,`jenis_has_produk_id_produk`) REFERENCES `jenis_has_produk` (`id_jnpro`, `jenis_id_jenis`, `produk_id_produk`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `stock_frezer`
--
ALTER TABLE `stock_frezer`
  ADD CONSTRAINT `fk_stock_frezer_jenis_has_produk1` FOREIGN KEY (`jenis_has_produk_id_jnpro`,`jenis_has_produk_id_jenis`,`jenis_has_produk_id_produk`) REFERENCES `jenis_has_produk` (`id_jnpro`, `jenis_id_jenis`, `produk_id_produk`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `stock_fumigasi`
--
ALTER TABLE `stock_fumigasi`
  ADD CONSTRAINT `fk_stock_fumigasi_jenis_has_produk1` FOREIGN KEY (`jenis_has_produk_id_jnpro`,`jenis_has_produk_id_jenis`,`jenis_has_produk_id_produk`) REFERENCES `jenis_has_produk` (`id_jnpro`, `jenis_id_jenis`, `produk_id_produk`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `stock_kemasan`
--
ALTER TABLE `stock_kemasan`
  ADD CONSTRAINT `fk_stock_kemasan_sub_produk1` FOREIGN KEY (`sub_produk_id`,`produk_id_produk`,`kemasan_id_kemasan`) REFERENCES `sub_produk` (`id_sub_produk`, `produk_id_produk`, `kemasan_id_kemasan`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `stock_packing`
--
ALTER TABLE `stock_packing`
  ADD CONSTRAINT `fk_stock_packing_sub_produk1` FOREIGN KEY (`sub_produk_id_sub_produk`) REFERENCES `sub_produk` (`id_sub_produk`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `stock_pressing`
--
ALTER TABLE `stock_pressing`
  ADD CONSTRAINT `fk_stock_pressing_jenis_has_produk1` FOREIGN KEY (`jenis_has_produk_id_jnpro`,`jenis_has_produk_id_jenis`,`jenis_has_produk_id_produk`) REFERENCES `jenis_has_produk` (`id_jnpro`, `jenis_id_jenis`, `produk_id_produk`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `stock_probaku`
--
ALTER TABLE `stock_probaku`
  ADD CONSTRAINT `fk_stock_probaku_jenis1` FOREIGN KEY (`jenis_id_jenis`) REFERENCES `jenis` (`id_jenis`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_stock_probaku_kategori1` FOREIGN KEY (`kategori_id_kategori`) REFERENCES `kategori` (`id_kategori`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `stock_qc`
--
ALTER TABLE `stock_qc`
  ADD CONSTRAINT `fk_stock_cool_jenis_has_produk2` FOREIGN KEY (`jenis_has_produk_id_jnpro`,`jenis_has_produk_id_jenis`,`jenis_has_produk_id_produk`) REFERENCES `jenis_has_produk` (`id_jnpro`, `jenis_id_jenis`, `produk_id_produk`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `stock_rfs`
--
ALTER TABLE `stock_rfs`
  ADD CONSTRAINT `fk_stock_rfs_sub_produk1` FOREIGN KEY (`sub_produk_id_sub_produk`) REFERENCES `sub_produk` (`id_sub_produk`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `stock_stiker`
--
ALTER TABLE `stock_stiker`
  ADD CONSTRAINT `fk_stock_stiker_sub_produk1` FOREIGN KEY (`sub_produk_id`,`produk_id_produk`) REFERENCES `sub_produk` (`id_sub_produk`, `produk_id_produk`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `stock_wrapping`
--
ALTER TABLE `stock_wrapping`
  ADD CONSTRAINT `fk_stock_wrapping_jenis_has_produk1` FOREIGN KEY (`jenis_has_produk_id_jnpro`,`jenis_has_produk_id_jenis`,`jenis_has_produk_id_produk`) REFERENCES `jenis_has_produk` (`id_jnpro`, `jenis_id_jenis`, `produk_id_produk`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `sub_produk`
--
ALTER TABLE `sub_produk`
  ADD CONSTRAINT `fk_sub_produk_kemasan1` FOREIGN KEY (`kemasan_id_kemasan`) REFERENCES `kemasan` (`id_kemasan`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_sub_produk_produk1` FOREIGN KEY (`produk_id_produk`) REFERENCES `produk` (`id_produk`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `fk_users_level_user` FOREIGN KEY (`level_user_id_level`) REFERENCES `level_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `value_reject`
--
ALTER TABLE `value_reject`
  ADD CONSTRAINT `fk_value_reject_noc_items1` FOREIGN KEY (`noc_items_id`) REFERENCES `noc_items` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `wrapping`
--
ALTER TABLE `wrapping`
  ADD CONSTRAINT `fk_wrapping_stock_pressing1` FOREIGN KEY (`stock_pressing_id`) REFERENCES `stock_pressing` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_wrapping_stock_probaku1` FOREIGN KEY (`stock_probaku_id`) REFERENCES `stock_probaku` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_wrapping_stock_wrapping1` FOREIGN KEY (`stock_wrapping_id`) REFERENCES `stock_wrapping` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
