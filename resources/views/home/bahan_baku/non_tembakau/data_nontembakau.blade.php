@extends('../../layout')

@section('title')
<title>BIN - ERP : Data Non-Tembakau</title>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css" />
@endsection

@section('content')
<div class="row page-titles">
	<div class="col-md-5 align-self-center">
		<h3 class="text-themecolor">NON TEMBAKAU</h3>
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a href="javascript:void(0)">Bahan Baku</a></li>
			<li class="breadcrumb-item active">NON-TEMBAKAU</li>
		</ol>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-body">
				<div class="row">
					<div class="col-md-6">
						<h4 class="card-title">NON-TEMBAKAU</h4>
					</div>
					<div class="col-md-6" align="right">
						<button type="button" id="tambah-nontembakau" class="btn btn-success btn-md btn-rounded" data-link="{{ route('nontembakau.post') }}"><i class="fa fa-plus"></i></button>
						<button type="button" id="refreshbutton" class="btn btn-secondary btn-md btn-rounded"><i class="mdi mdi-refresh"></i></button>
					</div>
					<div class="col-md-12" align="center" id="loadingdelete">
						<h5>Data tengah diproses. Mohon tunggu sejenak!</h5>
					</div>
					<div class="col-md-12">
						<div class="table-responsive">
							<table id="table-nontembakau" class="display nowrap table table-sm table-hover table-striped table-bordered">
								<thead>
									<tr align="center">
										<th rowspan="2">No.</th>
										<th rowspan="2">Aksi</th>
										<th rowspan="2">Tanggal</th>
										<th rowspan="2">Kode Produk</th>
										<th rowspan="2">Kode Sub</th>
										<th rowspan="2">Nama Sub</th>
										<th rowspan="2">Packaging</th>
										<th colspan="4" class="table-warning text-center">Data Cincin</th>
										<th colspan="6" class="table-secondary text-center">Data Stiker</th>
										<th colspan="6" class="table-success text-center">Data Cukai</th>
										<th colspan="4" class="table-primary text-center">Data Packaging</th>
										<th rowspan="2">Keterangan</th>
									</tr>
									<tr align="center">
										<th class="table-warning">Masuk</th>
										<th class="table-warning">Terpakai</th>
										<th class="table-warning">Afkir</th>
										<th class="table-warning">Sisa</th>

										<th class="table-secondary">Masuk Luar</th>
										<th class="table-secondary">Terpakai Luar</th>
										<th class="table-secondary">Sisa Luar</th>

										<th class="table-secondary">Masuk Dalam</th>
										<th class="table-secondary">Terpakai Dalam</th>
										<th class="table-secondary">Sisa Dalam</th>

										<th class="table-success">Masuk Cukai1</th>
										<th class="table-success">Terpakai Cukai1</th>
										<th class="table-success">Sisa Cukai1</th>

										<th class="table-success">Masuk Cukai2</th>
										<th class="table-success">Terpakai Cukai2</th>
										<th class="table-success">Sisa Cukai2</th>

										<th class="table-primary">Masuk</th>
										<th class="table-primary">Terpakai</th>
										<th class="table-primary">Afkir</th>
										<th class="table-primary">Sisa</th>
									</tr>
								</thead>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<div class="modal fade" id="modal-nontembakau" tabindex="-1" role="dialog" aria-labelledby="title" aria-hidden="true" style="display: none;">
	<div class="modal-dialog modal-xl">
		<div class="modal-content">
			<div class="card">
				<div class="card-header">
					<div class="modal-header">
						<h4 class="modal-title" id="title"></h4>
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					</div>
				</div>
				<div class="card-body">
					<form id="form-nontembakau" method="POST">
						<div class="modal-body row">
							<div class="form-group col-md-12 fa-3x spinner_nekr" align="center">
								<i class="fas fa-spinner fa-spin"></i>
							</div>
							<div class="form-group col-md-12">
								<label class="control-label">Tanggal</label>
								<input type="text" class="form-control" id="tanggal" name="tanggal">
							</div>
							<div class="form-group col-md-6">
								<label class="control-label">Kode Produk</label>
								<select class="form-control" id="kode-produk" name="kodeproduk">	
								</select>
								<input type="hidden" id="idproduk-hidden" name="produkhidden">
							</div>
							<div class="form-group col-md-6">
								<label class="control-label">Kode Sub</label>
								<select class="form-control" id="kode-sub" name="kodesub"></select>
								<input type="hidden" id="idsubproduk-hidden" name="subprodukhidden">
							</div>
							<div class="form-group col-md-12">
								<button type="button" id="showhidd" class="btn btn-sm btn-block btn-info">Cari</button>

								<input type="hidden" id="idaksesorischeck" name="idaksesorischeck">
							</div>
							<div class="form-group col-md-12 fa-3x spinner_cari" align="center">
								<i class="fas fa-spinner fa-spin"></i>
							</div>
							<div class="form-group col-md-12">
								<label><b id="hargaset_show"></b></label>
								<input type="hidden" id="id_kemasan" name="id_kemasan">

								<input type="hidden" id="id_stock_cincin" name="id_stock_cincin">
								<input type="hidden" id="id_stock_cukai" name="id_stock_cukai">
								<input type="hidden" id="id_stock_stiker" name="id_stock_stiker">
								<input type="hidden" id="id_stock_kemasan" name="id_stock_kemasan">

								<input type="hidden" id="idhargacincin" name="idhargacincin">
								<input type="hidden" id="idhargacukai" name="idhargacukai">
								<input type="hidden" id="idhargastiker" name="idhargastiker">
								<input type="hidden" id="idhargakemasan" name="idhargakemasan">

								<input type="hidden" id="stockawalcincin" name="stockawalcincin">
								<input type="hidden" id="stockawalstikerluar" name="stockawalstikerluar">
								<input type="hidden" id="stockawalstikerdalam" name="stockawalstikerdalam">
								<input type="hidden" id="stockawalcukai1" name="stockawalcukai1">
								<input type="hidden" id="stockawalcukai2" name="stockawalcukai2">
								<input type="hidden" id="stockawalkemasan" name="stockawalkemasan">

								<input type="hidden" id="stocknowcincin" name="stocknowcincin">
								<input type="hidden" id="stocknowstikerluar" name="stocknowstikerluar">
								<input type="hidden" id="stocknowstikerdalam" name="stocknowstikerdalam">
								<input type="hidden" id="stocknowcukai1" name="stocknowcukai1">
								<input type="hidden" id="stocknowcukai2" name="stocknowcukai2">
								<input type="hidden" id="stocknowkemasan" name="stocknowkemasan">
							</div>
							<div class="form-group col-md-12">
								<label class="control-label">Cincin: <b id="stockcincin"></b></label>
								<div class="row">
									<div class="col-md-4">
										<label>Masuk</label>
										<input type="number" class="form-control" id="masuk_cincin" name="masuk_cincin" placeholder="Masuk Cincin" required>
										<input type="hidden" class="form-control" id="masuk_cincinhidden" name="masuk_cincinhidden">
									</div>
									<div class="col-md-4">
										<label>Terpakai</label>
										<input type="number" class="form-control" id="terpakai_cincin" name="terpakai_cincin" placeholder="Terpakai Cincin" required>
										<input type="hidden" class="form-control" id="terpakai_cincinhidden" name="terpakai_cincinhidden">
									</div>
									<div class="col-md-4">
										<label>Afkir</label>
										<input type="number" class="form-control" id="afkir_cincin" name="afkir_cincin" placeholder="Afkir Cincin" required>
										<input type="hidden" class="form-control" id="afkir_cincinhidden" name="afkir_cincinhidden">
									</div>
								</div>
							</div>
							<div class="form-group col-md-12">
								<label class="control-label">Stiker: <b id="stockstiker"></b></label>
								<div class="row">
									<div class="col-md-6">
										<label>Masuk Luar</label>
										<input type="number" class="form-control" id="masuk_stikerl" name="masuk_stikerl" placeholder="Masuk Stiker Luar" required>
										<input type="hidden" class="form-control" id="masuk_stikerlhidden" name="masuk_stikerlhidden" >
									</div>
									<div class="col-md-6">
										<label>Terpakai Luar</label>
										<input type="number" class="form-control" id="terpakai_stikerl" name="terpakai_stikerl" placeholder="Terpakai Stiker Luar" required>
										<input type="hidden" class="form-control" id="terpakai_stikerlhidden" name="terpakai_stikerlhidden" >
									</div>
									<div class="col-md-6">
										<label>Masuk Dalam</label>
										<input type="number" class="form-control" id="masuk_stikerd" name="masuk_stikerd" placeholder="Masuk Stiker Dalam" required>
										<input type="hidden" class="form-control" id="masuk_stikerdhidden" name="masuk_stikerdhidden" >
									</div>
									<div class="col-md-6">
										<label>Terpakai Dalam</label>
										<input type="number" class="form-control" id="terpakai_stikerd" name="terpakai_stikerd" placeholder="Terpakai Stiker Dalam" required>
										<input type="hidden" class="form-control" id="terpakai_stikerdhidden" name="terpakai_stikerdhidden" >
									</div>
								</div>
							</div>
							<div class="form-group col-md-12">
								<label class="control-label">Cukai: <b id="stockcukai"></b></label>
								<div class="row">
									<div class="col-md-6">
										<label>Masuk Cukai1</label>
										<input type="number" class="form-control" id="masuk_cukai1" name="masuk_cukai1" placeholder="Masuk Cukai 1" required>
										<input type="hidden" class="form-control" id="masuk_cukai1hidden" name="masuk_cukai1hidden">
									</div>
									<div class="col-md-6">
										<label>Terpakai Cukai1</label>
										<input type="number" class="form-control" id="terpakai_cukai1" name="terpakai_cukai1" placeholder="Terpakai Cukai 1" required>
										<input type="hidden" class="form-control" id="terpakai_cukai1hidden" name="terpakai_cukai1hidden">
									</div>
									<div class="col-md-6">
										<label>Masuk Cukai2</label>
										<input type="number" class="form-control" id="masuk_cukai2" name="masuk_cukai2" placeholder="Masuk Cukai 2" required>
										<input type="hidden" class="form-control" id="masuk_cukai2hidden" name="masuk_cukai2hidden">
									</div>
									<div class="col-md-6">
										<label>Terpakai Cukai2</label>
										<input type="number" class="form-control" id="terpakai_cukai2" name="terpakai_cukai2" placeholder="Terpakai Cukai 2" required>
										<input type="hidden" class="form-control" id="terpakai_cukai2hidden" name="terpakai_cukai2hidden">
									</div>
								</div>
							</div>
							<div class="form-group col-md-12">
								<label class="control-label">Packaging (<b id="nama_kemasan"></b>): <b id="stockkemasan"></b></label>
								<div class="row">
									<div class="col-md-4">
										<label>Masuk</label>
										<input type="number" class="form-control" id="masuk_kemasan" name="masuk_kemasan" placeholder="Masuk Kemasan" required>
										<input type="hidden" class="form-control" id="masuk_kemasanhidden" name="masuk_kemasanhidden">
									</div>
									<div class="col-md-4">
										<label>Terpakai</label>
										<input type="number" class="form-control" id="terpakai_kemasan" name="terpakai_kemasan" placeholder="Terpakai Kemasan" required>
										<input type="hidden" class="form-control" id="terpakai_kemasanhidden" name="terpakai_kemasanhidden">
									</div>
									<div class="col-md-4">
										<label>Afkir</label>
										<input type="number" class="form-control" id="afkir_kemasan" name="afkir_kemasan" placeholder="Afkir Kemasan" required>
										<input type="hidden" class="form-control" id="afkir_kemasanhidden" name="afkir_kemasanhidden">
									</div>
								</div>
							</div>
							<div class="form-group col-md-12" id="aksesorisform">
							</div>
							<div class="form-group col-md-12">
								<label class="control-label">Keterangan</label>
								<textarea class="form-control" name="keterangan" id="keterangan" placeholder="Keterangan"></textarea>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" id="submit_nontembakau" class="btn btn-primary waves-effect text-left">Save</button>
							<button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Close</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>


<div class="modal fade" id="modal-aks" tabindex="-1" role="dialog" aria-labelledby="title" aria-hidden="true" style="display: none;">
	<div class="modal-dialog modal-xl">
		<div class="modal-content">
			<div class="card">
				<div class="card-header">
					<div class="modal-header">
						<h4 class="modal-title" id="title"></h4>
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					</div>
				</div>
				<div class="card-body">
					<form id="form-aks" method="POST">
						<div class="modal-body row">
							<div class="form-group col-md-12 fa-3x spinner_ame" align="center">
								<i class="fas fa-spinner fa-spin"></i>
							</div>
							<div class="form-group col-md-12" id="aksesorisformupdate">
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" id="submit_updateaks" class="btn btn-primary waves-effect text-left">Save</button>
							<button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Close</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('javascript')
<script src="{{ asset('node_modules/moment/moment.js') }}">
</script><script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
<script src="{{ asset('node_modules/datatables/datatables.min.js') }}"></script>
@include('home.bahan_baku.non_tembakau.script_nontembakau')
@endsection