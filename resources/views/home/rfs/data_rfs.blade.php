@extends('../../layout')

@section('title')
<title>BIN - ERP : Pengiriman</title>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css" />
<style type="text/css">
</style>
@endsection

@section('content')
<div class="row page-titles">
	<div class="col-md-5 align-self-center">
		<h3 class="text-themecolor">Pengiriman</h3>
		<ol class="breadcrumb" id="intop">
			<li class="breadcrumb-item"><a href="javascript:void(0)">Pengiriman</a></li>
			<li class="breadcrumb-item active" id="intop-dta">Data Pengiriman</li>
			<li class="breadcrumb-item active" id="intop-tambah">Tambah RFS</li>
			<li class="breadcrumb-item active" id="intop-update">Update RFS</li>
		</ol>
	</div>
</div>
<div class="row" id="list-data">
	<div class="col-md-12">
		<div class="row">
			<div class="col-md-6">
				<legend>Data Pengiriman</legend>
			</div>
			<div class="col-md-2 offset-md-4">
				<button type="button" data-link="#" class="btn btn-success btn-rounded btn-icons" id="tambah-rfs"><i class="fa fa-plus"></i></button>
				<button type="button" onclick="Refresh_rfs()" class="btn btn-rounded btn-icons btn-warning"><i class="mdi mdi-refresh"></i></button>
				<button type="button" onclick="change_position()" class="btn btn-secondary btn-rounded btn-icons" id="reject-rfs"><i class="fa fa-window-minimize" aria-hidden="true"></i></button>
			</div>
			<div id="rejectrfs" class="col-md-12">
				<br>
				<div class="row">
					<form class="col-md-6" id="rejectisi" method="POST">
						<div class="row">
							<div class="col-md-12" align="center">
								<legend>Reject RFS</legend>
							</div>
							<div class="form-group col-md-12 fa-3x spinner_nekraddrfs" align="center">
								<i class="fas fa-spinner fa-spin"></i>
							</div>
							<div class="form-group col-md-5">
								<label class="control-label">Produk</label>
								<select class="form-control" id="produk_reject"></select>
								<input type="hidden" id="produk_hidinrfs">
							</div>
							<div class="form-group col-md-5">
								<label class="control-label">Sub Produk</label>
								<select class="form-control" id="subproduk_reject"></select>
								<input type="hidden" id="subproduk_hidinreject">
								<input type="hidden" id="namesubproduk_hid">
							</div>
							<div class="form-group col-md-2">
								<input type="hidden" id="idstockrfscart"><br>
								<button type="button" onclick="getthis()" class="btn btn-light btn-md btn-block">Cari</button>
							</div>
							<div class="form-group col-md-12">
								<label class="control-label">Stock RFS</label>
								<input type="number" class="form-control" id="stockjmlrfs_for" placeholder="Stock RFS" readonly>
							</div>
							<div class="form-group col-md-12">
								<label class="control-label">JML Reject</label>
								<input type="number" class="form-control" id="jml_reject" placeholder="JML Reject">
							</div>
							<div class="form-group col-md-12">
								<button type="button" onclick="save_sessionreject()" class="btn btn-warning btn-sm btn-block">add</button>
							</div>
						</div>
					</form>
					<div class="col-md-6">
						<legend>Daftar Item</legend>
						<div class="" align="center" id="loadingdeletecart">
							<h5>Data tengah diproses. Mohon tunggu sejenak!</h5>
						</div>
						<div class="table-responsive">
							<table class="table table-striped">
								<thead class="thead-dark">
									<tr>
										<th>No.</th>
										<th>Name</th>
										<th>JML</th>
										<th>#</th>
									</tr>
								</thead>
								<tbody id="data_cart">
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="form-group col-md-12">
						<label class="control-label">Tanggal</label>
						<input type="text" id="tanggal_reject" class="form-control">
					</div>
					<div class="form-group col-md-12">
						<label class="control-label">Pesan :</label>
						<textarea class="form-control" id="pesan" placeholder="Pesan"></textarea>
					</div>
					<div class="form-group col-md-12">
						<label class="control-label">Keterangan</label>
						<textarea class="form-control" id="keterangan" placeholder="Keterangan"></textarea>
					</div>
				</div>
				<div class="row">
					<button type="button" onclick="save_rejectrfs()" class="btn btn-info btn-md btn-block">Send</button>
				</div>
			</div>
			<div class="col-md-12">
				<div class="table-responsive">
					<table id="table-rfs" class="display nowrap table table-hover table-striped table-bordered" width="100%">
						<thead>
							<tr>
								<th>No.</th>
								<th>Tanggal</th>
								<th>Untuk</th>
								<th>Bagan</th>
								<th>Keterangan</th>
								<th>Status</th>
								<th>Aksi</th>
							</tr>
						</thead>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="row" id="add-data">
	<div class="col-md-12">
		<div class="row">
			<div class="col-md-12 text-center">
				<legend>Tambah Pengiriman</legend>
			</div>
			<div class="col-md-12 text-center" align="center" id="loadingtambah_rfs">
				<h5>Data tengah diproses. Mohon tunggu sejenak!</h5>
			</div>
			<div class="col-md-6">
				<div class="table-responsive">
					<legend>Daftar Stock Pengiriman</legend>
					<div class="" align="center" id="loadingpengiriman">
						<h5>Data tengah diproses. Mohon tunggu sejenak!</h5>
					</div>
					<table id="table-stock" class="display nowrap table table-hover table-striped table-bordered" width="100%">
						<thead class="table-dark">
							<tr>
								<th>No.</th>
								<th>Sub Kode</th>
								<th>Sub Produk</th>
								<th>JML</th>
								<th>*add</th>
								<th>Aksi</th>
							</tr>
						</thead>
					</table>
				</div>
			</div>
			<div class="col-md-6">
				<div class="table-responsive">
					<legend>Daftar Keranjang</legend>
					<div class="" align="center" id="loadingkeranjang">
						<h5>Data tengah diproses. Mohon tunggu sejenak!</h5>
					</div>
					<table class="display nowrap table table-hover table-striped table-bordered" width="100%">
						<thead class="table-secondary">
							<tr>
								<th>No.</th>
								<th>Produk</th>
								<th>JML</th>
								<th>Aksi</th>
							</tr>
						</thead>
						<tbody id="table-session">
						</tbody>
					</table>
				</div>
			</div>
			<div class="col-md-12">
				<form id="tambah-rfs" class="row">
					<div class="form-group col-md-12">
						<label class="control-label">Tanggal</label>
						<input type="text" class="form-control" id="tanggal_rfs" name="tanggal_rfs" placeholder="Tanggal">
					</div>
					<div class="form-group col-md-12">
						<label class="control-label">Level Bagan</label>
						<select class="form-control" name="level" id="level">
						</select>
					</div>
					<div class="form-group col-md-12">
						<label class="control-label">User Bagan</label>
						<select class="form-control" name="userbagan" id="userbagan">
						</select>
					</div>
					<div class="form-group col-md-12">
						<label class="control-label">Keterangan</label>
						<textarea class="form-control" name="ket_rfs" id="ket_rfs" placeholder="Keterangan"></textarea>
					</div>
				</form>
				<div class="row">
					<div class="form-group col-md-12" align="right">
						<button type="button" class="btn btn-info btn-lg waves-effect" onclick="tambah_rfs()">Save</button>
						<button type="button" id="back_tolist" class="btn btn-secondary btn-lg waves-effect">Back</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('javascript')
<script src="{{ asset('node_modules/moment/moment.js') }}">
</script><script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
<script src="{{ asset('node_modules/datatables/datatables.min.js') }}"></script>
@include('home.rfs.script_rfs')
@endsection