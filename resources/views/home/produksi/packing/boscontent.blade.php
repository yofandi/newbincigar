<div class="row" id="load_bos">
	<div class="form-group col-md-4">
		<label class="control-label">Kemasan</label>
		<input type="text" class="form-control" id="kemasanbos" placeholder="Kemasan" readonly>

		<input type="hidden" id="idkemasanbos">
		<input type="hidden" id="idpackingbos">
		<input type="hidden" id="idstokemasanbos">
		<input type="hidden" id="idcincinbos">
		<input type="hidden" id="idstikerbos">
	</div>
	<div class="form-group col-md-4">
		<label class="control-label">Stock Kemasan</label>
		<input type="number" class="form-control" id="stockkemasanbos" placeholder="Stock Kemasan" readonly>
	</div>
	<div class="form-group col-md-4">
		<label class="control-label">Stock Cincin</label>
		<input type="number" class="form-control" id="stockcincinbos" placeholder="Stock Cincin" readonly>
	</div>
	<div class="form-group col-md-12">
		<label class="control-label">Stock Stiker</label>
		<div class="row">
			<div class="col-md-6">
				*luar
				<input type="number" class="form-control" id="stockluarstibos" placeholder="Stock Stiker Luar" readonly>
			</div>
			<div class="col-md-6">
				*dalam
				<input type="number" class="form-control" id="stockdalamstibos" placeholder="Stock Stiker Dalam" readonly>
			</div>
		</div>
	</div>
	<div class="form-group col-md-12" id="batangbos">
		<label class="control-label">Batang Quality Control</label>
		<div class="row">
			<div class="col-md-2">
				*Robusto
				<input type="number" class="form-control" id="batangrbs_bos" placeholder="Jml Robusto Quality Control" readonly>
			</div>
			<div class="col-md-2">
				*Corona
				<input type="number" class="form-control" id="batangcor_bos" placeholder="Jml Corona Quality Control" readonly>
			</div>
			<div class="col-md-2">
				*Half Corona
				<input type="number" class="form-control" id="batanghlf_bos" placeholder="Jml Half Corona Quality Control" readonly>
			</div>
			<div class="col-md-2">
				*Cigar Master
				<input type="number" class="form-control" id="batangcgr_bos" placeholder="Jml Cigar Master Quality Control" readonly>
			</div>
			<div class="col-md-2">
				*El Nino
				<input type="number" class="form-control" id="batangelnino_bos" placeholder="Jml El Nino Quality Control" readonly>
			</div>
			<div class="col-md-2">
				*C99
				<input type="number" class="form-control" id="batangc99_bos" placeholder="Jml C99 Quality Control" readonly>
			</div>
		</div>
	</div>
	<div class="form-group col-md-12">
		<label class="control-label">Stock Awal</label>
		<input type="number" class="form-control" id="stockawalbos" placeholder="Stock Awal" readonly>
	</div>
	<div class="form-group col-md-5">
		<label class="control-label">Packing</label>
		<input type="number" class="form-control" id="jmlpackingbos" placeholder="JML Packing">
	</div>
	<div class="form-group col-md-2" align="center">
		<br>
		<label class="control-label">X</label>
	</div>
	<div class="form-group col-md-5">
		<label class="control-label">Isi/Kemasan</label>
		<input type="text" class="form-control" id="isikemasanbos" placeholder="Isi Kemasan" readonly>
		*Robusto: 3; *Corona: 4; *Half Corona: 3; *Cigar Master: 4; *C99: 6; *El Nino: 5;
	</div><div class="form-group col-md-6">
		<label class="control-label">Terpakai Stiker Luar</label>
		<input type="number" class="form-control" id="terpakailuarbos" placeholder="Stiker Terpakai Luar">
	</div>
	<div class="form-group col-md-6">
		<label class="control-label">Terpakai Stiker Baru</label>
		<input type="number" class="form-control" id="terpakaidalambos" placeholder="Stiker Terpakai Baru">
	</div>
	<div class="form-group col-md-6">
		<label class="control-label">Terpakai Kemasan</label>
		<input type="number" class="form-control" id="terpakaikemasanbos" placeholder="Terpakai Kemasan">
	</div>
	<div class="form-group col-md-6">
		<label class="control-label">Terpakai Cincin</label>
		<input type="number" class="form-control" id="terpakaicincinbos" placeholder="Terpakai Cincin">
	</div>
	<div class="form-group col-md-12">
		<label class="control-label">Keterangan</label>
		<textarea class="form-control" id="ket_bos" placeholder="Keterangan"></textarea>
	</div>
</div>