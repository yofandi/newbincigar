<!DOCTYPE html>
<html lang="en">
<head>
    @include('layouts.head')
    @yield('title')
    @yield('css')
</head>
<body class="fix-header fix-sidebar card-no-border">
    @include('layouts.loader')
    @php
    date_default_timezone_set('Asia/Jakarta');
    $bahan = [1,2,3];
    $produksi = [1,2,3,4,5];
    $rfs = [1,2,3,4,5,6];
    $rfsbagan = [1,2,6,7,8,9,10,11];
    @endphp
    <div id="main-wrapper">
        <header class="topbar">
            @include('layouts.navbar')
        </header>
        <aside class="left-sidebar">
            @include('layouts.sidebar')
        </aside>
        <div class="page-wrapper">
            <div class="container-fluid">
                @yield('content')
            </div>
            @include('layouts.footer')
        </div>
    </div>
    @include('layouts.bottom')
    @yield('javascript')
    @include('layouts.notification')
</body>
</html>