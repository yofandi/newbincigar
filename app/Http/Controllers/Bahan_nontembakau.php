<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;
use DataTables;
use App\History_aksesoris;
use App\History_cincin;
use App\History_stiker;
use App\History_cukai;
use App\History_kemasan;

use App\Http\Controllers\Back_bahannontembakau;

class Bahan_nontembakau extends Controller
{
	public function __construct()
	{
		$this->middleware('auth');
		$this->backeigni = new Back_bahannontembakau;
	}

	public function getsomenontembakau(Request $request)
	{
		$query = DB::table('produk')
		->select(DB::raw('
			IFNULL(stock_cincin.stock_qty_cincin,0) AS stock_qty_cincin,
			IFNULL(stock_stiker.stock_luar,0) AS stock_luar,
			IFNULL(stock_stiker.stock_dalam,0) AS stock_dalam,
			IFNULL(stock_cukai.stock_qty_cukai,0) AS stock_qty_cukai,
			IFNULL(stock_cukai.stock_qty_cukai2,0) AS stock_qty_cukai2,
			IFNULL(stock_kemasan.stock_kemasan,0) AS stock_kemasan
			'))
		->leftJoin('sub_produk','produk.id_produk','=','sub_produk.produk_id_produk')

		->leftJoin('stock_cincin','produk.id_produk','=','stock_cincin.produk_id_produk')
		->leftJoin('stock_cukai','sub_produk.id_sub_produk','=','stock_cukai.sub_produk_id_sub_produk')
		->leftJoin('stock_stiker','sub_produk.id_sub_produk','=','stock_stiker.sub_produk_id')
		->leftJoin('stock_kemasan','sub_produk.id_sub_produk','=','stock_kemasan.sub_produk_id')

		->where([
			['produk.id_produk','=',$request->produk],
			['sub_produk.id_sub_produk','=',$request->subproduk]
		])
		->first();
		return json_encode($query);
	}

	public function getnontembakauval(Request $request)
	{
		$query = DB::table('produk')
		->select(DB::raw('
			produk.id_produk,
			sub_produk.id_sub_produk,
			kemasan.id_kemasan,
			kemasan.nama_kemasan,

			IFNULL(stock_cincin.id_stock_cincin,"") AS id_stock_cincin,
			IFNULL(stock_cukai.id_stock_cukai,"") AS id_stock_cukai,
			IFNULL(stock_stiker.id_stock_stiker,"") AS id_stock_stiker,
			IFNULL(stock_kemasan.id_stock_kemasan,"") AS id_stock_kemasan,

			IFNULL(stock_cincin.stock_qty_cincin,0) AS stock_qty_cincin,
			IFNULL(stock_cukai.stock_qty_cukai,0) AS stock_qty_cukai,
			IFNULL(stock_cukai.stock_qty_cukai2,0) AS stock_qty_cukai2,
			IFNULL(stock_stiker.stock_luar,0) AS stock_luar,
			IFNULL(stock_stiker.stock_dalam,0) AS stock_dalam,
			IFNULL(stock_kemasan.stock_kemasan,0) AS stock_kemasan,


			IFNULL(harga_cincin.harga,0) AS hargacincin,
			IFNULL(harga_cukai.harga,0) AS hargacukai,
			IFNULL(harga_stiker.harga_dalam,0) AS hargastiker_dalam,
			IFNULL(harga_stiker.harga_luar,0) AS hargastiker_luar,
			IFNULL(harga_kemasan.harga,0) AS hargakemasan,

			IFNULL(harga_cincin.id,"") AS idhargacincin,
			IFNULL(harga_cukai.id,"") AS idhargacukai,
			IFNULL(harga_stiker.id,"") AS idhargastiker,
			IFNULL(harga_kemasan.id,"") AS idhargakemasan,

			GROUP_CONCAT(IFNULL(stock_aksesoris.stock_aksesoris,0) SEPARATOR ",") AS stock_aksesoris,
			GROUP_CONCAT(sub_produk_has_aksesoris.id SEPARATOR ",") AS id_aksesoris,
			GROUP_CONCAT(aksesoris.aksesoris SEPARATOR ",") AS nama_aksesoris

			'))
		->leftJoin('sub_produk','produk.id_produk','=','sub_produk.produk_id_produk')
		->leftJoin('kemasan','sub_produk.kemasan_id_kemasan','=','kemasan.id_kemasan')

		->leftJoin('stock_cincin','produk.id_produk','=','stock_cincin.produk_id_produk')
		->leftJoin('stock_cukai','sub_produk.id_sub_produk','=','stock_cukai.sub_produk_id_sub_produk')
		->leftJoin('stock_stiker','sub_produk.id_sub_produk','=','stock_stiker.sub_produk_id')
		->leftJoin('stock_kemasan','sub_produk.id_sub_produk','=','stock_kemasan.sub_produk_id')

		->leftJoin('harga_cincin','produk.id_produk','=','harga_cincin.produk_id_produk')
		->leftJoin('harga_cukai','sub_produk.id_sub_produk','=','harga_cukai.sub_produk_id')
		->leftJoin('harga_stiker','sub_produk.id_sub_produk','=','harga_stiker.sub_produk_id')
		->leftJoin('harga_kemasan','sub_produk.id_sub_produk','=','harga_kemasan.sub_produk_id')

		->leftjoin('sub_produk_has_aksesoris','sub_produk.id_sub_produk','=','sub_produk_has_aksesoris.sub_produk_id')
		->leftjoin('aksesoris','sub_produk_has_aksesoris.aksesoris_id','=','aksesoris.id')
		->leftjoin('stock_aksesoris','sub_produk_has_aksesoris.id','=','stock_aksesoris.sub_has_aks_id')

		->where([
			['produk.id_produk','=',$request->produk],
			['sub_produk.id_sub_produk','=',$request->subproduk]
		])
		->first();
		return json_encode($query);
	}

	public function gettabledatanontembakau()
	{
		$query = DB::table('history_cincin')
		->select(
			'produk.id_produk',
			'produk.kode_produk',
			'produk.produk',
			'sub_produk.id_sub_produk',
			'sub_produk.sub_kode',
			'sub_produk.sub_produk',
			'kemasan.id_kemasan',
			'kemasan.nama_kemasan',

			'history_cincin.id AS idnontembakau',
			'history_cincin.tanggal_cincin',

			'history_cincin.stock_cincin_id_stock_cincin AS id_stock_cincin',
			'history_stiker.stock_stiker_id_stock_stiker AS id_stock_stiker',
			'history_kemasan.stock_kemasan_id_stock_kemasan AS id_stock_kemasan',
			'history_cukai.stock_cukai_id_stock_cukai AS id_stock_cukai',

			'history_cincin.harga_cincin_id AS idhargacincin',
			'history_stiker.harga_stiker_id AS idhargastiker',
			'history_kemasan.harga_kemasan_id AS idhargakemasan',
			'history_cukai.harga_cukai_id AS idhargacukai',

			'harga_cincin.harga AS hargacincin',
			'harga_cukai.harga AS hargacukai',
			'harga_stiker.harga_dalam AS hargastiker_dalam',
			'harga_stiker.harga_luar AS hargastiker_luar',
			'harga_kemasan.harga AS hargakemasan',

			'history_cincin.masuk',
			'history_cincin.terpakai',
			'history_cincin.afkir',
			'history_cincin.sisa',

			'history_stiker.masuk_luar',
			'history_stiker.pakai_luar',
			'history_stiker.hasil_luar',
			'history_stiker.masuk_dalam',
			'history_stiker.pakai_dalam',
			'history_stiker.hasil_dalam',

			'history_kemasan.masuk_kemasan',
			'history_kemasan.terpakai_kemasan',
			'history_kemasan.afkir_kemasan',
			'history_kemasan.stok_now',

			'history_cukai.masuk_cukai',
			'history_cukai.terpakai_cukailama',
			'history_cukai.sisa_cukai',
			'history_cukai.masuk_cukai2',
			'history_cukai.terpakai_cukaibaru',
			'history_cukai.sisa_cukai2',

			'history_cincin.ket_cin'
		)
		->leftJoin('produk','history_cincin.stock_cincin_produk_id_produk','=','produk.id_produk')
		->leftJoin('sub_produk','produk.id_produk','=','sub_produk.produk_id_produk')
		->leftJoin('kemasan','sub_produk.kemasan_id_kemasan','=','kemasan.id_kemasan')

		->leftJoin('history_stiker','history_cincin.id','=','history_stiker.id')
		->leftJoin('history_kemasan','history_cincin.id','=','history_kemasan.id')
		->leftJoin('history_cukai','history_cincin.id','=','history_cukai.id')

		->leftJoin('harga_cincin','produk.id_produk','=','harga_cincin.produk_id_produk')
		->leftJoin('harga_cukai','sub_produk.id_sub_produk','=','harga_cukai.sub_produk_id')
		->leftJoin('harga_stiker','sub_produk.id_sub_produk','=','harga_stiker.sub_produk_id')
		->leftJoin('harga_kemasan','sub_produk.id_sub_produk','=','harga_kemasan.sub_produk_id')
		->groupBy('history_cincin.id')
		->get();

		return Datatables::of($query)
		->addIndexColumn()
		->addColumn('awlstokcincin', function ($query) {
			$awl = $query->sisa - $query->masuk + ($query->terpakai + $query->afkir);
			return $awl;
		})
		->addColumn('awlstockstikerluar', function ($query) {
			$luar = $query->hasil_luar - $query->masuk_luar + $query->pakai_luar;
			return $luar;
		})
		->addColumn('awlstockstikerdalam', function ($query) {
			$dalam = $query->hasil_dalam - $query->masuk_dalam + $query->pakai_dalam;
			return $dalam;
		})
		->addColumn('awlstokkemasan', function ($query) {
			$awl = $query->stok_now - $query->masuk_kemasan + ($query->terpakai_kemasan + $query->afkir_kemasan);
			return $awl;
		})
		->addColumn('stockawalcukai1', function ($query) {
			$awl = $query->sisa_cukai - $query->masuk_cukai + $query->terpakai_cukailama;
			return $awl;
		})
		->addColumn('stockawalcukai2', function ($query) {
			$awl = $query->sisa_cukai2 - $query->masuk_cukai2 + $query->terpakai_cukaibaru;
			return $awl;
		})
		->make(true);
	}

	public function viewnontembakau()
	{
		$data['produk'] = DB::table('produk')->get();
		return view('home.bahan_baku.non_tembakau.data_nontembakau',$data);
	}

	public function showhistoryaksesoris(Request $request)
	{
		$query = DB::table('history_aksesoris')
		->select(DB::raw('
			GROUP_CONCAT(history_aksesoris.id SEPARATOR ",") AS idhisaks,
			history_aksesoris.tanggal_hisaks AS tanggal,
			GROUP_CONCAT(IFNULL(stock_aksesoris.stock_aksesoris,0) SEPARATOR ",") AS stock_aksesoris,
			GROUP_CONCAT(sub_produk_has_aksesoris.id SEPARATOR ",") AS id_aksesoris,
			GROUP_CONCAT(aksesoris.aksesoris SEPARATOR ",") AS nama_aksesoris,

			GROUP_CONCAT((history_aksesoris.hasil_aksesoris - history_aksesoris.masuk_aksesoris + history_aksesoris.terpakai_aksesoris) SEPARATOR ",") AS awal_aksesoris,

			GROUP_CONCAT(history_aksesoris.masuk_aksesoris SEPARATOR ",") AS masuk_aksesoris,
			GROUP_CONCAT(history_aksesoris.terpakai_aksesoris SEPARATOR ",") AS terpakai_aksesoris,
			GROUP_CONCAT(history_aksesoris.hasil_aksesoris SEPARATOR ",") AS hasil_aksesoris
			'))
		->join('stock_aksesoris','history_aksesoris.stock_aksesoris_id','=','stock_aksesoris.id')
		->join('sub_produk_has_aksesoris','stock_aksesoris.sub_has_aks_id','=','sub_produk_has_aksesoris.id')
		->join('aksesoris','sub_produk_has_aksesoris.aksesoris_id','=','aksesoris.id')
		->where('history_aksesoris.history_cincin_id',$request->history_cincin_id)
		->groupBy('history_aksesoris.history_cincin_id')
		->first();	

		return json_encode($query);
	}

	public function add_nontembakau(Request $request)
	{	
		DB::beginTransaction();
		if ($request->idhargacincin != '' && $request->idhargastiker != '' && $request->idhargacukai != '' && $request->idhargakemasan != '') {
			$hasil = $this->backeigni->hasilforaddnontembakau($request);
			$array = $this->backeigni->datainputnontembakau($request,$hasil);

			$query1 = DB::table('history_cincin')->insertGetId($array['data1']);
			$query2 = History_stiker::insert($array['data2']);
			$query3 = History_cukai::insert($array['data3']);
			$query4 = History_kemasan::insert($array['data4']);


			if ($query1 || $query2 || $query3 || $query4) {
				if ($request->idaksesorischeck != NULL) {
					$this->backeigni->hasilforaddaksesoris($request,$query1);
				}
				$alert = 'Stock Non tembakau berhasil ditambahkan!';
			} else {
				$alert = 'Stock Non tembakau gagal ditambahkan!';
			}
		} else {
			$alert = 'Maaf salah satu Harga Non Tembakau ada yang kosong, Mohon cek kembali di Referensi!';
		}
		echo json_encode($alert);
		DB::commit();
	}

	public function updatestocknontembakau($request)
	{
		$this->backeigni->ifuppcincin($request,$request->masuk_cincinhidden,$request->terpakai_cincinhidden,$request->afkir_cincinhidden);
		$this->backeigni->ifuppstiker($request,$request->masuk_stikerlhidden,$request->masuk_stikerdhidden,$request->terpakai_stikerlhidden,$request->terpakai_stikerdhidden);
		$this->backeigni->ifuppkemasan($request,$request->masuk_kemasanhidden,$request->terpakai_kemasanhidden,$request->afkir_kemasanhidden);
		$this->backeigni->ifuppcukai($request,$request->masuk_cukai1hidden,$request->terpakai_cukai1hidden,$request->masuk_cukai2hidden,$request->terpakai_cukai2hidden);

		$kerte['hasilcin'] = $request->stockawalcincin + $request->masuk_cincin - ($request->terpakai_cincin + $request->afkir_cincin);

		$kerte['hasilstil'] = $request->stockawalstikerluar + $request->masuk_stikerl - $request->terpakai_stikerl;
		$kerte['hasilstid'] = $request->stockawalstikerdalam + $request->masuk_stikerd - $request->terpakai_stikerd;

		$kerte['hasilcukai1'] = $request->stockawalcukai1 + $request->masuk_cukai1 - $request->terpakai_cukai1;
		$kerte['hasilcukai2'] = $request->stockawalcukai2 + $request->masuk_cukai2 - $request->terpakai_cukai2;

		$kerte['hasilkemasan'] = $request->stockawalkemasan + $request->masuk_kemasan - ($request->terpakai_kemasan + $request->afkir_kemasan);

		return $kerte;
	}

	public function loopingupdatenontembakau($request,$id)
	{
		$this->backeigni->loop_upcincin($request,$id);
		$this->backeigni->loop_upstiker($request,$id);
		$this->backeigni->loop_upkemasan($request,$id);
		$this->backeigni->loop_upcukai($request,$id);
	}

	public function update_nontembakau(Request $request,$id)
	{
		DB::beginTransaction();
		$fter = $this->updatestocknontembakau($request);
		$gahe = $this->backeigni->datauppjoinnontembakau($request,$fter);

		$query = DB::table('produk')
		->join('sub_produk','produk.id_produk','=','sub_produk.produk_id_produk')
		->join('history_cincin','produk.id_produk','=','history_cincin.stock_cincin_produk_id_produk')
		->join('history_stiker','sub_produk.id_sub_produk','=','history_stiker.stock_stiker_sub_produk_id')
		->join('history_cukai','sub_produk.id_sub_produk','=','history_cukai.stock_cukai_id_sub_produk')
		->join('history_kemasan','sub_produk.id_sub_produk','=','history_kemasan.stock_kemasan_sub_produk_id')
		->where([
			['history_cincin.id','=',$id]
		])
		// echo json_encode($request->all());
		->update($gahe);
		if ($query) {
			$this->loopingupdatenontembakau($request,$id);
			$alert = 'Berhasi Mengupdate Data Non Tembakau ID: '.$id.'!!';
		} else {
			$alert = 'Berhasi Mengupdate Data Non Tembakau ID: '.$id.'!!';
		}
		echo json_encode($alert);
		DB::commit();
	}

	public function update_aksesoris(Request $request,$id)
	{
		DB::beginTransaction();
		$query = DB::table('history_aksesoris')
		->select(
			'history_aksesoris.*',
			'aksesoris.aksesoris')
		->join('sub_produk_has_aksesoris','history_aksesoris.stocksub_has_aks_id','sub_produk_has_aksesoris.id')
		->join('aksesoris','sub_produk_has_aksesoris.aksesoris_id','=','aksesoris.id')
		->where(
			'history_aksesoris.history_cincin_id',$id
		)
		->orderBy('history_aksesoris.stocksub_has_aks_id','ASC');

		if ($query->count() > 0) {
			$get = $query->get();

			foreach ($get as $key) {
				$nameaksaw = 'stockawal_'.$key->aksesoris;	
				$nameaksst = 'stock_'.$key->aksesoris;
				$hasil_akhir = $key->hasil_aksesoris;

				$nameaksin = 'masuk_'.$key->aksesoris;
				$nameakster = 'terpakai_'.$key->aksesoris;

				$nameaksinhid = 'masukhidden_'.$key->aksesoris;
				$nameaksterhid = 'terpakaihidden_'.$key->aksesoris;

				if ($request->input($nameaksin) > $request->input($nameaksinhid)) {
					$kae = $request->input($nameaksin) - $request->input($nameaksinhid);
					$jke = $request->input($nameaksst) + $kae;
				} elseif ($request->input($nameaksin) < $request->input($nameaksinhid)) {
					$kae = $request->input($nameaksinhid) - $request->input($nameaksin);
					$jke = $request->input($nameaksst) - $kae;
				} else {
					$jke = $request->input($nameaksst);
				}

				if ($request->input($nameakster) > $request->input($nameaksterhid)) {
					$anme = $request->input($nameakster) - $request->input($nameaksterhid);
					$bar = $jke - $anme;
				} elseif ($request->input($nameakster) < $request->input($nameaksterhid)) {
					$anme = $request->input($nameaksterhid) - $request->input($nameakster);
					$bar = $jke + $anme;
				} else {
					$bar = $jke;
				}

				$hasilaks = $request->input($nameaksaw) + $request->input($nameaksin) - ( $request->input($nameakster) );

				DB::table('stock_aksesoris')->where([['sub_has_aks_id','=',$key->stocksub_has_aks_id]])->update(['stock_aksesoris' => $bar]);

				$cart = History_aksesoris::find($key->id);
				$cart->masuk_aksesoris = $request->input($nameaksin);
				$cart->terpakai_aksesoris = $request->input($nameakster);
				$cart->hasil_aksesoris = $hasilaks;
				$cart->author_session = Auth::user()->id;
				$cart->log = 'UPDATE';
				$cart->save();
				if (!$cart->save()) {
					$data = false;
					return false;
				} else {
					$this->backeigni->looping_aksesoris($request,$cart->id,$key->stock_aksesoris_id);
					$data = true;
				}
			}
		}
		echo json_encode($data);
		DB::commit();
	}

	public function updatestocknontembakaudel($id)
	{
		$res = DB::table('produk')
		->select(
			'produk.id_produk',
			'sub_produk.id_sub_produk',

			'history_cincin.masuk',
			'history_cincin.terpakai',
			'history_cincin.afkir',
			'history_cincin.sisa',

			'history_stiker.masuk_luar',
			'history_stiker.pakai_luar',
			'history_stiker.hasil_luar',
			'history_stiker.masuk_dalam',
			'history_stiker.pakai_dalam',
			'history_stiker.hasil_dalam',

			'history_kemasan.masuk_kemasan',
			'history_kemasan.terpakai_kemasan',
			'history_kemasan.afkir_kemasan',
			'history_kemasan.stok_now',

			'history_cukai.masuk_cukai',
			'history_cukai.terpakai_cukailama',
			'history_cukai.sisa_cukai',
			'history_cukai.masuk_cukai2',
			'history_cukai.terpakai_cukaibaru',
			'history_cukai.sisa_cukai2',

			'stock_cincin.stock_qty_cincin',
			'stock_cukai.stock_qty_cukai',
			'stock_cukai.stock_qty_cukai2',
			'stock_stiker.stock_luar',
			'stock_stiker.stock_dalam',
			'stock_kemasan.stock_kemasan'
		)
		->join('sub_produk','produk.id_produk','=','sub_produk.produk_id_produk')
		->join('kemasan','sub_produk.kemasan_id_kemasan','=','kemasan.id_kemasan')

		->join('history_cincin','produk.id_produk','=','history_cincin.stock_cincin_produk_id_produk')
		->join('history_stiker','history_cincin.id','=','history_stiker.id')
		->join('history_kemasan','history_cincin.id','=','history_kemasan.id')
		->join('history_cukai','history_cincin.id','=','history_cukai.id')

		->join('stock_cincin','produk.id_produk','=','stock_cincin.produk_id_produk')
		->join('stock_cukai','sub_produk.id_sub_produk','=','stock_cukai.sub_produk_id_sub_produk')
		->join('stock_stiker','sub_produk.id_sub_produk','=','stock_stiker.sub_produk_id')
		->join('stock_kemasan','sub_produk.id_sub_produk','=','stock_kemasan.sub_produk_id')
		->where([
			['history_cincin.id','=',$id]
		])
		->first();


		$dataup['stock_cincin.stock_qty_cincin'] = $res->stock_qty_cincin - $res->masuk + ($res->terpakai + $res->afkir);
		$dataup['stock_stiker.stock_luar'] = $res->stock_luar - $res->masuk_luar + $res->pakai_luar;
		$dataup['stock_stiker.stock_dalam'] = $res->stock_dalam - $res->masuk_dalam + $res->pakai_dalam;
		$dataup['stock_cukai.stock_qty_cukai'] = $res->stock_qty_cukai - $res->masuk_cukai + $res->terpakai_cukailama;
		$dataup['stock_cukai.stock_qty_cukai2'] = $res->stock_qty_cukai2 - $res->masuk_cukai2 + $res->terpakai_cukaibaru;
		$dataup['stock_kemasan.stock_kemasan'] = $res->stock_kemasan - $res->masuk_kemasan + $res->terpakai_kemasan + $res->afkir_kemasan;


		$query = DB::table('produk')
		->join('sub_produk','produk.id_produk','=','sub_produk.produk_id_produk')
		->join('kemasan','sub_produk.kemasan_id_kemasan','=','kemasan.id_kemasan')

		->join('stock_cincin','produk.id_produk','=','stock_cincin.produk_id_produk')
		->join('stock_cukai','sub_produk.id_sub_produk','=','stock_cukai.sub_produk_id_sub_produk')
		->join('stock_stiker','sub_produk.id_sub_produk','=','stock_stiker.sub_produk_id')
		->join('stock_kemasan','sub_produk.id_sub_produk','=','stock_kemasan.sub_produk_id')

		->where([
			['produk.id_produk','=',$res->id_produk],
			['sub_produk.id_sub_produk','=',$res->id_sub_produk]
		])
		->update($dataup);

		$hert = [
			'sisa' => $res->sisa - $res->masuk + ($res->terpakai + $res->afkir),
			'hasil_luar' => $res->hasil_luar - $res->masuk_luar + $res->pakai_luar,
			'hasil_dalam' => $res->hasil_dalam - $res->masuk_dalam + $res->pakai_dalam,
			'stok_now' => $res->stok_now - $res->masuk_kemasan + ($res->terpakai_kemasan + $res->afkir_kemasan),
			'sisa_cukai' => $res->sisa_cukai - $res->masuk_cukai + $res->terpakai_cukailama,
			'sisa_cukai2' => $res->sisa_cukai2 - $res->masuk_cukai2 + $res->terpakai_cukaibaru,
		];
		return $hert;
	}

	public function updatestockakesorisdel($id)
	{	
		$nas = DB::table('history_aksesoris')
		->select(
			'history_aksesoris.*',
			'aksesoris.aksesoris',
			'stock_aksesoris.stock_aksesoris'
		)
		->join('sub_produk_has_aksesoris','history_aksesoris.stocksub_has_aks_id','sub_produk_has_aksesoris.id')
		->join('aksesoris','sub_produk_has_aksesoris.aksesoris_id','=','aksesoris.id')
		->join('stock_aksesoris','sub_produk_has_aksesoris.id','=','stock_aksesoris.sub_has_aks_id')
		->where(
			'history_aksesoris.history_cincin_id',$id
		)
		->orderBy('history_aksesoris.stocksub_has_aks_id','ASC')
		->get();

		$tqds['aksesoris'] = [];
		$tqds['stock_aksesoris_id'] = [];
		foreach ($nas as $fla) {
			$stok = $fla->stock_aksesoris - $fla->masuk_aksesoris  + $fla->terpakai_aksesoris;
			DB::table('stock_aksesoris')->update(['stock_aksesoris' => $stok]);

			$tqds['aksesoris'][$fla->id] = $fla->hasil_aksesoris - $fla->masuk_aksesoris  + $fla->terpakai_aksesoris;
			$tqds['stock_aksesoris_id'][$fla->id] = $fla->stock_aksesoris_id;
		}
		return $tqds;
	}

	public function loopingupdatenontembakaudel($request,$id,$array)
	{
		$this->backeigni->loop_upcincin($request,$id,$array['sisa']);
		$this->backeigni->loop_upstiker($request,$id,$array['hasil_luar'],$array['hasil_dalam']);
		$this->backeigni->loop_upkemasan($request,$id,$array['stok_now']);
		$this->backeigni->loop_upcukai($request,$id,$array['sisa_cukai'],$array['sisa_cukai2']);
		foreach ($array['aksesoris'] as $key => $value) {
			$this->backeigni->looping_aksesoris($request,$key,$array['stock_aksesoris_id'][$key],$value);
		}
	}

	public function delete_aksesoris(Request $request,$id)
	{
		DB::beginTransaction();
		$blame = $this->updatestocknontembakaudel($id);
		$blame += $this->updatestockakesorisdel($id);

		$query = DB::delete('
		DELETE history_aksesoris, history_kemasan, history_cukai, history_stiker, history_cincin
		FROM history_aksesoris
		INNER JOIN  history_stiker ON history_aksesoris.history_cincin_id = history_stiker.id
		INNER JOIN  history_cukai ON history_aksesoris.history_cincin_id = history_cukai.id
		INNER JOIN  history_kemasan ON history_aksesoris.history_cincin_id = history_kemasan.id
		INNER JOIN  history_cincin ON history_aksesoris.history_cincin_id = history_cincin.id
		WHERE history_cincin.id = ?',[$id]);
		if ($query) {
			$this->loopingupdatenontembakaudel($request,$id,$blame);
			$alert = 'Berhasil hapus data';
		} else {
			$alert = 'Gagal hapus data';
		}
		echo json_encode($alert);
		DB::commit();
	}
}
