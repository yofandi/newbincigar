@extends('../../layout')

@section('title')
<title>BIN - ERP : List Notifikasi Packing</title>
@endsection

@section('content')
<div class="row page-titles">
	<div class="col-md-5 align-self-center">
		<h3 class="text-themecolor">Notifikasi</h3>
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a href="javascript:void(0)">Notifikasi</a></li>
			<li class="breadcrumb-item active">List Notifikasi Packing</li>
		</ol>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="row">
			<div class="col-md-12">
				<legend>List Notifikasi Packing</legend>
			</div>
			<div class="col-md-12">
				<div class="table-responsive m-t-40">
					<table id="table-list" class="display nowrap table table-hover table-striped table-bordered">
						<thead>
							<tr>
								<th>No.</th>
								<th>Tanggal</th>
								<th>Asal</th>
								<th>Keterangan</th>
								<th>Status</th>
								<th>Aksi</th>
							</tr>
						</thead>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('javascript')
<script src="{{ asset('node_modules/datatables/datatables.min.js') }}"></script>
<script>
	var table = $('#table-list').DataTable({
		"lengthMenu": [
		[3,5,10, 25, 50, 100, 150, 200, -1],
		[3,5,10, 25, 50, 100, 150, 200, "All"]
		],
		"order": [
		[0, 'desc']
		],
		processing: true,
		serverSide: true,
		ajax: '/api/noc_inrfs/json',
		columns: [
		{ data: 'DT_RowIndex', name: 'DT_RowIndex' },
		{ data: 'tanggal_inrfs', name: 'tanggal_inrfs' },
		{ data: 'asal', name: 'asal' },
		{ data: 'keterangan', name: 'keterangan' },
		{
			data: null,
			"bSortable": false,
			mRender: function (o) { 
				if (o.status_inrfs == 0) {
					$color = 'light';
				} else if (o.status_inrfs == 1) {
					$color = 'info';
				} else if (o.status_inrfs == 2) {
					$color = 'danger';
				} else if (o.status_inrfs == 3) {
					$color = 'warning';
				} else if (o.status_inrfs == 4) {
					$color = 'primary';
				}
				return '<span class="badge badge-'+ $color +'">'+ o.stlihat +'</span>'; 
			}
		},
		{
			data: null,
			"bSortable": false,
			mRender: function (o) { 
				return '<a href="/noc_isintd/'+ o.id +'" class="btn btn-rounded btn-sm btn-primary">Show</a>'; 
			}
		},
		],
		"displayLength": 10,
	});
	$('#table-list tbody').on('click', 'tr.group', function() {
		var currentOrder = table.order()[0];
		if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
			table.order([2, 'desc']).draw();
		} else {
			table.order([2, 'asc']).draw();
		}
	});
	$('#table-list tbody').on('click', 'tr.group', function() {
		var currentOrder = table.order()[0];
		if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
			table.order([2, 'desc']).draw();
		} else {
			table.order([2, 'asc']).draw();
		}
	});
</script>
@endsection