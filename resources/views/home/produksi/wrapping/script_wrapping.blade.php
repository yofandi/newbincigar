<script>
	$('#stock_showitwrap').hide();
	$('#loadingdeletewrap').hide();
	$('.spinner_nekrwrap').hide();

	$.extend(true, $.fn.datetimepicker.defaults, {
		icons: {
			time: 'far fa-clock',
			date: 'far fa-calendar',
			up: 'fas fa-arrow-up',
			down: 'fas fa-arrow-down',
			previous: 'fas fa-chevron-left',
			next: 'fas fa-chevron-right',
			today: 'fas fa-calendar-check',
			clear: 'far fa-trash-alt',
			close: 'far fa-times-circle'
		}
	});

	var tablewrap = $('#table-wrapping').DataTable({
		"lengthMenu": [
		[3,5,10, 25, 50, 100, 150, 200, -1],
		[3,5,10, 25, 50, 100, 150, 200, "All"]
		],
		"order": [
		[0, 'desc']
		],
		processing: true,
		serverSide: true,
		ajax: '/datawrapping/json',
		columns: [
		{ data: 'DT_RowIndex', name: 'DT_RowIndex' },
		{ data: 'tanggal_wrap', name: 'tanggal_wrap' },
		{ data: 'produk', name: 'produk' },
		{ data: 'jenis', name: 'jenis' },
		{ data: 'stockawal', name: 'stockawal' },
		{ data: 'weiswmasuk', name: 'weiswmasuk' },
		{ data: 'terpakaiprow', name: 'terpakaiprow' },
		{ data: 'terpakaiweisw', name: 'terpakaiweisw' },
		{ data: 'sisaprow', name: 'sisaprow' },
		{ data: 'awlbatang', name: 'awlbatang' },
		{ data: 'tambah_wrap', name: 'tambah_wrap' },
		{ data: 'batangreject', name: 'batangreject' },
		{ data: 'keterangan_wrap', name: 'keterangan_wrap' },
		{
			data: null,
			"bSortable": false,
			mRender: function (o) { 
				$button = '<button type="button" class="btn btn-info btn-rounded btn-xs" id="update-wrapping"';
				$button += ' data-title="Update Wrapping | ID: '+ o.id +'"';
				$button += ' data-link="/datawrapping/upp/'+ o.id +'/'+ o.weiswmasuk +'/'+ o.terpakaiprow +'/'+ o.terpakaiweisw +'/'+ o.batangreject +'/'+ o.reject_filling +'/'+ o.reject_binding +'/'+ o.tambah_wrap +'"';
				$button += ' data-tanggal_wrapping="'+ o.tanggal_wrap +'"';
				$button += ' data-jenis="'+ o.id_jenis +'"';
				$button += ' data-produk="'+ o.id_produk +'"';
				$button += ' data-stockpro="'+ o.stockawal +'"';
				$button += ' data-stockweis="'+ o.stockawalweis +'"';
				$button += ' data-stockbatang="'+ o.awlbatang +'"';
				$button += ' data-masukweis="'+ o.weiswmasuk +'"';
				$button += ' data-terpakaiprow="'+ o.terpakaiprow +'"';
				$button += ' data-terpakaiweisw="'+ o.terpakaiweisw +'"';

				$button += ' data-berat_jadi="'+ o.berat_jadi +'"';
				$button += ' data-batangreject="'+ o.batangreject +'"';
				$button += ' data-reject_filling="'+ o.reject_filling +'"';
				$button += ' data-reject_binding="'+ o.reject_binding +'"';

				$button += ' data-batang="'+ o.tambah_wrap +'"';
				$button += ' data-idprobaku="'+ o.stock_probaku_id +'"';
				$button += ' data-idpressing="'+ o.stock_pressing_id +'"';
				$button += ' data-idwrapping="'+ o.stock_wrapping_id +'"';
				$button += ' data-ket="'+ o.keterangan_wrap +'">';
				$button += '<i class="mdi mdi-dots-horizontal"></i></button>';
				$button += '<button type="button" class="btn btn-danger btn-rounded btn-xs" id="delete-wrapping"';
				$button += ' data-link="/datawrapping/del/'+ o.id +'/'+ o.weiswmasuk +'/'+ o.terpakaiprow +'/'+ o.terpakaiweisw +'/'+ o.batangreject +'/'+ o.reject_filling +'/'+ o.reject_binding +'/'+ o.tambah_wrap +'"';
				$button += ' data-stockbatang="'+ o.awlbatang +'"';
				$button += ' data-jenis="'+ o.id_jenis +'"';
				$button += ' data-reject_filling="'+ o.reject_filling +'"';
				$button += ' data-reject_binding="'+ o.reject_binding +'"';
				$button += ' data-idprobaku="'+ o.stock_probaku_id +'"';
				$button += ' data-idpressing="'+ o.stock_pressing_id +'"';
				$button += ' data-idwrapping="'+ o.stock_wrapping_id +'">';
				$button += '<i class="fa fa-trash"></i></button>';
				return $button; }
			},
			],
			"displayLength": 10,
		});
	$('#table-wrapping tbody').on('click', 'tr.group', function() {
		var currentOrder = tablewrap.order()[0];
		if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
			tablewrap.order([2, 'desc']).draw();
		} else {
			tablewrap.order([2, 'asc']).draw();
		}
	});
	$('#table-wrapping tbody').on('click', 'tr.group', function() {
		var currentOrder = tablewrap.order()[0];
		if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
			tablewrap.order([2, 'desc']).draw();
		} else {
			tablewrap.order([2, 'asc']).draw();
		}
	});

	function Refresh_wrapping() {
		$('#table-wrapping').DataTable().ajax.reload();
	}

	function getdatasearchwrap(position1,position2,position3) {
		let jenis = $('#jenis_inwrap').val();
		let produk = $('#produk_inwrap').val();

		$.ajax({
			url: '/datawrapping/getsearchfill',
			type: 'POST',
			dataType: 'json',
			data: {
				"_token": token,
				produk: produk,
				jenis: jenis
			},
		})
		.done(function(data) {
			$('#idpressingwrap').val(data.idpressing);
			$('#idwrapping').val(data.idwrapping);
			$('#idprobakuwrap').val(data.id);
			$('#berat_batang').val(data.berat_jadi);
			$('#' + position1).val(data.jml_produksi);
			$('#' + position2).val(data.stock_weiswrap);
			$('#' + position3).val(data.jml_pres);
		});
		
	}

	function seacrh_datawrap() {
		getdatasearchwrap('stockprowrap','stockweisw','batangpres');
	}

	function save_methodwrap() {
		let link = $('#form-wrapping').attr('action');
		let idprobaku = $('#idprobakuwrap').val();
		let idwrapping = $('#idwrapping').val();
		let idpressing = $('#idpressingwrap').val();
		let idjnpro = $('#idjnprowrap').val();
		let tanggal_wrapping = $('#tanggal_wrapping').val();
		let jenis_in = $('#jenis_inwrap').val();
		let produk_in = $('#produk_inwrap').val();
		let stockpro = $('#stockprowrap').val();
		let stockweisw = $('#stockweisw').val();
		let stockbatang = $('#batangpres').val();
		let masukweisw = $('#masukweisw').val();
		let terpakaipro = $('#terpakaiprowrap').val();
		let terpakaiweisw = $('#terpakaiweisw').val();
		let batangreject = $('#batangreject').val();
		let reject_filling = $('#reject_filling').val();
		let reject_binding = $('#reject_binding').val();
		let batangjml = $('#batangjmlwrap').val();
		let ket = $('#ket').val();
		$.ajax({
			url: link,
			type: 'POST',
			dataType: 'json',
			data: {
				"_token": token,
				tanggal_wrapping: tanggal_wrapping,
				jenis: jenis_in,
				produk: produk_in,
				stockpro: stockpro,
				stockweisw: stockweisw,
				stockbatang: stockbatang,
				masukweisw:masukweisw,
				terpakaipro: terpakaipro,
				terpakaiweisw: terpakaiweisw,
				batangreject: batangreject,
				reject_filling: reject_filling,
				reject_binding: reject_binding,
				batangjml: batangjml,
				idprobaku: idprobaku,
				idpressing: idpressing,
				idwrapping: idwrapping,
				idjnpro:idjnpro,
				ket:ket
			},
			beforeSend: function() {
				isProcessing = true;
				$('.spinner_nekrwrap').fadeIn();
			},
			success: function(data) {
				$('.spinner_nekrwrap').hide();
				isProcessing = false;
				alert(data);
				Refresh_wrapping();
				$('#modal-wrapping').modal('toggle');
			}
		});
	}

	$(document).on('click', '#tambah-wrapping', function(event) {
		let clik = $(this);
		$('.modal-header #title').text('Tambah Wrapping');
		$('#form-wrapping').attr('action', clik.data('link'));
		$('#form-wrapping').find('input').val('');
		$('#form-wrapping').find('select').val('');
		$('#form-wrapping').find('textarea').val('');
		$("#tanggal_wrapping").datetimepicker({
			format: 'YYYY-MM-DD HH:mm:ss',
		}).data("DateTimePicker").date(new Date());
		$('#stock_showitwrap').fadeIn();
		$('#jeniswrap').removeAttr('disabled');
		$('#produkwrap').removeAttr('disabled');
		ljenis('jeniswrap');
		$(document).on('change', '#jeniswrap', function(event) {
			let jam = $(this).val();
			$('#jenis_inwrap').val(jam);
			lproduk('produkwrap',jam);
			$(document).on('change', '#produkwrap', function(event) {
				let pwe = $(this).val();
				$('#produk_inwrap').val(pwe);
				let kk = $(this).find(':selected').data('id');
				$('#idjnprowrap').val(kk);
			});
		});

		$(document).on('keyup', '#batangreject', function(event) {
			$jke = $(this).val();
			$nr = $('#berat_batang').val();
			$jkemev = $jke * $nr;
			$hhjr = $jkemev * 2;

			$('#reject_binding').val($jkemev);
			$('#reject_filling').val($hhjr);
		});
		$('#modal-wrapping').modal('show');
	});

	$(document).on('click', '#update-wrapping', function(event) {
		let click = $(this);
		$('.modal-header #title').text(click.data('title'));
		$('#form-wrapping').attr('action', click.data('link'));
		$('#form-wrapping').find('input').val('');
		$('#form-wrapping').find('select').val('');
		$('#form-wrapping').find('textarea').val('');
		$('#stock_showitwrap').hide();
		$('#jeniswrap').attr('disabled','disabled');
		$('#produkwrap').attr('disabled','disabled');
		ljenis('jeniswrap',click.data('jenis'));
		lproduk('produkwrap',click.data('jenis'),click.data('produk'));

		$("#tanggal_wrapping").datetimepicker({
			format: 'YYYY-MM-DD HH:mm:ss',
		}).data("DateTimePicker").date(click.data('tanggal_wrapping'));
		$('#jenis_inwrap').val(click.data('jenis'));
		$('#produk_inwrap').val(click.data('produk'));
		$('#stockprowrap').val(click.data('stockpro'));
		$('#stockweisw').val(click.data('stockweis'));
		$('#batangpres').val(click.data('stockbatang'));
		$('#berat_batang').val(click.data('berat_jadi'));
		$('#masukweisw').val(click.data('masukweis'));
		$('#terpakaiprowrap').val(click.data('terpakaiprow'));
		$('#terpakaiweisw').val(click.data('terpakaiweisw'));
		$('#batangreject').val(click.data('batangreject'));
		$('#reject_filling').val(click.data('reject_filling'));
		$('#reject_binding').val(click.data('reject_binding'));
		$('#batangjmlwrap').val(click.data('batang'));
		$('#idprobakuwrap').val(click.data('idprobaku'));
		$('#idwrapping').val(click.data('idwrapping'));
		$('#idpressingwrap').val(click.data('idpressing'));
		$('#ket').val(click.data('ket'));

		$(document).on('keyup', '#batangreject', function(event) {
			$jke = $(this).val();
			$nr = click.data('berat_jadi');
			$jkemev = $jke * $nr;
			$hhjr = $jkemev * 2;

			$('#reject_binding').val($jkemev);
			$('#reject_filling').val($hhjr);
		});
		$('#modal-wrapping').modal('show');
	});

	$(document).on('click', '#delete-wrapping', function(event) {
		let sbse = confirm('Apakah anda yakin ingin menghapus data ini ?');
		if (sbse) {
			let link = $(this).data("link");
			let jenis = $(this).data('jenis');
			let stockbatang = $(this).data('stockbatang');
			let idprobaku = $(this).data('idprobaku');
			let idpressing = $(this).data('idpressing');
			let idwrapping = $(this).data('idwrapping');
			$.ajax(
			{
				url: link,
				type: 'DELETE',
				data: {
					"_token": token,
					jenis: jenis,
					stockbatang: stockbatang,
					idprobaku: idprobaku,
					idpressing: idpressing,
					idwrapping: idwrapping,
				},
				beforeSend: function() {
					isProcessing = true;
					$('#loadingdeletewrap').fadeIn();
				},
				success: function (data){
					$('#loadingdeletewrap').hide();
					isProcessing = false;
					alert(data);
					Refresh_wrapping();
				}
			});
		}
	});
</script>