@extends('../../layout')

@section('title')
<title>BIN - ERP : Data Packing</title>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css" />
@endsection

@section('content')
<div class="row page-titles">
	<div class="col-md-5 align-self-center">
		<h3 class="text-themecolor">Packing</h3>
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a href="javascript:void(0)">Proses</a></li>
			<li class="breadcrumb-item active">Data Packing</li>
		</ol>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="row">
			<div class="col-md-3">
				<legend>Data Packing</legend>
			</div>
			<div class="col-md-2 offset-md-7">
				<button type="button" class="btn btn-success btn-rounded btn-icons" data-link="{{ route('packing.post') }}" id="tambah-packing">Tambah</button>
				<button type="button" onclick="Refresh_packing()" class="btn btn-rounded btn-icons btn-primary">Refresh</button>
				<button type="button" onclick="change_position()" class="btn btn-secondary btn-rounded btn-icons">Kirim RFS</button>
			</div>
			<div id="v_blend2" class="col-md-12">
				<br>
				<div class="row">
					<form class="col-md-6" id="noc_inrfs" method="POST">
						<div class="row">
							<div class="col-md-12" align="center">
								<legend>Kirim Ke RFS</legend>
							</div>
							<div class="form-group col-md-12 fa-3x spinner_nekraddrfs" align="center">
								<i class="fas fa-spinner fa-spin"></i>
							</div>
							<div class="form-group col-md-5">
								<label class="control-label">Produk</label>
								<select class="form-control" id="produk_inrfs"></select>
								<input type="hidden" id="produk_hidinrfs">
							</div>
							<div class="form-group col-md-5">
								<label class="control-label">Sub Produk</label>
								<select class="form-control" id="subproduk_inrfs"></select>
								<input type="hidden" id="subproduk_hidinrfs">
								<input type="hidden" id="namesubproduk_hid">
							</div>
							<div class="form-group col-md-2">
								<input type="hidden" id="idpackinginrfs"><br>
								<button type="button" onclick="showforinrfs()" class="btn btn-light btn-md btn-block">Cari</button>
							</div>
							<div class="form-group col-md-12">
								<label class="control-label">Stock Packing</label>
								<input type="number" class="form-control" id="stockjmlpack_for" placeholder="Stock Packing" readonly>
							</div>
							<div class="form-group col-md-12">
								<label class="control-label">JML Keluar</label>
								<input type="number" class="form-control" id="jml_keluar" placeholder="JML Keluar">
							</div>
							<div class="form-group col-md-12">
								<button type="button" onclick="save_sessionker()" class="btn btn-warning btn-sm btn-block">add</button>
							</div>
						</div>
					</form>
					<div class="col-md-6">
						<legend>Daftar Item</legend>
						<div class="" align="center" id="loadingdeletecart">
							<h5>Data tengah diproses. Mohon tunggu sejenak!</h5>
						</div>
						<div class="table-responsive">
							<table class="table table-striped">
								<thead class="thead-dark">
									<tr>
										<th>No.</th>
										<th>Name</th>
										<th>JML</th>
										<th>#</th>
									</tr>
								</thead>
								<tbody id="data_cart">
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="form-group col-md-12">
						<label class="control-label">Tanggal</label>
						<input type="text" id="tanggal_inrfs" class="form-control">
					</div>
					<div class="form-group col-md-12">
						<label class="control-label">Keterangan</label>
						<textarea class="form-control" id="keterangan" placeholder="Keterangan"></textarea>
					</div>
				</div>
				<div class="row">
					<button type="button" onclick="save_inrfs()" class="btn btn-info btn-md btn-block">Send</button>
				</div>
			</div>
			<div class="col-md-12" align="center" id="loadingdeletepack">
				<h5>Data tengah diproses. Mohon tunggu sejenak!</h5>
			</div>
			<div class="col-md-12">
				<div class="table-responsive m-t-40">
					<table id="table-packing" class="display nowrap table table-hover table-striped table-bordered">
						<thead>
							<tr align="center">
								<th>No.</th>
								<th>Tanggal</th>
								<th>Kode Produk</th>
								<th>Sub Produk</th>
								<th>Kemasan</th>
								<th>Isi</th>
								<th>Masuk</th>
								<th>Stock Hi</th>
								<th>Ket</th>
								<th>Aksi</th>
							</tr>
						</thead>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal fade bs-example-modal-lg" id="modal-packing" tabindex="-1" role="dialog" aria-labelledby="title-tembakau" aria-hidden="true" style="display: none;">
	<div class="modal-dialog modal-xl">
		<div class="modal-content">
			<div class="card">
				<div class="card-header">
					<div class="modal-header">
						<h4 class="modal-title" id="title"></h4>
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					</div>
				</div>
				<div class="card-body">
					<form id="form-packing" method="POST">
						<div class="modal-body row">
							<div class="form-group col-md-12 fa-3x spinner_nekrpack" align="center">
								<i class="fas fa-spinner fa-spin"></i>
							</div>
							<div class="form-group col-md-12">
								<label class="control-label">Tanggal</label>
								<input type="text" class="form-control date_sam" id="tanggal_packing">
							</div>
							<div class="form-group col-md-6">
								<label class="control-label">Produk</label>
								<select class="form-control" id="produk"></select>
								<input type="hidden" id="produk_in">
							</div>
							<div class="form-group col-md-6">
								<label class="control-label">Sub Produk</label>
								<select class="form-control" id="subproduk"></select>
								<input type="hidden" id="subproduk_in">
							</div>
							<div class="form-group col-md-12" id="stock_showit">
								<button type="button" onclick="seacrh_data()" class="btn btn-primary btn-xs btn-block">Search</button>
							</div>
							<div class="col-md-12">
								@include('home.produksi.packing.singleproduk')
								@include('home.produksi.packing.eklusifcontent')
								@include('home.produksi.packing.boscontent')
								@include('home.produksi.packing.havanocontent')
							</div>
						</div>

						<div class="modal-footer">
							<button type="button" onclick="save_method()" class="btn btn-primary waves-effect text-left">Save</button>
							<button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Close</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="modal-cartpack" tabindex="-1" role="dialog" aria-labelledby="title-cart" aria-hidden="true" style="display: none;">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<div class="card">
				<div class="card-header">
					<div class="modal-header">
						<h4 class="modal-title" id="title-cart"></h4>
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					</div>
				</div>
				<div class="card-body">
					<form id="link_updatecart">
						<div class="modal-body row">
							<div class="form-group col-md-12 fa-3x spinner_nekrcart" align="center">
								<i class="fas fa-spinner fa-spin"></i>
							</div>
							<div class="form-group col-md-12">
								<label class="control-label">Qty</label>
								<input type="number" class="form-control" id="qtycart" placeholder="Qty">
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" onclick="update_sessionker()" class="btn btn-info waves-effect text-left">Save</button>
							<button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Close</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>	
</div>
@endsection

@section('javascript')
<script src="{{ asset('node_modules/moment/moment.js') }}">
</script><script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
<script src="{{ asset('node_modules/datatables/datatables.min.js') }}"></script>
@include('home.produksi.packing.script_packing')
@endsection