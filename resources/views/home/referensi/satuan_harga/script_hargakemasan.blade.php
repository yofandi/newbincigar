<script>
	var input5 = document.getElementById('harga_satkemasan');
	input5.addEventListener('keyup', function(e)
	{
		input5.value = format_number(this.value, '');
	}
	);

	function Refresh_kemasan() {
		$('#table-hargakemasan').DataTable().ajax.reload();
	}

	var tablekemasan = $('#table-hargakemasan').DataTable({
		"lengthMenu": [
		[3,5,10, 25, 50, 100, 150, 200, -1],
		[3,5,10, 25, 50, 100, 150, 200, "All"]
		],
		"order": [
		[0, 'desc']
		],
		processing: true,
		serverSide: true,
		ajax: '/api/hargakemasan/json',
		columns: [
		{ data: 'DT_RowIndex', name: 'DT_RowIndex' },
		{ data: 'tanggal_harga', name: 'tanggal_harga' },
		{ data: 'kode_hkemasan', name: 'kode_hkemasan' },
		{ data: 'produk', name: 'produk' },
		{ data: 'kodesubproduk', name: 'kodesubproduk' },
		{ data: "harga_kemasan_for", name: 'harga_kemasan_for' },
		{ data: 'keterangan_hargakem', name: 'keterangan_hargakem' },
		{	
			data: null,
			"bSortable": false,
			mRender: function(o) {
				$bind = '<button type="button" class="btn btn-xs btn-rounded btn-warning update-hargakemasan" data-title="Update Harga Cukai | ID '+ o.id +'" data-link="/api/hargakemasan/update/'+ o.id +'" data-tanggal_kemasan="'+ o.tanggal_harga +'" data-produk="'+ o.id_produk +'" data-kode="'+ o.kode_hkemasan +'" data-subproduk="'+ o.sub_produk_id +'" data-harga="'+ format_number(o.harga, '') +'" data-ket="'+ o.keterangan_hargakem +'" data-count="'+ o.countdata +'"><i class="mdi mdi-dots-horizontal"></i></button>';
				$bind += '<button type="button" class="btn btn-danger btn-rounded btn-xs" id="delete-hargakemasan" data-id="'+ o.id +'" data-count="'+ o.countdata +'"><i class="fa fa-trash"></i></button>';
				return $bind; 
			}
		},
		],
		"displayLength": 10,
	});
	$('#table-hargakemasan tbody').on('click', 'tr.group', function() {
		var currentOrder = tablekemasan.order()[0];
		if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
			tablekemasan.order([2, 'desc']).draw();
		} else {
			table.order([2, 'asc']).draw();
		}
	});
	$('#table-hargakemasan tbody').on('click', 'tr.group', function() {
		var currentOrder = tablekemasan.order()[0];
		if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
			tablekemasan.order([2, 'desc']).draw();
		} else {
			tablekemasan.order([2, 'asc']).draw();
		}
	});

	function save_methodkemasan() {
		let link = $('#form-hargakemasan').attr('action');
		let tanggal_kemasan = $('#tanggal_kemasan').val();
		let kode_hkemasan = $('#kode_hkemasan').val();
		let produk = $('#produk-kemasan').val();
		let subproduk = $('#subproduk-kemasan').val();
		let harga_satuan = $('#harga_satkemasan').val();
		let keterangan = $('#keterangan-kemasan').val();
		$.ajax({
			url: link,
			type: 'POST',
			dataType: 'json',
			data: {
				"_token": token,
				tanggal_kemasan: tanggal_kemasan,
				kode_hkemasan: kode_hkemasan,
				produk: produk,
				subproduk: subproduk,
				harga_satuan: harga_satuan,
				keterangan: keterangan,
			},
			beforeSend: function() {
				isProcessing = true;
				$('.spinner_nekrkemasan').fadeIn();
			},
			success: function(data) {
				$('.spinner_nekrkemasan').hide();
				isProcessing = false;
				alert(data);
				Refresh_kemasan();
				$('#modal-hargakemasan').modal('toggle');
			}
		});
	}

	$(document).on('click', '#tambah-hargakemasan', function(event) {
		let click = $(this);
		$('.modal-header #title-hargakemasan').text('Tambah Harga Kemasan');
		$('#form-hargakemasan').find('input').val('');
		$('#form-hargakemasan').find('select').val('');
		$('#form-hargakemasan').find('textarea').val('');
		$('#form-hargakemasan').attr('action', click.data('link'));
		lproduk('produk-kemasan');
		$('#produk-kemasan').change(function(event) {
			let valpro = $(this).val();
			lsubpro('subproduk-kemasan',valpro);
		});
		$("#tanggal_kemasan").datetimepicker({
			format: 'YYYY-MM-DD HH:mm:ss',
		}).data("DateTimePicker").date(new Date());
		$('#modal-hargakemasan').modal('show');
	});

	$(document).on('click', '.update-hargakemasan', function(event) {
		let click = $(this);
		$('.modal-header #title-hargakemasan').text(click.data('title'));
		$('#form-hargakemasan').find('input').val('');
		$('#form-hargakemasan').find('select').val('');
		$('#form-hargakemasan').find('textarea').val('');
		$('#form-hargakemasan').attr('action', click.data('link'));
		lproduk('produk-kemasan',click.data('produk'));
		lsubpro('subproduk-kemasan',click.data('produk'),click.data('subproduk'));
		$('#harga_satkemasan').val(click.data('harga'));
		$('#keterangan-kemasan').val(click.data('ket'));
		$('#kode_hkemasan').val(click.data('kode'));
		$("#tanggal_kemasan").datetimepicker({
			format: 'YYYY-MM-DD HH:mm:ss',
		}).data("DateTimePicker").date(click.data('tanggal_kemasan'));
		$('#modal-hargakemasan').modal('show');
	});
	
	$(document).on('click', '#delete-hargakemasan', function(event) {
		let dje = confirm('Apakah anda yakin ingin menghapus data ini ?');
		if (dje) {
			if ($(this).data("count") > 0) {
				alert('Maaf.. Anda tidak bisa menghapus data ini, dikarenakan akan menimbulkan error pada aplikasi ini!!');
			} else {

				let id = $(this).data("id");
				$.ajax(
				{
					url: '/api/hargakemasan/delete/' + id,
					type: 'DELETE',
					data: {
						"_token": token,
					},
					beforeSend: function() {
						isProcessing = true;
						$('#loadingdeletekemasan').fadeIn();
					},
					success: function (data){
						$('#loadingdeletekemasan').hide();
						alert(data);
						Refresh_kemasan();
						isProcessing = false;
					}
				});
			}
		}
	});
</script>