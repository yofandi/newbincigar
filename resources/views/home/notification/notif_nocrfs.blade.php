@extends('../../layout')

@section('title')
<title>BIN - ERP : Notifikasi Detail</title>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css" />
@endsection

@section('content')
<div class="row page-titles">
	<div class="col-md-5 align-self-center">
		<h3 class="text-themecolor">Notifikasi - ID: {{ $isi->id }}</h3>
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a href="javascript:void(0)">Notifikasi</a></li>
			<li class="breadcrumb-item"><a href="{{ route('listtable') }}">List Notifikasi Packing</a></li>
			<li class="breadcrumb-item active"><a href="javascript:void(0)">Notifikasi Detail</a></li>
		</ol>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-body">
				<div class="col-md-12" align="right">
					@if (in_array($isi->status_inrfs, [0,2]) && $isi->arah_dari == 1 || in_array($isi->status_inrfs, [2]) && $isi->arah_dari == 0)
					<button type="button" id="optklick" class="btn btn-success btn-rounded btn-icons"><i class="fa fa-plus" aria-hidden="true"></i></button>
					@endif
				</div>
				<div class="col-md-12" align="center" id="loadingprosess">
					<h5>Data tengah diproses. Mohon tunggu sejenak!</h5>
				</div>
				<div class="col-md-12" id="tambahshow">
					<form id="form-tambahsub">
						<div class="row">
							<div class="col-md-12" align="center">
								<legend>Tambah Sub Produk</legend>
							</div>
							<div class="form-group col-md-6">
								<label class="control-label">Produk</label>
								<select class="form-control" id="produk_in"></select>
							</div>
							<div class="form-group col-md-6">
								<label class="control-label">Sub Produk</label>
								<select class="form-control" id="subproduk_in"></select>
							</div>
							<div class="form-group col-md-12">
								<button type="button" id="cari-data" class="btn btn-primary btn-xs btn-block">Cari</button>
							</div>
							<div class="form-group col-md-6">
								<label class="control-label">Jumlah Stock</label>
								<input type="number" class="form-control" id="stockjmlrfs" placeholder="Jumlah Stock" readonly>
							</div>
							<div class="form-group col-md-6">
								<label class="control-label">Jumlah reject</label>
								<input type="number" class="form-control" id="jml_keluar" placeholder="JML reject">
							</div>
							<div class="form-group col-md-12">
								<button type="button" id="tambah_itemsobject" class="btn btn-warning btn-sm btn-block">Tambah</button>
							</div>
						</div>
					</form>
				</div>
				<div class="row">
					<div id="tbale1">
						<legend align="center">
							@switch($isi->status_inrfs)
							@case(0)
							@if ($isi->arah_dari == 0)
							Daftar Barang Check
							@else
							Daftar Barang Reject
							@endif
							@break
							@case(1)
							Daftar Barang Diterima
							@break
							@case(2)
							Daftar Barang Ditolak
							@break
							@case(3)
							@case(0)
							@if ($isi->arah_dari == 0)
							Daftar Barang Masuk
							@else
							Daftar Barang Reject dari RFS
							@endif
							@break
							@endswitch
						</legend><br>
						<table class="table">
							<thead>
								<tr class="table-primary">
									<th>No.</th>
									<th>Name</th>
									<th>Quantity</th>
									<th class="hidden">#</th>
								</tr>
							</thead>
							<tbody id="daftarrfsmasuk">
							</tbody>
						</table>
					</div>
					<div id="tbale2">
						<legend align="center">Reject</legend><br>
						<table class="table">
							<thead>
								<tr class="table-active">
									<th>No.</th>
									<th>Name</th>
									<th>Quantity</th>
									<th>#</th>
								</tr>
							</thead>
							<tbody id="daftarreject">
							</tbody>
						</table>
					</div>
				</div>
				<form id="formsub" method="POST">
					@csrf
					<div class="form-group row">
						<div id="ffger11">
							<input type="hidden" name="arah_dari" value="{{ $isi->arah_dari }}">

							<label class="control-label">Tanggal Notifikasi : </label>
							<input type="text" id="tanggal_noc" name="tanggal_noc" class="form-control" value="{{ $isi->tanggal_inrfs }}">
						</div>
						<div id="ffger21">
							<label class="control-label">Tanggal Reject : </label>
							<input type="text" class="form-control" id="tanggal_rej" name="tanggal" placeholder="Tanggal Reject" value="{{ date('Y-m-d h:i:s') }}">
						</div>
					</div>
					<div class="form-group row">
						<div id="ffger12">
							<label class="control-label">Keterangan Notifikasi :</label>
							<textarea class="form-control" name="ket" placeholder="Keterangan Notifikasi" @if (Auth::user()->level_user_id_level != 4)
								readonly="readonly" 
								@endif>{{ $isi->keterangan }}</textarea>
							</div>
							<div id="ffger22">
								<label class="control-label">Keterangan Reject :</label>
								<textarea class="form-control" name="ket_rej"  placeholder="Keterangan Reject"></textarea>
							</div>
							<div class="col-md-12">
								<label class="control-label">Status :</label>
								<input type="hidden" id="status_inrfs_in" name="status_inrfs_in" value="{{ $isi->status_inrfs }}">

								<select class="form-control" id="status" name="status_inrfs" @if(in_array($isi->status_inrfs,[3]) && $isi->arah_dari == 0) disabled @endif>
									@foreach($option as $r => $p)
									<option value="{{ $r }}"
									@if($r == $isi->status_inrfs) selected @endif
									>{{ $p }}</option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="form-group row">
							<div class="form-group col-md-12">
								<label class="control-label">Asal :</label>
								<input type="text" class="form-control" value="{{ $isi->asal }}" readonly>
							</div>
						</div>
						@if (in_array(Auth::user()->level_user_id_level, [1,3,4]))
						<div class="form-group row">
							<div class="form-group col-md-12">
								<label class="control-label">Pesan Packing:</label>
								<textarea class="form-control" name="pesan_packing" placeholder="Pesan Packing">{{ $isi->pesan_packing }}</textarea>
							</div>
						</div>
						@endif
						@if (in_array(Auth::user()->level_user_id_level, [1,5]))
						<div class="form-group row">
							<div class="form-group col-md-12">
								<label class="control-label">Pesan Quality Control :</label>
								<textarea class="form-control" name="pesan_qc" placeholder="Pesan Quality Control">{{ $isi->pesan_qc }}</textarea>
							</div>
						</div>
						@endif
						@if (in_array(Auth::user()->level_user_id_level, [1,6]))
						<div class="form-group row">
							<div class="form-group col-md-12">
								<label class="control-label">Pesan Pengiriman:</label>
								<textarea class="form-control" name="pesan_rfs" placeholder="Pesan Pengiriman">{{ $isi->pesan_rfs }}</textarea>
							</div>
						</div>
						@endif
						<div class="form-group row">
							<button type="button" onclick="formsubmit()" class="btn btn-primary btn-block" @if(in_array($isi->status_inrfs,[3]) && $isi->arah_dari == 0 || in_array($isi->status_inrfs,[3]) && $isi->arah_dari == 1) disabled @endif>Execute</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade" id="modal-urai" tabindex="-1" role="dialog" aria-labelledby="title-uraian" aria-hidden="true" style="display: none;">
		<div class="modal-dialog modal-xl">
			<div class="modal-content">
				<div class="card">
					<div class="card-header">
						<div class="modal-header">
							<h4 class="modal-title" id="title-uraian"></h4>
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
						</div>
					</div>
					<div class="card-body">
						<form id="form-uraian" method="POST">
							<div class="modal-body row">
								@csrf
								<div class="form-group col-md-12">
									<label>Quantity</label>
									<input type="number" class="form-control" id="jml_iteminrfs" name="jml_iteminrfs" placeholder="Quantity">

									<input type="hidden" id="isi_uraian">
									<input type="hidden" id="iduraiansubproduk">
								</div>
								<div class="form-group col-md-6">
									<label>Back Packing</label>
									<input type="number" class="form-control" id="topacking" name="topacking" placeholder="Back Packing">
								</div>
								<div class="form-group col-md-6">
									<label>Urai</label>
									<input type="number" class="form-control" id="touraian" name="touraian" placeholder="Urai">
								</div>
								<div class="form-group col-md-6">
									<label>Convert Jumlah Batang</label>
									<input type="number" class="form-control" id="conjml" name="conjml" placeholder="Convert Jumlah Batang" readonly>
								</div>
								<div class="form-group col-md-6">
									<label>Back Quality Control</label>
									<input type="number" class="form-control" id="toqc" name="toqc" placeholder="Back Quality Control">
								</div>
								<div class="form-group col-md-12">
									<label>Cincin</label>
									<input type="number" class="form-control" id="jmlcincin" name="jmlcincin" placeholder="Jumlah Cincin">
								</div>
								<div class="form-group col-md-12">
									<label>Stiker</label>
									<div class="row">
										<div class="col-md-6">
											<input type="number" class="form-control" id="stiker_luar" name="stiker_luar" placeholder="*Luar">
										</div>
										<div class="col-md-6">
											<input type="number" class="form-control" id="stiker_dalam" name="stiker_dalam" placeholder="*Dalam">
										</div>
									</div>
								</div>
								<div class="form-group col-md-12">
									<label>Cukai</label>
									<input type="number" class="form-control" id="jmlcukai" name="jmlcukai" placeholder="Jumlah Cukai">
								</div>
								<div class="form-group col-md-12">
									<label>Kemasan</label>
									<input type="number" class="form-control" id="jmlkemasan" name="jmlkemasan" placeholder="Jumlah Kemasan">
								</div>
								<div class="form-group col-md-12">
									<label>Bahan Temabaku</label>
									<div class="row">
										<div class="col-md-3">
											<input type="number" step="any" class="form-control" id="fill1" name="fill1" placeholder="Filler 1">
										</div>
										<div class="col-md-3">
											<input type="number" step="any" class="form-control" id="bind" name="bind" placeholder="Binding">
										</div>
										<div class="col-md-3">
											<input type="number" step="any" class="form-control" id="wrap" name="wrap" placeholder="Wrapping">
										</div>
										<div class="col-md-3">
											<input type="number" step="any" class="form-control" id="fill2" name="fill2" placeholder="Filler 2">
										</div>
									</div>
									<small>*Satuan Gram</small>
								</div>
							</div>
							<div class="modal-footer">
								<button type="submit" class="btn btn-info waves-effect text-left" @if (in_array($isi->status_inrfs,[3]) && $isi->arah_dari == 1))
									disabled 
								@endif>Save</button>
								<button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Close</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade" id="modal-noc_inrfs" tabindex="-1" role="dialog" aria-labelledby="title-noc_inrfs" aria-hidden="true" style="display: none;">
		<div class="modal-dialog modal-md">
			<div class="modal-content">
				<div class="card">
					<div class="card-header">
						<div class="modal-header">
							<h4 class="modal-title" id="title-noc_inrfs"></h4>
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
						</div>
					</div>
					<div class="card-body">
						<form id="form-noc_inrfs">
							<div class="modal-body row">
								<div class="form-group col-md-12">
									<label class="control-label">Quanity Reject</label>
									<input type="number" class="form-control" id="qty_noc_inrfs" placeholder="QTY">

									<input type="hidden" id="idnoc_itm">
									<input type="hidden" id="id_packing">
									<input type="hidden" id="id_sub_produk">
									<input type="hidden" id="name_reject">
								</div>
							</div>
							<div class="modal-footer">
								<button type="button" onclick="savereject()" class="btn btn-info waves-effect text-left">Save</button>
								<button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Close</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>	
	</div>
	@endsection

	@section('javascript')
	<script src="{{ asset('node_modules/moment/moment.js') }}">
	</script><script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
	@include('home.notification.script_notifnocrfs')
	@endsection