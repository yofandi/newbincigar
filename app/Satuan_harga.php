<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Satuan_harga extends Model
{
	protected $table = 'satuan_harga';

	public function pricetembakau()
	{
		return $this->hasMany('App\History_bahanmasuk', 'satua_harga_id','id');
	}
}
