-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: May 22, 2019 at 06:26 AM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `new_bincigar`
--

-- --------------------------------------------------------

--
-- Table structure for table `binding`
--

CREATE TABLE `binding` (
  `id` int(11) NOT NULL,
  `weisbmasuk` decimal(20,2) NOT NULL,
  `terpakaiprob` decimal(20,2) NOT NULL,
  `terpakaiweisb` decimal(20,2) NOT NULL,
  `sisaprob` decimal(20,2) NOT NULL,
  `sisaweisb` decimal(20,2) NOT NULL,
  `hasil_bind` varchar(45) NOT NULL,
  `hasil_akhirb` varchar(45) NOT NULL,
  `tanggal_bind` varchar(45) NOT NULL,
  `stock_binding_id` int(11) NOT NULL,
  `stock_filling_id` int(11) NOT NULL,
  `stock_probaku_id` int(11) NOT NULL,
  `author_session` varchar(150) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `log` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `cerutu_terjual`
--

CREATE TABLE `cerutu_terjual` (
  `id` int(11) NOT NULL,
  `penjualan_id` int(11) NOT NULL,
  `penjualan_customer_id_customer` int(11) NOT NULL,
  `stock_bagan_id` int(11) NOT NULL,
  `jml_terjual` int(20) NOT NULL,
  `diskon_terjual` int(20) NOT NULL,
  `total_terjual` varchar(45) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `cicil`
--

CREATE TABLE `cicil` (
  `id` int(11) NOT NULL,
  `penjualan_id` int(11) NOT NULL,
  `tanggal_cicil` datetime NOT NULL,
  `nominal_cicil` int(20) NOT NULL,
  `status_pembayar` varchar(20) NOT NULL,
  `status_lunas` varchar(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `cool`
--

CREATE TABLE `cool` (
  `id` int(11) NOT NULL,
  `tambah_cool` int(20) NOT NULL,
  `hasil_akhircool` int(20) NOT NULL,
  `lama_cool` time NOT NULL,
  `keterangan_cool` text NOT NULL,
  `tanggal_cool` datetime NOT NULL,
  `stock_cool_id` int(11) NOT NULL,
  `stock_fumigasi_id` int(11) NOT NULL,
  `author_session` varchar(200) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `log` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE `customer` (
  `id_customer` int(11) NOT NULL,
  `nama_customer` varchar(150) NOT NULL,
  `no_telf` varchar(20) NOT NULL,
  `email` varchar(100) NOT NULL,
  `alamat_customer` tinytext,
  `keterangan_customer` varchar(50) DEFAULT NULL,
  `users_id_users` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `drying1`
--

CREATE TABLE `drying1` (
  `id` int(11) NOT NULL,
  `tambah_dry1` int(11) NOT NULL,
  `hasil_akhirdry1` int(11) NOT NULL,
  `lama_dry1` time NOT NULL,
  `keterangan_dry1` text,
  `tanggal_dry1` datetime NOT NULL,
  `stock_wrapping_id` int(11) NOT NULL,
  `stock_drying1_id` int(11) NOT NULL,
  `author_session` varchar(45) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `log` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `drying2`
--

CREATE TABLE `drying2` (
  `id` int(11) NOT NULL,
  `tambah_dry2` int(20) NOT NULL,
  `hasil_akhirdry2` int(20) NOT NULL,
  `lama_dry2` time NOT NULL,
  `keterangan_dry2` text NOT NULL,
  `tanggal_dry2` datetime NOT NULL,
  `stock_frezer_id` int(11) NOT NULL,
  `stock_drying2_id` int(11) NOT NULL,
  `author_session` varchar(200) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `log` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `drying3`
--

CREATE TABLE `drying3` (
  `id` int(11) NOT NULL,
  `tambah_dry3` int(20) NOT NULL,
  `hasil_akhirdry3` int(20) NOT NULL,
  `lama_dry3` time NOT NULL,
  `keterangan_dry3` text NOT NULL,
  `tanggal_dry3` datetime NOT NULL,
  `stock_drying3_id` int(11) NOT NULL,
  `stock_cool_id` int(11) NOT NULL,
  `author_session` varchar(200) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `log` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `filling`
--

CREATE TABLE `filling` (
  `id` int(11) NOT NULL,
  `weisfmasuk` decimal(20,2) NOT NULL,
  `terpakaiprof` decimal(20,2) NOT NULL,
  `terpakaiweisf` decimal(20,2) NOT NULL,
  `sisaprof` decimal(20,2) NOT NULL,
  `sisaweisf` decimal(20,2) NOT NULL,
  `hasil_fill` int(20) NOT NULL,
  `tanggal_fill` datetime NOT NULL,
  `stock_probaku_id` int(11) NOT NULL,
  `stock_filling_id` int(11) NOT NULL,
  `author_session` varchar(150) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `log` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `frezer`
--

CREATE TABLE `frezer` (
  `id` int(11) NOT NULL,
  `tambah_fre` int(20) NOT NULL,
  `hasil_akhirfre` int(20) NOT NULL,
  `lama_fre` time NOT NULL,
  `keterengan_fre` text NOT NULL,
  `tanggal_fre` datetime NOT NULL,
  `stock_frezer_id` int(11) NOT NULL,
  `stock_drying1_id` int(11) NOT NULL,
  `author_session` varchar(200) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `log` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `fumigasi`
--

CREATE TABLE `fumigasi` (
  `id` int(11) NOT NULL,
  `tambah_fum` int(20) NOT NULL,
  `hasil_akhirfum` int(20) NOT NULL,
  `lama_fum` time NOT NULL,
  `keterangan_fum` text NOT NULL,
  `tanggal_fum` datetime NOT NULL,
  `stock_fumigasi_id` int(11) NOT NULL,
  `stock_drying2_id` int(11) NOT NULL,
  `author_session` varchar(200) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `log` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `history_bahanmasuk`
--

CREATE TABLE `history_bahanmasuk` (
  `id` int(11) NOT NULL,
  `tanggal_in` datetime NOT NULL,
  `asal` varchar(100) NOT NULL,
  `stock_masuk` decimal(20,2) NOT NULL,
  `diterima` decimal(20,2) NOT NULL,
  `diproduksi` decimal(20,2) NOT NULL,
  `hari_ini` decimal(20,2) NOT NULL,
  `ket_his` tinytext,
  `jenis_id_jenis` int(11) NOT NULL,
  `kategori_id_kategori` int(11) NOT NULL,
  `stock_probaku_id` int(11) NOT NULL,
  `author_session` varchar(150) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `log` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `history_bahanmasuk`
--

INSERT INTO `history_bahanmasuk` (`id`, `tanggal_in`, `asal`, `stock_masuk`, `diterima`, `diproduksi`, `hari_ini`, `ket_his`, `jenis_id_jenis`, `kategori_id_kategori`, `stock_probaku_id`, `author_session`, `created_at`, `updated_at`, `log`) VALUES
(1, '2019-01-01 11:15:50', 'Beli Exsternal', '3715.35', '3715.35', '0.00', '3715.35', NULL, 8, 4, 1, '1', '2019-05-21 04:17:46', '2019-05-21 04:17:46', 'INSERT'),
(2, '2019-01-01 11:18:29', 'Beli Exsternal', '5585.35', '5585.35', '0.00', '5585.35', NULL, 8, 3, 2, '1', '2019-05-21 04:18:56', '2019-05-21 04:18:56', 'INSERT'),
(3, '2019-01-01 11:19:24', 'Beli Exsternal', '5436.69', '5436.69', '0.00', '5436.69', NULL, 8, 1, 3, '1', '2019-05-21 04:19:52', '2019-05-21 04:19:52', 'INSERT');

-- --------------------------------------------------------

--
-- Table structure for table `history_cincin`
--

CREATE TABLE `history_cincin` (
  `id` int(11) NOT NULL,
  `masuk` int(15) NOT NULL,
  `terpakai` int(15) NOT NULL,
  `afkir` int(15) NOT NULL,
  `tanggal_cincin` datetime NOT NULL,
  `sisa` int(15) NOT NULL,
  `ket_cin` text,
  `stock_cincin_id_stock_cincin` int(11) NOT NULL,
  `stock_cincin_produk_id_produk` int(11) NOT NULL,
  `author_session` varchar(150) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `log` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `history_cukai`
--

CREATE TABLE `history_cukai` (
  `id` int(11) NOT NULL,
  `masuk_cukai` int(15) NOT NULL,
  `terpakai_cukailama` int(15) NOT NULL,
  `terpakai_cukaibaru` int(15) NOT NULL,
  `sisa_cukai` int(15) NOT NULL,
  `tanggal_cukai` datetime NOT NULL,
  `ket_hiscukai` tinytext,
  `stock_cukai_id_stock_cukai` int(11) NOT NULL,
  `stock_cukai_id_sub_produk` int(11) NOT NULL,
  `stock_cukai_produk_id_produk` int(11) NOT NULL,
  `packing_id_packing` int(11) DEFAULT NULL,
  `author_session` varchar(150) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `log` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `history_kemasan`
--

CREATE TABLE `history_kemasan` (
  `id` int(11) NOT NULL,
  `tanggal_hiskem` datetime NOT NULL,
  `masuk_kemasan` int(15) NOT NULL,
  `terpakai_kemasan` int(15) NOT NULL,
  `afkir_kemasan` int(15) NOT NULL,
  `stok_now` int(15) NOT NULL,
  `ket_kem` text,
  `stock_kemasan_id_stock_kemasan` int(11) NOT NULL,
  `stock_kemasan_kemasan_id_kemasan` int(11) NOT NULL,
  `stock_kemasan_produk_id_produk` int(11) NOT NULL,
  `author_session` varchar(150) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `log` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `history_stiker`
--

CREATE TABLE `history_stiker` (
  `id` int(11) NOT NULL,
  `stock_stiker_id_stock_stiker` int(20) NOT NULL,
  `stock_stiker_produk_id_produk` int(11) NOT NULL,
  `masuk_luar` int(15) NOT NULL,
  `pakai_luar` int(15) NOT NULL,
  `hasil_luar` int(15) NOT NULL,
  `masuk_dalam` int(15) NOT NULL,
  `pakai_dalam` int(15) NOT NULL,
  `hasil_dalam` int(15) NOT NULL,
  `ket_sti` text,
  `tanggal_hisstik` datetime NOT NULL,
  `author_session` varchar(150) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `log` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jenis`
--

CREATE TABLE `jenis` (
  `id_jenis` int(11) NOT NULL,
  `jenis` varchar(45) NOT NULL,
  `deskripsi_jenis` tinytext,
  `keterangan_jenis` tinytext
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jenis`
--

INSERT INTO `jenis` (`id_jenis`, `jenis`, `deskripsi_jenis`, `keterangan_jenis`) VALUES
(8, 'Tembakau Asalan', 'Tembakau Asalan', 'Tembakau Asalan');

-- --------------------------------------------------------

--
-- Table structure for table `jenis_has_produk`
--

CREATE TABLE `jenis_has_produk` (
  `id_jnpro` int(11) NOT NULL,
  `jenis_id_jenis` int(11) NOT NULL,
  `produk_id_produk` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jenis_has_produk`
--

INSERT INTO `jenis_has_produk` (`id_jnpro`, `jenis_id_jenis`, `produk_id_produk`) VALUES
(10, 3, 10),
(11, 8, 11),
(12, 8, 12),
(13, 8, 13),
(14, 8, 14),
(15, 8, 15),
(16, 8, 16),
(17, 8, 17),
(18, 8, 18),
(19, 8, 19),
(20, 8, 20),
(21, 8, 21),
(22, 8, 22),
(23, 8, 23),
(24, 8, 24),
(25, 8, 25),
(26, 8, 26),
(27, 8, 27),
(28, 8, 28),
(29, 8, 29),
(30, 8, 30),
(31, 8, 31),
(32, 8, 32),
(33, 8, 33),
(34, 8, 34),
(35, 8, 35),
(36, 8, 36),
(37, 8, 37),
(38, 8, 38),
(39, 8, 39),
(40, 8, 40),
(41, 8, 41),
(42, 8, 42),
(43, 8, 43),
(44, 8, 44),
(45, 8, 45),
(46, 8, 46),
(47, 8, 47),
(48, 8, 48),
(49, 8, 49);

-- --------------------------------------------------------

--
-- Table structure for table `kategori`
--

CREATE TABLE `kategori` (
  `id_kategori` int(11) NOT NULL,
  `kategori` varchar(45) NOT NULL,
  `deskripsi_kategori` tinytext,
  `keterangan_kategori` tinytext
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kategori`
--

INSERT INTO `kategori` (`id_kategori`, `kategori`, `deskripsi_kategori`, `keterangan_kategori`) VALUES
(1, 'FILLER 1', NULL, NULL),
(2, 'FILLER 2', NULL, NULL),
(3, 'OMBLAD', NULL, NULL),
(4, 'DEKBLAD', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `kemasan`
--

CREATE TABLE `kemasan` (
  `id_kemasan` int(11) NOT NULL,
  `nama_kemasan` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kemasan`
--

INSERT INTO `kemasan` (`id_kemasan`, `nama_kemasan`) VALUES
(1, 'WOODEN'),
(2, 'PAPER'),
(3, 'PLASTIK'),
(4, 'FOIL'),
(5, 'LOS'),
(6, 'TUBE'),
(7, 'KOTAK SENG'),
(8, 'AKRILIK');

-- --------------------------------------------------------

--
-- Table structure for table `laporan`
--

CREATE TABLE `laporan` (
  `id_laporan` int(11) NOT NULL,
  `curah_hujan` varchar(45) NOT NULL,
  `pagi` varchar(45) NOT NULL,
  `siang` varchar(45) NOT NULL,
  `sore` varchar(45) NOT NULL,
  `bak_air` varchar(45) NOT NULL,
  `lasiotrap_lemari` varchar(45) NOT NULL,
  `ds` varchar(45) NOT NULL,
  `store` varchar(45) NOT NULL,
  `agent` varchar(45) NOT NULL,
  `call` varchar(45) NOT NULL,
  `efektif_call` varchar(45) NOT NULL,
  `noo` varchar(45) NOT NULL,
  `direksi` tinytext NOT NULL,
  `kesulitan` tinytext NOT NULL,
  `tanggal_laporan` datetime NOT NULL,
  `author_session` varchar(200) NOT NULL,
  `status_laporan` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `level_user`
--

CREATE TABLE `level_user` (
  `id` int(11) NOT NULL,
  `level` varchar(45) NOT NULL,
  `deskripsi` tinytext
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `level_user`
--

INSERT INTO `level_user` (`id`, `level`, `deskripsi`) VALUES
(1, 'SUPER ADMIN', NULL),
(2, 'MANAGER', NULL),
(3, 'BAHAN BAKU', NULL),
(4, 'PROSES PRODUKSI', NULL),
(5, 'QUALITY CONTROL', NULL),
(6, 'RFS', NULL),
(7, 'STORE', NULL),
(8, 'AGENT', NULL),
(9, 'PHRI', NULL),
(10, 'NON PHRI', NULL),
(11, 'EXPORT', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_04_10_095836_add_new_column_stock_probaku', 1),
(4, '2019_04_11_031528_add_new_column_history_cincin', 1),
(5, '2019_04_12_032214_add_new_column_history_cukai', 2),
(6, '2019_04_12_032522_add_new_column_history_cukai1', 3),
(7, '2019_04_12_034220_add_new_column_history_stiker', 4),
(8, '2019_04_12_154520_add_new_column_history_kemasan', 5),
(9, '2019_04_18_141155_addcolumnfilling', 6),
(10, '2019_04_19_151711_add_columnstockfilling', 7),
(11, '2019_04_22_111431_addcolumn_binding', 8),
(12, '2019_04_22_111634_addcolumn_stock_binding', 9),
(13, '2019_05_04_114325_create_notifications_table', 10);

-- --------------------------------------------------------

--
-- Table structure for table `noc_inrfs`
--

CREATE TABLE `noc_inrfs` (
  `id` int(11) NOT NULL,
  `tanggal_inrfs` datetime NOT NULL,
  `status_lihat` int(1) NOT NULL,
  `status_inrfs` int(1) NOT NULL,
  `keterangan` tinytext,
  `author_session` varchar(45) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `log` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `noc_inrfs`
--

INSERT INTO `noc_inrfs` (`id`, `tanggal_inrfs`, `status_lihat`, `status_inrfs`, `keterangan`, `author_session`, `created_at`, `updated_at`, `log`) VALUES
(13, '2019-05-17 13:23:10', 1, 1, 'coba 1', '1', '2019-05-16 17:00:00', '2019-05-17 07:57:22', 'INSERT'),
(15, '2019-05-17 01:37:07', 1, 1, 'coba reject 1', '1', '2019-05-16 17:00:00', '2019-05-17 09:14:48', 'INSERT'),
(16, '2019-05-17 17:47:14', 1, 1, 'coba kirim 2', '1', '2019-05-16 17:00:00', '2019-05-17 11:09:39', 'INSERT'),
(24, '2019-05-17 18:15:38', 1, 1, 'ada yang rusak', '1', '2019-05-16 17:00:00', '2019-05-17 11:42:08', 'INSERT'),
(25, '2019-05-17 18:15:30', 1, 1, 'maneh', '1', '2019-05-16 17:00:00', '2019-05-17 11:28:53', 'INSERT'),
(29, '2019-05-17 18:39:14', 1, 1, 'udah mas ku', '1', '2019-05-16 17:00:00', '2019-05-17 11:41:46', 'INSERT'),
(30, '2019-05-17 18:43:00', 1, 1, 'aku sayang kamu', '1', '2019-05-16 17:00:00', '2019-05-18 02:53:57', 'INSERT'),
(31, '2019-05-17 06:43:58', 1, 2, 'bodo amat', '1', '2019-05-16 17:00:00', '2019-05-18 02:54:16', 'INSERT');

-- --------------------------------------------------------

--
-- Table structure for table `noc_items`
--

CREATE TABLE `noc_items` (
  `id` int(11) NOT NULL,
  `stock_packing_id` int(11) NOT NULL,
  `stock_packing_id_sub_produk` int(11) NOT NULL,
  `noc_inrfs_id` int(11) NOT NULL,
  `jml_nocitm` int(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `noc_items`
--

INSERT INTO `noc_items` (`id`, `stock_packing_id`, `stock_packing_id_sub_produk`, `noc_inrfs_id`, `jml_nocitm`, `created_at`, `updated_at`, `deleted_at`) VALUES
(14, 5, 2, 13, 2, NULL, '2019-05-17 11:06:42', NULL),
(17, 6, 4, 15, 2, NULL, '2019-05-17 07:58:44', NULL),
(18, 5, 2, 16, 3, NULL, '2019-05-17 11:08:01', NULL),
(19, 5, 2, 24, 1, NULL, '2019-05-17 11:03:51', NULL),
(20, 5, 2, 25, 1, NULL, '2019-05-17 11:26:26', NULL),
(21, 5, 2, 29, 1, NULL, '2019-05-17 11:26:26', NULL),
(22, 5, 2, 30, 1, NULL, '2019-05-17 11:44:49', NULL),
(23, 5, 2, 31, 1, NULL, '2019-05-17 11:44:49', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `order`
--

CREATE TABLE `order` (
  `id` int(11) NOT NULL,
  `tanggal_order` datetime NOT NULL,
  `keterangan_order` tinytext NOT NULL,
  `rfs_id` int(11) DEFAULT NULL,
  `status_lihatrfs` int(1) DEFAULT NULL,
  `verifikasi_rfs` int(1) DEFAULT NULL,
  `status_lihatbagan` int(1) DEFAULT NULL,
  `users_id` int(11) NOT NULL,
  `author_session` varchar(200) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `order_items`
--

CREATE TABLE `order_items` (
  `id` int(11) NOT NULL,
  `stock_bagan_id` int(11) NOT NULL,
  `stock_bagan_id_sub_produk` int(11) NOT NULL,
  `stock_bagan_id_produk` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `jml_order` int(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `packing`
--

CREATE TABLE `packing` (
  `id` int(11) NOT NULL,
  `batangpack` int(20) NOT NULL,
  `hasil_batangpack` int(20) NOT NULL,
  `tambah_packing` int(20) NOT NULL,
  `hasil_packing` int(20) NOT NULL,
  `keterangan_packing` tinytext NOT NULL,
  `tanggal_packing` datetime NOT NULL,
  `stock_packing_id` int(11) NOT NULL,
  `stock_qc_id` int(11) NOT NULL,
  `author_session` varchar(200) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `log` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `penerimaan`
--

CREATE TABLE `penerimaan` (
  `id` int(11) NOT NULL,
  `penjualan_id` int(11) NOT NULL,
  `tanggal_penerimaan` datetime NOT NULL,
  `nominal_penerimaan` int(20) NOT NULL,
  `status_pembayaran` varchar(45) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `penjualan`
--

CREATE TABLE `penjualan` (
  `id` int(11) NOT NULL,
  `no_invoice` varchar(200) NOT NULL,
  `harga_cerutu` int(20) NOT NULL,
  `diskon_penjualan` int(20) NOT NULL,
  `ongkir_penjualan` int(20) NOT NULL,
  `total_penjualan` int(20) NOT NULL,
  `yang_dibayar` int(20) NOT NULL,
  `tanggal_penjualan` datetime NOT NULL,
  `customer_id_customer` int(11) NOT NULL,
  `lokasi_kirim` tinytext NOT NULL,
  `depature_date` datetime DEFAULT NULL,
  `vessel` varchar(100) DEFAULT NULL,
  `port_of_loading` varchar(100) DEFAULT NULL,
  `port_of_destination` varchar(100) DEFAULT NULL,
  `keterangan_penjualan` tinytext,
  `status_pembayar` varchar(20) NOT NULL,
  `status_lunas` varchar(20) NOT NULL,
  `author_session` varchar(200) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `log` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pressing`
--

CREATE TABLE `pressing` (
  `id` int(11) NOT NULL,
  `tambah_pres` int(20) NOT NULL,
  `hasil_akhirp` int(20) NOT NULL,
  `lama` time NOT NULL,
  `keterangan_pres` text NOT NULL,
  `tanggal_pres` datetime NOT NULL,
  `stock_pressing_id` int(11) NOT NULL,
  `stock_binding_id` int(11) NOT NULL,
  `author_session` varchar(150) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `log` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `produk`
--

CREATE TABLE `produk` (
  `id_produk` int(11) NOT NULL,
  `produk` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `produk`
--

INSERT INTO `produk` (`id_produk`, `produk`) VALUES
(11, 'Robusto'),
(12, 'Corona'),
(13, 'Half Corona'),
(14, 'Maumere'),
(15, 'Cigar Master'),
(16, 'El Nino'),
(17, 'Mondlicht'),
(18, 'Jember Cigar'),
(19, 'C99'),
(20, 'Sumatera'),
(21, 'Don Agusto'),
(22, 'El Bomba'),
(23, 'Don Juan'),
(24, 'Sakera'),
(25, 'Merubetiri'),
(26, 'NFC Green'),
(27, 'NFC Red'),
(28, 'SIGO 1'),
(29, 'SIGO 2'),
(30, 'SIGO 3'),
(31, 'SIGO 4'),
(32, 'SIGO 5'),
(33, 'SIGO 6'),
(34, 'Robustos'),
(35, 'HK 52'),
(36, 'HK 54'),
(37, 'HK 56'),
(38, 'Magic'),
(39, 'Genio'),
(40, 'R. Supremo'),
(41, 'Piramide'),
(42, 'Majestuso'),
(43, 'Horsetail'),
(44, 'Secretos'),
(45, 'Talama'),
(46, 'Tambo'),
(47, 'FR Ring 52'),
(48, 'FR Ring 60'),
(49, 'Java Blend');

-- --------------------------------------------------------

--
-- Table structure for table `quality_control`
--

CREATE TABLE `quality_control` (
  `id` int(11) NOT NULL,
  `accept` int(20) NOT NULL,
  `back_bind` int(20) NOT NULL,
  `back_wrap` int(20) NOT NULL,
  `back_lazio` int(20) NOT NULL,
  `hasil_qc` int(20) NOT NULL,
  `keterangan_qc` text NOT NULL,
  `lama_qc` time NOT NULL,
  `tanggal_qc` datetime NOT NULL,
  `stock_qc_id` int(11) NOT NULL,
  `stock_drying3_id` int(11) NOT NULL,
  `author_session` varchar(200) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `log` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `quality_control`
--

INSERT INTO `quality_control` (`id`, `accept`, `back_bind`, `back_wrap`, `back_lazio`, `hasil_qc`, `keterangan_qc`, `lama_qc`, `tanggal_qc`, `stock_qc_id`, `stock_drying3_id`, `author_session`, `created_at`, `updated_at`, `log`) VALUES
(10, 140, 5, 5, 0, 225, 'sds', '01:50:00', '2019-04-30 11:46:08', 2, 1, '1', '2019-04-30 04:46:41', '2019-04-30 04:48:28', 'INSERT'),
(12, 50, 0, 0, 0, 175, 'sdeef', '02:25:00', '2019-04-30 11:47:20', 2, 1, '1', '2019-04-30 04:47:48', '2019-04-30 04:49:04', 'INSERT'),
(14, 500, 0, 0, 0, 20, 'aswrc', '02:45:00', '2019-05-13 11:40:31', 3, 2, '1', '2019-05-13 04:41:05', '2019-05-13 04:41:05', 'INSERT');

-- --------------------------------------------------------

--
-- Table structure for table `return`
--

CREATE TABLE `return` (
  `id_return` int(11) NOT NULL,
  `jenis_id_jenis` int(11) NOT NULL,
  `sub_produk_id_sub_produk` int(11) NOT NULL,
  `stock_bagan_id` int(11) DEFAULT NULL,
  `jml_forbagan` int(20) DEFAULT NULL,
  `tanggal_rtnbagan` datetime DEFAULT NULL,
  `ket_bagan` tinytext,
  `usersidbagan` varchar(150) DEFAULT NULL,
  `stock_rfs_id` int(11) DEFAULT NULL,
  `jml_forrfs` int(20) DEFAULT NULL,
  `tanggal_rtnrfs` datetime DEFAULT NULL,
  `ket_rfs` tinytext,
  `stock_qc_id` int(11) DEFAULT NULL,
  `stock_binding_id` int(11) DEFAULT NULL,
  `stock_wrapping_id` int(11) DEFAULT NULL,
  `stock_packing_id` int(11) DEFAULT NULL,
  `rtn_bind` int(20) DEFAULT NULL,
  `rtn_wrap` int(20) DEFAULT NULL,
  `rtn_pack` int(20) DEFAULT NULL,
  `rtn_lazio` int(20) DEFAULT NULL,
  `ket_qc` int(20) DEFAULT NULL,
  `tanggal_rtnqc` datetime DEFAULT NULL,
  `status_return` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rfs`
--

CREATE TABLE `rfs` (
  `id` int(11) NOT NULL,
  `tanggal_rfs` datetime NOT NULL,
  `keterangan_rfs` tinytext,
  `verifikasi_bagan` int(1) DEFAULT NULL,
  `keterangan_verifikasi` tinytext,
  `users_id` int(11) NOT NULL,
  `author_session` varchar(200) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `log` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rfs_items`
--

CREATE TABLE `rfs_items` (
  `id` int(11) NOT NULL,
  `rfs_id` int(11) NOT NULL,
  `stock_rfs_id` int(11) NOT NULL,
  `stock_rfs_id_sub_produk` int(11) NOT NULL,
  `keluar_rfs` int(45) NOT NULL,
  `sisa_rfs` int(45) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `setting`
--

CREATE TABLE `setting` (
  `idsetting` int(11) NOT NULL,
  `direktur_utama` varchar(100) DEFAULT NULL,
  `direktur_operasional` varchar(100) DEFAULT NULL,
  `kabag_produksi` varchar(100) DEFAULT NULL,
  `quality_control` varchar(100) DEFAULT NULL,
  `ready_for_sale` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `setting`
--

INSERT INTO `setting` (`idsetting`, `direktur_utama`, `direktur_operasional`, `kabag_produksi`, `quality_control`, `ready_for_sale`) VALUES
(1, 'Ir. H Febrian Ananta Kahar', 'Ir. H Imam Wahid Wahyudi', 'Slamet Wijaya', 'Risma', 'Citra Wahyu Lestari');

-- --------------------------------------------------------

--
-- Table structure for table `stock_bagan`
--

CREATE TABLE `stock_bagan` (
  `id` int(11) NOT NULL,
  `stock_bagan` int(20) NOT NULL,
  `sub_produk_id_sub_produk` int(11) NOT NULL,
  `sub_produk_id_produk` int(11) NOT NULL,
  `users_id_users` varchar(150) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `stock_binding`
--

CREATE TABLE `stock_binding` (
  `id` int(20) NOT NULL,
  `jml_bind` int(20) NOT NULL,
  `stock_weisb` decimal(20,2) NOT NULL,
  `jenis_has_produk_id_jnpro` int(11) NOT NULL,
  `jenis_has_produk_id_jenis` int(11) NOT NULL,
  `jenis_has_produk_id_produk` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `stock_cincin`
--

CREATE TABLE `stock_cincin` (
  `id_stock_cincin` int(11) NOT NULL,
  `stock_qty_cincin` int(20) NOT NULL,
  `produk_id_produk` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stock_cincin`
--

INSERT INTO `stock_cincin` (`id_stock_cincin`, `stock_qty_cincin`, `produk_id_produk`) VALUES
(2, 3797, 11),
(3, 5175, 12),
(4, 5691, 13),
(5, 1790, 14),
(6, 6354, 16),
(7, 4384, 18),
(8, 6624, 21),
(9, 4359, 49);

-- --------------------------------------------------------

--
-- Table structure for table `stock_cool`
--

CREATE TABLE `stock_cool` (
  `id` int(11) NOT NULL,
  `jml_cool` int(20) NOT NULL,
  `jenis_has_produk_id_jnpro` int(11) NOT NULL,
  `jenis_has_produk_id_jenis` int(11) NOT NULL,
  `jenis_has_produk_id_produk` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `stock_cukai`
--

CREATE TABLE `stock_cukai` (
  `id_stock_cukai` int(11) NOT NULL,
  `stock_qty_cukai` int(20) NOT NULL,
  `sub_produk_id_sub_produk` int(11) NOT NULL,
  `sub_produk_produk_id_produk` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stock_cukai`
--

INSERT INTO `stock_cukai` (`id_stock_cukai`, `stock_qty_cukai`, `sub_produk_id_sub_produk`, `sub_produk_produk_id_produk`) VALUES
(1, 90, 7, 11),
(2, 76, 8, 12),
(3, 14, 16, 13),
(4, 15, 18, 14);

-- --------------------------------------------------------

--
-- Table structure for table `stock_drying1`
--

CREATE TABLE `stock_drying1` (
  `id` int(11) NOT NULL,
  `jml_dry1` int(20) NOT NULL,
  `jenis_has_produk_id_jnpro` int(11) NOT NULL,
  `jenis_has_produk_id_jenis` int(11) NOT NULL,
  `jenis_has_produk_id_produk` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `stock_drying2`
--

CREATE TABLE `stock_drying2` (
  `id` int(11) NOT NULL,
  `jml_dry2` int(20) NOT NULL,
  `jenis_has_produk_id_jnpro` int(11) NOT NULL,
  `jenis_has_produk_id_jenis` int(11) NOT NULL,
  `jenis_has_produk_id_produk` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `stock_drying3`
--

CREATE TABLE `stock_drying3` (
  `id` int(11) NOT NULL,
  `jml_dry3` int(20) NOT NULL,
  `jenis_has_produk_id_jnpro` int(11) NOT NULL,
  `jenis_has_produk_id_jenis` int(11) NOT NULL,
  `jenis_has_produk_id_produk` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `stock_filling`
--

CREATE TABLE `stock_filling` (
  `id` int(20) NOT NULL,
  `jml_fill` int(20) NOT NULL,
  `stock_weisf` decimal(20,2) NOT NULL,
  `jenis_has_produk_id_jnpro` int(11) NOT NULL,
  `jenis_has_produk_id_jenis` int(11) NOT NULL,
  `jenis_has_produk_id_produk` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `stock_frezer`
--

CREATE TABLE `stock_frezer` (
  `id` int(11) NOT NULL,
  `jml_fre` int(11) NOT NULL,
  `jenis_has_produk_id_jnpro` int(11) NOT NULL,
  `jenis_has_produk_id_jenis` int(11) NOT NULL,
  `jenis_has_produk_id_produk` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `stock_fumigasi`
--

CREATE TABLE `stock_fumigasi` (
  `id` int(11) NOT NULL,
  `jml_fum` int(20) DEFAULT NULL,
  `jenis_has_produk_id_jnpro` int(11) NOT NULL,
  `jenis_has_produk_id_jenis` int(11) NOT NULL,
  `jenis_has_produk_id_produk` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `stock_kemasan`
--

CREATE TABLE `stock_kemasan` (
  `id_stock_kemasan` int(11) NOT NULL,
  `stock_kemasan` int(20) NOT NULL,
  `kemasan_id_kemasan` int(11) NOT NULL,
  `produk_id_produk` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stock_kemasan`
--

INSERT INTO `stock_kemasan` (`id_stock_kemasan`, `stock_kemasan`, `kemasan_id_kemasan`, `produk_id_produk`) VALUES
(1, 2206, 6, 13);

-- --------------------------------------------------------

--
-- Table structure for table `stock_packing`
--

CREATE TABLE `stock_packing` (
  `id` int(11) NOT NULL,
  `jml_packing` int(20) NOT NULL,
  `sub_produk_id_sub_produk` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `stock_pressing`
--

CREATE TABLE `stock_pressing` (
  `id` int(11) NOT NULL,
  `jml_pres` int(20) NOT NULL,
  `jenis_has_produk_id_jnpro` int(11) NOT NULL,
  `jenis_has_produk_id_jenis` int(11) NOT NULL,
  `jenis_has_produk_id_produk` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `stock_probaku`
--

CREATE TABLE `stock_probaku` (
  `id` int(11) NOT NULL,
  `jml_produksi` decimal(20,2) NOT NULL,
  `jml_bahanbaku` decimal(20,2) NOT NULL,
  `jenis_id_jenis` int(11) NOT NULL,
  `kategori_id_kategori` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stock_probaku`
--

INSERT INTO `stock_probaku` (`id`, `jml_produksi`, `jml_bahanbaku`, `jenis_id_jenis`, `kategori_id_kategori`, `created_at`, `updated_at`) VALUES
(1, '0.00', '3715.35', 8, 4, '2019-05-21 04:17:46', '2019-05-21 04:17:46'),
(2, '0.00', '5585.35', 8, 3, '2019-05-21 04:18:56', '2019-05-21 04:18:56'),
(3, '0.00', '5436.69', 8, 1, '2019-05-21 04:19:52', '2019-05-21 04:19:52');

-- --------------------------------------------------------

--
-- Table structure for table `stock_qc`
--

CREATE TABLE `stock_qc` (
  `id` int(11) NOT NULL,
  `jml_qc` int(20) NOT NULL,
  `jenis_has_produk_id_jnpro` int(11) NOT NULL,
  `jenis_has_produk_id_jenis` int(11) NOT NULL,
  `jenis_has_produk_id_produk` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `stock_rfs`
--

CREATE TABLE `stock_rfs` (
  `id` int(11) NOT NULL,
  `sub_produk_id_sub_produk` int(11) NOT NULL,
  `jml_rfs` int(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `stock_stiker`
--

CREATE TABLE `stock_stiker` (
  `id_stock_stiker` int(20) NOT NULL,
  `stock_luar` int(15) NOT NULL,
  `stock_dalam` int(15) NOT NULL,
  `produk_id_produk` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stock_stiker`
--

INSERT INTO `stock_stiker` (`id_stock_stiker`, `stock_luar`, `stock_dalam`, `produk_id_produk`) VALUES
(1, 0, 0, 11),
(2, 7, 0, 13);

-- --------------------------------------------------------

--
-- Table structure for table `stock_wrapping`
--

CREATE TABLE `stock_wrapping` (
  `id` int(11) NOT NULL,
  `jml_wrap` int(20) NOT NULL,
  `stock_weiswrap` decimal(20,2) NOT NULL,
  `jenis_has_produk_id_jnpro` int(11) NOT NULL,
  `jenis_has_produk_id_jenis` int(11) NOT NULL,
  `jenis_has_produk_id_produk` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sub_produk`
--

CREATE TABLE `sub_produk` (
  `id_sub_produk` int(11) NOT NULL,
  `sub_kode` varchar(50) NOT NULL,
  `sub_produk` varchar(50) NOT NULL,
  `produk_id_produk` int(11) NOT NULL,
  `kemasan_id_kemasan` int(11) NOT NULL,
  `tarif` varchar(100) NOT NULL,
  `hje` varchar(100) NOT NULL,
  `isi` int(20) NOT NULL,
  `image` tinytext
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sub_produk`
--

INSERT INTO `sub_produk` (`id_sub_produk`, `sub_kode`, `sub_produk`, `produk_id_produk`, `kemasan_id_kemasan`, `tarif`, `hje`, `isi`, `image`) VALUES
(7, 'BR10', 'Robusto BR10', 11, 1, '0', '540000', 10, NULL),
(8, 'BC10', 'Corona BC10', 12, 1, '0', '0', 10, NULL),
(12, 'HC1', 'Half Corona HC1', 13, 6, '110000', '110000', 1, NULL),
(13, 'BC1', 'Corona BC1', 12, 4, '0', '0', 1, NULL),
(14, 'BC3', 'Corona BC3', 12, 1, '0', '0', 3, NULL),
(15, 'BC45', 'Corona BC45', 12, 1, '0', '0', 45, NULL),
(16, 'HC10', 'Half Corona HC10', 13, 1, '0', '0', 10, NULL),
(17, 'BR12', 'Robusto BR12', 11, 1, '0', '0', 12, NULL),
(18, 'M5', 'Maumere M5', 14, 2, '0', '0', 5, NULL),
(19, 'BR3', 'Robusto BR3', 11, 2, '0', '0', 3, NULL),
(20, 'DA10', 'Don Agusto DA10', 21, 1, '0', '0', 10, NULL),
(21, 'DA5', 'Don Agusto DA5', 21, 1, '0', '0', 5, NULL),
(22, 'DA1', 'Don Agusto DA1', 21, 6, '0', '0', 1, NULL),
(23, 'JC15', 'Jember Cigar JC15', 18, 1, '0', '0', 15, NULL),
(24, 'JC12', 'Jember Cigar JC12', 18, 1, '0', '0', 12, NULL),
(25, 'MD20', 'Mondlicht MD20', 17, 1, '0', '0', 20, NULL),
(26, 'MD20', 'Mondlicht MD20', 17, 2, '0', '0', 20, NULL),
(27, 'M10', 'Maumere M10', 14, 1, '0', '0', 10, NULL),
(28, 'BC5', 'Corona BC5', 12, 2, '0', '0', 5, NULL),
(29, 'BC5', 'Corona BC5', 12, 1, '0', '0', 5, NULL),
(30, 'HC5', 'Half Corona HC5', 13, 2, '0', '0', 5, NULL),
(31, 'CM16', 'Cigar Master CM16', 15, 1, '0', '0', 16, NULL),
(32, 'M1', 'Maumere M1', 14, 6, '0', '0', 1, NULL),
(33, 'EN20', 'El Nino EN20', 16, 1, '0', '0', 20, NULL),
(34, 'EN5', 'El Nino EN5', 16, 2, '0', '0', 5, NULL),
(35, 'C16', 'C99 C16', 19, 1, '0', '0', 16, NULL),
(36, 'C20', 'C99 C20', 19, 8, '0', '0', 20, NULL),
(37, 'ST80', 'Sumatra ST80', 20, 1, '0', '0', 80, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `username` varchar(45) DEFAULT NULL,
  `email` varchar(200) NOT NULL,
  `password` varchar(225) NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `no_hp` varchar(15) DEFAULT NULL,
  `alamat_user` tinytext,
  `penanda_email` int(2) DEFAULT NULL,
  `upload_foto` varchar(100) DEFAULT NULL,
  `status_users` varchar(45) NOT NULL,
  `level_user_id_level` int(11) NOT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `username`, `email`, `password`, `email_verified_at`, `no_hp`, `alamat_user`, `penanda_email`, `upload_foto`, `status_users`, `level_user_id_level`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Yofandi Riki Winata', 'yofandi', 'yofandirikiwinata24@gmail.com', '$2y$10$63V8RiIuZ9PawxuPN91uy.SJaH5irmPvTU4JujP2N8obJuIMRE9Ea', NULL, '085287299141', 'Tunjungrejo, Yosowilangun, Lumajang', NULL, 'bincigar20190320111603.jpg', 'ACTIVE', 1, 'kdUilLNB02ri70ECABOvLI7fDBfeJkwuNDjAGdoAwYo8hiZOtdwDRVwH6WhY', '2019-03-01 03:34:40', '2019-03-20 04:59:36'),
(3, 'admin', 'admin', 'admin@gmail.com', '$2y$10$Mdrt89MA9guZm5Ll8kcFqeeCOajbllU4uyE4tz1NqaMWLg.4o8Bge', NULL, '12345678', NULL, NULL, 'bincigar20190521104210.jpg', 'ACTIVE', 1, '7WV8tguo0Pij0fc62Q6Sd3wL325bs3F4qwCdx3VXwvUA6SZDxJbgb6zHvCBQ', '2019-03-29 04:31:46', '2019-05-21 03:42:10'),
(12, 'Sri Wahyu Ningsih', 'yuni', 'yuni.ningsih@bincigar.com', '$2y$10$A5eim1LQG5k1hBhOGQWBle7HyHnjROaaW97RiPaYyMF0n0ZZ0HZti', NULL, NULL, 'Kasub. Administrasi', NULL, NULL, 'ACTIVE', 1, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `wrapping`
--

CREATE TABLE `wrapping` (
  `id` int(11) NOT NULL,
  `weiswmasuk` decimal(20,2) NOT NULL,
  `terpakaiprow` decimal(20,2) NOT NULL,
  `terpakaiweisw` decimal(20,2) NOT NULL,
  `sisaprow` decimal(20,2) NOT NULL,
  `sisaweisw` decimal(20,2) NOT NULL,
  `tambah_wrap` int(20) NOT NULL,
  `hasil_akhirw` int(20) NOT NULL,
  `keterangan_wrap` tinytext,
  `tanggal_wrap` datetime NOT NULL,
  `stock_probaku_id` int(11) NOT NULL,
  `stock_wrapping_id` int(11) NOT NULL,
  `stock_pressing_id` int(11) NOT NULL,
  `author_session` varchar(200) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `log` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wrapping`
--

INSERT INTO `wrapping` (`id`, `weiswmasuk`, `terpakaiprow`, `terpakaiweisw`, `sisaprow`, `sisaweisw`, `tambah_wrap`, `hasil_akhirw`, `keterangan_wrap`, `tanggal_wrap`, `stock_probaku_id`, `stock_wrapping_id`, `stock_pressing_id`, `author_session`, `created_at`, `updated_at`, `log`) VALUES
(11, '0.20', '1.50', '0.00', '18.50', '0.20', 25, 135, 'dfm', '2019-04-23 16:47:25', 7, 1, 1, '1', '2019-04-23 09:47:42', '2019-04-23 09:48:52', 'INSERT'),
(12, '0.30', '1.50', '0.00', '17.00', '0.50', 30, 105, 'nfj', '2019-04-23 16:47:47', 7, 1, 1, '1', '2019-04-23 09:48:09', '2019-04-23 09:48:52', 'INSERT'),
(13, '0.00', '2.00', '0.00', '15.00', '0.50', 400, 255, 'sdevrt', '2019-04-29 17:51:07', 7, 1, 1, '1', '2019-04-29 10:51:36', '2019-04-29 10:51:36', 'INSERT'),
(14, '2.00', '15.00', '0.00', '35.00', '2.00', 755, 15, 'kalbe', '2019-05-13 10:41:06', 10, 2, 2, '1', '2019-05-13 03:41:48', '2019-05-13 03:43:08', 'UPDATE');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `binding`
--
ALTER TABLE `binding`
  ADD PRIMARY KEY (`id`,`stock_binding_id`,`stock_filling_id`,`stock_probaku_id`),
  ADD KEY `fk_binding_stock_binding1_idx` (`stock_binding_id`),
  ADD KEY `fk_binding_stock_filling1_idx` (`stock_filling_id`),
  ADD KEY `fk_binding_stock_probaku1_idx` (`stock_probaku_id`);

--
-- Indexes for table `cerutu_terjual`
--
ALTER TABLE `cerutu_terjual`
  ADD PRIMARY KEY (`id`,`penjualan_id`,`penjualan_customer_id_customer`),
  ADD KEY `fk_cerutu_terjual_penjualan1_idx` (`penjualan_id`,`penjualan_customer_id_customer`),
  ADD KEY `fk_cerutu_terjual_stock_bagan1_idx` (`stock_bagan_id`);

--
-- Indexes for table `cicil`
--
ALTER TABLE `cicil`
  ADD PRIMARY KEY (`id`,`penjualan_id`),
  ADD KEY `fk_cicil_penjualan1_idx` (`penjualan_id`);

--
-- Indexes for table `cool`
--
ALTER TABLE `cool`
  ADD PRIMARY KEY (`id`,`stock_cool_id`,`stock_fumigasi_id`),
  ADD KEY `fk_cool_stock_cool1_idx` (`stock_cool_id`),
  ADD KEY `fk_cool_stock_fumigasi1_idx` (`stock_fumigasi_id`);

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`id_customer`,`users_id_users`),
  ADD KEY `fk_customer_users1_idx` (`users_id_users`);

--
-- Indexes for table `drying1`
--
ALTER TABLE `drying1`
  ADD PRIMARY KEY (`id`,`stock_wrapping_id`,`stock_drying1_id`),
  ADD KEY `fk_drying1_stock_drying11_idx` (`stock_drying1_id`),
  ADD KEY `fk_drying1_stock_wrapping1_idx` (`stock_wrapping_id`);

--
-- Indexes for table `drying2`
--
ALTER TABLE `drying2`
  ADD PRIMARY KEY (`id`,`stock_frezer_id`,`stock_drying2_id`),
  ADD KEY `fk_drying2_stock_drying21_idx` (`stock_drying2_id`),
  ADD KEY `fk_drying2_stock_frezer1_idx` (`stock_frezer_id`);

--
-- Indexes for table `drying3`
--
ALTER TABLE `drying3`
  ADD PRIMARY KEY (`id`,`stock_drying3_id`,`stock_cool_id`),
  ADD KEY `fk_drying3_stock_drying31_idx` (`stock_drying3_id`),
  ADD KEY `fk_drying3_stock_cool1_idx` (`stock_cool_id`);

--
-- Indexes for table `filling`
--
ALTER TABLE `filling`
  ADD PRIMARY KEY (`id`,`stock_probaku_id`,`stock_filling_id`),
  ADD KEY `fk_filling_stock_filling1_idx` (`stock_filling_id`),
  ADD KEY `fk_filling_stock_probaku1_idx` (`stock_probaku_id`);

--
-- Indexes for table `frezer`
--
ALTER TABLE `frezer`
  ADD PRIMARY KEY (`id`,`stock_frezer_id`,`stock_drying1_id`),
  ADD KEY `fk_frezer_stock_frezer1_idx` (`stock_frezer_id`),
  ADD KEY `fk_frezer_stock_drying11_idx` (`stock_drying1_id`);

--
-- Indexes for table `fumigasi`
--
ALTER TABLE `fumigasi`
  ADD PRIMARY KEY (`id`,`stock_fumigasi_id`,`stock_drying2_id`),
  ADD KEY `fk_fumigasi_stock_drying21_idx` (`stock_drying2_id`),
  ADD KEY `fk_fumigasi_stock_fumigasi1_idx` (`stock_fumigasi_id`);

--
-- Indexes for table `history_bahanmasuk`
--
ALTER TABLE `history_bahanmasuk`
  ADD PRIMARY KEY (`id`,`jenis_id_jenis`,`kategori_id_kategori`,`stock_probaku_id`,`author_session`),
  ADD KEY `fk_history_bahanmasuk_jenis1_idx` (`jenis_id_jenis`),
  ADD KEY `fk_history_bahanmasuk_kategori1_idx` (`kategori_id_kategori`),
  ADD KEY `fk_history_bahanmasuk_stock_probaku1_idx` (`stock_probaku_id`);

--
-- Indexes for table `history_cincin`
--
ALTER TABLE `history_cincin`
  ADD PRIMARY KEY (`id`,`stock_cincin_id_stock_cincin`,`stock_cincin_produk_id_produk`,`author_session`),
  ADD KEY `fk_history_cincin_stock_cincin1_idx` (`stock_cincin_id_stock_cincin`,`stock_cincin_produk_id_produk`);

--
-- Indexes for table `history_cukai`
--
ALTER TABLE `history_cukai`
  ADD PRIMARY KEY (`id`,`stock_cukai_id_stock_cukai`,`stock_cukai_id_sub_produk`,`stock_cukai_produk_id_produk`),
  ADD KEY `fk_history_cukai_stock_cukai1_idx` (`stock_cukai_id_stock_cukai`,`stock_cukai_id_sub_produk`,`stock_cukai_produk_id_produk`),
  ADD KEY `fk_history_cukai_packing1_idx` (`packing_id_packing`);

--
-- Indexes for table `history_kemasan`
--
ALTER TABLE `history_kemasan`
  ADD PRIMARY KEY (`id`,`stock_kemasan_id_stock_kemasan`,`stock_kemasan_kemasan_id_kemasan`,`stock_kemasan_produk_id_produk`),
  ADD KEY `fk_history_kemasan_stock_kemasan1_idx` (`stock_kemasan_id_stock_kemasan`,`stock_kemasan_kemasan_id_kemasan`,`stock_kemasan_produk_id_produk`);

--
-- Indexes for table `history_stiker`
--
ALTER TABLE `history_stiker`
  ADD PRIMARY KEY (`id`,`stock_stiker_id_stock_stiker`,`stock_stiker_produk_id_produk`),
  ADD KEY `fk_history_stiker_stock_stiker1_idx` (`stock_stiker_id_stock_stiker`,`stock_stiker_produk_id_produk`);

--
-- Indexes for table `jenis`
--
ALTER TABLE `jenis`
  ADD PRIMARY KEY (`id_jenis`);

--
-- Indexes for table `jenis_has_produk`
--
ALTER TABLE `jenis_has_produk`
  ADD PRIMARY KEY (`id_jnpro`,`jenis_id_jenis`,`produk_id_produk`),
  ADD KEY `fk_jenis_has_produk_produk1_idx` (`produk_id_produk`),
  ADD KEY `fk_jenis_has_produk_jenis1_idx` (`jenis_id_jenis`);

--
-- Indexes for table `kategori`
--
ALTER TABLE `kategori`
  ADD PRIMARY KEY (`id_kategori`);

--
-- Indexes for table `kemasan`
--
ALTER TABLE `kemasan`
  ADD PRIMARY KEY (`id_kemasan`);

--
-- Indexes for table `laporan`
--
ALTER TABLE `laporan`
  ADD PRIMARY KEY (`id_laporan`);

--
-- Indexes for table `level_user`
--
ALTER TABLE `level_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `noc_inrfs`
--
ALTER TABLE `noc_inrfs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `noc_items`
--
ALTER TABLE `noc_items`
  ADD PRIMARY KEY (`id`,`stock_packing_id`,`stock_packing_id_sub_produk`,`noc_inrfs_id`),
  ADD KEY `fk_noc_items_stock_packing1_idx` (`stock_packing_id`,`stock_packing_id_sub_produk`),
  ADD KEY `fk_noc_items_noc_inrfs1_idx` (`noc_inrfs_id`);

--
-- Indexes for table `order`
--
ALTER TABLE `order`
  ADD PRIMARY KEY (`id`,`users_id`),
  ADD KEY `fk_order_users1_idx` (`users_id`),
  ADD KEY `fk_order_rfs1_idx` (`rfs_id`);

--
-- Indexes for table `order_items`
--
ALTER TABLE `order_items`
  ADD PRIMARY KEY (`id`,`stock_bagan_id`,`stock_bagan_id_sub_produk`,`stock_bagan_id_produk`,`order_id`),
  ADD KEY `fk_order_items_stock_bagan1_idx` (`stock_bagan_id`,`stock_bagan_id_sub_produk`,`stock_bagan_id_produk`),
  ADD KEY `fk_order_items_order1_idx` (`order_id`);

--
-- Indexes for table `packing`
--
ALTER TABLE `packing`
  ADD PRIMARY KEY (`id`,`stock_packing_id`,`stock_qc_id`),
  ADD KEY `fk_packing_stock_qc1_idx` (`stock_qc_id`),
  ADD KEY `fk_packing_stock_packing1_idx` (`stock_packing_id`);

--
-- Indexes for table `penerimaan`
--
ALTER TABLE `penerimaan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_penerimaan_penjualan1_idx` (`penjualan_id`);

--
-- Indexes for table `penjualan`
--
ALTER TABLE `penjualan`
  ADD PRIMARY KEY (`id`,`customer_id_customer`),
  ADD KEY `fk_penjualan_customer1_idx` (`customer_id_customer`);

--
-- Indexes for table `pressing`
--
ALTER TABLE `pressing`
  ADD PRIMARY KEY (`id`,`stock_pressing_id`,`stock_binding_id`),
  ADD KEY `fk_pressing_stock_pressing1_idx` (`stock_pressing_id`),
  ADD KEY `fk_pressing_stock_binding1_idx` (`stock_binding_id`);

--
-- Indexes for table `produk`
--
ALTER TABLE `produk`
  ADD PRIMARY KEY (`id_produk`);

--
-- Indexes for table `quality_control`
--
ALTER TABLE `quality_control`
  ADD PRIMARY KEY (`id`,`stock_qc_id`,`stock_drying3_id`),
  ADD KEY `fk_quality_control_stock_qc1_idx` (`stock_qc_id`),
  ADD KEY `fk_quality_control_stock_drying31_idx` (`stock_drying3_id`);

--
-- Indexes for table `return`
--
ALTER TABLE `return`
  ADD PRIMARY KEY (`id_return`,`jenis_id_jenis`,`sub_produk_id_sub_produk`),
  ADD KEY `fk_return_sub_produk1_idx` (`sub_produk_id_sub_produk`),
  ADD KEY `fk_return_stock_rfs1_idx` (`stock_rfs_id`),
  ADD KEY `fk_return_jenis1_idx` (`jenis_id_jenis`),
  ADD KEY `fk_return_stock_packing1_idx` (`stock_packing_id`),
  ADD KEY `fk_return_stock_binding1_idx` (`stock_binding_id`),
  ADD KEY `fk_return_stock_qc1_idx` (`stock_qc_id`),
  ADD KEY `fk_return_stock_wrapping1_idx` (`stock_wrapping_id`),
  ADD KEY `fk_return_stock_bagan1_idx` (`stock_bagan_id`);

--
-- Indexes for table `rfs`
--
ALTER TABLE `rfs`
  ADD PRIMARY KEY (`id`,`users_id`),
  ADD KEY `fk_rfs_users1_idx` (`users_id`);

--
-- Indexes for table `rfs_items`
--
ALTER TABLE `rfs_items`
  ADD PRIMARY KEY (`id`,`rfs_id`,`stock_rfs_id`,`stock_rfs_id_sub_produk`),
  ADD KEY `fk_rfs_items_rfs1_idx` (`rfs_id`),
  ADD KEY `fk_rfs_items_stock_rfs1_idx` (`stock_rfs_id`,`stock_rfs_id_sub_produk`);

--
-- Indexes for table `setting`
--
ALTER TABLE `setting`
  ADD PRIMARY KEY (`idsetting`);

--
-- Indexes for table `stock_bagan`
--
ALTER TABLE `stock_bagan`
  ADD PRIMARY KEY (`id`,`sub_produk_id_sub_produk`,`sub_produk_id_produk`),
  ADD KEY `fk_stock_bagan_sub_produk1_idx` (`sub_produk_id_sub_produk`,`sub_produk_id_produk`);

--
-- Indexes for table `stock_binding`
--
ALTER TABLE `stock_binding`
  ADD PRIMARY KEY (`id`,`jenis_has_produk_id_jnpro`,`jenis_has_produk_id_jenis`,`jenis_has_produk_id_produk`),
  ADD KEY `fk_stock_binding_jenis_has_produk1_idx` (`jenis_has_produk_id_jnpro`,`jenis_has_produk_id_jenis`,`jenis_has_produk_id_produk`);

--
-- Indexes for table `stock_cincin`
--
ALTER TABLE `stock_cincin`
  ADD PRIMARY KEY (`id_stock_cincin`,`produk_id_produk`),
  ADD KEY `fk_stock_cincin_produk1_idx` (`produk_id_produk`);

--
-- Indexes for table `stock_cool`
--
ALTER TABLE `stock_cool`
  ADD PRIMARY KEY (`id`,`jenis_has_produk_id_jnpro`,`jenis_has_produk_id_jenis`,`jenis_has_produk_id_produk`),
  ADD KEY `fk_stock_cool_jenis_has_produk1_idx` (`jenis_has_produk_id_jnpro`,`jenis_has_produk_id_jenis`,`jenis_has_produk_id_produk`);

--
-- Indexes for table `stock_cukai`
--
ALTER TABLE `stock_cukai`
  ADD PRIMARY KEY (`id_stock_cukai`,`sub_produk_id_sub_produk`,`sub_produk_produk_id_produk`),
  ADD KEY `fk_stock_cukai_sub_produk1_idx` (`sub_produk_id_sub_produk`,`sub_produk_produk_id_produk`);

--
-- Indexes for table `stock_drying1`
--
ALTER TABLE `stock_drying1`
  ADD PRIMARY KEY (`id`,`jenis_has_produk_id_jnpro`,`jenis_has_produk_id_jenis`,`jenis_has_produk_id_produk`),
  ADD KEY `fk_stock_drying1_jenis_has_produk1_idx` (`jenis_has_produk_id_jnpro`,`jenis_has_produk_id_jenis`,`jenis_has_produk_id_produk`);

--
-- Indexes for table `stock_drying2`
--
ALTER TABLE `stock_drying2`
  ADD PRIMARY KEY (`id`,`jenis_has_produk_id_jnpro`,`jenis_has_produk_id_jenis`,`jenis_has_produk_id_produk`),
  ADD KEY `fk_stock_drying2_jenis_has_produk1_idx` (`jenis_has_produk_id_jnpro`,`jenis_has_produk_id_jenis`,`jenis_has_produk_id_produk`);

--
-- Indexes for table `stock_drying3`
--
ALTER TABLE `stock_drying3`
  ADD PRIMARY KEY (`id`,`jenis_has_produk_id_jnpro`,`jenis_has_produk_id_jenis`,`jenis_has_produk_id_produk`),
  ADD KEY `fk_stock_drying3_jenis_has_produk1_idx` (`jenis_has_produk_id_jnpro`,`jenis_has_produk_id_jenis`,`jenis_has_produk_id_produk`);

--
-- Indexes for table `stock_filling`
--
ALTER TABLE `stock_filling`
  ADD PRIMARY KEY (`id`,`jenis_has_produk_id_jnpro`,`jenis_has_produk_id_jenis`,`jenis_has_produk_id_produk`),
  ADD KEY `fk_stock_filling_jenis_has_produk1_idx` (`jenis_has_produk_id_jnpro`,`jenis_has_produk_id_jenis`,`jenis_has_produk_id_produk`);

--
-- Indexes for table `stock_frezer`
--
ALTER TABLE `stock_frezer`
  ADD PRIMARY KEY (`id`,`jenis_has_produk_id_jnpro`,`jenis_has_produk_id_jenis`,`jenis_has_produk_id_produk`),
  ADD KEY `fk_stock_frezer_jenis_has_produk1_idx` (`jenis_has_produk_id_jnpro`,`jenis_has_produk_id_jenis`,`jenis_has_produk_id_produk`);

--
-- Indexes for table `stock_fumigasi`
--
ALTER TABLE `stock_fumigasi`
  ADD PRIMARY KEY (`id`,`jenis_has_produk_id_jnpro`,`jenis_has_produk_id_jenis`,`jenis_has_produk_id_produk`),
  ADD KEY `fk_stock_fumigasi_jenis_has_produk1_idx` (`jenis_has_produk_id_jnpro`,`jenis_has_produk_id_jenis`,`jenis_has_produk_id_produk`);

--
-- Indexes for table `stock_kemasan`
--
ALTER TABLE `stock_kemasan`
  ADD PRIMARY KEY (`id_stock_kemasan`,`kemasan_id_kemasan`,`produk_id_produk`),
  ADD KEY `fk_stock_kemasan_kemasan1_idx` (`kemasan_id_kemasan`),
  ADD KEY `fk_stock_kemasan_produk1_idx` (`produk_id_produk`);

--
-- Indexes for table `stock_packing`
--
ALTER TABLE `stock_packing`
  ADD PRIMARY KEY (`id`,`sub_produk_id_sub_produk`),
  ADD KEY `fk_stock_packing_sub_produk1_idx` (`sub_produk_id_sub_produk`);

--
-- Indexes for table `stock_pressing`
--
ALTER TABLE `stock_pressing`
  ADD PRIMARY KEY (`id`,`jenis_has_produk_id_jnpro`,`jenis_has_produk_id_jenis`,`jenis_has_produk_id_produk`),
  ADD KEY `fk_stock_pressing_jenis_has_produk1_idx` (`jenis_has_produk_id_jnpro`,`jenis_has_produk_id_jenis`,`jenis_has_produk_id_produk`);

--
-- Indexes for table `stock_probaku`
--
ALTER TABLE `stock_probaku`
  ADD PRIMARY KEY (`id`,`jenis_id_jenis`,`kategori_id_kategori`),
  ADD KEY `fk_stock_probaku_jenis1_idx` (`jenis_id_jenis`),
  ADD KEY `fk_stock_probaku_kategori1_idx` (`kategori_id_kategori`);

--
-- Indexes for table `stock_qc`
--
ALTER TABLE `stock_qc`
  ADD PRIMARY KEY (`id`,`jenis_has_produk_id_jnpro`,`jenis_has_produk_id_jenis`,`jenis_has_produk_id_produk`),
  ADD KEY `fk_stock_cool_jenis_has_produk2_idx` (`jenis_has_produk_id_jnpro`,`jenis_has_produk_id_jenis`,`jenis_has_produk_id_produk`);

--
-- Indexes for table `stock_rfs`
--
ALTER TABLE `stock_rfs`
  ADD PRIMARY KEY (`id`,`sub_produk_id_sub_produk`),
  ADD KEY `fk_stock_rfs_sub_produk1_idx` (`sub_produk_id_sub_produk`);

--
-- Indexes for table `stock_stiker`
--
ALTER TABLE `stock_stiker`
  ADD PRIMARY KEY (`id_stock_stiker`,`produk_id_produk`),
  ADD KEY `fk_stock_stiker_produk1_idx` (`produk_id_produk`);

--
-- Indexes for table `stock_wrapping`
--
ALTER TABLE `stock_wrapping`
  ADD PRIMARY KEY (`id`,`jenis_has_produk_id_jnpro`,`jenis_has_produk_id_jenis`,`jenis_has_produk_id_produk`),
  ADD KEY `fk_stock_wrapping_jenis_has_produk1_idx` (`jenis_has_produk_id_jnpro`,`jenis_has_produk_id_jenis`,`jenis_has_produk_id_produk`);

--
-- Indexes for table `sub_produk`
--
ALTER TABLE `sub_produk`
  ADD PRIMARY KEY (`id_sub_produk`,`produk_id_produk`,`kemasan_id_kemasan`),
  ADD KEY `fk_sub_produk_produk1_idx` (`produk_id_produk`),
  ADD KEY `fk_sub_produk_kemasan1_idx` (`kemasan_id_kemasan`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`,`level_user_id_level`),
  ADD UNIQUE KEY `email_UNIQUE` (`email`),
  ADD KEY `fk_users_level_user_idx` (`level_user_id_level`);

--
-- Indexes for table `wrapping`
--
ALTER TABLE `wrapping`
  ADD PRIMARY KEY (`id`,`stock_probaku_id`,`stock_wrapping_id`,`stock_pressing_id`),
  ADD KEY `fk_wrapping_stock_pressing1_idx` (`stock_pressing_id`),
  ADD KEY `fk_wrapping_stock_probaku1_idx` (`stock_probaku_id`),
  ADD KEY `fk_wrapping_stock_wrapping1_idx` (`stock_wrapping_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `binding`
--
ALTER TABLE `binding`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cerutu_terjual`
--
ALTER TABLE `cerutu_terjual`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cicil`
--
ALTER TABLE `cicil`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cool`
--
ALTER TABLE `cool`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `drying1`
--
ALTER TABLE `drying1`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `drying2`
--
ALTER TABLE `drying2`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `drying3`
--
ALTER TABLE `drying3`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `filling`
--
ALTER TABLE `filling`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `frezer`
--
ALTER TABLE `frezer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fumigasi`
--
ALTER TABLE `fumigasi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `history_bahanmasuk`
--
ALTER TABLE `history_bahanmasuk`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `history_cincin`
--
ALTER TABLE `history_cincin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `history_cukai`
--
ALTER TABLE `history_cukai`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `history_kemasan`
--
ALTER TABLE `history_kemasan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `history_stiker`
--
ALTER TABLE `history_stiker`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jenis`
--
ALTER TABLE `jenis`
  MODIFY `id_jenis` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `jenis_has_produk`
--
ALTER TABLE `jenis_has_produk`
  MODIFY `id_jnpro` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;

--
-- AUTO_INCREMENT for table `kategori`
--
ALTER TABLE `kategori`
  MODIFY `id_kategori` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `kemasan`
--
ALTER TABLE `kemasan`
  MODIFY `id_kemasan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `laporan`
--
ALTER TABLE `laporan`
  MODIFY `id_laporan` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `level_user`
--
ALTER TABLE `level_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `noc_inrfs`
--
ALTER TABLE `noc_inrfs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `noc_items`
--
ALTER TABLE `noc_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `order`
--
ALTER TABLE `order`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `order_items`
--
ALTER TABLE `order_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `packing`
--
ALTER TABLE `packing`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `penerimaan`
--
ALTER TABLE `penerimaan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `penjualan`
--
ALTER TABLE `penjualan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pressing`
--
ALTER TABLE `pressing`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `produk`
--
ALTER TABLE `produk`
  MODIFY `id_produk` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;

--
-- AUTO_INCREMENT for table `quality_control`
--
ALTER TABLE `quality_control`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `return`
--
ALTER TABLE `return`
  MODIFY `id_return` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rfs`
--
ALTER TABLE `rfs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rfs_items`
--
ALTER TABLE `rfs_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `setting`
--
ALTER TABLE `setting`
  MODIFY `idsetting` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `stock_bagan`
--
ALTER TABLE `stock_bagan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `stock_binding`
--
ALTER TABLE `stock_binding`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `stock_cincin`
--
ALTER TABLE `stock_cincin`
  MODIFY `id_stock_cincin` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `stock_cool`
--
ALTER TABLE `stock_cool`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `stock_cukai`
--
ALTER TABLE `stock_cukai`
  MODIFY `id_stock_cukai` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `stock_drying1`
--
ALTER TABLE `stock_drying1`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `stock_drying2`
--
ALTER TABLE `stock_drying2`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `stock_drying3`
--
ALTER TABLE `stock_drying3`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `stock_filling`
--
ALTER TABLE `stock_filling`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `stock_frezer`
--
ALTER TABLE `stock_frezer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `stock_fumigasi`
--
ALTER TABLE `stock_fumigasi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `stock_kemasan`
--
ALTER TABLE `stock_kemasan`
  MODIFY `id_stock_kemasan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `stock_packing`
--
ALTER TABLE `stock_packing`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `stock_pressing`
--
ALTER TABLE `stock_pressing`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `stock_probaku`
--
ALTER TABLE `stock_probaku`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `stock_qc`
--
ALTER TABLE `stock_qc`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `stock_rfs`
--
ALTER TABLE `stock_rfs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `stock_stiker`
--
ALTER TABLE `stock_stiker`
  MODIFY `id_stock_stiker` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `stock_wrapping`
--
ALTER TABLE `stock_wrapping`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sub_produk`
--
ALTER TABLE `sub_produk`
  MODIFY `id_sub_produk` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `wrapping`
--
ALTER TABLE `wrapping`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `binding`
--
ALTER TABLE `binding`
  ADD CONSTRAINT `fk_binding_stock_binding1` FOREIGN KEY (`stock_binding_id`) REFERENCES `stock_binding` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_binding_stock_filling1` FOREIGN KEY (`stock_filling_id`) REFERENCES `stock_filling` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_binding_stock_probaku1` FOREIGN KEY (`stock_probaku_id`) REFERENCES `stock_probaku` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `cerutu_terjual`
--
ALTER TABLE `cerutu_terjual`
  ADD CONSTRAINT `fk_cerutu_terjual_penjualan1` FOREIGN KEY (`penjualan_id`,`penjualan_customer_id_customer`) REFERENCES `penjualan` (`id`, `customer_id_customer`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_cerutu_terjual_stock_bagan1` FOREIGN KEY (`stock_bagan_id`) REFERENCES `stock_bagan` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `cicil`
--
ALTER TABLE `cicil`
  ADD CONSTRAINT `fk_cicil_penjualan1` FOREIGN KEY (`penjualan_id`) REFERENCES `penjualan` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `cool`
--
ALTER TABLE `cool`
  ADD CONSTRAINT `fk_cool_stock_cool1` FOREIGN KEY (`stock_cool_id`) REFERENCES `stock_cool` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_cool_stock_fumigasi1` FOREIGN KEY (`stock_fumigasi_id`) REFERENCES `stock_fumigasi` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `customer`
--
ALTER TABLE `customer`
  ADD CONSTRAINT `fk_customer_users1` FOREIGN KEY (`users_id_users`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `drying1`
--
ALTER TABLE `drying1`
  ADD CONSTRAINT `fk_drying1_stock_drying11` FOREIGN KEY (`stock_drying1_id`) REFERENCES `stock_drying1` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_drying1_stock_wrapping1` FOREIGN KEY (`stock_wrapping_id`) REFERENCES `stock_wrapping` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `drying2`
--
ALTER TABLE `drying2`
  ADD CONSTRAINT `fk_drying2_stock_drying21` FOREIGN KEY (`stock_drying2_id`) REFERENCES `stock_drying2` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_drying2_stock_frezer1` FOREIGN KEY (`stock_frezer_id`) REFERENCES `stock_frezer` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `drying3`
--
ALTER TABLE `drying3`
  ADD CONSTRAINT `fk_drying3_stock_cool1` FOREIGN KEY (`stock_cool_id`) REFERENCES `stock_cool` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_drying3_stock_drying31` FOREIGN KEY (`stock_drying3_id`) REFERENCES `stock_drying3` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `filling`
--
ALTER TABLE `filling`
  ADD CONSTRAINT `fk_filling_stock_filling1` FOREIGN KEY (`stock_filling_id`) REFERENCES `stock_filling` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_filling_stock_probaku1` FOREIGN KEY (`stock_probaku_id`) REFERENCES `stock_probaku` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `frezer`
--
ALTER TABLE `frezer`
  ADD CONSTRAINT `fk_frezer_stock_drying11` FOREIGN KEY (`stock_drying1_id`) REFERENCES `stock_drying1` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_frezer_stock_frezer1` FOREIGN KEY (`stock_frezer_id`) REFERENCES `stock_frezer` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `fumigasi`
--
ALTER TABLE `fumigasi`
  ADD CONSTRAINT `fk_fumigasi_stock_drying21` FOREIGN KEY (`stock_drying2_id`) REFERENCES `stock_drying2` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_fumigasi_stock_fumigasi1` FOREIGN KEY (`stock_fumigasi_id`) REFERENCES `stock_fumigasi` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `history_bahanmasuk`
--
ALTER TABLE `history_bahanmasuk`
  ADD CONSTRAINT `fk_history_bahanmasuk_jenis1` FOREIGN KEY (`jenis_id_jenis`) REFERENCES `jenis` (`id_jenis`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_history_bahanmasuk_kategori1` FOREIGN KEY (`kategori_id_kategori`) REFERENCES `kategori` (`id_kategori`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_history_bahanmasuk_stock_probaku1` FOREIGN KEY (`stock_probaku_id`) REFERENCES `stock_probaku` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `history_cincin`
--
ALTER TABLE `history_cincin`
  ADD CONSTRAINT `fk_history_cincin_stock_cincin1` FOREIGN KEY (`stock_cincin_id_stock_cincin`,`stock_cincin_produk_id_produk`) REFERENCES `stock_cincin` (`id_stock_cincin`, `produk_id_produk`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `history_cukai`
--
ALTER TABLE `history_cukai`
  ADD CONSTRAINT `fk_history_cukai_packing1` FOREIGN KEY (`packing_id_packing`) REFERENCES `packing` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_history_cukai_stock_cukai1` FOREIGN KEY (`stock_cukai_id_stock_cukai`,`stock_cukai_id_sub_produk`,`stock_cukai_produk_id_produk`) REFERENCES `stock_cukai` (`id_stock_cukai`, `sub_produk_id_sub_produk`, `sub_produk_produk_id_produk`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `history_kemasan`
--
ALTER TABLE `history_kemasan`
  ADD CONSTRAINT `fk_history_kemasan_stock_kemasan1` FOREIGN KEY (`stock_kemasan_id_stock_kemasan`,`stock_kemasan_kemasan_id_kemasan`,`stock_kemasan_produk_id_produk`) REFERENCES `stock_kemasan` (`id_stock_kemasan`, `kemasan_id_kemasan`, `produk_id_produk`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `history_stiker`
--
ALTER TABLE `history_stiker`
  ADD CONSTRAINT `fk_history_stiker_stock_stiker1` FOREIGN KEY (`stock_stiker_id_stock_stiker`,`stock_stiker_produk_id_produk`) REFERENCES `stock_stiker` (`id_stock_stiker`, `produk_id_produk`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `jenis_has_produk`
--
ALTER TABLE `jenis_has_produk`
  ADD CONSTRAINT `fk_jenis_has_produk_jenis1` FOREIGN KEY (`jenis_id_jenis`) REFERENCES `jenis` (`id_jenis`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_jenis_has_produk_produk1` FOREIGN KEY (`produk_id_produk`) REFERENCES `produk` (`id_produk`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `noc_items`
--
ALTER TABLE `noc_items`
  ADD CONSTRAINT `fk_noc_items_noc_inrfs1` FOREIGN KEY (`noc_inrfs_id`) REFERENCES `noc_inrfs` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_noc_items_stock_packing1` FOREIGN KEY (`stock_packing_id`,`stock_packing_id_sub_produk`) REFERENCES `stock_packing` (`id`, `sub_produk_id_sub_produk`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `order`
--
ALTER TABLE `order`
  ADD CONSTRAINT `fk_order_rfs1` FOREIGN KEY (`rfs_id`) REFERENCES `rfs` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_order_users1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `order_items`
--
ALTER TABLE `order_items`
  ADD CONSTRAINT `fk_order_items_order1` FOREIGN KEY (`order_id`) REFERENCES `order` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_order_items_stock_bagan1` FOREIGN KEY (`stock_bagan_id`,`stock_bagan_id_sub_produk`,`stock_bagan_id_produk`) REFERENCES `stock_bagan` (`id`, `sub_produk_id_sub_produk`, `sub_produk_id_produk`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `packing`
--
ALTER TABLE `packing`
  ADD CONSTRAINT `fk_packing_stock_packing1` FOREIGN KEY (`stock_packing_id`) REFERENCES `stock_packing` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_packing_stock_qc1` FOREIGN KEY (`stock_qc_id`) REFERENCES `stock_qc` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `penerimaan`
--
ALTER TABLE `penerimaan`
  ADD CONSTRAINT `fk_penerimaan_penjualan1` FOREIGN KEY (`penjualan_id`) REFERENCES `penjualan` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `penjualan`
--
ALTER TABLE `penjualan`
  ADD CONSTRAINT `fk_penjualan_customer1` FOREIGN KEY (`customer_id_customer`) REFERENCES `customer` (`id_customer`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `pressing`
--
ALTER TABLE `pressing`
  ADD CONSTRAINT `fk_pressing_stock_binding1` FOREIGN KEY (`stock_binding_id`) REFERENCES `stock_binding` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_pressing_stock_pressing1` FOREIGN KEY (`stock_pressing_id`) REFERENCES `stock_pressing` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `quality_control`
--
ALTER TABLE `quality_control`
  ADD CONSTRAINT `fk_quality_control_stock_drying31` FOREIGN KEY (`stock_drying3_id`) REFERENCES `stock_drying3` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_quality_control_stock_qc1` FOREIGN KEY (`stock_qc_id`) REFERENCES `stock_qc` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `return`
--
ALTER TABLE `return`
  ADD CONSTRAINT `fk_return_jenis1` FOREIGN KEY (`jenis_id_jenis`) REFERENCES `jenis` (`id_jenis`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_return_stock_bagan1` FOREIGN KEY (`stock_bagan_id`) REFERENCES `stock_bagan` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_return_stock_binding1` FOREIGN KEY (`stock_binding_id`) REFERENCES `stock_binding` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_return_stock_packing1` FOREIGN KEY (`stock_packing_id`) REFERENCES `stock_packing` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_return_stock_qc1` FOREIGN KEY (`stock_qc_id`) REFERENCES `stock_qc` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_return_stock_rfs1` FOREIGN KEY (`stock_rfs_id`) REFERENCES `stock_rfs` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_return_stock_wrapping1` FOREIGN KEY (`stock_wrapping_id`) REFERENCES `stock_wrapping` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_return_sub_produk1` FOREIGN KEY (`sub_produk_id_sub_produk`) REFERENCES `sub_produk` (`id_sub_produk`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `rfs`
--
ALTER TABLE `rfs`
  ADD CONSTRAINT `fk_rfs_users1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `rfs_items`
--
ALTER TABLE `rfs_items`
  ADD CONSTRAINT `fk_rfs_items_rfs1` FOREIGN KEY (`rfs_id`) REFERENCES `rfs` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_rfs_items_stock_rfs1` FOREIGN KEY (`stock_rfs_id`,`stock_rfs_id_sub_produk`) REFERENCES `stock_rfs` (`id`, `sub_produk_id_sub_produk`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `stock_bagan`
--
ALTER TABLE `stock_bagan`
  ADD CONSTRAINT `fk_stock_bagan_sub_produk1` FOREIGN KEY (`sub_produk_id_sub_produk`,`sub_produk_id_produk`) REFERENCES `sub_produk` (`id_sub_produk`, `produk_id_produk`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `stock_binding`
--
ALTER TABLE `stock_binding`
  ADD CONSTRAINT `fk_stock_binding_jenis_has_produk1` FOREIGN KEY (`jenis_has_produk_id_jnpro`,`jenis_has_produk_id_jenis`,`jenis_has_produk_id_produk`) REFERENCES `jenis_has_produk` (`id_jnpro`, `jenis_id_jenis`, `produk_id_produk`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `stock_cincin`
--
ALTER TABLE `stock_cincin`
  ADD CONSTRAINT `fk_stock_cincin_produk1` FOREIGN KEY (`produk_id_produk`) REFERENCES `produk` (`id_produk`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `stock_cool`
--
ALTER TABLE `stock_cool`
  ADD CONSTRAINT `fk_stock_cool_jenis_has_produk1` FOREIGN KEY (`jenis_has_produk_id_jnpro`,`jenis_has_produk_id_jenis`,`jenis_has_produk_id_produk`) REFERENCES `jenis_has_produk` (`id_jnpro`, `jenis_id_jenis`, `produk_id_produk`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `stock_cukai`
--
ALTER TABLE `stock_cukai`
  ADD CONSTRAINT `fk_stock_cukai_sub_produk1` FOREIGN KEY (`sub_produk_id_sub_produk`,`sub_produk_produk_id_produk`) REFERENCES `sub_produk` (`id_sub_produk`, `produk_id_produk`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `stock_drying1`
--
ALTER TABLE `stock_drying1`
  ADD CONSTRAINT `fk_stock_drying1_jenis_has_produk1` FOREIGN KEY (`jenis_has_produk_id_jnpro`,`jenis_has_produk_id_jenis`,`jenis_has_produk_id_produk`) REFERENCES `jenis_has_produk` (`id_jnpro`, `jenis_id_jenis`, `produk_id_produk`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `stock_drying2`
--
ALTER TABLE `stock_drying2`
  ADD CONSTRAINT `fk_stock_drying2_jenis_has_produk1` FOREIGN KEY (`jenis_has_produk_id_jnpro`,`jenis_has_produk_id_jenis`,`jenis_has_produk_id_produk`) REFERENCES `jenis_has_produk` (`id_jnpro`, `jenis_id_jenis`, `produk_id_produk`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `stock_drying3`
--
ALTER TABLE `stock_drying3`
  ADD CONSTRAINT `fk_stock_drying3_jenis_has_produk1` FOREIGN KEY (`jenis_has_produk_id_jnpro`,`jenis_has_produk_id_jenis`,`jenis_has_produk_id_produk`) REFERENCES `jenis_has_produk` (`id_jnpro`, `jenis_id_jenis`, `produk_id_produk`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `stock_filling`
--
ALTER TABLE `stock_filling`
  ADD CONSTRAINT `fk_stock_filling_jenis_has_produk1` FOREIGN KEY (`jenis_has_produk_id_jnpro`,`jenis_has_produk_id_jenis`,`jenis_has_produk_id_produk`) REFERENCES `jenis_has_produk` (`id_jnpro`, `jenis_id_jenis`, `produk_id_produk`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `stock_frezer`
--
ALTER TABLE `stock_frezer`
  ADD CONSTRAINT `fk_stock_frezer_jenis_has_produk1` FOREIGN KEY (`jenis_has_produk_id_jnpro`,`jenis_has_produk_id_jenis`,`jenis_has_produk_id_produk`) REFERENCES `jenis_has_produk` (`id_jnpro`, `jenis_id_jenis`, `produk_id_produk`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `stock_fumigasi`
--
ALTER TABLE `stock_fumigasi`
  ADD CONSTRAINT `fk_stock_fumigasi_jenis_has_produk1` FOREIGN KEY (`jenis_has_produk_id_jnpro`,`jenis_has_produk_id_jenis`,`jenis_has_produk_id_produk`) REFERENCES `jenis_has_produk` (`id_jnpro`, `jenis_id_jenis`, `produk_id_produk`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `stock_kemasan`
--
ALTER TABLE `stock_kemasan`
  ADD CONSTRAINT `fk_stock_kemasan_kemasan1` FOREIGN KEY (`kemasan_id_kemasan`) REFERENCES `kemasan` (`id_kemasan`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_stock_kemasan_produk1` FOREIGN KEY (`produk_id_produk`) REFERENCES `produk` (`id_produk`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `stock_packing`
--
ALTER TABLE `stock_packing`
  ADD CONSTRAINT `fk_stock_packing_sub_produk1` FOREIGN KEY (`sub_produk_id_sub_produk`) REFERENCES `sub_produk` (`id_sub_produk`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `stock_pressing`
--
ALTER TABLE `stock_pressing`
  ADD CONSTRAINT `fk_stock_pressing_jenis_has_produk1` FOREIGN KEY (`jenis_has_produk_id_jnpro`,`jenis_has_produk_id_jenis`,`jenis_has_produk_id_produk`) REFERENCES `jenis_has_produk` (`id_jnpro`, `jenis_id_jenis`, `produk_id_produk`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `stock_probaku`
--
ALTER TABLE `stock_probaku`
  ADD CONSTRAINT `fk_stock_probaku_jenis1` FOREIGN KEY (`jenis_id_jenis`) REFERENCES `jenis` (`id_jenis`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_stock_probaku_kategori1` FOREIGN KEY (`kategori_id_kategori`) REFERENCES `kategori` (`id_kategori`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `stock_qc`
--
ALTER TABLE `stock_qc`
  ADD CONSTRAINT `fk_stock_cool_jenis_has_produk2` FOREIGN KEY (`jenis_has_produk_id_jnpro`,`jenis_has_produk_id_jenis`,`jenis_has_produk_id_produk`) REFERENCES `jenis_has_produk` (`id_jnpro`, `jenis_id_jenis`, `produk_id_produk`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `stock_rfs`
--
ALTER TABLE `stock_rfs`
  ADD CONSTRAINT `fk_stock_rfs_sub_produk1` FOREIGN KEY (`sub_produk_id_sub_produk`) REFERENCES `sub_produk` (`id_sub_produk`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `stock_stiker`
--
ALTER TABLE `stock_stiker`
  ADD CONSTRAINT `fk_stock_stiker_produk1` FOREIGN KEY (`produk_id_produk`) REFERENCES `produk` (`id_produk`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `stock_wrapping`
--
ALTER TABLE `stock_wrapping`
  ADD CONSTRAINT `fk_stock_wrapping_jenis_has_produk1` FOREIGN KEY (`jenis_has_produk_id_jnpro`,`jenis_has_produk_id_jenis`,`jenis_has_produk_id_produk`) REFERENCES `jenis_has_produk` (`id_jnpro`, `jenis_id_jenis`, `produk_id_produk`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `sub_produk`
--
ALTER TABLE `sub_produk`
  ADD CONSTRAINT `fk_sub_produk_kemasan1` FOREIGN KEY (`kemasan_id_kemasan`) REFERENCES `kemasan` (`id_kemasan`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_sub_produk_produk1` FOREIGN KEY (`produk_id_produk`) REFERENCES `produk` (`id_produk`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `fk_users_level_user` FOREIGN KEY (`level_user_id_level`) REFERENCES `level_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `wrapping`
--
ALTER TABLE `wrapping`
  ADD CONSTRAINT `fk_wrapping_stock_pressing1` FOREIGN KEY (`stock_pressing_id`) REFERENCES `stock_pressing` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_wrapping_stock_probaku1` FOREIGN KEY (`stock_probaku_id`) REFERENCES `stock_probaku` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_wrapping_stock_wrapping1` FOREIGN KEY (`stock_wrapping_id`) REFERENCES `stock_wrapping` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
