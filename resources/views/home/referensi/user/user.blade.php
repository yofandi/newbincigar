@extends('../../layout')

@section('title')
<title>BIN - ERP : User Management</title>
@endsection

@section('content')
<div class="row page-titles">
	<div class="col-md-5 align-self-center">
		<h3 class="text-themecolor">User Management</h3>
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a href="javascript:void(0)">Begin</a></li>
			<li class="breadcrumb-item active">User Management</li>
		</ol>
	</div>
</div>
<div class="row">
	<div class="col-12">
		<div class="card">
			<div class="card-body">
				<div class="row">
					<div class="col-md-12" id="session-flsh"></div>
					<div class="col-md-6">
						<legend>List</legend>
					</div>
					<div class="col-md-2 offset-md-4">
						<button type="button" data-link="{{ route('user.adduser') }}" class="btn btn-success btn-rounded btn-icons" id="tambah-user"><i class="fa fa-plus"></i></button>
					</div>
				</div>
				<div class="table-responsive m-t-40">
					<table id="example" class="display nowrap table table-hover table-striped table-bordered">
						<thead>
							<tr>
								<th>ID</th>
								<th>Nama</th>
								<th>User</th>
								<th>Email</th>
								<th>Jabatan</th>
								<th>Aksi</th>
							</tr>
						</thead>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="modal-pass" tabindex="-1" role="dialog" aria-labelledby="title-pass" aria-hidden="true" style="display: none;">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<div class="card">
				<div class="card-header">
					<div class="modal-header">
						<h4 class="modal-title" id="title-pass"></h4>
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					</div>
				</div>
				<div class="card-body">
					<form id="form-pass" method="POST">
						<div class="modal-body row">
							<div class="form-group col-md-12">
								<label class="control-label">New Password</label>
								<input type="text" class="form-control" id="pass-update" name="pass-update" placeholder="New Password">
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" onclick="save_pass()" class="btn btn-primary waves-effect text-left">Save</button>
							<button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Close</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

{{-- add & update data user  --}}
<div class="modal fade" id="modal-user" tabindex="-1" role="dialog" aria-labelledby="title" aria-hidden="true" style="display: none;">
	<div class="modal-dialog modal-xl">
		<div class="modal-content">
			<div class="card">
				<div class="card-header">
					<div class="modal-header">
						<h4 class="modal-title" id="title"></h4>
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					</div>
				</div>
				<div class="card-body">
					<form id="form-user" method="POST">
						<div class="modal-body row">
							<div class="form-group col-md-12">
								<label class="control-label">Nama Lengkap</label>
								<input type="text" class="form-control" id="nama" name="nama" placeholder="Nama">
							</div>
							<div class="form-group col-md-6">
								<label class="control-label">Email</label>
								<input type="email" class="form-control" id="email" name="email" placeholder="Email">
							</div>
							<div class="form-group col-md-6">
								<label class="control-label">Username</label>
								<input type="text" class="form-control" id="username" name="username" placeholder="Username">
							</div>
							<div class="form-group col-md-12" id="show-password">
								<label class="control-label">Password</label>
								<input type="password" class="form-control" id="password" name="password" placeholder="Password">
							</div>
							<div class="form-group col-md-12">
								<label class="control-label">No. HP</label>
								<input type="text" class="form-control" id="no_hp" name="no_hp" placeholder="No. HP">
							</div>
							<div class="form-group col-md-6">
								<label class="control-label">Status</label>
								<select class="form-control" name="status" id="status">
								</select>
							</div>
							<div class="form-group col-md-6">
								<label class="control-label">level</label>
								<select class="form-control" name="level" id="level">
								</select>
							</div>
							<div class="form-group col-md-12">
								<label class="control-label">Jabatan</label>
								<textarea class="form-control" id="alamat" name="alamat" placeholder="Jabatan"></textarea>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" onclick="save_method()" class="btn btn-primary waves-effect text-left">Save</button>
							<button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Close</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('javascript')
<script src="{{ asset('node_modules/datatables/datatables.min.js') }}"></script>
<script type="text/javascript">
	var table = $('#example').DataTable({
		"lengthMenu": [
		[3,5,10, 25, 50, 100, 150, 200, -1],
		[3,5,10, 25, 50, 100, 150, 200, "All"]
		],
		"order": [
		[0, 'desc']
		],
		processing: true,
		serverSide: true,
		ajax: 'users/json',
		columns: [
		{ data: 'rowNumber', name: 'rowNumber' },
		{ data: 'name', name: 'name' },
		{ data: 'username', name: 'username' },
		{ data: 'email', name: 'email' },
		{ data: 'alamat_user', name: 'alamat_user' },
		{
			data: null,
			"bSortable": false,
			mRender: function (o) { return '<button type="button" class="btn btn-info btn-rounded btn-xs" id="update-pass" data-link="users/uppass/'+ o.id +'" data-title="Update Password | ID: '+ o.id +' | Nama: '+ o.name +'" data-id="'+ o.id +'"><i class="fa fa-circle"></i></button><button type="button" data-link="/users/edit/'+ o.id +'" class="btn btn-warning btn-rounded btn-xs" id="update-user" data-title="Update - User | ID: '+ o.id +'" data-id="'+ o.id +'" data-name="'+ o.name +'" data-username="'+ o.username +'" data-email="'+ o.email +'" data-no_hp="'+ o.no_hp +'" data-status="'+ o.status_users +'" data-level="'+ o.level_user_id_level +'" data-alamat="'+ o.alamat_user +'"><i class="mdi mdi-dots-horizontal"></i></button><button type="button" class="btn btn-danger btn-rounded btn-xs" data-link="users/delete/'+ o.id +'"  data-id="'+ o.id +'" id="delete-user"><i class="fa fa-trash"></i></button>'; }
		},
		],
		"displayLength": 10,
	});
	$('#example tbody').on('click', 'tr.group', function() {
		var currentOrder = table.order()[0];
		if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
			table.order([2, 'desc']).draw();
		} else {
			table.order([2, 'asc']).draw();
		}
	});
	$('#example tbody').on('click', 'tr.group', function() {
		var currentOrder = table.order()[0];
		if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
			table.order([2, 'desc']).draw();
		} else {
			table.order([2, 'asc']).draw();
		}
	});

	function loadsts(position,active = '') {
		let data = ['ACTIVE','OFF'];
		opt = '<option value=""></option>';
		for (var i = 0; i < data.length; i++) {
			opt += '<option value="'+ data[i] +'"';
			if (data[i] == active) {
				opt +=  'selected';
			}
			opt += '>'+ data[i] +'</option>';
		}
		$('#' + position).html(opt);
	}

	function loadlevel(position,active = '') {
		$.ajax({
			url: '/level',
			type: 'GET',
			dataType: 'json',
		})
		.done(function(data) {
			opt = '<option value=""></option>';
			for (let i = 0; i < data.length; i++) {
				opt += '<option value="'+ data[i].id +'"';
				if (data[i].id == active) {
					opt +=  'selected';
				}
				opt += '>'+ data[i].level +'</option>';
			}
			$('#' + position).html(opt);
		});
	}

	$(document).on('click', '#tambah-user', function(event) {
		let clik = $(this);
		$('.modal-header #title').text('Tambah User');
		$('#form-user').attr('action', clik.data('link'));
		$('#form-user').find('input').val('');
		$('#form-user').find('select').val('');
		$('#show-password').fadeIn();
		loadsts('status');
		loadlevel('level');
		$('#modal-user').modal('show');
	});

	$(document).on('click', '#update-user', function(event) {
		let clik = $(this);
		$('.modal-header #title').text(clik.data('title'));
		$('#form-user').attr('action', clik.data('link'));
		$('#form-user').find('input').val('');
		$('#form-user').find('select').val('');
		$('#show-password').hide();
		$('#nama').val(clik.data('name'));
		$('#email').val(clik.data('email'));
		$('#username').val(clik.data('username'));
		$('#no_hp').val(clik.data('no_hp'));
		$('#alamat').val(clik.data('alamat'));
		loadsts('status',clik.data('status'));
		loadlevel('level',clik.data('level'));
		$('#modal-user').modal('show');
	});

	$(document).on('click', '#update-pass', function(event) {
		let clik = $(this);
		$('.modal-header #title-pass').text(clik.data('title'));
		$('#form-pass').attr('action', clik.data('link'));
		$('#form-pass').find('input').val('');
		$('#modal-pass').modal('show');
	});

	$(document).on('click', '#delete-user', function() {
		let dhk = confirm('Apakah anda yakin ingin menghapus data ini ?');
		if (dhk) {
			let id = $(this).data("id");
			let token = $("meta[name='csrf-token']").attr("content");
			$.ajax(
			{
				url: '/users/delete/' + id,
				type: 'DELETE',
				data: {
					"_token": token,
				},
				success: function (data){
					alert(data);
					$('#example').DataTable().ajax.reload();
				}
			});
		}
	});

	function save_pass(argument) {
		let token = $("meta[name='csrf-token']").attr("content");
		let link = $('#form-pass').attr('action');
		let password = $('#pass-update').val();
		$.ajax({
			url: link,
			type: 'POST',
			dataType: 'json',
			data: {
				"_token": token,
				password: password
			},
		})
		.done(function(data) {
			alert(data);
			$('#example').DataTable().ajax.reload();
			$('#modal-pass').modal('toggle');
		});
	}

	function save_method(argument) {
		let token = $("meta[name='csrf-token']").attr("content");
		let link = $('#form-user').attr('action');
		let nama = $('#nama').val();
		let email = $('#email').val();
		let username = $('#username').val();
		let password = $('#password').val();
		let no_hp = $('#no_hp').val();
		let status =  $('#status').val();
		let level = $('#level').val();
		let alamat = $('#alamat').val();

		$.ajax({
			url: link,
			type: 'POST',
			dataType: 'json',
			data: {
				"_token": token,
				name: nama,
				username: username,
				email: email,
				password: password,
				no_hp: no_hp,
				status: status,
				level: level,
				alamat: alamat
			},
		})
		.done(function(data) {
			alert(data);
			$('#example').DataTable().ajax.reload();
			$('#modal-user').modal('toggle');
		});
	}
</script>
@endsection