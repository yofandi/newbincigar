<script>
	$('#stock_showit').hide();
	$('.spinner_nekr').hide();
	$('#loadingdelete').hide();
	let token = $("meta[name='csrf-token']").attr("content");
	$.extend(true, $.fn.datetimepicker.defaults, {
		icons: {
			time: 'far fa-clock',
			date: 'far fa-calendar',
			up: 'fas fa-arrow-up',
			down: 'fas fa-arrow-down',
			previous: 'fas fa-chevron-left',
			next: 'fas fa-chevron-right',
			today: 'fas fa-calendar-check',
			clear: 'far fa-trash-alt',
			close: 'far fa-times-circle'
		}
	});

	const produk = {!! json_encode($produk) !!}
	produk.unshift({id_produk: '',produk: ''});

	function format_number(number, prefix, thousand_separator, decimal_separator)
	{
		var 	thousand_separator = thousand_separator || ',',
		decimal_separator = decimal_separator || '.',
		regex		= new RegExp('[^' + decimal_separator + '\\d]', 'g'),
		number_string = number.toString().replace(regex, ''),
		split	  = number_string.split(decimal_separator),
		rest 	  = split[0].length % 3,
		result 	  = split[0].substr(0, rest),
		thousands = split[0].substr(rest).match(/\d{3}/g);

		if (thousands) {
			separator = rest ? thousand_separator : '';
			result += separator + thousands.join(thousand_separator);
		}
		result = split[1] != undefined ? result + decimal_separator + split[1] : result;
		return prefix == undefined ? result : (result ? prefix + result : '');
	};

	var table = $('#table_cukai').DataTable({
		"lengthMenu": [
		[3,5,10, 25, 50, 100, 150, 200, -1],
		[3,5,10, 25, 50, 100, 150, 200, "All"]
		],
		"order": [
		[0, 'desc']
		],
		processing: true,
		serverSide: true,
		ajax: '/data_cukai/json',
		columns: [
		{ data: 'DT_RowIndex', name: 'DT_RowIndex' },
		{ data: 'tanggal_cukai', name: 'tanggal_cukai' },
		{ data: 'produk', name: 'produk' },
		{ data: 'sub_sam', name: 'sub_sam' },
		{ data: 'masuk_cukai', name: 'masuk_cukai' },
		{ data: 'terpakai_cukailama', name: 'terpakai_cukailama' },
		{ data: 'terpakai_cukaibaru', name: 'terpakai_cukaibaru' },
		{ data: 'sisa_cukai', name: 'sisa_cukai' },
		{ data: 'ket_hiscukai', name: 'ket_hiscukai' },
		{
			data: null,
			"bSortable": false,
			mRender: function (o) { 
				return '<button type="button" class="btn btn-info btn-rounded btn-xs" id="update-cukai" data-title="Update Stock Cukai | ID: '+ o.id +'" data-link="/data_cukai/upp/'+ o.id +'/'+ o.masuk_cukai +'/'+ o.terpakai_cukailama +'/'+ o.terpakai_cukaibaru +'" data-tanggal_cukai="'+ o.tanggal_cukai +'" data-id="'+ o.id +'" data-hargaid="'+ o.harga_cukai_id +'" data-produk="'+ o.stock_cukai_produk_id_produk +'" data-subproduk="'+ o.stock_cukai_id_sub_produk +'" data-stock="'+ o.stockawal +'" data-masuk="'+ o.masuk_cukai +'" data-terpakailama="'+ o.terpakai_cukailama +'" data-terpakaibaru="'+ o.terpakai_cukaibaru +'" data-sisa="'+ o.sisa_cukai +'" data-ket_hiscukai="'+ o.ket_hiscukai +'"><i class="mdi mdi-dots-horizontal"></i></button><button type="button" class="btn btn-danger btn-rounded btn-xs" id="delete-cukai" data-link="/data_cukai/del/'+ o.id +'/'+ o.masuk_cukai +'/'+ o.terpakai_cukailama +'/'+ o.terpakai_cukaibaru +'/'+ o.stockawal +'" data-id="'+ o.id +'" data-produk="'+ o.stock_cukai_produk_id_produk +'" data-subproduk="'+ o.stock_cukai_id_sub_produk +'"><i class="fa fa-trash"></i></button>'; }
			},
			],
			"displayLength": 10,
		});
	$('#table_cukai tbody').on('click', 'tr.group', function() {
		var currentOrder = table.order()[0];
		if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
			table.order([2, 'desc']).draw();
		} else {
			table.order([2, 'asc']).draw();
		}
	});
	$('#table_cukai tbody').on('click', 'tr.group', function() {
		var currentOrder = table.order()[0];
		if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
			table.order([2, 'desc']).draw();
		} else {
			table.order([2, 'asc']).draw();
		}
	});

	function lproduk(position,like = "") {
		let opt = '';
		for (var i =  0; i < produk.length; i++) {
			opt += '<option value="'+ produk[i].id_produk +'"';
			if (produk[i].id_produk == like) { opt += 'selected'; }
			opt +='>'+ produk[i].produk +'</option>';
		}
		$('#' + position).html(opt);
	}

	function lsubproduk(position,based,like = "") {
		$.ajax({
			url: '/data_cukai/getsubproduk',
			type: 'POST',
			dataType: 'json',
			data: {
				"_token": token,
				produk: based
			},
		})
		.done(function(data) {
			let opt = '<option value=""></option>';
			for (var i = 0; i < data.length; i++) {
				opt += '<option value="'+ data[i].id_sub_produk +'"';
				if (data[i].id_sub_produk == like) { opt += 'selected'; }
				opt += '>'+ data[i].sub_kode +' - '+ data[i].sub_produk +'</option>';
			}
			$('#' + position).html(opt);
		});
	}

	function getstocksub(position,basedpro,basedsub,val = 0,id = '') {
		$.ajax({
			url: '/data_cukai/getstockrow',
			type: 'POST',
			dataType: 'json',
			data: {
				"_token": token,
				produk: basedpro,
				subproduk: basedsub,
				val: val,
				id: id,
			},
		})
		.done(function(data) {
			$('#harga_cukai').val(format_number(data.harga, ''));
			if (position == '') {} else {$('#' + position).val(data.hasil);}
		});
	}

	function search_stock() {
		let produk_in = $('#produk_in').val();
		let subproduk_in = $('#subproduk_in').val();
		getstocksub('stock',produk_in,subproduk_in);
	}

	function Refresh() {
		$('#table_cukai').DataTable().ajax.reload();
	}
	
	function save_method() {
		let link = $('#form-cukai').attr('action');
		let tanggal_cukai = $('#tanggal_cukai').val();
		let produk_in = $('#produk_in').val();
		let subproduk_in = $('#subproduk_in').val();
		let stock = $('#stock').val();
		let masuk = $('#masuk').val();
		let terpakailama = $('#terpakailama').val();
		let terpakaibaru = $('#terpakaibaru').val();
		let ket_hiscukai = $('#ket_hiscukai').val();
		let harga_cukai = $('#harga_cukai').val();

		$.ajax({
			url: link,
			type: 'POST',
			dataType: 'json',
			data: {
				"_token": token,
				tanggal_cukai: tanggal_cukai,
				produk: produk_in,
				subproduk: subproduk_in,
				stock: stock,
				masuk: masuk,
				terpakailama: terpakailama,
				terpakaibaru: terpakaibaru,
				ket: ket_hiscukai,
				harga_cukai: harga_cukai,
			},
			beforeSend: function() {
				isProcessing = true;
				$('.spinner_nekr').fadeIn();
			},
			success: function(data) {	
				$('.spinner_nekr').hide();
				isProcessing = false;
				alert(data);
				$('#table_cukai').DataTable().ajax.reload();
				$('#modal-cukai').modal('toggle');
			}
		})
		.done(function(data) {
		});
	}

	$(document).on('click', '#tambah-cukai', function(event) {
		let click = $(this);
		$('.modal-header #title').text('Tambah Stock cukai');
		$('#form-cukai').attr('action', click.data('link'));
		$('#form-cukai').find('input').val('');
		$('#form-cukai').find('select').val('');
		$('#form-cukai').find('textarea').val('');
		$('#stock_showit').fadeIn();
		$('#produk').removeAttr('disabled');
		$('#subproduk').removeAttr('disabled');
		lproduk('produk');
		$("#tanggal_cukai").datetimepicker({
			format: 'YYYY-MM-DD HH:mm:ss',
		}).data("DateTimePicker").date(new Date());

		$('#produk').change(function(event) {
			let bal = $(this).val();
			$('#produk_in').val(bal);
			lsubproduk('subproduk',bal);
			$('#subproduk').change(function(event) {
				let sal = $(this).val();
				$('#subproduk_in').val(sal);
			});
		});		

		$('#modal-cukai').modal('show');
	});

	$(document).on('click', '#update-cukai', function(event) {
		let click = $(this);
		let id = $(this).data('id');
		let link = $(this).data('link');
		$('.modal-header #title').text(click.data('title'));
		$('#form-cukai').attr('action', link);
		$('#form-cukai').find('input').val('');
		$('#form-cukai').find('select').val('');
		$('#form-cukai').find('textarea').val('');
		$('#stock_showit').hide();
		$('#produk').attr('disabled','disabled');
		$('#subproduk').attr('disabled','disabled');

		lproduk('produk', $(this).data('produk'));
		lsubproduk('subproduk',$(this).data('produk'),click.data('subproduk'));
		getstocksub('',$(this).data('produk'),click.data('subproduk'),1,click.data('hargaid'));

		$("#tanggal_cukai").datetimepicker({
			format: 'YYYY-MM-DD HH:mm:ss',
		}).data("DateTimePicker").date(click.data('tanggal_cukai'));

		$('#produk_in').val(click.data('produk'));
		$('#subproduk_in').val(click.data('subproduk'));
		$('#stock').val(click.data('stock'))
		$('#masuk').val(click.data('masuk'));
		$('#terpakailama').val(click.data('terpakailama'));
		$('#terpakaibaru').val(click.data('terpakaibaru'));
		$('#ket_hiscukai').val(click.data('ket_hiscukai'));

		$('#modal-cukai').modal('show');	
	});

	$(document).on('click', '#delete-cukai', function(event) {
		let jdb =confirm('Apakah anda yakin ingin menghapus data ini ?');
		if (jdb) {
			let id = $(this).data("id");
			let link = $(this).data("link");
			let token = $("meta[name='csrf-token']").attr("content");
			let produk_in = $(this).data('produk');
			let subproduk_in = $(this).data('subproduk');
			$.ajax(
			{
				url: link,
				type: 'DELETE',
				data: {
					"_token": token,
					produk: produk_in,
					subproduk: subproduk_in,
				},
				beforeSend: function() { 
					isProcessing = true;
					$('#loadingdelete').fadeIn();
				},
				success: function (data){
					$('#loadingdelete').hide();
					isProcessing = false;
					alert(data);
					$('#table_cukai').DataTable().ajax.reload();
				}
			});
		}
	});
</script>