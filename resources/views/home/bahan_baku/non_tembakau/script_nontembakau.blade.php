<script>
	$(document).ready(function() {
		let token = $("meta[name='csrf-token']").attr("content");

		$('#loadingdelete').hide();
		$('.spinner_nekr').hide();
		$('.spinner_cari').hide();
		$('.spinner_ame').hide();
		$('#showhidd').hide();

		function format_number(number, prefix, thousand_separator, decimal_separator)
		{
			var 	thousand_separator = thousand_separator || ',',
			decimal_separator = decimal_separator || '.',
			regex		= new RegExp('[^' + decimal_separator + '\\d]', 'g'),
			number_string = number.toString().replace(regex, ''),
			split	  = number_string.split(decimal_separator),
			rest 	  = split[0].length % 3,
			result 	  = split[0].substr(0, rest),
			thousands = split[0].substr(rest).match(/\d{3}/g);

			if (thousands) {
				separator = rest ? thousand_separator : '';
				result += separator + thousands.join(thousand_separator);
			}
			result = split[1] != undefined ? result + decimal_separator + split[1] : result;
			return prefix == undefined ? result : (result ? prefix + result : '');
		};

		const produk = {!! json_encode($produk) !!}
		produk.unshift({id_produk: '',kode_produk: 'NULL',produk: 'NULL'});

		function lproduk(position,like = "") {
			let opt = '';
			for (var i =  0; i < produk.length; i++) {
				opt += '<option value="'+ produk[i].id_produk +'"';
				if (produk[i].id_produk == like) { opt += 'selected'; }
				opt +='>'+ produk[i].kode_produk +' - '+ produk[i].produk +'</option>';
			}
			$('#' + position).html(opt);
		}

		function lsubproduk(position,based,like = "") {
			$.ajax({
				url: '/data_cukai/getsubproduk',
				type: 'POST',
				dataType: 'json',
				data: {
					"_token": token,
					produk: based
				},
			})
			.done(function(data) {
				let opt = '<option value=""></option>';
				for (var i = 0; i < data.length; i++) {
					opt += '<option value="'+ data[i].id_sub_produk +'"';
					if (data[i].id_sub_produk == like) { opt += 'selected'; }
					opt += '>'+ data[i].sub_kode +' - '+ data[i].sub_produk +'</option>';
				}
				$('#' + position).html(opt);
			});
		}

		$(document).on('click', '#refreshbutton', function(event) {
			$('#table-nontembakau').DataTable().ajax.reload();
		});

		var table = $('#table-nontembakau').DataTable({
			"lengthMenu": [
			[3,5,10, 25, 50, 100, 150, 200, -1],
			[3,5,10, 25, 50, 100, 150, 200, "All"]
			],
			"order": [
			[0, 'desc']
			],
			processing: true,
			serverSide: true,
			ajax: '/api/data_nontembakau/json',
			dataSrc:"",
			columns: [
			{ 
				data: 'DT_RowIndex', name: 'DT_RowIndex' 
			},
			{
				data: null,
				"bSortable": false,
				mRender: function (o) { 
					$button = '<button type="button" class="btn btn-info btn-rounded btn-xs" id="update-nontembakau"';
					$button += ' data-link="/api/data_nontembakau/upp/'+ o.idnontembakau +'"';
					$button += ' data-tanggal="'+ o.tanggal_cincin +'"';
					$button += ' data-id_produk="'+ o.id_produk +'"';
					$button += ' data-kode_produk="'+ o.kode_produk +'"';
					$button += ' data-id_sub_produk="'+ o.id_sub_produk +'"';
					$button += ' data-sub_kode="'+ o.sub_kode +'"';
					$button += ' data-id_kemasan="'+ o.id_kemasan +'"';
					$button += ' data-kemasan="'+ o.nama_kemasan +'"';

					$button += ' data-id_stock_cincin="'+ o.id_stock_cincin +'"';
					$button += ' data-id_stock_cukai="'+ o.id_stock_cukai +'"';
					$button += ' data-id_stock_stiker="'+ o.id_stock_stiker +'"';
					$button += ' data-id_stock_kemasan="'+ o.id_stock_kemasan +'"';

					$button += ' data-idhargacincin="'+ o.idhargacincin +'"';
					$button += ' data-idhargastiker="'+ o.idhargastiker +'"';
					$button += ' data-idhargakemasan="'+ o.idhargakemasan +'"';
					$button += ' data-idhargacukai="'+ o.idhargacukai +'"';

					$button += ' data-hargacincin="'+ o.hargacincin +'"';
					$button += ' data-hargacukai="'+ o.hargacukai +'"';
					$button += ' data-hargastiker_dalam="'+ o.hargastiker_dalam +'"';
					$button += ' data-hargastiker_luar="'+ o.hargastiker_luar +'"';
					$button += ' data-hargakemasan="'+ o.hargakemasan +'"';

					$button += ' data-awlstokcincin="'+ o.awlstokcincin +'"';
					$button += ' data-masuk="'+ o.masuk +'"';
					$button += ' data-terpakai="'+ o.terpakai +'"';
					$button += ' data-afkir="'+ o.afkir +'"';

					$button += ' data-awlstockstikerluar="'+ o.awlstockstikerluar +'"';
					$button += ' data-masuk_luar="'+ o.masuk_luar +'"';
					$button += ' data-pakai_luar="'+ o.pakai_luar +'"';
					$button += ' data-awlstockstikerdalam="'+ o.awlstockstikerdalam +'"';
					$button += ' data-masuk_dalam="'+ o.masuk_dalam +'"';
					$button += ' data-pakai_dalam="'+ o.pakai_dalam +'"';

					$button += ' data-stockawalcukai1="'+ o.stockawalcukai1 +'"';
					$button += ' data-masuk_cukai="'+ o.masuk_cukai +'"';
					$button += ' data-terpakai_cukailama="'+ o.terpakai_cukailama +'"';
					$button += ' data-stockawalcukai2="'+ o.stockawalcukai2 +'"';
					$button += ' data-masuk_cukai2="'+ o.masuk_cukai2 +'"';
					$button += ' data-terpakai_cukaibaru="'+ o.terpakai_cukaibaru +'"';

					$button += ' data-awlstokkemasan="'+ o.awlstokkemasan +'"';
					$button += ' data-masuk_kemasan="'+ o.masuk_kemasan +'"';
					$button += ' data-terpakai_kemasan="'+ o.terpakai_kemasan +'"';
					$button += ' data-afkir_kemasan="'+ o.afkir_kemasan +'"';

					$button += ' data-ket_cin="'+ o.ket_cin +'"';

					$button += '><i class="mdi mdi-dots-horizontal"></i></button>';
					$button += '<button type="button" class="btn btn-warning btn-rounded btn-xs" id="update-aks" data-id="'+ o.idnontembakau +'" data-link="/api/data_nontembakau/uppaks/'+ o.idnontembakau +'"><i class="mdi mdi-dots-horizontal"></i></button>';
					$button += '<button type="button" class="btn btn-danger btn-rounded btn-xs" id="delete-nontembakau"';
					$button += ' data-link="/api/data_nontembakau/delaks/'+ o.idnontembakau +'"';
					$button += ' data-id_produk="'+ o.id_produk +'"';
					$button += ' data-id_sub_produk="'+ o.id_sub_produk +'"';
					$button += ' data-id_kemasan="'+ o.id_kemasan +'"';
					$button += '><i class="fa fa-trash"></i></button>';
					return $button; 
				}
			},
			{ data: 'tanggal_cincin', name: 'tanggal_cincin' },
			{ data: 'kode_produk', name: 'kode_produk' },
			{ data: 'sub_kode', name: 'sub_kode' },
			{ data: 'sub_produk', name: 'sub_produk' },
			{ data: 'nama_kemasan', name: 'nama_kemasan' },
			{ data: 'masuk', name: 'masuk' },
			{ data: 'terpakai', name: 'terpakai' },
			{ data: 'afkir', name: 'afkir' },
			{ data: 'sisa', name: 'sisa' },

			{ data: 'masuk_luar', name: 'masuk_luar' },
			{ data: 'pakai_luar', name: 'pakai_luar' },
			{ data: 'hasil_luar', name: 'hasil_luar' },
			{ data: 'masuk_dalam', name: 'masuk_dalam' },
			{ data: 'pakai_dalam', name: 'pakai_dalam' },
			{ data: 'hasil_dalam', name: 'hasil_dalam' },

			{ data: 'masuk_cukai', name: 'masuk_cukai' },
			{ data: 'terpakai_cukailama', name: 'terpakai_cukailama' },
			{ data: 'sisa_cukai', name: 'sisa_cukai' },
			{ data: 'masuk_cukai2', name: 'masuk_cukai2' },
			{ data: 'terpakai_cukaibaru', name: 'terpakai_cukaibaru' },
			{ data: 'sisa_cukai2', name: 'sisa_cukai2' },

			{ data: 'masuk_kemasan', name: 'masuk_kemasan' },
			{ data: 'terpakai_kemasan', name: 'terpakai_kemasan' },
			{ data: 'afkir_kemasan', name: 'afkir_kemasan' },
			{ data: 'stok_now', name: 'stock_now' },
			{ data: 'ket_cin', name: 'ket_cin' },
			],
			"displayLength": 10,
		});$('#table-nontembakau tbody').on('click', 'tr.group', function() {
			var currentOrder = table.order()[0];
			if (currentOrder[0] === 2 && currentOrder[2] === 'asc') {
				table.order([2, 'desc']).draw();
			} else {
				table.order([2, 'asc']).draw();
			}
		});
		$('#table-nontembakau tbody').on('click', 'tr.group', function() {
			var currentOrder = table.order()[0];
			if (currentOrder[0] === 2 && currentOrder[2] === 'asc') {
				table.order([2, 'desc']).draw();
			} else {
				table.order([2, 'asc']).draw();
			}
		});

		function appendform(position,data = [],sts) {
			$('#idaksesorischeck').val( data.id_aksesoris);
			$string = data.id_aksesoris;
			$name = data.nama_aksesoris;
			$stock = data.stock_aksesoris;
			if (sts == 1) {
				$awal = data.awal_aksesoris;
				$masuk = data.masuk_aksesoris;
				$terpakai = data.terpakai_aksesoris;
				$stawal = ( $awal == null ? [] : $awal.split(','));
				$masukgte = ( $masuk == null ? [] : $masuk.split(','));
				$terpakaigte = ( $terpakai == null ? [] : $terpakai.split(','));
			}
			let array = ( $string == null ? [] : $string.split(','));
			let karti = ( $name == null ? [] : $name.split(','));
			let jam = ( $stock == null ? [] : $stock.split(','));
			$html = '<div class="row">';
			if (array.length > 0 || sts != 0) {	
				$html += '<label class="control-label">Aksesoris:</label>';
				for (var i = 0; i < array.length; i++) {
					$html += '<div class="col-md-12">';
					if (sts == 1) {
						$html += '<input type="hidden" id="id_his_'+ karti[i] +'" name="id_his_'+ karti[i] +'" value="'+ array[i] +'">';

						$html += '<input type="hidden" id="stockawal_'+ karti[i] +'" name="stockawal_'+ karti[i] +'" value="'+ $stawal[i] +'">';

						$html +='<input type="hidden" class="form-control" id="masukhidden_'+ karti[i] +'" name="masukhidden_'+ karti[i] +'" value="'+ $masukgte[i] +'" required>';
						$html +='<input type="hidden" class="form-control" id="terpakaihidden_'+ karti[i] +'" name="terpakaihidden_'+ karti[i] +'" value="'+ $terpakaigte[i] +'" required>';

						$ken = karti[i] +': <b> Stock Awal: '+ $stawal[i] +' </b>';
					} else {
						$ken = karti[i] +': <b> Stock Awal: '+ jam[i] +' </b>';
					}
					$html += '<label class="control-label">'+ $ken +'</label>';
					$html +='<div class="row">';
					$html +='<div class="col-md-6">';
					$html += '<input type="hidden" id="id_'+ karti[i] +'" name="id_'+ karti[i] +'" value="'+ array[i] +'">';
					$html += '<input type="hidden" id="stock_'+ karti[i] +'" name="stock_'+ karti[i] +'" value="'+ jam[i] +'">';
					$html +='<input type="number" class="form-control" id="masuk_'+ karti[i] +'" name="masuk_'+ karti[i] +'" placeholder="Masuk '+ karti[i] +'"';
					if (sts == 1) {
						$html += ' value="'+ $masukgte[i] +'"'
					}
					$html +=' required>';
					$html +='</div>';
					$html +='<div class="col-md-6">';
					$html +='<input type="number" class="form-control" id="terpakai_'+ karti[i] +'" name="terpakai_'+ karti[i] +'" placeholder="Terpakai '+ karti[i] +'"';
					if (sts == 1) {
						$html += ' value="'+ $terpakaigte[i] +'"'
					}
					$html +=' required>';
					$html +='</div>';
					$html +='</div>';
					$html += '</div>';
				}
			} else {
				$html += '';
			}
			$html += '</div>';

			$('#' + position).html($html);
		}

		$(document).on('click', '#tambah-nontembakau', function(event) {
			let clik = $(this);
			$('#form-nontembakau').find('input[type=text]').val('');
			$('#form-nontembakau').find('input[type=hidden]').val('');
			$('#form-nontembakau').find('input[type=number]').val(0);
			$('#form-nontembakau').find('select').val('');
			$('#form-nontembakau').find('textarea').val('');

			$('#showhidd').fadeIn();
			$('.modal-header #title').text('Tambah Non Tembakau');		 
			$('#form-nontembakau').attr('action', clik.data('link'));
			
			$('#kode-produk').removeAttr('disabled');
			$('#kode-sub').removeAttr('disabled');

			lproduk('kode-produk');


			$(document).on('change', '#kode-produk', function(event) {
				let kje = $( this ).val();
				$('#idproduk-hidden').val(kje);
				lsubproduk('kode-sub',kje);
				$(document).on('change', '#kode-sub', function(event) {
					$yah = $( this ).val();
					$('#idsubproduk-hidden').val($yah);
				});
			});

			$("#tanggal").datetimepicker({
				format: 'YYYY-MM-DD HH:mm:ss',
			}).data("DateTimePicker").date(new Date());

			$('#modal-nontembakau').modal('show');
		});

		$(document).on('click', '#update-nontembakau', function(event) {
			let clik = $(this);
			$('#form-nontembakau').find('input[type=text]').val('');
			$('#form-nontembakau').find('input[type=hidden]').val('');
			$('#form-nontembakau').find('input[type=number]').val(0);
			$('#form-nontembakau').find('select').val('');
			$('#form-nontembakau').find('textarea').val('');

			// tampilan lable, input dll...
			$('#showhidd').hide();
			$('.modal-header #title').text('Update Non Tembakau : Tanggal: '+ clik.data('tanggal') +' - Produk: '+ clik.data('kode_produk') +' - Sub Produk: '+ clik.data('sub_kode') +'');		 
			$('#form-nontembakau').attr('action', clik.data('link'));

			$('#stockcincin').text(' Jumlah: ' + clik.data('awlstokcincin'));
			$('#stockstiker').text(' Jumlah: *Luar: ' + clik.data('awlstockstikerluar') + ': *Dalam: ' + clik.data('awlstockstikerdalam'));
			$('#stockcukai').text(' Jumlah: *Cukai1: ' + clik.data('stockawalcukai1') + ': *Cukai2: ' + clik.data('stockawalcukai2'));
			$('#nama_kemasan').text(clik.data('kemasan'))
			$('#stockkemasan').text(' Jumlah: ' + clik.data('awlstokkemasan'));

			$text = ' Harga Cincin: Rp. '+format_number(clik.data('hargacincin'),'')+'; Harga Cukai: Rp. '+format_number(clik.data('hargacukai'),'')+'; Harga Stiker: *luar-Rp. '+format_number(clik.data('hargastiker_dalam'),'')+'  *dalam-Rp. '+format_number(clik.data('hargastiker_luar'),'')+'; Harga Packaging: Rp. '+format_number(clik.data('hargakemasan'),'')+';';
			$('#hargaset_show').text($text);

			lproduk('kode-produk',clik.data('id_produk'));
			lsubproduk('kode-sub',clik.data('id_produk'),clik.data('id_sub_produk'));
			// ***
			// isi form
			$("#tanggal").datetimepicker({
				format: 'YYYY-MM-DD HH:mm:ss',
			}).data("DateTimePicker").date(clik.data('tanggal'));

			$('#kode-produk').attr('disabled','disabled');
			$('#kode-sub').attr('disabled','disabled');

			$('#idproduk-hidden').val(clik.data('id_produk'));
			$('#idsubproduk-hidden').val(clik.data('id_sub_produk'));
			$('#id_kemasan').val(clik.data('id_kemasan'));
			// id non tembakau
			$('#id_stock_cincin').val(clik.data('id_stock_cincin'));
			$('#id_stock_cukai').val(clik.data('id_stock_cukai'));
			$('#id_stock_stiker').val(clik.data('id_stock_stiker'));
			$('#id_stock_kemasan').val(clik.data('id_stock_kemasan'));
			//id harga non tembakau
			$('#idhargacincin').val(clik.data('idhargacincin'));
			$('#idhargacukai').val(clik.data('idhargacukai'));
			$('#idhargastiker').val(clik.data('idhargastiker'));
			$('#idhargakemasan').val(clik.data('idhargakemasan'));
			// stok awal non tembakau
			$('#stockawalcincin').val(clik.data('awlstokcincin'));
			$('#stockawalstikerluar').val(clik.data('awlstockstikerluar'));
			$('#stockawalstikerdalam').val(clik.data('awlstockstikerdalam'));
			$('#stockawalcukai1').val(clik.data('stockawalcukai1'));
			$('#stockawalcukai2').val(clik.data('stockawalcukai2'));
			$('#stockawalkemasan').val(clik.data('awlstokkemasan'));
			// data yang berubah
			$('#masuk_cincin').val(clik.data('masuk'));
			$('#terpakai_cincin').val(clik.data('terpakai'));
			$('#afkir_cincin').val(clik.data('afkir'));
			$('#masuk_stikerl').val(clik.data('masuk_luar'));
			$('#terpakai_stikerl').val(clik.data('pakai_luar'));
			$('#masuk_stikerd').val(clik.data('masuk_dalam'));
			$('#terpakai_stikerd').val(clik.data('pakai_dalam'));
			$('#masuk_cukai1').val(clik.data('masuk_cukai'));
			$('#terpakai_cukai1').val(clik.data('terpakai_cukailama'));
			$('#masuk_cukai2').val(clik.data('masuk_cukai2'));
			$('#terpakai_cukai2').val(clik.data('terpakai_cukaibaru'));
			$('#masuk_kemasan').val(clik.data('masuk_kemasan'));
			$('#terpakai_kemasan').val(clik.data('terpakai_kemasan'));
			$('#afkir_kemasan').val(clik.data('afkir_kemasan'));
			// data yang tetap
			$('#masuk_cincinhidden').val(clik.data('masuk'));
			$('#terpakai_cincinhidden').val(clik.data('terpakai'));
			$('#afkir_cincinhidden').val(clik.data('afkir'));
			$('#masuk_stikerlhidden').val(clik.data('masuk_luar'));
			$('#terpakai_stikerlhidden').val(clik.data('pakai_luar'));
			$('#masuk_stikerdhidden').val(clik.data('masuk_dalam'));
			$('#terpakai_stikerdhidden').val(clik.data('pakai_dalam'));
			$('#masuk_cukai1hidden').val(clik.data('masuk_cukai'));
			$('#terpakai_cukai1hidden').val(clik.data('terpakai_cukailama'));
			$('#masuk_cukai2hidden').val(clik.data('masuk_cukai2'));
			$('#terpakai_cukai2hidden').val(clik.data('terpakai_cukaibaru'));
			$('#masuk_kemasanhidden').val(clik.data('masuk_kemasan'));
			$('#terpakai_kemasanhidden').val(clik.data('terpakai_kemasan'));
			$('#afkir_kemasanhidden').val(clik.data('afkir_kemasan'));

			$('#keterangan').val(clik.data('ket_cin'));


			$.ajax({
				headers: {
					'X-CSRF-TOKEN': token
				},
				url: '/api/data_nontembakau/some',
				type: 'POST',
				dataType: 'json',
				data: {
					produk: clik.data('id_produk'),
					subproduk: clik.data('id_sub_produk'),
				},
			})
			.done(function(data) {
				console.log(data);
				$('#stocknowcincin').val(data.stock_qty_cincin);
				$('#stocknowstikerluar').val(data.stock_luar);
				$('#stocknowstikerdalam').val(data.stock_dalam);
				$('#stocknowcukai1').val(data.stock_qty_cukai);
				$('#stocknowcukai2').val(data.stock_qty_cukai2);
				$('#stocknowkemasan').val(data.stock_kemasan);
			});
			
			appendform('aksesorisform',[],0);

			$('#modal-nontembakau').modal('show');
		});

$(document).on('click', '#update-aks', function(event) {
	let id = $( this ).data('id');
	$('#form-aks').attr('action', $( this ).data('link'));
	$.ajax({
		headers: {
			'X-CSRF-TOKEN': token
		},
		url: '/api/data_nontembakau/getaks',
		type: 'GET',
		dataType: 'json',
		data: {
			history_cincin_id: id
		},
		success: function(data) {
			$tanggal = data.tanggal;

			$('.modal-header #title').text('Update Aksesoris : Tanggal: ' + $tanggal);		 
			// console.log($tanggal);
			appendform('aksesorisformupdate',data,1);

			$('#modal-aks').modal('show');
		}
	});
});

$(document).on('click', '#showhidd', function(event) {
	let kodeproduk = $('#kode-produk').val();
	let kodesub = $('#kode-sub').val();
	$.ajax({
		url: '/api/data_nontembakau/show',
		type: 'POST',
		dataType: 'json',
		data: {
			"_token": token,
			produk: kodeproduk,
			subproduk: kodesub,
		},
		beforeSend: function() {
			isProcessing = true;
			$('.spinner_cari').fadeIn();
		},
		success: function(data) {
			$('.spinner_cari').hide();
			isProcessing = false;

			$('#stockcincin').text(' Jumlah: ' + data.stock_qty_cincin);
			$('#stockstiker').text(' Jumlah: *Luar: ' + data.stock_luar + ': *Dalam: ' + data.stock_dalam);
			$('#stockcukai').text(' Jumlah: *Cukai1: ' + data.stock_qty_cukai + ': *Cukai2: ' + data.stock_qty_cukai2);
			$('#nama_kemasan').text(data.nama_kemasan)
			$('#stockkemasan').text(' Jumlah: ' + data.stock_kemasan);

			$('#id_kemasan').val(data.id_kemasan);

			$('#id_stock_cincin').val(data.id_stock_cincin);
			$('#id_stock_cukai').val(data.id_stock_cukai);
			$('#id_stock_stiker').val(data.id_stock_stiker);
			$('#id_stock_kemasan').val(data.id_stock_kemasan);

			$('#idhargacincin').val(data.idhargacincin);
			$('#idhargacukai').val(data.idhargacukai);
			$('#idhargastiker').val(data.idhargastiker);
			$('#idhargakemasan').val(data.idhargakemasan);

			$('#stockawalcincin').val(data.stock_qty_cincin);
			$('#stockawalstikerluar').val(data.stock_luar);
			$('#stockawalstikerdalam').val(data.stock_dalam);
			$('#stockawalcukai1').val(data.stock_qty_cukai);
			$('#stockawalcukai2').val(data.stock_qty_cukai2);
			$('#stockawalkemasan').val(data.stock_kemasan);

			$text = ' Harga Cincin: Rp. '+format_number(data.hargacincin,'')+'; Harga Cukai: Rp. '+format_number(data.hargacukai,'')+'; Harga Stiker: *luar-Rp. '+format_number(data.hargastiker_luar,'')+'  *dalam-Rp. '+format_number(data.hargastiker_dalam,'')+'; Harga Packaging: Rp. '+format_number(data.hargakemasan,'')+';';
			$('#hargaset_show').text($text);

			appendform('aksesorisform',data);
		},
		error: function(xhr, status, error) {
			var err = eval("(" + xhr.responseText + ")");
			alert(err.Message);
		}
	});
});

$(document).on('click', '#submit_nontembakau', function(event) {
	let link = $('#form-nontembakau').attr('action');
	var mydata = $("form#form-nontembakau").serialize();

	event.preventDefault();
	$.ajax({
		headers: {
			'X-CSRF-TOKEN': token
		},
		url: link,
		type: 'POST',
		dataType: 'json',
		data: mydata,
		beforeSend: function() {
			isProcessing = true;
			$('.spinner_nekr').fadeIn();
		},
		success: function(data) {
			$('.spinner_nekr').hide();
			isProcessing = false;
			alert(data);
			$('#table-nontembakau').DataTable().ajax.reload();
			$('#modal-nontembakau').modal('toggle');
		}
	});
});

$(document).on('click', '#submit_updateaks', function(event) {
	let link = $('#form-aks').attr('action');
	var mydata = $("form#form-aks").serialize();
	$.ajax({
		headers: {
			'X-CSRF-TOKEN': token
		},
		url: link,
		type: 'POST',
		dataType: 'json',
		data: mydata,
		beforeSend: function() {
			isProcessing = true;
			$('.spinner_ame').fadeIn();
		},
		success: function(data) {
			$('.spinner_ame').hide();
			isProcessing = false;
			console.log(data);
			if (data == true) {
				$msg = 'data berhasil diupdate';
			} else {
				$msg = 'data gagal diupdate';
			}
			alert($msg);
			$('#table-nontembakau').DataTable().ajax.reload();
			$('#modal-aks').modal('toggle');
		}
	});
});

$(document).on('click', '#delete-nontembakau', function(event) {
	$.ajax({
		headers: {
			'X-CSRF-TOKEN': token
		},
		url: $( this ).data('link'),
		type: 'DELETE',
		dataType: 'json',
		data: {
			produkhidden: $( this ).data('id_produk'),
			subprodukhidden: $( this ).data('id_sub_produk'),
			id_kemasan: $( this ).data('id_kemasan'),
		},
		beforeSend: function() {
			isProcessing = true;
			$('#loadingdelete').fadeIn();
		},
		success: function(data) {
			$('#loadingdelete').hide();
			isProcessing = false;
			alert(data);
			$('#table-nontembakau').DataTable().ajax.reload();
		}
	});
	
});

});

</script>