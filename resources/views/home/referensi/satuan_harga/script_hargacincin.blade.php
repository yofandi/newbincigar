<script>
	var input1 = document.getElementById('harga_satcincin');
	input1.addEventListener('keyup', function(e)
	{
		input1.value = format_number(this.value, '');
	}
	);

	function Refresh_cin() {
		$('#table-hargacin').DataTable().ajax.reload();
	}

	var tablecincin = $('#table-hargacin').DataTable({
		"lengthMenu": [
		[3,5,10, 25, 50, 100, 150, 200, -1],
		[3,5,10, 25, 50, 100, 150, 200, "All"]
		],
		"order": [
		[0, 'desc']
		],
		processing: true,
		serverSide: true,
		ajax: '/api/hargacincin/json',
		columns: [
		{ data: 'DT_RowIndex', name: 'DT_RowIndex' },
		{ data: 'tanggal_harga', name: 'tanggal_harga' },
		{ data: 'kode_hcincin', name: 'kode_hcincin' },
		{ data: 'produk', name: 'produk' },
		{ data: 'harga_for', name: 'harga_for' },
		{ data: 'keterangan_hargacin', name: 'keterangan_hargacin' },
		{	
			data: null,
			"bSortable": false,
			mRender: function(o) {
				$bind = '<button type="button" class="btn btn-xs btn-rounded btn-warning update-hargacin" data-title="Update Harga Cincin | ID '+ o.id +'" data-link="/api/hargacincin/update/'+ o.id +'" data-tanggal_cincin="'+ o.tanggal_harga +'" data-kode="'+ o.kode_hcincin +'" data-produk="'+ o.id_produk +'" data-harga="'+ format_number(o.harga, '') +'" data-ket="'+ o.keterangan_hargacin +'" data-count="'+ o.countdata +'"><i class="mdi mdi-dots-horizontal"></i></button>';
				$bind += '<button type="button" class="btn btn-danger btn-rounded btn-xs" id="delete-hargacin" data-id="'+ o.id +'" data-count="'+ o.countdata +'"><i class="fa fa-trash"></i></button>';
				return $bind; 
			}
		},
		],
		"displayLength": 10,
	});
	$('#table-hargacin tbody').on('click', 'tr.group', function() {
		var currentOrder = tablecincin.order()[0];
		if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
			tablecincin.order([2, 'desc']).draw();
		} else {
			table.order([2, 'asc']).draw();
		}
	});
	$('#table-hargacin tbody').on('click', 'tr.group', function() {
		var currentOrder = tablecincin.order()[0];
		if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
			tablecincin.order([2, 'desc']).draw();
		} else {
			tablecincin.order([2, 'asc']).draw();
		}
	});

	function save_methodcincin() {
		let link = $('#form-hargacincin').attr('action');
		let kode_hcincin = $('#kode_hcincin').val();
		let tanggal_cincin = $('#tanggal_cincin').val();
		let produk = $('#produk-cincin').val();
		let harga_satuan = $('#harga_satcincin').val();
		let keterangan = $('#keterangan-cincin').val();
		$.ajax({
			url: link,
			type: 'POST',
			dataType: 'json',
			data: {
				"_token": token,
				tanggal_cincin: tanggal_cincin,
				kode_hcincin: kode_hcincin,
				produk: produk,
				harga_satuan: harga_satuan,
				keterangan: keterangan,
			},
			beforeSend: function() {
				isProcessing = true;
				$('.spinner_nekrcin').fadeIn();
			},
			success: function(data) {
				$('.spinner_nekrcin').hide();
				isProcessing = false;
				alert(data);
				Refresh_cin();
				$('#modal-hargacincin').modal('toggle');
			}
		});
	}

	$(document).on('click', '#tambah-hargacin', function(event) {
		let click = $(this);
		$('.modal-header #title-hargacincin').text('Tambah Harga Cincin');
		$('#form-hargacincin').find('input').val('');
		$('#form-hargacincin').find('select').val('');
		$('#form-hargacincin').find('textarea').val('');
		$('#form-hargacincin').attr('action', click.data('link'));
		lproduk('produk-cincin');
		$("#tanggal_cincin").datetimepicker({
			format: 'YYYY-MM-DD HH:mm:ss',
		}).data("DateTimePicker").date(new Date());
		$('#modal-hargacincin').modal('show');
	});

	$(document).on('click', '.update-hargacin', function(event) {
		let click = $(this);
		$('.modal-header #title-hargacincin').text(click.data('title'));
		$('#form-hargacincin').find('input').val('');
		$('#form-hargacincin').find('select').val('');
		$('#form-hargacincin').find('textarea').val('');
		$('#form-hargacincin').attr('action', click.data('link'));
		lproduk('produk-cincin',click.data('produk'));

		$('#harga_satcincin').val(click.data('harga'));
		$('#keterangan-cincin').val(click.data('ket'));

		$("#tanggal_cincin").datetimepicker({
			format: 'YYYY-MM-DD HH:mm:ss',
		}).data("DateTimePicker").date(click.data('tanggal_cincin'));

		$('#kode_hcincin').val(click.data('kode'));
		$('#modal-hargacincin').modal('show');
	});

	$(document).on('click', '#delete-hargacin', function(event) {
		let dje = confirm('Apakah anda yakin ingin menghapus data ini ?');
		if (dje) {
			if ($(this).data("count") > 0) {
				alert('Maaf.. Anda tidak bisa menghapus data ini, dikarenakan akan menimbulkan error pada aplikasi ini!!');
			} else {

				let id = $(this).data("id");
				$.ajax(
				{
					url: '/api/hargacincin/delete/' + id,
					type: 'DELETE',
					data: {
						"_token": token,
					},
					beforeSend: function() {
						isProcessing = true;
						$('#loadingdeletecin').fadeIn();
					},
					success: function (data){
						$('#loadingdeletecin').hide();
						alert(data);
						Refresh_cin();
						isProcessing = false;
					}
				});
			}
		}
	});
</script>