<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use DataTables;

class Dashboard extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getlimitbahan()
    {
    	$query = DB::table('stock_probaku')
        ->leftJoin('jenis','stock_probaku.jenis_id_jenis','=','jenis.id_jenis')
        ->leftJoin('kategori','stock_probaku.kategori_id_kategori','=','kategori.id_kategori')
        ->get();
    	// return json_encode($query);

        return Datatables::of($query)
        ->addIndexColumn()
        ->addColumn('varientasi', function ($query) {
            $awl = $query->jenis.' - '.$query->kategori;
            return $awl;
        })
        ->make(true);
    }

    public function puteditlimit(Request $request)
    {
        $agh = str_replace(",", "", $request->jml);
    	$data = [
    		$request->column_name => $agh
    	];
    	$put = DB::table('stock_probaku')->where('id',$request->rowId)->update($data);
    	$eht = ( $put ? 'Success' : 'Sorry, Try again..');
    	return json_encode($eht);
    }

    public function dashboard()
    {
        return view('home.dashboard.dashboard');
    }
}
