<div class="row">
	<div class="col-md-6">
		<legend>Data Frezzer</legend>
	</div>
	<div class="col-md-2 offset-md-4">
		<button type="button" data-link="{{ route('freezer.post') }}" class="btn btn-success btn-rounded btn-icons " id="tambah-frezzer">Tambah</button>
		<button type="button" onclick="Refresh_frezzer()" class="btn btn-rounded btn-icons btn-secondary">Refresh</button>
	</div>
	<div class="col-md-12" align="center" id="loadingdeletefrez">
		<h5>Data tengah diproses. Mohon tunggu sejenak!</h5>
	</div>
	<div class="table-responsive">
		<table id="table-frezzer" width="100%" class="display nowrap table table-hover table-striped table-bordered">
			<thead>
				<tr>
					<th>No.</th>
					<th>Tanggal</th>
					<th>Produk</th>
					<th>Jenis</th>
					<th>Durasi</th>
					<th>Batang Drying1</th>
					<th>Tambah Batang</th>
					<th>JML</th>
					<th>Ket</th>
					<th>Aksi</th>
				</tr>
			</thead>
		</table>
	</div>
</div>