<div class="scroll-sidebar">
  <nav class="sidebar-nav">
    <ul id="sidebarnav">
      <li class="nav-small-cap">PERSONAL</li>
      <li class="active"> 
        <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="icon-Car-Wheel"></i><span class="hide-menu">Dashboard</a>
        <ul aria-expanded="true" class="collapse in">
          <li><a href="{{ route('home') }}">BEGIN</a></li>
        </ul>
      </li>
      @if(in_array(Auth::user()->level_user_id_level, $bahan))
      <li class="nav-small-cap">MANAGEMEN</li>
      <li> 
        <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-archive"></i><span class="hide-menu">Bahan Baku</span></a>
        <ul aria-expanded="false" class="collapse">
          <li><a href="{{ route('data.tembakau') }}">Tembakau</a></li>
          <li><a href="{{ route('data.reproduksi') }}">Reproduksi</a></li>
          {{-- <li><a href="{{ route('data.cincin') }}">Data Cincin</a></li>
          <li><a href="{{ route('data.stiker') }}">Data Stiker</a></li>
          <li><a href="{{ route('data.cukai') }}">Data Cukai</a></li>
          <li><a href="{{ route('data.kemasan') }}">Data Kemasan</a></li> --}}
          <li><a href="{{ route('data.non_tembakau') }}">Non-Tembakau</a></li>
        </ul>
      </li>
      @endif
      @if(in_array(Auth::user()->level_user_id_level, $produksi))
      <li> 
        <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-archive"></i><span class="hide-menu">Produksi</span></a>
        <ul aria-expanded="false" class="collapse">
          <li><a href="{{ route('produksi') }}">Proses Cerutu</a></li>
          {{-- <li><a href="{{ route('filling') }}">Filling</a></li>
          <li><a href="{{ route('binding') }}">Binding</a></li>
          <li><a href="{{ route('pressing') }}">Pressing</a></li>
          <li><a href="{{ route('wrapping') }}">Wrapping</a></li>
          <li><a href="{{ route('drying1') }}">Drying 1</a></li>
          <li><a href="{{ route('freezer') }}">Frezzer</a></li>
          <li><a href="{{ route('drying2') }}">Drying 2</a></li>
          <li><a href="{{ route('fumigasi') }}">Fumigasi</a></li> --}}
          <li><a href="{{ route('cool') }}">Cool</a></li>
          <li><a href="{{ route('drying3') }}">Humidor</a></li>
          <li><a href="{{ route('qc') }}">Quality Control</a></li>
          <li><a href="{{ route('packing') }}">Packing</a></li>
        </ul>
      </li>
      @endif
      @if(in_array(Auth::user()->level_user_id_level, $rfs))
      <li> 
        <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-archive"></i><span class="hide-menu">Pengiriman</span></a>
        <ul aria-expanded="false" class="collapse">
          <li><a href="{{ route('rfs') }}">Data Pengiriman</a></li>
        </ul>
      </li>
      @endif
      <li> 
        <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-archive"></i><span class="hide-menu">Laporan</span></a>
        <ul aria-expanded="false" class="collapse">
          <li><a href="{{ route('laporam.bahan') }}">Laporan Bahan</a></li>
          <li><a href="{{ route('laporam.produksi') }}">Laporan Produksi</a></li>
        </ul>
      </li>

      @if(Auth::user()->level_user_id_level == 1)
      <li class="nav-small-cap"> REFERENSI</li>
      <li> 
        <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="icon-Car-Wheel"></i><span class="hide-menu">User</span></a>
        <ul aria-expanded="false" class="collapse">
          <li><a href="{{ route('user') }}">User Management</a></li>
        </ul>
      </li>
      <li> 
        <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="sl-icon-settings"></i><span class="hide-menu">Referensi</span></a>
        <ul aria-expanded="false" class="collapse">
          <li><a href="{{ route('aksesoris') }}">Aksesoris</a></li>
          <li><a href="{{ route('merekcukai') }}">Merek Cukai</a></li>
          <li><a href="{{ route('jenis') }}">Jenis Tembakau</a></li>
          <li><a href="{{ route('jenis.kemasan') }}">Jenis Packaging</a></li>
          <li><a href="{{ route('produk') }}">Produk</a></li>
          <li><a href="{{ route('kategori') }}">Kategori</a></li>
          {{-- <li><a href="{{ route('cincin') }}">Cincin</a></li>
          <li><a href="{{ route('cukai') }}">Cukai</a></li>
          <li><a href="{{ route('stiker') }}">Stiker</a></li>
          <li><a href="{{ route('kemasan') }}">Kemasan</a></li> --}}
          <li><a href="{{ route('hargatembakau') }}">Harga</a></li>
          <li><a href="{{ route('setting') }}">Setting</a></li>
        </ul>
      </li>
      @endif
    </ul>
  </nav>
</div>