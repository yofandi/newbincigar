<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class History_bahanmasuk extends Model
{
	protected $table = 'history_bahanmasuk';

	protected $fillable = [
		'tanggal_in',
		'asal',
		'stock_masuk',
		'diterima',
		'diproduksi',
		'hari_ini',
		'ket_his',
		'jenis_id_jenis',
		'kategori_id_kategori',
		'stock_probaku_id',
		'satuan_harga_id',
		'author_session',
		'created_at',
		'log'
	];

	protected $with = ['baseprice'];

	public function baseprice()
	{
		return $this->belongsTo('App\Satuan_harga', 'satua_harga_id');
	}
}
