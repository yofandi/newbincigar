<div class="row">
	<div class="col-md-4">
		<h4 class="card-title">Harga Stiker</h4>
	</div>
	<div class="col-md-2 offset-md-6">
		<button type="button" class="btn btn-xs btn-success" id="tambah-hargastiker" data-link="{{ route('api.posthargastiker') }}">Tambah</button>
		<button type="button" onclick="Refresh_stiker()" class="btn btn-xs btn-icons btn-secondary">Refresh</button>
	</div>
	<div class="col-md-12" align="center" id="loadingdeletestiker">
		<h5>Data tengah diproses. Mohon tunggu sejenak!</h5>
	</div>
	<div class="table-responsive">
		<table id="table-hargastiker" class="display nowrap table table-hover table-striped table-bordered" width="100%">
			<thead>
				<th>No.</th>
				<th>Tanggal</th>
				<th>Kode Harga Stiker</th>
				<th>Produk</th>
				<th>Sub Produk</th>
				<th>Harga Luar</th>
				<th>Harga Dalam</th>
				<th>Keterangan</th>
				<th>Aksi</th>
			</thead>
		</table>
	</div>
</div>