<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Quality_control;
use App\Stock_drying3;
use App\Stock_qc;
use App\Stock_binding;
use App\Stock_wrapping;
use App\Stock_probaku;

class Back_quality extends Controller
{
	public function sproqc($request)
	{
		$qusrt = DB::table('stock_drying3')
		->select('stock_drying3.id','stock_drying3.jml_dry3')
		->join('jenis_has_produk','stock_drying3.jenis_has_produk_id_jnpro','=','jenis_has_produk.id_jnpro')
		->where([['jenis_has_produk.jenis_id_jenis','=',$request->jenis],['jenis_has_produk.produk_id_produk','=',$request->produk]]);

		$qusrt1 = DB::table('stock_qc')
		->select(DB::raw('
			stock_qc.id,
			stock_qc.jml_qc,
			stock_qc.jenis_has_produk_id_produk,
			(SELECT 
			berat_jadi 
			FROM sub_produk 
			WHERE produk_id_produk = stock_qc.jenis_has_produk_id_produk 
			ORDER BY id_sub_produk ASC 
			LIMIT 1) AS berat_jadi
			'))
		->join('jenis_has_produk','stock_qc.jenis_has_produk_id_jnpro','=','jenis_has_produk.id_jnpro')
		->Leftjoin('sub_produk','stock_qc.jenis_has_produk_id_produk','=','sub_produk.produk_id_produk')
		->where([['jenis_has_produk.jenis_id_jenis','=',$request->jenis],['jenis_has_produk.produk_id_produk','=',$request->produk]]);

		if ($qusrt->count() > 0) {
			$aem = $qusrt->first();
			$qer = $aem->jml_dry3;
			$qwc = $aem->id;
		} else { $qer = 0; $qwc = '';}

		if ($qusrt1->count() > 0) {
			$aem1 = $qusrt1->first();
			$ber = $aem1->jml_qc;
			$jkd = $aem1->id;
			$kan = $aem1->berat_jadi;
		} else { $ber = 0; $jkd = ''; $kan = 0;}

		$array = ['jml_dry3' => $qer,'jml_qc' => $ber,'iddrying3' => $qwc,'idqc' => $jkd,'berat_jadi' => $kan];
		return $array;
	}

	public function stockqc($request)
	{
		$bang = $request->stockbatang - ( $request->batangjml + $request->mutasijml );
		$cekwrt = Stock_drying3::find($request->iddrying3);
		$cekwrt->jml_dry3 = $bang;
		$cekwrt->save();
		$resultidwrt = $cekwrt->id;

		$rfill = Stock_probaku::where([['jenis_id_jenis','=',$request->jenis],['kategori_id_kategori','=',1]])->first();
		$uprfill = Stock_probaku::find($rfill->id);
		$uprfill->jml_bahanbaku = $rfill->jml_bahanbaku + $request->rejfill;
		$uprfill->save();

		$rlaz = Stock_probaku::where([['jenis_id_jenis','=',$request->jenis],['kategori_id_kategori','=',2]])->first();
		$uprlaz = Stock_probaku::find($rlaz->id);
		$uprlaz->jml_bahanbaku = $rlaz->jml_bahanbaku + $request->rejlaz;
		$uprlaz->save();

		$rbind = Stock_probaku::where([['jenis_id_jenis','=',$request->jenis],['kategori_id_kategori','=',3]])->first();
		$uprbind = Stock_probaku::find($rbind->id);
		$uprbind->jml_bahanbaku = $rbind->jml_bahanbaku + $request->rejbind;
		$uprbind->save();

		$rwrap = Stock_probaku::where([['jenis_id_jenis','=',$request->jenis],['kategori_id_kategori','=',4]])->first();
		$uprwrap = Stock_probaku::find($rwrap->id);
		$uprwrap->jml_bahanbaku = $rwrap->jml_bahanbaku + $request->rejwrap;
		$uprwrap->save();

		if ($request->idqc != '') {
			$id = $request->idqc; 
			$show = Stock_qc::find($id)->first();
			$jml = $show->jml_qc + $request->batangjml;

			$cek = Stock_qc::find($id);
			$cek->jml_qc = $jml;
			$cek->save();

			$resultid = $cek->id;
		} else {
			$jml = $request->batangjml;
			$save = new Stock_qc;

			$save->jml_qc = $jml;
			$save->jenis_has_produk_id_jnpro = $request->idjnpro;
			$save->jenis_has_produk_id_jenis = $request->jenis;
			$save->jenis_has_produk_id_produk = $request->produk;
			$save->save();
			$resultid = $save->id;
		}
		return ['iddrying3' => $resultidwrt,'idqc' => $resultid,'jml_dry3' => $bang,'jml_qc' => $jml];
	}

	public function ifuppstockqc($request,$batang,$mutasi,$back_fill,$back_bind,$back_wrap,$back_lazio)
	{
		$bawl = $batang + $mutasi;
		$rreq = $request->batangjml + $request->mutasijml;
		if ($batang != $request->batangjml || $mutasi != $request->mutasijml || $back_fill !=  $request->rejfill || $back_bind != $request->rejbind || $back_wrap != $request->rejwrap || $back_lazio != $request->rejlaz) {

			$shdry3 = Stock_drying3::where('id',$request->iddrying3)->first();
			$shqc = Stock_qc::where('id',$request->idqc)->first();
			$rfill = Stock_probaku::where([['jenis_id_jenis','=',$request->jenis],['kategori_id_kategori','=',1]])->first();
			$rlaz = Stock_probaku::where([['jenis_id_jenis','=',$request->jenis],['kategori_id_kategori','=',2]])->first();
			$rbind = Stock_probaku::where([['jenis_id_jenis','=',$request->jenis],['kategori_id_kategori','=',3]])->first();
			$rwrap = Stock_probaku::where([['jenis_id_jenis','=',$request->jenis],['kategori_id_kategori','=',4]])->first();

			if ($bawl > $rreq) {
				$qwty = $bawl - $rreq;
				$aqr = $shdry3->jml_dry3 + $qwty;
			} elseif ($bawl < $rreq) {
				$qwty = $rreq - $bawl;
				$aqr = $shdry3->jml_dry3 - $qwty;
			} else {
				$aqr = $shdry3->jml_dry3;
			}

			if ($back_fill > $request->rejfill) {
				$lembr = $back_fill - $request->rejfill;
				$ammw = $rfill->jml_bahanbaku - $lembr;
			} elseif ($back_fill < $request->rejfill) {
				$lembr = $request->rejfill - $back_fill;
				$ammw = $rfill->jml_bahanbaku + $lembr;
			} else {
				$ammw = $rfill->jml_bahanbaku;
			}			

			if ($back_lazio > $request->rejlaz) {
				$kempq = $back_lazio - $request->rejlaz;
				$mnw = $rlaz->jml_bahanbaku - $kempq;
			} elseif ($back_lazio < $request->rejlaz) {
				$kempq = $request->rejlaz - $back_lazio;
				$mnw = $rlaz->jml_bahanbaku + $kempq;
			} else {
				$mnw = $rlaz->jml_bahanbaku;
			}

			if ($back_bind > $request->rejbind) {
				$qxc = $back_bind - $request->rejbind;
				$qhk = $rbind->jml_bahanbaku - $qxc;
			} elseif ($back_bind < $request->rejbind) {
				$qxc = $request->rejbind - $back_bind;
				$qhk = $rbind->jml_bahanbaku + $qxc;
			} else {
				$qhk = $rbind->jml_bahanbaku;
			}

			if ($back_wrap > $request->rejwrap) {
				$lip = $back_wrap - $request->rejwrap;
				$rtyi = $rwrap->jml_bahanbaku - $lip;
			} elseif ($back_wrap < $request->rejwrap) {
				$lip = $request->rejwrap - $back_wrap;
				$rtyi = $rwrap->jml_bahanbaku + $lip;
			} else {
				$rtyi = $rwrap->jml_bahanbaku;
			}

			if ($batang > $request->batangjml) {
				$nah = $batang - $request->batangjml;
				$hasilw = $shqc->jml_qc - $nah;
			} elseif ($batang < $request->batangjml) {
				$nah = $request->batangjml - $batang;
				$hasilw = $shqc->jml_qc + $nah;
			} else {
				$hasilw = $shqc->jml_qc;
			}

			$uprbind = Stock_probaku::find($rfill->id);
			$uprbind->jml_bahanbaku = $ammw;
			$uprbind->save();

			$uprwrap = Stock_probaku::find($rlaz->id);
			$uprwrap->jml_bahanbaku = $mnw;
			$uprwrap->save();

			$uprbind = Stock_probaku::find($rbind->id);
			$uprbind->jml_bahanbaku = $qhk;
			$uprbind->save();

			$uprwrap = Stock_probaku::find($rwrap->id);
			$uprwrap->jml_bahanbaku = $rtyi;
			$uprwrap->save();

			$query1 = Stock_drying3::find($request->iddrying3);
			$query1->jml_dry3 = $aqr;
			$query1->save();

			$query2 = Stock_qc::find($request->idqc);
			$query2->jml_qc = $hasilw;
			$query2->save();
		}
	}

	public function loop_upqc($request,$id,$batang = '')
	{
		$ceh = Quality_control::where([['stock_qc_id','=',$request->idqc],['stock_drying3_id','=',$request->iddrying3],['id','>',$id]]);
		if ($ceh->count() > 0) {
			$reg = $ceh->get();
			for ($i=0; $i < $ceh->count(); $i++) {
				$show = Quality_control::where([['stock_qc_id','=',$request->idqc],['stock_drying3_id','=',$request->iddrying3],['id','<',$reg[$i]->id]])->orderBy('id','desc')->take('1');
				if ($show->count() > 0) {
					$first = $show->first();
					$hsie = $first->hasil_qc - ( $reg[$i]->accept + $reg[$i]->mutasi_batang );
				} else {
					if ($batang == '') {
						$awl1 = $reg[$i]->hasil_qc + $reg[$i]->accept + $reg[$i]->mutasi_batang;
						$hsie = $awl1 - ( $reg[$i]->accept + $reg[$i]->mutasi_batang );
					} else {
						$awl1 = $batang;
						$hsie = $awl1 - ( $reg[$i]->accept + $reg[$i]->mutasi_batang );
					}
				}
				$update = Quality_control::where('id',$reg[$i]->id)->update(['hasil_qc' => $hsie]);
			}
		}
	}

}
