<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;
use App\History_aksesoris;
use App\History_cincin;
use App\History_stiker;
use App\History_cukai;
use App\History_kemasan;

class Back_bahannontembakau extends Controller
{

	public function hasilforaddnontembakau($request)
	{
		$hasilcin = $request->stockawalcincin + $request->masuk_cincin - ($request->terpakai_cincin + $request->afkir_cincin);

		$hasilstil = $request->stockawalstikerluar + $request->masuk_stikerl - $request->terpakai_stikerl;
		$hasilstid = $request->stockawalstikerdalam + $request->masuk_stikerd - $request->terpakai_stikerd;

		$hasilcukai1 = $request->stockawalcukai1 + $request->masuk_cukai1 - $request->terpakai_cukai1;
		$hasilcukai2 = $request->stockawalcukai2 + $request->masuk_cukai2 - $request->terpakai_cukai2;

		$hasilkemasan = $request->stockawalkemasan + $request->masuk_kemasan - ($request->terpakai_kemasan + $request->afkir_kemasan);
		
		$skor = ['hasilcin' => $hasilcin,
		'hasilstil' => $hasilstil,
		'hasilstid' => $hasilstid,
		'hasilcukai1' => $hasilcukai1,
		'hasilcukai2' => $hasilcukai2,
		'hasilkemasan' => $hasilkemasan];

		$tey1 = DB::table('stock_cincin');
		if ($request->id_stock_cincin != "") {
			$tey1->where('id_stock_cincin',$request->id_stock_cincin);
			$tey1->update(['stock_qty_cincin' => $hasilcin]);
			$skor += ['id_stock_cincin' => $request->id_stock_cincin];
		} else {
			$qght = $tey1->insertGetId([
				'stock_qty_cincin' => $hasilcin,
				'produk_id_produk' => $request->produkhidden
			]);
			$skor += ['id_stock_cincin' => $qght];
		}

		$tey2 = DB::table('stock_stiker');
		if ($request->id_stock_stiker != "") {
			$tey2->where('id_stock_stiker',$request->id_stock_stiker);
			$tey2->update([
				'stock_luar' => $hasilstil,
				'stock_dalam' => $hasilstid
			]);
			$skor += ['id_stock_stiker' => $request->id_stock_stiker];
		} else {
			$abne = $tey2->insertGetId([
				'stock_luar' => $hasilstil,
				'stock_dalam' => $hasilstid,
				'sub_produk_id' => $request->subprodukhidden,
				'produk_id_produk' => $request->produkhidden
			]);
			$skor += ['id_stock_stiker' => $abne];
		}

		$tey3 = DB::table('stock_kemasan');
		if ($request->id_stock_kemasan != "") {
			$tey3->where('id_stock_kemasan',$request->id_stock_kemasan);
			$tey3->update(['stock_kemasan' => $hasilkemasan]);
			$skor += ['id_stock_kemasan' => $request->id_stock_kemasan];
		} else {
			$pemr = $tey3->insertGetId([
				'stock_kemasan' => $hasilkemasan,
				'kemasan_id_kemasan' => $request->id_kemasan,
				'sub_produk_id' => $request->subprodukhidden,
				'produk_id_produk' => $request->produkhidden,
			]);
			$skor += ['id_stock_kemasan' => $pemr];
		}

		$tey4 = DB::table('stock_cukai');
		if ($request->id_stock_cukai != "") {
			$tey4->where('id_stock_cukai',$request->id_stock_cukai);
			$tey4->update([
				'stock_qty_cukai' => $hasilcukai1,
				'stock_qty_cukai2' => $hasilcukai2
			]);
			$skor += ['id_stock_cukai' => $request->id_stock_cukai];
		} else {
			$hagv = $tey4->insertGetId([
				'stock_qty_cukai' => $hasilcukai1,
				'stock_qty_cukai2' => $hasilcukai2,
				'sub_produk_id_sub_produk' => $request->subprodukhidden,
				'sub_produk_produk_id_produk' => $request->produkhidden,
			]);
			$skor += ['id_stock_cukai' => $hagv];
		}

		return $skor;
	}

	public function hasilforaddaksesoris($request,$idhis)
	{
		$query = DB::table('sub_produk')
		->select(
			'sub_produk.id_sub_produk',
			'sub_produk_has_aksesoris.id AS idaksesoris',
			'sub_produk_has_aksesoris.kode_spesial',
			'aksesoris.aksesoris',
			'sub_produk_has_aksesoris.aksesoris_id')
		->join('produk','sub_produk.produk_id_produk','produk.id_produk')
		->join('sub_produk_has_aksesoris','sub_produk.id_sub_produk','sub_produk_has_aksesoris.sub_produk_id')
		->join('aksesoris','sub_produk_has_aksesoris.aksesoris_id','=','aksesoris.id')
		->where(
			'sub_produk_has_aksesoris.sub_produk_id',$request->subprodukhidden
		)
		->orderBy('sub_produk.id_sub_produk','ASC')
		->get();

		foreach ($query as $key) {
			$nameaksst = 'stock_'.$key->aksesoris;
			$nameaksin = 'masuk_'.$key->aksesoris;
			$nameakster = 'terpakai_'.$key->aksesoris;

			$hasilaks = $request->input($nameaksst) + $request->input($nameaksin) - ( $request->input($nameakster) );
			$data = [
				'stock_aksesoris' => $hasilaks
			];

			$cek = DB::table('stock_aksesoris')
			->where('sub_has_aks_id',$key->idaksesoris);
			if ($cek->count() > 0) {
				$gh = $cek->first();
				DB::table('stock_aksesoris')->where('id',$gh->id)->update($data);
				$pp = $gh->id;
			} else {
				$data += [
					'sub_has_aks_id' => $key->idaksesoris,
					'created_at' => date('Y-m-d H:i:s')
				];
				$pp = DB::table('stock_aksesoris')->insertGetId($data);
			}

			$cart = new History_aksesoris;
			$cart->stock_aksesoris_id = $pp;
			$cart->stocksub_has_aks_id = $key->idaksesoris;
			$cart->history_cincin_id = $idhis;
			$cart->tanggal_hisaks = $request->tanggal;
			$cart->masuk_aksesoris = $request->input($nameaksin);
			$cart->terpakai_aksesoris = $request->input($nameakster);
			$cart->hasil_aksesoris = $hasilaks;
			$cart->author_session = Auth::user()->id;
			$cart->created_at = date('Y-m-d H:i:s');
			$cart->log = 'INSERT';
			$cart->save();
		}
	}

	public function datainputnontembakau($request,$result)
	{
		$data['data1'] = [
			'masuk' => $request->masuk_cincin,
			'terpakai' => $request->terpakai_cincin,
			'afkir' => $request->afkir_cincin,
			'sisa' => $result['hasilcin'],
			'tanggal_cincin' => $request->tanggal,
			'ket_cin' => $request->keterangan,
			'stock_cincin_id_stock_cincin' => $result['id_stock_cincin'],
			'stock_cincin_produk_id_produk' => $request->produkhidden,
			'harga_cincin_id' => $request->idhargacincin,
			'author_session' => Auth::user()->id,
			'created_at' => date('Y-m-d H:i:s'),
			'log' => "INSERT"
		];
		$data['data2'] = [
			'stock_stiker_id_stock_stiker' => $result['id_stock_stiker'],
			'stock_stiker_sub_produk_id' => $request->subprodukhidden,
			'stock_stiker_produk_id_produk' => $request->produkhidden,
			'harga_stiker_id' => $request->idhargastiker,
			'masuk_luar' => $request->masuk_stikerl,
			'pakai_luar' => $request->terpakai_stikerl,
			'hasil_luar' => $result['hasilstil'],
			'masuk_dalam' => $request->masuk_stikerd,
			'pakai_dalam' => $request->terpakai_stikerd,
			'hasil_dalam' => $result['hasilstid'],
			'tanggal_hisstik' => $request->tanggal,
			'ket_sti' => $request->keterangan,
			'author_session' => Auth::user()->id,
			'created_at' => date('Y-m-d H:i:s'),
			'log' => "INSERT"
		];
		$data['data3'] = [
			'masuk_cukai' => $request->masuk_cukai1,
			'masuk_cukai2' => $request->masuk_cukai2,
			'terpakai_cukailama' => $request->terpakai_cukai1,
			'terpakai_cukaibaru' => $request->terpakai_cukai2,
			'sisa_cukai' => $result['hasilcukai1'],
			'sisa_cukai2' => $result['hasilcukai2'],
			'tanggal_cukai' => $request->tanggal,
			'ket_hiscukai' => $request->keterangan,
			'stock_cukai_id_stock_cukai' => $result['id_stock_cukai'],
			'stock_cukai_id_sub_produk' => $request->subprodukhidden,
			'stock_cukai_produk_id_produk' => $request->produkhidden,
			'harga_cukai_id' => $request->idhargacukai,
			'author_session' => Auth::user()->id,
			'created_at' => date('Y-m-d H:i:s'),
			'log' => 'INSERT'
		];
		$data['data4'] = [
			'tanggal_hiskem' => $request->tanggal,
			'masuk_kemasan' => $request->masuk_kemasan,
			'terpakai_kemasan' => $request->terpakai_kemasan,
			'afkir_kemasan' => $request->afkir_kemasan,
			'stok_now' => $result['hasilkemasan'],
			'stock_kemasan_id_stock_kemasan' => $result['id_stock_kemasan'],
			'stock_kemasan_kemasan_id_kemasan' => $request->id_kemasan,
			'stock_kemasan_sub_produk_id' => $request->subprodukhidden,
			'stock_kemasan_produk_id_produk' => $request->produkhidden,
			'harga_kemasan_id' => $request->idhargakemasan,
			'ket_kem' => $request->keterangan,
			'author_session' => Auth::user()->id,
			'created_at' => date('Y-m-d H:i:s'),
			'log' => "INSERT"
		];
		return $data;
	}

	public function ifuppcincin($request,$masuk,$terpakai,$afkir)
	{
		if ($masuk != $request->masuk_cincin || $terpakai != $request->terpakai_cincin || $afkir != $request->afkir_cincin) {
			$min = $terpakai + $afkir;
			$man = $request->terpakai_cincin + $request->afkir_cincin;

			if ($masuk > $request->masuk_cincin) {
				$masuksam = $masuk - $request->masuk_cincin;
				$har = $request->stocknowcincin - $masuksam;
			} elseif ($masuk < $request->masuk_cincin) {
				$masuksam = $request->masuk_cincin - $masuk;
				$har = $request->stocknowcincin + $masuksam;
			} else { 
				$har = $request->stocknowcincin;
			}
			$car = $har;
			if ($min > $man) {
				$das = $min - $man;
				$gh = $car + $das;
			} elseif ($min < $man) {
				$das = $man - $min;
				$gh = $car - $das;
			} else {
				$gh = $car - 0;
			}

			$retro = $gh;

			DB::table('stock_cincin')->where(['id_stock_cincin' => $request->id_stock_cincin])->update(['stock_qty_cincin' => $retro]);
		}
	}

	public function ifuppstiker($request,$masukl,$masukd,$pakail,$pakaid)
	{
		if ($masukl != $request->masuk_stikerl || $masukd != $request->masuk_stikerd || $pakail != $request->terpakai_stikerl || $pakaid != $request->terpakai_stikerd) {
			if ($masukl > $request->masuk_stikerl) {
				$ml = $masukl - $request->masuk_stikerl;
				$stl = $request->stocknowstikerluar - $ml;
			} elseif ($masukl < $request->masuk_stikerl) {
				$ml = $request->masuk_stikerl - $masukl;
				$stl = $request->stocknowstikerluar + $ml;
			} else {
				$stl = $request->stocknowstikerluar;
			}
			$rt = $stl;

			if ($pakail > $request->terpakai_stikerl) {
				$pl = $pakail - $request->terpakai_stikerl;
				$hasl = $rt + $pl;                
			} elseif ($pakail < $request->terpakai_stikerl) {
				$pl = $request->terpakai_stikerl - $pakail;
				$hasl = $rt - $pl;                
			} else {
				$hasl = $rt;                
			}

			if ($masukd > $request->masuk_stikerd) {
				$md = $masukd - $request->masuk_stikerd;
				$std = $request->stocknowstikerdalam - $md;
			} elseif ($masukd < $request->masuk_stikerd) {
				$md = $request->masuk_stikerd - $masukd;
				$std = $request->stocknowstikerdalam + $md;
			} else {
				$std = $request->stocknowstikerdalam;
			}
			$tr = $std;

			if ($pakaid > $request->terpakai_stikerd) {
				$pd = $pakaid - $request->terpakai_stikerd;
				$hasd = $tr + $pd;
			} elseif ($pakaid < $request->terpakai_stikerd) {
				$pd = $request->terpakai_stikerd - $pakaid;
				$hasd = $tr - $pd;
			} else {
				$hasd = $tr;
			}

			DB::table('stock_stiker')->where('id_stock_stiker',$request->id_stock_stiker)->update(['stock_luar' => $hasl,'stock_dalam' => $hasd]);
		}
	}

	public function ifuppkemasan($request,$masuk,$terpakai,$afkir)
	{
		if ($masuk != $request->masuk_kemasan || $terpakai != $request->terpakai_kemasan || $afkir != $request->afkir_kemasan) {
			if ($masuk > $request->masuk_kemasan) {
				$ret = $masuk - $request->masuk_kemasan;
				$ter = $request->stocknowkemasan - $ret;
			} elseif ($masuk < $request->masuk) {
				$ret = $request->masuk_kemasan - $masuk;
				$ter = $request->stocknowkemasan + $ret;
			} else {
				$ter = $request->stocknowkemasan;
			}

			$por = $ter;

			$wq = $terpakai + $afkir;
			$qr = $request->terpakai_kemasan + $request->afkir_kemasan;

			if ($wq > $qr) {
				$slk = $wq - $qr;
				$hasil = $por + $slk;
			} elseif ($wq < $qr) {
				$slk = $qr - $wq;
				$hasil = $por - $slk;
			} else {
				$hasil = $por;
			}

			DB::table('stock_kemasan')->where('id_stock_kemasan',$request->id_stock_kemasan)->update(['stock_kemasan' => $hasil]);
		}
	}

	public function ifuppcukai($request,$masuk1,$lama,$masuk2,$baru)
	{
		if ($masuk1 != $request->masuk_cukai1 || $lama != $request->terpakai_cukai1 || $masuk2 != $request->masuk_cukai2 || $baru != $request->terpakai_cukai2) {

			if ($masuk1 > $request->masuk_cukai1) {
				$aher = $masuk1 - $request->masuk_cukai1;
				$blade1 = $request->stocknowcukai1 - $aher;
			} elseif ($masuk1 < $request->masuk_cukai1) {
				$aher = $request->masuk_cukai1 - $masuk1;
				$blade1 = $request->stocknowcukai1 + $aher;
			} else {
				$blade1 = $request->stocknowcukai1;
			}

			if ($lama > $request->terpakai_cukai1) {
				$pl = $lama - $request->terpakai_cukai1;
				$hasil1 = $blade1 + $pl;                
			} elseif ($lama < $request->terpakai_cukai1) {
				$pl = $request->terpakai_cukai1 - $lama;
				$hasil1 = $blade1 - $pl;                
			} else {
				$hasil1 = $blade1;                
			}

			if ($masuk2 > $request->masuk_cukai2) {
				$md = $masuk2 - $request->masuk_cukai2;
				$std = $request->stocknowcukai2 - $md;
			} elseif ($masuk2 < $request->masuk_cukai2) {
				$md = $request->masuk_cukai2 - $masuk2;
				$std = $request->stocknowcukai2 + $md;
			} else {
				$std = $request->stocknowcukai2;
			}
			$tr = $std;

			if ($baru > $request->terpakai_cukai2) {
				$pd = $baru - $request->terpakai_cukai2;
				$hasil2 = $tr + $pd;
			} elseif ($baru < $request->terpakai_cukai2) {
				$pd = $request->terpakai_cukai2 - $baru;
				$hasil2 = $tr - $pd;
			} else {
				$hasil2 = $tr;
			}

			DB::table('stock_cukai')->where('id_stock_cukai',$request->id_stock_cukai)->update(['stock_qty_cukai' => $hasil1,'stock_qty_cukai2' => $hasil2]);
		}
	}

	public function datauppjoinnontembakau($request,$result)
	{
		$data = [
			'history_cincin.masuk' => $request->masuk_cincin,
			'history_cincin.terpakai' => $request->terpakai_cincin,
			'history_cincin.afkir' => $request->afkir_cincin,
			'history_cincin.sisa' => $result['hasilcin'],
			'history_cincin.tanggal_cincin' => $request->tanggal,
			'history_cincin.ket_cin' => $request->keterangan,
			'history_cincin.author_session' => Auth::user()->id,
			'history_cincin.log' => "UPDATE",

			'history_stiker.masuk_luar' => $request->masuk_stikerl,
			'history_stiker.pakai_luar' => $request->terpakai_stikerl,
			'history_stiker.hasil_luar' => $result['hasilstil'],
			'history_stiker.masuk_dalam' => $request->masuk_stikerd,
			'history_stiker.pakai_dalam' => $request->terpakai_stikerd,
			'history_stiker.hasil_dalam' => $result['hasilstid'],
			'history_stiker.tanggal_hisstik' => $request->tanggal,
			'history_stiker.ket_sti' => $request->keterangan,
			'history_stiker.author_session' => Auth::user()->id,
			'history_stiker.log' => "UPDATE",

			'history_cukai.masuk_cukai' => $request->masuk_cukai1,
			'history_cukai.masuk_cukai2' => $request->masuk_cukai2,
			'history_cukai.terpakai_cukailama' => $request->terpakai_cukai1,
			'history_cukai.terpakai_cukaibaru' => $request->terpakai_cukai2,
			'history_cukai.sisa_cukai' => $result['hasilcukai1'],
			'history_cukai.sisa_cukai2' => $result['hasilcukai2'],
			'history_cukai.tanggal_cukai' => $request->tanggal,
			'history_cukai.ket_hiscukai' => $request->keterangan,
			'history_cukai.author_session' => Auth::user()->id,
			'history_cukai.log' => 'UPDATE',

			'history_kemasan.tanggal_hiskem' => $request->tanggal,
			'history_kemasan.masuk_kemasan' => $request->masuk_kemasan,
			'history_kemasan.terpakai_kemasan' => $request->terpakai_kemasan,
			'history_kemasan.afkir_kemasan' => $request->afkir_kemasan,
			'history_kemasan.stok_now' =>$result['hasilkemasan'],
			'history_kemasan.ket_kem' => $request->keterangan,
			'history_kemasan.author_session' => Auth::user()->id,
			'history_kemasan.log' => "INSERT"
		];

		return $data;
	}

	public function loop_upcincin($request,$id,$stockawal = '')
	{
		$his  = History_cincin::where([['stock_cincin_produk_id_produk','=',$request->produkhidden],['id','>',$id]]);
		if ($his->count() > 0) {
			$reg = $his->get();
			for ($i=0; $i < $his->count(); $i++) { 
				$shoe  = History_cincin::where([['stock_cincin_produk_id_produk','=',$request->produkhidden],['id','<',$reg[$i]->id]])->orderBy('id','desc')->take('1');
				if ($shoe->count() > 0) {
					$jkp = $shoe->first();
					$today = $jkp->sisa + $reg[$i]->masuk - ($reg[$i]->terpakai + $reg[$i]->afkir);
				} else {
					if ($stockawal == '') {
						$awalcdd = $reg[$i]->sisa - $reg[$i]->masuk + ($reg[$i]->terpakai + $reg[$i]->afkir);
						$today = $awalcdd + $reg[$i]->masuk - ($reg[$i]->terpakai + $reg[$i]->afkir);
					} else {
						$today = $stockawal + $reg[$i]->masuk - ($reg[$i]->terpakai + $reg[$i]->afkir);
					}
				}

				$update = History_cincin::find($reg[$i]->id);
				$update->sisa = $today;
				$update->save();
			}
		}
	}

	public function loop_upstiker($request,$id,$stockluar = '',$stockdalam = '')
	{
		$cek = DB::table('history_stiker')->where([['stock_stiker_sub_produk_id','=',$request->subprodukhidden],['stock_stiker_produk_id_produk','=',$request->produkhidden],['id','>',$id]]);
		if ($cek->count() > 0) {
			$get = $cek->get();
			for ($i=0; $i < $cek->count(); $i++) {
				$ter = DB::table('history_stiker')->where([['stock_stiker_sub_produk_id','=',$request->subprodukhidden],['stock_stiker_produk_id_produk','=',$request->produkhidden],['id','<',$get[$i]->id]])->orderBy('id','desc')->take('1');
				if ($ter->count() > 0) {
					$erd = $ter->first();
					$hasil_luar = $erd->hasil_luar + $get[$i]->masuk_luar - $get[$i]->pakai_luar;
					$hasil_dalam = $erd->hasil_dalam + $get[$i]->masuk_dalam - $get[$i]->pakai_dalam;
				} else {
					if ($stockluar == '') {
						$pe = 1;
						$ajeb = $get[$i]->hasil_luar - $get[$i]->masuk_luar + $get[$i]->pakai_luar;
						$hasil_luar = $ajeb + $get[$i]->masuk_luar - $get[$i]->pakai_luar;
					} else {
						$pe = 0;
						$hasil_luar = $stockluar + $get[$i]->masuk_luar - $get[$i]->pakai_luar;
					}
					if ($stockdalam == '') {
						$ke = 1;
						$gahbe = $get[$i]->hasil_dalam + $get[$i]->masuk_dalam - $get[$i]->pakai_dalam;
						$hasil_dalam = $gahbe + $get[$i]->masuk_dalam - $get[$i]->pakai_dalam;
					} else {
						$ke = 0;
						$hasil_dalam = $stockdalam + $get[$i]->masuk_dalam - $get[$i]->pakai_dalam;
					}
				}

				$query = History_stiker::find($get[$i]->id);
				$query->hasil_luar = $hasil_luar;
				$query->hasil_dalam = $hasil_dalam;

				$query->save();   
				return $cek->count();             
			}
		}
	}

	public function loop_upkemasan($request,$id,$stockawal = '')
	{
		$cek = History_kemasan::where([['stock_kemasan_kemasan_id_kemasan','=',$request->id_kemasan],['stock_kemasan_sub_produk_id','=',$request->subprodukhidden],['stock_kemasan_produk_id_produk','=',$request->produkhidden],['id','>',$id]]);
		if ($cek->count() > 0) {
			$get = $cek->get();
			for ($i=0; $i < $cek->count(); $i++) {
				$zsf = History_kemasan::where([['stock_kemasan_kemasan_id_kemasan','=',$request->id_kemasan],['stock_kemasan_sub_produk_id','=',$request->subprodukhidden],['stock_kemasan_produk_id_produk','=',$request->produkhidden],['id','<',$get[$i]->id]])->orderBy('id','desc')->take('1');
				if ($zsf->count() > 0) {
					$his = $zsf->first();
					$hasil = $his->stok_now + $get[$i]->masuk_kemasan - ($get[$i]->terpakai_kemasan  +  $get[$i]->afkir_kemasan);
				} else {
					if ($stockawal == '') {
						$awalkem = $get[$i]->stok_now - $get[$i]->masuk_kemasan + ($get[$i]->terpakai_kemasan  +  $get[$i]->afkir_kemasan);
						$hasil = $awalkem + $get[$i]->masuk_kemasan - ($get[$i]->terpakai_kemasan  +  $get[$i]->afkir_kemasan);
					} else {
						$hasil = $stockawal + $get[$i]->masuk_kemasan - ($get[$i]->terpakai_kemasan  +  $get[$i]->afkir_kemasan);
					}
				}
				History_kemasan::where('id',$get[$i]->id)->update(['stok_now' => $hasil]);
			}
		}
	}

	public function loop_upcukai($request,$id,$stokawalen = '',$stokawalkn = '')
	{
		$kam = History_cukai::where([['stock_cukai_id_sub_produk','=',$request->subprodukhidden],['stock_cukai_produk_id_produk','=',$request->produkhidden],['id','>',$id]]);
		if ($kam->count() > 0) {
			$get = $kam->get();
			for ($i=0; $i < $kam->count(); $i++) {
				$wet = History_cukai::where([['stock_cukai_id_sub_produk','=',$request->subprodukhidden],['stock_cukai_produk_id_produk','=',$request->produkhidden],['id','<',$get[$i]->id]])->orderBy('id','desc')->take(1);
				if ($wet->count() > 0) {
					$qqa = $wet->first();
					$jqwt = $qqa->sisa_cukai + $get[$i]->masuk_cukai - ($get[$i]->terpakai_cukailama );
					$akern = $qqa->sisa_cukai2 + $get[$i]->masuk_cukai2 - ($get[$i]->terpakai_cukaibaru );
				} else {
					if ($stokawalen == '') {
						$abej = $get[$i]->sisa_cukai - $get[$i]->masuk_cukai + ( $get[$i]->terpakai_cukailama );
						$hane = $get[$i]->sisa_cukai2 - $get[$i]->masuk_cukai2 + ( $get[$i]->terpakai_cukaibaru );


						$jqwt = $abej + $get[$i]->masuk_cukai - ( $get[$i]->terpakai_cukailama );
						$akern = $hane + $get[$i]->masuk_cukai2 - ( $get[$i]->terpakai_cukaibaru );

					} else {
						$jqwt = $stokawalen + $get[$i]->masuk_cukai - ( $get[$i]->terpakai_cukailama );
						$akern = $stokawalkn + $get[$i]->masuk_cukai2 - ( $get[$i]->terpakai_cukaibaru );
					}

				}
				$asjl = History_cukai::where('id',$get[$i]->id)->update(['sisa_cukai' => $jqwt,'sisa_cukai2' => $akern]);
			}
		}
	}

	public function looping_aksesoris($request,$id,$hss,$awir = 0)
	{
		$cek = History_aksesoris::where([['id','>',$id],['stock_aksesoris_id','=',$hss]]);
		if ($cek->count() > 0) {
			$getse = $cek->get();
			for ($i=0; $i < $cek->count(); $i++) { 
				$hae = History_aksesoris::where([['id','<',$getse[$i]->id],['stock_aksesoris_id','=',$getse[$i]->stock_aksesoris_id]])->orderBy('id','DESC')->take(1);

				if ($hae->count() > 0) {
					$naj = $hae->first();
					$anemr = $naj->hasil_aksesoris;
				} else {
					if ($awir == '') {
						$anemr = $getse[$i]->hasil_aksesoris - $getse[$i]->masuk_aksesoris + $getse[$i]->terpakai_aksesoris;
					} else {
						$anemr = $awir;
					}
				}

				$sdrd = $anemr + $getse[$i]->masuk_aksesoris - $getse[$i]->terpakai_aksesoris;

				$ajenb = History_aksesoris::find($getse[$i]->id);
				$ajenb->hasil_aksesoris = $sdrd;
				$ajenb->save();
			}
		}
		
	}
}
