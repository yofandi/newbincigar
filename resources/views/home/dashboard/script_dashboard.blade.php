<script>
	$(function() {
		let token = $("meta[name='csrf-token']").attr("content");
		function formatNumber(s) {
			var parts = s.split(/,/)
			, spaced = parts[0]
			.split('').reverse().join('')
			.match(/\d{1,3}/g).join(',') 
			.split('').reverse().join(''); 
			return spaced + (parts[1] ? '.'+parts[1] : ''); 
		}
		function formatNumber1(numberString) {
			return numberString.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
		}

		var table = $('#table_limitbahan').DataTable({
			"lengthMenu": [
			[3,5,10, 25, 50, 100, 150, 200, -1],
			[3,5,10, 25, 50, 100, 150, 200, "All"]
			],
			"order": [
			[0, 'desc']
			],
			processing: true,
			serverSide: true,
			ajax: '/api/dashboard',
			createdRow: function( row, data, dataIndex ) {
				$( row ).find('td:eq(2)')
				.attr({
					'contenteditable': "true",
					'data-row_id': data.id,
					'data-column_name': 'minlimit_stock',
					'align': 'right'
				})
				.addClass('table_datalimit');

				$( row ).find('td:eq(3)')
				.attr({
					'contenteditable': "true",
					'data-row_id': data.id,
					'data-column_name': 'limit_stock',
					'align': 'right'
				})
				.addClass('table_datalimit');
			},
			columns: [
			{ 
				data: null,
				"bSortable": false,
				mRender: function (o) { 
					return '<span class="round round-success">TMB</span>'; 
				}
			},
			{ data: 'varientasi', name: 'varientasi' },
			{ 
				data: null, "bSortable": false, name: 'minlimit_stock', mRender: function(o) {
					return formatNumber1(o.minlimit_stock);
				} 
			},
			{ 
				data: null, "bSortable": false, name: 'limit_stock', mRender: function(o) {
					return formatNumber1(o.limit_stock);
				} 
			},
			],
			"displayLength": 10,
		});
		$('#table_limitbahan tbody').on('click', 'tr.group', function() {
			var currentOrder = table.order()[0];
			if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
				table.order([2, 'desc']).draw();
			} else {
				table.order([2, 'asc']).draw();
			}
		});
		$('#table_limitbahan tbody').on('click', 'tr.group', function() {
			var currentOrder = table.order()[0];
			if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
				table.order([2, 'desc']).draw();
			} else {
				table.order([2, 'asc']).draw();
			}
		});

		$(document).on('blur', '.table_datalimit', function(){
			var id = $(this).data('row_id');
			var table_column = $(this).data('column_name');
			var value = $(this).text();
			$.ajax({
				headers: {
					'X-CSRF-TOKEN': token
				},
				url: '/api/dashboard/limit/put',
				method:"PUT",
				data:{
					"_token": token,
					rowId:id,
					column_name:table_column,
					jml:value
				},
				success: function (data){
					$('#table_limitbahan').DataTable().ajax.reload();
					// alert(data);
				}
			})
		});

		$(document).on('keyup', '.table_datalimit', function(event) {
			var $this = $( this );
			$this.text(formatNumber1($this.text()));
		});

	});
</script>