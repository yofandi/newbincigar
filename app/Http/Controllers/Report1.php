<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Builder;

class Report1 extends Controller
{
	public function __construct()
	{
		$this->middleware('auth');
	}

	public function report_produksi()
	{
		$data['produksi'] = [0 => 'Pilih Laporan',1 => 'Packing',2 => 'Wrapping',3 => 'Quality Control'];
		$hje = DB::table('produk')->select('id_produk','produk')->get();
		$data['produk'] = $hje;
		$data['kemasan'] = DB::table('kemasan')->select('id_kemasan','nama_kemasan')->get();
		$data['sub_produk'] = DB::table('sub_produk')->select('id_sub_produk','sub_kode','sub_produk')->orderBy('produk_id_produk')->get();
		return view('home.laporan.produksi.laporan_produksi',$data);
	}

	public function getlaporanpacking(Request $request)
	{
		$query = DB::table('packing')
		->select('packing.*','sub_produk.sub_kode','sub_produk.sub_produk','kemasan.nama_kemasan')
		->join('stock_packing','packing.stock_packing_id','=','stock_packing.id')
		->join('sub_produk','stock_packing.sub_produk_id_sub_produk','=','sub_produk.id_sub_produk')
		->join('kemasan','sub_produk.kemasan_id_kemasan','=','kemasan.id_kemasan')
		->join('produk','sub_produk.produk_id_produk','=','produk.id_produk')
		->where([
			['produk.id_produk','like',$request->produk.'%'],
			['sub_produk.id_sub_produk','like',$request->sub_produk.'%'],
			['kemasan.id_kemasan','like',$request->kemasan.'%']
		])
		->whereBetween(DB::raw("(DATE_FORMAT(packing.tanggal_packing,'%Y-%m-%d'))"),[$request->tgl_awl,$request->tgl_akhr])
		->get();

		return $query->toJson();
	}

	public function getlaporanwrappping(Request $request)
	{
		if ($request->jnslap == 1) {
			$select = DB::raw('
			produk.produk,
			(SELECT ring FROM sub_produk WHERE produk_id_produk = produk.id_produk LIMIT 1) AS ring,
			(wrapping.sisaprow + wrapping.terpakaiprow) AS stockawal,
			wrapping.tanggal_wrap,
			wrapping.sisaprow,
			wrapping.tambah_wrap,
			wrapping.batangreject,
			wrapping.hasil_akhirw,
			wrapping.keterangan_wrap
			');
		} else {
			$select = DB::raw('
			produk.produk,
			(SELECT ring FROM sub_produk WHERE produk_id_produk = produk.id_produk LIMIT 1) AS ring,
			(wrapping.sisaprow + wrapping.terpakaiprow) AS stockawal,
			wrapping.tanggal_wrap,
			IFNULL(SUM(wrapping.sisaprow),0) AS sisaprow,
			IFNULL(SUM(wrapping.tambah_wrap),0) AS tambah_wrap,
			IFNULL(SUM(wrapping.batangreject),0) AS batangreject,
			IFNULL(SUM(wrapping.hasil_akhirw),0) AS hasil_akhirw,
			IFNULL(SUM(wrapping.keterangan_wrap),0) AS keterangan_wrap
			');
		}
		$query = DB::table('wrapping')
		->select($select)
		->join('stock_wrapping','wrapping.stock_wrapping_id','=','stock_wrapping.id')
		->join('produk','stock_wrapping.jenis_has_produk_id_produk','=','produk.id_produk')
		->whereIn('produk.id_produk',$request->produk_erd)
		->whereBetween(DB::raw("(DATE_FORMAT(wrapping.tanggal_wrap,'%Y-%m-%d'))"), [$request->tgl_awl,$request->tgl_akhr])
		->orderBy('wrapping.tanggal_wrap','ASC')
		->orderBy('produk.id_produk','ASC');
		if ($request->jnslap == 2) {
			$query->groupBy('produk.id_produk');
		}
		$rte = $query->get();

		return json_encode($rte);
	}

	public function getlaporanquality(Request $request)
	{
		if ($request->jnslap == 1) {
			$select = DB::raw('
			produk.produk,
			quality_control.tanggal_qc,
			(quality_control.hasil_qc + quality_control.accept + quality_control.mutasi_batang) AS stock_awl,
			quality_control.hasil_qc,
			quality_control.accept,
			quality_control.mutasi_batang,
			quality_control.keterangan_qc
			');
		} else {
			$select = DB::raw('
			produk.produk,
			quality_control.tanggal_qc,
			(quality_control.hasil_qc + quality_control.accept + quality_control.mutasi_batang) AS stock_awl,
			IFNULL(SUM(quality_control.hasil_qc),0) AS hasil_qc,
			IFNULL(SUM(quality_control.accept),0) AS accept,
			IFNULL(SUM(quality_control.mutasi_batang),0) AS mutasi_batang,
			quality_control.keterangan_qc
			');
		}
		$query = DB::table('quality_control')
		->select($select)
		->join('stock_qc','quality_control.stock_qc_id','=','stock_qc.id')
		->join('produk','stock_qc.jenis_has_produk_id_produk','=','produk.id_produk')
		->whereIn('produk.id_produk',$request->produk_erd)
		->whereBetween(DB::raw("(DATE_FORMAT(quality_control.tanggal_qc,'%Y-%m-%d'))"), [$request->tgl_awl,$request->tgl_akhr])
		->groupBy('produk.id_produk')
		->get();

		return $query->toJson();
	}
}
