@extends('../../layout')

@section('title')
<title>BIN - ERP : Profile</title>
@endsection

@section('css')
<link rel="stylesheet" href="{{ asset('node_modules/dropify/dist/css/dropify.min.css') }}">
@endsection


@section('content')
<div class="row page-titles">
	<div class="col-md-5 align-self-center">
		<h3 class="text-themecolor">Profile</h3>
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
			<li class="breadcrumb-item active">Profile</li>
		</ol>
	</div>
	{{-- <div class="col-md-7 align-self-center text-right d-none d-md-block">
		<button type="button" class="btn btn-info"><i class="fa fa-plus-circle"></i> Create New</button>
	</div> --}}
	<div class="">
		<button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
	</div>
</div>

<div class="row">
	<div class="col-lg-4 col-xlg-3 col-md-5">
		<div class="card">
			<div class="card-body">
				<center class="m-t-30"> <img src="{{ asset('images/app_user/'.Auth::user()->upload_foto) }}" class="img-circle" width="150" />
					<h4 class="card-title m-t-10">{{ Auth::user()->name }}</h4>
					<h6 class="card-subtitle">{{ $level->level }}</h6>
				</center>
			</div>
			<div><hr></div>
			<div class="card-body"> 
				<small class="text-muted">Email address </small>
				<h6>{{ Auth::user()->email }}</h6> <small class="text-muted p-t-30 db">Phone</small>
				<h6>{{ Auth::user()->no_hp }}</h6> <small class="text-muted p-t-30 db">Address</small>
				<h6>{{ Auth::user()->alamat_user }}</h6>
			</div>
		</div>
	</div>
	<div class="col-lg-8 col-xlg-9 col-md-7">
		<div class="card">
			<div class="flash-message">
				@foreach (['danger', 'warning', 'success', 'info'] as $msg)
				@if(Session::has('alert-' . $msg))

				<p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
				@endif
				@endforeach
			</div>
			<!-- Nav tabs -->
			<ul class="nav nav-tabs profile-tab" role="tablist">
				<li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#home" role="tab">Change Photo</a> </li>
				<li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#settings" role="tab">Profile</a> </li>
				<li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#pass" role="tab">Password</a> </li>
			</ul>
			<!-- Tab panes -->
			<div class="tab-content">
				<div class="tab-pane active" id="home" role="tabpanel">
					<div class="card-body">
						<div class="row">
							<div class="col-lg-12 col-md-12">
								<form class="" method="post" action="{{ route('profile.upload', Auth::user()->id) }}" enctype="multipart/form-data">
									@csrf
									<h4 class="card-title">File Upload - Photo</h4>
									<div class="form-group">
										<label for="input-file-now">Your so fresh input file — Default version</label>
										<input type="file" name="image" id="input-file-now" class="dropify" />
									</div>
									<div class="form-group">
										<button class="btn btn-primary btn-md btn-block">Save</button>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
				<div class="tab-pane" id="settings" role="tabpanel">
					<div class="card-body"> 
						<form class="form-horizontal form-material" method="POST" action="{{ route('profile.update',Auth::user()->id) }}">
							@csrf
							<div class="form-group">
								<label class="col-md-12">Full Name</label>
								<div class="col-md-12">
									<input type="text" name="name" placeholder="Full Your Name" class="form-control form-control-line" value="{{ Auth::user()->name }}">
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-12">Username</label>
								<div class="col-md-12">
									<input type="text" name="username" placeholder="username" class="form-control form-control-line" value="{{ Auth::user()->username }}">
								</div>
							</div>
							<div class="form-group">
								<label for="example-email" class="col-md-12">Email</label>
								<div class="col-md-12">
									<input type="email" name="email" placeholder="___@__.__" class="form-control form-control-line" id="example-email" value="{{ Auth::user()->email }}">
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-12">Phone</label>
								<div class="col-md-12">
									<input type="text" name="no_hp" placeholder="Your Number" class="form-control form-control-line" value="{{ Auth::user()->no_hp }}">
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-12">Alamat</label>
								<div class="col-md-12">
									<textarea rows="5" name="alamat_user" class="form-control form-control-line">{{ Auth::user()->alamat_user }}</textarea>
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-12">
									<button type="submit" class="btn btn-success">Update Profile</button>
								</div>
							</div>
						</form>
					</div>
				</div>
				<div class="tab-pane" id="pass" role="tabpanel">
					<div class="card-body">
						<div class="row">
							<div class="col-lg-12 col-md-12">
								<form class="m-t-40" method="post" action="{{ route('profile.changepass',Auth::user()->id) }}" novalidate>
									@csrf
									<div class="form-group">
										<label>Old Password</label>
										<input type="password" class="form-control" name="old_pass">
									</div>
									<div class="form-group">
										<h5>New Password<span class="text-danger">*</span></h5>
										<div class="controls">
											<input type="password" name="password" class="form-control" required data-validation-required-message="This field is required"> 
										</div>
									</div>
									<div class="form-group">
										<h5>Repeat New Password<span class="text-danger">*</span></h5>
										<div class="controls">
											<input type="password" name="password2" data-validation-match-match="password" class="form-control" required> 
										</div>
									</div>
									<div class="form-group">
										<button type="submit" class="btn btn-info btn-md btn-block">Change</button>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection


@section('javascript')
<script src="{{ asset('node_modules/dropify/dist/js/dropify.min.js') }}"></script>
<script>
	$(function() {
		$('.dropify').dropify();

		$('.dropify-fr').dropify({
			messages: {
				default: 'Glissez-déposez un fichier ici ou cliquez',
				replace: 'Glissez-déposez un fichier ou cliquez pour remplacer',
				remove: 'Supprimer',
				error: 'Désolé, le fichier trop volumineux'
			}
		});

		var drEvent = $('#input-file-events').dropify();

		drEvent.on('dropify.beforeClear', function(event, element) {
			return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
		});

		drEvent.on('dropify.afterClear', function(event, element) {
			alert('File deleted');
		});

		drEvent.on('dropify.errors', function(event, element) {
			console.log('Has Errors');
		});

		var drDestroy = $('#input-file-to-destroy').dropify();
		drDestroy = drDestroy.data('dropify')
		$('#toggleDropify').on('click', function(e) {
			e.preventDefault();
			if (drDestroy.isDropified()) {
				drDestroy.destroy();
			} else {
				drDestroy.init();
			}
		})
	});
</script>
<script src="{{ asset('js/validation.js') }}"></script>
<script>
	! function(window, document, $) {
		"use strict";
		$("input,select,textarea").not("[type=submit]").jqBootstrapValidation();
	}(window, document, jQuery);
</script>
@endsection