<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Fumigasi;
use App\Stock_drying2;
use App\Stock_fumigasi;

class Back_fumigasi extends Controller
{
	public function sprofumi($request)
	{
		$qusrt =DB::table('stock_drying2')
		->select('stock_drying2.id','stock_drying2.jml_dry2')
		->join('jenis_has_produk','stock_drying2.jenis_has_produk_id_jnpro','=','jenis_has_produk.id_jnpro')
		->where([['jenis_has_produk.jenis_id_jenis','=',$request->jenis],['jenis_has_produk.produk_id_produk','=',$request->produk]]);

		$qusrt1 = DB::table('stock_fumigasi')
		->select('stock_fumigasi.id','stock_fumigasi.jml_fum')
		->join('jenis_has_produk','stock_fumigasi.jenis_has_produk_id_jnpro','=','jenis_has_produk.id_jnpro')
		->where([['jenis_has_produk.jenis_id_jenis','=',$request->jenis],['jenis_has_produk.produk_id_produk','=',$request->produk]]);

		if ($qusrt->count() > 0) {
			$aem = $qusrt->first();
			$qer = $aem->jml_dry2;
			$qwc = $aem->id;
		} else { $qer = 0; $qwc = '';}

		if ($qusrt1->count() > 0) {
			$aem1 = $qusrt1->first();
			$ber = $aem1->jml_fum;
			$jkd = $aem1->id;
		} else { $ber = 0; $jkd = ''; }
		$array = ['jml_dry2' => $qer,'jml_fum' => $ber,'iddrying2' => $qwc,'idfumigasi' => $jkd];
		return $array;
	}

	public function stockfumi($request)
	{
		$bang = $request->stockbatang - $request->batangjml;
		$cekwrt = Stock_drying2::find($request->iddrying2);
		$cekwrt->jml_dry2 = $bang;
		$cekwrt->save();
		$resultidwrt = $cekwrt->id;

		if ($request->idfumigasi != '') {
			$id = $request->idfumigasi; 
			$show = Stock_fumigasi::where('id',$id)->first();
			$jml = $show->jml_fum + $request->batangjml;

			$cek = Stock_fumigasi::find($id);
			$cek->jml_fum = $jml;
			$cek->save();

			$resultid = $cek->id;
		} else {
			$jml = $request->batangjml;
			$save = new Stock_fumigasi;

			$save->jml_fum = $jml;
			$save->jenis_has_produk_id_jnpro = $request->idjnpro;
			$save->jenis_has_produk_id_jenis = $request->jenis;
			$save->jenis_has_produk_id_produk = $request->produk;
			$save->save();
			$resultid = $save->id;
		}
		return ['iddrying2' => $resultidwrt,'idfumigasi' => $resultid,'jml_dry2' => $bang,'jml_fum' => $jml];
	}

	public function ifuppstockfumi($request,$batang)
	{
		if ($batang != $request->batangjml) {
			$shfumi = Stock_fumigasi::where('id',$request->idfumigasi)->first();
			$shdry2 = Stock_drying2::where('id',$request->iddrying2)->first();

			if ($batang > $request->batangjml) {
				$nah = $batang - $request->batangjml;
				$hasilw = $shfumi->jml_fum - $nah;
				$slash = Stock_drying2::find($request->iddrying2);
				$slash->jml_dry2 = $shdry2->jml_dry2 + $nah;
				$slash->save();
			} elseif ($batang < $request->batangjml) {
				$nah = $request->batangjml - $batang;
				$hasilw = $shfumi->jml_fum + $nah;
				$slash = Stock_drying2::find($request->iddrying2);
				$slash->jml_dry2 = $shdry2->jml_dry2 - $nah;
				$slash->save();
			} else {
				$hasilw = $shfumi->jml_fum;
			}

			$query2 = Stock_fumigasi::find($request->idfumigasi);
			$query2->jml_fum = $hasilw;
			$query2->save();
		}
	}

	public function loop_upfumi($request,$id,$batang = '')
	{
		$ceh = Fumigasi::where([['stock_drying2_id','=',$request->iddrying2],['stock_fumigasi_id','=',$request->idfumigasi],['id','>',$id]]);
		if ($ceh->count() > 0) {
			$reg = $ceh->get();
			for ($i=0; $i < $ceh->count(); $i++) {
				$show = Fumigasi::where([['stock_drying2_id','=',$request->iddrying2],['stock_fumigasi_id','=',$request->idfumigasi],['id','<',$reg[$i]->id]])->orderBy('id','desc')->take('1');
				if ($show->count() > 0) {
					$first = $show->first();
					$hsie = $first->hasil_akhirfum - $reg[$i]->tambah_fum;
				} else {
					if ($batang == '') {
						$awl1 = $reg[$i]->hasil_akhirfum + $reg[$i]->tambah_fum;
						$hsie = $awl1 - $reg[$i]->tambah_fum;
					} else {
						$awl1 = $reg[$i]->hasil_akhirfum + $reg[$i]->tambah_fum + $batang;
						$hsie = $awl1 - $reg[$i]->tambah_fum;
					}
				}
				$update = Fumigasi::where('id',$reg[$i]->id)->update(['hasil_akhirfum' => $hsie]);
			}
		}
	}

}
