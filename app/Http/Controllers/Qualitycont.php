<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use DataTables;
use Auth;
use App\Quality_control;
use App\Stock_drying3;
use App\Stock_qc;
use App\Stock_binding;
use App\Stock_wrapping;
use App\Stock_probaku;

use App\Http\Controllers\Back_quality;

class Qualitycont extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->produksi = $res = new Back_quality;
    }
    
    public function searchweiproqc(Request $request)
    {
        $jekr = $this->produksi->sproqc($request);
        echo json_encode($jekr);
    }

    public function getdataqc()
    {
        $query = DB::table('quality_control')
        ->select(DB::raw('
            quality_control.*,
            jenis.id_jenis,
            produk.id_produk,
            jenis.jenis,produk.produk,
            (SELECT 
            berat_jadi 
            FROM sub_produk 
            WHERE produk_id_produk = produk.id_produk 
            ORDER BY id_sub_produk ASC 
            LIMIT 1) AS berat_jadi
            '))
        ->join('stock_qc','quality_control.stock_qc_id','=','stock_qc.id')
        ->join('jenis','stock_qc.jenis_has_produk_id_jenis','=','jenis.id_jenis')
        ->join('produk','stock_qc.jenis_has_produk_id_produk','=','produk.id_produk')
        ->get();
        return Datatables::of($query)
        ->addIndexColumn()
        ->addColumn('awlbatang', function ($query) {
            $awl = $query->hasil_qc + $query->accept + ( $query->mutasi_batang );
            return $awl;
        })
        ->make(true);
    }

    public function dataqc()
    {
        $data['jenis'] = DB::table('jenis')->select('id_jenis','jenis')->get();
        return view('home.produksi.qc.dataqc',$data);
    }

    public function addqc(Request $request)
    {
        DB::beginTransaction();

        $qq = $this->produksi->stockqc($request);

        $data = [
            'accept' => $request->batangjml,
            'mutasi_batang' => $request->mutasijml,
            'back_fill' => $request->rejfill,
            'back_bind' => $request->rejbind,
            'back_wrap' => $request->rejwrap,
            'back_lazio' => $request->rejlaz,
            'hasil_qc' => $qq['jml_dry3'],
            'lama_qc' => $request->durasi,
            'keterangan_qc' => $request->ket,
            'tanggal_qc' => $request->tanggal_qc,
            'stock_drying3_id' => $qq['iddrying3'],
            'stock_qc_id' => $qq['idqc'],
            'author_session' => Auth::user()->id,
            'created_at' => date('Y-m-d H:i:s'),
            'log' => "INSERT"
        ];
        $query1 = Quality_control::insert($data);
        if ($query1) {
            $alert = 'Quality Control berhasil ditambahkan!';
        } else {
            $alert = 'Quality Control gagal ditambahkan!';
        }
        echo json_encode($alert);
        DB::commit();
    }

    public function uppqc(Request $request,$id,$batang,$mutasi,$back_fill,$back_bind,$back_wrap,$back_lazio)
    {
        DB::beginTransaction();
        $jwty = $request->stockbatang - ( $request->batangjml + $request->mutasijml );
        $this->produksi->ifuppstockqc($request,$batang,$mutasi,$back_fill,$back_bind,$back_wrap,$back_lazio);

        $data = [
            'accept' => $request->batangjml,
            'mutasi_batang' => $request->mutasijml,
            'back_fill' => $request->rejfill,
            'back_bind' => $request->rejbind,
            'back_wrap' => $request->rejwrap,
            'back_lazio' => $request->rejlaz,
            'hasil_qc' => $jwty,
            'lama_qc' => $request->durasi,
            'keterangan_qc' => $request->ket,
            'tanggal_qc' => $request->tanggal_qc,
            'author_session' => Auth::user()->id,
            'log' => "UPDATE"
        ];
        $query1 = Quality_control::where('id',$id)->update($data);
        if ($query1) {
            $this->produksi->loop_upqc($request,$id);
            $alert = 'Quality Control ID: '.$id.' berhasil diupdate!';
        } else {
            $alert = 'Quality Control ID: '.$id.' gagal diupdate!';
        }
        echo json_encode($alert);
        DB::commit();
    }

    public function delqc(Request $request,$id,$batang,$mutasi,$back_fill,$back_bind,$back_wrap,$back_lazio,$awlbatang)
    {
        DB::beginTransaction();
        $rfill = Stock_probaku::where([['jenis_id_jenis','=',$request->jenis],['kategori_id_kategori','=',1]])->first();
        $rlaz = Stock_probaku::where([['jenis_id_jenis','=',$request->jenis],['kategori_id_kategori','=',2]])->first();
        $rbind = Stock_probaku::where([['jenis_id_jenis','=',$request->jenis],['kategori_id_kategori','=',3]])->first();
        $rwrap = Stock_probaku::where([['jenis_id_jenis','=',$request->jenis],['kategori_id_kategori','=',4]])->first();
        $werty = Stock_drying3::where('id',$request->iddrying3)->first();
        $qusrt = Stock_qc::where('id',$request->idqc)->first();

        $batangfill = $rfill->jml_bahanbaku - $back_fill;
        $batanglaz = $rlaz->jml_bahanbaku - $back_lazio;
        $batangbind = $rbind->jml_bahanbaku - $back_bind;
        $batangwrap = $rwrap->jml_bahanbaku - $back_wrap;
        $batangdry3 = $werty->jml_dry3 + ( $batang + $mutasi );
        $batangqc = $qusrt->jml_qc - $batang;

        $uprfill = Stock_probaku::find($rfill->id);
        $uprfill->jml_bahanbaku = $batangfill;
        $uprfill->save();
        $uprlaz = Stock_probaku::find($rlaz->id);
        $uprlaz->jml_bahanbaku = $batanglaz;
        $uprlaz->save();
        $uprbind = Stock_probaku::find($rbind->id);
        $uprbind->jml_bahanbaku = $batangbind;
        $uprbind->save();
        $uprwrap = Stock_probaku::find($rwrap->id);
        $uprwrap->jml_bahanbaku = $batangwrap;
        $uprwrap->save();
        $update3 = Stock_drying3::find($request->iddrying3);
        $update3->jml_dry3 = $batangdry3;
        $update3->save();
        $update2 = Stock_qc::find($request->idqc);
        $update2->jml_qc = $batangqc;
        $update2->save();

        $query1 = Quality_control::where('id',$id)->delete();
        if ($query1) {
            $this->produksi->loop_upqc($request,$id,$awlbatang);
            $alert = 'Quality Control ID : '.$id.' berhasil dihapus!';
        } else {
            $alert = 'Quality Control ID : '.$id.' gagal dihapus!';
        }
        echo json_encode($alert);
        DB::commit();      
    }

}
