<script>
	let token = $("meta[name='csrf-token']").attr("content");
	jsGrid.setDefaults({
		tableClass: "jsgrid-table table table-striped table-hover"
	});
	jsGrid.setDefaults("text", {
		_createTextBox: function() {
			return $("<input>").attr("type", "text").attr("class", "form-control input-sm")
		}
	});
	jsGrid.setDefaults("number", {
		_createTextBox: function() {
			return $("<input>").attr("type", "number").attr("class", "form-control input-sm")
		}
	});
	jsGrid.setDefaults("textarea", {
		_createTextBox: function() {
			return $("<input>").attr("type", "textarea").attr("class", "form-control")
		}
	});
	jsGrid.setDefaults("control", {
		_createGridButton: function(cls, tooltip, clickHandler) {
			var grid = this._grid;
			return $("<button>").addClass(this.buttonClass).addClass(cls).attr({
				type: "button",
				title: tooltip
			}).on("click", function(e) {
				clickHandler(grid, e)
			})
		}
	});
	jsGrid.setDefaults("select", {
		_createSelect: function() {
			var $result = $("<select>").attr("class", "form-control input-sm"),
			valueField = this.valueField,
			textField = this.textField,
			selectedIndex = this.selectedIndex;
			return $.each(this.items, function(index, item) {
				var value = valueField ? item[valueField] : index,
				text = textField ? item[textField] : item,
				$option = $("<option>").attr("value", value).text(text).appendTo($result);
				$option.prop("selected", selectedIndex === index)
			}), $result
		}
	});
	$('#grid_merekcukau').jsGrid({
		height: "500px",
		width: "100%",
		filtering: true,
		filterable:true,
		editing: true,
		inserting: true,
		sorting: true,
		paging: true,
		autoload: true,
		pageSize: 15,
		pageButtonCount: 5,
		deleteConfirm: "Apakah anda yakin ingin menghapus data ini ?",
		controller: {
			loadData: function(filter){
				return $.ajax({
					type: "GET",
					dataType: 'json',
					url: "/api/merek_cukai",
					data: filter
				});
			},
			insertItem: function(item){
				item._token = $('meta[name="csrf-token"]').attr('content');
				return $.ajax({
					type: "POST",
					url: "/api/merek_cukai/post",
					data:item
				});
			},
			updateItem: function(item){
				item._token = $('meta[name="csrf-token"]').attr('content');
				return $.ajax({
					type: "PUT",
					url: "/api/merek_cukai/put",
					data: item
				});
			},
			deleteItem: function(item){
				item._token = $('meta[name="csrf-token"]').attr('content');
				return $.ajax({
					type: "DELETE",
					url: "/api/merek_cukai/del",
					data: item
				});
			},
		},

		fields: [
		{
			name: "id",
			title: "ID",
			type: "hidden",
			css: 'hide',
			width: 50
		},
		{
			name: "kode_cukai",
			title: "Kode Cukai",
			type: "text", 
			width: 50
		},
		{
			name: "nama_cukai",
			title: "Nama Cukai",
			type: "text", 
			width: 50
		},
		{
			name: "keterangan",
			title: "Keterangan",
			type: "text", 
			width: 50
		},
		{
			type: "control"
		}
		]
	});

	
	function Refresh() {
		$("#grid_merekcukau").jsGrid("loadData");
	}
</script>