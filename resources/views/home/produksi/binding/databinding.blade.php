@extends('../../layout')

@section('title')
<title>BIN - ERP : Data Binding</title>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css" />
@endsection

@section('content')
<div class="row page-titles">
	<div class="col-md-5 align-self-center">
		<h3 class="text-themecolor">Binding</h3>
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a href="javascript:void(0)">Proses</a></li>
			<li class="breadcrumb-item active">Data Binding</li>
		</ol>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="row">
			<div class="col-md-6">
				<legend>Data Binding</legend>
			</div>
			<div class="col-md-2 offset-md-4">
				<button type="button" data-link="{{ route('binding.post') }}" class="btn btn-success btn-rounded btn-icons " id="tambah-binding">Tambah</button>
			</div>
			<div class="table-responsive">
				<table id="table-binding" class="display nowrap table table-hover table-striped table-bordered">
					<thead>
						<tr>
							<th>No.</th>
							<th>Tanggal</th>
							<th>Produk</th>
							<th>Jenis</th>
							<th>Stock</th>
							<th>Masuk (Weis)</th>
							<th>Terpakai (Produksi)</th>
							<th>Terpakai (Weis)</th>
							<th>Sisa</th>
							<th>Batang Filling</th>
							<th>Jml Batang</th>
							<th>Aksi</th>
						</tr>
					</thead>
				</table>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="modal-binding" tabindex="-1" role="dialog" aria-labelledby="title-tembakau" aria-hidden="true" style="display: none;">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="card">
				<div class="card-header">
					<div class="modal-header">
						<h4 class="modal-title" id="title"></h4>
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					</div>
				</div>
				<div class="card-body">
					<form id="form-binding" method="POST">
						<div class="modal-body row">
							<div class="form-group col-md-12">
								<label class="control-label">Tanggal</label>
								<input type="text" class="form-control date_sam" id="tanggal_binding">
							</div>
							<div class="form-group col-md-6">
								<label class="control-label">Jenis</label>
								<select class="form-control" id="jenisbind"></select>
								<input type="hidden" id="jenis_inbind">
							</div>
							<div class="form-group col-md-6">
								<label class="control-label">Produk</label>
								<select class="form-control" id="produkbind"></select>
								<input type="hidden" id="produk_inbind">
							</div>
							<div class="form-group col-md-12" id="stock_showitbind">
								<button type="button" onclick="seacrh_databind()" class="btn btn-primary btn-xs btn-block">Search</button>
								<input type="hidden" id="idprobakubind">
								<input type="hidden" id="idfillingbind">
								<input type="hidden" id="idbinding">
								<input type="hidden" id="idjnprobind">
							</div>
							<div class="form-group col-md-4">
								<label class="control-label">Stock Produksi (Omblad)</label>
								<input type="number" class="form-control" step="any" id="stockprobind" value="0" placeholder="Stock (Saat ini/Awal)" readonly>
							</div>
							<div class="form-group col-md-4">
								<label class="control-label">Stock Weis</label>
								<input type="number" class="form-control" step="any" id="stockweisb" value="0" placeholder="Stock Weis" readonly>
							</div>
							<div class="form-group col-md-4">
								<label class="control-label">Batang Filling</label>
								<input type="number" class="form-control" id="batangfill" placeholder="Jml Batang Filling" readonly>
							</div>
							<div class="form-group col-md-12">
								<label class="control-label">Terpakai Produksi</label>
								<input type="number" class="form-control" step="any" id="terpakaiprobind" placeholder="Stock Produksi Terpakai">
							</div>
							<div class="form-group col-md-6">
								<label class="control-label">Masuk Weis</label>
								<input type="number" class="form-control" step="any" id="masukweisb" placeholder="Stock Masuk Weisf">
							</div>
							<div class="form-group col-md-6">
								<label class="control-label">Terpakai Weisf</label>
								<input type="number" class="form-control" step="any" id="terpakaiweisb" placeholder="Stock Produksi Weisf">
							</div>
							<div class="form-group col-md-12">
								<label class="control-label">Batang Cerutu Yang Dihasilkan</label>
								<input type="number" class="form-control" id="batangjmlbind" placeholder="Jml Batang">
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" onclick="save_methodbind()" class="btn btn-primary waves-effect text-left">Save</button>
							<button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Close</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('javascript')
<script src="{{ asset('node_modules/moment/moment.js') }}">
</script><script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
<script src="{{ asset('node_modules/datatables/datatables.min.js') }}"></script>
<script>
	$('#stock_showitbind').hide();
	let token = $("meta[name='csrf-token']").attr("content");

	const jenis = {!! json_encode($jenis) !!};
	jenis.unshift({id_jenis: '',jenis: ''});

	var tablebind = $('#table-binding').DataTable({
		"lengthMenu": [
		[3,5,10, 25, 50, 100, 150, 200, -1],
		[3,5,10, 25, 50, 100, 150, 200, "All"]
		],
		"order": [
		[0, 'desc']
		],
		processing: true,
		serverSide: true,
		ajax: '/databinding/json',
		columns: [
		{ data: 'DT_RowIndex', name: 'DT_RowIndex' },
		{ data: 'tanggal_bind', name: 'tanggal_bind' },
		{ data: 'produk', name: 'produk' },
		{ data: 'jenis', name: 'jenis' },
		{ data: 'stockawal', name: 'stockawal' },
		{ data: 'weisbmasuk', name: 'weisbmasuk' },
		{ data: 'terpakaiprob', name: 'terpakaiprob' },
		{ data: 'terpakaiweisb', name: 'terpakaiweisb' },
		{ data: 'sisaprob', name: 'sisaprob' },
		{ data: 'awlbatang', name: 'awlbatang' },
		{ data: 'hasil_bind', name: 'hasil_bind' },
		{
			data: null,
			"bSortable": false,
			mRender: function (o) { 
				return '<button type="button" class="btn btn-info btn-rounded btn-xs" id="update-binding" data-title="Update binding | ID: '+ o.id +'" data-link="/databinding/upp/'+ o.id +'/'+ o.weisbmasuk +'/'+ o.terpakaiprob +'/'+ o.terpakaiweisb +'/'+ o.hasil_bind +'" data-tanggal_binding="'+ o.tanggal_bind +'" data-jenis="'+ o.id_jenis +'" data-produk="'+ o.id_produk +'" data-stockpro="'+ o.stockawal +'" data-stockweis="'+ o.stockawalweis +'" data-stockbatang="'+ o.awlbatang +'" data-masukweis="'+ o.weisbmasuk +'" data-terpakaiprob="'+ o.terpakaiprob +'" data-terpakaiweisb="'+ o.terpakaiweisb +'" data-batang="'+ o.hasil_bind +'" data-idprobaku="'+ o.stock_probaku_id +'" data-idfilling="'+ o.stock_filling_id +'" data-idbinding="'+ o.stock_binding_id +'"><i class="mdi mdi-dots-horizontal"></i></button><button type="button" class="btn btn-danger btn-rounded btn-xs" id="delete-binding" data-link="/databinding/del/'+ o.id +'/'+ o.weisbmasuk +'/'+ o.terpakaiprob +'/'+ o.terpakaiweisb +'/'+ o.hasil_bind +'" data-idprobaku="'+ o.stock_probaku_id +'" data-idfilling="'+ o.stock_filling_id +'" data-idbinding="'+ o.stock_binding_id +'"><i class="fa fa-trash"></i></button>'; }
			},
			],
			"displayLength": 10,
		});
	$('#table-binding tbody').on('click', 'tr.group', function() {
		var currentOrder = tablebind.order()[0];
		if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
			tablebind.order([2, 'desc']).draw();
		} else {
			tablebind.order([2, 'asc']).draw();
		}
	});
	$('#table-binding tbody').on('click', 'tr.group', function() {
		var currentOrder = tablebind.order()[0];
		if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
			tablebind.order([2, 'desc']).draw();
		} else {
			tablebind.order([2, 'asc']).draw();
		}
	});

	function getdatasearchbind(position1,position2,position3) {
		let jenis = $('#jenis_inbind').val();
		let produk = $('#produk_inbind').val();

		$.ajax({
			url: '/databinding/getsearchfill',
			type: 'POST',
			dataType: 'json',
			data: {
				"_token": token,
				produk: produk,
				jenis: jenis
			},
		})
		.done(function(data) {
			$('#idfillingbind').val(data.idfilling);
			$('#idbinding').val(data.idbinding);
			$('#idprobakubind').val(data.id);
			$('#' + position1).val(data.jml_produksi);
			$('#' + position2).val(data.stock_weisb);
			$('#' + position3).val(data.jml_fill);
		});
		
	}

	function ljenis(position,like) {
		let opt = '';
		for (let i = 0; i < jenis.length; i++) {
			opt += '<option value="'+ jenis[i].id_jenis +'"';
			if (jenis[i].id_jenis == like) {
				opt += 'selected';
			}
			opt += '>'+ jenis[i].jenis +'</option>';
		}
		$('#' + position).html(opt);
	}

	function lproduk(position,based,like) {
		$.ajax({
			url: '/datafilling/getproduk',
			type: 'POST',
			dataType: 'json',
			data: {
				"_token": token,
				jenis: based
			},
		})
		.done(function(data) {
			let opt = '<option value=""></option>';
			for(let i = 0; i < data.length; i++) {
				opt += '<option value="'+ data[i].id_produk +'"';
				if (data[i].id_produk == like) {
					opt += 'selected';
				}
				opt += ' data-id="'+ data[i].relid +'">'+ data[i].produk +'</option>';
			}
			$('#' + position).html(opt);
		});
	}

	function seacrh_databind() {
		getdatasearchbind('stockprobind','stockweisb','batangfill');
	}

	function save_methodbind() {
		let link = $('#form-binding').attr('action');
		let idprobaku = $('#idprobakubind').val();
		let idbinding = $('#idbinding').val();
		let idfilling = $('#idfillingbind').val();
		let idjnpro = $('#idjnprobind').val();
		let tanggal_binding = $('#tanggal_binding').val();
		let jenis_in = $('#jenis_inbind').val();
		let produk_in = $('#produk_inbind').val();
		let stockpro = $('#stockprobind').val();
		let stockweisb = $('#stockweisb').val();
		let stockbatang = $('#batangfill').val();
		let masukweisb = $('#masukweisb').val();
		let terpakaipro = $('#terpakaiprobind').val();
		let terpakaiweisb = $('#terpakaiweisb').val();
		let batangjml = $('#batangjmlbind').val();
		$.ajax({
			url: link,
			type: 'POST',
			dataType: 'json',
			data: {
				"_token": token,
				tanggal_binding: tanggal_binding,
				jenis: jenis_in,
				produk: produk_in,
				stockpro: stockpro,
				stockweisb: stockweisb,
				stockbatang: stockbatang,
				masukweisb:masukweisb,
				terpakaipro: terpakaipro,
				terpakaiweisb: terpakaiweisb,
				batangjml: batangjml,
				idprobaku: idprobaku,
				idfilling: idfilling,
				idbinding: idbinding,
				idjnpro:idjnpro
			},
		})
		.done(function(data) {
			alert(data);
			// console.log(data);
			$('#table-binding').DataTable().ajax.reload();
			$('#modal-binding').modal('toggle');
		});
		
	}

	$(document).on('click', '#tambah-binding', function(event) {
		let clik = $(this);
		$('.modal-header #title').text('Tambah binding');
		$('#form-binding').attr('action', clik.data('link'));
		$('#form-binding').find('input').val('');
		$('#form-binding').find('select').val('');
		$("#tanggal_binding").datetimepicker({
			format: 'YYYY-MM-DD HH:mm:ss',
		}).data("DateTimePicker").date(new Date());
		$('#stock_showit').fadeIn();
		$('#jenisbind').removeAttr('disabled');
		$('#produkbind').removeAttr('disabled');
		ljenis('jenisbind');
		$(document).on('change', '#jenisbind', function(event) {
			let jam = $(this).val();
			$('#jenis_inbind').val(jam);
			lproduk('produkbind',jam);
			$(document).on('change', '#produkbind', function(event) {
				let pwe = $(this).val();
				$('#produk_inbind').val(pwe);
				let kk = $(this).find(':selected').data('id');
				$('#idjnprobind').val(kk);
			});
		});
		$('#modal-binding').modal('show');
	});

	$(document).on('click', '#update-binding', function(event) {
		let click = $(this);
		$('.modal-header #title').text(click.data('title'));
		$('#form-binding').attr('action', click.data('link'));
		$('#form-binding').find('input').val('');
		$('#form-binding').find('select').val('');
		$('#stock_showit').hide();
		$('#jenisbind').attr('disabled','disabled');
		$('#produkbind').attr('disabled','disabled');
		ljenis('jenisbind',click.data('jenisbind'));
		lproduk('produkbind',click.data('jenisbind'),click.data('produkbind'));

		$("#tanggal_binding").datetimepicker({
			format: 'YYYY-MM-DD HH:mm:ss',
		}).data("DateTimePicker").date(click.data('tanggal_binding'));
		$('#jenis_inbind').val(click.data('jenisbind'));
		$('#produk_inbind').val(click.data('produkbind'));
		$('#stockprobind').val(click.data('stockpro'));
		$('#stockweisb').val(click.data('stockweis'));
		$('#batangfill').val(click.data('stockbatang'));
		$('#masukweisb').val(click.data('masukweis'));
		$('#terpakaiprobind').val(click.data('terpakaiprob'));
		$('#terpakaiweisb').val(click.data('terpakaiweisb'));
		$('#batangjmlbind').val(click.data('batang'));
		$('#idprobakubind').val(click.data('idprobaku'));
		$('#idbinding').val(click.data('idbinding'));
		$('#idfillingbind').val(click.data('idfilling'));
		$('#modal-binding').modal('show');
	});

	$(document).on('click', '#delete-binding', function(event) {
		let snjs = confirm('Apakah anda yakin ingin menghapus data ini ?');
		if (snjs) {
			let link = $(this).data("link");
			let idprobaku = $(this).data('idprobaku');
			let idfilling = $(this).data('idfilling');
			let idbinding = $(this).data('idbinding');
			$.ajax(
			{
				url: link,
				type: 'DELETE',
				data: {
					"_token": token,
					idprobaku: idprobaku,
					idfilling: idfilling,
					idbinding: idbinding,
				},
				success: function (data){
					alert(data);
					$('#table-binding').DataTable().ajax.reload();
				}
			});
		}
	});
</script>
@endsection