<script>
	$(document).ready(function() {
		$.extend(true, $.fn.datetimepicker.defaults, {
			icons: {
				time: 'far fa-clock',
				date: 'far fa-calendar',
				up: 'fas fa-arrow-up',
				down: 'fas fa-arrow-down',
				previous: 'fas fa-chevron-left',
				next: 'fas fa-chevron-right',
				today: 'fas fa-calendar-check',
				clear: 'far fa-trash-alt',
				close: 'far fa-times-circle'
			}
		});
		let token = $("meta[name='csrf-token']").attr("content");
		$('#loadingprosess').hide();
		$('#tambahshow').hide();

		let leveluser = {{ Auth::user()->level_user_id_level }};
		let leon = {{ $isi->verifikasi_rfs }};
		let gher = {{ $isi->status_terima }};
		const produk = {!! json_encode($produk) !!}
		produk.unshift({id_produk: '',produk: ''});
		const verifikasi = {!! json_encode($verifikasi) !!};
		const terima = {!! json_encode($terima) !!};
		const array = {!! json_encode($klasifikasi) !!};
		const jerl = [0,1];
		let idorder = {{ $isi->id }};


		$("#tanggal_order").datetimepicker({
			format: 'YYYY-MM-DD HH:mm:ss',
		});

		loaduser('userbagan',{{ $isi->level_user_id_level }},{{ $isi->users_id }});	

		if (!array.includes(leveluser) && !jerl.includes(leon) || gher == 1) {
			// $('#userbagan').removeAttr('disabled');
			// $('#verifikasi_rfs').removeAttr('disabled');
			// $('#status_terima').removeAttr('disabled');
			$('#userbagan').attr('disabled','disabled');
			$('#verifikasi_rfs').attr('disabled','disabled');
			$('#status_terima').attr('disabled','disabled');
		}

		$(document).on('change', '#level', function(event) {
			loaduser('userbagan',$(this).val());
			$('#level_in').val($(this).val());
			$(document).on('change', '#userbagan', function(event) {
				let pl = $(this).val();
				$('#userbagan_in').val(pl);
			});
		});

		function loaduser(position,base,active = '') {
			$.ajax({
				url: '/api/rfs/getbaganuser',
				type: 'POST',
				dataType: 'json',
				data: {
					"_token": token,
					level: base
				},
			})
			.done(function(data) {
				let opt = '<option value=""></option>';
				for(let i = 0; i < data.length; i++) {
					opt += '<option value="'+ data[i].id +'" ';
					if (data[i].id == active) {opt += 'selected ';}
					opt += '>'+ data[i].username +'</option>';
				}
				$('#' + position).html(opt);
			});
		}

		function lproduk(position,like) {
			let opt = '';
			for (let i = 0; i < produk.length; i++) {
				opt += '<option value="'+ produk[i].id_produk +'"';
				if (produk[i].id_produk == like) {
					opt += 'selected';
				}
				opt += '>'+ produk[i].produk +'</option>';
			}
			$('#' + position).html(opt);
		}

		function lsubproduk(position,based,liker = '') {
			$.ajax({
				url: '/api/Report/getsubproduk/json',
				type: 'POST',
				dataType: 'json',
				data: {
					"_token": token,
					produk: based
				},
			})
			.done(function(data) {
				let opt = '<option value=""></option>';
				for(let i = 0; i < data.length; i++) {
					opt += '<option value="'+ data[i].id_sub_produk +'" data-name="'+ data[i].sub_kode +' - '+ data[i].sub_produk +'" ';
					if (data[i].id_sub_produk == liker) {opt += 'selected ';}
					opt += '>'+ data[i].sub_kode +' - '+ data[i].sub_produk +'</option>';
				}
				$('#' + position).html(opt);
			});
		}

		verifikasival('verifikasi_rfs',{{ $isi->verifikasi_rfs }});
		$(document).on('change', '#verifikasi_rfs', function(event) {
			let krnk = $(this).val();
			$('#verifikasi_rfs_in').val(krnk);
		});

		function verifikasival(posisi,like) {
			$opt = '';
			$.each(verifikasi,function( index , key ) {
				$opt += '<option value="'+ index +'"';
				if (index == like) {
					$opt += 'selected';
				}
				$opt += '>'+ key +'</option>';
			});
			$('#' + posisi).html($opt);
		}

		terimaval('status_terima',{{ $isi->status_terima }});

		$(document).on('change', '#status_terima', function(event) {
			let jnka = $(this).val();
			$('#status_terima_in').val(jnka);
		});

		function terimaval(posisi,like) {
			$opt = '';
			$.each(terima,function( index , key ) {
				$opt += '<option value="'+ index +'" ';
				if (index == like) {
					$opt += 'selected';
				}
				$opt += '>'+ key +'</option>';
			});
			$('#' + posisi).html($opt);
		}

		var table = $('#items_order').DataTable({
			"lengthMenu": [
			[3,5,10, 25, 50, 100, 150, 200, -1],
			[3,5,10, 25, 50, 100, 150, 200, "All"]
			],
			"order": [
			[0, 'desc']
			],
			processing: true,
			serverSide: true,
			ajax: '/api/noc_rfsbagan/items/json/' + {{ $isi->id }},
			createdRow: function( row, data, dataIndex ) {
				if (array.includes(leveluser) && gher == 0) {
					$( row ).find('td:eq(2)').attr({
						'id': 'update-tablerfsbagan',
						'data-id':data.id,
						'data-column': 'jml_item',
						'contenteditable': ''
					});
				} else {
					$( row ).find('td:eq(2)').attr({
						'id': 'update-tablerfsbagan',
						'data-id':data.id,
						'data-column': 'jml_item'
					});

				}
			},
			columns: [
			{ data: 'DT_RowIndex', name: 'DT_RowIndex' },
			{ data: 'kodeproduk', name: 'kodeproduk' },
			{ data: 'jml_item', name: 'jml_item' },
			{
				data: null,
				"bSortable": false,
				mRender: function (o) { 
					$button = '';
					if (array.includes(leveluser) && gher == 0) {
						// $button += '<button type="button" class="btn btn-info btn-rounded btn-xs" id="update-rfsbagan"><i class="mdi mdi-dots-horizontal"></i></button>';
						$button += '<button type="button" class="btn btn-danger btn-rounded btn-xs" id="delete-rfsbagan" data-link="/api/rfs/data/delkeranjangitems/'+ o.id +'"><i class="fa fa-trash"></i></button>';
					}
					return $button; 
				}
			},
			],
			"displayLength": 10,
		});
		$('#items_order tbody').on('click', 'tr.group', function() {
			var currentOrder = table.order()[0];
			if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
				table.order([2, 'desc']).draw();
			} else {
				table.order([2, 'asc']).draw();
			}
		});
		$('#items_order tbody').on('click', 'tr.group', function() {
			var currentOrder = table.order()[0];
			if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
				table.order([2, 'desc']).draw();
			} else {
				table.order([2, 'asc']).draw();
			}
		});

		$(document).on('click', '#cari-data', function(event) {
			let produk_in = $('#produk_in').val();
			let subproduk_in = $('#subproduk_in').val();
			$.ajax({
				url: '/api/rejectrfs/item',
				type: 'POST',
				dataType: 'json',
				data: {
					"_token": token,
					produk: produk_in,
					subproduk: subproduk_in,
				},
			})
			.done(function(data) {
				$('#stockjmlrfs').val(data.jml);
			});
		});

		$(document).on('click', '#optklick', function(event) {
			let div = $('#tambahshow');

			div.fadeToggle(500, function() {
				$('#form-tambahsub').find('input, select, textarea').val('');
				lproduk('produk_in');
				$(document).on('change', '#produk_in', function(event) {
					let val = $(this).val();
					lsubproduk('subproduk_in',val);
				});
			});
		});

		$(document).on('click', '#tambah_itemsobject', function(event) {
			let subproduk_in = $('#subproduk_in').val();
			let jml_keluar = $('#jml_keluar').val();
			$.ajax({
				url: '/api/rfs/data/addkeranjangitems',
				type: 'POST',
				dataType: 'json',
				data: {
					"_token": token,
					subproduk: subproduk_in,
					idorder: idorder,
					jml_keluar: jml_keluar,
				},
				beforeSend: function() {
					isProcessing = true;
					$('#loadingprosess').fadeIn();
				},
				success: function(data) {
					$('#loadingprosess').hide();
					alert(data);
					isProcessing = false;
					$('#items_order').DataTable().ajax.reload();
				}
			});
		});

		$(document).on('blur', '#update-tablerfsbagan', function(){
			var id = $(this).data('id');
			var table_column = $(this).data('column');
			var value = $(this).text();
			$.ajax({
				url: '/api/rfs/data/uppkeranjangitems',
				method:"PUT",
				data:{
					"_token": token,
					rowId:id,
					column_name:table_column,
					jml:value
				},
				beforeSend: function() {
					isProcessing = true;
					$('#loadingprosess').fadeIn();
				},
				success: function (data){
					$('#loadingprosess').hide();
					isProcessing = false;
					alert(data);
					$('#items_order').DataTable().ajax.reload();
				}
			})
		});

		$(document).on('click', '#delete-rfsbagan', function(event) {
			let hks = confirm('Apakah anda yakin ingin menghapus data ini ?');
			if (hks) {
				let click = $(this);
				$.ajax({
					url: click.data('link'),
					type: 'DELETE',
					dataType: 'json',
					data: {
						"_token": token,
					},
					beforeSend: function() {
						isProcessing = true;
						$('#loadingprosess').fadeIn();
					},
					success: function (data){
						$('#loadingprosess').hide();
						isProcessing = false;
						alert(data);
						$('#items_order').DataTable().ajax.reload();
					}
				});
			}
		});

	});
</script>