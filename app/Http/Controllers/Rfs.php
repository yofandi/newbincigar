<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use DataTables;
use Auth;
use Cart;
use App\User;
use App\RFS_HIS;
use App\Order;
use App\Items_object;
use App\Noc_inrfs;
use App\Stock_rfs;

class Rfs extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $data['produk'] = DB::table('produk')->get();
        $data['level'] = DB::table('level_user')->select('id','level')->whereIn('id',[7,8,9,10,11])->get();
        return view('home.rfs.data_rfs',$data);
    }

    public function getlevelbagan(Request $request)
    {
        $query = User::where('level_user_id_level',$request->level)->select('id','username')->get();
        return json_encode($query);
    }

    public function getdatarfs()
    {
        // $query = RFS_HIS::all();
        $query = DB::table('rfs_his')
        ->select('rfs_his.*','users.username','level_user.level','order.status_terima')
        ->join('order','rfs_his.id','=','order.rfs_id')
        ->join('users','rfs_his.users_id','=','users.id')
        ->join('level_user','users.level_user_id_level','=','level_user.id')
        ->get();
        return Datatables::of($query)
        ->addIndexColumn()
        ->addColumn('status', function ($query) {
            switch ($query->status_terima) {
                case 0:
                $rty = 'Menunggu';
                break;
                
                case 1:
                $rty = 'Diterima';
                break;
            }
            return $rty;
        })
        ->make(true);
    }

    public function inrfs_addcartreject(Request $request)
    {
        $data = [
            'id' => $request->subproduk,
            'name' => $request->name,
            'qty' => $request->jml,
            'price' => 0,
            'options' => ['rfs' => $request->idrfs]
        ];
        $query = Cart::add($data);
        $if = $query ? 'data berhasil ditambahkan' : 'data gagal ditambahkan';
        return json_encode($if);
    }

    public function inrfs_updcartreject(Request $request)
    {
        $data = [
            $request->column_name => $request->jml
        ];
        $query = Cart::update($request->rowId,$data);
        $if = $query ? 'data '.$request->name.' berhasil diupdate' : 'data '.$request->name.' gagal diupdate';
        return json_encode($if);
    }

    public function inrfs_deletereject($rowId,$name)
    {
        $query = Cart::remove($rowId);
        $if = $query ? 'data '.$name.' gagal dihapus' : 'data '.$name.' berhasil dihapus';
        return json_encode($if);   
    }

    public function getsessioncartreject()
    {
        $data1 = [];
        $data = [];
        $query = Cart::content();
        foreach ($query as $key) {
            $data['rowId'] = $key->rowId;
            $data['id'] = $key->id;
            $data['name'] = $key->name;
            $data['qty'] = $key->qty;
            $data['price'] = $key->price;
            $data['options'] = $key->options;
            $data1[] = $data;
        }
        return json_encode($data1);
    }

    public function getquanity(Request $request)
    {
        $query = DB::table('stock_rfs')
        ->select('id','jml_rfs')
        ->join('sub_produk','stock_rfs.sub_produk_id_sub_produk','=','sub_produk.id_sub_produk')
        ->where([
            ['sub_produk.produk_id_produk','=',$request->produk],
            ['sub_produk.id_sub_produk','=',$request->subproduk]
        ]);
        if ($query->count() > 0) {
            $rte = $query->first();
            $variable = ['jml' => $rte->jml_rfs,'id' => $rte->id];
        } else {
            $variable = ['jml' => 0,'id' => ''];
        }
        return json_encode($variable);
    }

    public function getstockrfs()
    {
        $query = DB::table('stock_rfs')
        ->select('stock_rfs.*','sub_produk.id_sub_produk','sub_produk.sub_kode','sub_produk.sub_produk')
        ->join('sub_produk','stock_rfs.sub_produk_id_sub_produk','=','sub_produk.id_sub_produk')
        ->get();
        return Datatables::of($query)
        ->addIndexColumn()
        ->make(true);   
    }

    public function getitemsupdate($idorder)
    {
        $query = DB::table('items_object')
        ->select('items_object.*','sub_produk.id_sub_produk','sub_produk.sub_kode','sub_produk.sub_produk')
        ->join('sub_produk','items_object.sub_produk_id','=','sub_produk.id_sub_produk')
        ->where('order_id',$idorder)
        ->get();
        return json_encode($query);
    }

    public function loop_insertcart($id)
    {
        $query = Cart::content();
        foreach ($query as $key) {
            DB::table('noc_items')->insert([
                'sub_produk_id' => $key->id,
                'noc_inrfs_id' => $id,
                'created_at' => date('Y-m-d'),
                'jml_nocitm' => $key->qty,
            ]);
        }   
    }

    public function addrejectall(Request $request)
    {
        // arah itu maksudnya barang dikirim dari mana ke mana
        // 0 : packing->quality->rfs
        // 1 : rfs->quality->packing or proses
        $query = new Noc_inrfs;
        $query->tanggal_inrfs = $request->tanggal;
        $query->status_lihat = 0;
        $query->status_inrfs = 0;
        $query->arah_dari = 1;
        $query->keterangan = $request->keterangan;
        $query->author_session = Auth::user()->id;
        $query->created_at = date('Y-m-d');
        $query->log = 'INSERT';
        $save = $query->save();
        if ($save) {
            $id = $query->id;
            $this->loop_insertcart($id);
            $alert = 'Data berhasil ditambahkan, tinggal menunggu verifikasi Quality Control';
        } else {
            $alert = 'Data gagal ditambahkan';
        }
        Cart::destroy();
        echo json_encode($alert);
    }

    public function addsessioncartrfs_bagan(Request $request)
    {
        $data = [
            'id' => $request->subproduk,
            'name' => $request->name,
            'qty' => $request->jml,
            'price' => 0,
            'options' => []
        ];
        $query = Cart::add($data);
        $if = $query ? 'data berhasil ditambahkan kirim' : 'data gagal ditambahkan kirim';
        return json_encode($if);
    }

    public function loopinginset_item($id)
    {
        $query = Cart::content();
        foreach ($query as $key) {
            $Rty = new Items_object;
            $Rty->sub_produk_id = $key->id;
            $Rty->order_id = $id;
            $Rty->jml_item = $key->qty;
            $Rty->created_at = date('Y-m-d H:i:s');
            $Rty->save();
        }
    }

    public function rfsinsert(Request $request)
    {
        DB::beginTransaction();
        $rtey = new RFS_HIS;
        $rtey->tanggal_rfs = $request->tanggal_rfs;
        $rtey->keterangan_rfs = $request->ket_rfs;
        $rtey->users_id = $request->userbagan;
        $rtey->author_session = Auth::User()->id;
        $rtey->created_at = date('Y-m-d H:i:s');
        $rtey->log = 'INSERT';
        $jke  = $rtey->save();
        if ($jke) {
            $yuen = new Order;
            $yuen->tanggal_order = $request->tanggal_rfs;
            $yuen->keterangan_order = 'Barang Masuk dari RFS tanggal: '.$request->tanggal_rfs;
            $yuen->status_lihatrfs = 0;
            $yuen->verifikasi_rfs = 2;
            $yuen->status_lihatbagan = 0;
            $yuen->status_terima = 0;
            $yuen->users_id = $request->userbagan;
            $yuen->rfs_id = $rtey->id;
            $yuen->author_session = Auth::User()->id;
            $yuen->created_at = date('Y-m-d H:i:s');
            $yuen->save();

            $this->loopinginset_item($yuen->id);
            Cart::destroy();
            $alert = 'Data RFS berhasil ditambahkan';
        } else {
            $alert = 'Data RFS gagal ditambahkan';
        }

        echo json_encode($alert);
        DB::commit();
    }

    public function addnewitem_object(Request $request)
    {
        $data = [
            'sub_produk_id' => $request->subproduk,
            'order_id' => $request->idorder,
            'jml_item' => $request->jml_keluar,
            'created_at' => date('Y-m-d H:i:s'),
        ];
        $query = Items_object::insert($data);
        $alert = ( $query ? 'Data berhasil ditambahkan' : 'Data gagal ditambahkan');
        return json_encode($alert);
    }

    public function uppnewitem_object(Request $request)
    {
        $data = [
            $request->column_name => $request->jml,
        ];
        $query = Items_object::where('id',$request->rowId)->update($data);
        $alert = ( $query ? 'Data berhasil diupdate' : 'Data gagal diupdate');
        return json_encode($alert);
    }

    public function delnewitem_object($id)
    {
        $query = Items_object::find($id)->delete();
        $alert = ( $query ? 'Data berhasil dihapus' : 'Data gagal dihapus');
        return json_encode($alert);
    }

    public function viewupdate_rfs($id)
    {
        $data['isi'] = DB::table('rfs_his')
        ->select('rfs_his.*','users.level_user_id_level','order.id AS idorder')
        ->join('order','rfs_his.id','=','order.rfs_id')
        ->join('users','rfs_his.users_id','=','users.id')
        ->where('rfs_his.id',$id)
        ->first();
        $data['produk'] = DB::table('produk')->get();
        $data['level'] = DB::table('level_user')->select('id','level')->whereIn('id',[7,8,9,10,11])->get();
        return view('home.rfs.update_rfs',$data);   
    }

    public function rfsupdate(Request $request,$id)
    {
        $data = [
            'rfs_his.tanggal_rfs' => $request->tanggal_rfs,
            'rfs_his.keterangan_rfs' => $request->ket_rfs,
            'rfs_his.users_id' => $request->userbagan,
            'rfs_his.log' => 'UPDATE',
            'order.tanggal_order' => $request->tanggal_rfs,
            'order.users_id' => $request->userbagan,
        ];
        $query = DB::table('rfs_his')
        ->join('order','rfs_his.id','=','order.rfs_id')
        ->where('rfs_his.id',$id)
        ->update($data);
        if ($query) {
            echo '<script> alert("Data RFS berhasil diupdate"); window.location="'.URL::to('/datarfs').'"</script>';
        } else {
            echo '<script> alert("Data RFS gagal diupdate"); window.location="'.URL::to('/datarfs').'"</script>';
        }
    }

    public function rfsdelete($id)
    {
        $query = DB::table('rfs_his')
        ->join('order','rfs_his.id','=','order.rfs_id')
        ->join('items_object','order.id','=','items_object.order_id')
        ->where('rfs_his.id',$id)
        ->delete();
        $alert = ($query ? 'Data RFS ID: '.$id.' berhasil dihapus' : 'Data RFS ID: '.$id.' gagal dihapus');
        return json_encode($alert);
    }
}
