<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Drying2;
use App\Stock_frezer;
use App\Stock_drying2;

class Back_drying2 extends Controller
{
	public function sprodry2($request)
	{
		$qusrt = DB::table('stock_frezer')
		->select('stock_frezer.id','stock_frezer.jml_fre')
		->join('jenis_has_produk','stock_frezer.jenis_has_produk_id_jnpro','=','jenis_has_produk.id_jnpro')
		->where([['jenis_has_produk.jenis_id_jenis','=',$request->jenis],['jenis_has_produk.produk_id_produk','=',$request->produk]]);

		$qusrt1 = DB::table('stock_drying2')
		->select('stock_drying2.id','stock_drying2.jml_dry2')
		->join('jenis_has_produk','stock_drying2.jenis_has_produk_id_jnpro','=','jenis_has_produk.id_jnpro')
		->where([['jenis_has_produk.jenis_id_jenis','=',$request->jenis],['jenis_has_produk.produk_id_produk','=',$request->produk]]);

		if ($qusrt->count() > 0) {
			$aem = $qusrt->first();
			$qer = $aem->jml_fre;
			$qwc = $aem->id;
		} else { $qer = 0; $qwc = '';}

		if ($qusrt1->count() > 0) {
			$aem1 = $qusrt1->first();
			$ber = $aem1->jml_dry2;
			$jkd = $aem1->id;
		} else { $ber = 0; $jkd = ''; }
		$array = ['jml_fre' => $qer,'jml_dry2' => $ber,'idfrezzer' => $qwc,'iddrying2' => $jkd];
		return $array;
	}

	public function stockdry2($request)
	{
		$bang = $request->stockbatang - $request->batangjml;
		$cekwrt = Stock_frezer::find($request->idfrezzer);
		$cekwrt->jml_fre = $bang;
		$cekwrt->save();
		$resultidwrt = $cekwrt->id;

		if ($request->iddrying2 != '') {
			$id = $request->iddrying2; 
			$show = Stock_drying2::where('id',$id)->first();
			$jml = $show->jml_dry2 + $request->batangjml;

			$cek = Stock_drying2::find($id);
			$cek->jml_dry2 = $jml;
			$cek->save();

			$resultid = $cek->id;
		} else {
			$jml = $request->batangjml;
			$save = new Stock_drying2;

			$save->jml_dry2 = $jml;
			$save->jenis_has_produk_id_jnpro = $request->idjnpro;
			$save->jenis_has_produk_id_jenis = $request->jenis;
			$save->jenis_has_produk_id_produk = $request->produk;
			$save->save();
			$resultid = $save->id;
		}
		return ['idfrezzer' => $resultidwrt,'iddrying2' => $resultid,'jml_fre' => $bang,'jml_dry2' => $jml];
	}

	public function ifuppstockdry2($request,$batang)
	{
		if ($batang != $request->batangjml) {
			$shdry2 = Stock_drying2::where('id',$request->iddrying2)->first();
			$shfre = Stock_frezer::where('id',$request->idfrezzer)->first();

			if ($batang > $request->batangjml) {
				$nah = $batang - $request->batangjml;
				$hasilw = $shdry2->jml_dry2 - $nah;
				$slash = Stock_frezer::find($request->idfrezzer);
				$slash->jml_fre = $shfre->jml_fre + $nah;
				$slash->save();
			} elseif ($batang < $request->batangjml) {
				$nah = $request->batangjml - $batang;
				$hasilw = $shdry2->jml_dry2 + $nah;
				$slash = Stock_frezer::find($request->idfrezzer);
				$slash->jml_fre = $shfre->jml_fre - $nah;
				$slash->save();
			} else {
				$hasilw = $shdry2->jml_dry2;
			}

			$query2 = Stock_drying2::find($request->iddrying2);
			$query2->jml_dry2 = $hasilw;
			$query2->save();
		}
	}

	public function loop_updry2($request,$id,$batang = '')
	{
		$ceh = Drying2::where([['stock_frezer_id','=',$request->idfrezzer],['stock_drying2_id','=',$request->iddrying2],['id','>',$id]]);
		if ($ceh->count() > 0) {
			$reg = $ceh->get();
			for ($i=0; $i < $ceh->count(); $i++) {
				$show = Drying2::where([['stock_frezer_id','=',$request->idfrezzer],['stock_drying2_id','=',$request->iddrying2],['id','<',$reg[$i]->id]])->orderBy('id','desc')->take('1');
				if ($show->count() > 0) {
					$first = $show->first();
					$hsie = $first->hasil_akhirdry2 - $reg[$i]->tambah_dry2;
				} else {
					if ($batang == '') {
						$awl1 = $reg[$i]->hasil_akhirdry2 + $reg[$i]->tambah_dry2;
						$hsie = $awl1 - $reg[$i]->tambah_dry2;
					} else {
						$awl1 = $reg[$i]->hasil_akhirdry2 + $reg[$i]->tambah_dry2 + $batang;
						$hsie = $awl1 - $reg[$i]->tambah_dry2;
					}
				}
				$update = Drying2::where('id',$reg[$i]->id)->update(['hasil_akhirdry2' => $hsie]);
			}
		}
	}

}
