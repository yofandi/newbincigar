@extends('../../layout')

@section('title')
<title>BIN - ERP : Cukai</title>
@endsection

@section('content')
<div class="row page-titles">
	<div class="col-md-5 align-self-center">
		<h3 class="text-themecolor">Cukai</h3>
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a href="javascript:void(0)">Referensi</a></li>
			<li class="breadcrumb-item active">Cukai</li>
		</ol>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-body">
				<div class="row">
					<div class="col-md-4">
						<h4 class="card-title">Cukai</h4>
					</div>
					<div class="col-md-2 offset-md-6">
						<button type="button" class="btn btn-xs btn-success" id="tambah-cukai" data-link="{{ route('cukai.add') }}">Tambah</button>
						<button type="button" onclick="Refresh()" class="btn btn-xs btn-icons btn-secondary">Refresh</button>
					</div>
					<div class="col-md-12" align="center" id="loadingdelete">
						<h5>Data tengah diproses. Mohon tunggu sejenak!</h5>
					</div>
					<div class="table-responsive">
						<table id="table-cukai" class="display nowrap table table-hover table-striped table-bordered">
							<thead>
								<th>No.</th>
								<th>Produk</th>
								<th>Kode Sub</th>
								<th>Nama Sub</th>
								<th>Stock (QTY)</th>
								<th>Aksi</th>
							</thead>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="modal-cukai" tabindex="-1" role="dialog" aria-labelledby="title" aria-hidden="true" style="display: none;">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<div class="card">
				<div class="card-header">
					<div class="modal-header">
						<h4 class="modal-title" id="title"></h4>
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					</div>
				</div>
				<div class="card-body">
					<form id="form-cukai" method="POST">
						<div class="modal-body row">
							<div class="form-group col-md-12 fa-3x spinner_nekr" align="center">
								<i class="fas fa-spinner fa-spin"></i>
							</div>
							<div class="form-group col-md-12">
								<label class="control-label">Produk</label>
								<select id="produk" class="form-control"></select>
								<input type="hidden" id="produk_in">
							</div>
							<div class="form-group col-md-12">
								<label class="control-label">Sub Produk</label>
								<select id="sub_produk" class="form-control"></select>
								<input type="hidden" id="sub_produk_in">
							</div>
							<div class="form-group col-md-12">
								<label class="control-label">Stock (QTY)</label>
								<input type="number" class="form-control" id="stock" placeholder="Stock..">
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" onclick="save_method()" class="btn btn-primary waves-effect text-left">Save</button>
							<button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Close</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('javascript')
<script src="{{ asset('node_modules/datatables/datatables.min.js') }}"></script>
@include('home.referensi.cukai.script_cukai')
@endsection