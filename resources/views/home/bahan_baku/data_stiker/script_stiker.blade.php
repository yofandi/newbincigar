<script>
	$('#showhidd').hide();
	$('.spinner_nekr').hide();
	$('#loadingdelete').hide();
	let token = $("meta[name='csrf-token']").attr("content");
	$.extend(true, $.fn.datetimepicker.defaults, {
		icons: {
			time: 'far fa-clock',
			date: 'far fa-calendar',
			up: 'fas fa-arrow-up',
			down: 'fas fa-arrow-down',
			previous: 'fas fa-chevron-left',
			next: 'fas fa-chevron-right',
			today: 'fas fa-calendar-check',
			clear: 'far fa-trash-alt',
			close: 'far fa-times-circle'
		}
	});

	function format_number(number, prefix, thousand_separator, decimal_separator)
	{
		var 	thousand_separator = thousand_separator || ',',
		decimal_separator = decimal_separator || '.',
		regex		= new RegExp('[^' + decimal_separator + '\\d]', 'g'),
		number_string = number.toString().replace(regex, ''),
		split	  = number_string.split(decimal_separator),
		rest 	  = split[0].length % 3,
		result 	  = split[0].substr(0, rest),
		thousands = split[0].substr(rest).match(/\d{3}/g);

		if (thousands) {
			separator = rest ? thousand_separator : '';
			result += separator + thousands.join(thousand_separator);
		}
		result = split[1] != undefined ? result + decimal_separator + split[1] : result;
		return prefix == undefined ? result : (result ? prefix + result : '');
	};

	const produk = {!! json_encode($produk) !!}
	produk.unshift({id_produk: '',produk: ''});

	var table = $('#table_stiker').DataTable({
		"lengthMenu": [
		[3,5,10, 25, 50, 100, 150, 200, -1],
		[3,5,10, 25, 50, 100, 150, 200, "All"]
		],
		"order": [
		[0, 'desc']
		],
		processing: true,
		serverSide: true,
		ajax: '/data_stiker/json',
		columns: [
		{ data: 'DT_RowIndex', name: 'DT_RowIndex' },
		{ data: 'tanggal_hisstik', name: 'tanggal_hisstik' },
		{ data: 'produk', name: 'produk' },
		{ data: 'kodeproduk', name: 'kodeproduk' },
		{ data: 'masuk_luar', name: 'masuk_luar' },
		{ data: 'pakai_luar', name: 'pakai_luar' },
		{ data: 'hasil_luar', name: 'hasil_luar' },
		{ data: 'masuk_dalam', name: 'masuk_dalam' },
		{ data: 'pakai_dalam', name: 'pakai_dalam' },
		{ data: 'hasil_dalam', name: 'hasil_dalam' },
		{ data: 'masuk_tot', name: 'masuk_tot' },
		{ data: 'pakai_tot', name: 'pakai_tot' },
		{ data: 'hasil_tot', name: 'hasil_tot' },
		{ data: 'ket_sti', name: 'ket_sti' },
		{
			data: null,
			"bSortable": false,
			mRender: function (o) { 
				return '<button type="button" class="btn btn-info btn-rounded btn-xs" id="update-stiker" data-title="Update Stock Stiker | ID: '+ o.id +'" data-link="/data_stiker/upp/'+ o.id +'/'+ o.masuk_luar +'/'+ o.masuk_dalam +'/'+ o.pakai_luar +'/'+ o.pakai_dalam +'" data-id="'+ o.id +'" data-tanggal="'+ o.tanggal_hisstik +'" data-produk="'+ o.stock_stiker_produk_id_produk +'" data-subproduk="'+ o.stock_stiker_sub_produk_id +'" data-hargaid="'+ o.harga_stiker_id +'"  data-masuk_luar="'+ o.masuk_luar +'" data-pakai_luar="'+ o.pakai_luar +'" data-masuk_dalam="'+ o.masuk_dalam +'" data-pakai_dalam="'+ o.pakai_dalam +'" data-ket_sti="'+ o.ket_sti +'" data-awlstokl="'+ o.awlstockluar +'" data-awlstokd="'+ o.awlstockdalam +'"><i class="mdi mdi-dots-horizontal"></i></button><button type="button" class="btn btn-danger btn-rounded btn-xs" id="delete-stiker" data-link="/data_stiker/del/'+ o.id +'/'+ o.masuk_luar +'/'+ o.masuk_dalam +'/'+ o.pakai_luar +'/'+ o.pakai_dalam +'/'+ o.awlstockluar +'/'+ o.awlstockdalam +'" data-id="'+ o.id +'" data-produk="'+ o.stock_stiker_produk_id_produk +'" data-subproduk="'+ o.stock_stiker_sub_produk_id +'"><i class="fa fa-trash"></i></button>'; }
			},
			],
			"displayLength": 10,
		});
	$('#table_stiker tbody').on('click', 'tr.group', function() {
		var currentOrder = table.order()[0];
		if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
			table.order([2, 'desc']).draw();
		} else {
			table.order([2, 'asc']).draw();
		}
	});
	$('#table_stiker tbody').on('click', 'tr.group', function() {
		var currentOrder = table.order()[0];
		if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
			table.order([2, 'desc']).draw();
		} else {
			table.order([2, 'asc']).draw();
		}
	});

	function getstock(position1,position2,based1,based2,val = 0,id = '') {
		$.ajax({
			url: '/data_stiker/getstock',
			type: 'POST',
			dataType: 'json',
			data: {
				"_token": token,
				id_produk: based1,
				subproduk: based2,
				val: val,
				id: id,
			},
		})
		.done(function(data) {
			$('#harga_luar').val(format_number(data.hargaluar, ''));
			$('#harga_dalam').val(format_number(data.hargadalam, ''));
			if (position1 == '') {} else {$('#' + position1).val(data.luar);}
			if (position2 == '') {} else {$('#' + position2).val(data.dalam);}
		});
	}

	function lproduk(position,like = "") {
		let opt = '';
		for (var i =  0; i < produk.length; i++) {
			opt += '<option value="'+ produk[i].id_produk +'"';
			if (produk[i].id_produk == like) { opt += 'selected'; }
			opt +='>'+ produk[i].produk +'</option>';
		}
		$('#' + position).html(opt);
	}

	function lsubproduk(position,based,like = "") {
		$.ajax({
			url: '/data_cukai/getsubproduk',
			type: 'POST',
			dataType: 'json',
			data: {
				"_token": token,
				produk: based
			},
		})
		.done(function(data) {
			let opt = '<option value=""></option>';
			for (var i = 0; i < data.length; i++) {
				opt += '<option value="'+ data[i].id_sub_produk +'"';
				if (data[i].id_sub_produk == like) { opt += 'selected'; }
				opt += '>'+ data[i].sub_kode +' - '+ data[i].sub_produk +'</option>';
			}
			$('#' + position).html(opt);
		});
	}

	function searchdatastiker() {
		getstock('stockluar','stockdalam',$('#produk_in').val(),$('#subproduk_in').val());
	}

	function refresh() {
		$('#table_stiker').DataTable().ajax.reload();
	}

	function save_method() {
		let link = $('#form-stiker').attr('action');
		let tanggal_hisstik = $('#tanggal_hisstik').val();
		let produk_in = $('#produk_in').val();
		let subproduk_in = $('#subproduk_in').val();

		let stockluar = $('#stockluar').val();
		let stockdalam = $('#stockdalam').val();

		let masukluar = $('#masukluar').val();
		let masukdalam = $('#masukdalam').val();

		let terpakailuar = $('#terpakailuar').val();
		let terpakaidalam = $('#terpakaidalam').val();

		let ket_sti = $('#ket_sti').val();
		$.ajax({
			url: link,
			type: 'POST',
			dataType: 'json',
			data: {
				"_token": token,
				tanggal_hisstik: tanggal_hisstik,
				produk: produk_in,
				subproduk: subproduk_in,
				stockluar: stockluar,
				stockdalam: stockdalam,
				masukluar: masukluar,
				masukdalam: masukdalam,
				terpakailuar: terpakailuar,
				terpakaidalam: terpakaidalam,
				ket_sti: ket_sti,
			},
			beforeSend: function() {
				isProcessing = true;
				$('.spinner_nekr').fadeIn();
			},
			success: function(data) {
				$('.spinner_nekr').hide();
				isProcessing = false;
				alert(data);
				$('#table_stiker').DataTable().ajax.reload();
				$('#modal-stiker').modal('toggle');
			}
		});
	}

	$(document).on('click', '#tambah-stiker', function(event) {
		let clik = $(this);
		$('#showhidd').fadeIn();
		$('.modal-header #title').text('Tambah Stock Stiker');
		$('#form-stiker').attr('action', clik.data('link'));
		$('#form-stiker').find('input').val('');
		$('#form-stiker').find('select').val('');
		$('#form-stiker').find('textarea').val('');

		$('#produk').removeAttr('disabled');
		$('#subproduk').removeAttr('disabled');
		$("#tanggal_hisstik").datetimepicker({
			format: 'YYYY-MM-DD HH:mm:ss',
		}).data("DateTimePicker").date(new Date());

		lproduk('produk');
		$('#produk').change(function(event) {
			let bal = $(this).val();
			$('#produk_in').val(bal);
			lsubproduk('subproduk',bal);
			$('#subproduk').change(function() {
				let hebjf = $(this).val();
				$('#subproduk_in').val(hebjf);
			})	
		});		

		$('#modal-stiker').modal('show');
	});

	$(document).on('click', '#update-stiker', function(event) {
		let clik = $(this);
		$('#showhidd').hide();
		$('.modal-header #title').text(clik.data('title'));
		$('#form-stiker').attr('action', clik.data('link'));
		$('#form-stiker').find('input').val('');
		$('#form-stiker').find('select').val('');
		$('#form-stiker').find('textarea').val('');

		lproduk('produk',clik.data('produk'));
		lsubproduk('subproduk',clik.data('produk'),clik.data('subproduk'));

		$('#produk').attr('disabled','disabled');
		$('#subproduk').attr('disabled','disabled');

		$('#produk_in').val(clik.data('produk'));
		$('#subproduk_in').val(clik.data('subproduk'));

		getstock('','',clik.data('produk'),clik.data('subproduk'),1,clik.data('hargaid'));

		$('#stockluar').val(clik.data('awlstokl'));
		$('#stockdalam').val(clik.data('awlstokd'));

		$('#masukluar').val(clik.data('masuk_luar'));
		$('#masukdalam').val(clik.data('masuk_dalam'));

		$('#terpakailuar').val(clik.data('pakai_luar'));
		$('#terpakaidalam').val(clik.data('pakai_dalam'));

		$('#ket_sti').val(clik.data('ket_sti'));

		$("#tanggal_hisstik").datetimepicker({
			format: 'YYYY-MM-DD HH:mm:ss',
		}).data("DateTimePicker").date(clik.data('tanggal'));

		$('#modal-stiker').modal('show');
	});

	$(document).on('click', '#delete-stiker', function(event) {
		let jes = confirm('Apakah anda yakin ingin menghapus data ini ?');
		if (jes) {
			let id = $(this).data("id");
			let link = $(this).data("link");
			let produk_in = $(this).data('produk');
			let subproduk_in = $(this).data('subproduk');
			$.ajax(
			{
				url: link,
				type: 'DELETE',
				data: {
					"_token": token,
					produk: produk_in,
					subproduk: subproduk_in,
				},
				beforeSend: function() {
					isProcessing = true;
					$('#loadingdelete').fadeIn();
				},
				success: function (data){
					$('#loadingdelete').hide();
					isProcessing = false;
					alert(data);
					$('#table_stiker').DataTable().ajax.reload();
				}
			});
		}
	});
</script>