<div class="modal fade" id="modal-wrapping" tabindex="-1" role="dialog" aria-labelledby="title-tembakau" aria-hidden="true" style="display: none;">
	<div class="modal-dialog modal-xl">
		<div class="modal-content">
			<div class="card">
				<div class="card-header">
					<div class="modal-header">
						<h4 class="modal-title" id="title"></h4>
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					</div>
				</div>
				<div class="card-body">
					<form id="form-wrapping" method="POST">
						<div class="modal-body row">
							<div class="form-group col-md-12 fa-3x spinner_nekrwrap" align="center">
								<i class="fas fa-spinner fa-spin"></i>
							</div>
							<div class="form-group col-md-12">
								<label class="control-label">Tanggal</label>
								<input type="text" class="form-control date_sam" id="tanggal_wrapping">
							</div>
							<div class="form-group col-md-6">
								<label class="control-label">Jenis</label>
								<select class="form-control" id="jeniswrap"></select>
								<input type="hidden" id="jenis_inwrap">
							</div>
							<div class="form-group col-md-6">
								<label class="control-label">Produk</label>
								<select class="form-control" id="produkwrap"></select>
								<input type="hidden" id="produk_inwrap">
							</div>
							<div class="form-group col-md-12" id="stock_showitwrap">
								<button type="button" onclick="seacrh_datawrap()" class="btn btn-primary btn-xs btn-block">Search</button>
								<input type="hidden" id="idprobakuwrap">
								<input type="hidden" id="idpressingwrap">
								<input type="hidden" id="idwrapping">
								<input type="hidden" id="idjnprowrap">
							</div>
							<div class="form-group col-md-4">
								<label class="control-label">Stock Produksi (DEKLAD: Gram)</label>
								<input type="number" class="form-control" step="any" id="stockprowrap" value="0" placeholder="Stock (Saat ini/Awal)" readonly>
							</div>
							<div class="form-group col-md-3">
								<label class="control-label">Stock Weis (*Gram)</label>
								<input type="number" class="form-control" step="any" id="stockweisw" value="0" placeholder="Stock Weis" readonly>
							</div>
							<div class="form-group col-md-3">
								<label class="control-label">Batang Pressing</label>
								<input type="number" class="form-control" id="batangpres" placeholder="Jml Batang Pressing" readonly>
							</div>
							<div class="form-group col-md-2">
								<label class="control-label">Berat</label>
								<input type="number" class="form-control" id="berat_batang" placeholder="Berat Batang" readonly>
							</div>
							<div class="form-group col-md-12">
								<label class="control-label">Terpakai Produksi (*Gram)</label>
								<input type="number" class="form-control" step="any" id="terpakaiprowrap" placeholder="Stock Produksi Terpakai">
							</div>
							<div class="form-group col-md-6">
								<label class="control-label">Masuk Weis (*Gram)</label>
								<input type="number" class="form-control" step="any" id="masukweisw" placeholder="Stock Masuk Weisf">
							</div>
							<div class="form-group col-md-6">
								<label class="control-label">Terpakai Weis</label>
								<input type="number" class="form-control" step="any" id="terpakaiweisw" placeholder="Stock Produksi Weisf">
							</div>
							<div class="form-group col-md-12">
								<label class="control-label">Batang Cerutu Yang Dihasilkan</label>
								<input type="number" class="form-control" id="batangjmlwrap" placeholder="Jml Batang">
							</div>
							<div class="form-group col-md-12">
								<label class="control-label">Batang Reject</label>
								<input type="number" class="form-control" id="batangreject" placeholder="Jml Batang">
							</div>
							<div class="form-group col-md-6">
								<label class="control-label">Reject Filling (*Gram)</label>
								<input type="number" class="form-control" step="any" id="reject_filling" placeholder="Reject Filling" readonly>
								<small>*Reject filling otomatis dikali 2</small>
							</div>
							<div class="form-group col-md-6">
								<label class="control-label">Reject Binding (*Gram)</label>
								<input type="number" class="form-control" step="any" id="reject_binding" placeholder="Reject Binding" readonly>
							</div>
							<div class="form-group col-md-12">
								<label class="control-label">Keterangan</label>
								<textarea class="form-control" id="ket" placeholder="Keterangan"></textarea>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" onclick="save_methodwrap()" class="btn btn-primary waves-effect text-left">Save</button>
							<button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Close</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>