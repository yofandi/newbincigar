<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class History_cincin extends Model
{
	protected $table = 'history_cincin';

	protected $fillable = [
		'masuk',
		'terpakai',
		'afkir',
		'sisa',
		'tanggal_cincin',
		'ket_cin',
		'stock_cincin_id_stock_cincin',
		'stock_cincin_produk_id_produk',
		'harga_cincin_id',
    	'packing_id',
		'author_session',
		'created_at',
		'log'
	];

	public function historycukai()
	{
        return $this->hasOne('App\History_cukai','id');
	}

	public function historystiker()
	{
        return $this->hasOne('App\History_stiker','id');
	}

	public function historykemasan()
	{
        return $this->hasOne('App\History_kemasan','id');
	}
}
