<script>
	$.extend(true, $.fn.datetimepicker.defaults, {
		icons: {
			time: 'far fa-clock',
			date: 'far fa-calendar',
			up: 'fas fa-arrow-up',
			down: 'fas fa-arrow-down',
			previous: 'fas fa-chevron-left',
			next: 'fas fa-chevron-right',
			today: 'fas fa-calendar-check',
			clear: 'far fa-trash-alt',
			close: 'far fa-times-circle'
		}
	});

	$('#loadingupdate_rfs').hide();

	let token = $("meta[name='csrf-token']").attr("content");

	const produk = {!! json_encode($produk) !!}
	produk.unshift({id_produk: '',produk: ''});

	const level = {!! json_encode($level) !!}
	level.unshift({id: '',level: ''});

	var levelstack = {!! $isi->level_user_id_level !!};
	var user = {!! $isi->users_id !!};
	let idorder = {{ $isi->idorder }};

	$("#tanggal_rfs").datetimepicker({
		format: 'YYYY-MM-DD HH:mm:ss',
	});

	loaduser('userbagan',levelstack,user);

	getsession('table-item');

	function getsession(posisi) {
		$.ajax({
			url: '/api/rfs/dataupdate/keranjangitems/' + idorder,
			type: 'GET',
			dataType: 'json',
		})
		.done(function(datas) {
			$no = 1;
			$html = '';
			if (datas.length > 0) {
				for (var i = 0; i < datas.length; i++) {
					
					$html += '<tr>';
					$html += '<td>'+$no+'</td>';
					$html += '<td>'+datas[i].sub_kode+' - '+datas[i].sub_produk+'</td>';
					$html += '<td class="table_datacartupdate" data-row_id="'+datas[i].id+'" data-column_name="jml_item" contenteditable>'+datas[i].jml_item+'</td>';
					$html += '<td>';
					$html += '<button type="button" class="btn btn-danger btn-sm" id="deletecart" data-link="/api/rfs/data/delkeranjangitems/'+ datas[i].id +'"><i class="mdi mdi-delete-empty"></i></button>';
					$html += '</td>';
					$html += '</tr>';
					$no++;
				}
			} else {
				$html += '<tr>';
				$html += '<td colspan="4">No data in table</td>';
				$html += '</tr>';
			}
			$('#' + posisi).html($html);
		});
	}

	$(document).on('change', '#level', function(event) {
		loaduser('userbagan',$(this).val());
	});

	lproduk('produk_in');
	function lproduk(position,like) {
		let opt = '';
		for (let i = 0; i < produk.length; i++) {
			opt += '<option value="'+ produk[i].id_produk +'"';
			if (produk[i].id_produk == like) {
				opt += 'selected';
			}
			opt += '>'+ produk[i].produk +'</option>';
		}
		$('#' + position).html(opt);
	}

	$(document).on('change', '#produk_in', function(event) {
		let jne = $(this).val();
		lsubproduk('subproduk_in',jne);		
	});

	function lsubproduk(position,based,liker = '') {
		$.ajax({
			url: '/api/Report/getsubproduk/json',
			type: 'POST',
			dataType: 'json',
			data: {
				"_token": token,
				produk: based
			},
		})
		.done(function(data) {
			let opt = '<option value=""></option>';
			for(let i = 0; i < data.length; i++) {
				opt += '<option value="'+ data[i].id_sub_produk +'" data-name="'+ data[i].sub_kode +' - '+ data[i].sub_produk +'" ';
				if (data[i].id_sub_produk == liker) {opt += 'selected ';}
				opt += '>'+ data[i].sub_kode +' - '+ data[i].sub_produk +'</option>';
			}
			$('#' + position).html(opt);
		});
	}

	function loaduser(position,base,active = '') {
		$.ajax({
			url: '/api/rfs/getbaganuser',
			type: 'POST',
			dataType: 'json',
			data: {
				"_token": token,
				level: base
			},
		})
		.done(function(data) {
			let opt = '<option value=""></option>';
			for(let i = 0; i < data.length; i++) {
				opt += '<option value="'+ data[i].id +'" ';
				if (data[i].id == active) {opt += 'selected ';}
				opt += '>'+ data[i].username +'</option>';
			}
			$('#' + position).html(opt);
		});
	}

	function show() {
		let produk_in = $('#produk_in').val();
		let subproduk_in = $('#subproduk_in').val();
		$.ajax({
			url: '/api/rejectrfs/item',
			type: 'POST',
			dataType: 'json',
			data: {
				"_token": token,
				produk: produk_in,
				subproduk: subproduk_in,
			},
		})
		.done(function(data) {
			$('#stockjmlrfs').val(data.jml);
		});
	}

	$(document).on('blur', '.table_datacartupdate', function(){
		var id = $(this).data('row_id');
		var table_column = $(this).data('column_name');
		var value = $(this).text();
		$.ajax({
			url: '/api/rfs/data/uppkeranjangitems',
			method:"PUT",
			data:{
				"_token": token,
				rowId:id,
				column_name:table_column,
				jml:value
			},
			beforeSend: function() {
				isProcessing = true;
				$('#loadingupdate_rfs').fadeIn();
			},
			success: function (data){
				$('#loadingupdate_rfs').hide();
				isProcessing = false;
				alert(data);
				getsession('table-item');
			}
		})
	});

	$(document).on('click', '#deletecart', function(event) {
		let hks = confirm('Apakah anda yakin ingin menghapus data ini ?');
		if (hks) {
			let click = $(this);
			$.ajax({
				url: click.data('link'),
				type: 'DELETE',
				dataType: 'json',
				data: {
					"_token": token,
				},
				beforeSend: function() {
					isProcessing = true;
					$('#loadingupdate_rfs').fadeIn();
				},
				success: function (data){
					$('#loadingupdate_rfs').hide();
					isProcessing = false;
					alert(data);
					getsession('table-item');
				}
			});
		}
	});

	function save_updatekeranjang() {
		let subproduk_in = $('#subproduk_in').val();
		let jml_keluar = $('#jml_keluar').val();
		$.ajax({
			url: '/api/rfs/data/addkeranjangitems',
			type: 'POST',
			dataType: 'json',
			data: {
				"_token": token,
				subproduk: subproduk_in,
				idorder: idorder,
				jml_keluar: jml_keluar,
			},
			beforeSend: function() {
				isProcessing = true;
				$('#loadingupdate_rfs').fadeIn();
			},
			success: function(data) {
				$('#loadingupdate_rfs').hide();
				alert(data);
				isProcessing = false;
				getsession('table-item');
			}
		});
		
	}
</script>