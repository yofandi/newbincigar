@extends('../../layout')

@section('title')
<title>BIN - ERP : Update Pengiriman</title>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css" />
<style type="text/css">
</style>
@endsection

@section('content')
<div class="row page-titles">
	<div class="col-md-5 align-self-center">
		<h3 class="text-themecolor">Update Pengiriman</h3>
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a href="javascript:void(0)">Pengiriman</a></li>
			<li class="breadcrumb-item">Data Pengiriman</li>
			<li class="breadcrumb-item active">Update Pengiriman</li>
		</ol>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="row">
			<div class="col-md-12 text-center">
				<legend>Update Pengiriman: ID - {{ $isi->id }}</legend>
			</div>
			<div class="col-md-12 text-center" align="center" id="loadingupdate_rfs">
				<h5>Data tengah diproses. Mohon tunggu sejenak!</h5>
			</div>
			<div class="col-md-6">
				<form id="noc_inrfs">
					<div class="row">
						<div class="col-md-12" align="center">
							<legend>Tambah Sub Produk</legend>
						</div>
						<div class="form-group col-md-6">
							<label class="control-label">Produk</label>
							<select class="form-control" id="produk_in"></select>
						</div>
						<div class="form-group col-md-6">
							<label class="control-label">Sub Produk</label>
							<select class="form-control" id="subproduk_in"></select>
						</div>
						<div class="form-group col-md-12">
							<button type="button" onclick="show()" class="btn btn-primary btn-xs btn-block">Cari</button>
						</div>
						<div class="form-group col-md-6">
							<label class="control-label">Jumlah Stock</label>
							<input type="number" class="form-control" id="stockjmlrfs" placeholder="Jumlah Stock" readonly>
						</div>
						<div class="form-group col-md-6">
							<label class="control-label">Jumlah Keluar</label>
							<input type="number" class="form-control" id="jml_keluar" placeholder="JML Keluar">
						</div>
						<div class="form-group col-md-12">
							<button type="button" onclick="save_updatekeranjang()" class="btn btn-warning btn-sm btn-block">Tambah</button>
						</div>
					</div>
				</form>
			</div>
			<div class="col-md-6">
				<div class="table-responsive">
					<legend>Daftar Keranjang</legend>
					<table class="display nowrap table table-hover table-striped table-bordered" width="100%">
						<thead class="table-secondary">
							<tr>
								<th>No.</th>
								<th>Produk</th>
								<th>JML</th>
								<th>Aksi</th>
							</tr>
						</thead>
						<tbody id="table-item">
						</tbody>
					</table>
				</div>
			</div>
			<div class="col-md-12">
				<form id="tambah-rfs" method="POST" action="{{ route('rfs.update',$isi->id) }}" class="row">
					@csrf
					<div class="form-group col-md-12">
						<label class="control-label">Tanggal</label>
						<input type="text" class="form-control" id="tanggal_rfs" name="tanggal_rfs" placeholder="Tanggal" value="{{ $isi->tanggal_rfs }}">
					</div>
					<div class="form-group col-md-12">
						<label class="control-label">Level Bagan</label>
						<select class="form-control" name="level" id="level">
							<option value=""></option>
							@foreach ($level as $ke)
							<option value="{{ $ke->id }}"@if ($isi->level_user_id_level == $ke->id)selected @endif >{{ $ke->level }}</option>
							@endforeach
						</select>
					</div>
					<div class="form-group col-md-12">
						<label class="control-label">User Bagan</label>
						<select class="form-control" name="userbagan" id="userbagan">
						</select>
					</div>
					<div class="form-group col-md-12">
						<label class="control-label">Keterangan</label>
						<textarea class="form-control" name="ket_rfs" id="ket_rfs" placeholder="Keterangan">{{ $isi->keterangan_rfs }}</textarea>
					</div>
					<div class="form-group col-md-12" align="right">
						<button type="submit" class="btn btn-info btn-lg waves-effect">Save</button>
						{{-- <button type="button" id="back_tolist" class="btn btn-secondary btn-lg waves-effect">Back</button> --}}
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
@endsection

@section('javascript')
<script src="{{ asset('node_modules/moment/moment.js') }}">
</script><script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
<script src="{{ asset('node_modules/datatables/datatables.min.js') }}"></script>
@include('home.rfs.script_update_rfs')
@endsection