<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Noc_items extends Model
{
	use SoftDeletes;
	
    protected $table = 'noc_items';
    protected $dates = ['deleted_at'];
}
