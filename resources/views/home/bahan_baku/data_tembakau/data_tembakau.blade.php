@extends('../../layout')

@section('title')
<title>BIN - ERP : Data Tembakau</title>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css" />
@endsection

@section('content')
<div class="row page-titles">
	<div class="col-md-5 align-self-center">
		<h3 class="text-themecolor">Tembakau</h3>
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a href="javascript:void(0)">Bahan baku</a></li>
			<li class="breadcrumb-item active">Tembakau</li>
		</ol>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="row">
			<div class="col-md-6">
				<legend>Stock Bahan (Tembakau)</legend>
			</div>
			<div class="col-md-2 offset-md-4">
				<button type="button" data-link="{{ route('data.post') }}" class="btn btn-success btn-rounded btn-icons" id="tambah-tembakau"><i class="fa fa-plus"></i></button>
				<button type="button" onclick="Refresh()" class="btn btn-rounded btn-icons btn-secondary"><i class="mdi mdi-refresh"></i></button>
			</div>
		</div>
		<div class="col-md-12" align="center" id="loadingdelete">
			<h5>Data tengah diproses. Mohon tunggu sejenak!</h5>
		</div>
		<div class="table-responsive">
			<table id="table_tembakau" class="display nowrap table table-hover table-striped table-bordered">
				<thead>
					<tr>
						<th>No.</th>
						<th>Tanggal</th>
						{{-- <th>Asal</th> --}}
						<th>Jenis</th>
						<th>Kategori</th>
						<th>Masuk</th>
						<th>Diterima</th>
						<th>Diproduksi</th>
						<th>Stok Sisa</th>
						<th>Ket</th>
						<th>Aksi</th>
					</tr>
				</thead>
			</table>
		</div>
	</div>
</div>

<div class="modal fade" id="modal-tembakau" tabindex="-1" role="dialog" aria-labelledby="title-tembakau" aria-hidden="true" style="display: none;">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="card">
				<div class="card-header">
					<div class="modal-header">
						<h4 class="modal-title" id="title"></h4>
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					</div>
				</div>
				<div class="card-body">
					<form id="form-tembakau" method="POST">
						<div class="modal-body row">
							<div class="form-group col-md-12 fa-3x spinner_nekr" align="center">
								<i class="fas fa-spinner fa-spin"></i>
							</div>
							<div class="form-group col-md-12">
								<label class="control-label">Tanggal</label>
								<input type="text" class="form-control date_sam" id="tanggal_in">
							</div>
							<div class="form-group col-md-5">
								<label class="control-label">Jenis</label>
								<select class="form-control" id="jenis"></select>
								<input type="hidden" id="jenis_in">
							</div>
							<div class="form-group col-md-4">
								<label class="control-label">Kategori</label>
								<select class="form-control" id="kategori"></select>
								<input type="hidden" id="kategori_in">
							</div>
							<div class="form-group col-md-3">
								<br>
								<button type="button" id="bittonval" onclick="clikthisshowmoney()" class="btn btn-secondary btn-md btn-block">Cari</button>
							</div>
							<div class="form-group col-md-12" style="display: none;">
								<label class="control-label">Asal</label>
								<select class="form-control" id="asal"></select>
								<input type="hidden" id="asal_in">
								<input type="hidden" id="awlstok">
							</div>
							<div class="form-group col-md-12">
								<label class="control-label">Harga Temabakau per <span id="name-sat"></span></label>
								<input type="text" class="form-control" step="any" id="harga-tembakauset" placeholder="Harga" readonly>
							</div>
							<div class="form-group col-md-4">
								<label class="control-label">Stock Masuk (*gram)</label>
								<input type="number" class="form-control" step="any" id="st_masuk" placeholder="Stock Masuk">
							</div>
							<div class="form-group col-md-4">
								<label class="control-label">Stock Diterima (*gram)</label>
								<input type="number" class="form-control" step="any" id="st_terima" placeholder="Stock Terima">
							</div>
							<div class="form-group col-md-4">
								<label class="control-label">Stock Diproduksi (*gram)</label>
								<input type="number" class="form-control" step="any" id="st_produksi" placeholder="Stock Produksi">
							</div>
							<div class="form-group col-md-12">
								<label class="control-label">Keterangan</label>
								<textarea class="form-control" id="keterangan" placeholder="Keterangan"></textarea>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" onclick="save_method()" class="btn btn-primary waves-effect text-left">Save</button>
							<button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Close</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('javascript')
<script src="{{ asset('node_modules/moment/moment.js') }}">
</script><script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
<script src="{{ asset('node_modules/datatables/datatables.min.js') }}"></script>
@include('home.bahan_baku.data_tembakau.script_datatembakau')
@endsection