<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Builder;
use DataTables;
use Auth;
use App\User;
use App\History_bahanmasuk;

class Report extends Controller
{
	public function __construct()
	{
		$this->middleware('auth');
	}

	public function getsubproduk(Request $request)
	{
		$query = DB::table('sub_produk')
		->select('sub_produk.id_sub_produk','sub_produk.sub_kode','sub_produk.sub_produk','sub_produk.kemasan_id_kemasan')
		->join('produk','sub_produk.produk_id_produk','=','produk.id_produk')
		->where([['sub_produk.produk_id_produk','=',$request->produk]])
		->get();
		return $query->toJson();
	}

	public function report_bahan()
	{
		$data['berat'] = [1 => 'Kilogram',2 => 'Gram'];
		$data['bahan'] = [5 => 'Pilih',0 => 'Tembakau',1 => 'Cincin',2 => 'Stiker',3 => 'Cukai',4 => 'Packaging'];
		$data['produk'] = DB::table('produk')->get();
		$data['kemasan'] = DB::table('kemasan')->get();
		return view('home.laporan.bahan.laporan_bahan',$data);
	}

	public function dataobjectcincin($jenis,$query)
	{
		$data['title'] = 'CINCIN';
		$data['tanggal'] = [];
		$data['uraian'] = [];
		$data['harga'] = [];
		$data['awal'] =  [];
		$data['masuk'] = [];
		$data['keluar'] = [];
		$data['afkir'] = [];
		$data['Pcs'] = [];
		$data['totalrupiah'] = [];
		$tot1 = 0;
		$tot2 = 0;
		$tot3 = 0;
		$tot4 = 0;
		$tot5 = 0;
		$tot6 = 0;
		foreach ($query as $key) {
			if ($jenis == 1) {
				$awal = $key->csisa - $key->cmasuk + ($key->cpakai + $key->cafk);
				$pcs = $key->csisa;
				$data['tanggal'][] = $key->tanggal_cincin;
			} else {
				$awal = $key->awal;
				$pcs = $awal + $key->cmasuk - ($key->cpakai + $key->cafk);
			}
			$totrup = $pcs * $key->harga;

			$data['uraian'][] = $key->kode_produk;
			$data['harga'][] = number_format($key->harga, 2, ',','.');
			$data['awal'][] = $awal;
			$data['masuk'][] = $key->cmasuk;
			$data['keluar'][] = $key->cpakai;
			$data['afkir'][] = $key->cafk;
			$data['Pcs'][] = $pcs;
			$data['totalrupiah'][] = number_format($totrup, 2, ',','.');

			$tot1 += $awal;
			$tot2 += $key->cmasuk;
			$tot3 += $key->cpakai;
			$tot4 += $key->cafk;
			$tot5 += $pcs;
			$tot6 += $totrup;
		}
		$data['tot1'] = $tot1;
		$data['tot2'] = $tot2;
		$data['tot3'] = $tot3;
		$data['tot4'] = $tot4;
		$data['tot5'] = $tot5;
		$data['tot6'] = number_format($tot6, 2,',','.');

		return $data;
	}

	public function getlaporancincin(Request $request)
	{
		if ($request->jnslap == 1) {
			$select = DB::raw('
				produk.kode_produk,
				produk.produk,
				harga_cincin.harga,
				history_cincin.tanggal_cincin,
				history_cincin.masuk AS cmasuk, 
				history_cincin.terpakai AS cpakai, 
				history_cincin.afkir AS cafk, 
				history_cincin.sisa AS csisa');
		} else {
			$select = DB::raw('
				produk.kode_produk,
				produk.produk,
				harga_cincin.harga,
				IFNULL((history_cincin.sisa - history_cincin.masuk + history_cincin.terpakai + history_cincin.afkir),0) AS awal,
				IFNULL(SUM(history_cincin.masuk), 0) AS cmasuk, 
				IFNULL(SUM(history_cincin.terpakai), 0) AS cpakai, 
				IFNULL(SUM(history_cincin.afkir), 0) AS cafk, 
				IFNULL(SUM(history_cincin.sisa), 0) AS csisa');
		}
		$query = DB::table('produk')
		->select($select)
		->join('history_cincin','produk.id_produk','=','history_cincin.stock_cincin_produk_id_produk')
		->join('harga_cincin','history_cincin.harga_cincin_id','=','harga_cincin.id')
		->whereIn('history_cincin.stock_cincin_produk_id_produk',$request->produk)
		// ->where('history_cincin.stock_cincin_produk_id_produk','like',.'%')
		->whereBetween(DB::raw("(DATE_FORMAT(history_cincin.tanggal_cincin,'%Y-%m-%d'))"), [$request->tgl_awl,$request->tgl_akhr]);
		if ($request->jnslap != 1) {
			$query->groupBy('produk.id_produk');
		} else {
			$query->orderBy('history_cincin.id','ASC');
		}
		$kernel = $query->get();

		$data = $this->dataobjectcincin($request->jnslap,$kernel);
		echo json_encode($data);
	}

	public function dataobjectkemasan($query,$jenis)
	{
		$data['tanggal'] = [];
		$data['uraian'] = [];
		$data['harga'] = [];
		$data['awal'] =  [];
		$data['masuk'] = [];
		$data['keluar'] = [];
		$data['afkir'] = [];
		$data['Pcs'] = [];
		$data['totalrupiah'] = [];
		$tot1 = 0;
		$tot2 = 0;
		$tot3 = 0;
		$tot4 = 0;
		$tot5 = 0;
		$tot6 = 0;
		foreach ($query as $key) {
			if ($jenis == 1) {
				$awal = $key->stok_now - $key->masuk_kemasan + ($key->terpakai_kemasan + $key->afkir_kemasan);
				$pcs = $key->stok_now;
				$data['tanggal'][] = $key->tanggal_hiskem;
				$merk = $key->kode_produk.' | '.$key->sub_kode.' - '.$key->sub_produk.' | '.$key->nama_kemasan;
			} elseif ($jenis == 2) {
				$awal = $key->awal;
				$pcs = $awal + $key->masuk_kemasan - ($key->terpakai_kemasan + $key->afkir_kemasan);
				$merk = $key->kode_produk.' | '.$key->nama_kemasan;
			} else {
				$awal = $key->awal;
				$pcs = $awal + $key->masuk_kemasan - ($key->terpakai_kemasan + $key->afkir_kemasan);
				$merk = $key->kode_produk.' | '.$key->sub_kode.' - '.$key->sub_produk.' | '.$key->nama_kemasan;
			}

			$totrup = $pcs * $key->harga;

			$data['uraian'][] = $merk;
			$data['harga'][] = number_format($key->harga,2,',','.');
			$data['awal'][] = $awal;
			$data['masuk'][] = $key->masuk_kemasan;
			$data['keluar'][] = $key->terpakai_kemasan;
			$data['afkir'][] = $key->afkir_kemasan;
			$data['Pcs'][] = $pcs;
			$data['totalrupiah'][] = number_format($totrup,2,',','.');
			$tot1 += $awal;
			$tot2 += $key->masuk_kemasan;
			$tot3 += $key->terpakai_kemasan;
			$tot4 += $key->afkir_kemasan;
			$tot5 += $pcs;
			$tot6 += $totrup;
		}
		$data['tot1'] = $tot1;
		$data['tot2'] = $tot2;
		$data['tot3'] = $tot3;
		$data['tot4'] = $tot4;
		$data['tot5'] = $tot5;
		$data['tot6'] = number_format($tot6, 2,',','.');
		return $data;
	}

	public function getlaporankemasan(Request $request)
	{
		if ($request->jnslap == 1) {
			$select = DB::raw('
				history_kemasan.tanggal_hiskem,
				history_kemasan.masuk_kemasan,
				history_kemasan.masuk_kemasan,
				history_kemasan.terpakai_kemasan,
				history_kemasan.afkir_kemasan,
				history_kemasan.stok_now,
				harga_kemasan.harga,
				kemasan.nama_kemasan,
				sub_produk.sub_kode,
				sub_produk.sub_produk,
				produk.kode_produk,
				produk.produk');
		} else {
			$select = DB::raw('
				IFNULL((history_kemasan.stok_now - history_kemasan.masuk_kemasan + history_kemasan.terpakai_kemasan + history_kemasan.afkir_kemasan),0) AS awal,
				IFNULL(SUM(history_kemasan.masuk_kemasan), 0) AS masuk_kemasan,
				IFNULL(SUM(history_kemasan.terpakai_kemasan), 0) AS terpakai_kemasan,
				IFNULL(SUM(history_kemasan.afkir_kemasan), 0) AS afkir_kemasan,
				IFNULL(SUM(history_kemasan.stok_now), 0) AS stok_now,

				harga_kemasan.harga,
				kemasan.nama_kemasan,
				sub_produk.sub_kode,
				sub_produk.sub_produk,
				produk.kode_produk,
				produk.produk');
		}
		$query = DB::table('history_kemasan')
		->select($select)
		->join('kemasan','history_kemasan.stock_kemasan_kemasan_id_kemasan','=','kemasan.id_kemasan')
		->join('sub_produk','history_kemasan.stock_kemasan_sub_produk_id','=','sub_produk.id_sub_produk')
		->join('produk','sub_produk.produk_id_produk','=','produk.id_produk')
		->join('harga_kemasan','history_kemasan.harga_kemasan_id','=','harga_kemasan.id')
		->where([
			['stock_kemasan_kemasan_id_kemasan','like',$request->kemasan.'%']
		])
		->whereIn('history_kemasan.stock_kemasan_produk_id_produk',$request->produk)
		->whereBetween(DB::raw("(DATE_FORMAT(history_kemasan.tanggal_hiskem,'%Y-%m-%d'))"), [$request->tgl_awl,$request->tgl_akhr]);
		if ($request->jnslap == 1) {
			$query->groupBy('history_kemasan.id')->orderBy('history_kemasan.tanggal_hiskem','ASC');
		} elseif ($request->jnslap == 2) {
			$query->groupBy('produk.id_produk');
		} else {
			$query->groupBy('sub_produk.id_sub_produk');
		}
		$kernel = $query->get();

		$aken = DB::table('kemasan')->select('nama_kemasan')->where('id_kemasan',$request->kemasan);
		if ($aken->count() > 0) {
			$jebr = $aken->first();
			$data['title'] = $jebr->nama_kemasan;
		} else {
			$data['title'] = 'Packaging';
		}
		$data += $this->dataobjectkemasan($kernel,$request->jnslap);
		return json_encode($data);
	}

	public function dataobjectcukai($query,$jenis)
	{
		$data['title'] = 'CUKAI';
		$data['tanggal'] = [];
		$data['uraian'] = [];
		$data['harga'] = [];
		$data['awal'] =  [];
		$data['masuk'] = [];
		$data['keluar'] = [];
		$data['Pcs'] = [];
		$data['totprice'] = [];
		$tot1 = 0;
		$tot2 = 0;
		$tot3 = 0;
		$tot4 = 0;
		$tot5 = 0;
		foreach ($query as $key) {
			$ert = $key->terpakai_cukailama + $key->terpakai_cukaibaru;
			if ($jenis == 1) {
				$awal = $key->sisa_cukai - $key->masuk_cukai + ($key->terpakai_cukailama + $key->terpakai_cukaibaru);
				$data['tanggal'][] = $key->tanggal_cukai;
				$merk = $key->kode_produk.' | '.$key->sub_kode.' - '.$key->sub_produk;
			} elseif ($jenis == 2) {
				$awal = $key->awal;
				$merk = $key->produk;
			} else {
				$awal = $key->awal;
				$merk = $key->kode_produk.' | '.$key->sub_kode.' - '.$key->sub_produk;
			}
			$data['uraian'][] = $merk;
			$data['harga'][] = number_format($key->harga,2,',','.');
			$data['awal'][] = $awal;
			$data['masuk'][] = $key->masuk_cukai;
			$data['keluar'][] = $ert;
			$data['Pcs'][] = $key->sisa_cukai;
			$frty = $key->sisa_cukai * $key->harga;
			$data['totprice'][] = number_format($frty,2,',','.');
			$tot1 += $awal;
			$tot2 += $key->masuk_cukai;
			$tot3 += $ert;
			$tot4 += $key->sisa_cukai;
			$tot5 += $frty;
		}
		$data['tot1'] = $tot1;
		$data['tot2'] = $tot2;
		$data['tot3'] = $tot3;
		$data['tot4'] = $tot4;
		$data['tot5'] = number_format($tot5,2,',','.');

		return $data;
	}

	public function getlaporancukai(Request $request)
	{
		if ($request->jnslap == 1) {
			$select = DB::raw('
				history_cukai.tanggal_cukai,
				history_cukai.masuk_cukai,
				history_cukai.terpakai_cukailama,
				history_cukai.terpakai_cukaibaru,
				history_cukai.sisa_cukai,
				harga_cukai.harga,
				sub_produk.sub_kode,
				sub_produk.sub_produk,
				produk.kode_produk,
				produk.produk');
		} elseif ($request->jnslap == 2) {
			$select = DB::raw('
				( history_cukai.sisa_cukai - history_cukai.masuk_cukai + (history_cukai.terpakai_cukailama + history_cukai.terpakai_cukaibaru) ) AS awal,

				IFNULL(SUM(history_cukai.masuk_cukai),0) AS masuk_cukai,
				IFNULL(SUM(history_cukai.terpakai_cukailama),0) AS terpakai_cukailama,
				IFNULL(SUM(history_cukai.terpakai_cukaibaru),0) AS terpakai_cukaibaru,
				IFNULL(SUM(history_cukai.sisa_cukai),0) AS sisa_cukai,
				harga_cukai.harga,
				GROUP_CONCAT(DISTINCT produk.kode_produk SEPARATOR ",") AS produk');
		} else {
			$select = DB::raw('
				( history_cukai.sisa_cukai - history_cukai.masuk_cukai + (history_cukai.terpakai_cukailama + history_cukai.terpakai_cukaibaru) ) AS awal,

				IFNULL(SUM(history_cukai.masuk_cukai),0) AS masuk_cukai,
				IFNULL(SUM(history_cukai.terpakai_cukailama),0) AS terpakai_cukailama,
				IFNULL(SUM(history_cukai.terpakai_cukaibaru),0) AS terpakai_cukaibaru,
				IFNULL(SUM(history_cukai.sisa_cukai),0) AS sisa_cukai,
				harga_cukai.harga,

				sub_produk.sub_kode,
				sub_produk.sub_produk,
				produk.kode_produk,
				produk.produk');
		}
		$query = DB::table('history_cukai')
		->select($select)
		->join('sub_produk','history_cukai.stock_cukai_id_sub_produk','=','sub_produk.id_sub_produk')
		->join('produk','sub_produk.produk_id_produk','=','produk.id_produk')
		->join('harga_cukai','history_cukai.harga_cukai_id','=','harga_cukai.id')
		->whereIn('history_cukai.stock_cukai_produk_id_produk',$request->produk)
		->whereBetween(DB::raw("(DATE_FORMAT(history_cukai.tanggal_cukai,'%Y-%m-%d'))"), [$request->tgl_awl,$request->tgl_akhr]);
		if ($request->jnslap == 1) {
			$query->groupBy('history_cukai.id')->orderBy('history_cukai.tanggal_cukai','ASC');
		} elseif ($request->jnslap == 0)  {
			$query->groupBy('sub_produk.id_sub_produk');
		} else {
			// $query->groupBy('produk.id_produk');
			$query->groupBy('sub_produk.merek_cukai_id');
		}
		$kernel = $query->get();
		$data = $this->dataobjectcukai($kernel,$request->jnslap);
		echo json_encode($data);
	}

	public function dataobjectstiker($query,$jenis)
	{
		$data = [];
		$row = [];

		$totawl_l = 0;
		$totmsk_l = 0;
		$totpka_l = 0;
		$tothsl_l = 0;

		$totawl_d = 0;
		$totmsk_d = 0;
		$totpka_d = 0;
		$tothsl_d = 0;

		$totjmlawl_j = 0;
		$totjmlmsk_j = 0;
		$totjmlpka_j = 0;
		$totjmlhsl_j = 0;

		$tothargaluar = 0;
		$tothargadalam = 0;
		$totjmlharga = 0;
		$count = 0;

		$data['isi'] = [];
		foreach ($query as $key) {
			if ($jenis == 1) {
				$awal_l = $key->hasil_luar - $key->masuk_luar + $key->pakai_luar;
				$awal_d = $key->hasil_dalam - $key->masuk_dalam + $key->pakai_dalam;
				$hasil_l = $key->hasil_luar;
				$hasil_d = $key->hasil_dalam;
				$row['tanggal'] = $key->tanggal_hisstik;
			} else {
				$awal_l = $key->awal_luar;
				$awal_d = $key->awal_dalam;
				$hasil_l = $awal_l + $key->masuk_luar - $key->pakai_luar;
				$hasil_d = $awal_d + $key->masuk_dalam - $key->pakai_dalam;
			}

			$jmlhargl = $key->harga_luar * $hasil_l;
			$jmlhargd = $key->harga_dalam * $hasil_d;
			$jmltotharg = $jmlhargl + $jmlhargd;

			$row['kode_produk'] = $key->kode_produk;
			$row['sub_kode'] = $key->sub_kode;
			$row['sub_produk'] = $key->sub_produk;
			$row['produk'] = $key->produk;

			$row['harga_luar'] = number_format($key->harga_luar, 2,',','.');
			$row['awal_luar'] = $awal_l;
			$row['masuk_luar'] = $key->masuk_luar;
			$row['pakai_luar'] = $key->pakai_luar;
			$row['hasil_luar'] = $hasil_l;
			$row['hargaluar'] = number_format($jmlhargl, 2,',','.');

			$row['harga_dalam'] = number_format($key->harga_dalam, 2,',','.');
			$row['awal_dalam'] = $awal_d;
			$row['masuk_dalam'] = $key->masuk_dalam;
			$row['pakai_dalam'] = $key->pakai_dalam;
			$row['hasil_dalam'] = $hasil_d;
			$row['hargadalam'] = number_format($jmlhargd, 2,',','.');

			$row['awal_total'] = $awal_l + $awal_d;
			$row['masuk_total'] = $key->masuk_luar + $key->masuk_dalam;
			$row['pakai_total'] = $key->pakai_luar + $key->pakai_dalam;
			$row['hasil_total'] = $hasil_l + $hasil_d;
			$row['hargatotal'] = number_format($jmltotharg, 2,',','.');

			$totawl_l += $awal_l;
			$totmsk_l += $key->masuk_luar;
			$totpka_l += $key->pakai_luar;
			$tothsl_l += $hasil_l;

			$totawl_d += $awal_d;
			$totmsk_d += $key->masuk_dalam;
			$totpka_d += $key->pakai_dalam;
			$tothsl_d += $hasil_d;

			$totjmlawl_j += $awal_l + $awal_d;
			$totjmlmsk_j += $key->masuk_luar + $key->masuk_dalam;
			$totjmlpka_j += $key->pakai_luar + $key->pakai_dalam;
			$totjmlhsl_j += $hasil_l + $hasil_d;

			$tothargaluar += $jmlhargl;
			$tothargadalam += $jmlhargd;
			$totjmlharga += $jmltotharg;

			$data['isi'][] = $row;
			$count++;
		}
		// $data['countid'] = $count;
		$data['totawl_l'] = $totawl_l;
		$data['totmsk_l'] = $totmsk_l;
		$data['totpka_l'] = $totpka_l;
		$data['tothsl_l'] = $tothsl_l;
		$data['totawl_d'] = $totawl_d;
		$data['totmsk_d'] = $totmsk_d;
		$data['totpka_d'] = $totpka_d;
		$data['tothsl_d'] = $tothsl_d;

		$data['totjmlawl_j'] = $totjmlawl_j;
		$data['totjmlmsk_j'] = $totjmlmsk_j;
		$data['totjmlpka_j'] = $totjmlpka_j;
		$data['totjmlhsl_j'] = $totjmlhsl_j;

		$data['tothargaluar'] = number_format($tothargaluar, 2,',','.');
		$data['tothargadalam'] = number_format($tothargadalam, 2,',','.');
		$data['totjmlharga'] = number_format($totjmlharga, 2,',','.');

		return $data;
	}

	public function getlaporanstiker(Request $request)
	{
		if ($request->jnslap == 1) {
			$select = DB::raw('
				history_stiker.tanggal_hisstik,
				history_stiker.masuk_luar,
				history_stiker.pakai_luar,
				history_stiker.hasil_luar,
				history_stiker.masuk_dalam,
				history_stiker.pakai_dalam,
				history_stiker.hasil_dalam,
				harga_stiker.harga_luar,
				harga_stiker.harga_dalam,
				sub_produk.sub_kode,
				sub_produk.sub_produk,
				produk.kode_produk,
				produk.produk');
		} else {
			$select = DB::raw('
				( history_stiker.hasil_luar - history_stiker.masuk_luar + history_stiker.pakai_luar ) AS awal_luar,

				IFNULL(SUM(history_stiker.masuk_luar),0) AS masuk_luar,
				IFNULL(SUM(history_stiker.pakai_luar),0) AS pakai_luar,
				IFNULL(SUM(history_stiker.hasil_luar),0) AS hasil_luar,

				( history_stiker.hasil_dalam - history_stiker.masuk_dalam + history_stiker.pakai_dalam ) AS awal_dalam,

				IFNULL(SUM(history_stiker.masuk_dalam),0) AS masuk_dalam,
				IFNULL(SUM(history_stiker.pakai_dalam),0) AS pakai_dalam,
				IFNULL(SUM(history_stiker.hasil_dalam),0) AS hasil_dalam,

				harga_stiker.harga_luar,
				harga_stiker.harga_dalam,

				sub_produk.sub_kode,
				sub_produk.sub_produk,
				produk.kode_produk,
				produk.produk');
		}

		$query = DB::table('history_stiker')
		->select($select)
		->join('sub_produk','sub_produk.id_sub_produk','=','history_stiker.stock_stiker_sub_produk_id')
		->join('produk','sub_produk.produk_id_produk','=','produk.id_produk')
		->join('harga_stiker','history_stiker.harga_stiker_id','=','harga_stiker.id')
		// ->where([
		// 	['history_stiker.stock_stiker_produk_id_produk','like',$request->produk.'%']
		// ])
		->whereIn('history_stiker.stock_stiker_produk_id_produk',$request->produk)
		->whereBetween(DB::raw("(DATE_FORMAT(history_stiker.tanggal_hisstik,'%Y-%m-%d'))"), [$request->tgl_awl,$request->tgl_akhr]);

		if ($request->jnslap == 1) {
			$query->groupBy('history_stiker.id');
		} elseif ($request->jnslap == 0)  {
			$query->groupBy('sub_produk.id_sub_produk');
		} else {
			$query->groupBy('produk.id_produk');
		}
		$kernel = $query->get();

		$data = $this->dataobjectstiker($kernel,$request->jnslap);
		return json_encode($data);
	}

	public function queryobject($request,$data)
	{
		switch ($request->satuanberat) {
			case 1:
			$data['namasatuan'] = 'KG';
			$val = 1000;
			$sat = 1;
			$bagi = 1;
			break;

			case 2:
			$data['namasatuan'] = 'GRAM';
			$val = 1;
			$sat = 2;
			$bagi = 1000;
			break;
		}

		for ($i=0 ,$k = 1; $i < 12 ; $i++, $k++) { 
			if ($k < 10) {
				$var = '0'.$k;
			} else {
				$var = $k;
			}

			$datealw = $request->tahun.'-'.$var.'-01';
			$dateawl = $datealw.' 00:00:00';
			$datelast = date('Y-m-t',strtotime($datealw)).' 23:59:59';

			$dekb[$i] = DB::table('history_bahanmasuk')
			->select(DB::raw('
				IFNULL(SUM(history_bahanmasuk.diproduksi / '.$val.'),0) AS diproduksi,
				IFNULL(SUM(history_bahanmasuk.diproduksi / '.$val.' * satuan_harga.harga_tembakau / '.$bagi.'),0) AS jmlhargadiproduksi,
				IFNULL(SUM(history_bahanmasuk.hari_ini / '.$val.'),0) AS hari_ini,
				IFNULL(SUM(history_bahanmasuk.hari_ini / '.$val.' * satuan_harga.harga_tembakau / '.$bagi.'),0) AS jmlhargahari_ini
				'))
			->join('satuan_harga','history_bahanmasuk.satuan_harga_id','=','satuan_harga.id')
			->where('history_bahanmasuk.kategori_id_kategori',4)
			->whereBetween(DB::raw('date_format(history_bahanmasuk.tanggal_in, "%Y-%m-%d")'),[$dateawl,$datelast])
			->first();

			$omb[$i] = DB::table('history_bahanmasuk')
			->select(DB::raw('
				IFNULL(SUM(history_bahanmasuk.diproduksi / '.$val.'),0) AS diproduksi,
				IFNULL(SUM(history_bahanmasuk.diproduksi / '.$val.' * satuan_harga.harga_tembakau / '.$bagi.'),0) AS jmlhargadiproduksi,
				IFNULL(SUM(history_bahanmasuk.hari_ini / '.$val.'),0) AS hari_ini,
				IFNULL(SUM(history_bahanmasuk.hari_ini / '.$val.' * satuan_harga.harga_tembakau / '.$bagi.'),0) AS jmlhargahari_ini
				'))
			->join('satuan_harga','history_bahanmasuk.satuan_harga_id','=','satuan_harga.id')
			->where('history_bahanmasuk.kategori_id_kategori',3)
			->whereBetween(DB::raw('date_format(history_bahanmasuk.tanggal_in, "%Y-%m-%d")'),[$dateawl,$datelast])
			->first();

			$fill2[$i] = DB::table('history_bahanmasuk')
			->select(DB::raw('
				IFNULL(SUM(history_bahanmasuk.diproduksi / '.$val.'),0) AS diproduksi,
				IFNULL(SUM(history_bahanmasuk.diproduksi / '.$val.' * satuan_harga.harga_tembakau / '.$bagi.'),0) AS jmlhargadiproduksi,
				IFNULL(SUM(history_bahanmasuk.hari_ini / '.$val.'),0) AS hari_ini,
				IFNULL(SUM(history_bahanmasuk.hari_ini / '.$val.' * satuan_harga.harga_tembakau / '.$bagi.'),0) AS jmlhargahari_ini
				'))
			->join('satuan_harga','history_bahanmasuk.satuan_harga_id','=','satuan_harga.id')
			->where('history_bahanmasuk.kategori_id_kategori',2)
			->whereBetween(DB::raw('date_format(history_bahanmasuk.tanggal_in, "%Y-%m-%d")'),[$dateawl,$datelast])
			->first();

			$fill1[$i] = DB::table('history_bahanmasuk')
			->select(DB::raw('
				IFNULL(SUM(history_bahanmasuk.diproduksi / '.$val.'),0) AS diproduksi,
				IFNULL(SUM(history_bahanmasuk.diproduksi / '.$val.' * satuan_harga.harga_tembakau / '.$bagi.'),0) AS jmlhargadiproduksi,
				IFNULL(SUM(history_bahanmasuk.hari_ini / '.$val.'),0) AS hari_ini,
				IFNULL(SUM(history_bahanmasuk.hari_ini / '.$val.' * satuan_harga.harga_tembakau / '.$bagi.'),0) AS jmlhargahari_ini
				'))
			->join('satuan_harga','history_bahanmasuk.satuan_harga_id','=','satuan_harga.id')
			->where('history_bahanmasuk.kategori_id_kategori',1)
			->whereBetween(DB::raw('date_format(history_bahanmasuk.tanggal_in, "%Y-%m-%d")'),[$dateawl,$datelast])
			->first();

			$totalpakai = $dekb[$i]->diproduksi + $omb[$i]->diproduksi + $fill2[$i]->diproduksi + $fill1[$i]->diproduksi;
			$totalsisa = $dekb[$i]->hari_ini + $omb[$i]->hari_ini + $fill2[$i]->hari_ini + $fill1[$i]->hari_ini;

			$totaluangpakai = $dekb[$i]->jmlhargadiproduksi + $omb[$i]->jmlhargadiproduksi + $fill2[$i]->jmlhargadiproduksi + $fill1[$i]->jmlhargadiproduksi;
			$totaluangsisa = $dekb[$i]->jmlhargahari_ini + $omb[$i]->jmlhargahari_ini + $fill2[$i]->jmlhargahari_ini + $fill1[$i]->jmlhargahari_ini;

			$data['tanggal'][] = $datelast;
			$data['dekblad']['keluar'][] = number_format((float)$dekb[$i]->diproduksi, 2, '.', '');
			$data['dekblad']['hargakeluar'][] = number_format($dekb[$i]->jmlhargadiproduksi,2,',','.');

			$data['dekblad']['sisa'][] = number_format((float)$dekb[$i]->hari_ini, 2, '.', '');
			$data['dekblad']['hargasisa'][] = number_format($dekb[$i]->jmlhargahari_ini,2,',','.');

			$data['omblad']['keluar'][] = number_format((float)$omb[$i]->diproduksi, 2, '.', '');
			$data['omblad']['hargakeluar'][] = number_format($omb[$i]->jmlhargadiproduksi,2,',','.');

			$data['omblad']['sisa'][] = number_format((float)$omb[$i]->hari_ini, 2, '.', '');
			$data['omblad']['hargasisa'][] = number_format($omb[$i]->jmlhargahari_ini,2,',','.');

			$data['filler2']['keluar'][] = number_format((float)$fill2[$i]->diproduksi, 2, '.', '');
			$data['filler2']['hargakeluar'][] = number_format($fill2[$i]->jmlhargadiproduksi,2,',','.');

			$data['filler2']['sisa'][] = number_format((float)$fill2[$i]->hari_ini, 2, '.', '');
			$data['filler2']['hargasisa'][] = number_format($fill2[$i]->jmlhargahari_ini,2,',','.');

			$data['filler1']['keluar'][] = number_format((float)$fill1[$i]->diproduksi, 2, '.', '');
			$data['filler1']['hargakeluar'][] = number_format($fill1[$i]->jmlhargadiproduksi,2,',','.');
			
			$data['filler1']['sisa'][] = number_format((float)$fill1[$i]->hari_ini, 2, '.', '');
			$data['filler1']['hargasisa'][] = number_format($fill1[$i]->jmlhargahari_ini,2,',','.');

			$data['total_pakai'][] = number_format((float)$totalpakai, 2, '.', '');
			$data['total_sisa'][] = number_format((float)$totalsisa, 2, '.', '');
			$data['total_uangpakai'][] = number_format($totaluangpakai,2,',','.');
			$data['total_uangsisa'][] = number_format($totaluangsisa,2,',','.');

			$data['rowtotdekbpakai'] += number_format((float)$dekb[$i]->diproduksi, 2, '.', '');
			$data['rowtotdekbsisa'] += number_format((float)$dekb[$i]->hari_ini, 2, '.', '');
			$data['rowtotombldpakai'] += number_format((float)$omb[$i]->diproduksi, 2, '.', '');
			$data['rowtotombldsisa'] += number_format((float)$omb[$i]->hari_ini, 2, '.', '');
			$data['rowtotfill2pakai'] += number_format((float)$fill2[$i]->diproduksi, 2, '.', '');
			$data['rowtotfill2sisa'] += number_format((float)$fill2[$i]->hari_ini, 2, '.', '');
			$data['rowtotfill1pakai'] += number_format((float)$fill1[$i]->diproduksi, 2, '.', '');
			$data['rowtotfill1sisa'] += number_format((float)$fill1[$i]->hari_ini, 2, '.', '');

			$data['uangdekbpakai'] += $dekb[$i]->jmlhargadiproduksi;
			$data['uangdekbsisa'] += $dekb[$i]->jmlhargahari_ini;
			$data['uangombpakai'] += $omb[$i]->jmlhargadiproduksi;
			$data['uangombsisa'] += $omb[$i]->jmlhargahari_ini;
			$data['uangfill2pakai'] += $fill2[$i]->jmlhargadiproduksi;
			$data['uangfill2sisa'] += $fill2[$i]->jmlhargahari_ini;
			$data['uangfill1pakai'] += $fill1[$i]->jmlhargadiproduksi;
			$data['uangfill1sisa'] += $fill1[$i]->jmlhargahari_ini;

			$data['columtotpakai'] += number_format((float)$totalpakai, 2, '.', '');
			$data['columtotsisa'] += number_format((float)$totalsisa, 2, '.', '');
			$data['bar1'] += $totaluangpakai;
			$data['bar2'] += $totaluangsisa;
		}
		$data['columtotuangpakai'] = number_format((float)$data['bar1'], 2, ',', '.');
		$data['columtotuangsisa'] = number_format((float)$data['bar2'], 2, ',', '.');

		$data['rowtotdekbpakaiuang'] = number_format((float)$data['uangdekbpakai'], 2, ',', '.');
		$data['rowtotdekbsisauang'] = number_format((float)$data['uangdekbsisa'], 2, ',', '.');
		$data['rowtotombldpakaiuang'] = number_format((float)$data['uangombpakai'], 2, ',', '.');
		$data['rowtotombldsisauang'] = number_format((float)$data['uangombsisa'], 2, ',', '.');
		$data['rowtotfill2pakaiuang'] = number_format((float)$data['uangfill2pakai'], 2, ',', '.');
		$data['rowtotfill2sisauang'] = number_format((float)$data['uangfill2sisa'], 2, ',', '.');
		$data['rowtotfill1pakaiuang'] = number_format((float)$data['uangfill1pakai'], 2, ',', '.');
		$data['rowtotfill1sisauang'] = number_format((float)$data['uangfill1sisa'], 2, ',', '.');
		$data['count'] = 12;

		return $data;
	}

	public function queryrincian($request,$data)
	{
		switch ($request->satuanberat) {
			case 1:
			$data['namasatuan'] = 'KG';
			$val = 1000;
			$sat = 1;
			$bagi = 1;
			break;

			case 2:
			$data['namasatuan'] = 'GRAM';
			$val = 1;
			$sat = 2;
			$bagi = 1000;
			break;
		}
		$query = DB::table('history_bahanmasuk')
		->select(DB::raw('
			date_format(history_bahanmasuk.tanggal_in, "%Y-%m-%d") AS tanggal_in,
			(CASE
			WHEN history_bahanmasuk.kategori_id_kategori = 1
			THEN SUM(history_bahanmasuk.diproduksi)
			ELSE 0
			END) AS diproduksifill1,
			IFNULL((CASE
			WHEN history_bahanmasuk.kategori_id_kategori = 1
			THEN SUM(history_bahanmasuk.diproduksi / '.$val.' * satuan_harga.harga_tembakau / '.$bagi.')
			ELSE 0
			END), 0) AS hargaprofill1,
			(CASE
			WHEN history_bahanmasuk.kategori_id_kategori = 1
			THEN SUM(history_bahanmasuk.hari_ini)
			ELSE 0
			END) AS hari_inifill1,
			IFNULL((CASE
			WHEN history_bahanmasuk.kategori_id_kategori = 1
			THEN SUM(history_bahanmasuk.hari_ini / '.$val.' * satuan_harga.harga_tembakau / '.$bagi.')
			ELSE 0
			END), 0) AS hargasisafill1,

			(CASE
			WHEN history_bahanmasuk.kategori_id_kategori = 2
			THEN SUM(history_bahanmasuk.diproduksi)
			ELSE 0
			END) AS diproduksifill2,
			IFNULL((CASE
			WHEN history_bahanmasuk.kategori_id_kategori = 2
			THEN SUM(history_bahanmasuk.diproduksi / '.$val.' * satuan_harga.harga_tembakau / '.$bagi.')
			ELSE 0
			END), 0) AS hargaprofill2,
			(CASE
			WHEN history_bahanmasuk.kategori_id_kategori = 2
			THEN SUM(history_bahanmasuk.hari_ini)
			ELSE 0
			END) AS hari_inifill2,
			IFNULL((CASE
			WHEN history_bahanmasuk.kategori_id_kategori = 2
			THEN SUM(history_bahanmasuk.hari_ini / '.$val.' * satuan_harga.harga_tembakau / '.$bagi.')
			ELSE 0
			END), 0) AS hargasisafill2,

			(CASE
			WHEN history_bahanmasuk.kategori_id_kategori = 3
			THEN SUM(history_bahanmasuk.diproduksi)
			ELSE 0
			END) AS diproduksiomblad,
			IFNULL((CASE
			WHEN history_bahanmasuk.kategori_id_kategori = 3
			THEN SUM(history_bahanmasuk.diproduksi / '.$val.' * satuan_harga.harga_tembakau / '.$bagi.')
			ELSE 0
			END), 0) AS hargaproomblad,
			(CASE
			WHEN history_bahanmasuk.kategori_id_kategori = 3
			THEN SUM(history_bahanmasuk.hari_ini)
			ELSE 0
			END) AS hari_iniomblad,
			IFNULL((CASE
			WHEN history_bahanmasuk.kategori_id_kategori = 3
			THEN SUM(history_bahanmasuk.hari_ini / '.$val.' * satuan_harga.harga_tembakau / '.$bagi.')
			ELSE 0
			END), 0) AS hargasisaomblad,

			(CASE
			WHEN history_bahanmasuk.kategori_id_kategori = 4
			THEN SUM(history_bahanmasuk.diproduksi)
			ELSE 0
			END) AS diproduksidekblad,
			IFNULL((CASE
			WHEN history_bahanmasuk.kategori_id_kategori = 4
			THEN SUM(history_bahanmasuk.diproduksi / '.$val.' * satuan_harga.harga_tembakau / '.$bagi.')
			ELSE 0
			END), 0) AS hargaprodekblad,
			(CASE
			WHEN history_bahanmasuk.kategori_id_kategori = 4
			THEN SUM(history_bahanmasuk.hari_ini)
			ELSE 0
			END) AS hari_inidekblad,
			IFNULL((CASE
			WHEN history_bahanmasuk.kategori_id_kategori = 4
			THEN SUM(history_bahanmasuk.hari_ini / '.$val.' * satuan_harga.harga_tembakau / '.$bagi.')
			ELSE 0
			END), 0) AS hargasisadekblad
			'))
		->join('satuan_harga','history_bahanmasuk.satuan_harga_id','=','satuan_harga.id')
		->orWhereBetween(DB::raw('date_format(history_bahanmasuk.tanggal_in, "%Y-%m-%d")') ,[$request->tgl_awl,$request->tgl_akhr])
		->groupBy(DB::raw("date_format(tanggal_in, '%Y-%m-%d')"));

		$jam = $query->get();

		for ($i=0; $i < $query->count(); $i++) {

			$totalpakai = $jam[$i]->diproduksifill1 + $jam[$i]->diproduksifill2 + $jam[$i]->diproduksiomblad + $jam[$i]->diproduksidekblad;
			$totalsisa = $jam[$i]->hari_inifill1 + $jam[$i]->hari_inifill2 + $jam[$i]->hari_iniomblad + $jam[$i]->hari_inidekblad;

			$totaluangpakai = $jam[$i]->hargaprofill1 + $jam[$i]->hargaprofill2 + $jam[$i]->hargaproomblad + $jam[$i]->hargaprodekblad;
			$totaluangsisa = $jam[$i]->hargasisafill1 + $jam[$i]->hargasisafill2 + $jam[$i]->hargasisaomblad + $jam[$i]->hargasisadekblad;

			$data['tanggal'][] = $jam[$i]->tanggal_in;
			$data['dekblad']['keluar'][] = number_format((float)$jam[$i]->diproduksidekblad, 2, '.', '');
			$data['dekblad']['hargakeluar'][] = number_format($jam[$i]->hargaprodekblad,2,',','.');

			$data['dekblad']['sisa'][] = number_format((float)$jam[$i]->hari_inidekblad, 2, '.', '');
			$data['dekblad']['hargasisa'][] = number_format($jam[$i]->hargasisadekblad,2,',','.');

			$data['omblad']['keluar'][] = number_format((float)$jam[$i]->diproduksiomblad, 2, '.', '');
			$data['omblad']['hargakeluar'][] = number_format($jam[$i]->hargaproomblad,2,',','.');

			$data['omblad']['sisa'][] = number_format((float)$jam[$i]->hari_iniomblad, 2, '.', '');
			$data['omblad']['hargasisa'][] = number_format($jam[$i]->hargasisaomblad,2,',','.');

			$data['filler2']['keluar'][] = number_format((float)$jam[$i]->diproduksifill2, 2, '.', '');
			$data['filler2']['hargakeluar'][] = number_format($jam[$i]->hargaprofill2,2,',','.');

			$data['filler2']['sisa'][] = number_format((float)$jam[$i]->hari_inifill2, 2, '.', '');
			$data['filler2']['hargasisa'][] = number_format($jam[$i]->hargasisafill2,2,',','.');

			$data['filler1']['keluar'][] = number_format((float)$jam[$i]->diproduksifill1, 2, '.', '');
			$data['filler1']['hargakeluar'][] = number_format($jam[$i]->hargaprofill1,2,',','.');
			
			$data['filler1']['sisa'][] = number_format((float)$jam[$i]->hari_inifill1, 2, '.', '');
			$data['filler1']['hargasisa'][] = number_format($jam[$i]->hargasisafill1,2,',','.');

			$data['total_pakai'][] = number_format((float)$totalpakai, 2, '.', '');
			$data['total_sisa'][] = number_format((float)$totalsisa, 2, '.', '');
			$data['total_uangpakai'][] = number_format($totaluangpakai,2,',','.');
			$data['total_uangsisa'][] = number_format($totaluangsisa,2,',','.');

			$data['rowtotdekbpakai'] += number_format((float)$jam[$i]->diproduksidekblad, 2, '.', '');
			$data['rowtotdekbsisa'] += number_format((float)$jam[$i]->hari_inidekblad, 2, '.', '');
			$data['rowtotombldpakai'] += number_format((float)$jam[$i]->diproduksiomblad, 2, '.', '');
			$data['rowtotombldsisa'] += number_format((float)$jam[$i]->hari_iniomblad, 2, '.', '');
			$data['rowtotfill2pakai'] += number_format((float)$jam[$i]->diproduksifill2, 2, '.', '');
			$data['rowtotfill2sisa'] += number_format((float)$jam[$i]->hari_inifill2, 2, '.', '');
			$data['rowtotfill1pakai'] += number_format((float)$jam[$i]->diproduksifill1, 2, '.', '');
			$data['rowtotfill1sisa'] += number_format((float)$jam[$i]->hari_inifill1, 2, '.', '');

			$data['uangdekbpakai'] += $jam[$i]->hargaprodekblad;
			$data['uangdekbsisa'] += $jam[$i]->hargasisadekblad;
			$data['uangombpakai'] += $jam[$i]->hargaproomblad;
			$data['uangombsisa'] += $jam[$i]->hargasisaomblad;
			$data['uangfill2pakai'] += $jam[$i]->hargaprofill2;
			$data['uangfill2sisa'] += $jam[$i]->hargasisafill2;
			$data['uangfill1pakai'] += $jam[$i]->hargaprofill1;
			$data['uangfill1sisa'] += $jam[$i]->hargasisafill1;

			$data['columtotpakai'] += number_format((float)$totalpakai, 2, '.', '');
			$data['columtotsisa'] += number_format((float)$totalsisa, 2, '.', '');
			$data['bar1'] += $totaluangpakai;
			$data['bar2'] += $totaluangsisa;
		}

		$data['columtotuangpakai'] = number_format($data['bar1'], 2, ',', '.');
		$data['columtotuangsisa'] = number_format($data['bar2'], 2, ',', '.');

		$data['rowtotdekbpakaiuang'] = number_format($data['uangdekbpakai'], 2, ',', '.');
		$data['rowtotdekbsisauang'] = number_format($data['uangdekbsisa'], 2, ',', '.');
		$data['rowtotombldpakaiuang'] = number_format($data['uangombpakai'], 2, ',', '.');
		$data['rowtotombldsisauang'] = number_format($data['uangombsisa'], 2, ',', '.');
		$data['rowtotfill2pakaiuang'] = number_format($data['uangfill2pakai'], 2, ',', '.');
		$data['rowtotfill2sisauang'] = number_format($data['uangfill2sisa'], 2, ',', '.');
		$data['rowtotfill1pakaiuang'] = number_format($data['uangfill1pakai'], 2, ',', '.');
		$data['rowtotfill1sisauang'] = number_format($data['uangfill1sisa'], 2, ',', '.');
		$data['count'] = $query->count();

		return $data;
	}

	public function getlaporantembakau(Request $request)
	{
		$data['tanggal'] = []; 
		$data['dekblad']['keluar'] = []; 
		$data['dekblad']['hargakeluar'] = []; 
		$data['dekblad']['sisa'] = []; 
		$data['dekblad']['hargasisa'] = []; 
		$data['omblad']['keluar'] = []; 
		$data['omblad']['hargakeluar'] = []; 
		$data['omblad']['sisa'] = []; 
		$data['omblad']['hargasisa'] = []; 
		$data['filler2']['keluar'] = []; 
		$data['filler2']['hargakeluar'] = []; 
		$data['filler2']['sisa'] = []; 
		$data['filler2']['hargasisa'] = []; 
		$data['filler1']['keluar'] = []; 
		$data['filler1']['hargakeluar'] = []; 
		$data['filler1']['sisa'] = []; 
		$data['filler1']['hargasisa'] = []; 
		$data['total_pakai'] = []; 
		$data['total_sisa'] = []; 
		$data['columtotpakai'] = 0; 
		$data['columtotsisa'] = 0;
		$data['total_uangpakai'] = [];
		$data['total_uangsisa'] = [];
		$data['bar1'] = 0; 
		$data['bar2'] = 0;
		$data['rowtotdekbpakai'] = 0;
		$data['rowtotdekbsisa'] = 0;
		$data['rowtotombldpakai'] = 0;
		$data['rowtotombldsisa'] = 0;
		$data['rowtotfill2pakai'] = 0;
		$data['rowtotfill2sisa'] = 0;
		$data['rowtotfill1pakai'] = 0;
		$data['rowtotfill1sisa'] = 0;
		$data['uangdekbpakai'] = 0;
		$data['uangdekbsisa'] = 0;
		$data['uangombpakai'] = 0;
		$data['uangombsisa'] = 0;
		$data['uangfill2pakai'] = 0;
		$data['uangfill2sisa'] = 0;
		$data['uangfill1pakai'] = 0;
		$data['uangfill1sisa'] = 0;
		$data['rowtotdekbpakaiuang'] = 0;
		$data['rowtotdekbsisauang'] = 0;
		$data['rowtotombldpakaiuang'] = 0;
		$data['rowtotombldsisauang'] = 0;
		$data['rowtotfill2pakaiuang'] = 0;
		$data['rowtotfill2sisauang'] = 0;
		$data['rowtotfill1pakaiuang'] = 0;
		$data['rowtotfill1sisauang'] = 0;

		if ($request->jnslap == 0) {
			$data['nametag'] = '';
			$data = $this->queryobject($request,$data);
		} else {
			$data['nametag'] = $request->tgl_awl.' S.d '.$request->tgl_akhr;
			$data = $this->queryrincian($request,$data);
		}
		echo json_encode($data);
	}

}
